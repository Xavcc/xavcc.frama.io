---
title: About
layout: page
---
![Profile Image]({{ site.url }}/{{ site.picture }})
 <figcaption class="caption">« Do not trust any dark boxes! »</figcaption>
 
## Recherche-Action

#### Tables des contenus

1. [Ce que je fais](#ce-que-je-fais)
2. [Nous avons travaillé ensemble](#nous-avons-travaille-ensemble)
3. [Petits brins antiparallèles](#petits-brins-antiparallèles)
4. [Compétences](#compétences)
5. [Projets](#projets)
6. [Recherches](#recherches)
7. [Hackathons et IndieCamps](#hackathons-et-indiecamps)
8. [Livres Numériques](#livres-numériques)
9. [Qui je suis](#qui-je-suis)

## Ce que je fais

Actuellement, je travaille sur la configuration en tiers-lieux… **Tiers-lieux en situation critique / Third Places in Critical Situation** en tant que chercheur indépendant, ce qui je décline en 3 *terrains* d'observation/documentation participante embarquée :

+ Les tiers-lieux et leurs communautés qui pratiquent l'enquête environnementale
+ Les tiers-lieux et leurs communautés LGBTQIA+
+ Les tiers-lieux et leurs communautés de l’hospitalité, de l’accueil, des réfugié⋅e⋅s, des migrant⋅e⋅s, « sans-papiers / undocumented people ».

Aussi, Co-fondateur du Biohackerspace [Kaouenn Noz](https://wiki.kaouenn-noz.fr/), Membre de [Petites Singularités](https://ps.lesoiseaux.io/), consultant / formateur / auteur chez Tactical Tech (2019 - 2023), contributeur au [Public Lab](https://publiclab.org/), consultant pour [Association for Progressive Communication](https://www.apc.org/) sur Next Generation Internet Zero (NGI 0).


### Je viens de l'univers du biohacking, biopunk, biomimétisme.


Le biohacking est un ensemble de pratiques, des sciences et techniques et arts dits du _vivant_, lié à une approche de la biologie. Le Biohacking est l'une des situations de la biologie participative. 

Le biohacking consiste en une articulation de pratiques appuyées par une philosophie :

+ libre accès à l'information et la connaissance ;
+ étude et compréhension des phénomènes et fonctionnements du vivant ;
+ déconstruction des outils d'étude et d'analyse du vivant ;
+ détournement des méthodes et outils de travail ;
+ détournement des fonctionnements du vivant vers des finalités nouvelles ;
+ fabrication d'outils pour réaliser des expériences scientifiques et artistiques.

Le biomimétisme, s’inspirer du vivant pour résoudre des défis de fabrication, est un ensemble de disciplines et de pratiques qui a pour dessein d’imiter les matériaux, les formes, les mécanismes et les systèmes éprouvés par les conditions, les exigences et les contraintes de l’évolution du vivant. Ces velléités de conceptions sont liées aux notions de transfert de processus biologique et / ou physique et / ou chimique dans les nouvelles fabrications humaines.

> "_In Reductionist perspectives and the notion of information Nicolas Perret and Giuseppe Longo argue that “**the introduction of information theory into biology did not liberate biology from reductionism and that the adoption of information in biology is an erroneous transposition from a specific mathematical domain to one where it does not belong**_“ "

Le biomimétisme est aussi un processus d’apprentissage, basé et implanté au sein de l’expérimentation et du prototypage, interrogeant l’éthique, promettant une écologie (science de l’environnement) et des moyens plus soutenables (respect de l’environnement et de ses limites). Le travail de conception, le travail de biologie, le travail du code, le travail de l’architecture, et les autres disciplines, ne sont que des effets de bord de ce processus d’apprentissage. Nous devons apprendre à apprendre et apprendre à partager les connaissances, les pratiques et les documentations pour exercer le biomimétisme. Le Biomimétisme sans prototypage, sans expérimentation, sans connaissance libre, n’est une soutenable. Pour ces conditions, nous avons besoin d’un système libre et en source ouverte.

Prototypage rapide, tests et améliorations, détournement, apprentissage, aux services du ré-investissement dans la biosphère, écologie des communautés ; c'est dans les [Tiers-Lieux Libres et Open Source](http://movilab.org/index.php?title=Utilisateur:XavCC) que j'entreprends, apprends, entredonne, aujourd'hui. 

```
Open source Software - Open source Hardware 
               - BIOHACKING -
Open source Wetware - Open Knowledge

```
Si la fabrication de prototype, de la rétro-ingénierie d'auto-test de chromato-immuno-graphie, ou des protocoles d'investigations de l'eau, ou l'amélioration d'un robot sous-marin par exemple, sont des faits courants de mes activités, mon quotidien consiste également à rechercher, interroger et tester des modèles d'organisation et gouvernance adaptés. Passant parfois le la BioFabulation / BioFabbing. 

<div class="side-by-side">
    <div class="toright">
        <p>Ces savoir-faire et savoir-être acquis, les capacités de déconstruction, d’adaptation, de frugalité, et les expériences vécues collectivement font que régulièrement j’accompagne des organisations publiques ou privées dans la résolution de leurs problèmes, ainsi qu’à la formation et l’enseignement en école, université ou secteur associatif..</p>
    </div>
</div>

<div class="breaker"></div>

### Je pratique la recherche-action

+ [Norae: Notes Ouvertes en Recherche-Action Embarquée](https://notecc.kaouenn-noz.fr/norae:start)

### Je pratique les formations

+ [Ateliers et formations](https://notecc.kaouenn-noz.fr/atelier:start)

### Je conseille les organisations (collectivités, entreprises, associations...)

Depuis 2013 partout en France et ailleurs.

Bruxelles, Berlin, Rennes, Paris, Lyon, Pontivy...

## Nous avons travaillé ensemble

+ [Petites singularités](https://ps.lesoiseaux.io/) ASBL, 2024 and counting
+ Association for Progressive Communication <apc.org>, on [NGI0], 2023 − 2024. Consultancy − Associate Communications on Next Generation Internet (NGI), an European Commission initiative that aims to shape an internet that responds to people’s fundamental needs, including trust, security and inclusion
+ [L’École des arts appliqués de Prague](https://fr.wikipedia.org/wiki/%C3%89cole_des_arts_appliqu%C3%A9s_de_Prague), [Aquatic Alliances 2023](https://archive.org/details/handbook-aquatic-alliances-2023) *en tchèque* Vysoká škola uměleckoprůmyslová v Praze,  abrégé en UmPrum
+ Mastère « [Design de l'anthropocène / MSc Strategy and Design for the Anthropocene](https://web.archive.org/web/20230130195251/https://strategy-design-anthropocene.org/fr/msc-strategy-and-design-for-the-anthropocene) », Alexandre Monnin (Politiser le renoncement), semaine de formation 2023.
+ [Reprises des Savoirs](https://www.reprisesdesavoirs.org/), chantiers Pluri-versités, à Rennes, sur « [Louzaouiñ Graines de luttes 2022](https://wiki.kaouenn-noz.fr/hors_les_murs:reprises_des_savoirs:louzaouin_graines_de_luttes#fragments_documentaires)
+ Université de Nantes, Journée des Libertés Numériques 2022 : <https://videos.lescommuns.org/w/jpHcvAV5AA6GaWeLs6LGCb>, « Le biohacking, le biopunk, le Do it yourself : petites histoires de libertés dans la cité »
+ Tactical Tech (NGO), 2019 à 2023.
+ 4 dates de cours et formations libres à Rennes en 2019 pour des participant⋅e⋅s allant des sans-papiers au dirigeant de PME <"https://notecc.frama.wiki/atelier:agenda-et-inscriptions>
+ Festival des Sciences 2021, Saint-Brieuc
+ [Conception et réalisation du MediaLab pour Numérique en commun 2018](http://movilab.org/index.php?title=Medialab_NEC_2018)
+ TransDev Tvo, coopération et politique RSE & DD du groupe 2014
+ École européenne supérieure d'art de Bretagne, prise en stage numérique et prototypage avec des étudiant⋅e⋅s
+ ICAM, l’Institut Catholique d’Arts et Métiers ; formation d'un groupe d'élèves ingénieur⋅e⋅s au prototypage dans un fablab
+ ESC Rennes, formation de 2 promotions Executive MBA au design en 2016 et 2017
+ Médiathèque de Pontivy, workshop Biomimétisme, numérique, écologie et robot sous-marin pour science océanique
+ Réseau des médiathèques du Morbihan, Faire tiers-lieux, sciences et hacking en médiathèque
+ Médiathèque de Bretigny sur Orges, intervention biomimétisme pour la semaine de la science
+ Sorbonne Université - Université des Technologies de Compiègne, workshop de biomimétisme
+ Réseaux des FabLabs de L'Ouest, mise en place de coopération et collaboration à l'échelle inter-régionale
+ École des Métiers de l'Environnement de Rennes, Formation au biomimétisme des ingénieur⋅e⋅s diplomé⋅e⋅s se spécialisant en éco-conception. 2017, 2018, 2019, 2020
+ Laboratoire Artistique Populaire, Workshop de formation au biomimétisme, Renne
+ [Cours collectif indépendant](https://xavcc.frama.io/rennes-design) aux étudiant⋅e⋅s ingénieur⋅e⋅s inter-écoles en technologies</li>
+ Marseille Provence Métropole Capitale Européenne de la Culture 2013 : Conception, mise en œuvre et administration de « l'Opération Bouchon »

## Petits brins antiparallèles

Human Collider, Biohacker, transitioner, commoner et un peu plus encore

> Natural born explorer autour des pratiques collaboratives et des humanités.

Hacktivist, chercheur indépendant, entrepreneur, consultant ou stratégiste parfois mais toujours passeur et formateur. Autodidacte et profondément en collaboration.

Ex-abnégateur en tenue d’urgence sur 10 ans de vie (Bataillon des Marins Pompiers de Marseille), encore aujourd'hui au service des populations et de l’environnement dans une autre approche toujours humaniste.

Breton amoureux des océans et contemplateur de la biosphère.

+ Podcast : [Hors Circuit](https://open.audio/channels/horscircuit/)

## Compétences

<ul class="skill-list">
	<li>Organisation collaborative, gouvernance</li>
	<li>Innovation libre et ouverte</li>
	<li>Pratiques collaboratives, coopération ouverte</li>
	<li>Processus de consensus, design des instances</li>
	<li>Biomimétisme, biodesign, biohacking</li>
	<li>Écologie, écologie des communautés</li>
    <li>Biologie et Microbiologie</li>
	<li>Rétro-ingénierie</li>
	<li>Investigation & OSINT</li>
	<li>Linux</li>
	<li>Bash</li>
	<li>git, gitlab</li>
	<li>C, C++</li>
	<li>Arduino, Raspberry, ESP8266</li>
	<li>Python</li>
	<li>JavaScript</li>
	<li>Markdown, LaTeX, ASCIIdoc, Wikicode, dokuwiki</li>
	<li>Documentation</li>
	<li>Scrum and Kanban</li>
	<li>Hygiène et sûreté numérique, sécurité holistique</li>
</ul>


## Projets 

Les projets sur lesquels je m'investis sont valables sur un organigramme de flux d'une durée de 6 mois, renouvelés ou non.
<ul>
    <li><a href="https://xavcc.frama.io/appel-dons-pour-developper-test-enquete-environnementale/">Bioessai à base de graines</a> pour étude et enquête sur les qualités de l'eau en licence lire</li>
	<li><a href="https://web.stanford.edu/group/prakash-lab/cgi-bin/labsite">PrakashLab</a>, Stanford, bio-engineering & pregnancy test <a href="https://www.frugalscience.org/">Frugal Science</a> program</li>
	<li><a href="https://xavcc.frama.io/biononymous">Biononymous, Bioprivacy</a></li>
	<li><a href="https://exposingtheinvisible.org">Tactical Tech - Release Data</a> : Recherche-Action et formation aux pratiques de la bioinvestigation et publication avec mise en valeur et visualisation des données</li>
	<li><a href="https://notesondesign.org/xavier-coadic">Rivières Pourpres</a> « Prendre prise sur le quotidien d’un monde catastrophé »</li>
</ul>

### Contributions

+ Hackteria, Open Source Biological Art, DIY Biology, Generic Laboratory Infrastructure
+ Public Lab, democratizing science to address environmental issues that affect people.
+ Petites Singularités (ASBL)
  + OFFDEM 2023, Over Fearless Freedom Dead Empire Marvel
  + OFFDEM 2022, the OFFLINE Developer European Meeting
  + [DREAM](https://dream.public.cat/) (Distributed Replicated Edge Agency Machine)
  + Trans Hack eXploration, THX 2021
+ Tactictal Tech, 
  + Auteur de chapitres et contributeur à la traduction française [Exposing The Invisible](https://kit.exposingtheinvisible.org/)
  + Contributeur et formateur “Investigation Is Collaboration” 2020 & 2021
  + Formateur "Exposing the Invisible" Investigative Institute, project supported by the European Commission (DG CONNECT)
  + DataDetox Kit <https://datadetoxkit.org/fr/home>
  + The GlassRoom <https://theglassroom.org/>
+ Movilab, l'encyclopédie libre et vivante des Tiers Lieux
+ [Faire École Ensemble](https://faire-ecole.org/)
+ La [Cimade](https://fr.wikipedia.org/wiki/Cimade), Comité inter-mouvements auprès des évacués, dev et orchestration (Ansible) , sécurité de l'information et des infrastructures réseaux
+ [Gendersec](https://en.gendersec.train.tacticaltech.org) , [Gender & Tech Ressources](https://gendersec.tacticaltech.org/wiki/index.php/Main_Page)
+ Medialab, [Numérique en Commun(s)](https://movilab.org/wiki/Medialab_NEC_2018), 2018
+ [DAISEE](https://web.archive.org/web/20180318124634/http://daisee.org/), Internets of Energy | Energy as a Commons
+ [core contributeur](http://movilab.org/index.php?title=Utilisateur:XavCC) sur [Movilab.org](http://movilab.org) qui constitue le capital informationnel commun des Tiers Lieux
+ [core contributeur](https://github.com/multibao/site/blob/master/README.md) sur Le carnet de bord des organisations novatrices
+ ex-Membre [Digital Athanor](http://www.digital-athanor.com/team/xavier-coadic)
+ [La MYNE](https://www.lamyne.org/) - Manufacture des Idées et des Nouvelles Expérimentations - un laboratoire citoyen situé à Villeurbanne
+ Contributeur au fonctionnement de [l'Hôtel Pasteur](http://www.hotelpasteur.fr) à Rennes en 2018
+ Co-commissaire de la Biennale internationale du Design de Saint Étienne, L’expérience Tiers-Lieux – Fork The World

## Recherches

Les recherches que je mène actuellement :

1. Tiers-Lieux et situations Critiques
   + Configuration en tiers-lieux sur les enjeux d'enquête environnementale
   + Tiers-Lieux et hospitalité, accueil des réfugié⋅e⋅s
   + Communautés LGBTQIA+ et Tiers-Lieux
2. Biologie de garage, Biohacking, et (auto) soin / fabrication de dispositifs d'aide à la « _santé_ ».

### Précédent groupe de travail de Recherche

1. [Design des instances et consensus](https://xavcc.frama.io/recherche-consensus), **recherche action**
2. [Biononymous et Bioprivacy](https://xavcc.frama.io/biononymous-bioprivacy-research)
3. [Tiers-Lieux Libres et Open Source pour répondre aux enjeux critiques](https://xavcc.frama.io/recherches-tiers-lieux), **recherches action et documentation**
   + Libertés et sécurités informatique
   + Prévision et prévention des accidents corporels
   + Libertés et égalité au-delà des genres et des classes
   + Accessibilité, perméabilité, handicaps
   + Bioéthique et sciences ouvertes
   + Biononymous et bioprivacy
   + [Questions sur la gestion des risques dans les tiers lieux](http://movilab.org/index.php?title=Questions_sur_la_gestion_des_risques_dans_les_tiers_lieux)
4. [Océans, Espace, Villes, les interfaces de résilience](https://community.oscedays.org/t/ocean-space-city-interfaces/6555), **recherche expérimentation**
   + Les micro-organismes et la bioremédiation
   + Les données spatiales pour la prévention et prévision des risques
   + Les Internets de l'énergie, l'énergie comme un commun

## Hackathons, indieCamps, OFFDEM

Ceux dans lesquels je me suis investi comme co-organisateurs, formats plaçant l'éthique et la non-exploitation des humains en pré-requis :

<ul>
	<li><a href="https://oscedays.org">Open Source Circular Economy Days</a> Depuis Rennes avec près de 200 villes dans le monde. 2015, 2016, 2017, 2018</li>
	<li><a href="http://nuitcodecitoyen.org">Nuit Du Code Citoyen</a> avec plusieurs villes francophones, depuis Rennes, 2017, 2018</li>
</ul>	

### Indiecamp

+ Fiche de synthèse pour les municipales 2020 « [Quelles politiques de territoire](https://notes.ecrituresnumeriques.ca/fk3PUx00RVG6_onr4ek7AQ.html) ? »

Je fais partie également des personnes qui ont conçu et réalisé les premiers [IndieCamps](http://movilab.org/index.php?title=IndieCamp), 2016 à Kerbors (22), 2017 Nevez (29) et Kerbors, 2018 Kerbors. Formats Libres de créations, de prototypages, d'expériences et de vécus, écologiques et démocratiques.

10 Indiecamps ont eu lieu entre 2016 et 2022 dans plusieurs *coins* de France.

### OFFDEM

+ [Over Fearless Freedom Dead Empire Mervel](https://offdem.net/): 2022, 2023, 2024


## Livres numériques


+ [the Transgenic Human Genome Alternatives Project](https://archive.org/details/thGAP/) (thGAP). Hackteria. Identifier-ark 13960/s2cwkb0hh7x
+ [Présence Solidaire. Pænser Ensemble : Le collectif et le soin radical](https://thx.zoethical.org/t/presence-solidaire-publication/19) (Petites Singularités. ISBN 978-2-9602651-4-9)
+ [Anti Livre d'un Arthur Rimbaud Zap, ou l'enfer de Rimbaud à ciel ouvert](https://gitlab.com/antilivre/rimbaud.zap/-/blob/master/gabarit-abrupt/enfer-impression.pdf), chez C'est Abrüpt (ISBN : 978-3-0361-0164-4)
+ La revue Sens-public pour un travail concernant, « [De l’hypothèse de la documentation comme technique de résistance et du wiki comme objet de ces résistances](https://sens-public.org/article1375.html) »
+ « [Vivre ensemble, Faire ensemble](https://legacy.gitbook.com/book/xavcc/vivre-ensemble-faire-ensemble/details) Bretagne Fablab tour 2015. Co-auteur (en cours de finition)
+ [LabOSe: Laboratoire open source d’expériences libres et distribuées](https://xavcc.gitbooks.io/labose/content/), 4 mois et demi en tour de France dans les communs. co-auteur (en cours d'édition)
+ **Tiers-Lieux : Fork The World**, après la Biennale de Design de Saint-Etienne (9 mars - 9 avril 2017) consacrée aux mutations du travail


## Qui je suis

Breton de la terre et de la mer, c'est un des avantages d'être nomade, j'ai toujours parcouru différents univers culturels et de pratiques pour apprendre et pour partager. Depuis la co-création d'une première association à l'âge de 15 ans à ma première entreprise à 30 ans, j'ai toujours gardé à corps et cœur de cultiver l'abnégation, l'éthique et la préservation des écosystèmes et des communautés.

J'ai commencé à travailler adolescent mais c'est à l'âge de 20 ans que je décide de consacrer 10 ans de ma vie au secteur des Urgences chez les Marins pompiers de Marseille. Travaillant, apprenant sans cesse, et formant, à la sécurité des personnes, des biens et de l'environnement, la lutte contre les catastrophes naturelles et industrielles, la gestion de leurs conséquences, la prévention et la prévision des risques en milieu urbain et naturel péri-urbain, j'ai acquis autant de savoir-faire que de savoir-être.

En 2010, j'entre pour la première fois dans un hackerspace alors que je reprends, en parallèle de mon travail, des études en biologie, Master puis Master 2 de recherche en microbiologie. La recherche universitaire étant cloisonnée, c'est en début de 7ème année que je quitte cet univers pour celui des Tiers-Lieux. Depuis ce jour je continue à être un membre contributeur et porteur de projets dans plusieurs communautés de pratiques libres et ouvertes liées aux hackerspaces et FabLabs. Ceci me permettant de pratiquer et d'apprendre en transdisciplinarité, voire en antidisciplinarité. 

C'est en évidence la continuité de mon engagement dans la préservation des conditions de vie, de sauvegarde de la biosphère, et des libertés individuelles et collectives, qui s'exprime dans ces activités professionnelles et personnelles. 

Ce qui m'a mené à co-fonder un hacklab de biomimétisme avec des ami⋅e⋅s à Rennes en 2014, le Biome. Continuant ainsi une forme possible de Tiers-scientifique et d'entrepreneuriat, au sens profond de résoudre des défis. 

De 2014 à 2020, entre tour de France de hacklabs, collaborations sur 3 continents, et implications dans des ONG reconnues à l'international, les formations libres à tout public et les cours en école d'ingénieur⋅e⋅s : j'ai essayé de  cultiver, non pas de conserver figé, la curiosité, le partage, la sincérité, qui me font avancer depuis 20 ans, y compris en pensant à chaque fois que nécessaire contre-moi même − d'une forme d'hypothèse qui nécessité d'être réfutable pour être travaillée et vivante. 

<a rel="me" href="https://todon.eu/@XavCC">XavCC on Mastodon</a>

---
title: "Le jour où j’ai (re)découvert TALER − Taxable Anonymous Libre Electronic Reserves"
layout: post
date: 2024-05-25 22:30
image: /assets/images/fossa-2014.png
headerImage: true
tag:
- hsociety
category: blog
author: XavierCoadic
description: Practical and Provably Secure Electronic Payments. C’est politique ! BORDEL !
---

TALER est une solution logicielle de paiement et de sécurisation d’accès développée depuis plus de 10 ans. De son origine technique et académique aux agencements actuels liés à son existence en tant qu’opportunité, plusieurs buissons sont apparus, et des racines me sont revenues en mains. Je vais tenter de vous en partager quelques embranchements.

Remonter dans le temps est un exercice amusant. C’est tenter de trouver un chemin entre souvenirs, archives, sentiments, croyances, traces. C’est confronter ce que l’on imagine avec ce qui est figuré et avec des documentations produites par plusieurs tiers.

En février 2023, peu de temps après le festival [OFFDEM.0x03 / Ozone](https://oxygen.offdem.net/g/offdem.0x03), je prends connaissance de l’existence du projet TALER dans le forum des [petites singularités](https://ps.zoethical.). [TALER](https://taler.net/fr/) c’est pour "Taxable Anonymous Libre Electronic Reserves", et avec GNU Taler qui est un projet libre contenant un protocole de communication avec chiffrement, des applications et des services pour un système de paiement électronique, son code source et sa documentation technique. Sécuriser par (*pseudo*)anonymisation des échanges d’informations, notamment bancaires, c’est regarder les droits des humaines dans une optique politique.

![](/assets/images/TalerDiagram2.png "Représentation graphique du fonctionnement de TALER. Votre banque met à disposition de la monnaie, TALER avec son logiciel permet de créer un jeton pour un montant sélectionné. Ce jeton permet de payer cette somme à un⋅e commerçant⋅e qui utilise aussi TALER. Ce processus est auditable. Læ commerçant⋅e dépose ce jeton auprès de sa banque qui utilise TALER pour recevoir le montant en monnaie correspondante")

J’y voyais beaucoup de point d’intérêts avec mes récentes *aventures* et cela éveillait aussi de la curiosité en moi. Quelques rappels d’alertes s’étaient aussi activées en considérant les problèmes non résolus et souvent non traités de privilèges dans le logiciel libre, des rapports de dominations, de gros soucis de gouvernance. En bref, j’étais enjoué et sans naïveté. Aussi, un petit *je-ne-sais-quoi* bourdonnait dans ma tête sans que je sache quoi, ni pourquoi. J’y reviendrai en fin de ce billet.

Aujourd’hui je travaille avec les petites singularités (depuis février 2024), notamment sur l’"[Integration Community Hub de Taler](https://ich.taler.net/)" et l’ASBL petites singularités est membre du consortium européen [Next Generation Internet TALER](https://nlnet.nl/taler/background/#organisation). Cela implique que le financement de ce projet pilote est assuré par la Direction Générale CNECT de la Commission européenne (Communications Networks, Content and Technology) et le Secrétariat d’État suisse à la formation, à la recherche et à l’innovation (SERI). 

Un programme NGI implique le consortium NGI TALER dans l'attribution des financements (grants) par appel à projets. Il s’agit de *grants* entre 5 000 et 50 000 euros pour des logiciels libres et des efforts de préservation de la confidentialité contribuant à ou utilisant TALER. [L’appel est ouvert](https://nlnet.nl/taler/) (c’est le second depuis Mars 2024) aux PME, aux universitaires, au secteur public, aux associations à but non lucratif, aux communautés et aux individus. L’[Open Call Desk](https://ich.taler.net/pub/open-calls-helpdesk) pourrait aussi vous être utile.

Si tout ceci vous intéresse, je vous conseille de vous porter sur la "[NGI TALER Roadmap](https://ich.taler.net/t/ngi-taler-roadmap/53)" de 2024 à 2027.

Pour les personnes qui voudraient candidater aux appels à projets, pour les personnes qui voudraient contribuer, pour les personnes qui voudraient approfondir les usages de Taler, il y le forum [Community Hub](https://ich.taler.net/) qui est à votre disposition avec ses [Community Guidelines](https://ich.taler.net/t/community-guidelines/40).

Les professionnels du secteur du livre ont [une place de choix](https://ich.taler.net/t/about-the-independent-book-sector-category/28) dans ce *Community Hub*. Et pour une de mes parts, j’ai commencé à travailler avec des journalistes et étudiant⋅es en journalisme sur leurs [besoins et usages à venir de Taler](https://ich.taler.net/t/taler-with-journalists-for-journalists/169). Je renouvellerai cela à [Bruxelles en mai](https://ich.taler.net/t/lets-meet-in-brussels-at-the-end-of-may-2024/120). Le **Integration Community Hub** est ouvert à toutes les personnes intéressées par TALER et les sujets directement liés.

Odoo, Pretix, WooCommerce, Joomla, Mirage OS ont commencé à intégrer TALER (voir [ici dans l’ICH TALER](https://ich.taler.net/c/integrations/28)).

Pour écrire ce billet blog j’ai donc remonté mes souvenirs, sentiments, archives depuis février 2024 jusqu’à la fin d’année 2022 et la préparation de l’OFFDEM.0x03. C’est en faisant cela que le bourdonnement dans ma tête s’est *terminé* et un souvenir clair m’est revenu.

## Un peu plus loin…

En novembre 2014, à Rennes en Bretagne, avait lieu le Free Open Source Software for Academia (FOSSa), *crossroads of openness* pour cette édition à L’Institut National de Recherche en Informatique et en Automatique (INRIA). J’y étais convié pour présenter les activités d’un tout récent hacklab/biohackerspace sorti de terre. Les sites ou pages web FOSSa ou Carrefour des Possibles de l’époque ne sont plus accessibles et les archives numériques très parcellaires (voir par exemple une [archive de 2015](https://web.archive.org/web/20160313060818/https://fossa.inria.fr/press/)). Le FOSSa n’existe plus en tant que festival annuel.

Dans la même salle, Amandine Le Pape présentait Matrix, protocole standard ouvert. Dans la même salle, Guillaume Collet présentait "PIGTRONIC - Genome Assembly on a Raspberry Pi". Dans cette salle il y eut une conférence de 26 minutes pour présenter TALER − Taxable Anonymous Libre Electronic Reserves. C’est Christian Grothoff qui présentait TALER, il était alors Group Leader at INRIA *in* "DÉCENTRALISÉ" (2014 -2017). J’avais donc déjà croisé la route (*dans un carrefour*) de TALER il y a 10 ans. J’avais un peu oublié le passage et la rencontre…

Il est difficile d’écrire, et de décrire, des histoires passées avec peu d’archives et d’autant plus avec les interprétations inhérentes aux souvenirs. Aussi, je pourrais tenter un exercice similaire depuis d’autres évènements de rencontres et discussions : Festivals, Hackmeeting, IndieCamps, etc. J’appuie ici et maintenant sur l’importance de ces évènements, et du soin porté à leurs configurations sociales, comme éléments cruciaux de structuration de projet socio-techniques, de leurs émergences et de leurs continuités, y compris des approches critiques. Et je rappelle que documentations et archives sont des vertèbres incontournables.

En 2019, Florian Dold (Université Rennes I) défendait sa thèse doctorale “The GNU Taler System − « Practical and Provably Secure Electronic Payments »” (Unité de recherche : Inria), Christian Grothoff était directeur de cette thèse et Alan Schmitt président du Jury (INRIA / IRISA laboratory).

En juin 2022, je tentais à faciliter un évènement à Rennes qui viendra plus tard.
En mars et avril 2023, par l’intermédiaire d’Alan Schmitt, j’aidais à faciliter une rencontre à l’INRIA de Rennes, pour présenter et discuter de [Distributed Replicated Edge Agency Machine](https://dream.public.cat/) (DREAM) − *software associating P2P protocols and CmRDT-based implementation—also known as operation-based Conflict-free Replicated Data Types (CRDT). Internet protocols and applications to empower citizens to act and find agency together*.

Depuis 2014, il y avait eu des changements à l’INRIA, moi aussi j’avais changé.

En décembre 2023, NGI TALER est [annoncé](https://taler.net/en/news/2024-02.html), pour une *timeline* plus complète voir [ici](https://taler.net/en/news/index.html).

### Un consortium Européen…

Un consortium nommé Next Generation Internet (NGI)<marginnote>Il y a des dizaines de programmes NGI et oui, Oui, NGI, à cette date, c’est une ombrelle Européeno-centrée dans ses directives stratégiques</marginnote> TALER existe avec son fond de fonctionnement dédié[^note-1] [^note-2] [^note-3]. Il faut comprendre *consortium* dans les cadres définis pas « L’accord de consortium, version Horizon Europe » (*[Horizon Europe programme guide](https://ec.europa.eu/info/funding-tenders/opportunities/docs/2021-2027/horizon/guidance/programme-guide_horizon_en.pdf)*), en tant qu’*équipe* composée d’au moins trois organisations partenaires de trois pays différents de l’UE ou associés. Au moins un des trois partenaires doit être originaire d’un pays de l’UE. Outre ces trois partenaires, des organisations d’autres pays peuvent se joindre aux consortiums.<marginnote>Le "Collaborative and Investigative Journalism Initiative" (CIJI) https://www.ciji.info/about est aussi une forme de consortium financé par l’EU</marginnote>

Le consortium NGI TALER est composé avec 11 organisations de 8 pays EU et Zones EU (Netherlands, Belgium, France, Germany, Greece, Hungary, Luxembourg and Switzerland). Il  ya là une diversité de structures, avec de l’académie universitaire (Eindhoven University of Technology) un département de recherche (Berner Fachhochschule BFH), des organisations de petites tailles sans but lucratif (Code Blau GmbH, Taler Systems S.A., VisualVest), une association (petites singularités ASBL), deux banques coopératives (GLS Bank, MagNet Bank), une fondation (NLnet) et 2 coordinations civiques (E-Seniors Association, Homo Digitalis).

Les axes de travail sont répartis en « paquets » entre les organisations qui ont des rôles et responsabilités distribuées, et une même organisation peut participer à plusieurs « paquets ». Par exemple :
+ Implémentation verticale de TALER dans un secteur spécifique,
+ Communication,
+ Gestion des candidatures des Appels à Projets,
+ etc.

Un consortium de cette *trempe* c’est l’endroit pour vous et moi, pour de petites structures associatives (*de nos tailles respectives, de nos positions socio-professionnelles et nos sentiments "d’impostures"*) de porter et tenir des voix à une échelle et avec un écho international, jusqu’aux marches des Directions Général des *Hautes Institutions*. C’est aussi l’opportunité de faire cela depuis et avec nos localités et de ramener des fruits dans ces localités, jusque dans nos adelphités et amicalités. TALER dans ta librairie, TALER dans ton snack, du droit à la vie personnelle et intime protégée dans nos quotidiennetés.

J’écris ces lignes à quelques jours des élections européennes et dans un contexte de pérennisation de politique d’extrême droite dans de nombreux pays (politique montante depuis 2 décennies) comme ligne principale, de fascisme trans-frontalier, et de sondages très favorables aux partis racistes pour le parlement européen dans plusieurs pays de la zone EU. Des nazis viennent tout juste de défiler le bras levé en plein Paris, sans être inquiétés et en tétant encadrés par la police. J’ai lutté et je lutterais encore contre eux et leur projet politique.

### C’est politique !

Je réalise un truc qui me semble très important en essayant d’écrire…

Enfant et en début d’adolescence je vivais dans un rapport au Monde où nous pouvions faire des choses en groupe en se foutant des barrières et des frontières. Les images télévisées de la chute de mur de Berlin étant l’un des souvenirs les plus profondément marquant en moi, encore aujourd’hui.

Puis dans l’adolescence cela fut amenuisé, le FN, parti raciste d’extrême droite, en France, le FN à Marignane, Toulon, Vitrolles, Orange, Dreux… Mes ami⋅es enfermé⋅es par la police et l’administration., expulsé⋅es, pourchassé⋅es.

Jeune adulte ce rapport au Monde fut enfermé dans une boîte au relais des oubliettes. Il me fallait être læ travaillaire formaté·e dans le moule que l’on m’avait destiné et rester dans les restrictions du cadre imposé.

Les expériences professionnelles, même internationales et collaboratives - *humanitaires ou d’urgences, ect*, restaient là dedans. Une matérialisation du *rêve des machines*, « Si nous sommes considérés comme de meilleurs travailleurs, notre consommation sera d’autant plus satisfaisante et nous pourrons accomplir plus rapidement le devoir qui nous incombe d’œuvrer comme liquidateurs […] C’est pourquoi les armes sont des produits idéaux. C’est pourquoi la consommation des armes, comme la mort des victimes de guerre, représente la consommation idéale » (<cite>Günther Anders</cite>, 1960).

> « Vous admettrez que l’on ne fabrique pas plus des armes pour les laisser rouiller qu’on ne cuit des pains pour les laisser rassir. Elles sont destinées en fin de compte à ceux qui doivent les recevoir et les consommer : par conséquent aux victimes. […] L’utilisation des produits est un travail que nous, employés nommés "clients ou "consommateurs", avons à fournir pour assurer la fabrication de nouveaux produits et garantir la poursuite de la production. »
>
> <cite>Günther Anders</cite>, 1960

Aujourd’hui parvient l’opposé de la promesse politique, *paix et « fraternité »*, que l’on nous vendait depuis des décennies. Extrême droite, fascisation et surveillance, haines et cleptocracie, paupérisation et remilitarisation. C’est ça en France et ailleurs en Europe, et hors de l’EU.

Aujourd’hui, en travaillant dans le consortium européen NGI TALER (*et aussi dans [NGI0)](https://nlnet.nl/NGI0/)*[^note-apc]), avec des personnes de différents pays, de différentes perspectives et différentes structures, ce rapport au Monde tenu plus jeune m’est revenu comme une petite musique possible et tangible. Cela se passe même en vécu incarné, sans naïveté je le répète. Ce n’est ni aisé, ni simple, ni facile ; je le dis pour mois et je le pense pour les personnes avec lesquelles je travaille. C’est imparfait et emprunt dans des héritages capitalistes, colonialistes et sexistes. Pour toutes ces choses et infrastructures actuelles influentes[^note-infra], ne fermons pas sur cela nos yeux ni nos oreilles, ni nos paroles et encore moins aujourd’hui. C’est pourtant bien là, ce consortium avec son Integration Communtiy Hub (ICH) est là et ouvert. C'est une particularité du NGI TALER, mettre à disposition un espace d'organisation collective pour es personnes intéressées. Et c’est puissamment important ! Nous sommes là, là où nous, entités individuées, ne pouvions pas être il y a encore 10 ans.

Notre situation en tant que consortiums (NGI TALER et NGI0) est fragile et nos fonctionnements peu clairs. Nous avons beaucoup d’étape à franchir, **notamment dans la transparence de nos processus de décisions et dans la définition publique de qui nous sommes et ce que nous faisons**, et la communication interne, entre autres.

S’assembler est un droit et c’est politique ! (<cite>Article 20.1 UN Human Rights</cite>).

Nous nous *assemblons*<marginnote>Déclaration universelle des droits humaines, 1948. Article 20: 1. Toute personne a droit à la liberté de réunion et d’association pacifiques. 2. Nul ne peut être obligé de faire partie d’une association.</marginnote>, nous discutons des choix avec de grandes conséquences (*qui nous dépassent, je vous le dit honnêtement*), après décisions actées nous mettons en œuvre des opérations avec leurs chaines de *goods* et d’incertitudes.<marginnote>Et qu’il est important et et agréable de faire place aux incertitudes… !!! Déjà ça c’est une lutte !</marginnote>

La technique et les configurations sociales sont politiques et des moyens de transformations. J’affirme ma position anti-différentialiste[^note-diff] [^note-diff-2], la technique et la science et le social sont politiques. Il y a dans et avec la technique une ivresse et un jeu qui peut nous prendre en tourbillon et nous aspirer dans l’inconscience matérielle de nos actes et de nos engagements. Il nous faut être affûté⋅es, réunies et force de proposition dans cela, et aussi face au projet de l’EU « [d’identité numérique forte](https://www.europarl.europa.eu/topics/en/article/20230302STO76818/european-digital-identity-easy-online-access-to-key-services) », projet de [fusion entre carte d’identité et carte Vitale](https://www.igen.fr/ailleurs/2024/03/fusion-entre-carte-didentite-et-carte-vitale-le-projet-controverse-remis-sur-le-tapis-par-gabriel-attal-142575) en France, et les entreprises de paiement par Carte *Bancaire* qui veulent « [fournir l’historique d’achats de ses clients aux commerçants pour faciliter la publicité ciblée](https://siecledigital.fr/2024/05/17/visa-va-fournir-lhistorique-dachats-de-ses-clients-aux-commercants-pour-faciliter-la-publicite-ciblee/).


## Notes et références

+ <cite>Günther Anders</cite>, *Lettre à lettre à* [Francis Powers](https://fr.wikipedia.org/wiki/Francis_Gary_Powers). 6 août 1960, dans *Le rêve des machines*.

[^note-1]: This project has received funding from the European Union’s Horizon Europe research and innovation programme under grant agreement No. 101135475. Additional funding is made available by the Swiss State Secretariat for Education, Research and Innovation (SERI).

[^note-2]: EU contribution: € 4.508.355,40 ; NGI TALER Coordinator: Eindhoven University of Technology

[^note-3]: 15% of the budget of NGI TALER is reserved for open calls to fund additional free and open source efforts that are aligned with the topics and approach of NGI TALER

[^note-apc]: Je travaille dans NGI0 pour le compte d’Association for Progressive Communication apc.org

[^note-infra]: "*Infrastructures are things that move things. They often define how we live, and what we can or cannot do while claiming invisibility for themselves* <https://situated-infrastructures.kleio.com/>

[^note-diff]: les travaux de Renaud Debailly intitulés « La critique radicale de la science en France » à partir de 1969 (2010), la question du différentialisme et de l’antidifférentialisme (2013) dans ces mouvements critiques, et le chemin de « la contre-expertise et à la remise en question des hiérarchies et des inégalités au sein de l’institution scientifique. » (2023). Debailly, R.(2010), La critique radicale de la science en France : origines et incidences de la politisation de la science en France depuis Mai 1968 / Renaud Debailly ; sous la direction de Terry Shinn
Debailly,R. (2013), « La politisation de la science. Revues éphémères et mouvements de critique des sciences en France », L’Année sociologique, 2013/2 Vol. 63, p. 399-427. DOI : 10.3917/anso.132.0399. Debailly, R. (2023) Challenging science’s autonomy : the "critique des sciences" in France / in "Physis : rivista internazionale di storia della scienza : LVIII, 2, 2023, Firenze : Leo S. Olschki, 2023 , 2038-6265 - Casalini id: 5675370" - P. 313-334 - Permalink: http://digital.casalini.it/10.1400/294941 - Casalini id: 5675373 

[^note-diff-2]: If not us, who? If not now, when? – stories & stances on STS and activism https://easst.net/easst-review/easst-review-volume-421-july-2023/if-not-us-who-if-not-now-when-stories-stances-on-sts-and-activism/
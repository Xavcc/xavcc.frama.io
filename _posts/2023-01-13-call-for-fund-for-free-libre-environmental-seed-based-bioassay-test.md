---
title: "Call for donations to fund and develop a libre and open-source seed-based environmental survey test"
layout: post
date: 2023-01-13 07:40
image: /assets/images/water-sampling.jpg
headerImage: true
tag:
- hsociety
category: blog
author: XavierCoadic
description: "Bioassay and seeds of content"
---

<figcaption class="caption">"Header image description: A person's hand takes water from a dead arm of the Vilaine in Rennes using a plastic tube. This is done from a bank on a vegetated and grassy shore, the water corner is also greened by lentils and other plants. Photo from the Chantier Reprises des Savoirs, Pluri-versités in Rennes, October 2022</figcaption>
<br />

In 2015, on the [Nantes − Brest canal](https://en.wikipedia.org/wiki/Nantes%E2%80%93Brest_canal) sharing weir in Glomel (22), France, I started ecotoxicology experiments in local waters (canal, weir, Korong lake). Since 2019, I have been working on the design, testing, and deployment of a set of methods and techniques for environmental testing of water and soil qualities. I am doing this with the goal that this device can be used in your kitchen, basement, garage, squat, cabin and even from "places of deprivation of freedom (►►)"<marginnote>It's a small thing that I am setting up with partners right now.</marginnote>.

In short, I am currently working with partners to develop a device that can be used wherever and whenever needed, using existing resources and materials, with the aim of producing results of a comparable quality to those obtained in industries and laboratories.

Last Friday morning I experienced a small scene, almost daily for me, which unlocked within me a motivation and a sense of reassurance (which I needed).

I picked up about 50 centiliters of a puddle of water in the middle of a square. It was beautifully iridescent in its reflections.

A lady, who finished her obligation to sacrifice herself to work a long time ago, just passing by me asked me what I was doing.

"I'm picking up this water, then I put it in small glass jars in different quantities with distilled water. Then I put radish seeds in it and observe, photograph and record observations for 10 days."

Silence, and second silence, and looks that dissect me...

"Are you doing this for any laboratory?" she said.

"For you, for the children, for me."

`[Wide-eyed and smiling]`

I noticed that the three workers perched on the scaffolding of the facade of the adjoining building, under construction, have stopped and observe us in silence.

I remember that yesterday I had received an email from university and Higher Education and Research (ESR) classifying this as "oppositional architecture and counter-school". What a curious lexicon!

In my tender anecdotal Friday there are at least 4 points of view that collide through a small gesture and a few words to accompany it in a broader subject that is ecology and the quality of our environment (and our responsibilities and abilities to act).

What I am doing makes sense and is important. I have done it and continue to do it in Brittany, in Dordogne, in Belgium, and in video conferences for people in dozens of countries around the globe. Soon elsewhere, I hope, with a good dose of confidence.

Confined to a laboratory, and reserved for small groups of an "elite," environmental surveys, and pollution questions are the privilege of the privileged. I am trying to make it a political action that is a popular right rather than a privilege.

![](/assets/images/gege-neratoir-bioassay-2023.png)
<figcaption class="caption">Image description: A village square with a church in the background, trees and lawns in the second plan. In the foreground, on the sidewalk, characters drawn by Simon "Gee" Giraudot: A person on the right asks "But what are you doing on the ground, on your knees, with dirty water in your hands?" The other on the left answers: "Fernand Deligny left us with "Graine de crapule" (seed of villain). Well, today, we become Seeds of Struggle. And we open up free ways and capacities to act and take care." Created and assembled with Gégé, the generator of Grise Bouille. Grise Bouille and all its content (images and texts) are the work of Simon "Gee" Giraudot and distributed under Creative Commons By-Sa license. GéGé is an adaptation of Comic Gen by Willian Carvalho made by Cyrille Largillier for Framasoft.</figcaption>
<br />

In the same vein as thousands of people who [monitor the quality of the surrounding air](https://sensor.community/), and others [share biodiversity data](https://www.gbif.org/), we can act similarly on water and soil. This is then creating a common good of production means.

**Table of contents:**

+ [A call for donations on what and why?](#a-call-for-donations-on-what-and-why)
+ [How much money, where to give and why - and what can you give if you have no money?](#how-much-money-where-to-give-and-why---and-what-can-you-give-if-you-have-no-money)
+ [What happens if I don't receive €3,800?](#what-happens-if-i-dont-receive-€3800)
+ [Some previous milestones for this little story](#some-previous-milestones-for-this-little-story)

## A call for donations on what and why?

By collecting water from various sources such as a drain near a construction site, a river, or rainwater, and filtering soil from the ground, then placing seeds and observing their germination, or lack thereof, as well as the shapes and colors of any sprouts, I am conducting [bioassays](https://en.wikipedia.org/wiki/Bioassay) in the field. It takes only 5 to 8 days to see observable results with significant differences.

![](/assets/images/graines-control-serie-1-reprises-des-savoirs-rennes.jpg)<marginnote>Plastic boxes sold for storing jewelry beads can also do the trick and are autoclavable, Plastic Shuttle Cup 2 1/2" with Removable Lid - reusable, Made of polypropylene and can be reautoclaved several times.</marginnote>
<figcaption class="caption">Image description: Radish seeds that have germinated in distilled water in a small glass round container retrieved from a supermarket trash (then disinfected before re-use) which was used to hold a crème brûlée.</figcaption>
<br />

I am dedicating my time and energy to designing an "bioassay" with a manufacturing recipe and implementation under a open-source license, that is actually accessible to people who want and need it, and that [can be used in their everyday living conditions]((https://open.audio/library/tracks/347219/)). In the same time frame, I have fallen into severe poverty and do not have access to my rights to social welfare or other coverage.

This work is already taking shape in two languages through existing documentation:

+ [Investigating Water with Seeds on Public Lab]((https://publiclab.org/notes/xavcc/12-17-2021/investihating-water-with-seeds)) (English)
+ [Initiation to biological tests with seeds to expose toxicity in water](https://wiki.kaouenn-noz.fr/hors_les_murs:hack2eaux:initiation-aux-essais-biologiques-avec-des-graines-pour-exposer-la-toxicite-dans-eau)) on the Biohackerspace Kaouenn Noz wiki (French)

These beginnings of "recipes made available" need to be improved and experiments need to be continued in order to refine the method, protocol, and "kit". I also wrote this morning to Donat Hader and Gilmar Erzinger, authors of "Bioassays Advanced Methods and Applications" (SBN: 978-0-12-811861-0), asking for their help.

> "Other considerations for a practical bioassay include the robustness of the instrument and a long storage time in the case of biomaterials such as extracted DNA, lyophilized bacteria, or plant seeds." "*A comparison of commonly used and commercially available bioassays for aquatic ecosystems*" <cite>Azizullah Azizullah and  Donat-P. Häder</cite> in "Bioassays Advanced Methods and Applications"

This research and development follows the open-source guide I wrote in 2020, "[Bio-investigations on the Field](https://kit.exposingtheinvisible.org/en/bio-investigation.html)" (available in French and English). These efforts are also part of the [Hack2O project]((https://ps.zoethical.org/t/hack2o-intro-explication/6116)), in which I will also be fully committed very soon.

Today I am writing to you as well, asking for your help!

![](/assets/images/Bioinvestigation_montage.png)
<figcaption class="caption">Image description: A hand-drawn illustration on white paper depicting a town square with a pedestrian paving area, trees and circles of landscaped areas at their feet. The illustration is accompanied by photographs highlighting points of interest for the environmental survey: tree leaves, lichens on the trunk and bark, low vegetation at the base, and soil sample collection and water retention point.</figcaption>

<div class="breaker"></div>

This research and development that I am conducting is contained within a framework of criteria:

+ It must be **reproducible and repeatable** (otherwise, it is only a performance). 
  + Failures, including in reproducibility, are [considered essential](https://www.pnas.org/doi/full/10.1073/pnas.1806370115). 
  + The [slow work of debugging is desirable](https://www.codeworks.fr/articles/un-manifeste-du-slow-debug) (and far beyond computer code)
+ **Publication, data, documentation in free and open-source license**; **software, device, hardware, wetware under libre open-source license** (as free as possible)
+ True openness (i.e. **accessibility and inclusivity**). 
  + Respect for different rhythms and ways of proceeding, between and within lives/individuals/practices. 
  + The materiality of access to basic equipment for this "kit/test" and differences according to place of life/social status/economic resources are considered with great attention (example access to seeds, access to glass containers, time available to perform the bioassay, etc.)
+ Involving "citizen" **contributors as peers** (i.e. at least as "co-authors").
+ Methods that focus on values of **humility, equity, and good relationship** with the environment, including the non-human and non-living (example https://civiclaboratory.nl).
+ Taking into account **political issues, potential and actual threats** to individuals who practice.
+ **Safety, individual and collective digital security** (yes, in France you can be arrested for water sampling or have to deal with the national anti-terrorist prosecutor (PNAT) or you can be threatened by a neighbor with a tool or weapon)
  + I have contributed and given training on: 
    + Exposing The Invisible <https://exposingtheinvisible.org/>
    + [Gendersec](https://en.gendersec.train.tacticaltech.org) , [Gender & Tech Ressources](https://gendersec.tacticaltech.org/wiki/index.php/Main_Page)
    + DataDetox Kit <https://datadetoxkit.org/fr/home>
    + Security in a Box <https://tacticaltech.org/projects/security-in-a-box/>
    + The GlassRoom <https://theglassroom.org/>

There is a hidden bonus to this amount of work: I am also developing low-tech devices/aids under open license that complement and/or are needed for seed-based bioassays:

+ [A centrifuge built using beer coasters](https://wiki.kaouenn-noz.fr/hors_les_murs:lab0_bi0_p0p:la_sauce_bio:ohl_sauce_centrifuge))
+ A [microplastic fishing net](https://wiki.kaouenn-noz.fr/hors_les_murs:hack2eaux:pieges_a_microplastiques)

In the field of ecotoxicology and bioassays, the scientific quality standard is called ECOTOX, created by INRA in 2009 and regulated by ISO 6341 (2012). It involves juvenile daphnias placed in an environment where the presence of a chemical pollutant or a cocktail of agents is suspected or known. The parameter measured is the inhibition of swimming, movement and mobility for 24 to 48 hours. This is the horizon that could serve as a comparison for seed-based bioassays. 

For seed-based bioassay, another standard exists using Arabidopsis thaliana Col-0 ecotype as [reference Model Organisms](https://en.wikipedia.org/wiki/Model_organism) in biology. However, obtaining and verifying these seeds can be challenging for anyone outside of specialized laboratories due to their small size and difficulty in handling, making them a poor choice for observation using the naked eye or a "public" camera

To practice "verifications" on the results obtained with the seed-based bioassay prototypes, and also to practice ecotoxicology experiments in parallel with these, I use the open-source biological material from [Standford Free Genes](https://stanford.freegenes.org/) under an [open material transfer agreement](https://fr.wikipedia.org/wiki/Convention_ouverte_MTA) (OpenMTA). There is now a small network that allows obtaining these wetware for free or at the cost of a postal shipment.

![](/assets/images/ruisselement-chantier-rennes-automne-2022.JPG)
<figcaption class="caption">Description image: A photograph of the interior of a building under construction taken at street level. A grey PVC pipe is seen releasing water from the foundation of the building into the sewer grate on the sidewalk.</figcaption>
<br />

In order for a modern and effective bioassay system to be established, there are several prerequisites that must be met:

+ The system's parameters / endpoints must be very sensitive to the toxic substance being analyzed/targeted.
+ The response time must be "fast".
+ The biological test must be able to be used for both acute (short-term) and long-term measurements.
+ The device must be easy to use and not require extensive personnel training.
+ The cost should not be excessive, especially when intended to be used in developing countries.
+ The biological material should be easily accessible and the operating costs should be low.

## How much money, where to give and why - and what can you give if you have no money?

**3 800 €** to be received in donations before January 31, 2022, here is the objective! (and my safe-conduct to cross the precariousness and in a corner of France which wants to condition the access to the social minima [of which my file is in waiting of treatment] to 20 hours of work without the assets and without the rights of work).

For the year 2022 I received 3 902,19 € of donations via Liberapay. Mostly from June from people who helped me not to fall more precarious than I already was.

![](/assets/images/liberapay-2022.png)
<figcaption class="caption">The image is a screenshot from my Liberapay account. The background is white, and there are two charts displayed: one for "Income per week" (in euros) and one for "Number of donors per week". The charts are horizontal, displaying the chronological history of donations received, with vertical bars to illustrate the number of euros received per week and the number of donors.</figcaption>
<br />

It is possible to make a donation with a credit card or through bank transfer, the accepted currencies are: €; $; $AU; $CA; CHF; £.

I am therefore asking for [donations through Liberapay here](https://liberapay.com/Xav.CC/donate). I am not using any crowdfunding website because it would involve competing with other projects and I do not have the time or energy for that, I also don't want this kind of competition. Additionally, it would require you to provide personal information to these "platforms" and I don't want that.

**3 800 €** for:

+ **3 months of work** (February, March, April), which is 1 000 € per month
  + Correcting and improving existing documentation
    + Adding "How to choose seeds for biological tests and where to find them?"
    + Adding "How to choose equipment and materials"
  + Redoing series of bioassays with micro-dosages and different seeds
  + Comparing with other protocols and experiences and then evaluating different standards
  + Making comparisons between the "from your kitchen" mode and the "analysis laboratory" modus operandi on protocols and results
+ **3 public workshops** of 5 consecutive days each in places/communities in need (see the October 2022 [Louzaouiñ Graines de luttes 2022 project](https://wiki.kaouenn-noz.fr/hors_les_murs:reprises_des_savoirs:louzaouin_graines_de_luttes))
  + Using methods of [popular education](https://en.wikipedia.org/wiki/Popular_education)
  + With a half-day "Biopanic, kitchen, and feminism" workshop
+ Putting all this into documentation
+ **800 € of expenses**
  + 500 € of travel (South France, North Italy, Belgium)
  + 300 € of materials
    + Equipment for experiments with reference standards in classic laboratories
    + Various consumables (micro-tubes, distilled water, reagents, Whatman paper, etc.)

![](/assets/images/pocket-pcr.jpeg)
<figcaption class="caption">The image is a small machine, electronic board and circuit with a LED screen and a navigation button. On the left of this board: A kind of carousel (it doesn't move) allowing to place 0.2 ml micro-tubes and to heat/cool them by cycles, thermocycling, allowing among other things to make polymerase chain reactions (PCR). This is the Pocket PCR from Gaudi Lab under Open Hardware and free software license that I regularly use.</figcaption>
<br />

**If you don't have any money, you can also contribute with:**

+ Your social capital: by mobilizing your networks and connections and spreading the word about this donation appeal with a personal message from you
+ Your time by talking to me
+ Your open-source software skills if you have some illustration/graphics/drawing knowledge
+ Your equipment/materials that you no longer need and could help in this endeavor.

![](/assets/images/bioessai-serie-janv-2023.jpg)
<figcaption class="caption">A workbench with 5 transparent plastic cassettes, each containing 6 cuvettes inside of which there are 3 radish seeds per cuvette. Each cassette is dedicated to the germination of the seeds in a specific concentration within a liquid medium: 1. Control 100% "sterilized" water; 2. 75% "sterilized" water and 25% sample water; 3. 50% "sterilized" water and 50% sample water; 4. 25% and 75%; 5. 100% sample water.</figcaption>
<br />

## What happens if I don't receive €3,800?

Donations and counter-donations, I am [obligated to you]((https://journals.openedition.org/sdt/29087)) by the "promise, a promise that requires trust from all towards all and feeds on it."

Whatever happens with my appeal here, the environmental investigation work will continue and the prototyping as well. I do not know at what pace or in what time frame. Furthermore, I have already started interviewing water hacktivists in Pakistan and Hawaii for the beginning of the year to spread their actions and perhaps provide some support. I have begun three series of bioassays this week, including one using water from a parking lot, a second using micro-doses of less than or equal to 0.1 ml of gasoline in distilled water, and a third using micro-doses of less than or equal to 0.01 ml. 

I plan to write down the details of this *quest* and any donations on my blog during the first weekend of February.

![](/assets/images/micro-dosage-2023.jpg)
<figcaption class="caption">Image description: A workbench with 5 transparent plastic trays each containing 6 wells, in which 3 radish seeds are placed in each well. A micropipette is placed on the workbench in the middle of the 5 trays. Each tray here is dedicated to germinating seeds in a specific concentration within a liquid medium with a micro-dosage of less than or equal to 0.1 ml.</figcaption>

## Some previous milestones for this little story
    
+ 2002 to 2012, [Specialized Operational Section in depollution]((https://defense-zone.com/blogs/news/bataillon-marins-pompiers-marseille)), BMPM, Marseille
+ [Ecotoxicology, in 2015, on the Nantes to Brest canal sharing reach in Glomel (22), France]((https://pixelfed.social/p/XavCC/114362136498671616))
+ "[Biopanic, kitchen and feminism](https://wiki.kaouenn-noz.fr/ateliers:biopanique_cuisine_feminisme)" workshops since 2016
+ [IndieCamps]((https://movilab.org/wiki/IndieCamp#IndieCamp)) (2016 to 2022)
+ I have taught Courses in Environmental engineering schools(2017 to 2020), and UT Compiègne
+ [Purple Rivers Action]((https://notesondesign.org/xavier-coadic/)) (2019)
+ [Frugal Science]((https://www.frugalscience.org/)) (2020) from the Prakash Lab, Standford
+ Tactical Tech and Exposing The Invisible (2019 to 2022)
+ Brussels and Liege, Belgium (2022)
+ Chantier Reprises des savoirs, Pluri-versités, Rennes, France, October 2022
+ [Hack2o](https://ps.zoethical.org/t/hack2o-intro-explication/6116) (2023)

![](/assets/images/liege-biohack-2022.jpg)
<figcaption class="caption">Description of image: Scene of a biohacking workshop in an "improvised" location in Belgium, DNA extraction and initiation to basic microbiology and environmental investigation techniques. Two people are leaning over a folded cardboard base on which two test tubes are being manipulated by hand and with a micro-pipette of previously extracted DNA from seeds. Liege, Belgium, 2022, during the temporary and contracted occupation of the former Ougrée town hall on the banks of the Meuse River at the foot of the abandoned Arcelor Mital blast furnaces.</figcaption>

<div class="breaker"></div>
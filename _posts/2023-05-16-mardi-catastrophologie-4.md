---
title: "Mardi catastrophologie : votre fournée de ressources (n°4)"
layout: post
date: 2023-05-16 05:40
image: /assets/images/DSC04984-depot-petrolier-guilvinec-2023_01.jpg
headerImage: true
tag:
- hsociety
- Catastrophologie
category: blog
author: XavierCoadic
description: Un mardi matin sur la Terre
---

Les numéros précédents : [[1](//mardi-catastrophologie/)], [[2](/mardi-catastrophologie-2/)], [[3](/mardi-catastrophologie-3)].

> « […] ce n'est pas à nous qu'est accordée la liberté, mais aux mécanismes de la catastrophe » <cite>Günther Anders</cite>, *Le rêves des machines*, 1960, ed. Allia.

Le groupe Zotero [Catastrophologie](https://www.zotero.org/groups/4989006/catastrophologie/library) est garni de 98 items.

![](/assets/images/DSC04984-depot-petrolier-guilvinec-2023.jpg)
<figcaption class="caption">Dépôt pétrolier. Danger. I Love Port du Guilvinec, Finistère, quai D'Estienne d'Orves. Mai 2023. Description de l'image : Photographie en zone urbaine. Un mur blanc avec des graffitis, au centre un portail en bois peint en bleu sur lequel est fixé un panneau blanc sur lequel est écrit « Dépôt pétrolier. Défense d'entrer. Interdit de fumer ». À droite du portail, sur le mur blanc, une inscription en rouge : « DANGER » au dessous de laquelle est écrit à la bombe de peinture aérosol, tag graffité en vert, « I Love ».</figcaption>
<br />

+ [Publi] **Le concept de crise : un paradigme explicatif obsolète ? Une approche sexospécifique**, <cite>Natacha Ordioni</cite>, 2011 <https://www.cairn.info/revue-mondes-en-developpement-2011-2-page-137.htm>

> « La fréquente référence au concept de crise ne se limite pas au champ médiatique : les institutions internationales ont également mis l’accent sur la nécessité d’analyser l’impact des "crises mondiales combinées (énergétique, alimentaire, économique, financière…)" (Unesco, 2010) sur les populations des pays en développement (PED), une attention particulière se voyant accordée aux répercussions de "la crise" sur la situation des femmes, plus vulnérables à l’égard du chômage et de la pauvreté. »

+ [Livre] (pdf) **L'anthropologie**, Par <cite>Marc Augé</cite>, <cite>Jean-Paul Colleyn</cite>, 2021, Presses Universitaires de France. <https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=12244405-1.pdf>

+ [Livre] **Manuel d'ethnographie**, <cite>Marcel Mauss</cite>, 1926, <https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=manuel_ethnographie.pdf>

+ [Actu] **Le nombre de personnes déplacées par les conflits et catastrophes naturelles atteint un nouveau record** <https://legrandcontinent.eu/fr/2023/05/12/le-nombre-de-personnes-deplacees-par-les-conflits-et-catastrophes-naturelles-atteint-un-nouveau-record/>

> « Selon un récent rapport, 71,1 millions de personnes vivaient en situation de déplacement interne dans le monde à la fin de l'année 2022, soit une augmentation de 20 % en un an et le chiffre le plus élevé jamais enregistré. Les conditions climatiques extrêmes et l’apparition de nouveaux conflits, dont la guerre en Ukraine, ont largement contribué à cette augmentation. »

+ [Actu (merci Khrys)] **Sécheresse des sols : un quart de l’Europe manque d’eau en avril, contre la moitié l’an dernier** <https://www.liberation.fr/environnement/climat/secheresse-des-sols-un-quart-de-leurope-manque-deau-en-avril-contre-la-moitie-lan-dernier-20230513_UA5UPPE26BD47M67YLRSUAD634/>

> « Excepté en Espagne, les sols du continent européen sont globalement dans une meilleure situation hydrique qu’en 2022, selon des données des satellites Copernicus analysées vendredi 12 mai. Mais les réserves d’eau restent à des niveaux alarmants. »

+ [Actu] **New temperature records, food security threats likely as El Niño looms**

> "The development of an El Niño climate pattern in the Pacific Ocean this year is more and more likely, with dangerously high temperatures and extreme weather events expected, the World Meteorological Organization (WMO) said on Wednesday." <https://news.un.org/en/story/2023/05/1136312>

+ [Actu] **Record ocean temperatures put Earth in ‘uncharted territory’, say scientists**. ‘Unprecedented’ warming indicates climate crisis is taking place before our eyes, experts say. <https://www.theguardian.com/environment/2023/apr/26/accelerating-ocean-warming-earth-temperatures-climate-crisis>

![](/assets/images/ocean-surface-temp-april-26th-2023.png)
<figcaption class="caption">Guardian graphic. Source: Noaa, Maine Climate Office, Climate Change Institute, University of Maine. Note: data covers oceans from 60 degrees north to 60 degrees south of the equator</figcaption>
<br />

+ [Actu (thanks to Snoro)] **Cyclone Mocha is now the second-most intense May cyclone in Bay of Bengal**, Mocha may intensify into a super cyclone before landfall 

> "Cyclone Mocha on May 13, 2023, became the second-most intense cyclone to develop in the Bay of Bengal in the month since 1982, a scientist told Down To Earth (DTE)" <https://www.downtoearth.org.in/news/natural-disasters/cyclone-mocha-is-now-the-second-most-intense-may-cyclone-in-bay-of-bengal-89314>

+ **"A humanitarian crisis looms in Myanmar as Mocha makes #landfall as a category 4 storm with 155 mph winds, after peaking at category 5 strength with 175 mph winds."** (But the cyclone did not make landfall at Cox' Bazaar, the refugee camp, as initially feared.) <https://yaleclimateconnections.org/2023/05/cyclone-mocha-north-indian-oceans-strongest-storm-on-record-hits-myanmar/>

+ [Actu] **EFFIS Prévision de danger d'incendie pour le 11 mai [2023]** <https://effis.jrc.ec.europa.eu/apps/effis.longterm.forecasts/>
 + Risques très extrême :
   + Zaragoza et dans des zones limitées de Catalunya, Espagne
 + Risque extrême dans de larges zones :
   + Espagne : Castilla La Mancha, Andalucia, Canarias & Extremadura 
   + Portugal : Algarve & Alentejo
 + Les anomalies de températures (pas le risque incendie) sont prévues pour très fortes à extrême pour la Scandinavie et l'Islande.
 + La chaleur anormale (record) se poursuit au cours de la semaine au Canada, même jusqu'au cercle arctique...

![](/assets/images/media_Fv1chtqXgAAkZGD.jpg)
![](/assets/images/Europe_MonthlyAnomalies_T2m_20230508_w1.png)
<br />

+ [Actu] **The far north is burning—and turning up the heat on the planet** The Arctic and surroundings are being transformed from carbon sink to carbon emitter. <https://arstechnica.com/science/2023/05/the-far-north-is-burning-and-turning-up-the-heat-on-the-planet/>

+ [Ref] **The extreme wildfires in Siberia, California and Australia highlighted limitations of and hindrances to human capacity for rescue, survival and adaptation to different scales of exposure to technogenic catastrophes and the effects of climate change.** <cite>Olga Ulturgasheva</cite> <https://research.manchester.ac.uk/en/persons/olga.ulturgasheva>

> "The questions of how we understand human limitations to avert risks and hazards, how we take into account the increasing vulnerability of the affected communities and how we comprehend human and non-human feedback to new and emerging forms of ‘normal’ stress importance of the need to recognize urgency of acknowledging that expert knowledge emerges in many forms and that it can be communicated in many ways. The understanding of complex networks of risks and strategies undertaken for risk reduction and amelioration will depend on how they are interpreted and articulated, and by whom. Olga Ulturgasheva will examine technologies and practices of reading environmental uncertainty in Northeast Siberia while evaluating human capacity and its limits to avert and mitigate the impact of wildfires."

+ [Atcu] **Ongoing costs from Elephant Hill wildfire pegged at $1B a year: report** <https://globalnews.ca/news/9689139/elephant-hill-wildfire-environmental-costs/>

+ [Publi (Thanks Kai Kupferschmidt )] **Malawi is experiencing the deadliest cholera outbreak in its history with more than 36.000 cases and 1210 deaths**.

> "Currently, the large geographic spread and the high number of reported cases in the country are stretching all capacity to respond to the outbreak, increasing the risk of serious public health impact. […]
>
> "With a sharp increase of cases seen over the last month, fears are that the outbreak will continue to worsen without strong interventions." <https://www.who.int/emergencies/disease-outbreak-news/item/2022-DON435>
>
> "Tropical storm Ana (Jan 22) and Cyclone Gombe (Mar 22) caused flooding, displaced people and led to the outbreak. "This outbreak, which was initially limited to the Southern Region and the flood-affected areas, has now spread to all the regions and districts in the country"
>
> This is part of a surge in cholera cases worldwide that has led to a shortage in vaccines and to health organisations rationing doses." <https://www.science.org/content/article/vaccines-are-short-supply-amid-global-cholera-surge>

+ [Actu] **Sécheresse : le manque d’eau menace le canal de Panamá et le trafic maritime mondial**

> « Les plus gros navires ne peuvent plus passer, car à chaque passage, ce sont environ 200 millions de litres d’eau douce qui sont déversés dans la mer.
>
> Les deux lacs artificiels qui fournissent le canal en eau sont à sec, obligeant les autorités panaméennes à limiter l’accès à la voie reliant les océans Atlantique et Pacifique, où transite 6% du trafic maritime mondial. » <https://www.liberation.fr/environnement/climat/secheresse-le-manque-deau-menace-le-canal-de-panama-et-le-trafic-maritime-mondial-20230426_OHQ45TGK3BDL3GTEJGCM6HSYHE/>

+ [Actu] **Canada : plusieurs incendies encore incontrôlables** <https://fr.euronews.com/video/2023/05/10/canada-plusieurs-incendies-encore-incontrolables>

> « Le nombre d'incendies de forêt qui ont forcé 30 000 personnes à fuir au cours des quatre derniers jours est passé de 110 à 81, 24 d'entre eux étant toujours hors de contrôle.
>
> Les autorités ont toutefois prévenu qu'un retour à des conditions chaudes et sèches était attendu d'ici vendredi et qu'il se poursuivrait tout au long du week-end.
>
> Danielle Smith, Première ministre de l'Alberta, a fait remarquer que 30 000 hectares (74 000 acres) de la province sont habituellement consumés par des incendies de forêt chaque année. "390 000 hectares ont déjà brûlé, soit dix fois plus que l'année habituelle »

+ [Publi / Chronique] **La dimension environnementale des inégalités sociales**, Catherine Larrère (dir.), Les Inégalités environnementales, Paris, Presses universitaires de France (« La Vie des idées »), 2017, 104 p. par <cite>Élias Burgel</cite> <https://journals.openedition.org/etudesrurales/11797>

> « L'ouvrage part ainsi du constat, établi par Catherine Larrère, selon lequel les pensées de l’environnement sont souvent tentées de mettre l’« accent sur les effets égalitaires des dégradations de l’environnement », en postulant que la finitude écologique terrestre engendre mécaniquement une « communauté de destin », nécessitant une prise de conscience universelle (on pense, par exemple, à la question du réchauffement climatique). L’ “absence de sensibilité environnementale des études socio-économiques » posséderait ainsi sa réciproque, à savoir la « faible sensibilité sociale des études écologiques” »

+ [Publi] **Gouverner les ressources de la mer. Une histoire environnementale de l’inspection des pêches françaises au XVIIIe siècle**, <cite>Romain Grancher</cite> <https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=1054179ar.pdf>

+ [Actu] **À Volvic, Danone pompe malgré la pénurie d’eau.**

> « Dans le Puy-de-Dôme, 44 communes du Puy-de-Dôme sont visées par un arrêté préfectoral de restriction sur l’eau potable "pour éviter les risques de pénuries". Les entreprises du territoire sont également tenues de diminuer d’un quart leurs prélèvements sur le réseau. Des restrictions auxquelles échappe la société des eaux de Volvic, propriété de Danone. » <https://reporterre.net/A-Volvic-Danone-pompe-malgre-la-penurie-d-eau>

+ [Publi] **The projected timing of abrupt ecological disruption from climate change**, Nature, <cite>Christopher H. Trisos</cite>, <cite>Cory Merow</cite> & <cite>Alex L. Pigo</cite>, 2020, <https://www.nature.com/articles/s41586-020-2189-9#data-availability>

![](/assets/images/e16a63ddb3acca97.png)
<br />

+ [Actu] **Au Brésil, les géants du sucre responsables d’une pluie toxique**

> « Les multinationales du sucre, parmi lesquelles le groupe coopératif français Tereos, organisent l’épandage aérien de pesticides hautement toxiques interdits par l’Union européenne sur leurs champs de canne à sucre, selon des documents obtenus par Mediapart en collaboration avec le média à but non lucratif Lighthouse Reports, le « Guardian », « Die Zeit », et Repórter Brasil. » <https://www.mediapart.fr/journal/international/250423/au-bresil-les-geants-du-sucre-responsables-d-une-pluie-toxique>

+ [Podcast] **L’Actu des Oublié⋅es • Saison III • Episode 15 • La guerre des graines I Afrique de l’Ouest**

> L’industrie semencière tente de profiter à la fois de la guerre en Ukraine et de la crise climatique pour accélérer l’offensive qu’elle mène sur le continent africain. Au cours des dernières décennies, elle s’est aménagée un terrain idéal pour s’implanter,
>
> Face à elle, les organisations paysannes s’organisent pour conserver ou réintroduire les systèmes semenciers communautaires. Tant à l’échelle locale en organisant formations ou foire d’échanges de graines, qu’à l’échelle globale pour tenter d’obtenir des législations favorables aux semences paysannes, il se joue là une bataille majeure pour la souveraineté alimentaire à l’échelle planétaire. Une lutte qui n’aboutira qu’en dépassant les frontières.
>
> <https://audioblog.arteradio.com/blog/157476/podcast/203180/saison-iii-episode-15-la-guerre-des-graines-i-afrique-de-l-ouest>

+ [Enquête] **La faim, peine sans fin des prisonniers à Madagascar** Plongée dans les prisons de l'île

> « Dans l'un des pays où l'on condamne encore à des travaux forcés, la situation des personnes détenues peine à émouvoir une population durement affectée par l'envolée du prix des produits de première nécessité et à mobiliser une classe politique entièrement tournée vers l'échéance présidentielle à venir. « L'enfer carcéral » malgache ne cesse pourtant de s'aggraver. Et la faim, une des souffrances les plus insidieuses infligées aux prisonniers, de s'amplifier. » <https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=la_faim_peine_sans_fin_des_prisonniers_a_madagascar_reflets.info.pdf>

+ [Actu (merci Khrys)] **Bébés de 7 et 8 mois placés en centre de rétention : la France condamnée par la CEDH**

> « La Cour européenne des droits de l’homme (CEDH) a de nouveau mis à l’index la France, jeudi 4 mai, dans deux affaires distinctes de placements en centre de rétention de mineurs étrangers, dont certains très jeunes, âgés de 7 et 8 mois.
> [...]
> Ces nouvelles décisions portent à onze le nombre de condamnations de la France par la CEDH sur ce sujet » <https://www.lemonde.fr/societe/article/2023/05/04/bebes-de-7-et-8-mois-places-en-centre-de-retention-la-france-condamnee-par-la-cedh_6172125_3224.html>

+ [Actu] **Opération Wuambushu à Mayotte et conséquences**

> « A Mayotte le bureau des admissions de l’hôpital est fermé, tout comme le dispensaire situé juste en face sous la pression des collectifs pro-Wuambushu qui fustigent l’accès aux soins des sans-papiers. Impossible donc d’honorer un rendez-vous médical ou de déclarer une naissance » <cite>Louis Witter</cite>, 5 mai 2023 sur Twitter

+ [Actu] **Réfugiés: cible de l’extrême droite, le maire de Saint-Brévin démissionne**

> Incendie de sa maison, « manque de soutien de l’État»: le #maire de Saint-Brevin-les-Pins, attaqué par l’Extrême-Droite en raison d’un projet d’extension d’un centre d’accueil de demandeurs d’asile dans sa ville, a jeté l’éponge mercredi. » <https://www.mediapart.fr/journal/france/110523/refugies-cible-de-l-extreme-droite-le-maire-de-saint-brevin-les-pins-demissionne>

+ [Podcast] **Sarah Nuttall about hydropolitics in the Global South, about entanglement, multi-spirited waters, and the elemental**. Sarah Nuttall is a South African scholar and the director of the Wits Institute for Social and Economic Research at the University of the Witwatersrand, Johannesburg. Over the past 10 years, her work has focused on post-colonial criticism, urban theory and literary and cultural studies, especially in relation to Africa and its diasporas.

> "We go through droughts, rainfall, mud, drift and unexpectedly end with fire! We also go into extrapolative science fiction as prefigurative social theory, finding the right language and asking the right questions. » <cite>Radio Web MACBA</cite> <https://rwm.macba.cat/en/sonia/sonia-372-sarah-nuttall>

+ [Actu (thanks to RadicalAnthro)] **Leader of Kenyan waste pickers: ‘We are the backbone of recycling’** <https://www.theguardian.com/environment/2023/may/12/leader-kenya-waste-pickers-we-are-backbone-of-recycling-plastic-pollution>

> « Aujourd'hui, dans l'industrie du plastique qui pèse plusieurs millions de livres, ce sont les personnes qui vivent et travaillent sur les montagnes d'ordures en Afrique et dans le monde entier qui constituent l'épine dorsale invisible du recyclage du plastique et permettent aux multinationales d'atteindre leurs objectifs en matière de réduction de son utilisation.
> […]
> Chweya, qui dirige aujourd'hui l'association des ramasseurs de déchets du Kenya, qui représente 36 000 ramasseurs, a joué un rôle déterminant pour inciter les pays à reconnaître les 20 millions de ramasseurs de déchets du monde dans le traité. »

+ [Publi] **Terrains océaniens : enjeux et méthodes.** Actes du 24e Colloque CORAIL, 2012. Sous la direction de <cite>Véronique Fillol</cite> et <cite>Pierre-Yves Le Meur</cite> <https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=ethno-terrain-oceanien-010064301.pdf>

+ [Publi] **<cite>Michel Naepels</cite>. Dans la détresse. Une anthropologie de la vulnérabilité**, Compte rendu, critique par <cite>Sandrine Revet</cite> <https://www.cairn.info/revue-critique-internationale-2021-2-page-183.htm>

+ [Actu] **The Palestinian “Nakba” (“catastrophe” in Arabic) refers to the mass expulsion of Palestinian Arabs from British Mandate Palestine during Israel’s creation (1947-49).**, "Nakba Day", May 15, <https://fr.wikipedia.org/wiki/Nakba>

+ [Livre (thansk to <cite>Vjosa Musliu</cite>)] **Migrant Academics’ narratives of Precarity and Resilience in Europe**, EditEd By
<cite>Olga Burlyuk</cite> and <cite>Ladan Rahbari</cite> <https://books.openbookpublishers.com/10.11647/obp.0331.pdf>

+ [Actu] **ils s’appelaient Foad, Bin, Sara, Gebrel, Mawda ou Omar. Ils venaient de Chine, d’Afghanistan, de Somalie…**

> « Des hommes, des femmes, des enfants. Tous sont morts entre la zone frontière franco-belge et le Royaume-Uni, autour de Calais, depuis 1999. 367 morts, une litanie sourde et sans fin. Comme si un tueur sévissait depuis 25 ans, sans que les institutions policière et judiciaire ne s’émeuvent.
>
> “Les Jours” remontent le fil d’un carnage silencieux et politique. <https://lesjours.fr/obsessions/calais-migrants-morts/ep1-memorial/>

<div class="breaker"></div>
---
title: "Liste de configurations expérimentées et documentées sous licence libre pour une conception de la contribution publique et citoyenne"
layout: post
date: 2018-09-26 15:00
image: /assets/images/participation.jpg
headerImage: false
tag:
- Communs
- Rennes
- hsociety
- Gouvernance
- Tiers-Lieux
category: blog
author: XavierCoadic
description: Servez-vous ! 
---

Liste publiée de ressources opérationnelles (ou recette si vous préférez), contenant des retours d'expériences, suite à un entretien avec Damien Bongart, Vice Président Conseil Départemental 35, chargé du numérique et très haut-débit, Conseiller municipal Rennes,  Dominique Kergosien, déléguée aux projets de transformation numérique au département d'Ille-et-Vilaine, et Delphine Tanguy, chargée de mission tourisme et numérique. Cette liste étant destinée à évoluer et être éditée dans le temps, il est conseillé de s'abonner par [flux RSS](https://framanews.org/) pour un suivi optimisé.

Cette entrevue était motivée par des échanges autour de la consultation nationale portant sur le numérique inclusif à laquelle j'ai contribué ( [[1]](https://xavcc.github.io/mission-inclusion-numerique-1), [[2]](https://xavcc.github.io/mission-numerique-2) ), nous amenant à évoquer plusieurs enjeux de société ainsi que différentes configurations et expériences en cours ou passées qui pourraient être utiles à une forme de « design des politiques publiques » avec une volonté d'inclusion (ce qui est conditionné par le sentiment d'appartenance à une communauté) et des possibles engagements dans des Communs. J'ai voulu dans ce billet blog amorcer un partage plus large et le plus diffusable possible de ressources qui, de mon expérience, peuvent amener des élèments d'actions vers une conception de la contribution publique et citoyenne.

Il ne s'agit pas d'écrire une énième liste de fiches de lecture non éprouvées, ni d'éditer un annuaire, mais de fournir les moyens par des expériences documentées de refaire par vous-même et/ou de rentrer en relation pour apprendre en faisant au contact de personnes ayant conçu et/ou réalisé ses expériences. Si des concepts ou des dénominations vous semblent éloignés de ce que espérez mettre en œuvre dans vos organisations soyez assuré que chaque communauté de pratiques possède un langage spécifique mais que les modes d'actions, les configurations, les processus, peuvent être réutilisés, transposés ou adaptés, surtout lorsque ceux-ci sont documentés sous licence libre. 

Je me tiens à la disposition des personnes parcourant ces lignes pour de plus amples explications ainsi que pour des mises en liens ou des de tests in situ. 

> «_Les communs comme espace politique informent la notion d’intelligence comme un « savoir cohabiter »_ »

La table des contenus est présentée selon l’ordre suivant : 
- Des exemples d'actions larges, d'envergures citoyennes et de longue durée, assurant une contribution permanente et pérenne ; 
- Des exemples de mise en place de communautés de pratiques, permettant de faciliter des actions ; 
- Des exemples d'actions collectives au format court, pouvant aussi s'intégrer de façon répétée dans la configuration sociale proposée en premier point, ou en complément d'autres configurations ; 
- Des exemples de réalisations, ayant facilité la transformation d'organisations qui se sentaient non légitimes ou éloignées des approches proposées dans les 3 points précédents.

Ces différentes ressources n'ont aucunement la prétention d'être exhaustives, elles constituent une humble partie parmi un tout plus grand ; elles sont le fait et le travail de personnes consciencieuses. 

> « _Or dans les collectifs de travail (que ce soit en entreprise ou en administration), on peut souvent constater que c’est la capacité à s’auto-organiser qui fait justement défaut, à cause du “carcan managérial” auquel les individus sont soumis. Ainsi se trouve vérifiée ce qui constitue une des grandes leçons que nous enseignent les Communs : ce qui compte d’abord n’est pas la ressource, mais bien la communauté et sa capacité à se gouverner elle-même._ » [Lionel Morel](https://scinfolex.com/2018/09/25/ce-que-lon-apprend-sur-les-communs-dans-les-frigos-collectifs) 25 septembre 2018

### Table des contenus (au 26 septembre 2018)

+ [Configuration sociale pour la contribution et la concertation](#configuration-sociale-pour-la-contribution-et-la-concertation)
   + [3DD un espace-temps Agora à Génève](#3dd-un-espace-temps-Agora-a-geneve)
   + [Planification de transition de territoire par les communs](#planification-de-transition-de-territoire-par-les-communs)
   + [Construire des communs et Gouvernance](#construire-des-communs-et-gouvernance)
   + [Forum Ouvert](#forum-ouvert)
   + [Faire Tiers-Lieux en Bretagne](#faire-tiers-lieux-en-bretagne)
   + [Design moi 1 Tiers Lieux](#design-moi-1-tiers-lieux)!
   + [Cercle excentrique](#cercle-excentrique)
   + [Communs territoriaux](#communs-territoriaux)
+ [Design Pattern de communautés](#design-pattern-de-communautes)
   + [Tiers-Lieux Hackerspace Design Patterns](#tiers-lieux-hackerspace-design-patterns)
   + [Exemple de Code de Conduite](#exemple-de-code-de-conduite)
   + [Rencontre régionale](#rencontre-regionale)
 + [Format de sprint de création ou amélioration](format-de-sprint-de-creation-ou-amelioration)
   + [MediaLab](#medialab)
   + [Hackathons](#hackathons)
   + [Remix](#remix)
 + [Bouger des lignes](#bouger-des-lignes)
   + [4 pratiques pour faciliter la collaboration dans un environnement institutionnel](#4-pratiques-pour-faciliter-la-collaboration-dans-un-environnement-institutionnel)
   

>  « _Ni privés ni publics, les communs offrent une réponse à des problèmes de dépossession et d’exclusion. Ils remettent en question le paradigme de la propriété individuelle exclusive. Privilégiant l’usage des ressources sur leur détention, ils développent des processus collectifs (commoning) dont une communauté se dote pour gérer des ressources sur lesquelles elle revendique des droits. Les communs favorisent la création de richesse par la mise en commun de ressources intellectuelles, sociales, matérielles et environnementales_ ». [« Le Devoir » de Laure Waridel, Chercheuse associée et professeure associée à l’UQAM](https://www.ledevoir.com/opinion/idees/537180/la-pertinence-des-communs)

## Configuration sociale pour la contribution et la concertation

Pourquoi et comment mettre en œuvre des dispositifs favorables aux actions plus transversales entre différents corps de métiers et différentes organisations de la société, ceci par des expériences documentées sous licence libre.
Pour concevoir une configuration de communautés aux visions partagées tout en conservant la diversité avec des responsabilités partagées.

### 3DD un espace-temps Agora à Génève

Ouvert en septembre 2017 - au rez-de-chaussée de l’immeuble du 3 rue David Dufour 1205 Genève dans un ancien espace d’accueil (l’entrée du service de la police des étrangers) qui a longtemps été laissé à l’abandon, le 3dd espace de concertation a l'ambition de générer des situations inédites de rencontres créatives entre des publics très hétérogènes et parfois antagonistes afin de faire émerger un nouveau collectif capable d’apporter des réponses nouvelles, responsables et communes aux multiples défis qui attendent les citoyens et les organisations de Genève et sa région

+ [3DD](https://wiki.3ddge.ch/index.php?title=Accueil), Qu'est-ce que le 3DD ?

> « _Quelle Genève voulons-nous demain ? Quelle qualité de vie dans nos futurs quartiers ? Quelles places pour nous, citoyen.ne.s? Autant de question que ce laboratoire des possibles propose d'explorer. Le 3DD espace de concertation est un lieu dédié à la ville de demain et aux démarches participatives. Il est conçu comme le point de rencontre de différents publics et de différents regards, propice à l’émergence de nouvelles idées. En mettant à disposition cet espace, le Département du territoire rend possible la contribution de toutes celles et ceux qui souhaitent participer au développement qualitatif de notre territoire._»

**Contact** : [Antoine Burret](http://movilab.org/index.php?title=Utilisateur:Antoine)

### Planification de transition de terrritoire par les communs

Une consultation pour transformer le terrain d'action ou le territoire d'une entité qui peut se dérouler sous la forme de :

+ Une cartographie conséqunete de projets communs par secteur d'activité (alimentation, logement, transport, etc.), à travers un wiki, exemple disponible sur <http://wiki.commons.gent>
+ Un nombre conséquent d'entretiens et conversations en tête-à-tête avec d'éminents roturiers et chefs de projet
+ Un questionnaire écrit auquel plus de 70 participants ont répondu
+ Une série d'ateliers au cours desquels les participants ont été invités par thème : "Nuémrique en commun" , " L'alimentation comme bien commun ", " L'énergie comme bien commun ", " Les transports comme bien commun ", etc.
+ Un atelier Commons Finance Canvas, basé sur la méthodologie développée par Stephen Hinton, qui s'est penché sur les opportunités économiques, les difficultés et les modèles utilisés par les projets Commons.

Un rapport sous licence libre qui peut comprendre quatre parties.

La **première partie** présente le contexte de l'émergence des biens communs urbains, qui a décuplé sur le territoire au cours des dix dernières années. Il se concentre sur le défi qu'il représente pour la ville et les pouvoirs publics, pour les acteurs du marché et pour les organisations traditionnelles de la société civile, et sur la manière dont la nouvelle logique contributive des biens communs remet en cause (mais aussi enrichit) la logique de représentation des politiques démocratiques européennes, dans ce cas particulier, au niveau d'une ville. Il examine également les opportunités inhérentes aux nouveaux modèles, telles qu'une participation plus active des habitants à la co-construction de leurs villes, à la résolution des défis écologiques et climatiques, et à la création de nouvelles formes de travail significatives au niveau local.

La **deuxième partie** donne un aperçu de l'évolution des biens communs urbains dans le monde, mais surtout dans les villes européennes, et examine de plus près les expériences de Bologne (avec le Règlement de Bologne pour le soin et la régénération des biens communs urbains, maintenant adopté par de nombreuses autres villes italiennes), de Barcelone (les politiques pro-communes de la nouvelle coalition politique En Comu), Frome, Royaume-Uni (pour sa coalition civile qui remplace les partis politiques dans la gestion des villes) et Lille, pour son expérience avec une Assemblée des biens communs comme voix et expression des bien commun locaux.

La **troisième partie** est l'analyse des biens communs urbains du territoire même, en soulignant certaines de ses forces et faiblesses.

Enfin, dans **la quatrième partie**, sur la base de l'analyse produite des trois premières parties, présentation de recommandations au territoire, en termes d'adaptation institutionnelle du territoire  aux nouvelles demandes centrées sur les communs qui émergent à travers les activités communes. Il s'agit d'un ensemble de propositions pragmatiques intégrées pour la création de processus publics communs de co-création à l'échelle du territoire d'action. D'une certaine manière, elle représente le passage d'une vision urbaine commune à une vision plus ambitieuse de la " ville en tant que bien commun ".

[Exemple de Gand en Belgique](https://commonstransition.org/commons-transition-plan-city-ghent)

**Contact** : Maïa Dereva, Michel Bauwens

### Construire des communs et Gouvernance

+ [Quelle gouvernance pour permettre à tous et chacun de s'approprier le commun ; sans pour autant réduire l'initiative individuelle ?](http://unisson.lescommuns.org/gouvernance)
L'initiative progresse par décisions horizontales collectives. L'organisation est transparente. L'ensemble des informations du projet (économique, humaine, etc...) est publique. Il y a des informations en ligne, claires et complètes, sur le financement du projet et son état. Les discussions, en particulier stratégiques, se font sur des espaces "ouverts" où chacun peut participer, et elles sont archivées. Il y a affichage des membres et contacts possibles, ainsi que des procédures internes. Des espaces pour remonter et prendre en compte les tensions existent, et un mode de prise de décision a été choisi. Si nécessaire, un glossaire des mots-clés est rédigé, afin d'éviter les malentendus et favoriser une meilleure compréhension.

**Contact** : Simon Sarazin

### Forum Ouvert
Le Forum ouvert est conçu comme un processus permettant à des groupes de se réunir afin de créer une dynamique de réflexion profonde sur des questions simples ou complexes. Le Forum ouvert permet de faire travailler ensemble un grand nombre de personnes, d’une dizaine à plusieurs centaines, autour d’un thème commun tout en laissant une grande liberté aux participants. Cette méthodologie est basée sur l’auto-organisation, la créativité et la liberté d’expression, l’objectif est de créer un climat favorisant l’initiative et l’apprentissage.

+ [Avec multibao, la boite à outil des organisation collaborative](http://www.multibao.org/#cpcoop/animer_ateliers/blob/master/echanger/forum_ouvert.md)

**Contact** : [Thomas Wollf](http://lesvigies.fr/), Stéphane Langlois, [Romain Lalande](https://osons.cc/?PagePrincipale), [Lilian Ricaud](http://www.lilianricaud.com/), Xavier Coadic

### Faire Tiers-Lieux en Bretagne

Recettes, retour d'expriences, documentations, de processus de repolitisation par le service.

+ [Le portail des exemples documentés et retour d'expérience](http://movilab.org/index.php?title=MoviLab_Bretagne)

**Contact** : Emmanuel Poisson Quinton, [Jaxom](http://movilab.org/index.php?title=Utilisateur:Jaxom), La fabrique du Loch à Auray, Xavier Coadic.

### Design moi 1 Tiers Lieux 

Comment, avec l'aide du design, engager des communautés dans une configuration permettant la recherche action transversale.

+ [Portail D1mTL](http://movilab.org/index.php?title=Portail:Dm1TL)

La constitution d'un patrimoine informationnel et matériel commun servant de matière 1ère pour la production (locale) de nouveaux événements, expositions, services ou produits

**Contact** : [Sylvia Fredriksson](http://www.sylviafredriksson.net), Yoann Duriaux

### Cercle excentrique

La démarche du cercle excentrique, aussi appelé "Fish Bowl" (bocal à poisson) est une manière d'impliquer un plus grand groupe dans la discussion d'un plus petit groupe (5 – 8 personnes); son principe de base est qu'un participant doit écouter et attendre avant de s'exprimer.

+ [Animer un atelier Cercle excentrique](http://www.multibao.org/#cpcoop/animer_ateliers/blob/master/echanger/cercle_excentrique.md)

### Communs territoriaux

+ [Concevoir, réaliser, animer et documenter un temps de médiation et de contribution](https://framastory.org/story/xavcc/les-communs-territoriaux-1/preview) au sujet des communs territoriaux, ici notamment sur le focus de tiers-lieux à tiers-scientifiques.

**Contact**: Charlotte Rizzo, Rieul Techer, Xavier Caodic

## Design Pattern de communautés

Voici quelques exemples d'expériences menées dans un design par et pour les personnes qui font communauté(s).

Pourquoi et comment concevoir les conditions de vie et d'épanouissement de communauté(s) contributive(s).

### Tiers-Lieux Hackerspace Design Patterns

+ Si Vous étiez simplement curieux, ou si vous étiez intéressé à parler de ce qui a bien fonctionné (et de ce qui n’a pas si bien fonctionné) dans votre espace, alors continuons à discuter et à distiller de nouveaux motifs pour ajouter au [Hackerspace Design Patterns](https://xavcc.github.io/tilios-design/)

### Exemple de Code de Conduite 

Pourquoi et comment établir des rêgles de vivre et de faire ensemble.

+ [Traduction du texte “HOWTO design a code of conduct for your community”](https://xavcc.github.io/tilios/)

### Rencontre régionale

+ [Concevoir et réaliser un rencontre régionales multi-acteurs d'intérêts communs](http://wiki.fablab.fr/index.php/Fabrique_2017)
Démarche de préfiguration de l'événement portée par plusieurs individus, démarche de rencontres, liens, documentation et partage portée par Théo Vital, Matthieu Brient, Xavier Coadic 

## Format de sprint de création ou amélioration

La conception et la mise en œuvre de temps courts visant au prototypage ou à la transformation, ainsi que l'agrégation et l'engagement de personnes et communautés de pratiques, ne sont pas choses aisées. Ces problèmes constituent une opportunité de collaborer avec des individus expérimentés pour apprendre en faisant. 

Ces temps courts sont parfois recherchés par des collectivité ou organisations ou des personnes peu accoutumées à ces formats, voici quelques ressources par le vécu qui pourraient aider à s'approprier des moyens de répondre à des besoins exprimés.

### MediaLab

C’est un espace collectif et physique qui met en synergie plusieurs techniques du numérique (Atelier réseaux sociaux, espace web ressources, SMS/Tweet-WALL, Open plateau radio et vidéo, Reportage en tourné-monté, design/animation, Conférence de rédaction citoyenne). Il peut être permanent ou éphémère. Il est animé par une équipe pluridisciplinaire.

+ [Un MediaLab peut être](http://movilab.org/index.php?title=Medialab_NEC_2018) :

  + sédentaire ou itinérant
  + éphémère ou permanent
  + au sein ou par un tiers lieu.

> « _Interroger et travailler les enjeux de société », est la vocation des recherches menées par le MediaLab. Enjeux humain, écologiques, libertés individuelles et collectives, enjeux politiques, numériques, tiers-lieux..._ 

**Contact** : Aurélien Marty, Déborah Thébault, Rieul Techer, Charlotte Rizzo, Nicolas Loubet, Xavier Coadic

### Hackathons

+ [Nuits du code citoyens](http://movilab.org/index.php?title=Nuit_du_code_citoyen_Rennes_2018) et [Open Source Ciruclar Economy Days](https://lebiome.github.io/#LeBiome/Hackathons/tree/master/oscedays).

L'évènement rennais reprend la philosophie globale des "Nuits du Code Citoyen" : il s'agit de permettre à tous les citoyen-ne-s de s'impliquer dans des projets informatiques libres, qui répondent à un besoin public ou permettent de faciliter la vie du citoyen.

Le format "hackathon" implique de consacrer une partie de l'évènement à coder concrètement le logiciel, mais d'autres tâches sont prévues comme la documentation, la communication autour des projets, les retours d'expérience sur les usages des outils, etc..

+ [Recette frugale d'hackathon citoyen open source: en 32 jours et sans budget](http://movilab.org/index.php?title=Recette_frugale_d%27hackathon_citoyen_open_source:_en_32_jours_et_sans_budget)

Comment et pourquoi organiser sans risques financiers un évènement décloisonnant les pratiques et les cultures, c'est l'objectif de cette ébauche de documentation. Afin que toutes personnes ayant besoin d'une recette de base puisse faire sienne cette expérience ainsi que les savoir-faire déployés et acquis. 

**Contact** : [Jaxom](http://movilab.org/index.php?title=Utilisateur:Jaxom), Xavier Coadic

### Remix

Pour (re)faire autrement ce que vous avez déjà produit. 

+ [Biblio Remix](https://biblioremix.wordpress.com/le-projet/) 
est un dispositif librement copiable, remixable et adaptable. Si vous avez envie d’en organiser un dans votre bibliothèque ou d'autres expaces, n’hésitez plus : voici la documentation détaillée de l’organisation de l’évènement.

**Contact** : [Nicolas Vigneron](https://twitter.com/belett)

+ [mécano du remix](http://www.multibao.org/#hackmuseomix/organisation_communaute_museomix/blob/master/mecano-museomix.md) par Muséomix
Voici une proposition d organisation type avec les différentes pièces du mécano pour construire un remix.

**Contact** : Benoit Vallaury, Samuel Bausson

## Bouger des lignes

C'est souvent dans cette volonté que sont exprimées des difficultés, voir des « murs », par des personnes qui depuis des insitutions publiques ou privées essaient d'être force de proposition et moteur de changements. Voici quelques ressources pouvent peut-être aider. 

### 4 pratiques pour faciliter la collaboration dans un environnement institutionnel

+ [étude de cas: inauguration de La Base](http://lesvigies.fr/la-base-laboratoire)

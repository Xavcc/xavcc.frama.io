---
title: "Les Mains Dans Le Temps : Chronique de la Bio − Ep.0"
layout: post
date: 2020-05-04 14:40
image: /assets/images/inside_bio.jpg
headerImage: true
tag:
- hsociety
- Lmdlt
- podcast
- Biohacking
category: blog
author: XavierCoadic
description: prequel d'un exercice descriptif dans les pratiques et techniques de la Bio
---

Je viens de terminer l'audio et les texte sur un discours de [Michel Serres de 2002](/humains-4) qui traite de la question de « l'humain » notamment par ses rapports à la technique. Celle qui nous aide à agir sur le temps par *pliage* et cela de façon *multiple*.

Je tente aussi depuis quelques mois de formuler un retour d'expériences sur 6 ans de biohacklab et 9 mois d'atelier de biologie populaire dans un café. Entremêlant cela avec les 12 derniers mois de collaborations sur « [Exposing The Invisible](https://kit.exposingtheinvisible.org/en/) »

De plus, j'essaie d'explorer des possibles aspects et occurrences de *[scénifications](https://notecc.frama.wiki/norae:biologicus:note_lab0_bi0_p0p_critique-)* dans certaines pratiques de la biologie. Enfin, je me casse les dents à espérer motiver des personnes averties ou non sur les enjeux d'*open wetware*[^1].

Tout cela relève de l'effort performatif, chaque engagement constitue, même partiellement, simultanément l'action qu'il exprime. Ces actions sont toutes composées de techniques, matérielles et intellectuelles, qui *brillent* par leur performativité et *plient* de *multiples* couches de temps concentrées en des points cristallisants et cristallisés − agencement par symétrie,  en matérielle ou intellectuelle, de composant en une forme plus ou moins esthétique et résistante.

Tout cela masque et fait oublier des essentiels, les gestes de la main et tous ceux qui se passent avant ce phénomène du *tout* décrit ci-avant. C'est un masque qui se pose, que je pose, sur tout un langage et une histoire, rendant muet des processus et des vécus qui précèdent la création.

Retournons la table `(╯°□°)╯ ҉ ┻━┻` !

Je commence dès lors une chronique `#Lmdlt` « Les mains dans le temps » pour suspendre ce « tout qui masque » et décrire des vécus, introspection, et des gestes, rétrospection. Essayons de nous sortir des croyances et de l'apparition en des réalités extérieures à ce que nuos faisons. Qu'est le vécu dans l'avant de la conception qui préfigure une séance de chromatographie dans un parc urbain ? Quels sont les gestes, réussis ou accidentés, avant la [mise à l'eau d'un robot sous-marin](openrov-explore/) ? Quels sont angles dans lesquels on navigue ou on se cogne avant de concevoir une bio-investigation ? D'où viennent les inspirations, les informations, les ressources, les situations d'apprentissage, pour faire des actions collectives de prélèvements en rivières polluées ?

### Dire, écrire, décrire

Lors de l'entretien sonore mené en mæstria par Sylvia Fredriksson, intitulé « [Prendre prise sur le quotidien d’un monde catastrophé](https://notesondesign.org/xavier-coadic/) », j'avais déjà pris conscience de ce besoin de *suspendre* les *performativités* et *mises en scène*, politisées parfois, sidérantes de plus en plus souvent. Le « phénomène tiers-lieux » francophone est de plusieurs manières souffrant de ces mascarades. Il ne s'agit en aucune volonté de dépolitiser des pratiques de la biologie ouverte, citoyenne, hors des institutions, dont on aurait oublié déjà la courte histoire[^2]. Il ne s'agit pas, *a pari*, de dépolitiser le tier-lieu[^3].

Cette chronique naissante, `#Lmdlt`, je la prends à corps et à cœur pour lui faire porter par différents formats (son, écriture, vidéo, dessins…) l'exercice de la description de gestes et de vécus. Cette pratique mise en série documentaire pourrait parfois prendre des allures d'épistémologie qui chercheraient dans un *amont* des signes et des mots qui décortiqueraient la main sous la table, la tête derrière les nuages, le corps sous l'habit. En ce sens, bien que non prédéterminé et souhaité conditionnel dans ses formes, cet entraînement, cette manœuvre, s'insère dans des efforts entretenus collectivement de repolitisation.

J'espère trouver des fils pour défaire des voiles ; des fibres pour apprendre et pour comprendre à composer autrement des rapports au monde dans ces chroniques. J'espère d'autant plus fort donner à voir et à intéresser des personnes qui jusqu'à présent regardaient à peine ces micro-univers de notre monde, ou craignait de n'y rien comprendre. En effet, il y a bien des points communs et ressentis partagés par de nombreux individus, quand bien même ces confluences seraient situées entre les bouts d'une personne qui fait dessiner des enfants et une autre qui travaillerait sur le génome d'une bactérie.

Vivement que l'on se retrouve autour de la chronique n°1.


## Notes et références

[^1]: Dans le fediverse <https://mamot.fr/@XavCC/104047471000281713> et <https://mamot.fr/@XavCC/102858017526478840> ; dans « le monde d'après <https://lemondedapres.reflets.info/contributions/52cf335a-9b8b-418a-9027-35219c03cbbc> ; dans «  Appel 'Pour du libre et de l’open en conscience' » <https://www.covid19-que-lire.fr/newest> ou <https://www.covid19-que-lire.fr/item?id=327>

[^2]: "Forgotten Histories of DIYbio, Open, and Citizen Science: Science of the People, by the People, for the People?" Denisa Kera <https://www.academia.edu/42856631/Forgotten_Histories_of_DIYbio_Open_and_Citizen_Science_Science_of_the_People_by_the_People_for_the_People>

[^3]: ÉTUDE DE LA CONFIGURATION EN TIERS-LIEU - La repolitisation par le service. Antoine BURRET Thèse de doctorat de Sociologie. Dirigée par Gilles Herreros. <https://movilab.org/wiki/Etude_de_la_configuration_en_Tiers-Lieu_-_La_repolitisation_par_le_service>
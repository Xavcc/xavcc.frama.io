---
title: "Pourquoi des ateliers de biologie populaire à Rennes ?"
layout: post
date: 2019-10-04 16:48
image: /assets/images/alice-in-wonderland.jpg
headerImage: true
tag:
- hsociety
- Tiers-Lieux
- Biodesign
- Rennes
category: blog
author: XavierCoadic
description: faire des petits de trucs et les assembler ensemble.
---

Des petits ateliers accueillants chaque semaine, cela pourrait être sympa ? Un peu comme une récréation avec ses bordures dans lesquelles beaucoup de choses sont permises, y compris de se cacher et de faire des bêtises.  

C'est ce qui est proposé à Rennes depuis déjà 3 semaines et qui va se poursuivre jusqu'en juin 2020. Pour faire clair, il s'agit plus de rencontres, sans trop de prétention, pour partager ensemble des savoirs et des techniques, des histoires, autour, sur et dans le Vivant. 

Intéressant... Cependant la biologie c'est hyper vaste… La biologie pour beaucoup de personne un hyper-champs qui par son immensité peu donner le vertige, qui par des formes de mystifications peu amener des craintes, qui par ces craintes peut faire penser ne pas être « à la hauteur » ou « légitime » pour s'atteler à la biologie et venir un ou des atelier(s) − craintes que je ressens aussi en écrivant cet article par exemple. Dans le Vivant les choses, les phénomènes, n'adviennent pas d'un individu seul, comme pour ce article (voir ([Remerciements](#remerciements)). Dans ce champ vaste de la biologie on peut croiser les routes des questions de santé, de l'écologie, de l'étude cellulaire, ou encore de la biochimie, de la biocomputation, l'agriculture, la cuisine... Mais aussi de la fermentation, de la bièrification, de la boulangerie, de l'étude des organismes de océans, des plantes, de l'intimité humaine et des interrogations sur les rapports humains et non-humain.

> « Important : **en 2020, les ateliers de biologie populaire continuent les Mercredi de 17h à 20h toutjours au même endroit**

**Ne prenez pas panique aux abords de grands concepts ! Ils ne seront que tous petits une fois que vous aurez commencé à marcher dedans jusqu'à un bon point de vue, comme en randonnée**.

Et puis à plusieurs, même si la route est longue, nous allons plus loin.

**Tous les Mercredi de 17h à 20h**, 212 rue de Vern, dans ces rencontres et avec la répétition de ces rencontres, se tissent des liens et se construisent des capacités qui peuvent permettre de mieux comprendre les phénomènes biologiques, de détourner ou de fabriquer des outils d'études et de manipulation des phénomènes biologiques. Y compris en se réappropriant le langage qui permet de dire et de décrire le Vivant.

Tout ceci vient parfois aux portes des sciences plus traditionnelles et des pratiques artistiques plus courantes. 

Nous prenons notre temps pour ouvrir des voies libres dans lesquelles vos voies libèrent des imaginaires avec votre langage pour un peu mieux vivre et faire des petits bouts de trucs ensemble.

## Ateliers ?

Cela se passe dans un café excentré du centre-ville (vous trouverez les informations [ici](https://www.agendadulibre.org/events/20473)). Vous venez, vous prenez une consommation de votre choix au comptoir et c'est parti.

**Atelier pour les personnes de 7 à 77 ans**, il n'y a aucun pré-requis de compétence ou de qualification, ni de biologie, ni de paysagisme `{^_^}`
J'essaie d'y mettre des méthodes d'éducation populaire, lecture et aussi tricot ou origami. Par exemple, le jeudi 3 octobre nous avons parcouru à 3 personnes un texte « [Anthropocène, Capitalocène, Plantationocène, Chthulucène](https://www.cairn.info/revue-multitudes-2016-4-page-75.htm#) », De [Donna Haraway](https://fr.m.wikipedia.org/wiki/Donna_Haraway) avec la méthode [d'arpentage](https://la-trouvaille.org/arpentage/). C'est une pratique qui provient des ouvrières et des ouvriers qui cherchaient à s'approprier les textes de lois pour s'émanciper et s'auto-organiser. Donc, avec deux personnes, ni ingénieures, ni scientifiques, qui venaient poser des questions sur les bouleversements climatiques qui les inquiète, nous avons arpenté un texte avec des concepts puissants et nous avons commencé à préparer un atelier [Chthulucene](https://notecc.frama.wiki/norae:biologicus:note_lab0_bi0_p0p_methodes-1). Le choix de travailler les bouleversements du système Terre et de systèmes mondes avaient décidé par d'autres participant⋅e⋅s le jeudi précédent. En 2019 les ateliers se déroulaeient le jeudi, en 2020 ce sera, eu moins jusq'en mars, les mercredi de 17h à 20h.

C'est aussi un atelier qui recrée, c'est vous qui choisissez de proposer de ce que l'on y fait ou c'est vous qui choisissez de ne rien dire et de suivre d'autres personnes. Vous avez droits de faire des erreurs.

Nous tentons d'être très attentives et attentifs aux conditions des personnes qui viennent et à leurs bien être. Pour cela, nous appliquons un [code de conduite](https://notecc.frama.wiki/cgu#code_de_conduite)

Dans ces ateliers on parle du Vivant, son histoire, son fonctionnement, ses manipulations. Il y a des regards diverses qui se croisent sur les OGM, notamment lors d'ateliers d'études pratiques, ainsi que sur bien d'autres sujets comme la population, les catastrophes industrielles, la bière et le pain...

Zéro blouse, ces ateliers ne sont pas dangereux, ne tâchent pas et ne comporte pas de risque. Sauf peut être le risque de s'amuser et d'apprendre.

![](https://pixelfed.social/storage/m/9415380b19c20b948c98d7c5f1fb65710b040a9e/84138976bd0c83fbc4e555c25b01217e9455d976/lBN9zsCVvXIiPogwXGZoLr9MrcHH9g8eReABQAAz.jpeg)

<figcaption class="caption">Hier une personne s'inquiétait de ne rien pouvoir faire car peu de moyen financier et peur de ne pas savoir utiliser du matériel professionnel si il lui était mis à disposition. J'ai montré comment faire de la microscopie une un téléphone équipé de caméra, une lentille grossissante de récup et un peu de bricolage. <a href="https://fr.wikipedia.org/wiki/Foldscope">Un cousin du Foldoscope</a>. On est allé chercher ensemble un lichen, Xanthoria parietin,  sur le trottoir car une autre personne présente cherchait comment identifier et inventorier "les indicateurs de qualité de l'environnement". Méthode d'identification et d'inventaire seront traitées dans un prochain atelier à la demande de cette personne. C'était juste une démonstration et de la démystification − Faire ensemble de choses simples</figcaption>

Il y a des choses un peu plus _folles_ prévues dans les tiroirs, par exemples, faire de musiques avec des micro-organismes (avec l'artiste [Guido Huebner](https://www.discogs.com/artist/76933-Das-Synthetische-Mischgewebe)), des essais sur l'obfuscation des intimités face à la surveillance faciale et fichage génétique, des tests à faire soi-même sur la qualité de sols et de l'eau, des séances cinéma... **Et en fait tout ce que vous proposerez**

Le lieu, le jour et les horaires des ateliers populaires de biologie à Rennes pourront être modifiés dans les cas où d'autres lieux se manifesteraient, dans les cas où des personnes motivé⋅e⋅s souhaiteraient s'impliquer dans l'organisation et l'animation.


## À quoi ça sert ? Y a-t-il un objectif concret à court, moyen terme ?
 
À se rencontrer...En sortira ce qui en sortira. Ce sera déjà bien. 

+ Tu peux être aidé⋅e par une personne qui anime
+ Tu t'organises, sans te reposer sur les non-organisat⋅rices.eurs.
+ Tu honores la loi des deux pieds.
+ Tu sais que les personnes présentes sont les bonnes
+ Les participant⋅e⋅s viennent lorsqu'elles, ils le peuvent, elles et ils restent le temps qu'elles et ils souhaitent
+ Les participant⋅e⋅s viennent les mains vides ou avec ce qu'elles désirent apporter, en fonction de leurs possibilités et de leurs projets
+ Ce qui ressort de ces ateliers sont les fruits des jardins communs cultivés par les parties prenantes

Des ressources sous licence libre, des personnes qui proposent d'autres ateliers ailleurs ?

Des petits bouts de trucs qui collent ou qui puent... Des souvenirs et des partages.

Il n'y a pas d'intérêt financier. Ce travail est fait bénévolement et gratuitement. Les dons sont acceptés.

## Populaire ?

Par usage et questionnement sur et dans la culture populaire. Par exemple le personnage/concept [Chthulu](https://fr.wikipedia.org/wiki/Cthulhu), ou l'écoute d'émission de radio ensemble pour discuter sous cercle de paroles ensuite. Les échanges de manga et d'autres formes de littératures qui abordent le sujet que nous explorons... Tout cela nous permet d'aborder autrement que par la serrure académique des sujets variés dans le vaste champ du Vivant tout en confiant à autrui, même temporairement, une part de chose qui nous constitue, un échange de morceau de culture.

Cette culture populaire est une substance molle. Oui nous avons du mal à la définir et à la circonscrire. Pourtant nous savons la ressentir, elle nous imprime et nous savons la partager. À chaque effort de définir ces parts de cultures, elles prennent d’autres sens dans nos rapports humains sociaux et sociétaux. Elles n’ont de cesse de nous échapper. Ce qui faut le cas lors du festival Pas Sage En Seine 2019 et le moment « [Biopanique, Cuisine et Streethack](pas-sage-en-seine-2019/) »

Par emploi de méthodes issues de l'éducation populaire et aussi par conception de nouvelles méthodes. Nous avons déjà utilisé l'arpentage et des personnes venues au premier jeudi ont proposé d'autres méthodes (cercle samoan, atelier non-mixte, dessin à plusieurs mains...). Il y a aussi une volonté de mettre à l'épreuve de nouvelles méthodes issues de ces ateliers populaire, comme « [Atelier Biopanique, cuisine et féminisme](https://notecc.frama.wiki/atelier:biopanique_cuisine_et_feminisme) », sous licence libre, afin cultiver nous aussi les jardins qui nous apportent de ressources.

Puis il y a des choses simple et efficace. Les échanges de graines, de levures, de plantes... 

Qui permettent de se poser collectivement des questions _dingues_. Comment faire la cuisine une fois que l'on a appris la manipulation des levures ? Pourquoi la cuisine libre est un partage de pan de culture libre ? Anthropocène ou Capitalocène ? Le rapport au temps est-il différent lorsque l'on est opprimé⋅e⋅s ? Comment décoloniser nos pratiques ? Comment concevoir une gestion de Communs ?

![](https://pixelfed.social/storage/m/9415380b19c20b948c98d7c5f1fb65710b040a9e/84138976bd0c83fbc4e555c25b01217e9455d976/SqAcSD8JclDFtFeUqm3CyaBjr6rNeFTq2EW7hg6j.jpeg)

<figcaption class="caption"> Début d'études sur biofilm et les procédés de biofabrication à #Rennes dans le cadre de Lab0·bi0·p0p.
#biologie populaire. <a href="https://pixelfed.social/p/XavCC/85648436279513088">Source d'origine</a>
Cellulose produite à partir d'un symbiote d'Acetobacter (bactéries) et de Saccharomyces cerevisae (levures également présentes dans la bièrification, la fabrication du pain et dans les pratiques d'aquariophilie) ; obtenu en environnement de fermentation</figcaption>

> « _Et en termes d'orga, comment ça se passe ? Conférences ? Ateliers pratiques ? Une question lancée et des petits groupes y répondent ? Un peu de tout ? Est-ce qu'on permet aux gens timides de prendre la parole, pour que vraiment tout le monde participe ?_ » Ewen

Très concrètement, il n'y a pas vraiment d'organisation, ou de menu, pré-établi. D'abord parce que c'est une démarche récente et seulement quelques personnes curieuses sont venues. Ensuite, il est important de laisser le plus d'ouverture possible, y compris aux choses surprenantes ; et même de s'efforcer d'ouvrir d'autres possibles petit à petit. Ce sont les personnes présentes et/ou impliquées (à distance parfois) qui proposent des sujets, des formats, des thèmes. 

**Les pratiques ?** J'aide à ce que toute personne intéressée puisse aller à son rythme et à sa manière vers de l'autonomie.

**L'animation ?** J'aide de la même manière que pour les pratiques.

**Les personnes timides** ne sont pas exposées aux situations dérangeantes et les personnes _plus extraverties_ sont invitées à prendre attention de ne pas faire du tort à autrui. La parole n'étant pas l'unique moyen d'expressions. il y a même de la programmation informatique.

```bash
`--> (cat hilbert.Blc; echo -n "1234") | ./tromp64
 _   _   _   _   _   _   _   _ 
| |_| | | |_| | | |_| | | |_| |
|_   _| |_   _| |_   _| |_   _|
 _| |_____| |_   _| |_____| |_ 
|  ___   ___  | |  ___   ___  |
|_|  _| |_  |_| |_|  _| |_  |_|
 _  |_   _|  _   _  |_   _|  _ 
| |___| |___| |_| |___| |___| |
|_   ___   ___   ___   ___   _|
 _| |_  |_|  _| |_  |_|  _| |_ 
|  _  |  _  |_   _|  _  |  _  |
|_| |_| | |___| |___| | |_| |_|
 _   _  |  ___   ___  |  _   _ 
| |_| | |_|  _| |_  |_| | |_| |
|_   _|  _  |_   _|  _  |_   _|
 _| |___| |___| |___| |___| |_ 
```

## Pourquoi tout ça ?

Il n'y a aujourd'hui à Rennes rien de similaire, il était alors impossible de venir se greffer sur une initiative, foisonnante ou non, pour l'enrichir et profiter de son implantation pour être serein⋅e. Il y a aussi à Rennes une politique municipale et métropolitaine qui place ces priorités ailleurs que dans ces champs exposés au travers de ces lignes. Il a eu à Rennes le [Biome Hacklab](https://invidio.us/watch?v=o-LkwTpjm6g), en sommeil après une résidence à l'Hôtel Pasteur fin 2017.

Ces ateliers sont ainsi une forme de [rejet botanique](https://fr.wikipedia.org/wiki/Rejet_(botanique)).

Il y a aujourd'hui des questionnements profonds qui angoissent des personnes, des risques quotidiens qui affectent et abîmes d'autres, des prophètes et gurus plus ou moins médiatiques, des catastrophes qui déchirent les chaires et les vies... Comment s’équiper face aux bouleversements des mondes connectés et catastrophés ? Ces ateliers ne portent pas la prétention d'apporter des réponses à tout cela, ils ne sont et ne resteront que des circonstances de rencontres pour s'entrainer, s'entraider, se poser des questions, parfois en hachant et déconstruisant des logiques, aussi technicienne ou technologicienne.

Ces ateliers sont aussi là car je ruminais et souffrais depuis presque un an de travailler (tous ces trucs de préparation à...) à l'ouverture d'un _prochain_ biohackerspace à Rennes sans prendre le temps de refaire des petits bouts de trucs simples et des rencontres. Kaouenn-noz arrive et qu'il est lourd et chiant à faire advenir...Ces ateliers ont lieu car j'en avais envie et besoin.

Ces ateliers en cours car des personnes autour de moi, à Rennes ou plus loin, et d'autres rencontrées ont exprimé l'envie de voir advenir ces ateliers. Ils sont des clins d'œil, des appels du pied et des preuves que c'est faisable et agréable ; Ils sont une invitation.

Ces ateliers sont là car dans une ville que se gentrifie à cadence TGV, la vie de quartier devient un enjeu de persistance.

Ces ateliers ont lieu car si j'avais titré Biohacking et Biodesign à Rennes cela aurait repoussé encore plus de personnes.

> « _Le “vert” (Green), symboliquement associé au “naturel” et utilisé pour hyper-compenser ce que les humains ont perdu, doit être considéré comme la plus anthropocentrique de toutes les couleurs, dans son ambiguïté inhérente entre naturel et artificiel. Sommes-nous en contrôle du “vert” ? Malgré ses connotations largement positives, le “vert” Le “vert” sert de plus en plus le désir inconditionnel de naturalisation fétichiste et techno-romantique afin d'hyper-compenser métaphoriquement la biopolitique systémique matérielle consistant en la manipulation technique croissante et l'exploitation des systèmes vivants, des écologies, et de la biosphère en général._ » Jens Hauser, openfields 2019 <https://openfields2019.rixc.lv/openconf.php>


## Si vous êtes gourmand⋅e

Et que vous désirez déjà aller plus loin avant de nous rejoindre, je vous conseille cette excellente émission radio

« [Ex0114 Design spéculatif, biotech et algorithme prédictif -  Version longue de l'interview diffusée dans l'émission CPU.](https://cpu.dascritch.net/post/2019/09/26/David-Benqu%C3%A9%2C-chercheur-et-designer)

+ Enregistrée en Juillet 2019 au sein de la Bibliothèque de la Goldsmiths University de Londres.

Bio is the new Black invite artistes, designers, philosophes, scientifiques et ingénieurs à explorer les multiples questions éthiques, critiques et de créations qui se posent avec les technologies de bio-fabrication. »

## Remerciements

Cela faisait trois semaines que l'envie d'écrire ce texte me rendait boule et capharnaüm. J'ai fait un appel dans un coin d'Internet et des personnes sont venues m'aider et veiller à l'accouchement de ce texte. Pour votre maïeutique, je vous remercie grandement les ami⋅e⋅s :
+ Ewen
+ La Reine de Elfes
+ Fipaddict
+ David Benqué
+ Maïa

#### Illustration d'en-tête

License: [CC0 Public Domain](https://creativecommons.org/publicdomain/zero/1.0/)

Dawn Hudson has released this “Alice In Wonderland” image under Public Domain license
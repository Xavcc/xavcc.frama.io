---
title: "Notes depuis le colloque : À l’épreuve des tempêtes. Institutions et crises : approches historiques"
layout: post
date: 2023-04-27 06:40
image: /assets/images/colloque-rennes2-2023.png
headerImage: true
tag:
- hsociety
- Notes
category: blog
author: XavierCoadic
description: 
---

Le 16 et 17 mars 2023, avait lieu à la Maison des Sciences de l’Homme, en Bretagne à Rennes, un colloque intitulé « [À l’épreuve des tempêtes. Institutions et crises : approches historiques](https://ahmuf.hypotheses.org/11299) ([archive web](https://web.archive.org/web/20230305191547/https://ahmuf.hypotheses.org/11299)).

C’est <cite>Solène Minier</cite>, Doctorante en histoire médiévale à Sorbonne Université et à l’Università degli Studi di Padova, qui m’a donné l’envie d’y participer.
Ses recherches portent sur la participation des femmes aux circulations économiques dans les sociétés urbaines et aux mutations de l’État de Terre ferme vénitien de la première Renaissance (XIVe-XVe siècles).

> « Connaissez-vous les conséquences inattendues des terribles épidémies de peste qui ravagent l’Italie médiévale à partir de 1348 ? Les 16 et 17 mars 2023 je serai à Rennes (laboratoire Tempora) au colloque Crises et #Institutions pour expliquer comment les nouveaux legs religieux apparus pendant les pestes permettent au XVe siècle à des aristocrates privés de pouvoir politique de réaffirmer leur domination sociale. » <cite>Solène Minier</cite>, *via Mastodon*, 9 mars 2023

Je n’ai pas eu le loisir et le plaisir d’assister à l’intégralité du colloque. Je fus présent uniquement lors de la première journée jusqu’à l’intervention de <cite>Lucile Chaput</cite>, Tempora (Université Rennes 2) : « *Les camps d’internement canadiens durant la Seconde Guerre mondiale : entre mise en ordre et remise en cause* ».

Le colloque était annoncé ainsi :

> « La crise est omniprésente, au moins dans les discours. Sans tomber dans le catastrophisme, il faut bien constater que la pandémie de Covid-19 a mis à l’épreuve nos contemporains et nombre d’institutions (Bergeron et al. 2020) ; sans doute les défis à venir, ou en tout cas annoncés, les éprouveront davantage. Cela ne peut manquer d’interpeller l’historien·ne. Quelles relations entretiennent les institutions et les crises ? La question doit être posée au regard des expériences des sociétés du passé. Surtout, elle ne peut faire l’économie d’un travail de définition de ces concepts pour le moins vagues. Voici un enjeu essentiel de ce colloque adressé aux jeunes chercheur·e·s, qui n’entend pas proposer un prêt-à-penser, mais suggérer des pistes de réflexion, afin de conférer une vertu heuristique à ces concepts. Nous insistons ici sur l’importance des études empiriques pour fonder le comparatisme : c’est la condition pour atteindre un degré de généralité satisfaisant dans une telle perspective. » <cite>Association des Historiens Modernistes des Universités Françaises</cite> (Ahmuf), dans *[hypothèses](https://ahmuf.hypotheses.org/10260)*

![](/assets/images/colloque-rennes2-2023.png)
<figcaption class="caption">Capture d’image de l’illustration depuis le pdf de présentation du colloque</figcaption>
<br />

Voici mes notes parcellaires (avec les pieds et depuis le fond de la salle près de la fenêtre ouverte)

**TOC**

+ [Ouverture](#ouverture-par-jean-françois-morvan) par <cite>Jean-François Morvan</cite>
+ Session 1 [(Re)négocier les cadres institutionnels dans les contextes de crise (I) : Élites et institutions urbaines dans la crise](#renégocier-les-cadres-institutionnels-dans-les-contextes-de-crise-i--élites-et-institutions-urbaines-dans-la-crise)
  + [À qui profite la crise ? Les consuls de Toulouse pendant la croisade contre les Albigeois (1215-1229)](#à-qui-profite-la-crise--les-consuls-de-toulouse-pendant-la-croisade-contre-les-albigeois-1215-1229), Margaux Lemaire
  + [Paroxysme documentaire et innovations sociales dans l’Italie tardo-médiévale](#paroxysme-documentaire-et-innovations-sociales-dans-litalie-tardo-médiévale), Solène Minier
  + [Crises et institutions urbaines dans les petites villes du Cantal (XIIIe-XVIe siècles)](#crises-et-institutions-urbaines-dans-les-petites-villes-du-cantal-xiiie-xvie-siècles)
+ Session 2 [(Re)négocier les cadres institutionnels dans les contextes de crise (II) : Négociations, débats et remises en ordre des institutions](#renégocier-les-cadres-institutionnels-dans-les-contextes-de-crise-ii--négociations-débats-et-remises-en-ordre-des-institutions)
  + [Concilier défense militaire et intérêt du corps de ville : une décennie de crise à La Rochelle (1621-1628)](#concilier-défense-militaire-et-intérêt-du-corps-de-ville--une-décennie-de-crise-à-la-rochelle-1621-1628), Ariane Godbout
  + [Des crises dans la gestion des ressources halieutiques. Les “malaigues” de l’étang de Manuguio (bas Languedoc, XVIIIe siècle)](#des-crises-dans-la-gestion-des-ressources-halieutiques-les-malaigues-de-létang-de-manuguio-bas-languedoc-xviiie-siècle), Elias Burgel
  + [Les camps d’internement canadiens durant la Seconde Guerre mondiale : entre mise en ordre et remise en cause](#les-camps-dinternement-canadiens-durant-la-seconde-guerre-mondiale--entre-mise-en-ordre-et-remise-en-cause), Lucile Chaput

Les dimensions de temporalité et d’intensité font partie de la « Crise ». Une analyse lexicométrique permet d’annoncer que l’utilisation du terme est en augmentation  du 18e siècle jusqu’en 1929.

La « Crise » représente une tension avec un futur non advenu. Elle met l’institution a nue, elle a donc un caractère processuel. La « crise » est d’abord un rapport au temps alors que l’institution est force dite pérenne qui n’est pas prise dans les contingences.

> « Si, au XIXe siècle, la philosophie progressiste lui donna un sens positif, celui d’un temps de transformations, reste que la crise demeure dans ses utilisations contemporaines un repoussoir. Mais, qu’elle soit positive ou négative, la crise est bien le temps des possibles entre un passé révolu et un futur non advenu. » <cite>Amhuf<cite>

La « crise » est considérée comme rupture (*face à une continuité supposée*). La crise s’inscrit dans le temps de la conjecture.

`Crise = temps normal & stabilité = fiction`

> « La relation entre crise et institution politique est établie. Michel Dobry défend l’idée de dépassement de la rupture portée par la crise pour penser la continuité des institutions et des individus qui la composent (Dobry 1989). L’intérêt du colloque réside tout entier dans la conviction que les institutions masquent un caractère processuel, que rappelle l’étymologie du mot et que les crises peuvent faire ressurgir » <cite>Amhuf</cite>

## Ouverture par Jean-François Morvan

La « crise » est événement, lorsque l’on porte son regard dessus un des risques est de tenir une focale trop étroite.

<cite>Michel Dobry</cite> : les logiques propres de la crise sont liées aux logiques de situation. Une crise se caractérise par un effacement des frontières organisationnelles. La situation devient “fluide”, la nature du problème comme le rôle des différentes organisations ne sont plus clairement établis et font l’objet de négociations, de stratégies isolées ou de conflits.
Dans leurs diachronies elles sont des entités inscrites dans le temps et elles sont mortelles.

Alors, les Institutions sont-elles filles de crises ? 

+ La permanence qui efface le processus, autrement dit la « crise », doit être regardée dans ses rapports avec et dans l’Institution et sa définition.

Les institutions sont de grandes pourvoyeuses d’écritures grises. Avec ces écritures l’Institution met en scène une fiction de stabilité(s) au travers de productions de règlements. 

La crise remet du politique dans le processus.

Les pratiques et les routines qui font exister l’Institution sont actualisées par des individus, il y a un espace normatif qui les acteurs peuvent combler.

La « crise » ouvre un espace qui permet de redéfinir les règles de constitutions de l’Institution.

Des acteurs peuvent avoir intérêts à la « crise », il y a donc des usages sociaux de la crise.

En 3 phases :

+ Invoquer la crise c’est créer le sentiment d’urgence.
+ La réforme d’une Institution : se réforme dans le cadre discursif de crise.
+ Dans la narration de la stabilité : il s’agit de justifier sa légitimité par l’Institution pour la mettre au service d’une stabilité.

## (Re)négocier les cadres institutionnels dans les contextes de crise (I) : Élites et institutions urbaines dans la crise 

### À qui profite la crise ? Les consuls de Toulouse pendant la croisade contre les Albigeois (1215-1229)

<cite>Margaux Lemaire</cite>, FRAMESPA (Université Toulouse Jean Jaurès)

*Communication du colloque non trouvée publiée au 24 avril*.

La crise « supprime » un des haut lieu d’incarnation, de réunion, et d’expression du pouvoir.

Histoire de tactique face à la « crise », ici conflit armé :
+ Quitter
+ faire profil bas
+ Collaborer


Après retour de « la normalité » d’avant crise ?
+ Un renforcement de la concorde, lieu de pouvoir « supprimé », avec inclusion des 3 profils ci-dessus

À qui profite la crise ?
+ Le consulat : plus puissant et plus indépendant qu’avant et qu’il n’a jamais été.

**Nuance à faire :  cette stabilité et retour à quelque chose avec puissance ne s’applique qu’à une des strates sociales : celles de hautes élites.**

Voir aussi : « *Le comte, le consul et les notaires. L’écriture statutaire à Toulouse au XIIIe siècle*, <cite>Gabriel Poisson</cite> <https://books.openedition.org/psorbonne/26054>

> « […] les élites laïques de la ville de Toulouse, d’où est souvent absent le comte Raymond V, se constituent en un chapitre de consuls, mentionné en 1152, qui assume l’essentiel de la direction de la ville à partir des années 1180. Cette structure sociale et institutionnelle se trouve confrontée à la guerre dirigée par la papauté contre l’hérésie, qui devient rapidement un conflit politique marqué par l’élimination du vicomte Trencavel en 1209, puis celle du comte de Toulouse en 1215. La croisade menée désormais par le roi Louis VIII s’achève en 1229 par la défaite des Méridionaux et entraîne la réunion du Toulousain aux domaines capétiens, malgré une tentative de révolte de Raymond VII en 1242. La volonté de reprise en main de sa principauté par le dernier comte raymondin se heurte aux ambitions et aux pratiques des élites toulousaines, menant le pouvoir comtal à essayer de contrôler l’administration municipale, notamment dans ses compétences normatives », <cite>Gabriel Poisson</cite>, dans _La confection des statuts dans les sociétés méditerranéennes de l’Occident (xiie-xve siècle)_  Didier Lett (dir.), 2017


### Paroxysme documentaire et innovations sociales dans l’Italie tardo-médiévale 

<cite>Solène Minier</cite>, CRM (Sorbonne Université)

> « Ce travail veut interroger les temporalités de la crise : comment le choix d’une échelle de temps pour étudier une crise influe-t-il sur la définition même de la crise ? Quelles interactions unissent le temps court d’une épidémie et le temps plus long des crises institutionnelles et élitaires ? A Padoue, au tournant des XIVe et XVe siècles, d’une part, le sursaut épidémique momentané accélère et rend acceptables des innovations testamentaires formelles, en particulier des dispositions visant à la concentration patrilinéaire du patrimoine, et qui, passée la crise, sont pérennisées au service des stratégies d’affirmation sociale des élites. D’autre part, de nouveaux types de legs pieux apparus lors de l’épidémie (maisons réservées à des veuves pauvres) deviennent, dix à vingt ans plus tard, de véritables institutions charitables de bien plus vaste ampleur. Celles-ci offrent aux élites, privées par la conquête de leurs leviers politiques usuels, un exutoire pour retrouver du pouvoir et une justification de leur prééminence sociale. »

*Communication du colloque non trouvée publiée au 24 avril*.

**Localisation** : Ouest de Venise à Padoue, « cité rival.

**Situation** : Césure de la conquête de Padoue (*dont le travail de recherche de Solène Minier s’insère entre les travaux des 14èmistes et 15èmistes*)

Des mutations remarquables apparaissent dans des testaments avant la crise de 1405 (Institution de travail), quantité, contenu et ayant droit, finalité. Les ravages de l’épidémie, le moment *du millénaire* et de la religion, le menace de Venise, influent sur les personnes, leur propension à « écrire un avenir » et leurs dispositions inscrites dans leurs *Desiderata*.

Après sa conquête armée, Venise impose des punitions face à la résistance adverse :
+ Mise sous tutelle directe
+ Disparition des 2 institutions historiques et endogènes
+ pression fiscale très forte en faveur de Venise
+ perte de contrôle de l’Université

C’est un moment de crises sociales à Padoue et il y a mise en force de la perte d’identité organisée par Venise.

Pour le nombre de testaments, je préfère garder le tableur, car il fera partie de mes annexes de thèse qui sera publiée ensuite. En revanche, le mouvement à retenir, c’est le pic de testaments (dans la limite des biais de composition du corpus) sur la période 1385-1400 : une période marquée par des guerres récurrentes (1388, 1390, 1403-1405), des épidémies de peste (surtout 1393-1400).

Le contexte du « juste avant  la crise politico-militaire » : la crise épidémique des années 1390 et attentes eschatologiques et attentes messianiques sont très fortes.

> « Ce n’est donc pas seulement la hausse numérique de la mortalité qui conduit à l’augmentation du nombre de testaments, mais aussi des préoccupations accrues des testateurs pour le salut de leur âme.
>
> Les hommes _sans enfants_ nomment de plus en plus souvent leur épouse héritière. Avec enfants, cela change pas, les descendant⋅e⋅s directes (garçons ou filles) sont choisi⋅e⋅s dans l’écrasante majorité des testaments.
>
> La tendance à faire une place à l’épouse comme héritière, même avec des conditions limitantes (ex. ne pas aliéner certains biens, ne pas disposer de tout dans son testament à elle), apparaît assez nettement à partir de la 1ere moitié du 15e par rapport à la 2e moitié du 14e s. » <cite>Solène Minier</cite>

Une personne joue un rôle important dans la « reconstruction » d’Institution de Padoue et la préservation d’une politisation par le social. Sibilia, aristocrate, fille de Gualperto Cetto (riche négociant, propriétaire foncier et usurier,) et épouse en secondes noces de Baldo Bonafari avec lequel elle fonde l’hôpital de Padoue : Ospedale di San Francesco Grande (aujourd’hui MUSME, Museo della storia medicina in Padova). Inspiré⋅e⋅s par les idéaux évangéliques et franciscains, elle et il ont mis en place un centre de soins pour les malades et les marginaux qui répondait aux besoins sociaux émergents.

> « Dès le début, l’histoire de l’hôpital est liée à celle de la ville de Padoue : dans son testament de 1421 dans son testament de 1421, Sibilla, devenue veuve, confie son gouvernement et la gestion de son patrimoine au prestigieux Collège des juristes de Padoue, qui nomme les organes de direction et les personnalités administratives de l’institution (les gouverneurs, le prieur, le facteur, etc.) » *Ospedale di San Francesco Grande*, [Wikipedia](https://it.wikipedia.org/wiki/Ospedale_di_San_Francesco_Grande)

Déchu⋅e⋅s par les Vénitien⋅es, des Padouan⋅ne⋅s s’investissent là où les vénitiens n’ont pas de pouvoir, donc là dans de nouvelles pratiques sociales et/ou émergentes, avec aussi des petites structures charitables gérées par les laïques.

On peut sortir ici 3 axes dans lesquels une certaine classe sociale s’investit :

+ Résoudre la crise par un nouveau processus d’institutionnalisation parallèle
+ Processus qui se nourrit d’innovations non formalisées
+ Capacité d’une élite à re-créer de la domination sociale

**Ndlr** : *Je remercie (encore) Solène Minier pour avoir répondu à mes questions plusieurs semaines après le colloque.*

## Crises et institutions urbaines dans les petites villes du Cantal (XIIIe-XVIe siècles)

<cite>Joséphine Moulier</cite>, CHEC (Université Clermont-Auvergne)

*Communication du colloque non trouvée publiée au 24 avril*.

Ici, les « crises » sont à la faveur de l’émancipation politique des petites villes rurales, au pouvoir urbain très faible, dans le rapport de force avec les grandes villes.

S’affranchir de la corvée imposée dans la justification de lutte face à une menace / face à une crise : c’est s’affranchir tout court.

Les petites villes sont un laboratoire pour observer les pratiques politiques face aux crises et dans la crise ; la crise étant « l’occasion » de renégocier les termes du contrat du rapport de force.

Les crises sont prises en opportunités par des dominé⋅e⋅s et des dominant⋅e⋅ss pour investir des creux / des vides.

Voir <cite>Albert Rigaudiere</cite>. [Saint-Flour ville d’Auvergne au bas Moyen Age. Etude d’histoire administrative et financière](https://www.persee.fr/doc/bec_0373-6237_1983_num_141_2_450311_t1_0368_0000_3). Paris : Presses universitaires de France, 1982. 1 t. en 2 vol. in-8°, 1008 pages. (Publications de l’Université de Rouen.)

## (Re)négocier les cadres institutionnels dans les contextes de crise (II) : Négociations, débats et remises en ordre des institutions

### Concilier défense militaire et intérêt du corps de ville : une décennie de crise à La Rochelle (1621-1628)

<cite>Ariane Godbout</cite>, Tempora (Université Rennes 2 – Université du Québec à Montréal)

*Communication du colloque non trouvée publiée au 24 avril*.

**Contexte** : Le siège de La Rochelle, ville soutenue par l’Angleterre en tant que ville protestante

À aucun moment dans les sources le terme de crise est employé, on retrouve « urgence » et « extraordinaire », donc un phénomène de rupture, voir définition de <cite>Janet Roitman</cite> de la crise.

La crise ici influe sur une masse documentaire supplémentaire.

La « Crise » diplomatico-politique articulée autour des velléités de personnalité pour leur propre carrière dans un contexte de conflit armé (églises des réformés de France VS Roi de France).

**Voir aussi**

Le projet « Perspectives citoyennes » porté par Ariane Godbout. <https://www.grhs.uqam.ca/perspectives-citoyennes/>, 12 capsules vidéo / balado d’une durée de 30 minutes environ 

> « Ce projet veut contribuer à élargir notre conception de la citoyenneté, à repenser les bases de notre implication dans l’espace public et à explorer différentes manières de se politiser, explique Ariane Godbout. Ces besoins urgents sont mis en lumière par la perte de confiance des citoyens envers les institutions et leurs représentants, par les enjeux mondialisés que sont les changements climatiques et la pandémie de Covid-19 ainsi que par les tensions engendrées par les politiques d’immigration. »

### Des crises dans la gestion des ressources halieutiques. Les “malaigues” de l’étang de Manuguio (bas Languedoc, XVIIIe siècle)

<cite>Elias Burgel</cite>, HISTEMÉ (Université de Caen-Normandie)

*Communication du colloque non trouvée publiée au 24 avril*.

![](/assets/images/etang-manuguio-2023.png)
<figcaption class="caption">Capture d’écran de la fiche et cartographie de l’étang de Manugio depuis le site web <a href="https://inpn.mnhn.fr/site/natura2000/FR9112017">Inventaire National du Patrimoine Naturel</a> du Muséum national d’Histoire naturelle (MNHN)</figcaption>
<br />

Crise environnementale : est toujours et aussi une crise des institutions et des droits (cf. Diapos présentées n°1 et 3 points dont celui concernant <cite>Jean-Paul Bravard</cite>)

Des normes, des institutions, des droits, des pratiques sociales, se sont construites autour de la migration des poissons.

Les crises, ici de l’eau (qualités) et de resources pour activités humaines (poissons, anguilles), s’appellent des Malaigues<marginnote> « toponymie […] les noms sont la mémoire des catastrophes » Odile Plattard, voir <a href="https://xavcc.frama.io/notes-lecture-george-E-B-morren-2/">Notes de lecture sur la catastrophe : “Identification of Hazards and Responses”, George E. B. Morren Jr, (1983) 2/3</a></marginnote> (occitan, mauvaises eaux).

Louis XIV use de la rhétorique de la crise et se sert de prétexte à l’institutionnalisation du pouvoir royal dans une zone où il était absent jusque  là. L’Ordonnance de 1669 puis 1670, réforme des eaux et foret avec la grande réforme de Toulouse, avec [Louis de Froidour](https://fr.wikipedia.org/wiki/Louis_de_Froidour).

Il affirme la nécessité du pouvoir royale pour contrer le dépoissonnement et gérer les ressources. Ce qui apparaît aussi dans des procès verbaux et les dépositions de l’époque.

Réalité biophysique ou rhétorique ? Le discours de la crise permet de prétendre à la domanialité de cet espace pour institutionnellement en assurer la bonne gestion.

Voir les « seigneurs des étangs » au moment de la réforme de 1670.

Vient alors l’institution des amirautés autour du plan d’eau.

Puis déclaration de 23 août 1728 (dont l’Inspection des pêches).

> « Qui ordonne qu’en dérogeant à l’article VIII de la déclaration du 23. Août 1728. Il fera permis aux Pêcheurs de l’Amirauté d’Aiguës -mortes feulement, de pratiquer lu pêche de l’Anguille sortirttune, dans les étangs faiez situes dans ladite
Amirauté, depuis le premier juillet jusques if compris le dernier de février de chaque année, avec des filets fixes iffédentaires, dont la plus petite maille fera au moins de trois lignes en quarré. » depuis [Gallica](https://gallica.bnf.fr/ark:/12148/bpt6k9741546n.texteImage), BNF.

Il y a là création de nouvelles normes juridiques en plus de nouvelles institutions.

+ Réglementation des engins de pêches, notamment les engins de traîne.
+ Saisonnalité de la pêche

Un cadre assez fixe qui tient jusqu’à la révolution française.

Derrière la création d’un arsenal juridique et institutionnel, qui fonctionne avec très peu de moyen, la crise est partie intégrante du fonctionnement. Porter un regard sur les crises aide à interroger les fonctionnements véritables des institutions, dont la réalité est de tourner autour des processus de négociation permanents.

Pour Aigues-mortes il n’y a que 5 à 10 cas de « crise » pour tout le XVIIIe siècle.

Dans l’ancien régime, c’est la loi articulée à la police et à la supplique / pétition qui fait et maintient l’Institution. La justice royale n’a aucun sens sans les crises qui lui permettent de s’adapter. Elle s’adapte en 2 séquences articulées :

+ 1ère partie : la rhétorique de la crise
+ 2ème partie : la pragmatique de la crise

Ici, cela se déroule au XVIIIe avant l’émergence de la figure scientifique experte en collusion avec le pouvoir royal.

### Les camps d’internement canadiens durant la Seconde Guerre mondiale : entre mise en ordre et remise en cause

<cite>Lucile Chaput</cite>, Tempora (Université Rennes 2)

**Localisation** : [Garnison de Petawa](https://fr.wikipedia.org/wiki/Garnison_Petawawa)

*Communication du colloque non trouvée publiée au 24 avril*.

25 000 civil⋅e⋅s ont été interné⋅e⋅s, 22 000 canadiens d’origines japonaises ect. et des membres du parti communiste.

> « […] de considérer les camps d’internement comme des lieux « spécifiquement destinés à l’incarcération des civils » et qui servent “à la détention des prisonniers politiques et de groupes minoritaires incarcérés pour diverses raisons – sécurité
de l’État, exploitation ou châtiment” (AUGER 2005 : 11) » <cite>Lucile Chaput</cite>, 2016

Voir aussi : « *L’internement au Canada durant la Seconde Guerre mondiale : le camp n° 33, 1939-1946* », L. Chaput <https://journals.openedition.org/eccs/772?lang=en>, dans *Revue interdisciplinaire des études canadiennes en France*, 81 | 2016, Identité(s) canadienne(s) et changement global

Il y a ensuite un transfert en 1940 de prisonniers Allemands depuis l’Angleterre jusqu’au Canada. Le camp compte 28 nationalités différentes. Camilien Houde, alors maire de Montréal, est interné à Petawawa le 5 août 1940 suite à son opposition à l’enregistrement national qu’il perçoit comme une étape vers la conscription.

> « La chronologie du camp de Petawawa permet donc de mettre en avant les deux phases des opérations d’internement au Canada. La première, de 1940 à 1943, se caractérise par l’internement des civils. La seconde, de 1942 à 1946, correspond à l’arrivée des prisonniers de guerre. Leur arrivée engendre une réorganisation des opérations d’internement dans le pays en raison de l’augmentation massive des effectifs. De nouveaux camps sont ainsi créés et c’est à ce moment-là que le département de la Défense nationale décide que les camps seront appelés par un chiffre afin d’augmenter l’efficacité administrative. Le camp de Petawawa, le camp P, devient alors le camp n°33.
>
> Les nombreuses catégories d’internés présentes au camp n°33 offrent la possibilité au chercheur d’observer les interactions sociales entre des internés d’origines différentes et aux idéaux parfois opposés. De fait, le camp se présente comme une véritable « tour de Babel » en raison de la grande diversité de nationalités qui y sont représentées » <cite>Lucile Chaput</cite>, 2016

Ce camp fut déjà utilisé entre 1914 et 1916 pour enfermer des prisonniers de guerre (Autrichiens, Allemands). Ici on peut se demander si la crise constitue réellement une rupture.<marginnote>Voir aussi <a href="https://xavcc.frama.io/notes-lecture-les-camps/">Notes de lectures : Les camps et enfermement(s)</a></marginnote>

> « Chez les prisonniers de guerre, les cas d’insolence et d’évasions notamment se multiplient conduisant fréquemment à des peines d’emprisonnement de quelques jours. Des antagonismes opposent les internés aux autorités, mais aussi les prisonniers entre eux en raison d’idéologies politiques antagonistes. Le contrôle des prisonniers de guerre nazis et la prévention du développement de la doctrine nazie dans les camps canadiens constituent un enjeu majeur pour les autorités canadiennes » <cite>Lucile Chaput</cite>, 2016

**4 formes d’ajustements face à la crise en contexte d’internement** :

1. ajustements matériels, système éclairage pour faciliter la surveillance suite à des évasions par exemple
2. ajustement du personnel, programme d’entraînement, ect.
3. ajustement des internés eux-mêmes par/pour eux-mêmes, face aux nouveaux paramètres, nouvelles configurations issues de 1. et 2.
4. ajustements à l’échelle « exo » du camp / échelle nationale des règles et des acceptions

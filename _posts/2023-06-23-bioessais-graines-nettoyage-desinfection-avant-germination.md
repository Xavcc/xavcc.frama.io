---
title: "Bioessais avec des graines : quelques modes de désinfection / stérilisation avant mise en germination"
layout: post
date: 2023-06-23 05:20
image: /assets/images/bluelight-ebe714c0595ec3ad.jpg
headerImage: true
tag:
- hsociety
- bioassay
category: blog
author: XavierCoadic
description: L'idée renonce à sa substance dès son énonciation
---

Les mains vêtues de gants, la manipulation de graines des Spermatophytes, dans le but d'un dosage avec analyse effectuée pour déterminer l'effet biologique ou la toxicité d'une substance (ex : hormone, pesticide, médicament...) ou d'un contexte / qualité d'un environnement, requiert une attention particulière face des choix de design tactique.

C'est à dire de considérer et évaluer les étapes dans l'approche de la qualité par la conception pour le développement des processus[^note-design-tactique], avant de concevoir l'observation d'un "mode of action (MoA)", changement fonctionnel ou anatomique résultant de l'exposition d'un organisme vivant à une substance ou plusieurs / dans un contexte[^note-toxicology]. 

Par exemple, dans le cas où l'on voudrait tester des pollutions des sols / des eaux de ruissellement par les hydrocarbures issus de l'exploitation et usages du pétrole[^note-eau-petrole].

[^note-design-tactique]: "*Best practices in bioassay development to support registration of biopharmaceuticals*" <cite>John R White, Marla Abodeely, Sammina Ahmed, Gaël Debauve, Evan Johnson, Debra M Meyer, Ned M Mozier, Matthias Naumer, Alessandra Pepe, Isam Qahwash, Edward Rocnik, Jeffrey G Smith, Elaine SE Stokes, Jeffrey J Talbot & Pin Yee Wong</cite>, 2019 <https://doi.org/10.2144/btn-2019-003>

[^note-toxicology]: (Free Access) "*Comprehensive Toxicology*", <cite>Charlene A McQueen</cite>, Second Edition, Elsevier Science, 2, 2010 <https://en.annas-archive.org/isbn/9780080468846>

[^note-eau-petrole]: (Free access) "*Comparison of Plants for Germination Toxicity Tests in Petroleum-Contaminated Soils*", Water, Air & Soil Pollution, #1-4, 167, pages 211-219, 2005 oct, <cite>M. K. Banks</cite>; <cite>K. E. Schultz</cite>

Dans cette approche par design tactique, la désinfection / pseudo-stérilisation / stérilisation, ou non traitement, de la surface externe des graines est primordiale. Tout apport extérieur, désiré ou non, du fait de la main ou des micro-organismes présents à la surface de l'enveloppe des graines, portera des conséquences dans l'observation, le relevé d'information, la mise en "data", l'interprétation et la conclusion.

Dans la procédure, sans approche de design tactique (du moins pas conscientisée) que nous avons décrite dans "[Training on "Bioassay Test with seed to expose toxicity"](https://publiclab.org/notes/xavcc/12-23-2021/training-on-bioassay-test-with-seed-to-expose-toxicity), 2021 avec Public Lab, nous avions proposé ceci :

> « Traitez les graines de laitue dans une solution d'eau de Javel à 10 % (10 ml d'eau de Javel dans 100 ml d'eau déminéralisée) pendant 20 minutes, puis rincez avec des gants (ne touchez pas les graines, pas de contact direct entre les graines et la peau de vos mains) quatre fois avec de l'eau déminéralisée ou de l'eau distillée. Cela permet d'éliminer les spores fongiques qui peuvent entraver la germination des graines.

Et en alternative :

> « Placer une serviette en papier sous un espace propre et laminaire. Parsemer les graines. Pulvérisez de l'isopropanol à 70 % sur la zone des graines disposées sur le papier. Laissez sécher jusqu'à ce que le papier / sopalin retrouve un aspect presque initial. Répétez l'opération deux ou trois fois. Vos graines sont maintenant prêtes pour votre test !

Ci-après, nous allons partager d'autres pratiques pour la désinfection / pseudo-stérilisation / stérilisation des graines.

## Éthanol, javel, détergent de labo

Stérilisation de surface des graines dans de l'éthanol à 96 % pendant 30 secondes puis dans une solution d'eau à 20 % (v/v) d'eau de javel (5 % NaOCl), ensuite complétée par un bain de 5 μL de détergent Tween-20 pendant 6-8 min[^note-sterilisation-1].

[^note-sterilisation-1]: <cite>Khaliluev MR, Bogoutdinova LR, Raldugina GN, Baranova EN</cite>. "*A Simple and Effective Bioassay Method Suitable to Comparative In Vitro Study of Tomato Salt Tolerance at Early Development Stages*. Methods Protoc. 2022 Jan 19;5(1):11. doi: 10.3390/mps5010011. PMID: 35200528; PMCID: PMC8877814.

## Chlorure de mercure

Stérilisation par trempage dans une solution de chlorure de mercure (0.1% solution) pendant 2 minutes et lavées cinq fois à l'eau distillée[^note-sterilisation-2].

[^note-sterilisation-2]: <cite>Nayak NI, Lakshmi P</cite>. "*Seed germination: An alternative to animal model to teach bioassay principles*. J Pharmacol Pharmacother. 2014 Jan;5(1):56-8. doi: 10.4103/0976-500X.124426. PMID: 24554913; PMCID: PMC3917169.

La production d'eau de javel, d'éthanol, d'autres détergent, ainsi que leur utilisation ont des impacts non-négligeable pour l'environnement et les conditions d'habitabilité de la Terre. Le chlorure de mercure est l'une des formes les plus toxiques du mercure et de plus il est très soluble dans l'eau. Il est souvent déconseillé, parfois interdit d'utilisation, selon les règles en vigueur dans votre zone géo-administrative.

## Par UV ?

Avec un autre angle d'*attaque*, nous testons actuellement au Biohackerspace Kaouenn-noz le traitement des graines avant bioessai par Ultra-Violet à l'aide d'un *simple* appareil de manucure pour pose de gel à ongle fonctionnant avec des LEDs et une alimentation électrique. Ce qui est loin de satisfaire à nos exigences environnementales.

## Discutons en !

Nous vous invitons à discuter de tout cela à nous par les modalités et les moyens qui vous conviendront. En attendant, nous conseillons de regarder le court métrage magnifique "Terra Mater - Mother Land", *"We Need Respect – We Demand Respect !" Elle se tient là, avec l’aplomb d’une déesse, environnée de déchets technologique*, de <cite>Kantarama Gahigiri</cite>, 2023.

![](/assets/images/bluelight-ebe714c0595ec3ad.jpg)

## Notes et références

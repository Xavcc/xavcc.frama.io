---
title: "4 ans sans téléphone portable − 2dne partie"
layout: post
date: 2020-04-28 14:40
image: /assets/images/Rorschach_blot.jpg
headerImage: true
tag:
- hsociety
- Rennes
category: blog
author: XavierCoadic
description: Voilà quelques retours
---

<figcaption class="caption">Forks on te road</figcaption>
<br>

Continuons dans le récit, après une [narration introductive](/sans-telephone/), d'un vécu sans cet appareil qui se fait tant répéter dans l'actualité : le téléphone portable.

Ce petit boitier qui en philosophie selon Borgman peut être vu comme un « device paradigm » pour articuler les objets et les pratiques usuelles d’un côté et les “appareils technologiques”[^1].

Voilà 4 années parcourues et travaillées sans téléphone mobile, je vais ici tenter de vous partager quelques retours d'expérience.

> **NDLR : Depuis 2013 je ne travaille que dans du « télétravail », tiers-lieux, « coworking »**

## Suppléments sociaux et les pertes

Je partais dans cette histoire depuis un constat de _sur-solicitations_ inutiles et abusives. J'espérai alors trouver dans ma démarche des _gains_ dans le rapport au temps et des _respirations_ dans les rapports à autrui ainsi hypothétiquement dessinés moins en tension.

Effectivement, des transformations sont advenues et elle commencèrent par moi, ma posture et mon comportement. 

Tout d'abord, nombreux rapports sociaux, personnels ou professionnels, trouvent un point « d'échange de coordonnées », ou de « je t'envoie un sms » qui apparait assez précocement dans la relation. Tentez d'être attentive, attentif, à ce phénomène lorsque vous rencontrez quelqu'un⋅e pour la première fois, ou déjà connu⋅e, ou après une relativement longue interruption d'échanges. Dans ma situation de « non-équipé de l'appareil _magique_ » je ne pouvais pas jouer ce jeu contractualisé − échanger coordonnées et moyens d'utiliser ces informations pour dialoguer et être joignable, ou joindre, selon des modalités plus ou moins floues de convivialité. Je ne pouvais pas non plus décemment proposer à la ou les personnes en face de moi, qui cherchent à nouer des rapports sociaux, que je souhaite prendre un temps conséquent et une attention élevée pour leur raconter mon histoire. Il m'a fallu puiser dans la pédagogie, dans une forme d'intelligence relationnelle, pour trouver les bons mots avec la bonne posture afin de donner les informations et justifications claires et brèves aux personnes pour appréhender une invitation à des rapports différents, tout en accompagnant cela de moyens différents pour mettre en suspens l'approche croyante du téléphone portable comme ultime solution.

Je me positionnais dès lors dans un rapport autre que l'immédiateté envers autrui. Je me situais dans une considération et gestion du temps et de nos rapports arrangés et entendus des un⋅e⋅s envers les autres. Ceci n'était pas une économie de la ressource temps ni une technologie[^2] de communication. C'était, et cela me semble toujours l'être aujourd'hui, une paire de main dont l'une est tendue vers autrui et l'autre remet l'emprise sur le temps.

En étant, préalablement à l'action, différent dans l'intention , j'étais plus profondemment ancré dans les rapports avec les personnes dans ces moments de monde. C'est une forme d'un petit supplément repris au [panopticon](https://fr.wikipedia.org/wiki/Panoptique).

J'aimerais écrire une *recette* fiable et claire de composition d'éléments pour aider d'autres à l'essayer. Je n'y arrive pas. Peut-être que je pourrais juste conseiller d'être simple, aimable, didactique, conscis⋅e, lorsque l'on tente de telles tambouilles.

Envisager le monde avec de nouvelles lunettes, ne serait-ce que des micro-mondes ; se placer en variété dans cette considération ; agir avec nuances dans cette configuration ; être autrement avec ses co-habitant⋅e⋅s ; tout cela amène à rencontrer des personnes nouvelles, hors d'anciennes bulles sociales. Au jour où j'avais brûlé une carte sim, je ne pensais absolument pas en arriver là.

Au travers de ce regard dans un vécu et dans l'expérience entreprise il y a aussi des déceptions et des dommages.

J'ai, par exemple, manqué plusieurs moments familiaux importants, car je ne pouvais être contacté *facilement* et *rapidement* selon les modalités de communication des membres de la famille. Le courriel et les messageries instantanées n'étant pas leur panacée. Ma place sociale dans la famille a changé. Pour des raisons comparables, des discussions avec des ami⋅e⋅s que j'estime se sont faites, ou sont nées, plus rares. J'ai appris à savourer ces échanges avec plus de cœur et de goût.

Socialement, je suis probablement devenu plus *exigeant* et je m'éloigne sans aucun problème de comportements que l'on pourrait dire toxiques; je repère (comme une antenne ?) les personnes qui se nourrissent de l'accaparement vorace du temps et de l'espace. Chaque jour un peu plus je *perds* la capacité à supporter, et donc à travailler, dans des organisations et avec des personnes techno-béates et ensevelies dans le _[new public management](https://fr.wikipedia.org/wiki/Nouvelle_gestion_publique)_ ou dans la _disruption_. Cela réduit considérablement ma position sociale dans l'univers « professionnel ». Dans cette *perte*, je compte en grande quantité les temps et rencontres libéré⋅e⋅s pour apprendre et pratiquer, instruire, former, enseigner et éduquer ; recevoir et donner,  pour la médiation.

## Pourquoi et comment j'ai persisté

j'y ai pris goûts et plaisir ; je suis opiniâtre.

Cependant, ces deux satisfactions ne suffisent pas à justifier et expliquer la durée de cette histoire.

Dans les fondements de cette persistance il y a mes fréquentations, hackerspaces et libristes en première instance, qui m'ont permis d'acquérir avant cette histoire, puis d'autant plus pendant, assez d'assises intellectuelles et techniques pour dépasser un enfermement, une captivité, d'un monde envisagé au travers la serrure d'un téléphone portable, pour m'enquérir du monde. Il y a aussi les disponibilités d'outils qui permettent les communications ailleurs et autrement qu'avec le téléphone portable.

D'autres parts, dans ces fils du tissu décrit ci-avant, j'ai conforté mon choix et ses déroulements dans des réalisations dont les conceptions et les mises en œuvre sont liées à cette vie sans téléphone portable − comme les [IndieCamp](https://movilab.org/wiki/IndieCamp), [les rencontres aux pieds nus](https://www.ouest-france.fr/bretagne/concarneau-29900/un-tour-de-france-pour-aider-les-inventeurs-4167570) pour exemple.

J'ai continué à vivre sans téléphone portable car cela me plaît et cela me permet de vivre autrement. Le *comment* étant lié aux passifs et aux capacités présentes de chacune des personnes qui pourraient être concernées par une démarche similaire, il serait étrange pour moi d'entreprendre l'exercice de description seul. Je m'attache au *pourquoi*, j'aimerais à rattacher à d'autres pour le *comment*.

## Les changements dans la vie personnelle 

Avant 2015 je n'étais ni très bavard, ni très pendu au téléphone portable. Après l'acte de m'en séparer ma place dans le cercle des rapports familiaux a évolué − comme j'ai pu commencer à l'évoquer auparavant. 

Je ne suis pas la personne que l'on contacte immédiatement en cas de *pépins*, du moins dans la famille. Bien évidemment, réduire à la seule absence d'un téléphone portable ce phénomène serait mal considérer les relations familiales et faire insultes aux membres de la famille concernée.

Dans un cercle en peu plus élargi, j'ai « manqué » des annonces de décès et des enterrements, puisque je ne pouvais pas être contacté selon les habitus et moyens des personnes informées. Cela ne changeant pas mon rapport à la mort, je dois avouer une influence et des questionnements sur la considération et la gestion du deuil collectivement en société d'hyper-information.

Je n'ai vécu que très peu de rendez-vous annulé « à la dernière minute » depuis 5 ans dans la vie personnelle. Ça parait anodin, c'est pourtant un grand soin dans les vécus et ressentis collectifs.

Il y a aujourd'hui très sûrement des anecdotes qui m'échappent et qui auraient puis accrocher lectrices et lecteurs. Je m'habitue et j'oublie peu à peu des petites choses qui, par un passé, auraient de lourdes impressions.

## Les impacts sur l'activité professionnelle

Mes activités professionnelles ne relèvent pas de la fonction de centrale d'appel téléphonique. J'ai aussi évoqué, dans la première partie puis ici, les possibilités d'outils numériques et de connexions réseaux sans téléphone portable. Les téléphones portables dits intelligents faisant mille choses de travers, et finalement servant peu à communiquer par la voix, mes pratiques dans les activités professionnelles sont certainement plus dégagées et plus nettes sans ce dispositif. Aussi, moins dérangé et plus distancié, j'en deviens mieux disponible et disposé pour des échanges dont les règles sont plus clairement connues et partagées. Je suis plus souvent libre et attentif.

Des impacts ? comme si j'avais reçu des projectiles sur une activité à laquelle je serais lié…

J'aimerais vous écrire que cela se résume à l'absence de publicité et démarchages abusifs et intrusifs ; ainsi que la tenue à l'éloignement d'organisation nocives. Pour la première part de ce résumé est en immense partie vrai. Pour la seconde…

La « fracture numérique » est la poudre favorite sortie du chapeau en France depuis quelques années. Le tour de passe-passe consistant à ce que des individus se placent elles et eux-mêmes sur un piédestal pour affubler une partie de la population de souffrance d'une fracture numérique. Cela relevant autant de la magie que d'un cocktail de mépris social, d'égo-trip, et d'inculture avec des glaçons d'incompétences.

Ainsi, mes gros *couacs* liés à l'absence de téléphone portable l'ont été avec des dites *start-up*, des boites de la high-tech, des incubateurs, des *responsables de programmes d'innovation*, y compris dans des collectivités territoriales.

Absence de maîtrise des outils et techniques, des méthodes et des considérations humaines ; absence de culture, tout cela masqué derrière la captativité formée dans l'habit du concept de « téléphone portable intelligent ».

Anecdote professionnelle. En 2018, j'avais travaillé sur et dans le [MediaLab Numérique en Commun](https://movilab.org/wiki/Medialab_NEC_2018). La phase précédente à cette réalisation était un appel dit « ouvert » à proposition pour l'évènement Numérique En Commun à laquelle j'avais contribué. La réponse par courriel *sublime* que j'avais alors reçue était de la teneur : « Votre proposition nous intéresse beaucoup et nous la trouvons pertinente. Nous avons cependant décidé de confier sa réalisation à `<autrui>` ». J'étais alors entré par « les tuyaux des eaux usées », par l'excuse du MediaLab, pour participer à cet évènement en tant que prestataire − comprenez là « hors des voies prévues ». Lors de ce « MédiaLab », une des personnes en responsabilité de pilotage de ce _raout_ national était venue me voir pour me dire : « Ah bonjour Xavier, c'est toi ? Tu devrais me laisser ton numéro de téléphone pour que nous discutions ». L'hypocrisie, au corps, chevillée sous un vêtement de *bienveillance*… L'incantation d'un téléphone portable dans les rapports sociaux, un habit et un masque des rapports sociaux, entre autres de dominination.

Je fais cours en école d'ingénieur⋅e⋅s depuis 2015, jamais des élèves, ni les enseignant⋅e⋅s chercheuses et chercheurs, ni l'administration des écoles, n'ont trouvé de problème ou de difficulté dans le fait établi que je ne possède pas de téléphone portable.

Je travaille à distance depuis 1 an avec l'ONG Tactical Tech basée à Berlin, je suis amené à réaliser des séances, rencontres « virtuelles » et communications dans plusieurs pays sur 3 continents. Là encore, aucun problème, l'absence de téléphone portable n'influe pas sur nos capacités à travailler ensemble.

Dans ce monde professionnel, les choses de cet univers, les vécus et les rapports avec lesquels nous nous articulons aux premiers cités, nous sont pas binaires, ne sont pas figés. Il m'a d'ailleurs été possible d'obtenir un [rendez-vous professionnel](/contribution-par-design) avec un vice-président de conseil de département et déléguée et chargée de projet au numérique sans passer par le téléphone.

### Les relations avec les administrations 

Beaucoup de personnes raillent « l'administration », beaucoup y compris des personnes de l'administration, pour ses lenteurs, sa rigidité, sa folie administrative. Beaucoup d'histoires existent pour relater des tragédies de courrier et communication téléphoniques. 

Je n'échappe pas aux turpitudes produites par notre administration française. Vous pourriez croire que l'absence de téléphone soit complique mes échanges, soit me tient à l'écart d'une partie des ennuis. Si je compare à ce que vivent des personnes de mon entourage, empirisme au doigt mouillé, je peux affirmer que l'administration n'a cure (absolument aucune que je possède ou non un téléphone − c'est juste une ligne vide ou remplie dans l'ether de la masse de leur formulaire. Je dirais, en accord avec ce que j'ai tenté d'expliquer en début d'article, que j'aborde avec plus de distance et de philosophie les possibilités de rapports et de communication avec les administrations publiques. J'évite ainsi, en quelque sorte, une charge d'énervement pour moi et pour les personnes de ces administrations. Je trouve des chemins, presque toujours éphémères, de dialogue et d'arrangements communicationnels − entre le rendez-vous présentiel in situ et le courriel − pour dénouer des rapports obligés dans des situations compliquées.

D'un certain abord, tenter de composer avec l'administration française s'est s'évertuer à manipuler du temps. Le problème plus profond est que l'accès au temps est fortement intriqué avec notre classe sociale.

Il me restera encore un peu à vous écrire pour raconter la suite et fin de cette histoire. Avec des morceaux sur possibles *exclusions* subies ; les outils, les techniques et méthodes dans cette histoire ; pénibilité et fluidité en général dans la vie et enfin le prix de la chandelle. Cette troisième partie pourrait vous étonner `(>_0)`

> **NDLR : Et l'instant où je termine ces lignes, je reçois un message de Suisse d'un ami qui me remercie de prendre soin de donner et de demander des nouvelles**

### Rermerciements

+ Mère Teresa
+ Themartylake
+ Dad Daniel

### Notes et références

[^1]: Gauthier Roussilhe « Une erreur de tech » <https://gauthierroussilhe.com/fr/posts/une-erreur-de-tech>

[^2]: Technologie comme étude de la cutlure et contrôle des comportements (Lemonnier, Mauss…)
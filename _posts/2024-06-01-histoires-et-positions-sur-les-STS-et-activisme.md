---
title: "Ajouts au travail sur « Histoires et positions sur les STS et l’activisme »"
layout: post
date: 2024-06-01 10:00
image: /assets/images/rac-2024.png
headerImage: true
tag:
- hsociety
- Notes
- Anthropologie
category: blog
author: XavierCoadic
description: Si ce n’est pas nous, alors qui ? Si ce n’est pas maintenant, ce sera quand ?
---

La traduction, de mon fait, de « [Si ce n’est pas nous, alors qui ? Si ce n’est pas maintenant, ce sera quand ? Histoires et positions sur les STS et l’activisme](https://journals.openedition.org/rac/32912) » est publiée dans la Revue d’Anthropologie des Connaissances, dossier de juin 2024 : Comment passer à l’action ? Préparer, mettre en place, répéter, pour que les choses adviennent.

C’est un plaisir pour moi et cela représente des mois de travaux, presque une année complète. Je vous partage ici des éléments complémentaires qui, j’espère, aideront à d’autres formes de compréhensions.

Cet article est une traduction *libre* en d’une publication parue dans l’European Association for the Study of Science and Technology (EASST) en juillet 2023 (V. 421). Cette traduction vise à nourrir la réflexion sur les pratiques d’auto-critique dans les STS et d’ouvrir la discussion tant sur les héritages et transmissions au prisme pris par Sarah Rose Bieszczad, Guus Dix, Jorrit Smit que sur les privilèges liés aux langues sources et aux langues cibles dans lesquelles sont prononcées ces critiques.

Le désir de faire circuler la publication initiale continue d’être exprimé, y compris au-delà des entités individuées citées précédemment. Celui-ci soutient la volonté de discussions sur les axes déployés dans la publication en question, axes qui contiennent des thèmes dont la présence dans les discours et dans les actualités se renforcent chaque jour. La traduction avec une tentative de rigueur de la lettre (bien que *Traduttore, traditore*) n’est pas l’activité la plus consommatrice de temps ni d’énergie dans le processus ici exposé avec son contexte spécifique. Les discussions contextuelles, les besoins d’accords, les alignements sur des règles communes et sur des règles externes, le respect de volontés exprimées, l’intégration dans des agendas pré-existants, la soumission à des normes en vigueur, sont tous autant de facteurs qui participent d’un rythme et d’une temporalité qui entrent en frictions avec le « si ce n’est pas maintenant, ce sera quand ? » de la publication traduite, et de l’intention qui lui est inscrite.

Des chercheuses et chercheurs en STS publient leurs interrogations critiques dans leur champ avec la pratique de « l’appel ». Cet acte est une continuation d’exercices présents dans les STS depuis leurs prémices. D’autres en histoire traitent des relations entre « crises » et institutions, les “Disasters Studies” par l’anthropologie ont plus de 40 ans. Ces courants et l’exercice des sciences qui les bâtissent se font dans le contexte du [en] « dessous de 1,5 degré de réchauffement de la planète ».

J’ai traduit en *pratique amatrice* cette publication de l’anglais vers le français sur mon temps libre. Les autrices et auteurs de cette publication ont participé de la traduction, notamment par annotations et commentaires en itération sur différentes versions.
Cette contribution bénévole, effectuée en collaboration asynchrone à distance, entre nous s’est déroulée entre août 2023 et mai 2024. Pour ma part le travail nécessaire entourant cette traduction et publication a été facilité par des échanges dont je nomme les personnes et entités en remerciements ci-après.

## Remerciements

Je remercie Sarah Rose Bieszczad, Guus Dix, Jorrit Smit, Émilie Picton, les salarié⋅es d’Association for Progressive Communication, Petites Singularités ASBL, Laura Ranca (Tactical Tech), Yves Picard (Université Rennes 2), Nicolas Sauret (Université Paris VIII), Renaud Debailly (GEMASS, Sorbonne Université), Dominique Vinck (UNIL), Éléonore Legeai pour relecture.



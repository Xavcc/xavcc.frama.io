---
title: "Expérimentation : typer les documentations des tiers-lieux libres et en source ouverte sur un wiki"
layout: post
date: 2020-12-14 11:40
image: /assets/images/classifiaction.jpg
headerImage: true
tag:
- hsociety
- Tiers-lieux
category: blog
author: XavierCoadic
description: "Établir une corrélation systématique entre : d’une part les activités symboliques (idéologie, politique, culture) ; et d’autre part les formes d’organisation, les systèmes d’autorité induits par tel ou tel mode de production, d’archivage et de transmission de l’information. Voilà le but"
---

<figcaption class="caption">Immage d'illustration d'en-tête : "Figure 4: One of the two MPTs resulted from MP analysis of the combined (nrITS+plastid accD-psaI, psbJ-petA, ycf6-psbM-trnD) sequences displayed as a phylogram (A) and as a cladogram (B)"      Sramkó G, Molnár V. A, Tóth JP, Laczkó L, Kalinka A, Horváth O, Skuza L, Lukács BA, Popiela A. 2016. Molecular phylogenetics, seed morphometrics, chromosome number evolution and systematics of European Elatine L. (Elatinaceae) species. PeerJ 4:e2800 https://doi.org/10.7717/peerj.2800 − Creative Commons BY 4.0</figcaption>
<br>

Cette expérimentation récente en cours prend racine dans 10 années de contributions, actions, réflexions, au sein des Tiers-Lieux Libres et Open Source (*tilios*) francophones[^1]. Elle commença à se formaliser en 2017 lors de « [L’expérience Tiers-Lieux – Fork The World](https://forktheworld.eu/) » à la biennale Internationale du Design de Saint-Étienne[^3].

À partir de septembre 2018 émerge l'envie d'explorer « La documentation comme technique de résistance, le wiki comme objet de ces résistances ? » au travers de plusieurs wiki identifiés et leurs communautés de contribution.

Fin 2018, à l'invitation de la designeure, chercheuse, enseignante, Sylvia Fredriksson un essai textuel réflexif est lancé en travail d'écriture, avec l'appui de [Nicolas Sauret](https://nicolassauret.net/about/) (alors doctorant sur les dispositifs numériques d’écriture et de lecture dans l’élaboration et la circulation des savoirs )[^4]. Cette effort aboutira dans la revue [Sens-Public](https://sens-public.org/), de l'Université de Montréal, dans un essai intitulé « [ De l’hypothèse de la documentation comme technique de résistance et du wiki comme objet de ces résistances](https://sens-public.org/articles/1375/) » (Nicolas Belett Vigneron ; Émilie Picton Sébastien Beyou ; Xavier Coadic), publié en 2019[^5]

Dans cet essai nous avions essayé de dégager 4 « *grands* » types de documentations dans les wiki :

+ **La forme tutorielle** : qui est axée sur l’apprentissage, permet au nouvel arrivant de commencer, telle une leçon. Elle est similaire à l’acte d’apprendre à planter des légumes ou d’apprendre à faire la cuisine à un individu.

+ **La forme de guide pratique** : qui est axée sur les buts, montre comment résoudre un problème spécifique, tout comme une série d’étapes. Elle est semblable à l’acte de cultiver des légumes ou à une recette dans un livre de cuisine.

+ **La forme d’explication** : qui est axée sur la compréhension, explique, fournit des renseignements généraux et le contexte. Elle est comparable à un article sur l’histoire sociale de la tomate ou l’histoire sociale culinaire.

+ **La forme d’un guide de référence** : qui est axé sur l’information, décrit la conception réalisée. Elle vise l’exactitude, donc la vérification et l’ajout de sources, et recherche à être complète. Elle est semblable à un article d’encyclopédie de référence.


**Aujourd'hui, avec la contingence des efforts avec [Les résidences de la contribution sur Movilab](https://movilab.org/wiki/Compte_rendu_synth%C3%A9tique_de_la_r%C3%A9sidence_contributive_Movilab_du_16-17_mars_2020), nous tentons de mettre en pratique ce typage sur des pages wiki de Tiers-Lieux libres et en sources ouvertes**

Nous lançons cette expérimentation sur 2 wiki : Movilab (L'encyclopédie des tilios) et [Kouenn-noz](https://wiki.kaouenn-noz.fr/) (wiki d'un biohackerspace en Bretagne)

## Pourquoi typer ?

### Qu'est qu'un type ?

> « PHILOS. Moule, modèle idéal qui détermine la forme d'une série d'objets qui en dérivent; concept abstrait, où s'exprime l'essence d'une chose, considéré comme un moule, un modèle (d'apr. Lal. 1968) » *[CNRTL](https://www.cnrtl.fr/definition/type)*

> « Modèle idéal, défini par un ensemble de traits, de caractères essentiels.

> Ensemble de traits correspondant à une sorte de modèle générique

> Ensemble de caractères physiques qui distinguent des groupes

> Catégorie formée par un ensemble de propriétés, de traits généraux » *[Larousse](https://www.larousse.fr/dictionnaires/francais/type/80365)*

### Origine de la démarche et contexte

En décembre 2020, [Movilab](https://movilab.org/wiki/Accueil) contient plus de 8 200 pages wiki, très variées dans leurs qualités, leurs types, leurs formes, leurs catégories ; et aussi variées sur le « véhicule matériel de l'information » (Régis Debray, 1994) que ces pages entreprennent.

Dans Movilab l'usage hérité des [catégorisations](https://fr.wikipedia.org/wiki/Aide:Syntaxe#Liens_vers_des_cat%C3%A9gories,_des_images,_des_mod%C3%A8les%E2%80%A6) dans le logiciel [Mediawiki](https://www.mediawiki.org/wiki/MediaWiki/fr), et la pratique largement utilisée par Wikipedia, est une habitude courante depuis des années pour créer des répertoires de pages spécifiques en fonction des catégories attribuées par les contributrices et contributeurs. 

Cette [structure des réseaux de catégorie](https://fr.wikipedia.org/wiki/Aide:Cat%C3%A9gorie#Structure_du_r%C3%A9seau_de_cat%C3%A9gories) est explicitement liée symboliquement à l'arborescence et [classification scientifique des espèces](https://fr.wikipedia.org/wiki/Classification_scientifique_des_esp%C3%A8ces)

![](/assets/images/1155px-Catgraph_Mammifère_préhistorique.svg.png)
<figcaption class="caption"> Source : page wikipedia « Aide:Catégorie / Structure du réseau de catégories »</figcaption>
<br>

Le problème de la diffusion des concepts documentés dans un wiki, dans l’état actuel proposé de cette documentation, nous intéresse dans un regard soucieux à la fois d'accueil de la lecture / contribution et de la qualité scientifique amatrice[^7] de ce qui est produit et mis en partage avec movilab ([avis du conseil scientifique 2015](https://movilab.org/wiki/Fichier:Avis_CS_final_Movilab.pdf)).

Pour citer Sylvie Fayet-Scribe au sujet de la médiologie chez Régis Debray, dans « Pour une nouvelle économie du savoir ». Dans Solaris, nº 1, Presses Universitaires de Rennes, 1994

> « Établir une corrélation systématique entre : d’une part les activités symboliques (idéologie, politique, culture) ; et d’autre part les formes d’organisation, les systèmes d’autorité induits par tel ou tel mode de production, d’archivage et de transmission de l’information. Voilà le but »

Nous cherchons alors dans notre démarche expérimentale à :

+ contribuer à l'amélioration du classement des ressources de movilab, avec leur indexation et lisibilité
+ contribuer à une meilleure considération de la médiologie des contenues de movilab, et donc des tiers-lieux libres et open source.

![](/assets/images/525px-Taxonomic_hierarchy.svg.png)

<figcaption class="caption"> "L'espèce est l'unité de base de la hiérarchie du vivant." Source : page wikipedia « Classification scientifique des espèces »</figcaption>
<br>

### Constat 

Nous constatons très régulièrement qu'au travers de wiki qui ne sont pas uniquement composés de page typée « guide de référence (encyclopédique) », avec des centaines d'heures d'entretien et d'ateliers avec des personnes qui ne contribuent pas aux wikis, un sentiment de perdition dans un labyrinthe de ressources (un peu comme dans un hyper marché qui est une allégorie qui est souvent retournée).

Ces pages web (page wiki) sur lesquelles nous écrivons sont des murs par lesquels nous espérons transmettre des informations, parfois de la connaissance ; pour que hypothétiquement une personne curieuse les transforme en savoirs. 

Peut-être devrions-nous mieux les considérer comme des portes (logique[^6]   ou pseu-dologique) ; et alors afficher et offrir les clés (les types par exemple) directement sur ces portes pour les franchir. Allant aussi jusqu'à partager les méthodes, les outils, étapes, les recettes, pour fabriquer soi-même des clés.

Pour le côté labyrinthe, nous essayons de poser des étiquettes sur ces portes, toujours espérant que celles-ci servent de repères pour s'orienter.

> « il m'arrive régulièrement de dire que je ne comprends pas tout ce qui est écrit. Non pas parce que ce n'est pas clair, mais juste que je n'ai pas les clés pour comprendre. Mais je sais que ce Wiki a été fait pour les gens qui sont comme moi. 
C'est donc à mes yeux très important de te donner des retours d'expérience utilisateur. C'est fastoche et ça me donne vachement moins de boulot qu'à toi Et c'est exactement ce que je ressens en naviguant sur le Web. Et parfois, j'ai le droit à un fauteuil accueillant et quelqu'un avec qui discuter de ces serrures que je ne sais pas ouvrir. Et on cherche ensemble la clé qui correspond le mieux à la forme de ma main, afin que ce soit plus facile d'ouvrir ces serrures. Et on s'amuse ensemble de ces différences de mains qui compliquent le choix des clés. Moments d'entraide, de complicité. » *Une Internaute pseudonyme*.

## Comment nous expérimentons ce typage ?

### Dans Movilab

*Movilab est un espace de documentation collaborative basé sur le logiciel Mediawiki*

Nous avons opté pour un choix d'indication graphique placé dans le haut des pages wiki juste à la suite du titre de la page. Par exemple, la page « [Low-Tech Camp](https://movilab.org/wiki/Low-tech_Camp) » dans movilab

![](/assets/images/movilab-typologie.png)

Cet espace inséré à  l'aide de la balise `{Explication}` dans le wikicode de la page est conçu dans un modèle sous licence libre disponible et modifiable [ici](https://movilab.org/wiki/Mod%C3%A8le:Explication)

```
[[Fichier:Ampoule.png|middle|link=Accueil]] '''Cette page est une documentation explicative''' <br>
 Vous pouvez partager vos connaissances en l’améliorant ([[Comment contribuer sur ce wiki ?|comment ?]]).<br>
 axée sur la compréhension, explique, fournit des renseignements généraux et le contexte. 
 Elle est comparable à un article sur l’histoire sociale de la tomate ou l’histoire sociale culinaire. 
 Exemple : [https://wiki.lescommuns.org/wiki/Accueil Wiki des communs] (documentation empirique et théorique) <br>
 '''Répertoire''': Toutes les pages de ce wiki de '''type [https://movilab.org/wiki/Cat%C3%A9gorie:Explication Explication]'''<br>
 '''Support''' : [[Portail:Documentation et codes sources|Le portail dédié à la documentation et aux codes sources]]

[[Catégorie:Explication]]
```
La balise `[[Catégorie:Explication]]` permet de générer automatiquement un répertoire de toutes les pages typée en « [Explication](https://movilab.org/wiki/Cat%C3%A9gorie:Explication) » :

![](/assets/images/movilab-catégorie-explication.png)

Nous avons également pris soin d'expliciter la démarche et d'indiquer la démarche à suivre dans le wiki concerné dans « [Indiquer le type de documentation contenu sur une page movilab](https://movilab.org/wiki/Comment_contribuer_sur_ce_wiki_%3F#Indiquer_le_type_de_documentation_contenu_sur_une_page_movilab)

![](/assets/images/movilab-typologie-2.png)


### Chez Kaouenn-noz

*Le wiki de Kaouenn-noz est basé sur le logiciel [Dokuwiki](https://www.dokuwiki.org/start?id=fr%3Adokuwiki&go=%E2%86%92)*

Nous avons conçu un modèle de page wiki insérable dans d'autres pages : <https://wiki.kaouenn-noz.fr/modele:type_tutorielle>

Ce modèle inséré ensuite dans d'autres page sert à indiquer le type de documentation que la personne lit, exemple
<https://wiki.kaouenn-noz.fr/hors_les_murs:hack2eaux:pieges_a_microplastiques>

![](/assets/images/kaouenn-noz-typage.png)

Nous décrit et documenté la démarche, notamment les différents modules complémentaires installés et utilisés
<https://wiki.kaouenn-noz.fr/config:doc#typage_des_documentation>

Un répertoire ([exemple](https://wiki.kaouenn-noz.fr/tag:prototype?do=showtag&tag=Prototype)) pour lister les pages en fonction d'étiquettes attribuées existe également :

![](/assets/images/kaouenn-noz-etiquette.png)


> « Et voilà le wiki wikifié. Youhou ! » *Un goéland dans Internet*

Ensuite un répertoire des types sera créé dans les jours à venir. Nous serons alors en effort de transition vers une « Typologie de nos documentations » − « *démarche méthodique consistant à définir ou étudier un ensemble de types, afin de faciliter l'analyse, la classification et l'étude de réalités complexes* » Wikipedia

> « **Que veut dire "Médio" dans "logie" ?**

> C'est l'ensemble en devenir des corps intermédiaires s'intercalant entre une production de sens et une production d'événements. Tout ce qui permet à un message de prendre effet, ou d'avoir une efficacité. Prendre effet, c'est prendre corps. À tous les sens du mot "corps". » *Compte-rendu de « Regis Debray. Médiologie » : Sylvie Fayet-Scribe "Pour une nouvelle économie du savoir". In Solaris, nº 1, Presses Universitaires de Rennes, 1994*

### Comment cela est mis en place

Dans les deux exemples présentés ici et toujours encours d'expérimentation, nous avons utilisé plusieurs modules complémentaires (des logiciels complémentaires aux ligiciels mediwiki et dokuwiki installés sur nos serveurs) pour créer puis pour embarque des « modèles » dans des pages du wiki, et ensuite générer des répertoires. Cela correspond à notre volonté de travailler « en porte logique » et cela crée aussi une forme en « matriochkas ».

Nous avons aussi pris « inspiration » sur des pratiques dans d'autres professions et métiers[^8].

## Ce qui est largement améliorable aujourd'hui

**Trois points techniques critiques** :

+ La dépendance à de nombreux modules externes rend notre expérimentation sensible et fragile à tous les risques de défaut de ces dépendances (maintient, développement, sécurité, etc).
+ La compostion en « matriochkas » crée une sorte de [boîte obscure](/defaire/) qui freine la découverte et les apprentissage
+ La tentation de produire des « modèles » sur-ingénierisés ([exemple](https://movilab.org/wiki/Mod%C3%A8le:Tiers-Lieux)) qui rendent la contribution plus fastidieuse

**Deux points d'alerte** :

+ L'absence d'archivages des liens externes dans les wiki (URL de source par exemple) complique la visée pérenne et la fiabibilité de la documentation, par rebond toute tentative de Classement des pages wiki de documentation
+ Un wiki est un espace de documentation collaborative que nous mettons en place et entretenons par des moteurs de wiki (dokuwiki ou mediawiki par exemple). Ils ne sont pas des CMS pour des blogs

**Amélioration esthétique** :

+ Toute l'esthétique des « boites » graphiques que nous créent et utilisées sont « facilement » modifiables, et elles en ont bien besoin
+ Le choix de « boites graphiques » pourra être remise en cause face à des options pertinante de contribution au typage des pages wiki avec intégration dans le Classement de ces pages

**Amélioration de Classification** :

+ **Pour Movilab**
  + avoir un répertoire propre au `Type`. Sortir les répertoires de `Type` de la confusion actuelle avec les `Catégories` ([exemple de confusion](https://movilab.org/wiki/Cat%C3%A9gorie:Guide_de_R%C3%A9f%C3%A9rence))
  + Intégrer les `Types` à l'arborescence sémantique de Movilab
+ **Pour Kaouenn-noz** :
  + Créer des répertoires des `types`
  + Retravailler l'esthétique des « boîtes » de `types`

## Ce que nous espérons pour demain

+ Plus de collaborations avec des personnes intéressées ou curieuses de la démarche
+ Trouver ailleurs des démarches comparables


## Notes et références

[^1]: [Historique de Movilab](https://movilab.org/wiki/Historique_de_Movilab) (2000 -2020)

[^2]: [commissariat](https://www.sylviafredriksson.net/2017/03/09/lexperience-tiers-lieux-fork-the-world-biennale-internationale-design-2017/) L’Expérience Tiers-Lieux. Fork The World. Biennale Internationale Design 2017 − Sylvia Fredriksson

[^3]: [GitBook Fork The World](https://world-trust-foundation.gitbook.io/fork-the-world/)

[^4]: Thèse [De la revue au collectif : la conversation comme dispositif d’éditorialisation des communautés savantes en lettres et sciences humaines](https://these.nicolassauret.net/)

[^5]: Nous suggérons en écho à notre essai le Texte de Félix Tréguer [Pouvoir et résistance dans l'espace public : une contre-histoire d'Internet (XVe -XXIe siècle)](https://halshs.archives-ouvertes.fr/tel-01631122), novembre 2019, CRH - Centre de Recherches Historiques.

[^6]: Fonctionnement d'un ordinateur/Les portes logiques <https://fr.wikibooks.org/wiki/Fonctionnement_d%27un_ordinateur/Les_portes_logiques>

[^7]: L, Laurence Allard, « L'amateur: une figure de la modernité esthétique », 1999 <https://www.persee.fr/doc/comm_0588-8018_1999_num_68_1_2028>

[^8]: Exemple récent : [Comment j'organise mes notes professionnelles avec Dokuwiki et ses plugins](https://blog.chezjln.xyz/comment-jorganise-mes-notes-professionnelles-avec-dokuwiki-et-ses-plugins), Jln, 31 juillet 2020

---
title: "[Podcsat]: Qu'est-ce qu'une catastrophe ? Sandrine Revet, Recherche en Cours (2006)"
layout: post
date: 2022-08-09 07:40
image: /assets/images/Caribe_y_Caraballeda_Enero_2000_025.jpg
headerImage: true
tag:
- hsociety
- podcast
- Lmdlt
- Catastrophologie
- Anthropologie
category: blog
author: XavierCoadic
description: "Qu'est-ce que la santé cotemunautaire"
---

<figcaption class="caption">Image d'en-tête “Caribe y Caraballeda ado Vargas - Venezuela un mes despues de los deslaves de diciembre de 1999 (Enero 2000)”, Veronidae − CC BY-SA 3.0 − <a href="https://commons.wikimedia.org/wiki/Vargas_in_January_2000#/media/File:Caribe_y_Caraballeda_Enero_2000_025.jpg">Wikimedia Commons</a></figcaption>
<br>

Rédiffusion d'une interview de Sandrine Revet, anthropologue. Ses premiers travaux se sont orientés autour de l’anthropologie des catastrophes, avec une thèse sur les coulées de boue de 1999 au Venezuela (Anthropologie d’une catastrophe, Presses de la Sorbonne nouvelle, 2007). 
<marginnote>Cette republication postée en juin 2022 fait partie d'un travail plus large sur la « catastrophe » avec des notes de lecture disponibles sur ce blog</marginnote>

Cette émission de 2006 a été réalisé par [Recherche En Cours](https://www.rechercheencours.fr/) [`REC`], en direct sur Aligre FM 93.1 en Ile de France depuis 2005, et publiée sous licence Creative Commons BY NC.

<iframe width="100%" height="150" scrolling="no" frameborder="no" src="https://open.audio/front/embed.html?&amp;type=track&amp;id=197443"></iframe>

> « Sandrine Revet est anthropologue, chargée de recherche au Centre d’Etudes et de Recherches Internationales (CERI) de Sciences-Po et chercheure associée au Groupe de Sociologie Politique et Morale (EHESS). Ses recherches actuelles portent sur le monde international de gestion des catastrophes naturelles, de ses modes de légitimation à ses appuis épistémiques en passant par l’ethnographie des dispositifs et des acteurs qui le constituent » <cite><a href="https://journals.openedition.org/amades/1307">La rupture de l’événement. Une anthropologie des catastrophes</a></cite>, <u>Baptiste Moutaud</u>
---
title: "Notes de cours : Réfugié⋅es au 21e siècle -- Un monde de réfugié⋅es et 2 questions sur les camps"
layout: post
date: 2024-04-10 19:30
image: /assets/images/Réfugiés_serbes_près_des_tentes.JPEG
headerImage: true
tag:
- hsociety
- Notes
- Anthropologie
category: blog
author: XavierCoadic
description:
---

<figcaption class="caption"> Réfugié⋅es serbes près des tentes qui les abritent -- [photographie de presse] / Agence Meurisse. 1915. Source gallica.bnf.fr / Bibliothèque nationale de France.  Notice de recueil : http://catalogue.bnf.fr/ark:/12148/cb40499420x</figcaption>

---

Je commence le MOOC "Refugees in the 21st Century" de l’University of London. Les intervenant⋅es sont :
+ Professor David Cantor,the Director of the Refugee Law Initiative at the University of London
+ Dr. Sarah Singer, Senior Lecturer in Refugee Law at the Refugee Law initiative and Program Director of our online master’s program in Refugee Protection and Forced Migration Studies

Je partage mes annotations et références dans un espace dédié d’un groupe ouvert Zotero [ici](https://www.zotero.org/groups/4989006/catastrophologie/collections/S33VZF5B).

J’amorce ici dans ce billet blog un partage de travail. Pour cette première note il s’agit de la semaine 1 intitulée « Un monde de réfugiés » et des deux questions posées dans cette semaine concernant les camps.

**TOC**

+ [Quelques chiffres](#quelques-chiffres)
+ [Pourquoi certains préfèrent accueillir les réfugié⋅es dans des camps](#pourquoi-certains-pays-préfèrent-accueillir-les-réfugié⋅es-dans-des-camps)
+ [Quels pourraient être les inconvénients de cette approche tant pour les communautés d’accueil que pour les réfugié⋅es](#quels-pourraient-être-les-inconvénients-de-cette-approche-tant-pour-les-communautés-daccueil-que-pour-les-réfugié⋅es-elleux-mêmes)
+ [Références bibliographiques & notes](#references-bibliographiques--notes)


## Quelques chiffres

_NDLR: je rappelle que vous devez toujours poser a minima les questions suivantes : **D’où viennent les données ? Qui les collecte et les fournit ? Qui finance collecte et fourniture de ces données ?**_

**Le nombre de personnes déplacées par des persécutions, souffrances et conflits 2012-2022**

Source : HCR/UNRWA/IDMC HCR/UNRWA/IDMC (*in* “[Conflict and crisis in an interconnected world
Global displacement trends](https://stories.nrc.no/conflict-and-crisis-in-an-interconnected-world/)”)

- 2012 : 42,7 millions
    
- 2013 : 51,3 millions
    
- 2014 : 59,2 millions
    
- 2015 : 65,1 millions
    
- 2016 : 65,5 millions
    
- 2017 : 68,5 millions
    
- 2018 : 70,8 millions
    
- 2019 : 19,5 millions
    
- 2020 : 82,3 millions
    
- 2021 : 89,3 millions
    
- 2022 : 108,4 millions

La plupart des personnes qui fuient leur pays d’origine seront accueillies dans les pays limitrophes et voisins.

**Le 10 déplacements de populations les plus négligés dans le monde en 2022**

1. Burkina Faso
2. DR Congo
3. Colombia
4. Sudan
5. Venezuela 
6. Burundi 
7. Mali
8. Cameroon 
9. El Salvador  
10. Ethiopia

**Vue d’ensemble**

108,4 millions de personnes déplacées au total
+ 62,5 millions sont déplacées dans leur propre pays
+ 45,5 millions sont des réfugiés dans d’autres pays  

+ **L’Afrique en chiffres**
  + 37,1 millions de personnes déplacées au total
    - 28,1 millions sont déplacées dans leur propre pays
    - 9 millions sont des réfugiées dans d’autres pays

+ **Moyen-Orient et Asie**
  + 41,7 millions de personnes déplacées au total
    - 19,9 millions sont déplacées dans leur propre pays
    - 2,8 sont des réfugiés dans d’autres pays

+ **Amérique**
  + 14,5 millions de personnes sont déplacées au total
    - 5,9 millions sont déplacées à l’intérieur de leur propre pays
    - 8,6 millions sont des réfugiées dans d’autres pays

+ **Europe**
  + 14,8 millions de personnes sont déplacées au total
    - 8,5 millions sont déplacés dans leur propre pays
    - 6,3 millions sont des réfugiés dans d’autres pays


## Les questions posées en fin de semaine

### Pourquoi certains pays préfèrent accueillir les réfugié⋅es dans des camps ?

Tous les pays ne font pas le choix du « camp » comme dispositif d'accueil des personnes déplacées.

Le “camp” est un dispositif ancré dans l’Histoire. Pourtant,  les historiennes et historiens ont longtemps négligé d’inclure les réfugiés dans leurs écrits de l’Histoire générale nationale, régionale ou mondiale (D. Stone (2018)). Le camp peut être « improvisé », par un regroupement d’individus déplacés ; ou installé et organisé par des ONGs, institutions, consortium de l’action humanitaire ; ou par un État et ses administrations ; ou un mix entre ces 3 orientations de conception.

Les camps ont en conséquence une charge historique et politique, comme “‘Gulag Archipelago’ of Soviet labour camps” (K. Hewitt, 1983, *p. 13*), *The colonial legacy* […] [In Belize] *Later in the 18th century small isolated camps were developed in the interior* (Jerry A. Hall (1983) *p. 145*, *ibid*). Ce que rappellent Ben White, Simone Haysom and Eleanor Davey (2013) dans “For good historical reasons many Syrians approach camp life with trepidation”.

L’ancien Empire d’Égypte aurait utilisé la forme camp pour des populations déplacées il y a 4 000 ans (W. M. Patton (1903)).
L’empire Romain utilisaient la forme camp pour ces légions dans les territoires occupés, l’ONU utilise des formes camps différentes pour ses casques bleus et pour les réfugiés.

Jusqu’au 19ème siècle ils étaient possible de circuler et traverser des frontières sans documents d’identités.
En Norvège, le gouvernement a lancé des programmes d’installation forcée pour les éleveurs de rennes des Samis aux 19e et 20e siècles, et les dits « nomades » ont été rassemblés de force, placés dans des camps de redressement et leurs enfants retirés de force à leurs familles et placés dans des familles d’accueil norvégiennes (Engebrigtsen, A. I. (2017)).

La France a été figure d’innovation des formes camps depuis la fin du XIXe siècle notamment sur les populations dites nomades et dites tziganes (Hacker (2023)) jusqu’à aujourd’hui où plusieurs formes de camps sont en place du fait de a France sur son territoire et aussi sur le territoire de pays voisins par le fait de la France (Le Cour Grandmaison (2007)).
L’Empire britannique avec son Commonwealth avait systématisé les camps de réfugié⋅es en tant que réponse militaire et humanitaire aux catastrophes dites « naturelles » ou désastres causées directement du fait activités humaines (Mans & Panayi (2020))

C’est après la première mondiale que des agences inter-étatiques (Société des Nations), avec le Norvégien Nansen (1861-1930) comme Haut-Commissaire aux réfugiés russes, et avec l’augmentation du nombre de personnes déplacés en « masse », que des états *inquiets* et des agences passent de « l’installation de camps ou alors de façon très sommaire et provisoire pour des durées et des populations limitées » (où les personnes sont « sont rapidement réinstallées ») à des États « débordés » et des agences qui vont systématiser et massifier les camps *d’accueil* (Ryfman (2015)).

Le « camp » est un choix politique avec ses stratégies dédiées et son administration pleine et entière (parfois déléguée à des ONG) (Atlani-Duault (2009))

> « Les camps ne sont pas des zones de non-droit, mais bien au contraire des institutions juridiques. Ces instruments d’une technique particulière visent à séparer les nationaux des étrangers » <cite>Gilles Lhuillier</cite> (Essai de définition : l’Institution juridique des camps, *p. 16*).
> 
> […]
> 
> ces lieux et zones sont inter-reliées, les personnes y sont déplacées par les autorités, et forment ainsi un réseau maillé, « zone abstraite et concrète, juridique et géographique » (<cite>Gilles Lhuillier</cite>, *p. 28*) in *Le retour des camps ? Sangatte, Lampedusa, Guantanamo…*, <cite>Olivier Le Cour Grandmaison</cite>, <cite>Gilles Lhuillier</cite> et <cite>Jérome Valluy</cite>. 2007.* ISBN-10:  2-7467-0926-0

La catégorie « réfugié⋅es » est connotée et renvoie à une identité d’étranger, le réfugié ayant été, par définition, forcé de quitter sa terre d’origine (Foxen (2007), *ibid*). Des études ethnographiques anciennes sur les migrations économiques urbaines provoquées par la colonisation et la modernisation de l’Afrique et de l’Amérique latine (Gluckman (1961) ; Turner (1957) ; Padilla (1958)) relèvent aussi cela.

Des « populations autonomes et des communautés d’accueil » (White, Haysom and Davey (2013)). peuvent aussi faire le choix de proposer d’autres forme d’hospitalité que celles de formes camps. Cependant, si « Dans les décennies de 1860 à 1914, l’Empire ottoman a fourni des terres, des allégements fiscaux et une aide au développement agricole pour réinstaller des millions de réfugiés musulmans du Caucase et des Balkans. » (*ibid*) le cas du tremblement de terre de 1976 dans l’est de la Turquie, qui a détruit la plupart des habitats et autres abris de la région. Le tremblement de terre [Çaldıran–Muradiye](https://en.wikipedia.org/wiki/1976_%C3%87ald%C4%B1ran%E2%80%93Muradiye_earthquake), à la frontière de l’Iran et de la Turquie, à la suite duquel les responsables gouvernementaux turcs se sont efforcés de reloger les survivant⋅es des zones rurales vers les zones urbaines en prévision des conditions hivernales difficiles. C’est aussi en Turquie un moment de politique avec des choix effectifs liés à l’industrialisation qui a activé la croissance des besoins d’emplois non agricoles, donc d’une main d’œuvre disponible dans des zones conçues à cet effet (G. E. B. Morren (1983)) [^others]

[^others]: See also [URBANISATION, INÉGALITÉS URBAINES ET DÉVELOPPEMENT EN TURQUIE (1950-2000)](https://regionetdeveloppement.univ-tln.fr/wp-content/uploads/8-CatinKamal.pdf), Maurice CATIN et Abdelhak KAMAL, Région et Développement n° 34-2011; [Le passé et le présent des politiques d’urbanisation et de logement en Turquie](https://shs.hal.science/halshs-03479632/file/article-NAQD_version-auteur.pdf), *Gülçin Erdi* (2021)

> **(10) There is an apparent paradox of order and disorder created by the interplay or points (8) and (9) above, corresponding to the difficulty of assessing balance between the real benefits and real cots of escalation from phase to phase.**
> 
> > “According to Bateson (1972b. *p. 498*), it is a paradox that authorities, including planners and analysts, need consciously weigh, and it is, I assert, weighted by ordinary people often to the consternation of such authorities. Thus in the aftermath of the 1976 earthquake in eastern Turkey, which effectively destroyed most shelter in the area, government officials laboured vigorously to relocate survivors from rural to urban areas in anticipation of harsh winter conditions. On their part, many common people resisted this with equal vigor in anticipation of the loss of their livelihood *[subsistence]* the quality or state of being lively, and the fate of their livestock, which would not be able to survive without human care.”
> 
> <cite>Georges E. B Morren</cite>, 1983, *p. 295*

Selon comment sont perçus les individus demandant de l’aide, assistance et refuge, le traitement « accordé » par les institutions en responsabilités varient.

Ainsi, que le choix soit pris par des États ou des actaires humanitaires, l’acte d’installer et de gérer un camp pour des « réfugiés » est conditionné par:

- Le rapport entretenu à la figure de « l’autre », de « l’étranger », du « réfugié »

- Le nombre de personnes déplacées à accueillir

- Les moyens financiers disponibles

- Le contexte Historique et politique des populations déplacées

- Le contexte Historique et politique de l’État et actaires humanitaire en responsabilité d’accueil

- La zone géographique disponible et l’état du terrain

- l’acception ou le refus des réfugié⋅es du dispositif de camp
    
- Les conditions quotidiennes et sanitaires de vie dans un camp
 
- La volonté ou non et stratégie associée pour absorbing and integrating les populations déplacées
    
### Quels pourraient être les inconvénients de cette approche, tant pour les communautés d’accueil que pour les réfugié⋅es elleux-mêmes ?

Des individus se regroupant lors d’un déplacement, ou après celui-ci, pour établir un lieu de vie collective est un fait avéré depuis des millénaires. Archéologues, paléo-anthropologues, et bien d’autres parleront volontiers de camps. Ces sites de vie en camps apportant des avantages pour la subsistance des individus et du groupe (García-Diez M, Vaquero M (2015)).
    
L’utilisation du « camp » dans ses versions *modernes* dédiées aux refugié⋅es ne permet pas d’apporter une solution tenable dans le flux augmentant des populations déplacées. Les populations déplacées sont aussi utilisées comme arme dans les rapports de force entre États (Behrouz Boochani (2018)). Le camp n’est pas non plus une réponse formulée pour participer à la résolution des causes des déplacements de populations.

Le « camp », par sa configuration même en *fermeture* et du fait qu’il concentre, favorise l’émergence d’épidémies, comme en Haïti en 2010 dont le point d’origine est un camp de casques bleus (Piarroux (2019)). Pour un autre cas d’exemple, les camps de malades déplacés sont le théâtre de perpétuation coloniale comme en Guinée lors d’une pandémie d’Ebola en 2014 (Veronica Gomez-Temesio et Frédéric Le Marcis (2019)). « Dans certains pays, les personnes réfugiées sont exclues des plans de vaccination. Dans leur lutte contre le Covid-19 » (Amnesty International (2021)). En 2023 une crise sanitaire frappe les camps de déplacés alors que le conflit fait rage au Soudan (UNCHR (2023)).  
      
Dans les fonctions du « camp » il y a la séparation de la population encampée du reste de la population, et aussi le tri des individus au sein même du camp. Le camp réduit les interactions sociales. Et les personnes déplacées sont pour beaucoup en situation d’exil pour une durée de 20 années.

Globalement la gestion des populations déplacées souffrent du manque de moyens financiers.
    
Gap between funds needed and funds pledged (Source: UNHCR/UNRWA/IDMC, *in* Conflict and crisis in an interconnected world,P. Ireland (2024))
    
2012:
    
- pledged: 6 billions
        
- needed: 9 billions
        
    
2022
    
- pledged: 29.7 billions
        
- needed 51.6 billions
        
    
L’installation, la gestion, et la maintenance d’un camp sont des dépenses très élevées pour les États et les institutions en responsabilités. Ces coûts peuvent accompagner une volonté de réduction des dépenses et de minimisation des investissements. “the Canadian protection system exhibits a series of deficiencies, ranging from detention policies and deportation in the case of asylum seekers, down to the integration obstacles and other associated challenges encountered by resettled refugees” (Martani, E., & Helly, D. (2022)). Les institutions en charge du fonctionnement des camps peuvent avoir recours à de stratégie de déléguer la fabrication des habitats et des infrastructures internes, comme pour Zaatari et Azraq en Jordanie en 2014 et 2015 (Doraï (2014, 2015)). Après avoir expulsé de réfugié⋅es de leur camp administré, la cohabitation avec des camps dits informels peut aussi être actée, comme en 2016 au Liban, où se concentraient 400.000 réfugiés (Bonnet (2014)), et le camp informel des réfugié⋅es de Syrie de la Bekaa (Marj) (Aubin-Boltansky (2016)). Nous rappelons ici que l’individu, et probablement l’entité individuée à travers lui, n’est pas uniquement ni strictement conscrit à un rôle prescrit d’executant de taches.
         
La vie en camp est très fréquemment un vécu d’*horreur*, comme le révèle le [The logbook of Moria](https://wearesolomon.com/mag/focus-area/migration/the-logbook-of-moria/). Une horreur vécue par les employé⋅es de l’ “International Organization for Migration” ([IOM](https://www.iom.int/countries/greece)) comme par les réfugié⋅es.
    
Ceci articulé aux conditions de vie sanitaires évoqués ci-avant, l’approche de la gestion des populations déplacées peut devenir une solution déshumanisante dans laquelle de nombreux droits humains fondamentaux ne sont pas respectés. 
    
Les États se méfient eux aussi de créer des camps sur leur sol (Ben White, Simone Haysom and Eleanor Davey (2013)). Ils peuvent alors tenter *d’invisibiliser les camps* dans l’architecture et le paysage, ou de les installer avec des accords bilatéraux trans-nationaux dans des pays voisins comme nous l’avons mentionné ci-avant pour le cas de France ci-avant (Olivier Le Cour Grandmaison, *& al.* (2007)).
      
Ils peuvent aussi déployer des camps le plus loin possible. [Les dossiers de Nauru](https://web.archive.org/web/20200810013430/http://suzonfuks.net/reading-nauru-files-2017/) sont des rapports de fuite d’informations du personnel (enseignants de Save the Children, travaillaires du social, personnel de loisirs et certains peuvent avoir été rédigés par les services de sécurité) concernant des abus, des actes d’automutilation et de négligence envers les réfugié·e·s, y compris les enfants, qui ont été détenus pendant 7 ans ou indéfiniment par le gouvernement australien. Les réfugié⋅e⋅s et les demandeurs d’asile qui ont tenté de rejoindre l’Australie par bateau à partir du 19 juillet 2013 ont été expulsés par le gouvernement australien vers des camps situés sur les îles Nauru et Manus (à 3~4500 km de l’Australie). En 2020, 12 personnes sont mortes et la santé mentale des individus a atteint des niveaux alarmants et catastrophiques dans ces deux îles-prisons.
    
Le camp enferme, détient, éloigne, invisibilise des populations « étrangères » rendues ainsi encore plus *étranges*. En résulte un terrain propice aux rumeurs, aux craintes. Ce que Hannah Arendt rappelle dans son discours de philosophie politique de 1943 « We Refugees » 
    
> If we are saved we feel humiliated, and if we are helped we feel degraded. We fight like madmen for private existences with individual destinies, since we are afraid of becoming part of that miserable lot of *schnorrers* whom we, many of us former philanthropists, remember only too well.
> 
> Just as once we failed to understand that the so-called *schnorrer* was a symbol of Jewish destiny and not a *shlemihl*, so today we don’t feel entided to Jewish solidarity; we cannot realize that we by ourselves are not so much concerned as the whole Jewish people. Sometimes this lack of comprehension has been strongly supported by our protectors
> 
> […]
> The natives, confronted with such strange beings as we are, become suspicious; from their point of view, as a rule, only a loyalty to our old countries is understandable.

Le camp entrave le regard porté par la population accueillante sur la population accueillie. Les écritures grises accentuent ce phénomène.
Aussi, dans la population d’accueil les personnes déplacées peuvent être ressenties comme un problème. « L’assistance peut susciter un ressentiment à l’égard des réfugiés, en particulier lorsque la société d’accueil souffre également de privations ou de tensions politiques » (Ben White, Simone Haysom and Eleanor Davey (2013)). Ces craintes peuvent alors être tournées en outil/arme politique.
    
- En 1999, le Centre d’hébergement et d’accueil d’urgence humanitaire (CHAUH) de Sangatte est créé. La fermeture du centre est ordonnée par Nicolas Sarkozy en 2002, alors Ministre de l’Intérieur. Le centre est détruit en décembre 2021. Entre 65 000 et 70 000 personnes y auraient été enfermé⋅e⋅s entre 1999 et 2002.
    
- Pour le cas de la Belgique, c’est l’utilisation d’un concept de « Zone Neutre » pour accueillir des réfugié⋅es qui a été utilisé. Cette utilisation participant de la minimisation des revendications politiques et de leurs droits par les réfugié⋅es elleux-mêmes. Une zone neutre en droit international est un territoire aménagé pendant un conflit, sur le territoire des belligérants, pour abriter des personnes non-combattantes (civils, blessés, malades)[^neutre].
    
D’un point de vue d’historiennes et d’historiens[^crise], les institutions, États et administrations déléguée, sont de grandes pourvoyeuses d’écritures grises. Avec ces écritures l’Institution met en scène une fiction de stabilité(s) au travers de productions de règlements. Des actaires peuvent avoir intérêts à la « crise », il y a donc des usages sociaux de la crise.

En 3 phases :

- Invoquer la crise c’est créer le sentiment d’urgence.

- La réforme d’une Institution : se réforme dans le cadre discursif de crise.

- Dans la narration de la stabilité : il s’agit de *auto*-justifier sa propre légitimité par l’Institution pour la mettre au service d’un récit de stabilité

Ce qui fut le cas pour « *Les camps d’internement canadiens durant la Seconde Guerre mondiale : entre mise en ordre et remise en cause* » [*camps de prisonniers*] (<cite>Lucile Chaput</cite>, *Tempora Université Rennes 2 (2023), *opt cit*).

Ces écritures grises ne contiennent pas les récits, témoignages, analyses des personnes mises en camps. Dans la gestion et l’administration des camps s’écrit une histoire sans les premières personnes impactées. L’expression attitrée “Undocumented people” dit cela aussi -- *Why ’Undocumented’ or ’Irregular’?* (UNCHR (2018))[^unchr] -- puisqu’il s’agit de nier l’existence d’une *documentation* valide dans un espace administratif donné pour des personnes demandant refuge du fait de leur histoire documentée ailleurs.

[^unchr]: <https://www.unhcr.org/cy/wp-content/uploads/sites/41/2018/09/TerminologyLeaflet_EN_PICUM.pdf>

Les mouvements de populations déplacées sont souvent liées à ce qui peut être appelé « crise ».  
La « crise » est considérée comme rupture (*face à une continuité supposée*). La crise s’inscrit dans le temps de la conjecture. Le phénomène migratoire et les réfugiés et les camps peuvent servir de sujet d’invocation de crise, comme pour le cas de “Fortress Europe” continues to resist new arrivals” (Ireland (2023)).

S’appuyant sur sa propre expérience de juive allemande en Amérique, Arendt nous met au défi d’imaginer le monde du point de vue d’un immigrant. « L’histoire a imposé aux uns et aux autres le statut de hors-la-loi, de parias et de parvenus. » (H. Arendt (1943))

Avant de faire des camps puis d’y enfermer des individus il est convenu de stigmatiser une ou plusieurs *catégories* d’individu. Les mettre au ban par le langage et signifier déjà là leur exclusion. « Le paria. Une figure de la modernité ». Ce qui n’est qu’une pensée presque *marginale*. (Eleni Varikas, Martine Leibovici, *Collectif*, Tumultes, n° 21-22, 2003). La stigmatisation est une exécution de peine et une planche pour le pire[^pariat].

> « Jamais la rhétorique des droits de l’homme n’a tant co-existée avec la prolifération d’hommes et de femmes privées de cette condition première qu’Arendt associe à l’exercice des droits : une place dans le monde qui garantit que nos opinions ont du poids et nos actions de l’effet »
>
> <cite>Eleni Varikas</cite> (2003), *[La figure du Paria : une exception qui éclaire la règle](https://www.cairn.info/revue-tumultes-2003-2-page-87.htm)*

[^neutre]: 2022, sur Radio Panik, nous pouvions entendre des témoignages ahurissants, de personnes sans-papiers qui triment de 8 heures du matin à 20h pour 25 € la journée, soit une « rémunération » de 2€ et cinquante centimes par heure de travail. Journal des Sans-Papiers, Mars 2022, « Elles, ce sont les personnes résidant en Belgique sans titre de séjour régulier, celles qu’on appelle les personnes « sans-papiers ». Cela fait des années que les « sans-papiers » se mobilisent quotidiennement pour réclamer leurs droits. Droit au logement, à un travail décent, à l’éducation, à… 100,000 femmes, enfants et hommes en sont privés aujourd’hui en Belgique, et la politique de restriction et de rejet du gouvernement fabrique chaque jour de nouveaux « sans-papiers » parmi les milliers de candidats réfugiés qui sont (mal) accueillis dans ce pays. » [https://archive.org/details/dkbct-2-offdem](https://archive.org/details/dkbct-2-offdem)

[^crise]: Xavier Coadic, 2023, Notes depuis le colloque : À l’épreuve des tempêtes. Institutions et crises : approches historiques <https://xavcc.frama.io/notes-colloque-institutions-crises-recherche-histoire/> ; Les camps d’internement canadiens durant la Seconde Guerre mondiale : entre mise en ordre et remise en cause ; Lucile Chaput, *Tempora (Université Rennes 2*) <https://xavcc.frama.io/notes-colloque-institutions-crises-recherche-histoire/#les-camps-dinternement-canadiens-durant-la-seconde-guerre-mondiale--entre-mise-en-ordre-et-remise-en-cause>

[^pariat]: Nous avions travaillé cette problématique en préambule (2022) de Notes de lectures : Les camps et enfermement(s) [https://xavcc.frama.io/notes-lecture-les-camps/#la-figure-du-paria--une-exception-qui-%C3%A9claire-la-r%C3%A8g](https://xavcc.frama.io/notes-lecture-les-camps/#la-figure-du-paria--une-exception-qui-%C3%A9claire-la-r%C3%A8gle)

La parole prise par Hannah Arendt il y a plus de 80 ans nous amène à l’emphase et aux souvenirs avec une perspective politique, socialement située, et philosophique ; notamment face au programme Nazi et des camps d’extermination. Ceci est toujours aujourd’hui un rappel que la parole et la légitimité est asymétrique. 

Aujourd’hui, les analyses et discours sont encore majoritairement offerts à des personnes n’ayant pas fait l’expérience dans camps en *qualité* de refugié⋅es et seulement quelques fois dans les « savoirs experts » (Chivallon (2025)) des ONG, des journalistes, des politiciennes et politiciens, ou bien plus dans les grammaires et arcanes des académies scientifiques qui sont *de facto* pas ou peu accessible aux refugié⋅es et personnes encampées. Cela rejoignant les questions et problèmes de *perspective of sustainability sciences*, “the status of the human in terms of rights and law” (Toivanen R. & Cambou D. (2021)).

Le « camp » est un dispositif qui participe aux récits depuis un point très spécifique et avec des effacements, avec la double question de qui possède le droit à la tribune et de qui et rendu muet. Ce dispositif de camp ordonne et implémente le rapport à « l’autre », en étant un outil de séparation et de tri. Dans plusieurs champs de travail académique et scientifique les « camps » sont étudiés et détaillés comme continuum du colonialisme avec ségrégation et racisme. Nous en avons cités certains de ces champs avec des références reconnues. Dans la majorité de ces littératures la parole des personnes déplacées et encampées et étiquetées réfugié⋅es est reléguée et n’est pas celle prise en *figure d’autorité*. Des sachants débattent et des réfugiés subissent.  
Les « camps » influent aussi sur nos rapports sociaux, intimes et politiques.

Une fois les camps de réfugié⋅es démantelés ou détruits leur place dans l’Histoire est classée aux archives administratives, celles des réfugié⋅s et de leurs vécus dans ces camps est renvoyée aux oubliettes.

Ce n’est pas avec les instruments de celleux qui vivent dans l’opulence des privilèges que nous pouvons comprendre et analyser ce qui se passe aujourd’hui.

> Il n’y a malheureusement rien de nouveau à affirmer que les peuples opprimés et colonisés ont été, et continuent à être, soumis à une violence épistémique - altérisation, réduction au silence et visibilité sélective - qui les rend inaudibles, ou ne les montre et ne les écoute que dans l’unique cadre de certains points de vue ou registres perceptuels - terroristes, manifestant·es, meurtrièr·es, sujets humanitaires – tout en les privant de leurs qualités les plus humaines
>
> <cite>Salih, Ruba</cite>. *December 2023*. ’Can the Palestinian speak?’. Allegra Lab. https://allegralaboratory.net/can-the-palestinian-speak/

## References bibliographiques & notes

- Amnesty International, Réfugiés : les laissés-pour-compte de la vaccination, Publié le 18.06.2021 <https://www.amnesty.fr/refugies-et-migrants/actualites/refugies--les-laisses-pour-compte-de-la-vaccination>

- ATLANTI-DUAUKT, L. & Vidal, L. (2009). *Anthropologie de l’aide humanitaire et du développement: Des pratiques aux savoirs des savoirs aux pratiques*. Armand Colin. [https://doi.org/10.3917/arco.atlan.2009.02](https://doi.org/10.3917/arco.atlan.2009.02)

- ARENDTH, H. “We Refugees,” Menorah Journal 31, no. 1 (January 1943): pp 69-77.

- AUBIN-BOLTANSKY, E. (2016). Camp informel de réfugiés syriens.. Photography. Lebanon. [⟨medihal-02023512⟩](https://media.hal.science/medihal-02023512)

- BOOCABI B. (2018), Refugees’ lives have become weapons in a rugged political contest <https://www.theguardian.com/commentisfree/2018/jun/20/our-lives-are-have-become-weapons-in-a-rugged-political-contest>

- BONNET, M. (2014). Réfugiés syriens : d’une crise humanitaire nationale à un défi pour l’humanité. *Les Cahiers de l’Orient*, 116, 67-77. [https://doi.org/10.3917/lcdlo.116.0067](https://doi.org/10.3917/lcdlo.116.0067)

- CHIVALLON, C. (2005) Les enjeux de la qualification des savoirs : l’exemple afro-américain (Qualifying knowledges : the african-american example. January 2005. Bulletin de l Association de géographes français 82(3):343-357 DOI:[10.3406/bagf.2005.2469](http://dx.doi.org/10.3406/bagf.2005.2469)

- DORAÏ, K. (2014) Des enfants syriens de retour de l’école dans le camp de réfugiés de Zaatari, Jordanie. Photography. Camp de Zaatari, Jordan. [⟨medihal-01316405⟩](https://media.hal.science/medihal-01316405)
    
- DORAÏ, K. (2015). Un atelier de menuiserie dans le camp de réfugiés de Zaatari - Jordanie. Photography. Camp de Zaatari, Jordan.  [⟨medihal-01314370⟩](https://media.hal.science/medihal-01314370)
    
- DORAÏ, K (2015) Un habitat auto-construit par un réfugié syrien, Azraq - Jordanie (4). Photography. Azraq, Jordan. [⟨medihal-01508959⟩](https://media.hal.science/medihal-01508959)

- Engebrigtsen, A. I. (2017). Key figure of mobility: The nomad. Social Anthropology/Anthropologie sociale, 25(1), 42-54. Retrieved Apr 8, 2024, from https://doi.org/10.1111/1469-8676.12379

- García-Diez M, Vaquero M (2015) Looking at the Camp: Paleolithic Depiction of a Hunter-Gatherer Campsite. PLoS ONE 10(12): e0143002. https://doi.org/10.1371/journal.pone.0143002

- GLUCKMAN, (1961), “Anthropological problems arising from the african industrial revolution”, in A.W. SOUTHALL, Social Change in Modern Africa, London, Oxford University Press.

- GOMEZ-TEMESIO V. et Le MARCIS F. (2019), « La mise en camp de la Guinée », L’Homme [En ligne], 222 | 2017, mis en ligne le 01 juin 2019, consulté le 05 avril 2024. URL : http://journals.openedition.org/lhomme/30147 ; DOI : https://doi.org/10.4000/lhomme.30147

- HACKER W. (2023) Où sont les « gens du voyage » ? Une histoire actuelle de l’antitsiganisme.

- Hewitt, K. (Ed.). (1983). Interpretations of Calamity: From the Viewpoint of Human Ecology (1st ed.). Routledge. https://doi.org/10.4324/9780429329579

- Le Cour Grandmaison (2007) *Le retour des camps ? Sangatte, Lampedusa, Guantanamo…*, *Olivier Le Cour Grandmaison, Gilles Lhuillier et Jérome Valluy. 2007.* ISBN-10:  2-7467-0926-0

- MARTANI, E., & HELLY, D. (2022). Asylum and resettlement in Canada. Historical development, successes, challenges, and lessons. Zenodo. https://doi.org/10.5281/zenodo.7609641

- MANZ, Stefan, and PANIKOS Panayi, Enemies in the Empire: Civilian Internment in the British Empire during the First World War (Oxford, 2020; online edn, Oxford Academic, 23 Apr. 2020), https://doi.org/10.1093/oso/9780198850151.001.0001, accessed 4 Apr. 2024

- MORREN, E. B *Jr*, *in* Hewitt, K. (Ed.). (1983). Interpretations of Calamity: From the Viewpoint of Human Ecology (1st ed.). Routledge. https://doi.org/10.4324/9780429329579

- PADILLA E. (1958), Up From Puerto Rico. New York, Columbia University Press.

- PATTON, Walter Melville, (1903), Ancient Egypt And Syria. Bibliotheca Sacra 1903-01: Vol 60 Iss 237

- PIARROUX P. (2019) Choléra. Haïti 2010-2018. Histoire d’un désastre. // CNRS éditions,

- RYFMAN Philippe, « L’héritage des camps de la Retirada au XXIe siècle. L’impasse de la réponse par le camp à l’existence du « réfugié » », Exils et migrations ibériques aux XXe et XXIe siècles, 2015/1 (N° 7), p. 256-267. URL : https://www.cairn.info/revue-exils-et-migrations-iberiques-2015-1-page-256.htm 

- TURNER V., (1957), Schism and Continuity in an African Society, Manchester, Manchester University Press for Rhodes-Livingstone Institute.

- STONE, D. (2018). Refugees then and now: memory, history and politics in the long twentieth century: an introduction. Patterns of Prejudice, 52(2–3), 101–106. https://doi.org/10.1080/0031322X.2018.1433004

- TOIVANEN R. & CAMBOU D. (2021). Human Rights. In: Krieg C. & Toivanen R (eds.), *Situating Sustainability*. Helsinki: Helsinki University Press. DOI: [https://doi.org/10.33134/HUP-14-4](https://doi.org/10.33134/HUP-14-4)

- UNCHR, 2023, Une crise sanitaire frappe les camps de déplacés alors que le conflit fait rage au Soudan <https://www.unhcr.org/fr/actualites/articles-et-reportages/une-crise-sanitaire-frappe-les-camps-de-deplaces-alors-que-le>

- VARIKAS, E. (2003). La figure du Paria : une exception qui éclaire la règle. *Tumultes*, 21-22, 87-105. <https://doi.org/10.3917/tumu.021.0087>

- WHITE B, HAYSOM S. and DAVEY E. (2013) Refugees, host states and displacement in the Middle East: an enduring challenge.
    
### Notes
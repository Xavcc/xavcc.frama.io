---
title: "Notes de lectures : Les camps et enfermement(s)"
layout: post
date: 2022-10-13 14:40
image: /assets/images/alter1fo-cra.jpg
headerImage: true
tag:
- hsociety
- Notes
- Rennes
- Catastrophologie
- Anthropologie
category: blog
author: XavierCoadic
description: Camps et technique d'enfermement
---

<u>Camps et technique d'enfermement</u>

<figcaption class="caption">Image d'en-tête : « [22 septembre 2020] – Un jour, une photo : La CIMADE s’affiche » depuis un article de Alter1fo « <a href="https://alter1fo.com/cluster-centre-retention-temoignage-rennes-135554">Nouveau cluster au CRA de Rennes, le témoignage de Félicy.</a> »</figcaption>
<br />

L'encampement, la mise en camps avec technique d'enfermement, est une pratique bien instituée, légitimité sur de vastes zones avec réseaux et infrastructures. Il s'agit d'actes bien ancrés dans un passé long. Évoquée comme « mesure temporaire et exceptionnelle » la mise en enfermement est en fait permanente depuis plusieurs siècles, c'est même lorsqu'elle n'a pas cours que l'exception est réelle. Ses origines, tout comme sa forme et application actuelle, se lient finement et solidement avec le racisme et la colonisation ; prolifèrent dans un silence collectif et s'ancrent très profondément en tant que *<u>poison organique permanent et persistant</u>*.

Il y a un camp près de chez, il y a un camp chez vos ami⋅e⋅s, un camp aussi non-loin de vos parent⋅e⋅s. Ne pas voir ces camps, y compris ceux qui ont disparu, ni le système des camps et d'enfermement, ne pas voir les personnes enfermées en camp d'internement, cela dit déjà beaucoup de nous en tant que groupe et collectivité, de notre manière de faire société.

Quel manant suis-je pour écrire de cela ?

--- 

**Table des contenus**

+ [La figure du Paria : une exception qui éclaire la règle](#la-figure-du-paria--une-exception-qui-éclaire-la-règle)
+ [Nauru (Australie / Océan perdu pour vies oubliées)](#nauru)
+ [Le retour des camps ?](#le-retour-des-camps)
  + [Qu’est-ce qu’un camp pour l’enferment ?](#quest-ce-quun-camp-pour-lenferment)
  + [Histoire brève et institution juridique d'un camp d’enfermement à la française](#histoire-brève-et-institution-juridique-dun-camp-denfermement-à-la-française)
  + [“Libertés”, violences, notion de vie](#libertés-violences-notion-de-vie)
  + [La forme-camp par et avec Sangatte, Lampedusa et les figurants sur le port, Guantanamo](#la-forme-camp-par-et-avec-sangatte-lampedusa-et-les-figurants-sur-le-port-guantanamo)
    + [Lampedusa](#lampedusa)
+ [Où sont les “gens du voyage” ? Inventaire critique des aires d’accueil](#inventaire-critique-des-aires-daccueil-où-sont-les-gens-«-du-voyage-»)
  + [Stigmatisation et fichage](#stigmatisation-et-fichage)
  + [Le placement dans des « aires »](#le-placement-dans-des-«-aires-»)
+ [Autres ressource](#autres-ressources)
+ [Notes et références](#notes-et-références)

---

J'ai mentionné dans un précédent écrit sur « [une prise sur un quotidien catastrophé](tiers-lieux-et-catastrophes/) » le moment au début des années 2000 où j'ai mis les pieds dans un centre de rétention administrative. Lorsque j'ai travaillé avec Tactical Tech / Exposing The Invisible, j'ai eu le plaisir et le privilège d'avoir des séances avec Megha Rajagopalan et Alison Killing qui ont remporté le prix Pultizer en 2021 pour leur enquête sur les camps d'internement des musulman⋅e⋅s en Chine. Puis, j'ai passé un peu de temps avec des personnes de l'équipe d'investigation sur le camp de Moria (Grèce) − “[From the Ashes to Europe's Audience. The Creation of 'The Logbook of Moria'](https://exposingtheinvisible.org/en/films/logbook-of-moria/).

> « À la suite de l'incendie de Moria, le camp de réfugié⋅e⋅s le plus grand et le plus notoire d'Europe jusqu'à ce jour (septembre 2020), un registre du personnel a été trouvé. Il s'agissait d'un journal quotidien appartenant aux personnes qui étaient là pour protéger les mineur⋅e⋅s non accompagné⋅e⋅s, intervenant⋅e⋅s qui se sentaient souvent incapables de le faire. Ses pages révèlent une réalité horrible. »

J'ai ensuite eu la chance d'avoir une discussion téléphonique avec William Acker, auteur de « <u>Où sont les "gens du voyage" ? - Inventaire critique des aires d’accueil</u> » (2021, ed. Du commun). J'ai dévoré et re-relu ce magnifique livre. J'ai aussi pu suivre à distance la [résidence d'écriture THX](https://thx.zoethical.org/) au pays basque à l'été 2022 (*suivi parcellaire et parfois très légèrement contributif pour ma part infime*). Les sujets de discussions et les textes à paraître sont assez proches des questions sur les camps et les techniques d'enfermements. 

J'ai trouvé dans une boite à livres « <u>Le retour des camps ? Sangatte, Lampedusa, Guantanamo… </u>» (<cite>Olivier Le Cour, Gilles Lhuilier, Jérôme Valluy, Collectif</cite>, 2007. Éd. Autrement). **J'aime les boites à livres !!!**

<br />
> « Aller se coucher sur des images de flics qui expulsent -encore- un camp à Calais.
>
> Ils portent des sur-chaussures en plastique pour éviter de souiller leurs godasses coquées.
>
> Le camp a été labouré après l'expulsion, afin de créer des sillons profonds empêchant toute nouvelle installation de tentes.
>
> L'abandon définitif de notre humanité se fait avec un raffinement nauséabond qui interdit l'hypothèse accidentelle. » <cite>Un inter-fedinaute que j'apprécie (2021, France)</cite>

<br />


Enfin, j'ai croisé la route de professionnel⋅le⋅s de l'intervention humanitaire (ONG, Union Européenne, ONU, autres). Certaines de ces personnes ont mis sur la table une question : « faire tiers-lieu dans un camp de refugié⋅e⋅s ? ». Avec des débats sur les enjeux numériques (un biais fréquent)<marginnote>En Éthiopie, <a href="https://fr.globalvoices.org/2020/04/01/247242/">, le nouveau système d'identification biométrique pour les réfugiés suscite de nombreuses inquiétudes</a> /, Version anglaise <a href="https://advox.globalvoices.org/2020/03/19/refugees-in-ethiopias-camps-raise-privacy-and-exclusion-concerns-over-unhcrs-new-digital-registration/">Refugees in Ethiopia's camps raise privacy and exclusion concerns over UNHCR’s new digital registration</a></marginnote>.

L'enfermement comme technique est un vécu et un quotidien pour de nombreuses personnes très souvent invisibilisées, y compris *au nom de la médecine*.

> « C’est justement ce dont nous vous parlons en tant que personnes handicapées lorsque nous dénonçons notamment : le confinement en institutions, que vous trouvez pourtant souvent parfaitement défendable » *[Le confinement : une nouveauté pour vous une réalité pour nous](https://auxmarchesdupalais.wordpress.com/2020/03/18/le-confinement-une-nouveaute-pour-vous-une-realite-pour-nous/)*, <cite>Élisa ROJAS</cite> (mars 2020)

Une enfance aux près d'Habib qui disparu après plus 15 ans en France peu après un enfermement au Centre de Rétention Administrative de Marseille ; la vie à la rue et les nuits sous les ponts, les squats de Marseille à Bruxelles avec les réfugié⋅e⋅s / migrant⋅e⋅s / sans-papier / exilé⋅e⋅s ; la rencontre avec Saïd de la Maison des Migrants (Belgique)…

Voici quelques raisons qui me mènent ici à partager quelques notes de lectures. Je laisse, pour l'instant, de côté cette question « tiers-lieux et camps ». **Les crimes accommodent des palliatifs et du silence**

## La figure du Paria : une exception qui éclaire la règle

Avant de faire des camps puis d'y enfermer des individus il est convenu de stigmatiser une ou plusieurs *catégories* d'individu. Les mettre au ban par le langage et signifier déjà là leur exclusion. « Le paria. Une figure de la modernité »<marginnote>Le titre m'a immédiatement fait penser à « L'amateur: une figure de la modernité esthétique » ; article de Laurence Allard (1999). Ce qui n'est qu'une pensée presque <i>marginale</i>.</marginnote> (<cite>Eleni Varikas, Martine Leibovici, Collectif</cite>, Tumultes, n° 21-22, 2003). La stigmatisation est une exécution de peine et une planche pour le pire.

> « Jamais la rhétorique des droits de l’homme n’a tant co-existé avec la prolifération d’hommes et de femmes privés de cette condition première qu’Arendt associe à l’exercice des droits : une place dans le monde qui garantit que nos opinions ont du poids et nos actions de l’effet » <cite>Eleni Varikas</cite> (2003), *[La figure du Paria : une exception qui éclaire la règle](https://www.cairn.info/revue-tumultes-2003-2-page-87.htm)*

C'est ici une entrée en matière avec une référence à Hannah Arendt, « L’impérialisme », *Les Origines du totalitarisme*. Se référer à Arendt est fréquent, presque une « obligation » *mondaine*[^citation-mondaine], dans les exercices, notamment universitaires, de questionnements et de descriptions de camps et autres coercitions.

[^citation-mondaine]: Généalogies scientifiques, pratiques et privilèges citationnels : “Les murs de l’université” (Living a Feminist Life) « Qui cite-t-on dans les articles, et surtout qui ne cite-t-on jamais (ou presque jamais) ? Qui entend-on en colloque, et surtout qui n'entend-on jamais (ou presque jamais) ? Réfléchissant à partir de l'exposé d'un certain nombre d'expériences vécues dans le cadre de son propre parcours, Sara Ahmed élucide certains des mécanismes citationnels, spécifiques au monde universitaire, qui organisent les héritages, les généalogies et les accointances scientifiques en reconduisant les hiérarchies et les discriminations sociales ordinaires ». <cite>Sara Ahmed</cite>, « Academic walls », Living a Feminist Life, Durham and London, Duke University Press, 2017, p. 148-158 ; traduit de l’anglais (Royaume-Uni) par Aurore Turbiau, avec l’aide de Marie-Jeanne Zenetti. <https://www.fabula.org/lht/26/ahmed.html>

C'est d'ailleurs ce que firent Olivier Le Cour Grandmaison, Gilles Lhuillier et Jérome Valluy dans l'introduction du livre « Le retour des camps », *Quels camps ? Quel retour ?*, avec une note de bas page ayant pour référence : « Les sans-état « ni minoritaires, ni prolétaires, en dehors de toutes les lois » (<u>H. Arendt</u>) <cite>Caloz-Tschopp Marie-Claire</cite>, *Tumultes*, n°21-22.

> « Bien qu’introduit depuis le début du XVIe siècle, le mot n’entre dans l’espace public littéraire et politique de l’Europe qu’à la fin du XVIIIe. Il y est précédé par le mot caste dont la première connotation péjorative remonte à Montesquieu qui souligne l’élément nouveau qui va donner au paria toute sa force contestataire : l’aversion et le mépris attachés désormais à l’infériorité de rang qui rompt la solidarité humaine » <cite>Eleni Varikas</cite> (2003), *Ibid.*

Sous-humaniser, voir déshumaniser, est la stratégie de base qui permet la conception puis le déploiement de la mise en camps et *in fine* de l'enfermement. Eleni Varikas prend la nouvelle « [La Chaumière indienne](https://fr.wikipedia.org/wiki/La_Chaumi%C3%A8re_indienne) » pour illustrer la diffusion et circulation en Europe d'un « *topos* littéraire » et romantisme du *paria* par l'histoire entre un brahmane et un [intouchable](https://fr.wikipedia.org/wiki/Intouchables_(Inde)) (caste sociale).

Des mots, des signifiants, qui signent la conformité d'une partie de la population avec *le temps* définit par une part du groupe dominant de cette population sur des plus démuni⋅e⋅s. Une supériorité signée.

> « […] les disputes de Valladolid qui avaient fait peser un doute sur la qualité de *veros homines* des Indiens, enfin l’état d’exception dans les pratiques et la législation de l’esclavage et de la colonisation furent des terrains précoces où s’inventait de fait l’art d’une catégorisation hiérarchique spécifiquement moderne. » <cite>Eleni Varikas</cite> (2003), *Ibid.*

Les parias pouvant être pris⋅e⋅s dans les étiquettes par autrui via leurs croyances religieuses, leurs couleurs de peaux, leurs origines géographiques ou sociales, leurs modes de vie, leurs sexualités et modes de romances, leurs genres, leurs orientations politiques, de leurs santés, de leurs *déviances* qui seront souvent pathologisées.

### Nauru

[Le futur a déjà eu lieu à Nauru](https://web.archive.org/web/20200820171316/https://www.theguardian.com/australia-news/ng-interactive/2016/aug/10/the-nauru-files-the-lives-of-asylum-seekers-in-detention-detailed-in-a-unique-database-interactive). Peu de gens ont entendu parler de Nauru. Plus rares encore sont celleux capables de situer cette petite île sur une carte. « Minuscule État qui nous raconte l'histoire de notre propre effondrement à venir ».

[Les dossiers de Nauru](https://web.archive.org/web/20200810013430/http://suzonfuks.net/reading-nauru-files-2017/) sont des rapports de fuite d'informations du personnel (enseignants de Save the Children, travailleurs sociaux, personnel de loisirs et certains peuvent avoir été rédigés par la sécurité) concernant des abus, des actes d'automutilation et de négligence envers les réfugié·e·s, y compris les enfants, qui ont été détenus pendant 7 ans ou indéfiniment par le gouvernement australien. Les réfugié⋅e⋅s et les demandeurs d'asile qui ont tenté de rejoindre  l'Australie par bateau à partir du 19 juillet 2013 ont été expulsés par le gouvernement australien vers des camps situés sur les îles Nauru et Manus (à 3~4500 km de l'Australie). En 2020, 12 personnes sont mortes et la santé mentale des individus a atteint des niveaux alarmants et catastrophiques dans ces deux îles-prisons.

Une lecture (plus de 18h !) de ces « dossiers » fut proposée en 2017 en « [spectacle en réseau]((https://philitt.fr/2018/04/23/le-futur-a-deja-eu-lieu-a-nauru/)) » pour commémorer 7 ans jour pour jour de détention indéfinie « de personnes qui ont tenté d'arriver,et de demander refuge ». En lisant ces documents, l'espoir était d'inciter les politiciens australiens à fermer les camps, faire venir ces personnes au cœur de l'Australie pour qu'elles soient traitées équitablement et rapidement, parlez aux réfugié⋅e⋅s et aux demandeurs d'asile avec respect et humanité, en respectant l'État de droit, la Charte internationale des droits humain⋅e⋅s.

![](/assets/images/osm-nauru-notes.png)
<figcaption class="caption">Capture d'écran depuis Open Street Map. L'île de Nauru est fléchée en rouge au nord-est de l'Australie dans l'océan</figcaption>
<br />

Cette sordide pratique continuant en pleine pandémie COVID-19 en 2020 : [New Zealand to take 450 refugees from Australian processing centres](https://www.reuters.com/world/asia-pacific/new-zealand-take-450-refugees-australian-processing-centres-2022-03-24/). C'est la « [solution du Pacifique](https://fr.wikipedia.org/wiki/Solution_du_Pacifique) » où stade de sport et State House du président de cette petite république peuvent servir de camps ([Nauru Regional Processing Centre](https://en.wikipedia.org/wiki/Nauru_Regional_Processing_Centre)), située à 42 km au sud de l'équateur.

## Le retour des camps ?

Aux parias il est convenu d'accoler une réputation bien gluante, et puante selon les langues qui s'expriment (Le bruit et l'odeur de Chirac, les sens dents de Hollande, Sarkozy et le karcher sur les racailles, Ceux qui ne sont rien de Macron, etc.). Cela permet « l'adoption de dispositions nouvelles toujours plus restrictives, destinées à prémunir la France contre leur arrivée et leur présence, réputées dangereuses sur le plan économique, social et politique » (*Le retour des camps ? Sangatte, Lampedusa, Guantanamo…*, <cite>Olivier Le Cour Grandmaison, Gilles Lhuillier et Jérome Valluy</cite>, *p. 5*).

![](/assets/images/inventaires-camps.png)
<figcaption class="caption">Le livre en question depuis mon « <a href="https://inventaire.io/items/31d2a00c225350cac674074f6ba4daff">inventaire.io</a> »</figcaption>
<br />

Nous sommes en 2007, après 2 années de Ministre de l'Intérieur et de l'Aménagement du territoire, Sarkozy devient président de la république (cette élection fait l'objet d'une information judiciaire depuis 2013 pour une suspicion de financement illégal de la part du régime libyen de Mouammar Kadhafi), François Fillon devient 1er ministre, Michèle Alliot-Marie devient Ministre de l'Intérieur, de l'Outre-mer et des Collectivités territoriales, Kouchner aux affaires Européennes, Brice Hortefeux Ministre de l'Immigration, de l'Intégration, de l'Identité nationale et du Codéveloppement, Rachida Dati Garde des Sceaux, ministre de la Justice, Valérie Pécresse Ministre de l'Enseignement supérieur et de la Recherche. Une ouverture au « centre gauche » étant l'argument langagier de l'époque, une *ouverture* qui se continue aujourd'hui dans le sérail des politiques de l'État comme dans la production de connaissance issue de la recherche universitaire.

+ **La prison militaire de Guantanmo** a été créée en 2001, dans le sud-est de Cuba, impliquant aussi des entreprises de droits privés, camp d'origine à usage militaire. En 1994, les USA ouvrent un camp de détention pour isoler des prisonniers haïtiens et aux réfugié⋅e⋅s du coup d'État de 1991, origine coloniale et raciste et politique non mentioné dans le livre de 2007. C'est dans ce camp (aussi nommé camp X-Ray) que seront enfermés les détenus soupçonnés de terrorisme fin 2001. En juin 2006, la Cour suprême des États-Unis a déclaré illégales les procédures judiciaires d'exception mises en place à Guantánamo.
+ En 1999, **Centre d'hébergement et d'accueil d'urgence humanitaire (CHAUH) de Sangatte** est créé. La fermeture du centre est ordonnée par Nicolas Sarkozy en 2002, alors Ministre de l'Intérieur. Le centre est détruit en décembre 20021. Entre 65 000 et 70 000 personnes y auraient été enfermé⋅e⋅s entre 1999 et 2002.
+ La page Wikipedia francophone sur les [Centre de Rétention Administrative (**CRA**)](https://fr.wikipedia.org/wiki/Centre_de_r%C3%A9tention_administrative_en_France) est de bonne qualité et pourra vous aider.
+ Il existe les assignations à résidence, locaux de rétention administrative (**LRA**), zones d'attente pour personnes en instance (**ZAPI**), Les **CPTA**, centres de permanence temporaire et d’accueil), Centre d'Accueil des Demandeurs d'Asile (**CADA**), Centre d'hébergement et de réinsertion sociale (**CHRS**), Centre provisoire d'hébergement (**CPH**) et d’un **CDA** (centre de premier accueil), les Centre d'Accueil des Français d'Indochine (**CAFI**), les « hameaux forestiers » de travail pour l'Office National des Forêts (ONF) et si refus alors c'est d'office camps disciplinaires en rechange,  Centre de Séjour Surveillé (**CSS**) et tant d'autres acronymes à travers d'autres pays pour désigner **une même politique infligée sur des êtres humain⋅e⋅s de tout âge**.
+ **Les dites « Zones Neutres »**[^zone-neutre], **les camps humanitaires, les camps d'insertion et de travail**.
+ La **Convention de Dublin** avait été signée en Irlande le 15 juin 1990, Dublin II a été adopté en 2003, Dublin III en 2013.
+ Agence européenne pour la gestion de la coopération opérationnelle aux frontières extérieures (**FRONTEX**) de 2004 à 2016, repris par l’Agence européenne de garde-frontières et de garde-côtes ensuite
+ **L'[Hospitalisation sans consentement](https://fr.wikipedia.org/wiki/Soins_psychiatriques_sans_consentement_en_France), d'internements d'Office (HO) et sur demande d'un tiers (HDT)** sont, en 2007, une pratique répandue en France. Elles ne sont pas évoquées, ni traitées, dans ce livre. Il y a une rapide mention des lois de 1838 concernant l'internement psychiatrique (*p. 45*), « modèle aux futurs décrets d'internement politique ».

[^zone-neutre]: 2022, sur Radio Panik, nous pouvions entendre des témoignages ahurissants, de personnes sans-papiers qui triment de 8 heures du matin à 20h pour 25 € la journée, soit une « rémunération » de 2€ et cinquante centimes par heure de travail. Journal des Sans-Papiers, Mars 2022, « Elles, ce sont les personnes résidant en Belgique sans titre de séjour régulier, celles qu’on appelle les personnes « sans-papiers ». Cela fait des années que les « sans-papiers » se mobilisent quotidiennement pour réclamer leurs droits. Droit au logement, à un travail décent, à l’éducation, à... 100,000 femmes, enfants et hommes en sont privés aujourd’hui en Belgique, et la politique de restriction et de rejet du gouvernement fabrique chaque jour de nouveaux « sans-papiers » parmi les milliers de candidats réfugiés qui sont (mal) accueillis dans ce pays. » <https://archive.org/details/dkbct-2-offdem>

Cet ouvrage (2007) regroupe des politologues, philosophes, sociologues et juristes qui analysent ce phénomène singulier caractérisé par la stigmatisation, la violence et des mesures d’exception qui tendent à devenir permanentes. « Documents, enquêtes et témoignages relatifs aux épreuves de celles et ceux qui ont transité par ces camps permettent de prendre la mesure de la gravité et de l’ampleur de la situation. »

Un peu plus de 200 pages aux 4 parties cohérentes, dont la partie 4 avec 30 pages de documents et bibliographie, aux chapitres denses qui constituent une œuvre incontournable pour toutes personnes s'intéressant aux questions des lieux, des régimes de surveillance, des camps, de l'hospitalité et solidarités, des droits humains, de l'enfermement.

Dès la première page le ton est affiché sans équivoque, le langage, la manière dont les personnes et les phénomènes sont appelés, est un socle de base pour installer une « urgence » qui engendre une nécessité dans laquelle « l'image impersonnelle et réifiante des mouvements de populations  prétendument menaçants » sert de clé à la mise en œuvre de contrôle et mise au ban.

Ces pages sont une agglomération de textes provenant de plusieurs spécialités apportant des angles complémentaires et articulés pour tenter une histoire du camp et de l'enfermement, de décrire les caractéristiques, les évolutions de formes, et les notions (de vie et de l'inacceptable) que portent et propagent ces camps. Cette forme de synthèse se fait dans l'intellectualité feutrée et engagée une certaine distance, peu incarnée et policée, y compris avec le passage concernant l'entretien avec Élisabeth Perceval (scénariste et comédienne) et Nicolas Klotz (réalisation et mise en scène) autour du film « [La blessure](https://fr.wikipedia.org/wiki/La_Blessure_(film,_2004)) (2004) (partie II du livre). Les différents passages écrits de cet ouvrage contiennent bien évidemment ce qu'il est poliment attendu d'offuscation et de pointes parsemées d'indignation, sans dépasser de la feuille et ne surtout pas salir la table, ou dénonciation « soft » *cosmopolite*. La parole et le sort de l'opprimé⋅e⋅s ne peuplent pas ce livre avec vivacité. Des notes de bas de page en références à des articles de presse, témoignage de vie sous colonisation *p. 35* (issu d'un livre), témoignage de violences policières à Roissy *p. 62 - 63* (issu du journal Le Monde) puis extrait du bilan l'Anafé de 2005 *p. 185*, ou au travers du récit par procuration dans l'entretien avec Klotz et Perceval, hormis la dernière et quatrième partie dédiée aux documents avec l'entretien de Serge G. réalisé par Migreurope et le témoignage de Kingsley Abang Kum en fin de livre (*p. 178* à *p. 183*) et un certificat médical, après violences policières (*p. 187*). Même la chapitre sous forme de témoignage / rapport de Henri Courau, « De Sangatte aux projets de portails d'immigration : Essai sur une conceptualisation de la “Forme-camp”, ex-employé de la Coix-Rouge puis bénévole, se cantonne dans la description « froide », sans paroles aux opprimé⋅e⋅s, servant une conceptualisation intellectuelle distanciée et feutrée. Un livre de *colloque d'entre savant⋅e⋅s*. Ici les mots des savant⋅e⋅s relèguent et confinent la parole des premières personnes victimes et opprimées.

Les gants pris dans ces lignes servent aussi à exposer les concours et responsabilités des associations, ONG, Haut Commissariats et fonctions ONUsiennes (autre pendant de l'externalisation de l'enfermement et de gestion des camps), et des personnalités, y compris, politiques sous mandat du peuple à l'époque de la publication, ainsi que les « témoins responsables de son actualité » (<cite>Élisabeth Perceval</cite>, en référence à  « La viande humaine ; à propos du film *La Blessure*, <cite>Alain Brossat</cite>, *Combat* n°42, décembre 2005) :

> « Les *réfugiés* ne s'apparentent plus à des personnes ou à des sujets mais à des victimes réduites au sens le plus basique de l'humanité. Ils deviennent des « masses » décrites comme occupant une place en dehors de l'*ordre national des choses*. […] L'institution humanitaire, en l'occurrence la Croix-Rouge et les associations de bénévoles, définit les réfugiés, les stigmatise en tant que tels et attribue cette nouvelle identification : le label réfugié. […] L'institution configure et conditionne cette victime dans un statut qui légitime de fait son action d'aide tout en évacuant les aspects politiques qui ont créé la situation de crise et ceux qui pourraient la résoudre. […] Dans l'humanitaire, on construit un appareil qui tout à la fois “fait vire”, assure la reproduction biologique des individus, mais détruit toute possibilité d'égalité » (<cite>Henri Courau</cite>, Humanitarisation de la cause (*p. 99 à p. 100*)).

**Le livre en question est bâti et tissé de références, notes, et points bibliographiques fournis par des associations, ONG, militant⋅e⋅s, articles de journaux et lanceuses, lanceurs d'alertes. Et ne pourrait pas exister tout simplement sans cela**

Ces camps, dans leur forme et leur fond, ne sont pas des zones de *non-droits*, ils sont bien effectivement des espaces d'infra-droit pour certain⋅e⋅s et de nouveaux droits pour d'autres, notamment pour la police et ses agent⋅e⋅s et le débridage des droits de violences ; tout en « faisant vivre et laissant mourir ». Pensé et dit pour *se protéger de la menace étrangère*, le camp avec sa technique d'enfermement fut utilisé sur des *ennemi⋅e⋅s de l'intérieur* au gré des *ambiances* politiques et des *météos* administratives, c’est-à-dire opposant⋅e⋅s politiques, militant⋅e⋅s, ex-combant⋅e⋅s au service de l'État, révolté⋅e⋅s, *grand-déplacé⋅e⋅s* d'une catastrophe, indépandistes, *inadapté⋅e⋅s*, etc. C'est encore le cas aujourd'hui et le sera probablement demain.

Un livre en 4 parties, aux chapitres courts, qui détaille un peu l'externalisation par les États de la charge des camps et des techniques d'enfermement, comme pour Guantánamo en exemple. Il s'agit de rendre bien compréhensible, et d'en détailler les infimes coutures, y compris le tricotage juridique et administratif, « d'une sorte d'état d'exception permanent institué » (*p. 13*). L’externalisation de « l'asile » est une politique déployée au sein de l'Union européenne qui délocalise l'accueil et l'hébergement, et l'enfermement, des demandeuses et demandeurs d'asile, ainsi que le traitement administratif de leurs demandes, dans des lieux situés à proximité des frontières de l'UE, ou dans des pays, situés hors de l'UE, par lesquels les personnes transitent. Que se soit dans l'hexagone français, via des *zones* de rétention, ou dites *d'attentes* (pour user du langage comme douce arme), bien spécifiques, ou hors de France, ces lieux et zones sont inter-reliées, les personnes y sont déplacées par les autorités, et forment ainsi un réseau maillé, « zone abstraite et concrète, juridique et géographique » (<cite>Gilles Lhuillier</cite>, *p. 28*).

> « Les camps ne sont pas des zones de non-droit, mais bien au contraire des institutions juridiques. Ces instruments d'une technique particulière visent à séparer les nationaux des étrangers » <cite>Gilles Lhuillier</cite> (Essai de définition : l'Institution juridique des camps,  *p. 16*).

Comme les [ZAPI](https://www.infomigrants.net/fr/post/31783/zapi-de-roissy--lanafe-se-retire-pour-denoncer-la-mise-en-danger-des-migrants), (en 2018, 51 zones d’attente en France)[^zapi] à Roissy au début des années 2000, les zones dites d'attentes sont « organisées par un “État de droit” » qui fait fit, et même disparaitre, du moins partiellement, l'*habeas corpus* par une simple décision administrative.

[^zapi]: « [Businessmen, touristes ou personnes soupçonnées de vouloir immigrer sont enfermés dans la zone d’attente de l’aéroport. Droits bafoués, fausses couches, enfermement de mineurs… StreetPress te raconte le quotidien de cette prison qui ne dit pas son nom](https://www.streetpress.com/sujet/1520249739-zone-attente-aeroport-roissy) » par Claire Corrion et Yann Castanier pour Street Press (2018)

![](/assets/images/paul-chiron-cimade.png)
<br />

> « Le camp est alors une métaphore concrète, dans l'espace géographique, cette césure fondamentale entre nationaux et étrangers qui structure tout le droit public moderne. Acmé étatique… » <cite>Gilles Lhuillier</cite> (Essai de définition : l'Institution juridique des camps, *p. 18*).

Si Guantánamo est cité, et que la Libye, le Maroc, l'Afrique subsaharienne, Mauritanie, la Prusse (avec la détention  propective *[Schutzhaft](https://fr.wikipedia.org/wiki/Schutzhaft)*) et l'Allemagne le sont aussi, cet ouvrage tient d'un prisme hexagonal français que les autrices et auteurs étirent jusqu'à certains autres points géographiques de ce *réseau* de lieux d'enfermements  ». Il n'y a pas d'autres perspectives abordées dans ce livre qui est pour autant bien écrit et précis. Cette agglomération et points de développements sont boutées sur l'arc méditerranéen de l'hexagone français.

Dans ce livre aucune mention n'est faite au sujet des mises en camp et enfermement des personnes *nationales* / *non-(im)migrantes* par leur État d'appartenance suite à une catastrophe dite « naturelle » et/ou « technologique »[^note-catastrophe].

[^note-catastrophe]: Je renvoie ici à mes notes n°1, n°2, n°3 (sur ce blog) « Notes de lecture sur la catastrophe : “Identification of Hazards and Responses”, George E. B. Morren Jr, (1983) » ainsi que l'article, toujours ce blog, « [Tiers-Lieux : Prise sur le quotidien d’un monde catastrophé](tiers-lieux-et-catastrophes/), et aussi sur les travaux en Anthropologie de la catastrophe de Sandrine Revet.

### Qu'est-ce qu'un camp pour l'enferment ?

C'est un espace de regroupement forcé (<cite>Marc Bernardot</cite>, *p. 48*) qui permet de « fusionner des traditions de contrôle et de surveillance » « avec une fin précise » qui est la mise au ban, la destitution, l'expulsion. Avec une « grande plasticité et facilité d'application à des populations très diverses » (*p. 44*), il est pensé et déployé avec une « utilisation rationalisée de la technique […] » (*p. 49*) sur des personnes qui n'ont pas même le droit fondamental de la présomption d'innocence, ni de recours en justice, et sont mise sous le plomb de la présomption de menaces ; soit un retournement violent de la charge. Il est le reflet de mécanismes « du pouvoir, sociaux, économiques, médicaux » (*p. 76*), un abandon « du monde et des hommes de lois » (*p. 110*), issu d'un « projet global de division du monde entre ceux qui circulent et ceux que l'on enferme » (*p. 138*), […] concentrées dans une configuration particulière avec ces infrastructures, ses petites mains et ses circulations. Le camp « consiste dans la matérialisation de l'état d'exception » (*p. 69* en citant G. Agamben). Son externalisation est confiée à des  « territoires » de pays tiers avec une gestion quotidienne ordonnée à des organisations diverses. « Les camps, bien qu'organisés par l'État, nient en effet les droits fondamentaux […] » (*p. 16*).

> « Le plus extraordinaire sans doute réside en ce que le principe de l'*habeas corpus*, ce droit pour toute personne emprisonnée d'être présentée à un juge pour qu'il statue sur la validité de l'arrestation, est en partie écarté pour les personnes internées par l'administration. Si le “détenu” est enfermé par décision judiciaire, la personne “maintenue” dans une zone d'attente est “retenue”, c’est-à-dire internée par simple décision administrative » <cite>Gilles Lhuillier</cite> (Essai de définition: Institution juridique des camps) ; « La mesure classique est la détention sans charge ni procès, autrement dit la détention arbitraire. Des lois expéditives et mal conçues sont votées pour accorder des pouvoirs excessifs à l'exécutif, qui limitent les droits et libertés indiviuels au-delà des exigences de la situation. Souvent, cette perte de liberté est permanente » <cite>Lord Johan Steyn, juge à la cour d'appel de la Chambre des Lords, 25 nov. 2003</cite> (Citation d'introduction dans « Les origines coloniales : Extension et banalisation d'une mesure d'exception, <cite>Olivier Le Cour Gandmaison</cite>)

![](/assets/images/img-1.jpg)
<figcaption class="capation">Carte 2009 des camps d’enfermement pour étrangers en Europe et autour de la Méditerranée, Migreurop <a href="https://books.openedition.org/iheid/487">Annexe 1 : Européanisation des politiques d’immigration et d’asile, Chronologie simplifiée, 1999-2010</a>. Celle imprimée dans le livre dans la partie VI Documents est réalisée avec les données accumulées jusqu'en 2005</figcaption>
<br />

Il faut attendre la page 42 et une des notes de bas de page pour apprécier le distinguo proposé entre les camps d'internement « qui visent à l'expulsion et à défaut à la mise à l'écart de certaines populations civiles » et les camps de concentrations qui « participent *in fine* d'un projet de destruction des individus » (Marc Benardot, *p 42*, Les mutations de la figure des camps). Dans ce régime concentrationnaire, je n'hésite pas à placer les bateaux de transport des esclaves et les « comptoirs » *d'attente* comme le fut l'île de Gorée. Les esclaves ayant un statut très particulier, que l'administration française voulu réguler par le dit *code noir* (certains diront pour encadrer et éviter les dérives de l'esclavage), puis iels furent excluent de la vie sociale réservée aux personnes « plus humaine » dans leur quotidien une fois installé⋅e⋅s aux services d'un *possesseur*. Les camps d'enfermement se caractérisent aussi par « le type de population reçue et le style de vie sociale qu'elle [`la forme-camp`] engendre en son sein » (<cite>Henry Courau</cite>, « De Sangatte aux projets de portails d'immigration : Essai sur une conceptualisation de la “*forme*-camp”, Figures de la *forme*-camp, *p. 102*) instauré sur la base « de pouvoirs spéciaux » avec une autorité administrative (*p. 44*).

Un système camp avec technique d'enfermement se conçoit aussi avec la volonté d'effacer le souvenir et l'histoire des groupes encampés.

### Histoire brève et institution juridique d'un camp d'enfermement à la française

Je m'attendais ici à un moins un paragraphe concernant la traite Atlantique, la Caraïbe, les Antilles, les Amériques, et les révoltes et révolutions (1770 - 1802). Gilles Lhuillier, dans la partie « Essai de définition : L'institution juridique des camps », prend un départ plus récent.

« Le droit de rétention a presque toujours été reconnu en droit français. Il était parfois limité, comme par cette loi du vendémiaire, an VI [`période de la révolution française républicaine 1796-1797`], qui prévoyait un droit d'expulsion, mais interdisait à l'administration de l'exécuter elle-même » (*p. 17*). Il donne le Code pénal de 1810, article 120, comme base de l'établissement et cadre juridique de la rétention ; conçu comme « provisoire » et qui légitimera le « principe même de la rétention administrative jusqu'en 1981 ». Ce cadre sera utilisé jusque avant la première guerre mondiale en 1914, puis il sera aussi utilisé pour organiser la déportation en 1940, avec l'ajout du décret-loi du 18 novembre 1939, « œuvre de la III<sup>e</sup> république.

Dans le chapitre intitulé « Les Origines coloniales : Extension et banalisation d'une mesure d'exception » (partie II), Olivier Le Cour Grandmaison mentionne l'Algérie, les « Arabes » et les « indigènes, La Nouvelle-Calédonie, l'Indochine, les Malgaches, la Tunisie pour illustrer les variations des modalités d'enfermement et d'usage des camps en fonction des territoires, des époques, et surtout des *types de populations* visées. C'est aussi un chapitre qui introduit ce que je comprends comme une plasticité détenue par les maitres, avec aussi son esthétique dont l'interprétation est réservée et consignée par une élite, du droit en matière de camps et d'enfermement.

> « Les camps n'apparaissent pas spontanément au début du XX<sup>e</sup> siècle » <cite>Marc Bernardot</cite> (« Les mutations de la figure du camp » (*p. 43*)). 

Il prend plusieurs exemples pour illustrer « la plasticité de cette technique » (l'internement), comme en début 1914 où « civils ennemis étrangers et aussi « “suspects” nationaux, Alsaciens, nomades, clochards, “filles de mauvaises vie” » sont enfermé⋅e⋅s et le sort d'internement concentrationnaire avec travail forcé réservé aux Chinois, à des « Indigènes de colonies françaises ou à des prisonniers russes » (*p. 46*).

Puis la période de mutation des camps entre 1938 et 1963, avec les « travailleurs étrangers installés dans le camp de Rivesaltes » qui était en 2007 utilisés comme Centre de Rétention Administrative (CRA), ainsi que l'enfermement par la France des espagnol⋅e⋅s qui venaient de fuir la victoire et les massacres de la dictature d'extrème-droite de Franco, 350 000 ont été internées. Cette période fut aussi celle de la réutilisation des camps et leurs adaptations, comme ceux utilisés pour la Main-d'œuvre Indochinoise (MOI) qui furent *transformés* pour interner les personnes venues d'Afrique nord ensuite.

> « Mais si l'on considère  la seule pratique de l'internement administratif, elle n'a officiellement disparu en France métropolitaine que durant trois courtes périodes (de 1920 à 1931, de 1946 à 1957 et de 1963 à 1975) »  <cite>Marc Bernardot</cite> (Les mutations de la figure du camp, *p. 42*)

En France<marginnote>En 2022, en France, vous pouvez prétendre à aune aide financière d'État de 150€ sur vous hébergez un personne fuyant l'Ukraine. Vous pourrez aussi écoper de 135€ d'amende par la police si vous donner de l'eau à une personne venue d'un pays du continent Africain.</marginnote>, et pas uniquement là, vous serez traité⋅e⋅s différemment selon d'où vous venez et ce que vous représentez pour les personnes qui s'occupent de vous. C'est aussi un marqueur des camps et de la technique d'enfermement.

> « […] le cas de 9 000 réfugiés hongrois accueillis en 1957 dans une quinzaine de centres en France, au Havre, à Montluçon, à Nancy ou encore à Strasbourg, dont les conditions d'hébergement ont été bien meilleures. Ils ont été installés dans ces camps, militaires pour nombre d'entre eux, mais correctement réhabilités à cet effet. Des organisations comme la Croix-Rouge et la CIMADE en assurent la gestion alors qu'elles ne pouvaient qu'intervenir par des visites et des actions ponctuelles dans les camps précédents ». <cite>Marc Bernardot</cite> (La mutation de la figure des camps. La Banalisation des camps (1975 - 2006), *p. 52*).

La documentation, le récit, les droits, les violences, les obligations, sont également fluctuants en fonction des variations de critères appliquées à des *populations* d'individus pour les *identifier* et *qualifier*.

Dans le prisme métropolitain français de cet ouvrage une catégorie de personnes et leur histoire est oubliée : les personnes dites « gens du voyage ». Ces personnes sont nommées presque anecdotiquement les « Tsiganes étrangers » (*p. 19* et *p. 49*), les « Nomades » (<cite>Marc Bernardot</cite>, « Les mutations de la figure des camps », *p. 21*, *p. 45*, *p. 46*, *p. 49*). J'y reviendrais dans le paragraphe ci-après concernant le livre de William Acker.

Les personnes LGBTQI n'existent pas dans les lignes de ce livre, plus que et les camps « sidatorium » et les personnes séropositives (hors une mention unique de la « figure de l'homosexuel » (*p. 74*, Le camp et la notion de vie, <cite>Maria Muhle</cite>, et sauf peut-être dans les documents pointés en bibliographie, je n'ai pas fini de les parcourir à cette heure).

Les camps, et la technique d'enfermement, ont été rationalisés puis banalisés. Ils font désormais partie intégrante de notre histoire, de notre paysage, de notre quotidien, de notre manière d'envisager le monde. Ils ont été multipliés, ce qui conduit à « l'envisager […] en tant qu'espaces de prolifération de “libertés” singulières […] » (<cite>Alain Brossat<cite>, « Zones d'attente, centres de rétention et “Libertés” policières).

### “Libertés”, violences, notion de vie

Dans la seconde partie de ce livre, « Zones d'attentes, centre de rétention et “libertés” policières, Alain Brossat propose une très intéressante perspective d'approche et de description des camps « en tant qu'espace de prolifération de “libertés” singulières », au sens de liberté en philosophie politique classique chez Hobbes -- désir et capacité qui ne rencontre pas d'obstacle.

> « […] le corps policier ”vit” pleinement sa liberté en tant que celle-ci est la liberté “naturelle” d'un corps répressif, donc destiné à la violence − liberté d'interdire, de supprimer, de terroriser, de brutaliser, d'humilier, de décréter, etc. Ce qui a en gros pour résultat l'établissement, en ces lieux, d'une “norme” correspondant, pour ce qui est des droits des personnes et des mœurs étatiques, é ce qui se constate dans un régime policier, disons “semi-autoritaire”, pour employer un langage convenu\* » <cite>Alain Brossat</cite> (Zone d'attente, centres de rétention et « libertés » policières, *p. 62*)

\* En note n°8 de bas de pages 62 et 63 de ce livre : « Des Ivoiriens expulsés par charter en 2003 témoignent des méthodes policières auxquelles ils ont eu affaire :

> « J'y [au poste de police du terminal 2A de Roissy] suis resté cinq jours sans me laver, dans des conditions inhumaines ; nous étions près de cent personnes entassées. Seule une dizaine d'entre nous pouvaient aller aux toilettes à des horaires précis, 6 h 30, 13 h 30, 22 h 30 […] Ils nous ont mis tout nus et nous ont regardé le derrière pour voir si on avait pas de rasoir caché […]. Ils nous ont déshabillés et filmés nus […]. J'ai refusé l'embarquement mais ils m'ont frappé à coup de poing dans la poitrine », *Le Monde* (14 mars 2003).

Cette prolifération se fait conjointement avec une passivité de la population *nationale* non-enfermée / non-encampée, avec une « évaluation négative » de la part des « associations s'activant autour des questions liées au droit, les observateurs attentifs ou scandalisés » (*p. 58*), aussi avec des associations, ONG, entreprises, imbriquées dans la gestion et la vie des camps et de la technique d'enfermement.

En prenant pour appui le film « La Blessure », dont Nicolas Klotz et Élisabeth Perceval ont une entrevue retranscrite dans ce livre, Alain Brossat donne une description de la forme de cette « prolifération de “libertés policières” : « violences physiques, humiliations, privations, pressions psychologiques, mensonges, insultes, menaces, discriminations raciales, absence de soin, ect. »<marginnote>En 2021, à Calais, des militaires en mission Vigipirate s'« étaient amusés »,dimanche 19 décembre, à faire des dérapages avec leur 4x4 autour d'un camp de migrant⋅e⋅s, avant d'embourber le véhicule. Ce sont les personnes migrant⋅e⋅s ayant subi cette humiliation qui ont aidé les militaires à sortir le véhicule</marginnote> Cela par des directives données par des autorités de tutelles et une couverture des policiers par la hiérarchie et également par l'autorité politique.

> « En France, ça prévoit du matériel de puériculture pour les enfants de moins de 36 mois, car priver de liberté et enfermer des bébés, des gosses  ou des adolescents en centre de rétention ne choque plus. » <cite>Politistution</cite> (2022) *[Quand le centre de rétention administrative de Rennes se prépare à accueillir des enfants](https://alter1fo.com/cra-retention-rennes-mineur-famille-135382)*, [marché public (archive web)](https://web.archive.org/web/20220624120245/https://www.marches-publics.gouv.fr/?page=Entreprise.EntrepriseAdvancedSearch&AllCons&id=2044392&orgAcronyme=g6l)

Il s'agit de « sites “décadrés”, “hors champ” avec un « crédit de violence » non illimité contrairement aux camps de concentration nazi », avec cependant des policiers, dans les zones d'attente, qui ont « à peu près tous les droits », du moins en 2007. Une rencontre entre « des plus ordinaires des hommes et des lieux dés-exposés du public, des groupes stigmatisés, pauvres en droits, de tâches irréalisables et d'obsessions sécuritaires » (*p. 64*) avec une « routinisation locale » (*p. 66*).

Seule la Police est prise dans les développements de ce chapitre sur le camp et l'enferment comme espace de prolifération de « libertés » et de pouvoir, les autres agent⋅e⋅s de ce système ne sont pas abordé⋅e⋅s avec description. Dans un camp avec technique d'enfermement chaque agent⋅e est un fer qui tient un des pignons de la structure.

> « On nous laisse devenir des “cachetonnés”. Tous les jeunes qui sont là sont tombés dans un cercle vicieux entre morphine et benzodiazépine. La réalité est que chaque jour ici est un jour pourri. On est en train de vivre un enfer sur terre. » <cite>Politistution</cite> (2021) *[Au centre de rétention administrative de Rennes : « On est en train de vivre un enfer sur terre. »](https://alter1fo.com/centre-retention-cra-rennes-enfer-131171)*

Voici comment ne s'écrit pas l'Histoire, écrivait Pierre-Jakez Hélias[^helias]. Je serais tenté d'ajouter que voici comment sont violés des droits, voici comment (ne sont pas) orientés des esprits vers des questions et (ne) sont (pas) déployées des *vérités* avec le tabou de leur conception.

[^helias]: « Les autres et les miens. La gloire des manants. » <cite>Pierre-Jakez Hélias</cite> (Plon. 1977)

Le chapitre<marginnote>Ici, plus encore qu'à mon habitude tenace de solliciter mon entourage, j'aurais aimé discuter et échanger avec Léna Dormeau, avec un « <a href="https://groupestasis.com/cahiers-d-enquetes">Stasis. Cahier d'enquêtes. Soigner la technologie</a> ; Aussi avec Alain Giffard pour produire une approche critique et sérieuse des déliquescences d'Agamben, figure d'autorité / théorie du pire / abandon des références pesantes (voir aussi « <a href="https://iaata.info/Pour-Agamben-tout-le-monde-est-fasciste-sauf-ceux-qui-ont-reellement-ete-4907.html">Pour Agamben, tout le monde est fasciste sauf ceux qui ont réellement été fascistes</a> »).</marginnote>, de la partie II, « Le camp et la notion de vie », par Maria Mulhe, est court et très relevé essai de philosophie politique, partant d'une référence à Arendt, notamment avec un travail sur les écrits de Giorgio Agamben avec la « vie nue » qui mise à mort échappe à la juridiction humaine, y compris avec une opposition formulée à l'encontre d'Agamben et des inserts de bio-politique de Foucault.

C'est un passage de mise en discussion de « dispositifs concrets » qui permettent de décrire et d'analyser où et comment l'invocation, de la « protection de la vie de l'individu bio-politique » puis l'instauration quasi instantanée des dispositifs nécessaires dus à « l'urgence », s'entrelace avec les « technologies de pouvoir » et du *fer*-pouvoir disciplinaire pour tendre vers « l'extermination » que celleux jugées mauvais⋅e⋅s dans un moment et un contexte donnés.

Je passe sans note aucune sur la partie « Entretien autour du film “La Blessure” ».

### La forme-camp par et avec Sangatte, Lampedusa et les figurants sur le port, Guantanamo

Henri Courau, aussi auteur d'une thèse à l'EHESS en 2015 « La porte du sable. Éthnologie du camp de Sangatte : de l'exception à la régulation », maitrise parfaitement le sujet « Sangatte ». Pour décrire celui-ci puis en tirer une problématisation sur « l'humanitarisation de la cause » et « dépolitisation de la prise en charge », il s'exprime et écrit solidement et avec justesse.

Lorsqu'il aborde « la *forme*-camp », sur laquelle il a écrit un livre en 2007 (que Jean-François Barré commente [ici](https://journals.openedition.org/lhomme/28801)), je lis en me remémorant les questions que j'ai soulevées depuis le début de l'ouvrage, et les écueils, dont je vous partage quelques notes ici. Il y a dans l'écrit de Courau quelques articulations intéressantes, succinctes certainement.

> « La *forme*-camp ne possède pas de signature architecturale propre […]. Pour établir des lieux d'hébergement provisoires et de gestion, les États utilisent principalement de structures existantes et les détournent de leur fonction initiale. […] camping, zone-militaires, dancing, prisons, casernes, hôtels, gymnases, hangars, zones abandonnées, quartiers, hangars, CHRS […] camps de réfugiés, camps humanitaires, zone d'attente, CADA, ghettos, banlieues, […] aéroports, gares, ports, billes et bidonvilles. […] zones tampons ». (*p. 103 et 105*)

Cependant, au regard angles *morts* précédemment évoqués et du prisme des auteurices de l'ouvrage, je ne peux qu'être tenu à la pensée d'une conceptualisation fragile et d'une synthèse incomplète.

Un des aspects de la *forme*-camp est d'être un lieu « des humains surnuméraires » dans lequel l'individu « se dés-intègre ou se dés-assimile de l'ordre prévu des choses ». « Le déplacement de la *forme*-camp au plus proche des populations permet de conscrire dans un même lieu le centre du problème et sa résolution. En investissant ces lieux déjà inscrit dans le paysage urbain, ces *forme*-camps tendent plus à rendre invisible leur destination seconde d'entreposage » (*p. 102 et 103*). Il s'agit de « zone d'indifférence entre extérieur et intérieur, exclusion et inclusion. […]<marginnote>Le tri comme technique est aussi un marqueur de la politique de santé de santé publique dans son acception dite « moderne » en France</marginnote> La géographie des camps désigne les frontières d'une exclusion qui est de l'ordre du tri, du stockage, de déchets et du “jetable” » (*p. 104*).

Ainsi, je fais aussi l'impasse de notes sur le chapitre « Avenir de la *forme*-camp aux frontières européennes ».

#### Lampedusa

Lampedusa ? Depuis 1999, « Une sorte d'usine à fabriquer des clandestins et un centre de confinement et de vexations obligatoires […] Frontière d'Italie, frontière de l'Europe, frontière de la mer » avec une organisation catholique à la gestion depuis 2002, la Misericordia. (<cite>Frederica Rossi</cite>, traduite par Sara Prestianni et Jean-Jacques Branchu, *p. 110*). Lampedusa et son devenir médiatique et spectacle, par aussi l'arrivée d'autres agent⋅e⋅s (Nations Unies, Organisation Internationale pour les Migrations, Croix-Rouge, journaliste, touristes (déjà présent⋅e⋅s puis venant pour une autre forme de voyeurisme)).

C'est ici l'un des chapitres des plus crus et des plus durs du livre. Et surtout l'installation du gazoduc, plus grand de la méditerranée à l'époque, par Khadafi et Berlusconi, le [Greenstream](https://fr.wikipedia.org/wiki/Greenstream) (2004), révèle l'abject jeu abyssal des États et des Unions d'États, y compris pour celui des questions climatiques et écologiques, énergétiques qui sont fondamentales dans l'abord des mouvements de populations. Un tuyau enfoui qui est l'une des profondes raisons des *échanges* de « migrant⋅e⋅s rappatrié⋅e⋅s “volontaires“ » contre une fourniture de ressource énergétique carbonée, là sous les eaux et sous la mer où des gens meurent en fuyant ou sont enfermés de force : du gaz contre de la déportation (*p. 117*).

La fin du livre, 43 pages hors documents adjoints, je la traiterais lors d'ateliers avec méthodologies d'animation en éducation populaire durant des rencontres à venir. Cette partie traite des jeux entre États, avec accords et échanges économiques, pour installer des camps à l'extérieur de *chez soi* afin de ne pas trop s'encombrer du respect des droits fondamentaux dans son propre espace de responsabilités (partie III, en 4 chapitres « Guantanamo : le camp de la guerre contre le terrorisme », « Aux marges de l'Europe : la construction de l'inacceptable », « Algérie, Libye, Maroc : des camps européens au Maghreb » et « Le HCR dans la logique des camps »). J'ai déjà ici bien *trop* annoté et commenté ce livre pour le support qu'est un *simple* billet de blog. Il est temps de fournir plus de matières et de précisions.

Entre « Le retour des camps » et « Où sont les "gens du voyage" ? - Inventaire critique des aires d’accueil » il y a quelques références bibliographiques communes, pas H. Arendt en tout cas. À cette heure, précariat obligeant et temps conséquemment asservi, je n'ai pas terminé de tout comparer, ni de lire ou relire les œuvres des 2 bibliographies. Pas de <cite>Denis Peschanki</cite> (La France des camps (2002)) dans le livre de William Acker.

G. Agamben (indirectement, car en citation à Walter Benjamin, Baudelaire, Édition établie par Giorgio Agambem, Barbara Chitussi et Clemens-Carl Härle, dans le livre de 2021 de W. Acker), Didier Fassin (pour 2 refs différentes), Emmanuel Filhol (idem, différence de ref), et Olivier Le Cour Grandmaison, directeur et auteur du livre précédemment commenté, est pris en référence (Saimir MILE, « L’antitsiganisme : une tradition française », in Omar SLAOUTI, Olivier LE COUR GRANDMAISON (dir.), Racismes de France, 2020, La Découverte, p. 19).

Je mets les pieds dans le plat des pages.

Il y a surtout un point fort et similaire entre traitement externalisée des *migrations* et traitement appliqués par l'État français sur les « gens dits du voyage » : Acheter une forme de « bien être économique et social » auprès d'une *sous*-autorité dépendante politiquement et économiquement, par *budget alloué* et par *financement de politique locale de développement*. C'est le cas entre l'État français et les communes et départements, c'est le cas de la France et de l'Union Européenne avec des pays du continent africain. Et la gestion *quotidienne* des aires et camps est confiée à des tiers obligés.

## Inventaire critique des aires d’accueil. Où sont les gens « du voyage » ?

14 années après le livre traité ci-avant, « Le retour des camps ? Sangatte, Lampedusa, Guantanamo… », un pavé est écrit et lancé dans le verre des camps et de la technique de l'enfermement, la ségrégation, les infra-droits, la surveillance, la mise au ban, et le racisme, participant même à consolider l'approche par racisme environnemental porté sur des groupes d'individus. Le livre est disponible [ici](https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=pages:norae:hsociety:ou_sont_les_gens_du_voyage_-_william_acker.pdf) (pdf), sous licence Creative Commons : Attribution - Pas d’utilisation commerciale - Partage dans les mêmes conditions 4.0 International.

Les [Éditions du Commun ont grand besoin de nos aides](https://www.editionsducommun.org/blogs/actualites-evenements/nous-avons-besoin-de-votre-aide) !

Il y avait un (non)traitement *anecdotique* des gens dit du « voyage » dans le livre de 2007. Le travail de William Acker apporte d'importantes et magistrales contributions, qui aide aussi à mieux envisager camp et technique d'enfermement.

![](/assets/images/william-acker-ed-commun.png)

Partout en France, les lieux dits « d'accueil » sont aujourd'hui attribués aux personnes relevant de cette dénomination administrative. Ils se trouvent à l'extérieur des villes, loin de tout service, ou dans des zones industrielles a proximité de diverses sources de nuisances. Constatant l'absence de chiffres opposables aux pouvoirs publics sur l'isolement de ces zones, et l'exclusion des individus, le rôle de ce régime dans les inégalités environnementales, William Acker recense, département par département, et situent géographiquement et sociologiquement, juridiquement, dans un contexte historique, ces « aires ». Juriste, il cisèle avec une approche en éducation populaire de l'enchevêtrement des *souffleries du verre et des tenanciers* du droit, surtout des interprétations et mise en application. En découle une exposition d'un régime raciste, violent, ancien, rationalisé, perfectionné, institué et largement déployé. En découle l'état de faits et quotidien des gens dit du « voyage » et toute personne autour. William Acker travaille au cœur et l'ouvrage le fait d'avoir place dans le monde avec la mise en mouvement et les outils pour garantir que nos opinions ont du poids et nos actions de l’effet. « […] la puissance de la réappropriation narrative à l'œuvre » (*p. 194*).

Un très bel et puissant ouvrage, dans le sens aussi où il offre des capacités pour se mettre soi-même en action. Forgé dans une méthodologie d'enquête rigoureuse, dont certains aspects, aussi avec la technique et les outils, m'ont rappelé les travaux de Megha Rajagopalan et Alison Killing, Williman Acker écrit un livre dense, incarné, conséquent, pas uniquement de part les 300 pages, qui est à la fois une encyclopédie sur un thématique précise et un manuel d'investigation pour de nombreuses communautés et individus. Je m'en sers régulièrement dans les ateliers et formations que je donne sur l'enquête nvironnementale et dans d'autres *champs*. Je suis partisan et ne m'en cache pas. C'est pour cela que mes doigts tremblent depuis le début de mes notes partagées ici envers vous.

> « La première partie de cet ouvrage analyse le contexte historique, sociologique et politique de ces communautés et du rapport que l'État entretient avec elles. La seconde partie est l'inventaire exhaustif et cartographie' des aires d'accueil. Cet inventaire s'appuie sur des critères précis et factuels. »

Dans cette forge, William Acker use de vécu personnel, d'histoire(s) familiale(s), de récits et paroles communautaires, d'historiographie et références précises. Sa posture dans et face aux témoignages et recueils, sa sagacité sur la complexité, l'ampleur et le poids de l'étude m'ont fait beaucoup penser à <cite>Pierre-Jakez Hélias</cite> (*<u>Les autres et les miens, La gloire des manants</u>* (Plon, 1977)). J'ai été ému à la première lecture et aux suivantes. J'assume mon éloge et j'en profite pour remercier William Acker. Et les notes de bas de page et les références bibliographiques sont des hautes coutures, tant en qualité qu'en diversité et en nombre ! (sauf pour celles qui renvoient vers de publications sur de réseaux sociaux fermés (et panopticlick) d'entreprises privées qui ont tout droit de les faire disparaitre).

![](/assets/images/acker-2021.png)
<figcaption class="caption">« Et nous on continue de compter les aires d'accueil (seuls lieux légaux de vie pour beaucoup de familles) voisines directes de déchetterie, de stations d'épuration, d'usines chimiques, de sites SEVESO, d'abattoirs, de méthanisateurs ou d'autoroutes. » <cite>William Acker</cite>, septembre 2022</figcaption>
<br />

### Stigmatisation et fichage

Les français⋅e⋅s sont « abreuvé⋅e⋅s de productions  médiatiques sensationnalistes depuis des décennies ». Nomades & Bohémiens<marginnote>est devenu <i>cool</i> de se nommer ainsi pour certaine mouvance de personnes non opprimées et sans conséquences de persécution</marginnote>, Gitans, Rroms, Tsiganes, Romanichels, Manouches, Yéniches, Sintés, Oursaris, catalans du midi, Gadjés… Tant d'individus, de groupes, de personnes, qui ont vécu et savent la discrimination et la stigmatisation. W. Acker l'écrit justement, et sa précision de source et de placement dans la frise de l'histoire est chirurgicale.

> « Le terme a aussi une dimension politique […] le terme « ţigani » est historiquement lié à l’esclavage en Roumanie […] dont
les premières traces écrites remontent au XIVe siècle. Pendant cinq siècles, les Tsiganes sont réduits à la « robie » et pourront être vendus comme esclaves. Ainsi en 1818 le Legiuirea Caragea (ouvrage de droit qui contient des normes de droit civil, pénal et de procédure) prévoit le statut d’esclave des Tsiganes et son hérédité » (<cite>William Acker</cite>, *p. 26*).

Il y a bien, et par force, une hérédité des stigmates (P-J Hélias en serait aussi un descripteur) et également une hérédité générationnelle de la mise au ban. Racisme et colonialisme… avec une « schématique à une partie de l'Europe… [] », géographie : je vous écris ici encore et encore…

Les Sciences Humaines et Sociales, en sauce française, ont (re)pris le « “mot“ “Tisganes“ sous forme générique dès 1826 […] » (*p. 27*)

L'une des axiologies remarquables dans le livre de William Acker se tient ici : […] travailler la mémoire en réformant la “méthodologie du souvenir des crimes historiques de masse en déplaçant son centre de la figure de victime à celle du héros ».

Le livre qui nous occupait juste avant posait le Code pénal de 1810 comme l'un des tournants de l'enfermement dans des camps (et j'ai exprimé mes réserves sur ce point). William Acker écrit :

>  « Les premières mesures de police administratives pour tenter d'encadrer l'itinérance apparaissent en 1795 par la création d'un passeport de l'intérieur ».

Et, oui, effectivement en 1810 « ces mesures renforcent avec l'introduction dans le Code pénal d'un délit de vagabondage et en 1815 l'apparition du carnet pour colporteur » (*p. 37*).

Dès 1895 un fichage systématisé, avec participation de la « population » est effectif sur les « nomades », un recensement au « statut juridique » (*p. 40*).

> « Ainsi le 20 mars [`1810`] un recensement général des ”Bohémiens et des nomades” est lancé simultanément dans tous les départements français. Pour arrêter les caravanes […] les maires et même parfois la population […] pour relever les identités de chaque individu, leurs professions, nationalités, sexes, âges, et ”Bande d'appartenance”. […] En Juillet 1909, 7 790 “nomades” seront recensés et fichés. ». (<cite>William Acker</cite>, « Inventaire critique des aires d’accueil. Où sont les gents « du voyage », (2021), *p. 41*).

Le contrôle et le fichage est toujours, et le fut, expérimenté sur les plus *marginalisé⋅es*, « la loi votée en 1912, intitulée loi sur “l'exercice des professions ambulantes et de la circulation des nomades » (*p 43*) consacre la « non-activité »<marginnote>Sur la criminalisation de l'oisiveté et de la flânerie, voir également : « Habermas au Starbucks. Clients, oisifs et traînards dans le tiers-lieu capitaliste », Robin Wagner-Pacifici, Dans Les Politiques Sociales 2021/1-2 (N° 1-2), pages 27 à 53</marginnote>. La vie politique contraint les « nomades » à la fixation « géographique » et la loi de 1912 assortie des mesures de contrôles instaure « Leur statut de sous-citoyen […] qui n'est plus un point bloquant lorsqu'il s'agit de mourir pour la patrie ». (*p.  45*)

Les « collectifs manouches » ont une « présence attestée en France depuis le XV<sup>e</sup> siècle » (*p. 37). Le fichage, le camp et l'enfermement *à la française* sont bien des instruments de luttes contre des ennemi⋅e⋅s de l'intérieur.

> « Voilà donc deux exonymes créés par le droit français. Le premier, “nomade”, est un statut relatif à une loi de 1912. Le > statut de “nomade” est assorti d’un ensemble de contraintes juridiques discriminatoires : carnet anthropométrique, fichage, > obligation d’identifier un groupe de voyage, plaque d’immatriculation spécifique, visa obligatoire pour entrer dans une
> ville, etc. C’est sous ce statut que les « nomades » sont internés et à partir de ce fichage ethnique qu’ils sont assignés > à résidence sous la IIIe République, puis internés en camps de concentration et d’internement à partir d’octobre 1940, > jusqu’à l’été 1946.
> 
> À partir de 1948, l’administration française utilisera fréquemment l’appellation « personne d’origine nomade » qui fera l’objet de polémiques que je détaillerai plus tard. Pour le reste, le statut juridique de « nomade » est abrogé en 1969 et c’est une catégorie administrative, celle des « gens du voyage » qui le remplace. Fini les carnets anthropométriques, place aux carnets et livrets de circulations (abrogés en 2012 et 2017). La loi de 1969 est enfin complètement abrogée en 2017. » (<cite>William Acker</cite>, Nomade / Gens du voyage, *p. 30*)

L'anthropométrie, plus encore comme technique de police et moyen de fichage, doit beaucoup au français Alphonse Bertillon, père en 1882, du premier laboratoire de police d'identification criminelle et le créateur de l'anthropométrie judiciaire. 

La France<marginnote>Voir également : <a href="https://basta.media/CAF-controles-abusifs-des-allocataires-CNAF-score-de-risque-fraude-tribune-changer-de-cap">Caisse d'allocation familiale et surveillance ?</a> ; “Plus de 1 000 données par personne sont collectées. Chaque allocataire fait l’objet d’un profilage, des algorithmes déterminent des « scores de risque » de fraude et ciblent les femmes seules avec enfants, les chômeurs, des personnes handicapées, d’origine étrangère… »</marginnote>, toujours elle, se distingue aussi et encore par sa volonté de pratiquer des « tests osseux » sur les mineurs étrangers pour déterminer leur âge conditionnant leur droit, leur possibilité d'enfermement et d'expulsion.

> « Le 6 mai 2015, la Commission des affaires sociales de l’Assemblée Nationale, dépose un amendement visant l’interdiction des tests osseux utilisés pour déterminer l’âge d’un mineur (ou majeur) isolé étranger » (<cite>Gaëtane Lamarche-Vadel</cite>, Tests osseux pour les mineurs étrangers isolés, Dans Multitudes (2016)). 

Pour les USA et l'administration Trump, en 2018, ce fut des tests ADN pour réunir jusqu’à 3 000 mineur⋅e⋅s migrant⋅e⋅s avec leurs parents sans-papiers. En Belgique c'est sur les mineurs étrangers non accompagnés (MENA) qu'est réalisé le « triple test ». Il consiste en trois radiographies: orthopantomogramme (image de la dentition), radiographie du poignet et radiographie de la clavicule. En Angleterre<marginnote>Voir également : Panopticon sur les pauvres : les algorithmes cachés qui façonnent l'État-providence britannique</a>, Angleterre, Pays de Galles et Ecosse
<a href="https://bigbrotherwatch.org.uk/wp-content/uploads/2021/07/Poverty-Panopticon.pdf">ref</a>, Les algorithmes cachés que les institutions  ont utilisés pour classer, surveiller et profiler les personnes qui reçoivent des allocations à leur insu. Ces algorithmes cachés « traitent les pauvres avec suspicion et préjugés », <a href="https://www.bbc.co.uk/news/uk-57869647">ref</a> attribuent secrètement des scores de risque de fraude à 540 000 personnes avant qu'elles ne puissent bénéficier d'une allocation logement ou d'une aide à la taxe d'habitation, traitent les données personnelles de 1,6 million de personnes vivant dans des logements sociaux, afin de prévoir les arriérés de loyer, prédire la probabilité d'être sans abri, sans emploi ou victime d'abus chez plus de 250 000 personnes. Soit 250 000+ données de personnes sont traitées par une série d'outils automatisés sans consentement pour prédire si elles seront maltraitées, deviendront sans abri, seront sans travail ou seront impliquées dans la criminalité. La base de données d'analyse prédictive de sur les services de l'enfance de Bristol contient des informations sur 170 000 personnes. Ces institutions s'appuient sur le « Télésoin ».
Des dispositifs d'écoute Amazon Echo Dot sont installés dans des milliers de foyers de personnes âgées à travers le pays *sans protection supplémentaire des données*, ce qui signifie qu'Alexa envoie d'énormes quantités de données sur ces personnes au géant du commerce Amazon. Royaume-Uni va scanner les visages de enfants pour l'accès à la cantine scolaire ?</marginnote>, 2020, c'est la tentation de la reconnaissance faciale par vidéosurveillance basé sur de fichier d'individus et bracelet électronique GPS[^gb-migrants].

[^gb-migrants]: [https://www.infomigrants.net/fr/post/42482/le-royaumeuni-veut-imposer-la-reconnaissance-faciale-aux-migrants-coupables-dinfractions](https://www.infomigrants.net/fr/post/42482/le-royaumeuni-veut-imposer-la-reconnaissance-faciale-aux-migrants-coupables-dinfractions)

Stigmatisation, fichage, rationalisation, puis prolifération, et ensuite automatisation des traitements des informations des camps et de la technique d'enfermement.

### Le placement dans des « aires »

Après des siècles de persécutions, les « gens du voyage », se voient *offrir* des dites « aires d'accueils » par l'État français ; pensées, conçues, déployées avec une standardisation (*p. 14*) et mise en œuvre avec une remarquable répétition de racisme et de racisme environnemental.

Des archétypes du « non-lieu » écrit William acker en citant l'anthropologue Marc Augé avec une contrainte évidente à la « fixation » pour les personnes visées, le « “gens du voyage”, citoyen accueilli et explusé (1990 - 2021) » (*p. 67*), cela aussi, en plus du champ lexical et des atrocités évoquées auparavant, procède de « cette nécessité d'appréhender l'individu au prisme du collectif ».

Ce qui de l'invisible est révélé et décrit et mis en paroles par William Acker.

> « […] c’est cet appel au secours d’habitants des aires sur internet, dans les médias locaux ou sur les réseaux, comme ceux de Saint-Girons (09) qui écrivent en 2013 : « Une aire d’accueil a été aménagée au “Pont du Rat”. Autrefois, la SPA a été déplacée de ce site, car les chiens tombaient malades. La Mairie y a, sans honte, installé les voyageurs – un cul-de-sac où le soleil n’arrive jamais, humide, froid, insalubre, sans borne incendie, surpeuplé » (*p. 67*).

Une « urbanité » faite avec « doubles herses anti-intrusion, dispositif de vidéo-surveillance, gardien 24h/24 7j/7 […] du chantage et de la menace » (*p. 83*). Un camp ! Des camps, par la force administrative, installés en « Zones industrielles, usines, déchèteries, stations d’épuration ou autoroutes, les aires d’accueil sont particulièrement soumises à ces pollutions environnementales et industrielles » (*p 85*), un système duquel les personnes tentent d'échapper en leur propre réseau de lieux (*p. 101*).

> « […] une forme de mise à l’écart ou d’assignation spatiale des « gens du voyage » par l’encampement et une stratégie globale de dé-nomadisation par la sédentarisation. » (*p. 87*)

![](/assets/images/visio-carto.png)
<figcaption class="caption">base de données, sous forme d'une cartographie interactive, et d'un fichier à télécharger pour que chacun·e puisse faire ses propres analyses. Il est possible que cet inventaire, que nous voulons aussi précis que possible, comporte des erreurs ; n’hésitez pas à nous les signaler, ou d'ailleurs à nous contacter pour toute question. Le fichier sera mis à jour régulièrement à partir des informations nouvelles et de l’évolution de la situation. Via <a href="https://visionscarto.net/aires-d-accueil-les-donnees">Visionscarto.net</a></figcaption>
<br />

William acker écrit et décrit avec brio<marginnote>breton bri (« honneur »), l’ancien irlandais bríg (« force, audacité »), gallois bri (« haute estime, respect »)</marginnote> les menus signes, les menus gestes, qui composent la grande puissance du système camp appliqué aux « gens du voyage ». Il expose intelligiblement les rouages administratifs, les *fabrications* du droit et les plis er replis applicatifs, la chaine de hiérarchie entre État et toutes les autres entités impliquées.

> « Si certaines choses portent bien leur nom, il ne s’agit certainement pas des aires d’accueil. Cela supposerait évidemment qu’elles soient accueillantes. Au moins ont-elles la vertu de permettre une distinction tranchée entre accueil et hospitalité » (*p. 74*)

Merci William !

> « Prendre place requiert aussi de la reconnaissance. Aucune réparation n’a été engagée sur les spoliations de familles « nomades » pendant la Seconde Guerre mondiale. Pendant que les bourreaux des « nomades », le plus souvent des gendarmes français, ont continué à vivre en toute impunité de leurs crimes, recevoir des décorations et toucher leur pension militaire,
les « nomades » ont bataillé des années pour recevoir quelques indemnités misérables dues à leur internement » (<cite>William Acker</cite>, *p. 205*)

# Autres ressources

+ [Le camp d’internement des nomades de Rennes : une sortie de l’oubli](https://lesamisdemagda.fr/le-camp-dinternement-des-nomades-de-rennes-une-sortie-de-loubli/) ; [Histoire et mémoire. Le camp d'internement des nomades de Rennes](https://archives.mrap.fr/mediawiki/images/8/8b/Camp_de_Rennes_opt.pdf) (Archive du MRAP)

+ [Workshops at the No Border Camp 2022. No Border Kitchen Lesvos - No Border Kitchen Lesvos](https://nobordercamps.eu/workshops/)

+ [Zone Neutre 31 mars - Appel aux soutiens](https://ps.zoethical.org/t/zone-neutre-31-mars-appel-aux-soutiens/5580) (mars 2022, Belgique)

+ « La construction d'une politique européenne de l'asile, entre discours et pratiques ». & « L'Introuvable statuts de réfugié, révélateur de la crise de l'État moderne. » <https://www.histoire-immigration.fr/revue-hommes-migrations?page=5>

+ « Allons enfants de la Guyane. Éduquer, évangéliser, coloniser les Améridiens dans la République », Hélène Ferrarini, 2022, Éditions Anacharsis

+ « Homes Indiens en Guyane française, pensionnats catholiques pour enfants amérindiens 1948 - 2012 », Françoise Armanville, 2012 <https://web.archive.org/web/20210721135533/https://www.guyaweb.com/assets/F.-ARMANVILLE-M2-Homes-Indiens-red.pdf>

+ « Personnes trans en migration », Stuart Calvo et Giovanna Rincon (2022), dans « Atlas des migrations dan sle mondes. Libertés de circulation, frontières, inégalités » par Migreurop (2022, Éd. Amrand Colin)

+ « Anthropologie d'une catastrophe. *Conclusion* . Les tensions de l’urgence », Sandrine Revet, <https://books.openedition.org/psn/1196>

> « Considéré sous cet angle, l’humanitaire est une activité antisociale puisqu’il réfute l’idée même d’un échange : ce que je donne à un réfugié, en soins, en nourriture, en sollicitude, n’attend de sa part aucune contrepartie. Plus largement, on ne peut fonder un modèle de relations sociales sur l’assistance, ce qui me fait dire que la gratuité de l’humanitaire est « antisociale ». Une société qui se fonderait sur le modèle humanitaire ne pourrait être que totalitaire, au sens d’Arendt pour qui ce système se construisait sur une « masse d’ayants droit » (Brauman, 2006 : 215).
> 
> Face à l’idéal égalitaire d’une assistance prodiguée au nom d’une égale humanité, l’ensemble des acteurs de l’aide, qu’ils soient volontaires, fonctionnaires du FUS ou acteurs d’ONG internationales, se heurtent à la difficulté de reconnaître aux victimes leur statut d’égal. »

+ « Trouver refuge, histoires vécues par-delà les frontières », Stéphanie Besson, (2020, Éditions Glénat)

+ L’exposition Moving Beyond Borders (MBB) est un outil pédagogique, interactif et multimédia. Mise en scène par le collectif d’artistes Étrange Miroir et inaugurée à Bruxelles en 2015, elle est le fruit des connaissances accumulées ces dix dernières années par le réseau Migreurop sur les obstacles, injustices et violations des droits subis quotidiennement par les migrants dans les pays de transit, aux frontières de l’Union européenne et en Europe même. <https://migreurop.org/article2601.html>

+ Reportage video par Street Press, 2022, « Dans l'enfer d'un centre de rétention pour sans-papiers »

+ Le documentaire « Flee » (Jonas Poher Rasmussen, 2022) ; le film documentaire « Seamen's club » (Marc Picavez)

+  Échelle Inconnue. L'art politique n'est pas un plan (Arnaud Le Marchand (2013)) <https://www.cairn.info/revue-multitudes-2013-2-page-25.htm>

# Notes et références

+ Notes et pensées de rue par moi-même sur un bout de papier, juin 2022, Rennes :

> « Les centres de rétention administrative (CRA) sont ignobles et contraires aux droits des humain·e·s.
>
> Y enfermer des personnes par l'arbitraire sous l'égide d'un contrôle au faciès à un arrêt de bus, place de la république, attendant avec 2 enfants pour rentrer dans un logement très précaire dans un pays qu'iels pensaient hospitalier après la fuite face aux menaces de morts ailleurs.
>
> Être tiré·e menotté·e devant un tribunal administratif (TA) après des semaines de CRA pour se voir, face à sa famille désemparée, prononcer un jargon pédant pour une Obligation de Quitter de le Territoire Français (OQTF) immédiate et une Interdiction de Rejoindre le Territoire Français de 2 ans (IRTF)…
> 
> L'Office français de l'immigration et de l'intégration (OFII), L'Office français de protection des réfugiés et apatrides (OFPRA), et cætera. Procès Verbal (PV)…
>
> Chatiments et violences, les accronymes comme clous des bourreaux.
> 
> Je n'ai pas beaucoup de mots là. 
> Quelque part je pense à ceux d'Hannah Arendth, l'aspect banal du mal couvert par des expression toutes montées qui nous empêchent l'empathie et nous plongent dans l'absence de pensée. Les agencements entretenus par des domino-savants, des fonctions à tenir, des devoirs à remplir, une institution mécanique à faire tourner, les gens drapés. Je me suis senti figé dans les pages d'un tome des livres « Les Passagers du Vent ».
Mr M. et sa famille ont ressenti bien pire.
> 
> Cette fois ci je ne peux que décrire, retranscrire, et peut-être commenter.
> L'aveuglement et le silence tuent
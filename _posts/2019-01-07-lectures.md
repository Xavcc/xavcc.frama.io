---
title: "Le sang des cerises (tome 1), bigoudène, langue bretonne, la Commune, anarchisme..."
layout: post
date: 2019-01-07
image: /assets/images/le-sang-des-cerises.png
headerImage: true
tag:
- hsociety
- Notes
- Bretagne
category: blog
author: XavierCoadic
descritpion: Regard sur le nouvel album de François Bourgeon des "Passagers du Vent"
---

Il a y des œuvres qui vous marquent plusieurs fois dans une vie. Pour moi la série de bande dessinées « Les passagers du vent »<sup>[wikipedia](https://fr.wikipedia.org/wiki/Les_Passagers_du_vent)</sup> est de cette trempe.

Pré-adolescent je tombais par surprise dans les affaires familiales sur « La fille sous la dunette »[^1] ansi que « Le comptoir de Juda »[^2] et « L'heure du serpent »[^3]. Je sais pas encore si hier et aujourd'hui des parents placeraient leur confiance dans leur enfant de 12 ans pour lui offrir ces heures de decouvertes.

Les magnifiques planches de François Bourgeon<sup>[wikipedia](https://fr.wikipedia.org/wiki/Fran%C3%A7ois_Bourgeon)</sup> m'ont toujours fasciné ; la puissance des personnages et de leurs histoires à Travers l'Histoire m'ont fait relire ces bd très régulièrement depuis 25 ans ; le rôle de femmes dans cette histoire et leur influence sue l'éthique et la morale m'interroge toujours à chaque fois ; la documentation poussée et les références fines et rafraîchissantes me ramènent toujours vers plus de (re)découverte, de curiosité et d'âbimes sur notre société actuelle.

> « _Par deux fois, les « Passagers du vent » ont été menacés de censure. Ils avaient même été inscrits sur une liste d’ouvrages qu’un député voulait faire exclure des librairies pour n’être plus vendus qu’en sex-shop. Plus récemment, une association de parents d’élèves, téléguidée par un célèbre mouvement religieux de l’extrême-droite américaine avait réussi à faire sortir des CDI de l’éducation nationale un certain nombre d’ouvrages dont, une fois encore, « Les Passagers du vent » (le décret de l’inspecteur d’académie responsable a été annulé par Jack Lang, alors ministre de l’éducation, dès que nous lui en avons donné connaissance)_ »[^4].

Conditions de femmes, la lutte contre l’esclavage, traite des noirs, guerre de Sécession, enfant métis né d’une esclave et d’un planteur... voilà des sujets qui, parmi tant d'autres, faisait le paysage a explorer dans ces bandes dessinées.

Le nouvel album « Le sang de cerises⋅», sorti en 2018, continue ces plaisirs. Et peut être pour cause d'actualité sociale et politique, peut être pour raisons d'embruns de l'histoire bretonne dans cet épisode, j'ai été plus touché qu'à l'habitude. 

Après quelques premières lectures j'ai sans attendre mis en disponibilité ce livre 1 sur inventaire.io[^5]. J'ai également voulu vous partager quelques appréciations qui ont germé lors de cette lecture (dont le fait que la nudité, féminie et masculine, soit moins présente que dans les tous premiers épisodes).

## Un bande dessinnée foisonnante

Une adolescente bretonne, arrive à Paris en février 1885 de son plus beau costume familial vêtue. Sur le chemin qui la mène de la gare à Montmartre, où l’attendent ses maîtres (employeurs), elle croise les obsèques de Jules Vallès, le communard et défenseur intransigeant du peuple. Là, Klervi fait la connaissance de Zabo (personnage central du précédent opus) et du docteur Lukaz. Seule, ne parlant que le Breton et comprenant à peine le français, bien moins encore l'argot parisien de l'époque. Dépouillée par des personnes qui lui reprochent d'être « tête à poux » (argot: breton, bretonne) dans la rancœur de l'écrasement sanglant de la commune ; puis aidée par Zabo, anarchiste au grand cœur, elle  rejoint, pour y être servante, la maison de sa destinée de classe sociale, en longeant l’immense chantier du futur Sacré-Cœur, symbole d'un Paris en mutation et d'une lutte idéologique. Trois ans plus tard, Zabo, qui se fait appeler Clara, retrouve Klervi dans des circonstances violentes, la soustrait des mains d’un souteneur et l’accueille chez elle.

Les premières planches vous bousculent sans attendre que vous ayez absorbé les années qui sépare chaque opus de la serie (1979, 1980, 1981, 1982, 1984, 2009, 2010, 2018)[^6]. L'argot parisiens et la langue bretonne se confrontent dès les premières bulles et les réfèrences historiques jaillissent comme pour réveiller en vous un âtre oublié ou en apathie. 

Dès l'apparition de Klervi j'ai pensé au « Cheval d'orgueil » de Jakez Helias[^7] qui décrit de façon détaillée la vie d'une famille pauvre de paysans bigoudens après la situation historique la BD de Bourgeon à la suite de la première mondiale. Là encore la culture et l'image de la Bretagne sont le terrains de critiques, de reproches et de polémiques, masquant à peine de tensions de classes sociales comme dans « Le cheval couché »[^8]. J'en songerais plusieurs nuits ensuite avec entremelés les souvenirs des débats « Nuit Debout » VS « Jour Couché » en 2016 et 2017 un peu partout en France[^9].

En tout début d'histoire, deux bulles, entre bretons - chouans - royalistes - priars, m'ont accrochées au point que je ressente le besoin de creuser plus en amont avant de vous en écrire quelques retours. Je vous les cites espérant vous appâter : 

> Lukaz « _[...]Je souffre de constater combien les Versaillais ont su tirer parti du vieil antagonisme des villes et des campagnes. Quinze ans après les faits, ouvriers, paysans se détestent toujours, faisant chacun le jeu de ceux qui les exploitent_ »

> Clara « _Ce serait encore pire si la Légion Bretonne, que Thiers appelait de ses vœux, avait pris forme à temps pour participer au massacre_ »

> Lukaz « _Diviser pour mieux régner ! La tactique est ancienne mais quasi impalpable pour qui sait caresser dans les sens du poil "la bêtise au front de taureau" !_ »

Jules Vallès, la Commune, La sociale, l'internationale, droite nationalite et Paul Déroulède<sup>[Wikipedia](https://fr.wikipedia.org/wiki/Paul_D%C3%A9roul%C3%A8de)</sup>, Bakounine et lutte contre les lois de soummisions de femmes, le club républicains de la jeunesse de Bretagne et Vendée qui militait pour l'abolition de l'esclavage, les haines des camps, les tensions intestines... en quelques pages Bourgeons plante une ambiance marquée et marquante à l'aide de personnes intriguant⋅e⋅s. N'ayez craintes des dialogues en bretons ou en argot parisien de l'époque, des notes et traductions vous apprendront beaucoup à la fin du livre. 

Bien qu'il ne soit pas cité dans cette BD, jai évidemment pensé au natif de Pontivy (56, Morbihan) Ange Guépin<sup>[Wikipedia](https://fr.wikipedia.org/wiki/Ange_Gu%C3%A9pin)</sup> et ses luttes pour les progrès de l'humanité, l'abolition de l'esclavage, le féminisme. Je crois que cette émanation de ma mémoire s'est produite en réponse au nombreuses références à Louis Jules Trochu<sup>[Wikipedia](https://fr.wikipedia.org/wiki/Louis_Jules_Trochu)</sup>,général breton dont les gardes mobiles (mobots) canardaient sur la place de l'Hôtel de Ville à Paris, symbole de _fausse idole du moment_[^10] et illustrations du _diviser pour mieux régner_.

Dans les planches suivantes vous croiserez entre deux bulles Eugène Pottier<sup>[Wikipedia](https://fr.wikipedia.org/wiki/Eug%C3%A8ne_Pottier)</sup>, Jean-Baptiste Clément<sup>[Wikipedia](https://fr.wikipedia.org/wiki/Jean_Baptiste_Cl%C3%A9ment)</sup>, Lafargue, Vaillant, Felix Pyat<sup>[Wikipedia](https://fr.wikipedia.org/wiki/F%C3%A9lix_Pyat)</sup>. Puis vous passerez devant les fondations de « Notre-Dame des Briques » qui _accouchera d'un Sacré-Cœur dont les visées expiatoires insultaient les communeux_. Là vous n'en serez qu'au tout début de l'aventure...

Plus tard ce sera au tour de l'évocation renoncement d'Adolphe Thiers<sup>[Wikipedia](https://fr.wikipedia.org/wiki/Adolphe_Thiers)</sup> et l'élection du Maréchal Mac-Mahon[^11], perçu comme _la victoire l'odre moral_ et _le massacreur de la Commune comme président_, de vous mener dans un tripot parisien après avoir parcouru la Grande Expo Universelle, les quartiers en « rénovation » (gentrification n'existait pas alors), les jardins ouvriers de ceux qui triment à leur propre exil. 

Certainement que vous serez séduit⋅e par les Champs de Klervi ou l'accordeon instrument qui fait peur au curé, peut être intrigué⋅e⋅s par les nombreux mystères qui peuplent les deux femmes de cette histoire et leurs combats, peut être inspiré⋅e⋅s par toutes les briques d'Histoire et de culture, souvent populaire, qui colorent cette œuvre.

Il ne vous reste plus qu'à vous procurer « Le sang de cerises » par emprunt, don ou achat, pour mieux comprendre ce titre énigmatique. 

## Remerciements

+ La Reine des Elfes pour ses contributions à ce billet blog en corrigeant des fautes via ces [premiers pas](https://framagit.org/Xavcc/xavcc.frama.io/merge_requests/49) sur framagit


## Notes et références

[^1]: Fiche wikipedia de « La fille sous la dunette », 1979, <https://fr.wikipedia.org/wiki/La_Fille_sous_la_dunette>
[^2]: Ficheiche wikipedia « Le comptoir de Juda », 1981, <https://fr.wikipedia.org/wiki/Le_Comptoir_de_Juda>
[^3]: Fiche wikipedia « L'heure du serpent »,1982, <https://fr.wikipedia.org/wiki/L%27Heure_du_serpent>
[^4]: ref <https://www.actuabd.com/Francois-Bourgeon-Les-Passagers-du>
[^5]: livre disponible en prêt sur inventaire.io ici : <https://inventaire.io/items/a295a78be16b5a714e74fd6a9d6aac5a>, un site libre et open source permettant le partage de livre et nourrissant les données ouvertes et libres
[^6]: dates de publications des albums : <https://fr.wikipedia.org/wiki/Les_Passagers_du_vent#Albums>
[^7]: Resumé du livre de Jakez Hélias <https://www.babelio.com/livres/Helias-Le-cheval-dorgueil/282253>
[^8]: Le cheval couché de Xavier Grall, 1977, <https://fr.wikipedia.org/wiki/Le_Cheval_couch%C3%A9>
[^9]: Souvenirs d'un parcours et d'efforts dans les communs pendant près de 3 mois en 2016 <https://medium.com/@XavierCoadic/le-tour-de-france-dans-les-communs-en-infographie-4935d12b2b18>
[^10]: Victor Hugo l'a marqué d'une définition cinglante : « Trochu, participe passé du verbe Trop Choir. » Sous les apparences d'un chef courageux et dévoué, c'était un caractère ambitieux, dissimulé et irrésolu. Répétant sans cesse « j'ai mon plan », il était convaincu que toute résistance et toute défense de Paris étaient vaines. <https://www.universalis.fr/encyclopedie/louis-jules-trochu>. La chanson du Plan Trochu <https://fr.wikisource.org/wiki/Le_Plan_de_Trochu>
[^11]: 24 mai 1873, léction de Mac-Mahon <https://www.herodote.net/24_mai_1873-evenement-18730524.php>
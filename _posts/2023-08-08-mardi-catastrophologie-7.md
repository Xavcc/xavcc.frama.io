---
title: "Mardi catastrophologie : votre fournée de ressources (n°7)"
layout: post
date: 2023-08-08 06:40
image: /assets/images/8f5177e37c09d1d0.jpg
headerImage: true
tag:
- hsociety
- Catastrophologie
category: blog
author: XavierCoadic
description: C'est un fait social !
---

> « Est fait social toute manière de faire, fixée ou non, susceptible d'exercer sur l'individu une contrainte extérieure ; ou bien encore, qui est générale dans l'étendue d'une société donnée tout en ayant une existence propre, indépendante de ses manifestations individuelles », *Les Règles de la méthode sociologique* (1895) <cite>É. Durkheim</cite>

Je tente :

La « catastrophe » n'existe que dès lors qu'une personne au moins la déclare, la dit. Et pour autant c'est une contrainte imposée, souvent aux paramètres tenant des héritages, avec ses existences propres indépendantes de nos manifestations individuelles qui exerce sur nous, par ses ruptures portées, une (re)configuration de nos habitudes, de nos rapports à l'autres, des contions de *ce qui fait nous des humain⋅e⋅s*[^human]

[^human]: *Qu'est-ce que l'humain 4/5, 4ème volet de regard à partir de 3 points de vue (neurobiologie, paléoanthropologie et science cognitive)* sur une question à la charnière de la théorie de l’évolution, de la préhistoire, de la philosophie, *XavCC*, notes de lectures, 2020 <https://xavcc.frama.io/humain-4/>

Pour cela, il semble nécessaire a minima un intérêt qui soit pré-requis entre cette personne ou ce groupe de personnes et « l'évènement qui vient provoquer une rupture » (Sandrine Revet), une empathie est nécessaire.

De là, avec d'autres intrants, sont conçus des critères qualificatifs et normatifs avec des médiations à différents niveaux de communications et de disséminations (exemple : EM-DAT et autres <https://xavcc.frama.io/mardi-catastrophologie-5/>)

De la « catastrophe » il y a celleux qui l'expriment, et la “planifient”, dans un advenir et celleux qui mesurent / *monitorent* (dialogue entre prévention/prévision/intervention "post et après)

La catastrophe  « naturelle », ou « technologique » (Polluants, Génétique/OGM, Vaccin/ARN, etc), sociale, économique…

C’est souvent là où iels pensaient avoir fait preuve d’une lucidité ou d’une prescience supérieures que les commentateurices de l’actualité et les chroniqueurs ET chroniqueuses du changement sociétal se sont fait le relais des stéréotypes et des préjugés les plus ordinaires. Derrière un ensemble de craintes dont il n’y a pas lieu de nier les fondements.

Démystifier les rapports aux techniques, aux dogmes, aux sciences, à la science comme idéologie (cf *Les seules choses qui soient immuables en ce monde, ce sont les lois de la nature qui gouvernent notre univers. C’est la raison qui est l’arbitre ultime, et non la morale et ses caprices* cité dans *L’enfant du futur au prisme de la science-fiction*, par <cite>Marika Moisseeff</cite> <https://books.openedition.org/ined/17204> & <cite>Habermas</cite>: *La technique et la science comme « idéologie »*, 1974 )  et distinguer plus clairement le registre de la fiction, celui de la réflexion théorique et celui de la connaissance empirique – pour tirer de chacun le meilleur parti – devraient être les préalables à tout débat serein.

(Pour paraphraser <cite>Fabrice Cahen</cite> dans *La fin du mâle ? Don de sperme et récits d’anticipation en France (années 1950-1970)* <https://books.openedition.org/ined/17229>)

+ [Livre]
  + *Habiter la pollution industrielle : expériences et métrologies citoyennes de la contamination*,
<cite>Christelle Gramaglia</cite>
  + *Le vécu au quotidien et ethnométhodes des ressortissants congolais de R.D.C. en France*, <cite>Dinyendje Longelo J.</cite>
  + *Recherches en ethnométhodologie*, <cite>Harold Garfinkel</cite>, Puf (2020)

+ [Actu] **French uranium mine leaves 20 million tonnes of radioactive waste in Niger** <https://www.rfi.fr/en/africa/20230124-french-uranium-miner-leaves-20-million-tonnes-of-radioactive-waste-in-niger>

> “Niger’s northern town of Arlit has been left wallowing in 20 million tonnes of radioactive waste after a uranium mine run by French company Orano (formerly Areva) closed down. People living in the area are exposed to levels of radiation above the limits recommended by health experts”. 

+ [Actu] **Science activism is surging – which marks a culture shift among scientists** <https://theconversation.com/science-activism-is-surging-which-marks-a-culture-shift-among-scientists-207454>

> "by Scott Frickel and Fernando Tormos-Aponte, on how more and more scientists are becoming activists and the tactics and strategies collectives etc. are using"

+ [Actu (thanks Snoro)] **The War on Climate Activism Is Reaching Dangerous New Heights** <https://www.thenation.com/article/activism/climate-movement-terrorism-repression/>

> "rowing trend of state-sanctioned violence and repression against climate activists in the United States and other countries, such as the United Kingdom and France.
>
> Between 2012 and 2022, according to the nonprofit Global Witness, at least 1,733 people around the world were killed, as Tortuguita was, while trying to protect their land. That’s an average of one death every two days"

+ [Carto / Data Viz (Thanks to breadandcircuses)] **Average Daily Temperature on Earth − 1980 - 2023 Summer period**

![](/assets/images/41c77b53e1e72220.jpeg)
![](/assets/images/climate-analyzer.png)

+ [Carto / Data Viz (thanks to Mikko Tuomi)] **A Tale of Humanity and Habitability** <https://globaia.org/habitability>

> Throughout history, humans have predominantly inhabited a surprisingly limited range of Earth's diverse climates, typically characterised by a mean annual temperature of around 13°C, i.e. human climate niches. This distribution may be a result of the human temperature niche, which is influenced by fundamental constraints. These findings show that, depending on future population growth and climate warming scenarios, between 1 and 3 billion people could find themselves living outside the climate conditions that have supported human flourishing for the past 6,000 years over the next half-century.

![](/assets/images/8f5177e37c09d1d0.jpg)

> "Human climate niches in green and regions of mean annual temperature > 29 °C in purple.
>
> The MAT > 29°C is an important threshold as beyond this point, humans are exposed to historically unprecedented levels of #heat, with an increased frequency of potentially lethal maximum temperatures over 40°C and physiologically challenging wet bulb temperatures (WBT) over 28°C, posing serious threats to health and survival."

+ [Actu] **Burning pavement, scalding water hoses: Perils of a Phoenix heat wave** <https://www.washingtonpost.com/climate-environment/2023/07/13/phoenix-heat-wave-conditions/>

+ [Carto / Data Viz] **Active Fires in Canada − 11/07/2023** <https://cwfis.cfs.nrcan.gc.ca/interactive-map>

![](/assets/images/ative-fires-canada-11-07-2023.png)
<br />

+ [Actu] **Plus 10 millions d’hectares partis en fumée au Canada** <https://www.ledevoir.com/societe/794652/plus-10-millions-d-hectares-partis-en-fumee-au-canada>

+ [Actu (thanks tuxom)] **‘Zombie fires’ in the Arctic: Canada’s extreme wildfire season offers a glimpse of new risks in a warmer, drier future** <https://theconversation.com/zombie-fires-in-the-arctic-canadas-extreme-wildfire-season-offers-a-glimpse-of-new-risks-in-a-warmer-drier-future-209666>

> "First, as the organic-rich Arctic soils dry up because of changing climate conditions, they can burn slowly and release vast amounts of smoke into the atmosphere.
>
> Second, soil fires that spread underground are harder for firefighters to tame and extinguish, thus demanding more resources for longer periods of time. Firefighters in Alberta, Canada, where carbon-rich peatlands are common, have been dealing with fires smoldering to depths dozens of feet underground in 2023. Because peat fires can make the ground unstable, using heavy equipment to excavate the fire areas also becomes risky.
>
> Finally, these soil fires don’t die easily. Recent research finds that Arctic soil fires can smolder through the winter and reignite during early spring when temperatures rise, hence the nickname ‘zombie fires’.”

+ [Actu] **Europe heatwave 2023 – live: Italy braces for 49C as wildfire sparks evacuation in Spain** <https://www.independent.co.uk/news/world/europe/europe-heatwave-2023-weather-map-b2375816.html>

> "At least 500 are forced to flee their homes in Spain, authorities say"
>
> [NDLR] *Des températures élevées sont attendues dans le sud de la Californie, en Arizona et dans le sud du Nevada ce week-end et jusqu'à lundi. La Vallée de la Mort pourrait connaître des températures proches de 130°F / 54°C*

+ [Publi (Thanks to AGF poem producer)] **Decoloniality in Eastern Europe: A Lexicon of Reorientation** <https://kuda.org/sites/default/files/docs/Decoloniality%20in%20Eastern%20Europe_A%20Lexicon%20of%20Reorientation.pdf>
  +  i.e ""Decentering humanitarianism from Southeast Europe*", <cite>Čarna Brković</cite>

> "The Red Cross…should change the character of its activity, reject the notion of caritativeness and develop its activity into a system which establishes more objectively the existing needs and mobilizes forces necessary to satisfy them. It should fight against inequality of men, against discrimination
of all kinds, particularly racial. (RCY, 1976: 12)"

+ [Actu] **‘It’s pillage’: thirsty Uruguayans decry Google’s plan to exploit water supply** <https://www.theguardian.com/world/2023/jul/11/uruguay-drought-water-google-data-center>

> "Country suffering its worst drought in 74 years, with government even mixing saltwater into drinking supply. […] Last month, the world’s biggest pulping plant started operations in Uruguay, the third such mill in the country. The new plant, run by the Finnish company UPM to create raw material for paper, is forecast to use 129.6m litres (34m gallons) of water a day, and releases effluent into a local river. UPM said it treats the effluent before release, and constantly monitors the water quality in the Río Negro.
>
> A UPM spokesperson told the Guardian: “Uruguay is facing the worst drought in a century. Within this framework, UPM’s operations in Uruguay have no connection with the drought that is occurring. The drinking water consumed in Montevideo comes from the Santa Lucía River. None of the pulp mills installed in Uruguay are linked to this river. This challenging climatic situation cannot be associated in any way with the forestry sector.”"

+ [Actu] **How a Saudi firm tapped a gusher of water in drought-stricken Arizona** <https://www.washingtonpost.com/politics/2023/07/16/fondomonte-arizona-drought-saudi-farm-water/>

> "Lax rules let the foreign-owned company pump water from state land to grow alfalfa for the kingdom’s cattle. After almost a decade, the deal is in jeopardy."

+ [Actu] **A new front in the water wars: Your internet use** <https://www.washingtonpost.com/climate-environment/2023/04/25/data-centers-drought-water-use/>

> "In the American West, data centers are clashing with local communities that want to preserve water amid drought"

+ [Actu (merci clochix)] **Sécheresse en Espagne : quand le numérique pompe l'eau** <https://www.francetvinfo.fr/meteo/secheresse/secheresse-en-espagne-quand-le-numerique-pompe-l-eau_5841185.html>

> « Pour développer le metavers et l'intelligence artificielle, le géant américain Meta souhaite implanter un nouveau centre de données en Espagne, dans la région de la Castilla-La Mancha. Un projet très gourmand en eau qui pose question dans cette province régulièrement touchée par la sécheresse. […] En 2022, en Irlande, capitale européenne des centres de données, les serveurs ont crée des coupures de courant et consommé un demi-million de litres d’eau par jour, et jusqu’à cinq millions de litres par jour quand il fait chaud.
>
> Aux Pays-Bas, les installations d’un grand groupe américain ont elles, utilisé quatre fois plus d’eau que prévu... si bien que le gouvernement a décidé de limiter l'accès à l'eau pour ces infrastructures avec un moratoire. »

+ [Actu] **Crise de l'eau en Europe : la situation est plus grave que ce que l'on pensait** (15/12/2022) <https://www.nationalgeographic.fr/environnement/2022/12/crise-de-leau-en-europe-la-situation-est-plus-grave-que-ce-que-lon-pensait?utm_source=pocket-newtab-bff>

> « La sécheresse historique de cette année ne constitue en réalité que la partie émergée de l'iceberg : de nouvelles études révèlent le déclin alarmant du niveau de l’eau dans les aquifères du continent. »

+ [Actu] **En Uruguay, la moitié de la population n’a plus d’eau potable et reproche à Google de vouloir « piller » les dernières gouttes** <https://www.huffingtonpost.fr/environnement/article/en-uruguay-la-moitie-de-la-population-n-a-plus-d-eau-potable-et-reproche-a-google-de-piller-les-dernieres-gouttes_220702.html>

+ [Actu] **Secrets toxiques sous les eaux du barrage de Sauviat** <https://www.mediapart.fr/journal/france/040823/secrets-toxiques-sous-les-eaux-du-barrage-de-sauviat>

> « Dans l’est du Puy-de-Dôme, le barrage hydroélectrique de Sauviat accumule depuis 120 ans des centaines de milliers de mètres cubes de sédiments gorgés de métaux lourds issus de l’activité industrielle de la région. De quoi provoquer une catastrophe s’ils étaient libérés. Ici, l’assèchement des rivières pourrait être une solution plutôt qu’un problème. »

+ [Opinion] **La guerre de l’eau et des données. De l’eau tech aux low techs.** <cite> Olivier Ertzscheid</cite> <https://affordance.framasoft.org/2023/08/guerre-eau-et-des-donnees/>

# Notes et références
---
title: "Investiguer les adaptations aux changements climatiques : Méthodes et principes (Des ressources à prendre)"
layout: post
date: 2023-09-08 05:20
image: /assets/images/ETI_workshop-climate-adaptation_hero.jpg
headerImage: true
tag:
- hsociety
- bioassay
- Catastrophologie
category: blog
author: XavierCoadic
description: Avec Tactical Tech & Sweden's government agency for development cooperation. État d'esprit, méthodes et outils pour investiguer les adaptations et vulnérabilités aux changements climatiques.
---

Ce jeudi, préparant la permanence d'accueil au Biohackerspace la Kaouenn Noz[^centrifuge]<sup>,</sup>[^dormance], j'apprends de ma collègue Laura que notre guide de préparation et d'animation d'atelier est [disponible en français](https://exposingtheinvisible.org/fr/ateliers/climate-change-adaptation/). "[No Disaster Is Natural: How investigating climate change adaptation could make a difference](https://exposingtheinvisible.org/en/articles/investigating-climate-change-adaptation/) lui aussi sera prochainement lisible en français.

Yupeeee…

[^centrifuge]: Exemple de préparation de ressource wikifiée, travail en cours, pour cette rentrée « [Suis-je créateurice d'une centrifugeuse à faire accoucher ?](https://notecc.kaouenn-noz.fr/doku.php?id=pages:norae:hsociety:homo_note-technique-suis-je-createur-de-centrifugeuse-a-accoucher) »

[^dormance]: autre exemple de travail en cours « [Note sur la dormance des graines](https://wiki.kaouenn-noz.fr/hors_les_murs:hack2eaux:note_sur_la_dormance_des_graines) »

> « Cet atelier présente aux participant⋅e⋅s les principes de base de l'adaptation au changement climatique. Il fournit des conseils et des astuces pour enquêter sur l'exposition aux impacts climatiques, les vulnérabilités et les capacités d'adaptation que les participant.e⋅s peuvent appliquer à leur contexte local. Les participant⋅e⋅s seront sensibilisé⋅e⋅s à la relation complexe entre le changement climatique, les politiques publiques et la sécurité public, ainsi qu'aux ainsi qu'aux éventuels méfaits liés à l'adaptation au changement climatique. »

Avec Tactical Tech, et plus encore avec l'[équipe Exposing The Invisible](https://kit.exposingtheinvisible.org/fr/), nous avons travaillé ensemble sans relâche depuis 2019. Tout en prenant en compte, entre autres axes, la [Lutte performative, Activisme artistique, théorique et transféministe](https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=norae:hsociety:lutte_performative_activisme_artistique.pdf).

Ce printemps 2023, après avoir encadré une semaine de formation sur le terrain pour le MSc Strategy and Design for the Anthropocene (dirigé par <cite>Alexandre Monnin</cite>, dont le livre « Politiser le renoncement » vient de paraître (*Éditions divergences*)), j'avais animé 2 sessions de 3 jours à Rennes d'[ateliers d'investigation aux adaptations aux changements climatiques](https://notecc.kaouenn-noz.fr/doku.php?id=atelier:note_retour_atelier_investigations_changements_climatiques_2023). Ces sessions étaient destinées à des personnes n'ayant pas les bons papiers selon les administrations en vigueur. Réalisé avec l'aide de Tactical Tech / [Exposing the Invisvible](https://exposingtheinvisible.org/) et avec financement du Sweden's government agency for development cooperation (SIDA). Cela se faisant dans la continuité de [Louzaouiñ Graines de luttes 2022](https://wiki.kaouenn-noz.fr/hors_les_murs:reprises_des_savoirs:louzaouin_graines_de_luttes).

Nous avions ensuite, quelques semaines après, improvisé dans un bar *du quartier* un atelier “Getting Into Fights With Data Centres” (<cite>Anne Pasek</cite>) <https://emmlab.info/Resources_page/Data%20Center%20Fights_digital.pdf>.

Aujourd'hui, nous préparons un cours pour l'Academy of Arts Architecture & Design de Prague (AAAD / UMPRUM) dans le cadre du cursus [Planet B 2023: Aquatic Alliances](https://www.planetb.fun/apply).

---

## Notes et références
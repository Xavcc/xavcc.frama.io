---
title: "Notes de lecture sur la catastrophe : “Identification of Hazards and Responses”, George E. B. Morren Jr, (1983) 1/3"
layout: post
date: 2022-07-26 17:40
image: /assets/images/pen-bron-2021.jpg
headerImage: true
tag:
- hsociety
- Notes
- Catastrophologie
- Anthropologie
category: blog
author: XavierCoadic
description: "Regards sur la catastrophe"
---

*Images d'en-tête : Couleurs de marais salants de Guérande à Pen Bron, une bouffée d'Océan, 2021. Par Xavier Coadic sous licence CC BY SA 4.0*

En préparant un article (à venir) sur mon blog, dans lequel la « catastrophe » est un concept important d'articulation, j'ai découvert *Interpretation of Calamity From the Viewpoint of Human Ecology*, Kenneth Hewitt & Al. (1983) ; avec une partie de cette œuvre qui m'a interpellée : PART III, p. 229 p. 297 (Mickael Watts “Epistemology ant the human-environment problematic”, “Labour, Nature and Social reproduction” ; Paul Susma, Phil o'Keefe, Ben Wismer “Global Disasters, A Radical Interpretation” ; George E. B. Morren Jr, “A General Approach to the identifiaction of hazards & responses” p. 295).

J'ai été projeté dans les pages de ce livre au travers d'un travail sur un corpus de texte impliquant Sandrine Revet, 

> « On attribue généralement à Kenneth Hewitt (1983) la première critique, dans le champ des études sur les catastrophes, de la construction de l’édifice technocratique sur la base d’idéaux types ou de conceptions « mythiques » de la société. », <u>S. Revet</u>, <cite>*Le sens du désastre. Les multiples interprétations d’une catastrophe "naturelle" au Venezuela*, 2010</cite>

Critique ciselée de la vision dominante portée par la recherche sur les risques dits « naturels », l'ouvrage est riche de mise en perspectives, notamment en ce qui concerne les conditions météorologiques et les rendements du blé en Union soviétique et au Canada, une vue historique de ce qui est nommé sous-développement et des risques en Irlande et l'impact d'une réponse à la sécheresse en Afrique australe, au Sahel et dans les grandes plaines des États-Unis. Des tremblements de terre en Chine, USA, Afghanistan et Turquie et l'éruption de la Soufrière de 1976 en Guadeloupe. Le tout remit en contexte les aléas « naturels » dans les prismes pris du développement économique et des changements sociaux-politiques. La plupart des études de cas sont issues de la scène rurale et agricole. Ce livre porte une vision singulière, au début des années 1980, de l'importance cruciale de la production alimentaire et de l'impact considérable, et parfois calamiteux, que le gel, les inondations, les tempêtes et la sécheresse ont sur le bien-être et les conditions de vies de millions de personnes et sur la stabilité du système économique international ; avec tout autant de facteurs d'influences sur « la catastrophe » qui proviennent de sphères économiques, politiques, sociales, architecturales.

Il y a aussi quelques lignes claires de critique du colonialisme, y compris d'une justification « écologique » du colonialisme.

![](/assets/images/couv-hewitt.png)
<figcaption class="caption">Capture d'écran de la couverture du livre prise dans researchgate.net, signature de Kenneth Hewitt</figcaption>
<br/>

Je vous en partage quelques notes de lecture, notamment sur la partie III, écrite par George E. B. Morren Jr, "A General Approach to the Identifiaction of Hazards and Responses” et plus spécifiquement dans ce premier billet de notes de la partie “Difficulties and hazards”, ici notes écrites en français.

+ [Un peu de contexte](#un-peu-de-contexte)
+ [A general approach to the identification of hazards and responses](#a-general-approach-to-the-identification-of-hazards-and-responses)
+ [Difficulties and hasards](#difficulties-and-hasards)
+ [Remerciements](#remerciements)
+ [Notes et références](#notes-et-références)

Un second billet de notes à venir portera sur le développement “The features of responses processes” où George E. B. Morren Jr expose 13 axes de frictions, 13 “assumptions”, qui sont autant de focus lors des « réponses apportées face à la catastrophe », autrement dit des hypothèses de problématisation et de réponses. J'en transcrirais le texte en anglais afin de participer à l'intérêt pédagogique de cette ressource. 

Pour être transparent, je me lance tout autant dans cet exercice en espérant trouver des chemins de compréhension sur le *pourquoi* le chapitre de Morren a retenu plus singulièrement mon attention que les autres passages de cette œuvre (question à laquelle je n'ai pas réponse au démarrage de l'exercice). Ces notes sont donc des relectures avec un *pas à pas* effectué après 2 premiers passages, une conséquence, pour exemple, est que j'ai décidé en fin de première salve d'écriture de scinder en deux billets mes notes de lecture(s).

Je prends aussi ici et maintenant un passage précurseur à celui de George E. B. Morren Jr, dans le même ouvrage, concernant le caractère « de la catastrophe vue comme extension d'un quotidien » par les auteurs.

À la page 263 de cet ouvrage *Interpretation of Calamity From the Viewpoint of Human Ecology*, 1983. Kenneth Hewitt & Al. DOI:10.2307/214106, une introduction dans la partie III *Alternative Frameworks*, au développement 14 :

“The first important element to note is that disaster is an event (or series of events) which seriously disrupts normal activities (Cisin & Clark 1962).

This emphasis on normalcy highlights the needs to observe *disaster as an extension of everyday life*.”  *Global Disasters, a radical Interpretation*, Paul Susman, Phil O'Keefe and Ben Wisner

## Un peu de contexte

Né à West Brighton, Staten Island, en 1939, George Edward Bradshaw Morren Jr. est diplômé de l'université de Columbia en 1960. Il a servi dans la marine américaine de 1960 à 1963, puis est retourné à Columbia où il a obtenu son doctorat en 1974. Il a mené des recherches sur la tribu Miyanmin dans les Highlands de Papouasie-Nouvelle-Guinée qui ont été publiées dans une monographie intitulée *The Miyanmin : Human Ecology of a Papua New Guinea Society* (Iowa State University Press).
M. Morren a été l'un des premiers à utiliser la télédétection par satellite pour des recherches sur le terrain financées par la National Science Foundation de 1991 à 1994. Avant que les appareils GPS[^gps] ne soient disponibles pour le *grand public*, il a utilisé leurs précurseurs pour étudier les changements humains et environnementaux en Papouasie-Nouvelle-Guinée[^obituary].

Il a été « l'un des fondateurs de l'anthropologie environnementale » et avait jusqu'en 2021 un page web dédiée dans le Morris Museum[^morris-museum].

L’anthropologie environnementale est une branche de spécialité du domaine de l’anthropologie qui joue un rôle actif dans l’examen des relations entre les humain⋅e⋅s et leur environnement dans l’espace et dans le temps. L’anthropologue américain Julian Steward (1902-1972) est considéré comme l'un des créateurs de l'anthropologie de l’écologie culturelle, notamment par son rôle important dans  le développement de théories sur l'évolution socio-culturelle et les structures culturelles. Une voie aussi empruntée, avec plus de nuances, par Leslie White (1900-1975). Steward a officiellement formulé le cadre théorique et méthodologique de base pour l’écologie culturelle dans les années 1950-1960. La transformation de l’écologie culturelle en anthropologie écologique a eu lieu avec les contributions d'anthropologues tel que John Bennett, Roy A. Rappaport et Andrew P. Vayda. Et ceci n'est pas une liste exhaustive.

L'un des points sous-tendu ici sur lequel portent mes intérêts est l'anthropologie de la catastrophe venant s'allier avec les humanités écologiques, et l'amorce du détachement du caractère obligatoire de « l'exceptionnel » dans l'événement pour qualifier la catastrophe.

Goerges E. B. Morren Jr. est décédé chez lui à l'âge de 72 ans, le 29 septembre 2011[^deces]. Sur son angle géographique et terrain de prédilection, notamment Pacifique / Papouasie, comme me l'a fait remarquer le compte twitter “Ethnolectures”, Morren est dans la génération des chercheurs qui sont allés dans cette région lorsque l'africanisme et l'amazonisme « passent de mode ». La question d'un choix heuristique particulier par Morren reste en suspend au moment de mes notes de lectures.

Pour Véronique Fillol et Pierre-Yves Le Meur, 

> « La “ruée ethnographique” vers la Papouasie-Nouvelle-Guinée à partir des années 1960 constituera ensuite un moment clef dans le renouvellement de problématiques anthropologiques classiques, mais dont les effets se feront sentir au-delà de cette seule discipline, et ce, « entre autres, dans les domaines de la parenté, des échanges ou des logiques politiques (voir Kuper, 1993 : 92-93 ; Holy, 1996 : 90 et sq.) ». <cite>*[Terrains océaniens : enjeux et méthodes](https://horizon.documentation.ird.fr/exl-doc/pleins_textes/divers16-05/010064301.pdf)*, p.10, éditions L'Harmattan, 2014.</cite>

J'ai aussi un point d'interrogation importante : pourquoi et comment George Morren est passé de l'ethnologie / anthropologie & anthropologie de l'écologie humaine (en Papouasie notamment) à la synthèse d'une approche générale sur les dangers et les réponses face à la catastrophe dans un ouvrage majeur sur le sujet, avec Kenneth Hewitt, géographie et études environnementales.

> “While this is fair comment, geographers as well as anthropologists have some claim to be the nomads, foragers and bricoleurs of the human sciences; inhabiting those interstices between conventional subject areas, borrowing appropriate theory and data where they have none of their own.” <u>Ellen, R</u>. <cite>(1988). *Persistence and change in the relationship between anthropology and human geography. Progress in Human Geography*, 12(2), 229–262. <doi:10.1177/030913258801200204></cite>

À la fin des années 1970, la « nouvelle géographie » (Ellen, R. 1988) a trouvé des « résonances ailleurs » (Ellen, R, *Op. cit.*) que dans son champ disciplinaire avec la « nouvelle anthropologie » (Ardener, 1971), la « nouvelle archéologie « (Clarke, 1972), la « nouvelle ethnographie » (Sturtevant, 1964).

> “At Oxford, geography, ethnology and prehistory constitute a single faculty. At University College London, the links between anthropology and geography established by Forde, after he had moved there from Aberytwyth, continue. As recently as 1975, social anthropology was introduced into the syllabus of one British University (St Andrews), not as part of sociology (ashad become the pattern by the mid-1960s), but as part of geography.” <u>Ellen, R</u>. <cite>*Op. cit.*</cite>

Morren fut, dans les années 1980 à 2000, Professeur d'anthropologie et d'écologie au Département d'écologie humaine, Collège Cook, Rutgers[^rutgers]. Et il laissa aussi quelques traces de discussion par courriel via liste de diffusion de l'Australian National University[^mihalic] concernant les dénominations d'enfant⋅e⋅s héritées des époques impériales et coloniales.

“The Rural Ecology of the British Drought of 1975-1976“ de George E. B. Morren, Jr, en 1980, avaient écrit avec les contributions et relectures de Kenneth Hewitt, P. A. Vayda, Bonnie McCay et Bill Torre

## A general approach to the identification of hazards and responses

Dans cette première partie de la synthèse, Morren passe en revue les études sur les causes des catastrophes. Son introduction par une approche générale nous informe sur son positionnement.

> “This chapter is a tentative approach to the natural hazards field by an ecological-anthropologist […] My immersion in the technical litterature is only partial. Indeed, I have been more attracted to popular and historical writting on disaster − journalistic accounts, memorial volumes and memoires” <cite>*p. 284*</cite>

Morren situe son point de vue sur la catastrophe et prend aussi des précautions *d'usages* sur les matériaux dont il y a disposé pour rédiger sa synthèse.

> “I have two main concerns. The first is to develop a way to identify and describe 'realistically' the main environmental problems confronting particular groups of people. We need to emphasise that, at any given time, people face a variety of problems; that these interact in significant ways, and that we need to account for a variety of their problem characteristics. The latter include but are not limited to initiating agents or triggers such as geophysical events, and predisposing factors such as settlement patterns. They require some emphasis on group history. Economic and political 'causes' of environmental problems are frequently overlooked and, as Hewitt noted (Ch. I), studies of hazards appear to be particularly susceptible to this kind of over-sight.”

Morren appuie ici, en se référant au à Kenneth Hewitt auteur principal, sur les faits de négligences des « causes » *non-naturelles* des catastrophes et les négligences subséquentes dans les études de risques concernant les catastrophes. Cela me semble être en 1983 un point important dans l'amorce d'une approche nouvelle de la catastrophe ; et aussi une référence tacite à *Ecological dimensions of environmental hazards* de Burton et K. Hewitt (1974).

> “Thus, particular hazard events must be put in a social, material and historical context including other hazard events and environmental problems and responses. Indeed, I think that it is a common error to view disasters as events. Rather, they should be viewed as parts of processes with spatial, temporal and other properties (see Vayda 1974, Burton & Hewitt 1974, p. 256)” <cite>*p. 285*</cite>

Continuant à dérouler son fil de synthèse, Morren soulève un angle de *contradiction légère*, avec les auteurs du passage précédent, concernant l'inscription dans le quotidien de la catastrophe, que sont Paul Susman, Phil O'Keefe et Ben Wisner ; passage qui porte sur le fait d'envisager la catastrophe comme un événement. Morren invite explicitement à renforcer l'approche fournie par les auteurs avec considérations des catastrophes « comme des parties de processus ayant des propriétés spatiales, temporelles et autres ».

Ici nous avons Paul Susman qui est journaliste et archéologue, Phil O'Keefe géographe, et Ben Wisner géographie et sciences de la terre.

> “The links between geography and anthropology have arisen from a substantial overlap in their subject matters, a polymorphic and wideranging conception of their respective disciplinary enterprises, a fieldwork tradition and a common set of historical origins. All of these have been reflected over the years in professional and institutional structures, which in turn have influenced intellectual preoccupations.” <u>Ellen, R.</u> <cite>*Op. cit.*</cite>

### Difficulties and hasards

Goerges E. B. Morren Jr ouvre cette première partie de sa synthèse depuis une revue de lecture de standards sur le sujet des sécheresses et dont « bon nombre des difficultés peuvent être étendues au domaine des risques en général ». Il évoque l'“obfuscation” autour de l'identification du phénomène naturel connu, et pose ainsi ce qu'il identifie comme « […] une sorte de mystification du problème général posé qui comporte plusieurs caractéristiques ». Morren expose 4 arguments critiques.

**(1) Il n'est accordé qu'une attention superficielle à la multitude de problèmes humains fondamentaux qui peuvent accompagner, interagir ou être déclenchés par des événements de risques naturels (perte des moyens de subsistance, des moyens d'existence, des abris, des réserves et de la sécurité)**

> “One of the reasons for this misdirection and misallocation of research effort is the social structure of the field; just as in medicine, professionals make decisions affecting other professionals, or else advise politicians on the details of programmes affecting professional activity aimed at the public. The members of communities concerned only rarely have a role in shaping the outcome of professionally directed research or action programmes.” <cite>*p. 285*</cite>

Pour Morren, les situations dans lesquelles un modèle de perturbation et de réorganisation sociale (plutôt qu'environnementale) semble découler d'une catastrophe sont également préoccupantes. Les catastrophes apparaissent porter un pouvoir de déstabilisation politique. Il prend en exemple les tremblements de terre en Chine orientale, séisme de 1976 à Tangshan, où gouvernement de la république populaire de Chine, sortant juste de la période dite de la révolution culturelle, refusa l'aide internationale. Autre exemple en Iran, séisme de Tabas, 

Je relève aussi que les deux séismes sont mentionnés, par Morren, avec leur année d'occurrence et le pays de référence, sans aucune autre précision géographique ou d'autres propriétés. J'ai donc ajouté les « villes de références ».

Il souligne aussi qu'il est tout autant erroné d'« attribuer les causes de tels "désastres politiques" à des événements naturels (cf. Barkun 1974) que d'attribuer d'autres conséquences humaines exclusivement à des événements naturels ». Dans une autre part de la synthèse écrite par Morren, il invite à ne pas nier le rôle des catastrophes et  « des autres problèmes locaux aigus » dans la stimulation de réarrangements et de nouveaux arrangements sociaux et politiques. 

En lecteur *commentateur* a posteriori, j'ai fortement pensé ici au film « **Et la vie continue** » réalisé par Abbas Kiarostami (1991), et aussi  « **Où est la maison de mon ami ?** », racontant l'histoire d'un père et son fils avant et juste après le tremblement de terre qui avait ravagé le nord de l'Iran en 1990. Un regard incarné, humble et intime depuis l'intérieur des personnes et des lieux qui ont vécu une catastrophe.

**(2) On accorde encore moins d'attention à la manière dont les gens ordinaires, en tant qu'individus ou membres d'un petit groupe, entreprennent - et doivent entreprendre - de faire face à ces difficultés et face aux problèmes connexes.**

Morren prend ici en référence “CONSEQUENCES OF EARTHQUAKE PREDICTION ON OTHER ADJUSTMENTS TO EARTHQUAKES”, J. Haas, D. Mileti (1976) pour critiquer les recherches par sondages sur des populations, avec biais de sélection et de conception, par rapport à des dangers perçus ou à des situations de catastrophe potentielles. Il suggère de prendre une voie orientée par, et non pas vers, les activités des « gens ordinaires » dans une localité. Puis il entre dans une posture de suggestion. Les populations locales et leurs voisin⋅e⋅s étant, selon les mots de Morren, beaucoup plus à même de savoir comment faire, notamment plus que les personnes extérieures. Ces « gens ordinaires » connaissent plus ou moins leurs propres priorités, et sont bien sûr « placés » pour réagir rapidement, avec des avantages pendant les premières phases d'une catastrophe qui peuvent être critiques, non seulement en ce qui concerne la survie individuelle, le sauvetage et le soulagement de la souffrance, mais aussi en termes de mobilisation des groupes et de minimisation des lésions écologiques, économiques et sociales permanentes à long terme.

La publication de J. Haas et D Mileti, porte sur une étude la prédiction des tremblements de terre, une « technologie émergente » à l'époque, spécifiquement par United States Geological Survey (USGS) en 1976 pour la région de Los Angeles, l'affinage des prédictions et probabilités, les mesures de réponses préparées en amont, l'agence avait clairement indiqué qu'elle n'annonçait pas une prédiction officielle de tremblement de terre. Deux sismologues non gouvernementaux réputés avaient déclaré que leur interprétation des données disponibles les amenait à une probabilité de 25% d'un tremblement de terre. Au début du mois d'août 1977, l'USGS avait publié la première prédiction officielle d'un tremblement de terre, avec une probabilité de 50% pour d'une magnitude de 7,0 ou plus de frapper une zone désignée en septembre ou octobre 1979. À la mi-novembre 1978, environ neuf mois avant le séisme prévu, la prédiction a été révisée et la probabilité qu'il se produirait au cours du mois de septembre 1979, avec une magnitude de de 7,1 à 7,4 (échelle de Richter). Enfin, soixante jours avant l'événement prévu, l'USGS a encore révisé la prédiction. Le séisme se produirait pendant la première semaine de septembre 1979, avec une magnitude attendue de 7,3. La probabilité était alors de 80%. Le précédent dans la région était Sylmar earthquake en 1971.

> « Compte tenu de ce scénario de prévision des tremblements de terre informations sur la prévision des séismes, quelles seront probablement conséquences pour la communauté "cible" ? Les conséquences, quelles qu'elles soient, découleront et se refléteront dans les actions des organisations mobilisées. » <u>J. Haas, D. Mileti</u>

La prédiction du tremblement de terre peut provenir d'une agence gouvernementale et/ou d'une source universitaire publique ou privée. Le traitement de l'information implique aussi d'autres groupes d'acteurs. L'étude porte une analyse a posteriori, avec la mise en œuvre d'une « série de questions (24 au total) conçues pour exploiter la réponse probable de toute organisation à une prédiction de tremblement de terre spécifique ». Avec aussi l'établissement des actions (par les médias), des mesures (autorités publiques et services publiques), ajustements (secteurs privés et ceux précédemment cités) sur la période 1976 - 1979.

Le tremblement de terre dévastateur n'a pas eu lieu en 1979. Un grave crise de dépression économique locale, liée à la spéculation pendant la période de prévision de la catastrophe, a lourdement impactée les populations. L'expérience acquise de cette période, et du modèle de prévision et diffusion, et l'étude du phénomène, a influencé d'autres localités et systèmes d'organisations en responsabilité ailleurs. Un séisme eu lieu en 1983, le [Coalinga earthquake](https://en.wikipedia.org/wiki/1983_Coalinga_earthquake) (puis 1984, 1987, 1989, avril et juin (× 2) 1992).

**(3) À l'opposé, l'accent est généralement mis sur les solutions technologiques et sur les réponses « à large spectre » appuyées par les hautes instances gouvernementales et les agences internationales, ou d'autres types d'« d'agents extérieurs ». Cette focalisation s'accompagne d'une image de toute évidence trompeuse des coûts, des avantages, de la pertinence et de l'efficacité des « grandes solutions ».**

Morren pointe des effets très courants de l'aide extérieure lors de désastre :

+ empêchement de l'effort individuel et l'initiative locale
+ empêchement des personnes de sauver leurs biens personnels ou de conserver d'autres biens
+ création de concurrence sur les sources locales de nourriture et de matériaux, etc. 

Il prend en exemple le tremblement de terre et de l'incendie de San Francisco en 1906 où « l'insertion apparemment illégitime de troupes fédérales » et les évacuations forcées sont venues empêcher les personnes de sauver leurs biens mobiliers et de prévenir ou de combattre les incendies dans leurs habitations et leurs entreprises. 

En deuxième exemple, le séisme de 1980 en Italie, du 23 novembre 1980 en Irpinia je présume, car il n'écrit aucune autre information, pour faire l'assertion de « problèmes similaires rencontrés » à ceux de San Francisco en 1906. 

Troisième exemple, l'éruption du volcan La Soufrière dans les Antilles françaises, celle de 1976 je présume également. Pour Morren « la plus grande mission à laquelle ont été assignées les forces de sécurité, dont beaucoup avaient été envoyées d'Europe, a été d'empêcher les populations rurales de se faufiler (*sneaking back*) sur leurs terres pour s'occuper du bétail. ».

+ « [Il y a 30 ans... la Soufrière. Synthèse des rapports de 1975-1977, IPGP](https://www.ipgp.fr/~beaudu/soufriere/IPGP75-77.html) ».
+ « [À propos de la polémique “Soufrière 1976”...](https://www.ipgp.fr/~beaudu/soufriere/forum76.html) »

Un autre effet de l'aide extérieure est de rendre permanentes certaines des conséquences d'une catastrophe et de conjurer (*to exorcise*) la capacité locale à faire face aux problèmes ultérieurs. 

« La description par Erikson (1976) des logements d'urgence fournis par le ministère américain du logement et du développement urbain aux survivant⋅e⋅s de la tempête de Buffalo Creek (Virginie occidentale) en est un exemple particulièrement aggravant ». Ici, date, pays, région, et précision géographique sont données par Morren. « […]selon Erikson, a été la destruction de la « communauté », c'est-à-dire du réseau de liens interpersonnels caractéristique de la société dans les petits hameaux de montagne. L'affectation plus ou moins aléatoire des survivants dans des camps de caravanes, sans parler du passage au bulldozer et de l'incendie des maisons endommagées ou détruites, a rendu permanente cette perte potentiellement réversible ». Un point de fort sur lequel Morren reviendra en toute fin de sa synthèse avec un exemple en Turquie.

 Pour clore l'argumentation de sa troisième affirmation, Morren prend l'angle de la prescription : 
 
 > « Le type de recherche que j'ai suggéré au point (2) ci-dessus produirait des informations utiles pour concevoir des programmes externes visant à encourager, renforcer et compléter les initiatives individuelles et locales de manière efficace. Cela constituerait une alternative aux programmes dont on sait maintenant qu'ils ont l'effet inverse ».

**(4) Dans l'étude des causes des catastrophes, on accorde trop d'importance aux systèmes terrestres et atmosphériques, en tant qu'agents initiateurs immédiats, au détriment de l'examen des facteurs de causalité socioculturels (y compris politiques et économiques). En d'autres termes, je dirais qu'il existe une relation directe entre le développement (y compris le changement social et la modernisation) et la vulnérabilité aux dangers.**

C'est la quatrième et dernière affirmation dans cette première partie “Difficulties and hasards”.

Morren rentre frontalement dans ce qu'il nomme le « blâme de la victime » : les cultivateurs itinérants sont accusés de l'érosion et des glissements de terrain en conséquences, le surpâturage est la faute des pasteurs  qui favorise la désertification, les péri-urbains choisissent de vivre dans les plaines inondables, la haute fertilité et natalité dans le tiers-monde.

Très clairement, sans ambiguïté, il affiche et dénonce une tendance à une approche de l'écologie comme moyen de domination pour stigmatiser des autochtones sous prétexte de *préservation de la nature (sauvage et protectrice de la catastrophe)*, justification de système colonial pour de nombreux pays / États.

Il mentionne aussi un « grain de vérité nécessaire dans les milieux scientifiques » avec prudence et mise en critique, puis invite à une « vision plus large » pour se concentrer sur le fait qu'il s'agit de « réactions de gens ordinaires à des actes de développement sur lesquels ils n'ont aucun contrôle : système de commerce et expansion de l'agriculture, urbanisation, demande de main-d'œuvre, etc. »

Ouvrant ensuite sur une stratégie d'approche de la catastrophe avec les aspects « non-naturels », par un « éventail de risques environnementaux peut être considéré avec le plus grand profit comme des conséquences de l'activité humaine ; plus précisément, comme des conséquences de l'interaction entre l'humain⋅e et son environnement », Morren propose 4 voies pour aborder cette amorce de nouvel angle pour la recherche : 

**(a)** Que les dangers proviennent directement et principalement de l'activité humaine. Cette situation est parfaitement illustrée par les dangers propres à la vie urbaine-industrielle, notamment les accidents industriels et l'environnement affectant les travailleurs et les voisins, les accidents de transport, les atteintes au développement telles que les délocalisations urbaines et rurales, les dépressions économiques et autres. 

« La guerre, qu'elle soit primitive ou moderne, n'est pas en reste. Ces risques manifestement non naturels montrent l'utilité d'éliminer la distinction "naturel" / "artificiel", car les réactions humaines évoquées dans de nombreux cas sont identiques, et parce que ces risques artificiels interagissent avec les facteurs naturels de manière significative. » Bien qui publié l'année suivante, 1984, j'ai l'intuition de Morren s'appuie ici sur ces travaux qui seront publiés dans “Warfare, culture, and environment” (que je n'ai pas lu au moment de la rédaction des notes ici écrites)

**(b)** Que l'acuité des effets dommageables est liée à l'ampleur de l'exploitation et de la modification du milieu par l'humain⋅e et à la densité de la population. Les exemples ruraux sont particulièrement pertinents, car leurs composantes non naturelles sont souvent masquées par la prédominance des facteurs naturels biotiques, terrestres et atmosphériques impliqués, par exemple les parasites, les sols ou les précipitations. 

« L'acte de développement le plus élémentaire, le défrichement des forêts, a été impliqué dans un large éventail d'événements comportant des risques directs et des dangers au plus long court : inondations, vents et répartition des chutes de neige, vulnérabilité aux tremblements de terre, glissements de terrain, sécheresse et désertification, etc. »

Pour Morren, le développement commercial dans les pays à prédominance agricole force la production de subsistance vers des terres marginalisées. Cette politique encourage également la sélection de cultures et de techniques qui contribuent à des « risques nutritionnels de différents degrés d'extrémité : mauvaises récoltes et famine ou régimes alimentaires riches en amidon et sous-alimentation chronique ».

**(c)** Ce développement tend à favoriser la dépendance et la spécialisation des individus et des communautés, réduisant ainsi leur capacité à réagir efficacement ou réduisant la gamme des variations normales de l'environnement auxquelles ils sont capables de faire face par eux-mêmes.

**(d)** La participation de « groupes extérieurs supra-locaux » peut rendre permanents les effets d'un problème local de courte durée. 

+ Le sort des réfugiés ou des "évacués permanents", exemple les survivants de Buffalo Creek.

Il s'agit d'un phénomène général dans la mesure où le retour à un semblant d'état "normal" avant la catastrophe est empêché ou inhibé par les efforts officiels de secours et de restauration. 

+ tremblement de terre de 1976 au Guatemala (Riding 1977) : l'arrivée de grandes quantités de céréales nord-américaines a réduit le marché des produits alimentaires locaux disponibles et a causé plus de dommages économiques aux agriculteurs que le tremblement de terre lui-même

+ les gelées meurtrières de 1972 sur les hauts plateaux de Nouvelle-Guinée (Waddell 1975) : les efforts de secours peuvent avoir perturbé de façon permanente des modèles étendus de relations intergroupes. 

> « Des travaux récents dans le domaine des risques ont commencé à redresser l'importance excessive accordée aux faits naturels. (par exemple, White 1974 ; Burton et al. 1978) bien que de nombreuses questions que je soulève ici restent à étudier de manière systématique. », <u>Morren</u>.

En résumé et conclusion de cette première partie, « en plus d'identifier les problèmes et les dangers environnementaux de manière plus réaliste », Morren invite également à réorienter l'attention sur les réponses. « Ceci doit être fait de manière à nous permettre de relier leurs caractéristiques aux caractéristiques des problèmes. Une approche en cours d'élaboration concernant un large éventail de problèmes environnementaux est en cours d'élaboration. »

Je traiterais dans un très prochain billet la seconde partie “The fetaures of response processes”, qui conclue le livre *Interpretation of Calamity From the Viewpoint of Human Ecology*. Un travail sur 13 “assumptions” développées par George E. B. Morren Jr.

## Bibliographie (avec les pieds) incomplète de G. E. B. Morren Jr

*39 occurences dans [Worldcat](https://www.worldcat.org/search?q=au%3AMorren%2C+George+E.+B.&&dblist=638&fq=)*

+ “Settlement strategies and hunting in a New Guinea society”, Columbia University, 1974

+ “An Evolutionary Approach to the Southeast Asian Cultural Sequence [and Comments and Reply].” Hutterer, Karl L., Jim Allen, S. A. Arutiunov, D. T. Bayard, D. K. Bhattacharya, Bennet Bronson, M. A. Chlenov, et al. Current Anthropology 17, no. 2 (1976): 221–42. <http://www.jstor.org/stable/2741534>.

+ “From Hunting to Herding: Pigs and the Control of Energy in Montane New Guinea” (1977)

+ “The Rural Ecology of the British Drought of 1975-1979“ de George E. B. Morren, Jr (1980) <http://www.jstor.org/stable/4602534>

+ “Reconsidering Violence in Simple Human Societies: Homicide among the Gebusi of New Guinea [and Comments and Reply]”
Bruce M. Knauft, Martin Daly, Margo Wilson, Leland Donald, George E. E. Morren Jr., Keith F. Otterbein, Marc Howard Ross, H.U. E. Thoden van Velzen, and W. van Wetering (1987)

+  "Warfare on the Highland Fringe of New Guinea: The Case of the Mountain Ok." (1984) In Warfare, Culture, and Environment, edited by B. Ferguson, 169-207. Orlando, Fla.: Academic Press.

+ “Tairora Culture: Contingency and Pragmatism. JAMES B. WATSON“ (1987)

+ “"The Taro Monoculture of Central New Guinea." Human Ecology 15:301-315.” Morren, George E. B., Jr., and David Hyndman (1988)

+ “A preliminary vernacular guide to the vegetation of Northern Telefomin District, Papua New Guinea”, George E B Morren & Al. Rutgers University (1992)

+ “Traditional Ecological Knowledge: Wisdom for Sustainable Development.˘ Nancy M. Williams and Graham Baines, eds
George E. B. Morren Jr. (1995) <https://doi.org/10.1525/aa.1995.97.1.02a00520>

+ “When the Chips are Down: Individual and Community Responses to Environmental Problems” (2007)

## Remerciements

+ La personne qui anime le compte twitter Ethnolectures

# Notes et références

+ *Le sens du désastre. Les multiples interprétations d’une catastrophe "naturelle" au Venezuela*, Sandrine Revet, p. 42-55 <https://doi.org/10.4000/terrain.13936> ; OpenEdition <https://journals.openedition.org/terrain/13936#ftn33>.

[^gps]: « Mis en place par le département de la Défense des États-Unis à des fins militaires à partir de 1973, le système avec vingt-quatre satellites est totalement opérationnel en 1995 » [Wikipedia](https://fr.wikipedia.org/wiki/Global_Positioning_System)

[^obituary]: Kimble Funeral, Dr. George E. B. Morren (2011) obituary <https://www.thekimblefuneralhome.com/obituary/1276365> ; Library of Congress <https://id.loc.gov/authorities/names/n85064849.html>.

[^morris-museum]: page web avec erreur 404 du Morris Museum <https://morrismuseum.org/dr-george-morren-miyanmin-people/> ; screen d'archive de cette page sur Internet Archive [ici](https://web.archive.org/web/20210601000000*/https://morrismuseum.org/dr-george-morren-miyanmin-people/)

[^deces]: Page web [legacy](https://www.legacy.com/us/obituaries/mycentraljersey/name/george-morren-obituary?id=22710199)

[^rutgers]: archive de Rutgers <https://catalogs.rutgers.edu/generated/nb-ug_0305/pg3920.html>.

[^mihalic]: voir [list](https://mailman.anu.edu.au/pipermail/mihalic/2000-November/author.html#207)
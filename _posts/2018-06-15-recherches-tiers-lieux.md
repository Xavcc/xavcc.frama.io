---
title: "Recherches action et documentation : Tiers-Lieux Libres et Open Source pour répondre aux enjeux critiques"
layout: post
date: 2018-06-15 22:10
tag:
- Consensus
- Design
- Tiers-Lieux
- Libertés
image: /assets/images/Monts_d_Arée.jpg
headerImage: true
projects: true
hidden: true # don't count this post in blog pagination
description: "On ne résoud pas un problème avec les modes de pensées qui l'ont engendré"
category: project
author: XavierCoadic
externalLink: false
---

Voici un accès central à mes travaux axés sur les points suivants :

+ [Enjeux critiques et Communs](https://movilab.org/wiki/Enjeux_critiques_et_Communs)
  + Libertés et sécurités informatique
  + Prévision et prévention des accidents corporels
  + Libertés et égalité au delà des genres et des classes
  + Accessibilité, perméabilité, handicaps
  + Bioéthique et sciences ouvertes
  + Biononymous et bioprivacy

#### Tables de contenus

1. [Protoypages](#prototypages)
2. [Publications](#publications)
3. [Interventions](#interventions)
   + [2019](#2019)
   + [2018](#2018)
   + [2017](#2017)
   + [2016](#2016)
   + [2015](#2015)
   + [2014](#2014)
4. [Notes](#notes)
5. [Contributions](#contributions)

## Prototypages

+ [MediaLab Numérique en Commun](http://movilab.org/index.php?title=Medialab_NEC_2018), Nantes 2018
+ [Indicamp](https://movilab.org/index.php?title=IndieCamp) Kerkors et Névez de 2016 à 2019

<iframe id='ivplayer' type='text/html' width='640' height='360' src='https://invidio.us/embed/oNhMK6sgLFA' frameborder='0'></iframe>
   
## Publications

+ [Tiers-Lieux et artificialité : La faute à pas de sciences ?](/tilios-sciences)

+ [Contribution à la mission société numérique inclusive : Les écueils de gouvernance à éviter](/mission-inclusion-numerique-1)

+ [Contribution à la mission société numérique sur l'inclusivité : Fracture, communautés, accéssibilité, lieux, territoire, paysage, pour les publics](/mission-numerique-2)

+ [Tiers-Lieux : Hackerspace Design Patterns 2.0 en VF](/tilios-design)

+ [Tiers-Lieux : Exemple d'un Code de Conduite](/tilios)

+ [De l’hypothèse de la documentation comme technique de résistance et du wiki comme objet de ces résistances](https://www.sens-public.org/article1375.html), 1er mars 2019, Revue Sens Public, Iniversité de Montréal

+ [L’économie circulaire open source en France](https://circulatenews.org/2017/06/leconomie-circulaire-open-source-en-france/), 2017, Ciruclate Economy News by Ellen Mac Arthur Foundation

+ [Biomimétisme, le vivant comme modèle](https://issuu.com/kaizen-magazine/docs/k28-issu-provisoire-fabrique) Published on Sep 1, 2016  − Kaizen n°28 (septembre-octobre 2016)

## Interventions

### 2019

### 2018

+ Forum de usages coopératifs de Brest 2018 : [Tiers-Lieux Libres et open source et la collaboration pour répondre aux enjeux critiques](/tilios-forum)

+ Pas Sage en Seine : Google et l'idiot du village − Choisy le roi

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://video.passageenseine.fr/videos/embed/eddda14c-69a2-470c-8a3a-068ab3e59f49" frameborder="0" allowfullscreen></iframe>


### 2017

+ Rennes 2017 : Tiers-Lieux libres et open source pour repolitiser la ville
+ Sciences Po Lyon, 2016, pour le réseau des territoiralistes : « Tiers-lieux, tiers-scientifiques et territoires »
+ Mairie de Concarneau 2016 : Convaincre Une Mairei d'ouvrir un FabLab
+ Biennale Internationale du Design 2017

À Saint-Étienne

  + [Restitution de l'Expérience Tiers-Lieux Fork The World : et après](https://invidio.us/watch?v=VXSs6vBp0a8) − Fin de la restitution, après l'expérience où allons-nous ?
Invités :
- Sylvia Fredriksson (Pôle Recherche de la Cité du Design)
- Xavier Coadic (Le Biome, Rennes)
- Antoine Burret (Sociologue)
Conférence dans le cadre de l'expérience Tiers-Lieux Fork the World, Biennale Internationale du Design 2017, le mardi 4 avril 2017.

<iframe id='ivplayer' type='text/html' width='640' height='360' src='https://invidio.us/embed/VXSs6vBp0a8' frameborder='0'></iframe>

### 2016

+ [Biennale Économique de Rennes 2016 : Où nous mènent les nouveaux modèles d'organisation](/next-eco/)

### 2015 

Knopée 2015 - Biomimétisme : 3,8 milliards d'années de R&D

<iframe id='ivplayer' type='text/html' width='640' height='360' src='https://invidio.us/embed/4etWDrP00lU' frameborder='0'></iframe>

### 2014

La Fing − 6mn of serendipity (fOSSa 2014, Institut national de recherche en informatique et en automatique) Rennes

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://framatube.org/videos/embed/d2454b13-a682-4cba-b645-df1c44d77372" frameborder="0" allowfullscreen></iframe>


## Notes

+ #THSF [Toulouse HackerSpace Factory 2017](https://framastory.org/story/xavcc/thsf-toulouse-hackerspace-factory-2017/preview)
+ [Des heures Utopiales pour comprendre le monde et l'avenir](https://framastory.org/story/xavcc/des-heures-utopiales-pour-comprendre-le-monde-et-l-avenir-oupas/preview) #OuPas
+ [It's Kind of a #Biocolab Story](https://framastory.org/story/xavcc/it-s-kind-of-a-biocolab-story/preview)
+ [Nomades et nomadismes, ]( http://www.multibao.org/#nomades)Le nomadisme est un mode de vie fondé sur le déplacement ; il est par conséquent un mode de peuplement matériel ou immatériel.
+ Lier le nomadisme postmoderne à une volonté de comprendre ce qui a fait de nous une Humanité il y a des milliers d’années ?

## Contributions

+ Sur le portail informationnel commun de tiers-lieux, [Movilab](http://movilab.org/index.php?title=Utilisateur:XavCC) :
   + Le processus de l atelier aux "Tiers Lieux"
   + IndieCamp
   + MoviLab Bretagne
   + HackerSpace
   + Nomades et laboratoires citoyens
   + Tiers-lieux et océans
   + Questions sur la gestion des riques dans les tiers lieux
   + Etapes vers une conception politique du tiers-lieu/En cours
   + Hotel pasteur
   + Tiers-lieux et monnaie libre
   + BILIOS: Biomimétisme Libre et Open Source. Camp 2017
   + Recette frugale d'hackathon citoyen open source: en 32 jours et sans budget
   + Portail:Dm1TL
   + OSCEDays 2017 à Rennes
   + Actions particulières
     + IndieCamp 2017 Névez
     + L'expérience Tiers Lieux, Fork The World ! - Biennale du Design 2017
     + MoviLab Bretagne
     + Nomades et laboratoires citoyens

+ [Livre numérique « Fork The world »](https://world-trust-foundation.gitbooks.io/fork-the-world/content/), des 'Tiers-Lieux' (espaces de coworking, ateliers de co-réparation, squats artistiques auto-gérés, jardins partagés, etc.). En bref, ce "Gitbook" est UNE mise en forme des textes rédigés par/pour l'Expérience Tiers-Lieux "Fork The World" de la Biennale de Design de Saint-Etienne (9 mars - 9 avril 2017) consacrée aux mutations du travail. Ils sont régis par les termes de la licence Creative Commons BY-SA 4.0 et ont été co-rédigés par un réseau d'acteurs des Tie

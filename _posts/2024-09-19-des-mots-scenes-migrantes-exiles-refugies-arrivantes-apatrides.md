---
title: "Des mots scènes : migrant⋅es, exilé⋅es, réfugié⋅es, arrivant⋅es, apatrides"
layout: post
date: 2024-09-19 21:30
image: /assets/images/the-migrant-mother.jpg
headerImage: true
tag:
- hsociety
- Notes
- Anthropologie
category: blog
author: XavierCoadic
description: l’écriture inclusive fait diminuer nos stéréotypes discriminatoires
---

<figcaption class="caption">Image by Dorothea Lange, 1936 − Migrant agricultural worker's family. Seven hungry children. Mother aged thirty-two. Father is a native Californian. Destitute in pea picker's camp, Nipomo, California, because of the failure of the early pea crop. These people had just sold their tent in order to buy food. Of the twenty-five hundred people in this camp most of them were destitute. https://lccn.loc.gov/2017762903</figcaption>
<br />

La langue un objet social éminemment politique, objet de rapports de force, de pouvoir, et donc d’émancipation (<cite>Jean-François Bert</cite>, *2024*). Ces luttes se déroulent dans le théâtre aux chapitres de nos quotidiens.
Mon quotidien est ensoleillé par un⋅e petite être humain⋅e[^note-inclusive] de presque 2 années chez qui le langage est une voie lactée traversée d’étoiles filantes et de comètes. J’en suis alors plus exposé et plus sensible aux questions de langage et des mots que je n’avais pu l’être auparavant.

[^note-inclusive]: “L’écriture inclusive fait diminuer nos stéréotypes discriminatoires”. « D’après de multiples études, l’usage du masculin neutre favorise les biais sexistes. Alors que les diverses formes d’écriture inclusive ont l’effet contraire, selon Pascal Gygax, chercheur en psycholinguistique. » <cite>Kyrill Nikitine</cite>, Télérama, 22 juin 2024.

Cette linguistique m’est à nouveau renvoyée en plein visage avec les questions d’humanité[^note-mauss]. Pour <cite>C. Lévi-Strauss.</cite>, dès le langage apparu, l’Univers tout entier est devenu signifiant. Nous aurions alors vécu une transition de « Rien n’avait de sens » à « […] tout possédait un sens ». « L’Univers entier est devenu “significatif”, il n’en a pas pour autant été tout mieux connu ».

> « L’ambiguïté n’est pas le plus grand défaut du langage, c’est sa principale qualité ! Sans cette plasticité, le langage ne parviendrait pas à assurer sa fonction principale. De la même façon que la mémoire ne représente pas le passé tel qu’il s’est produit mais tel que l’on a besoin de s’en souvenir, la langue modélise le réel de façon à nous permettre de s’en saisir, et non tel qu’il est réellement. »
>
> <cite>Altar Silas</cite>, *un jour dans Fediverse*

[^note-mauss]: c’est en substance l’affirmation que j’avais tenue en 2022 dans « [« Théorie générale de la Magie », Marcel Mauss, avec les pieds (partie 1)](theorie-generale-magie-mauss-a-pied-partie-1/) » après lecture de travaux de <cite>Alpheratz</cite>, 2018, *Grammaire du français inclusif*, Éd. Vent Solars Linguistique.

Les langages se transmette avec des évolutions, et la langue écrite et orale aussi. Cette transmission pour se faire au foyer entre parent⋅es et enfant⋅es, elle peut se faire aussi depuis une colonisation vers de colonisé⋅es, avec des dynamiques internes à des petits groupes et des dynamiques externes « du haut vers le bas », trajectoires individuelles et collectives. Et bien d’autres configurations qui s’articulent les unes avec les autres. Il peut en résulter une très forte dépendance à une unique langue écrite / parlée devenu une forme de standard obligatoire avec des conséquences qui entravent, par exemple, le travail des sciences cognitives tronqué par l’anglais (<cite>Damián E. Blasi</cite> & al. , *2022*).

> « […] “le vent souffle” et “la rivière coule”, le vent serait-il donc autre chose que l’action même de souffler, la rivière autre chose que l’eau qui s’écoule ? Y a-t-il un vent qui ne souffle pas, une rivière qui ne coule pas ? » <cite>Norbert Elias</cite>, *Du temps* (1999)

Par le langage nous traversons des biais et parfois *même* nous en rendons une réponse d’autant plus influencée…

 > `Personne 1 tape à la vitre d’une voiture :` « Comment faites-vous pour avoir une voiture si bien entretenue ? »
 >
 > `Personne 2 ouvre sa fenêtre de voiture :` « Polish »
 >
 > `Personne 2 :` « Ok Jak udaje Ci się utrzymać samochód w tak dobrym stanie? »
 >
 > Un mème circulant sur le web [*NDLR: quel(s) genre(s) votre imaginaire a attribué aux personnes parlantes dans cette courte histoire ?*]

*La langue anglaise oriente la conversation et les rapports entre ces deux personnes*.

Læ petite être humain⋅e qui illumine notre vie intègre actuellement des éléments de langue orale :<marginnote>En fait, c'est déjà bien plus riche et complexes maintenant alors que je n'ai toujours pas fini d'écrire ici</marginnote>
+ « Pipeuplait », pour obtenir satisfaction d’une demande
+ « Bonjour », pour entrer en première interaction avec une personne nouvelle d’une situation nouvelle.
+ « Au revoir », avec un signe de la main
+ « Câlin », qu’iel accompagne de celui-ci par le geste notamment envers les animaux, insectes compris, et ses parent⋅es (pour l’instant où j’écris)

Ces éléments sont dis de « politesse » dans le groupe socio-culturel auquel nous sommes intégré⋅es. C’est là un début d’intégration de règles politiques sociales avec une grammaire de classement par statuts alloués à des « objets ». Et cela se fait aussi dans l’intention d’instaurer des relations en commodités. Je reste volontairement simpliste ici.

**Le mot écrit ou dit enveloppe des articulations d’architectures qui sont subséquentew à des comportements humains. Après ce mot adviennent des conséquences dans les comportements humains.
Dans ce que nous fabriquons, ce que nous concevons, y compris les mots, nous insufflons un peu de nous et ce petit peu de nous est ensuite transmis, diffusé, médié, interprété par des tiers.**

Certaines personnes pourraient être tentées d’appeler, dans ce cas de communication et langages, une forme de code, et pire de programme qui utilise ce code. Laissons cela à l’ingénierie humaine se faisant sur des machines à automatisation ! Je rappelle au passage que l’ADN n’
est lui non plus un code, et qu’il n’y a pas de programme génétique (<cite>Jean-Jacques Kupiec</cite>, <cite>Pierre Sonigo</cite> *2000*). Ce biais pour nommer provient des inter-relations entre des éminences du mouvement cybernetic, avec la théorie de l’information signal, avec des éminences de la biologie en début de seconde partie du vingtième siècle. Ce travers perdure encore aujourd’hui, il sert aussi de socle d’appui à des eugénistes, transhumanistes et théories d’extrême droite. Rapports de forces et construction d’idéologie et de politique…

Læ petite être humain⋅e, qui brille toujours autant dans mes yeux, a été exposée à maintes reprises à des discussions sur l’altérité avec des questionnements sur des termes tels que migrant⋅es, exilé⋅es, réfugié⋅es, arrivant⋅es, étranger et étrangères. C’est un biais fait maison, je l’
avoue. Iel n’a pas, pour l’instant, toute possibilité d’un art de n’être pas tellement scripté⋅e (*The Art of Not Being Scripted So Much*, <cite>Jean Michaud</cite> *2020*). C’est une histoire foyer, maison, de transmission et nous sommes hérité⋅es autant que nous héritons. Pour notre petite être humain⋅e il n’y pas à ce jour de verbalisation des mots migrant⋅es, exilé⋅es, réfugié⋅es, arrivant⋅es, étranger et étrangères. Soit une personne est là, soit iel n’est pas là, et les raisons d’une venue ne sont pas intriquées dans des jugements incarcérant autrui.

Il y a quelques semaines j’avais essayé de me pencher sur une question « [Pourquoi certains médias ont-ils appelé les Syrien⋅nes arrivant en Europe "réfugié⋅es" alors que d’autres utilisaient le terme "migrant⋅es" ?](/refugies-21-eme-qu-est-ce-que-refugies-et-confusion/) ». Pour faire cela je prenais un angle avec pente de formalisme scolaire (en plus en surplomb) en composant avec :
+ Les problèmes du réductionnisme lors de catégorisation
+ L’origine du terme migrant avec son occurrence d’attribution dans l’Histoire : 
  + les protestant⋅es chassé⋅es du royaume de France à la fin du XVIie siècle
+ Des facteurs pour l’
attribution du statut relatif tels que :
  + L’interprétation des motifs de persécutions
  + L’interprétation du groupe social
  + L’interprétation de leurs opinions politiques
  + L’attribution de critères aux autaires de persécutions
  + L’interprétation d’une cause dite « naturelle » imputée à une catastrophe (*Aucune catastrophe n’est naturelle*)
+ L’obtention de droits en fonction du statuts attribués
+ L’étiquette médiatique et la médiation de l’information
+ Un cas de différence de traitement des individus à Bruxelles.

<marginnote>Il a relevé que Weird a été utilisée comme qualificatif par le camp de Kamala Harris pour se moquer des partisans de Trump lors de la campagne à la présidentielle aux USA dans l'été 2024. Et que ce « tour » de langage a performé médiatiquement et a ulcéré les républicains trumpistes</marginnote>
Une personne qui arrive dans à l’intérieur des frontières administrées d’un État dit démocratique et républicain est *reçue* avec un rapport de confrontation. Cette personne est obligée de faire preuve du *bien fondé* de sa présence dans cet espace, elle est aussi obligée de fournir des éléments constituants le *proto*-droit à prétendre à des droits par sa présence dans cet espace. Tout ceci se fait dans des règles pensées, éditées, appliquées par des WEIRD (<cite>Joseph Henrich</cite> *2024*) − Western, Educated, Industrial, Rich, Democratic − ce qui correspond à un modèle imposé à cette personne, modèle dit éducatif selon une temporalité et la discipline de travail dans le capitalisme industriel (<cite>E. P. Thompson</cite> (1967)). **Les statuts alloués sont transcripts en mots puis gravé dans des papiers administratifs qui sont autant de carte de rôles assignés**.

Les demandeureuses d’asiles sont le fruit et les victimes d’une industrie (<cite>Julia Caroline Morris</cite> *2023*) instituée par les mêmes personnes “WEIRD“. Pour imposer une forme *d'urgence*, qui sert de levier à la réduction de droits, il est très fréquent de déposséder les personnes et communautés de leur propre récit et documentation (i.e “undocumented people”). Les termes « refugié⋅es, exilé⋅es, apatrides, migrant⋅es » sont promis comme nécessaire à des catégorisations liées à des ouvertures de droits et cependant ils comportent dans leurs usages au quotidien des réductions de la condition matérielle humaine [^note-wittgenstein].

[^note-wittgenstein]: Remix d'un précipice de Wittgenstein automne 2023 <https://notecc.kaouenn-noz.fr/doku.php?id=pages:norae:biologicus:groupe_preparation_atelier_remix_wittgenstien>

Je  n’avais pas investi l’effort de travail sur les langages et les langues, délaissant dès lors un objet social éminemment politique, objet de rapports de force, de pouvoir, et donc d’émancipation. J’essaie ici et maintenant de partager quelques pistes préambulaires aux questions sur « migrant⋅es, refugié⋅es, exilé⋅es, arrivant⋅es, apatrides[^apatrides] ».

[^apatrides]: Voir [Le séjour en Belgique des apatrides – procédure d’admission au séjour pour cause d’apatridie](https://web.archive.org/web/20230320131537/https://www.cgra.be/fr/apatrides)

Un questionnement qui ne regarde pas sa propre catastrophe n'est pas un questionnement. Dans ce travail et mes notes partagées je n’ai pas frontalement à ce jour pris un oblique Postcolonial / décolonial (<cite>Alice Aterianus-Owanga</cite> et <cite>Emir Mahieddin</cite> *2023*) :

> « Des pans entiers d’humanité sont ainsi réifiés, réduits à l’état de marchandises par l’accumulation du capital (l’esclavage étant la forme la plus absolue), ou à l’état d’objets de connaissance par les sciences, comme autant d’énigmes à déchiffrer par le sujet du savoir, historiquement l’individu-sujet européen. Ce dernier devient une figure prométhéenne déifiée, occupant un point de vue surplombant, dominant la nature, niant le caractère intersubjectif de la production de connaissance »

Dans l’édito de ce numéro de revue sont ici en question « Les malaises de l’anthropologie universitaire » avec :

+ **A.** les problèmes méthodologiques et épistémologiques posés par des raisonnements parfois trop décrochés des observations empiriques, donc la légitimité de  la parole dans la création de connaissance et de savoir (voir aussi, entre autres exemples ’Le sucre’ chez Zist <https://www.zist.co/2020/01/28/le-sucre/>, "Being-in-the-Room Privilege: Elite Capture and Epistemic Deference" by <cite>Olúfémi O. Táíwò</cite> <https://www.thephilosopher1923.org/post/being-in-the-room-privilege-elite-capture-and-epistemic-deference>)

+ **B**. les postures et approches en surplomb  et/ ou décalage (voir entre autres "Le cas de Little Rock Hannah Arendt et Ralph Ellison sur la question noire" by <cite>Hourya Bentouhami</cite> et Why We Should Stop Saying “Underrepresented” by <cite>N. Chloé Nwangwu</cite> <https://hbr.org/2023/04/why-we-should-stop-saying-underrepresented> & ’Underrepresented Minority’ Considered Harmful, Racist Language By <cite>Tiffani L. Williams</cite> <https://cacm.acm.org/blogs/blog-cacm/245710-underrepresented-minority-considered-harmful-racist-language/fulltext>)

+ **C.** sur "la rationalité comme le capitalisme s’articule à l’idée maîtresse d’une relation d’extériorité entre un sujet et des objets", certain⋅e⋅s auteurices en sociologie / anthropologie prennent et assument un choix dans cet oblique (cf A et B).

Les études et questions des migrations n’échappent pas à ces malaises, et moi en tant qu’individu essayant d’y travailler j’en suis exposé et influencé. Voici un bout de lunette que je propose en conséquence.

**Lorsque l’on se penche sur l’après combat du parcours déshumanisant des personnes pour obtenir un statut reconnu par les administrations des États, nous pouvons constater que ces personnes, et leurs groupes d’appartenance sociales, restant confinées, voir exclues, à plusieurs égards. Bien qu’ iels est déjà fait *preuves* dans les conditions imposées, iels sont maintenant en dette dans cette société dite accueillante. Cette *liberté* acquise, avec droits afférents, est considérée comme payée par les infrastructures et deniers de société dite accueillante. Les personnes alors accueillies sont alors plongées dans un système de dette qu’elles doivent rembourser à cette société. C’est une dette de l’émancipation (<cite>Saidiya Hartman<cite> *1997*) comparable à celle des esclaves affranchi⋅es. L’esclavage ayant été aussi une industrie. Comme si les *péchés* des migrations devaient être remboursés par les labeurs de migrant⋅es.**

> « À cette liberté soi-disant conquise, répond immédiatement un ensemble de contraintes et de devoirs extraordinaires, qu’on pourrait résumer (pour dire vite) dans l’injonction à embrasser (avec zèle et joie) le mode de vie […] »
>
> <cite>Dana Hiliot</cite>, [Saidiya Hartman, Scenes of Subjection (extraits traduits)](https://outsiderland.com/danahilliot/saidiya-hartman-scenes-of-subjection-extraits-traduits/) *2024*

Je suis ne suis pas maintenant proche d’un travail de « Penser la différence culturelle du colonial au mondial. Une anthologie transculturelle » (<cite>Fernanda Azeredo de Morae</cite> *2023*). J’y mets du cœur à l’ouvrage ici dans ce billet et j’espère que cela me mènera plus loin encore.

> « Le problème n’a jamais été que les subalternes ne pouvaient pas parler : c’est plutôt que les dominants n’écoutaient pas. » (Young [1990] 2019, in PDC : 101)
>
> La phrase provocatrice de l’historien britannique Robert Young, en épigraphe de cet article, peut être lue comme une clé pour comprendre l’importance de la publication présente, ainsi que comme un commentaire sur les enjeux qui ont donné forme à cette anthologie. Le livre Penser la différence culturelle, du colonial au mondial. Une anthologie transculturelle, dirigé par Silvia Contarini, Claire Joubert et Jean-Marc Moura, accomplit la tâche importante de faire connaître au public francophone une partie des voix qui composent la multitude des courants de la pensée postcoloniale.
>
> <cite>Fernanda Azeredo de Morae</cite>

**La domination de la langue anglaise participe de l’imposition du terme "migrants" dans les textes réglementaires, dans la production d’
information et ses circulations. Aussi là se joue en rapport à l’autre et au Monde.**

De nombreuses langues expriment le statut relatif attribué aux parties évoquées à des degrés divers : formes humbles à la première personne, verbes, noms et pronoms indiquant un rang « bas / moyen/ haut/ très haut ». Il ne semble pas exister pas de formes égalitaires actuellement des langues. 

Comme le rappelait Will Tuladhar-Douglas, sur le réseau social Mastodon en discussion avec [Radical Anthropology](https://web.archive.org/web/20240211054516/http://radicalanthropologygroup.org/), :

> + 1) De nombreuses langues n’ont pas de genre mais seulement un mot (par exemple le finnois, le tibétain) ; de nombreuses langues signifient beaucoup _plus_ d’informations sur le genre (du locuteur, locutrice, locutaire, de l’auditeur, auditrice, auditaire, etc.) ; et de nombreuses langues ne confondent pas le « genre » _grammatical_ avec le sexe/genre (langues bantoues pour exemple).
>
> + 2) le bhasa népalais et d’autres langues tibéto-birmanes, nous disposons d’une série de caractéristiques obligatoires qui centrent la conversation sur l’interlocuteur, et non sur le locuteur - par exemple, trois pronoms déictiques, (mon) ceci, (ton) ceci, cela ; formes inclusives/exclusives de la première personne (répandues dans de nombreuses langues), la forme grammaticale de la question adressée à l’interlocuteur anticipe la grammaire de la réponse de l’interlocuteur. (Pour demander à quelqu’un s’il a mangé, on dit « J’ai mangé ? »).
>
> + 3) Pour offrir de la nourriture dans de nombreuses langues d’Asie du Sud et de l’Himalaya, vous *devez* indiquer si la personne est de rang inférieur ou supérieur au vôtre, et il existe même des formes spéciales pour les êtres divins. Le tibétain possède de nombreux noms honorifiques (il existe une forme de respect pour « nourriture »). D’un point de vue pragmatique, informer du rang de cette manière répond aux mêmes exigences sociales que le cadre « please / thank you / you’re welcome » en anglais.
>   + Cette dernière expression dérange vraiment les anglophones, car (pour des raisons religieuses / politiques) la personne a perdu tout indicateur de formalité (donc pas de tu/usted ou de du/sie) et apprendre qu’il existe des langues qui exigent un rang est moralement répréhensible. L’utilisation de termes de parenté et d’une grammaire de classement peut en effet être très politique, mais elle contribue également à créer des relations confortables.

Ce à quoi Radical Anthropology ajoute :

> Pour de nombreuses langues africaines, la question est celle de l’ancienneté plutôt que celle du rang ou du statut en soi.
>
> Mais les chassaires-cueillaires fondamentalement égalitaires[^note-venkataraman] ne veulent pas dire « s’il vous plaît » ou « merci » (que les gens partagent par défaut), ni s’expliquer ou s’excuser (cela porterait atteinte à l’autonomie).

[^note-venkataraman]: Voir par exemple : Vivek V Venkataraman (2024) "The Ju/’hoansi protocol. *Hunter-gatherer societies are highly expert in group deliberation and decision-making which respects both difference and unity*. Edited bySam Dresser <https://aeon.co/essays/what-the-ju-hoansi-can-tell-us-about-group-decision-making>

De langue exprimée en mots du quotidien sortent des mécanismes spécifiques, des principes et le pouvoir symbolique des règles, formelles et informelles, codifiées et non codifiées, explicites et implicites.

Lorsque nous disons « réfugié⋅es, exilé⋅es, apatrides, migrant⋅es » nous exprimons autant de notre puissance / nuisance par domination que nous affirmons un qualificatif imposé à autrui. Et nous y accolons une injonction aux personnes accueillies à embrasser cette configuration violente.

Nous avons le choix de dire et d'écrire avec plus d'inclusivités, de tolérances et d'hospitalités. Pour faire diminuer nos stéréotypes discriminatoires et désincarcérer.

## Remerciements

+ Dana Hilliot
+ Khrys
+ Radical Anthropology Group
+ Will Tuladhar-Douglas

## Bibliographie

+ Alpheratz, (2018). *Grammaire du français inclusif*, Éd. Vent Solars Linguistique.

+ Aterianus-Owanga A., Mahieddin E. (dir.) (2023). *Lectures anthropologiques n°10 – “Postcolonial / décolonial”* <https://www.lecturesanthropologiques.fr/1043>

+ Bert, J. F. (2024). *Démêler Marcel Cohen. À propos de : Josiane Boutet, Marcel Cohen, linguiste engagé dans son siècle (1884-1974), éditions Lambert-Lucas* <https://laviedesidees.fr/Demeler-Marcel-Cohen>

+ Blasi DE, Henrich J, Adamou E, Kemmerer D, Majid A. Over-reliance on English hinders cognitive science. Trends Cogn Sci. 2022 Dec;26(12):1153-1170. doi: 10.1016/j.tics.2022.09.015. Epub 2022 Oct 14. PMID: 36253221.

+ Elias N. (1997). *Du Temps*, Éd. Pocket

+ Hartman S. V. (1997). Scenes of Subjection: Terror, Slavery, and Self-Making in Nineteenth-Century America. Oxford University Press

+ Henrich, J. (2024). WEIRD. In M. C. Frank & A. Majid (Eds.), Open Encyclopedia of Cognitive Science. MIT Press. <https://doi.org/10.21428/e2759450.8e9a83b0>

+ Hubbard, K. E., Dunbar, S. D., Peasland, E. L., Poon, J., & Solly, J. E. (2022). *How do readers at different career stages approach reading a scientific research paper? A case study in the biological sciences*. International Journal of Science Education, Part B, 12(4), 328–344. <https://doi.org/10.1080/21548455.2022.2078010>

+ Kupiec J-J., Sonigo P. (2020). *Ni Dieu, ni gène. Pour une autre théorie de l’hérédité*, Éd. Points.

+ Lévi-Strauss C. (1950).  Préface, *Marcel Mauss. Sociologie et Anthropologie*, Éd. PUF « sociologie d’
aujourd’hui »

+ Michaud, J. (2020). "*The Art of Not Being Scripted So Much The Politics of Writing Hmong Language(s).*" Current Anthropology, 61(2)

+ Moraes F. A. de (2023). *Bâtir des ponts : dialogues manqués entre théories féministes et histoire des pensées postcoloniales*, Lectures anthropologiques <https://www.lecturesanthropologiques.fr/1067>

+ Morris J. C. (2023) Asylum and Extraction in the Republic of Nauru. Cornell University Press ISBN10: 1501765841

+ Thompson, E. P. “Time, Work-Discipline, and Industrial Capitalism.” *Past & Present*, no. 38 (1967): 56–97. <http://www.jstor.org/stable/649749>


## Notes



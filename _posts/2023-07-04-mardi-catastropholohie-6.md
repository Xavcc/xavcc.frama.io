---
title: "Mardi catastrophologie : votre fournée de ressources (n°6)"
layout: post
date: 2023-07-04 05:20
image: /assets/images/france-hausse-niveau-mers-hcc-2023.png
headerImage: true
tag:
- hsociety
- Catastrophologie
category: blog
author: XavierCoadic
description: Même la mort relève de la consommation
---

Les numéros précédents : [[1](/mardi-catastrophologie/)], [[2](/mardi-catastrophologie-2/)], [[3](/mardi-catastrophologie-3)], [[4](/mardi-catastrophologie-4/)], [[5](/mardi-catastrophologie-5)].

« […] L’ontologie ne peut résider qu’au niveau de l’action. L’être n’est pas *abstraitement*. […] L’*être* n’est pas l’*être* en tant qu’*être*, mais en tant que devenir », *thèses* 1.11 et 1.111, <cite>Kosmokritik</cite>, *[Praxis critique ontologique](https://abrupt.cc/kosmokritik/praxis-critique-ontologique/)*, Éditions Abrüpt, 2022, (CC BY-NC-SA 4.0).

C'est sur l'*être* et dans le devenir que la « catastrophe » inter-vient *perpétuellement* tout en provocant des ruptures.

> « Aucune créature humaine ne survit ni ne subsiste sans la dépendance d'un environnement qui lui assure une assistance, des formes sociales de relations, de formes économiques qui supposent et structurent l'interdépendance. Il est vrai que la dépendance implique la vulnérabilité et que cette dernière est parfois justement une vulnérabilité à ces formes de pouvoir qui menacent ou diminuent notre existence » <cite>Judith Butler</cite>, *p. 91*, *Ce qui fait une vie. Essai sur la violence, la guerre et le deuil*, trad. par <cite>Joëlle Marelli</cite>, Ed. Zones, 2010.

Lorsque des « crises » découlent de cela, celles-ci sont parfois prises en opportunités par des dominé⋅e⋅s et des dominant⋅e⋅ss pour investir des creux / des vides (*[Notes depuis le colloque : À l’épreuve des tempêtes. Institutions et crises : approches historiques](/notes-colloque-institutions-crises-recherche-histoire/#crises-et-institutions-urbaines-dans-les-petites-villes-du-cantal-xiiie-xvie-siècles)*, Rennes, 2023).

+ [Actu] **A dive into the deep past reveals Indigenous burning helped suppress bushfires 10,000 years ago** <https://theconversation.com/a-dive-into-the-deep-past-reveals-indigenous-burning-helped-suppress-bushfires-10-000-years-ago-203754>

> "Indigenous Australians have conducted cultural burning for at least ten millenia and the practice helped
reduce bushfire risk in the past, our new research shows. The study provides more evidence of the very
long history of cultural burning in southeast Australia. While the burning was probably not specifically used
to manage bushfires, our data suggest it nonetheless reduced fire extremes. Indigenous cultural burning
involves applying frequent, small and low-intensity or “cool” fires to clean out grasses and undergrowth.
But the scientific evidence for when in history Indigenous Australians used cultural burning, and what they
were seeking to achieve, is unclear. Our findings suggest Indigenous cultural burning in the past may have
helped reduce the intensity of bushfires. These findings are important because evidence suggests cultural
burning can assist modern land management as climate change worsens."

+ [Publi] **Exploration of the Burning Question: A Long History of Fire in Eastern Australia with and without People** <https://www.mdpi.com/2571-6255/6/4/152>

+ [Publi] **Concrete in the city** <cite>Kate Harriden</cite>; Australian Journal of Water Resources, 2022

> Abstract
> "Despite the hydrological imperative and engineering capacity for change, concrete storm water infrastructure remains obdurate in the urban waterscape. This obduracy manifests both as an unwillingness to remove existing infrastructure and the continuing construction of new infrastructure in locations previously free of these systems. This paper identifies four critical socio-political values underlying the obduracy of concrete storm water infrastructure and the resultant urban stream syndrome.
Following a brief critique of reactive storm water management frameworks to manage this syndrome, this paper articulates four common values of Indigenous science(s) that are well placed can contribute to improve storm water management. Supporting this argument is an example of Indigenous science(s) changing the form and function of a reach of an extant concrete storm water channel in Canberra, Australia. While these interventions will be assessed primarily from water quality perspectives, they
contribute to a greater range of environmental processes than purely hydrological." <https://doi.org/10.1080/13241583.2021.2002508>

+ [Ressource (thanks RadicalAnthro)] **The Indigenous Science Network** <https://research.acer.edu.au/isn/about.html>

+ [Actu] **In a 5-4 decision Supreme Court rules against Navajo Nation.** *599 U. S. ____ (2023)*

> "Justice Kavanaugh wrote the opinion concluding that the United States is not responsible in protecting Water Rights for Indian Country leaving the Navajo Nation still fighting for access to Water. He found that the 1868 Treaty establishing the Navajo Reservation did not require the United States to take affirmative steps to secure Water for the Tribe." <https://web.archive.org/web/20230626150327/https://mailchi.mp/waterprotectorlegal/breaking-scotus-rules-against-navajo-nation>

+ [Ressources] **Anthropocene & Molecular Colonialism**
  + <cite>Madriga Mendes</cite> "Molecular Colonialism", Consumption and Growth under Climate Austerity <https://www.anthropocene-curriculum.org/contribution/molecular-colonialism>
    + [PDF] <https://inhabitants-tv.org/oct2018_colonialismomolecular/MargaridaMendes_MatterFictions_EN_126-141.pdf>
  + [Podcast] The World in Which We Occur: Molecular Colonialism <https://archive.org/details/TWWWOsession2>
  + [Podcast] The World in Which We Occur: **Water Politics** <https://archive.org/details/WaterPolitics> 
  + Molecular Colonialism: A Geography of Agrochemicals in Brazil, <https://www.librarystack.org/molecular-colonialism-a-geography-of-agrochemicals-in-brazil/>
    + <cite>Larissa Mies Bombardi</cite>, "A Geography of Agrotoxins use in Brazil and its Relations to the European Union" <https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=a-geography-of-agrotoxins-use-in-brazil-and-its-to--annas-archive.pdf>

+ [Actu] **Meltwater is hydro-fracking Greenland’s ice sheet through millions of hairline cracks – destabilizing its internal structure** <https://theconversation.com/meltwater-is-hydro-fracking-greenlands-ice-sheet-through-millions-of-hairline-cracks-destabilizing-its-internal-structure-207468>

+ [Actu] **Incendies au Québec : Montréal suffoque, l’Europe menacée par un nuage de cendres** <https://www.liberation.fr/international/amerique/montreal-suffoque-sous-un-nuage-de-fumee-du-aux-incendies-qui-ravagent-le-quebec-20230626_T62FG5AXQ5GBTHKE7RTFRTDKNI/>
> « Le port du masque est recommandé dans la mégapole québécoise, qui subit de plein fouet les conséquences des feux qui ravagent le Canada depuis des semaines. La fumée est aussi en train de traverser l’Atlantique.
>
> 81 feux de forêt sont encore actifs au Québec, dont 27 considérés comme hors de contrôle. »

+ [Carto / Data viz] **Fire Information for Resource Management System US / Canada**

![](/assets/images/firms-us-canada.png)
![](/assets/images/global-firms.png)
<br />

+ [Actu] **Pollution : l’État jugé pour la première fois en partie responsable des maladies respiratoires de deux enfants**
> Le tribunal administratif a condamné vendredi l’État à indemniser des parents d’enfants qui ont souffert d’asthme et de bronchiolites à répétition à cause de la mauvaise qualité de l’air parisien. <https://www.liberation.fr/environnement/pollution/pollution-letat-juge-pour-la-premiere-fois-en-partie-responsable-des-maladies-respiratoires-de-deux-enfants-20230620_6MGCRPTC5BDKZHDQNXZCUFFPE4/>

+ [Actu Reminder] Cette inclinaison de justice n’est appliquée avec les mêmes modes opératoires en fonction de la place qui nous est assignée[^note-eaux] [^note-mines] [^note-biblio]. Toujours pour la France, cette fois ci loin de l’hexagone et du système centrée sur Paris (et du traitement médiatique induit), « Chlordécone aux Antilles : la justice prononce un non-lieu » (Le Monde)[^ref-lemonde], Après seize années de procédure, la justice a rendu une décision de non-lieu définitif, jeudi 5 janvier, dans le dossier du chlordécone. Utilisé durant depuis 1958 dans les Antilles comme intrant antiparasitaires à usage agricole et dont la toxicité (1963), écotoxicité, reprotoxicité (1965) et persistance sont connues, malgré son interdiction en France depuis 1990.

[^note-eaux]: Depuis 2019, Eau & Rivières de Bretagne alerte l’Agence régionale de santé (ARS). Par un tour de passe passe, elle écarte les mauvais résultats sur la qualité des eaux de baignade et déclasse ainsi d’importantes pollutions. Les conséquences ? Le public ne connaît pas précisément l’état des eaux et se baigne parfois, à ses dépens, dans une eau de mer polluée. Aujourd’hui, Juin 2023, Le rapporteur public du tribunal administratif vient de donner raison à Eau & Rivières de Bretagne : l’Agence régionale de santé pourrait avoir écarté les “mauvaises” analyses des eaux de baignade. Un scandale sanitaire enfin révélé au grand jour. <https://www.eau-et-rivieres.org/eaux-baignade-TA>

[^note-mines]: « Fermons les Mines », L’État français : « je peux pas traiter votre demande d’indemnisation pour exposition à l’amiante car vous n’avez pas été exposé » ; Court de cassation : 1. ces personnes ont été exposées 2. Elles ont aussi droit à une indemnité pour anxiété face aux risques en plus. État français : « Nous ne pouvons traiter votre demande car les documents nécessaires ont été contaminés à l’amiante » 3. Donc pas d’indemnités, ni de procédure administrative et judiciaire complète. Il va être fait une « zone re-naturée » sur une zone ultra-polluée (où seront expulsés les jardins ouvriers). <https://www.radiofrance.fr/franceculture/podcasts/lsd-la-serie-documentaire/on-ferme-les-mines-7968090>

[^note-biblio]: Liste de ressources sur une approche anthropologie des catastrophes <https://xavcc.frama.io/tags/#catastrophologie>

[^ref-lemonde]: *Chlordécone aux Antilles : la justice prononce un non-lieu*, Le Monde avec AFP, 1 mai 2023. « Après seize années de procédure, la justice a rendu une décision de non-lieu définitif, jeudi 5 janvier, dans le dossier du chlordécone. Utilisé durant des décennies »

+ [Citation] <cite>Paulo Freire</cite>

> « Comment transmettre une culture qui ne soit ni intellectuellement ni politiquement aliénante ? Face à la « culture du silence » intégrée par les opprimés, la réponse de Freire est centrée sur la notion de « dialogue », c'est à dire la parole en action, la parole authentique qui « transforme le monde » au moment où elle dit ce qui était tu. Loin du verbiage, parole sans action, à la fois aliénée et aliénante, le dialogue sincère et véritable est déjà une libération. C'est donc les conditions de ce dialogue qu'il faut rechercher et faire émerger. Car sortir du premier stade, celui du « mutisme » n'est pas suffisant, encore faut-il dépasser l'individualisation du problème, prendre conscience de sa capacité de changer le monde avec les autres. Si le problème de l'éducation bancaire est de faire accepter à l'élève le monde tel qu'il est, celui de l'éducation dialogique est de lui faire dire le monde tel qu'il devrait être. » *N'Autre école* n°12, 2006 - *La pédagogie des opprimés* <cite>Paulo Freire</cite> - La "méthode" d'éducation dialogique

+ [Actu] **South Korea shoppers buy up salt before Japan's Fukushima water dump** <https://www.reuters.com/world/asia-pacific/south-korea-shoppers-buy-up-salt-before-japans-fukushima-water-dump-2023-06-29/>

> "South Korean shoppers are snapping up sea salt and other items as worry grows about their safety with Japan due to dump more than 1 million metric tons of treated radioactive water from a wrecked nuclear power plant into the sea.
>
> The water was mainly used to cool damaged reactors at the Fukushima power plant north of Tokyo, after it was hit by an earthquake and tsunami in 2011."

+ [Report] **Water for life, not profit**, *co-publication by Transnational Institute (TNI), Platform for Public Community Partnerships of the Americas (PAPC), The Catalan Association of Engineering Without Borders (ESF), Blue Planet Project, and Corporación Ecológica y Cultural Penca de Sábila.* <https://www.tni.org/en/publication/rivers-of-resistance> (22 pages)

+ [Actu] **Chine : une forte chaleur écrase une partie du pays, Pékin en alerte rouge pour la première fois de son histoire** <https://www.rtbf.be/article/chine-une-forte-chaleur-ecrase-une-partie-du-pays-pekin-en-alerte-rouge-pour-la-premiere-fois-de-son-histoire-11217811>

+ [Data] **Extreme temperatures above 50°C next days in MiddleEast**. Forecast up to 18. July 2023 <https://www.wxcharts.com/?panel=default&model=gfs,gfs,gfs,gfs&region=mid_east&chart=2mtemp,850temp,wind10mkph,snowdepth&run=06&step=054&plottype=10&lat=51.500&lon=-0.250&skewtstep=0>
+[](/assets/images/wx-charts.png)
<br />

+ [Actu] **Disappeared in the desert: bodies lie in the sand in Niger while Europe pours millions into blocking migration route**, Death at 45 Degrees: The Situation of Migrants Crossing the Sahara. <https://www.theguardian.com/global-development/2023/jun/15/death-in-the-desert-bodies-lie-in-the-sand-in-niger-while-europe-pours-millions-into-blocking-migration-route>

+ [Actu] **Naufrage au large de la Grèce : “C’est une tragédie sans fin, il n’existe aucune solidarité européenne”** <https://www.liberation.fr/international/europe/naufrage-au-large-de-la-grece-cest-une-tragedie-sans-fin-il-nexiste-aucune-solidarite-europeenne-20230614_57636FKVCZGVJG3LGJ7JN7ERJM/>

> « C’est une nouvelle tragédie insupportable. Avec ce nouveau naufrage très meurtrier, nous allons dépasser les 27 000 morts en Méditerranée depuis 2014. Ce sont des hommes, des femmes et des enfants qui perdent la vie aux portes de l’Europe. »

+ [Carto / Data Viz (thanks Nicolas Lambert)] **A new map about Dead and Missing Migrants in the Mediterranean** <https://observablehq.com/@neocartocnrs/dead-and-missing-migrants>

![](/assets/images/map-dead-and-missing-2023.png)
<br />

> « Y-a-t'il des genres de vies qu'on considère déjà comme des non-vies, ou comme partiellement vie, ou comme déjà mortes et perdues d'avance, avant toute forme de destruction ou d'abandon ? » <cite>Judith Butler</cite>,*p. 63*, *Qu'est-e qu'une vie bonne ?*, traduction et préface par <cite>Martin Rueff</cite>, Ed. Payot & Rivages

+ [Publi] **WATER BODIES**, issue 03 <https://three.compost.digital/>

> "It’s boiled into steam, then turns turbines for the electricity that powers your device, the router it connects to, and every whirring, beating part of the network that brings you these words. This page is brought to you by a million liters of water, as droughts, contamination, and rising sea levels continue to creep us towards a boiling point of human and non-human suffering.
>
> For this third issue of COMPOST, we turn to our relationships with, and acknowledge ourselves as, Water Bodies."

+ [Publi] **Fictions climatiques** <https://journals.openedition.org/resf/>
  + Fiction climatiques. Introduction, <cite>Irène Langlet<cite> et <cite>Aurélie Huz</cite>
  + Cli-fi : une mosaïque globale, <cite>Carl Abbott, Lieven Ameel, Simon Bréan, Sébastien Fevry et Irène Langlet</cite>
  + Définir la fiction climatique, ou cli-fi, <cite>Andrew Milner</cite>
  + « Vivre avec le trouble » du changement climatique : écoféminismes posthumains dans *Le Roman de Jeanne de Lidia Yuknavitch (2017) et The Tiger Flu de Larissa Lai (2018)*, <cite>Lisa Haristoy</cite>
  + Une cli-fi composite : les artefacts science-fictionnels dans *Hors sol* de <cite>Pierre Alferi</cite>, <cite>Julia Ori</cite>
  + Désenchanter l’habitat spatial : environnements artificiels et mondes sans nature dans *Aurora* (K. S. Robinson), *Shangri-La* (M. Bablet) et *Nos Temps contraires* (G. Toriko), <cite>Gatien Gambin<cite>
  + Penser le Cthulhucène et son géotraumatisme : le cas *Cyclonopedia* de Reza Negarestani, <cite>Fabien Richert</cite>

> « Ce numéro aborde les fictions climatiques, genre plurimédiatique émergent. De simple décor, le climat devient un levier narratif, ou le thème principal de ces fictions. Ce dossier a pour ambition de mieux cerner la place de la « cli-fi » dans le champ littéraire, en lien avec la poétique de science-fiction mais aussi avec les discours sociaux, ainsi que dans l'histoire culturelle. »

+ [Enquête] **Une compagnie minière française menace la vie d’un peuple autochtone pour produire des batteries de voitures électriques** <https://lareleveetlapeste.fr/une-compagnie-miniere-francaise-menace-la-vie-dun-peuple-autochtone-pour-produire-des-batteries-de-voitures-electriques/>

> « Je veux partager mes connaissances avec mes petits-enfants et ceux qui veulent apprendre à manger et à vivre dans la forêt » “explique un hongana manyawa”

+ [Tribune] **Dissolution des Soulèvements de la Terre : « Le gouvernement s’en prend à ceux qui refusent de demeurer passifs face à la catastrophe »** <https://www.nouvelobs.com/opinions/20230624.OBS74918/dissolution-des-soulevements-de-la-terre-le-gouvernement-s-en-prend-a-ceux-qui-refusent-de-demeurer-passifs-face-a-la-catastrophe.html>

+ [Déclaration] **<cite>António Guterres</cite>, Secretary-General of the UN**, IPCC Press Conference for CLIMATE CHANGE , Monday 4 April 2022: Mitigation of Climate Change

> "Climate activists are sometimes depicted as dangerous radicals, but the truly dangerous radicals are countries that are increasing production of fossil fuels."

+ [Publication] **le Haut Conseil pour le Climat (HCC) a publié son dernier rapport.**, rapport annuel 2023 "**Acter l’urgence, engager les moyens**" <https://www.hautconseilclimat.fr/wp-content/uploads/2023/06/HCC_RA_2023_.pdf> (200 pages)

+ [Enquête] **Le nombre de morts liées à une intervention policière a atteint un pic en 2021** <https://basta.media/le-nombre-de-morts-liees-a-une-intervention-policiere-a-atteint-un-pic-en-2021>
  + [Actu] **En Allemagne, il y a eu un tir mortel en dix ans pour refus d’obtempérer, contre 16 en France depuis un an et demi**. <https://www.letemps.ch/monde/le-probleme-des-tirs-mortels-lors-de-refus-d-obtemperer-est-systemique-en-france>
  + [Actu (merci Pr. Logos)] **France to deploy 40,000 officers on streets as suburbs boil over after police teen shooting** <https://www.telegraph.co.uk/world-news/2023/06/29/paris-shooting-riots-police-arrests/>
  + **Un mort dans la révolte du 30 Juin au premier Juillet au CRA** [Centre de Rétention Administrative] de **Marseille** <https://marseilleanticra.noblogs.org/post/2023/07/02/revolte-de-la-nuit-du-30-juin-au-premier-juillet-au-cra-de-marseille/>, Les violences de racisme systémique, les violences des administrations, les violences de la police, et les violences par le silence TUENT ! Et la France est en pôle position là aussi <https://xavcc.frama.io/notes-lecture-les-camps/>

+ [Déclaration] **L’ONU appelle la France à s’attaquer aux « profonds problèmes » de racisme au sein des forces de l’ordre** <https://news.un.org/fr/story/2023/06/1136572>

+ [Citation] **<cite>Günther Anders</cite>**
> Même la mort relève de la consommation
>
> « Vous admettrez que l'on ne fabrique pas plus des armes pour les laisser rouiller qu'on ne cuit des pains pour les laisser rassir. Elles sont destinées en fin de compte à ceux qui doivent les recevoir et les consommer : par conséquent aux victimes. […]  L'utilisation des produits est un travail que nous, employés nommés "clients ou "consommateurs", avons à fournir pour assurer la fabrication de nouveaux produits et garantir la poursuite de la production. Si nous sommes considérés comme de meilleurs travailleurs, notre con,sommation sera d'autant plus satisfaisante et nous pourrons accomplir plus rapidement le devoir qui nous incombe d'œuvrer comme liquidateurs […] C'est pourquoi les armes sont des produits idéaux. C'est pourquoi la consommation des armes, comme la mort des victimes de guerre, représente la consommation idéale »
>
> <cite>Günther Anders<cite>, lettre à Francis Gary Powers, dans Le rêve des machines, 2022, ed. Allia

+ [Reportage] **Un demi-siècle d’aberrations urbaines à Grigny** <https://www.monde-diplomatique.fr/2022/12/PUCHOT/65357>
> « […] emblématique des banlieues déshéritées, Grigny n’était en 1969 qu’un bourg paisible de trois mille âmes quand l’État décida, sans concertation avec les autorités locales, d’y implanter deux grands ensembles : Grigny 2, la plus importante copropriété privée de France ; et la Grande Borne, un immense quartier de logements sociaux. Un péché originel dont la ville ne s’est jamais remise. »

+ [Livre] **Dans la détresse. Une anthropologie de la vulnérabilité**, <cite>Michel Naepels</cite>, 2019, Ed. EHESS.

> « C'est l'état d'exception, et de terreur, comme d'habitude. Approcher la réalité par la violence, l'incertitude, et la précarité sociale conduit à penser, comme Benjamin, que la « tradition des opprimés nous enseigne que l“état d'exception” dans lequel nous vivons est la règle [*Sur le concept d'Histoire*, <cite>Walter Benjamin</cite> [1940], 2000, Ed. Gallimard] » <cite>Michel Naepels</cite>, *p. 82*.

![](/assets/images/france-hausse-niveau-mers-hcc-2023.png)
<figcaption class="caption">Enjeux d’adaptation en fonction de l’évolution de la température en France et de la hausse du niveau marin au XX et XXIe siècle, La hausse des températures futures dépendra des émissions futures. Illustration parle Huat Conseil pour le climat, Le rapport annuel 2023 "Acter l’urgence, engager les moyens", page 186 − sources : D’après GIEC (2023) et Ribes et al. (2022)</figcaption>
<br />

Moi, biohacktiviste[^note-pses],

[^note-pses]: voir conférence : *PSES 2023 : Biohacking, Bio Punk, DIY Biologie* <https://xavcc.frama.io/pses-2023-biohacking-biopunk-diybio/> ; & *Bioessais* <https://xavcc.frama.io/tags/#bioassay> ; & *Biennale Économique de Rennes 2016 : Où nous mènent les nouveaux modèles d'organisation* (Documentation collaborative ouverte de l’atelier “Modes d’organisations et inégalités” dans le cadre du forum biennal Changer l’économie à Rennes , 30/01/16). Forum changer l’économie Fractures numériques - Les Champs Libres <https://xavcc.frama.io/next-eco/>

Malgré les mesures, malgré les protocoles, malgré les calculs et vérifications finales, je ne fais qu'appliquer des recettes héritées, quand bien même j'ai *parfois* détourné quelques règles<marginnote>Dans mes microtubes, avec mes micropipettes, au travers de mon mini thermocycleur et technique PCR, lorsque je manipule des plasmides, les zones grises prennent puissance. Lorsque je traque l'hormone chorionique gonadotrope humaine rien ne m'est assuré. Je donne confiance et pouvoir à des forces externes, j'investis confiance et croyance.</marginnote>. Contingentes, mes mains effectuent des gestes qui comportent et symbolisent des agencements de pouvoir. Ces gestes sont aussi le repliement d'une incommensurable quantité de savoirs acquis dans des espaces-temps multi millénaires.

Des objets appareillés imposent des gestes, produisent des habitudes corporelles, convoquent des sensations. Tous cela est déployé dans des caves, des cuisines, des berges de rivières polluées, des champs sinistrés, des cafés marginaux. Cependant, toute condition sociale « anormale » ne prépare pas à l'exercice de cette « guidance » (<cite>Marcel Mauss</cite>).

Un geste, même s'il vous paraît anodin, est chargé de symbolique, d'histoire, d'héritages[^note-mains] (y compris d'évolutions et de biologiques, cf le pouce opposable de la main). Ce billet blog en est une illustration autre et pour autant comparable à celle de l'enquête environnementale dite de terrain.

[^note-mains]: **Bibliographie** :
     + *Théorie générale de la Magie* , Marcel Mauss, avec les pieds (partie 1) <https://xavcc.frama.io/theorie-generale-magie-mauss-a-pied-partie-1/>
     + *Magie et/comme hacking* <https://journaldumauss.net/spip.php?page=imprimer&id_article=1552>
     + *Les Mains Dans Le Temps : Introspection et suspension en DIYbio − Ep.1* <https://xavcc.frama.io/les-mains-dans-le-temps-introspection-suspensions-diybio/>
     + *Les techniques du corps de Marcel Mauss / Revue Le Diable probablement* <https://www.radiofrance.fr/franceculture/podcasts/l-essai-et-la-revue-du-jour-14-15/les-techniques-du-corps-de-marcel-mauss-revue-le-diable-probablement-8262050>
     + *Pédagogie des opprimés* <http://www.sanstransition.org/wp-content/uploads/pedagogie_des_opprimes.pdf>
     + *Les fondements utilitariste et antiutilitariste de la coopération en biologie*; <cite>Bruno Kestemont</cite> <https://www.journaldumauss.net/?Les-fondements-utilitariste-et>
     + *Qu'est-ce que l'humain ?*, <https://xavcc.frama.io/humain-4/>
     + *La geste technique » : parler objets… par les milieux*, Actualité de la recherche et de la muséologie <https://www.mucem.org/programme/la-geste-technique-parler-objets-par-les-milieux>
     + *Habiter le mouvement, aviver l’inerte. Une étude du geste, en mésologie* <https://mucemlab.hypotheses.org/385>
     + *Anthropologie du geste chez Jousse* <https://studylibfr.com/doc/2542772/l-anthropologie-du-geste>

Dans cette situation et ces conditions, « Comment transmettre une culture qui ne soit ni intellectuellement ni politiquement aliénante ? Face à la “culture du silence” intégrée par les opprimé⋅e⋅s », la réponse de Freire est centrée sur la notion de « dialogue », c'est à dire la parole en action, la parole authentique qui « transforme le mon **de » au moment où elle dit, cette parole, ce qui était, avec la position et situation matérielle, pragmatique, et sociale du "tu/je".

+ [Publi] **The weirdest people in the world?**, <cite>Ara Norenzayan</cite>, <cite>Steven J. Heine</cite>, <cite>oseph Henrich</cite>, <https://www2.psych.ubc.ca/~henrich/pdfs/WeirdPeople.pdf>

> “Behavioral scientists routinely publish broad claims about human psychology and behavior in the world’s top journals based
on samples drawn entirely from **Western, Educated, Industrialized, Rich, and Democratic (WEIRD)** societies. Researchers – often implicitly – assume that either there is little variation across human populations, or that these “standard subjects” are as representative of the species as any other population. Are these assumptions justified? Here, our review of the comparative database from across the behavioral sciences suggests both that there is substantial variability in experimental results across populations and that WEIRD subjects are particularly unusual compared with the rest of the species – frequent outliers.”

## Notes et références


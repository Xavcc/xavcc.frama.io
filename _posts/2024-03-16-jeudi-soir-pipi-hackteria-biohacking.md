---
title: "Ces Jeudis soirs où nous faisions pipi"
layout: post
date: 2024-03-16 20:40
image: /assets/images/new-seed.jpg
headerImage: true
tag:
- hsociety
- Biohacking
- bioassay
category: blog
author: XavierCoadic
description: PTsD -- Piss Thurday's Delight. Hackteria. Biohacking
---

Cette petite histoire[^ph-hg] débute en 2020 avec un petit groupe autonommée "Pussy Riot" (Mexique / Inde / France) lors du [BioE271: Frugal Science](https://web.archive.org/web/20230113132124/https://www.frugalscience.org/), programme initié par Manu Prakash du laboratoire éponyme de [bio-ingénierie de Standford](https://web.archive.org/web/20230307015253/http://prakashlab.stanford.edu/). Nous travaillions à l'élaboration d'un test de grossesses et des problématiques sociales, dont les violences et inégalités associées.

Là, nous apprenons qu'une forme de test de grossesse par l'urine existe avec usage de graines, et qu'il est documenté depuis plus de 3000 ans dans un papyrus d'Égypte, XIXe dynastie (-1296/-1186). Le « Papyrus de Berlin n°3038 ».

> “Barley [and] wheat, let the woman water [them] with her urine every day with dates [and] the sand, in two bags. If they [both] grow, she will bear. If the barley grows, it means a male child. If the wheat grows, it means a female child. If both do not grow, she will not bear at all” 
>
> « L'orge et le blé, la femme les arrose chaque jour avec son urine, avec des dattes et du sable, dans deux sacs. S'ils poussent tous les deux, elle enfantera. Si l'orge pousse, c'est un enfant mâle. Si le blé pousse, c'est qu'il s'agit d'un enfant de sexe féminin. Si les deux ne poussent pas, elle n'enfantera pas du tout. » <cite>Papyrus de Berlin n°3038</cite>[^bln]

Nous avions déjà plus de dix années derrière nous d'[Atelier Biopanique, Cuisine, et féminisme](https://wiki.kaouenn-noz.fr/ateliers:biopanique_cuisine_feminisme).

Après quelques aléas, c'est [Louzaouiñ Graines de luttes 2022](https://wiki.kaouenn-noz.fr/hors_les_murs:reprises_des_savoirs:louzaouin_graines_de_luttes%7C) et dans cette foulée une collaboration avec les copaines d'Hackteria sur [Seeds of Contend / Bioassay in your own dirt](https://www.hackteria.org/wiki/Seeds_of_Contend_/_Bioassay_in_your_own_dirt).

Alors un nouveau petit groupe de personnes à Rennes, France, se lance dans **PTsD − Piss Thurday's Delight** (*Févier 2023*). Les jeudis soirs à Rennes au Biohackerspace la Kaouenn Noz.

> Vous avez probablement déjà entendu parler du syndrome de stress post-traumatique (PTSD/ SSPT)…
>
> Imaginez que depuis des générations et des générations chez nos ancêtres jusqu'à nous aujourd'hui, on nous oblige à pisser dans une boîte avec [offuscation](https://fr.wikipedia.org/wiki/Offuscation) pour obtenir un (pseudo) diagnostic mêlant notre état de santé, notre état métabolique, notre avenir et surtout les coercitions potentielles que des individus/groupes pourront ensuite exercer sur nous.
>
> Non, n'imaginez pas ! Vous l'avez probablement vécu ou le vivrez un jour prochain.

L'intention première est de reproduire ce « protocole », avec nos urines, du Papyrus de Berlin n°3038 et de le vivre par nous même. Puis de faire de même avec d'autres « tests de grossesses » qui ont été élaboré et pratiqué à travers les âges et les groupes. Pour prendre en charge les reproductions sociales et questions de violences internes à notre groupe de travail, un cellule dédiée a été créée et elle porte le nom de « **Queer We Are** / BioPunk We Do / Hackalypse (is coming)! ». Cette cellule et le groupe général de travail doivent beaucoup à [Gynepunk](https://www.hackteria.org/wiki/GynePUNK).

![](/assets/images/400px-Queerweare.jpg "Un dessin au crayon noir et bleu sur page blanche. C'est une micropipette à l'horizontale un peu comme une fusée spatiale. Son slogan est 'Hackalypse' et 2 tags l'accompagnent 'Queer We Are' et 'BioPunk We Do'").

Les jeudis et d'autres jours en d'autres circonstances nous nous réunissons et nous travaillons ensemble. Vous pouvez nous rejoindre si cela vous tente ([infos](https://wiki.kaouenn-noz.fr/)).

Dans cette cuisine, au sens de cuisine par Saul Pandelakis[^pandelakis], je contribue à l'intention partagée et aux engagements en communs sur le démontage[^stasis] de choses.

Lors d'une séance récente les arguments du champ « transgénérationnel et psychogénéalogie » ont été portés aux discussions. Je n'y avait plus été confronté depuis 2006 (Cyrulnik[^cyrulnik]) 2004 (Anne Ancelin Schutzenberger[^schutzenberger]) , 2007 (Michel Serres[^serres]), 2000 (Todorov[^todorov]).

Afin de ne pas tomber dans la posture d'intellectuelles aliénées, nous avons opté pour agir ainsi :
- nous partager par écrit des références / biblio
- se mettre ensemble à faire du crochetage de cadenas / lockpicking. Donc s'occuper les mains pour dialoguer autrement.

Oui, c'est bien ça que nous faisons : s'outiller, s'entre aider et s’entraîner, comprendre de mécanismes, identifier de verrous, faire sauter des fermeture, démanteler et liquide des infrastructures.

Nous serons à Bruxelles en mai 2024 pour [Eukairos](https://thx.zoethical.org/t/eukairos-2024/626), les rencontres des pratiques militantes et collectives de fictions spéculatives.

Nos prochaines étapes de travail :
+ Finaliser le protocole Bln 2024
  + Tout décrire et documenter
+ Spectométrie d'urine avec matériel libre ([ici](https://gaudishop.ch/index.php/product/3d-printed-fiber-spectrometer-kit/))
+ [Chromatographie circulaire](https://www.hackteria.org/wiki/Seeds_of_Contend_/_Bioassay_in_your_own_dirt/PTsD#Re-inventing_the_wheel) de nos urines
+ [OHL SAUCE Centrifuge](https://wiki.kaouenn-noz.fr/hors_les_murs:lab0_bi0_p0p:la_sauce_bio:ohl_sauce_centrifuge?s[]=ohl&s[]=sauce&s[]=centrifuge) sur urine

Nous irons pisser sur les tombes sur un air de Boris Vian… C'est un probabilité émise par Vernon Scully.


## Notes et références

[^ph-hg]: About "Petites histoires, Grande Histoire. Nous travaillons avec cette méthode d'amination depuis 2015. Elle consiste en une auto-investigation, individuelle puis mis en partage collectif, d'un sujet donné. Ce qui permet à la fois la cohésion d'un groupe et la mise *sur la table* de vécu en considérant la subjectivité comme instrument politique / re-politisé de déploiement d'une trame de lecture / de travail du sujet investigué. <https://notecc.kaouenn-noz.fr/doku.php?id=atelier:note_retour_atelier_investigations_changements_climatiques_2023&s[]=petite&s[]=histoire#about_petites_histoires_grande_histoire>

[^bln]: Source Papyrus de Berlin n°3038, Bln 194 to Bln 199 in the transcript provided here: <https://sae.saw-leipzig.de/de/dokumente/papyrus-berlin-p-3038> <https://sae.saw-leipzig.de/en/documents/papyrus-berlin-p-3038> ; <https://web.archive.org/web/20140227053834/https://www.medizinische-papyri.de/PapyrusBerlin3038/assets/images/Foto.jpg> ; Der grosse medizinische Papyrus des Berliner Museums (Pap. Berl. 3038) : in Facsimile und Umschrift <https://archive.org/details/56620280R.nlm.nih.gov> ; 

[^pandelakis]: Arpentage : Prendre soin, prendre pouvoir, design en cuisine et empowerment (Saul Pandelakis) https://wiki.kaouenn-noz.fr/hors_les_murs:reprises_des_savoirs:louzaouin_graines_de_luttes:arpentage_tract_pandelakis

[^stasis]:  incluant les démons / deamon comme décrit dans Code, Figures - Olivier Lanctôt, in Soigner la technologie, Cahier d'enquêtes. Collectif Stasis, 2011.

[^cyrulnik]: *De chair et d'âme* Boris Cyrulnik

[^schutzenberger]: *Aïe, mes aïeux !* Anne Ancelin Schützenberger

[^serres]: *Rameaux* Michel Serres

[^todorov]: *Mémoire du mal, tentation du bien. Enquête sur le siècle*, Tzvetan Todorov
---
title: "La Fediverse, Nathalie Mühlstein Josso, la Wikipedia, XX / XXY de genres (sans Dieu ni gène) et 50 nuances de vies"
layout: post
date: 2023-09-07 05:20
image: /assets/images/screen-thgap-51.png
headerImage: true
tag:
- hsociety
- Biohacking
- Notes
category: blog
author: XavierCoadic
description: 
---

Nathalie Mühlstein Josso (1934-2022), la pédiatre endocrinologue française qui a isolé et nommé l'hormone anti-müllérienne (AMH), hormone protéique homodimérique d’origine gonadique apparentée à la superfamille du TGFβ, qui chez le fœtus joue un rôle dans le développement des organes génitaux. Josso a aussi étudié l'intersexuation et bien d'autres choses encore.

Un *toot* (ou pouet/pouêt en français) diffusé sur un réseau de réseaux sociaux fédérés[^par-toot] et je *(re)* prends connaissance de l'histoire et de la vie, des apports scientifiques de Nathalie Mühlstein Josso (1934-2022), par l'intermédiaire de [Hilda Bastian](https://orcid.org/0000-0001-8544-7386) via [Mastodon](https://fr.wikipedia.org/wiki/Mastodon_(r%C3%A9seau_social)). 

[^par-toot]: Archive disponible ici <https://web.archive.org/web/20230907193039/https://mastodon.online/@hildabast/111022453151447144>

C'est ici une forme de magie du territoire [^magie-tir-1]<sup>,</sup>[^Marcel-Mauss]<sup>,</sup>[^hacking-magie], du territoire *numérique* ?.

[^magie-tir-1]: Voir et écouter l'album de 2Bal 2Neg (notamment le titre « la magie du tiroir ») "*3× plus efficace*” (avec Sages Poètes & al.)

[^Marcel-Mauss]: voir aussi <https://xavcc.frama.io/theorie-generale-magie-mauss-a-pied-partie-1/> ; *PSES 2023 : Biohacking, Bio Punk, DIY Biologie* <https://xavcc.frama.io/pses-2023-biohacking-biopunk-diybio/>

[^hacking-magie]:  Complément du n°56 : Magie et/comme hacking / Texte publié le 27 novembre 2020 <https://journaldumauss.net/spip.php?page=imprimer&id_article=1552>

Nous avions déjà travaillé depuis longtemps[^ref-bmpm] sur ces points. Des ateliers [x nuances de XY XX XXY](https://wiki.kaouenn-noz.fr/ateliers:biofabbing:x_nuances_de_xy_xx_xxy) et aussi [BioPanique & Cuisine et Féminise](https://wiki.kaouenn-noz.fr/ateliers:biopanique_cuisine_feminisme), depuis plus de 10 années.

[^ref-bmpm]: <https://xavcc.frama.io/tiers-lieux-et-catastrophes/>

Ce fut donc un grand plaisir de lire la page de la Wikipedia anglophone de [Nathalie Mühlstein Josso](https://en.wikipedia.org/wiki/Nathalie_Josso) toute fraîchement publiée. Puis un tout aussi plaisant moment de démarrer la [page francophone](https://fr.wikipedia.org/wiki/Utilisateur:XavCCCzh/Nathalie_Josso) dans la Wikipedia en partant de la traduction de cette première. J'en ai ainsi, au préalable, apporté une modification à la page concernant l'[hormone de régression müllérienne (HRM)](https://fr.wikipedia.org/w/index.php?title=Hormone_de_r%C3%A9gression_m%C3%BCll%C3%A9rienne&oldid=207655522).

Puisqu'il est ici aussi question de Liberté, et des libertés biologiques, je tiens à rappeler que la génétique avait, dans son apparition et dans son développement, ré-introduit la dualité essence / existence. La notion de gène ayant été dès son commencement dérivée des expériences de George Mendel, avec une relation spécifique entre un caractère et un déterminant sous-jacent appelé gène. L'influence de l'école de pensée de la Cybernétique ayant ensuite largement influencé la biologie, et la génétique en particulier, avec son approche conceptuelle de la théorie du signal/information. En résulte une tenace et vivace propension, avec ses cohortes d'adeptes, la croyance en un « Dieu-programme » (<cite>Kupiec</cite>, <cite>Sonigo</cite> (2000)) pour ce qui concerne l'ADN (avec des contradictions et même des négation de l'évolution selon Darwin).

J'ai même eu le plaisir (encore) de manifester en hacktiviste face à des participant⋅e⋅s au Human Genome Editing Summit (2023, Francis Crick Institute, London) de la Royal Society. Les affiches à l'entrée du bâtiment titraient "Would you copy paste your DNA". Certains conférenciers invoquant la nécessité du "Editing a better human" avec comme garde fou le [Mana](https://fr.wikipedia.org/wiki/Mana_(spiritualit%C3%A9)), et d'autres « police de la nature » (<cite>Kupiec</cite>, <cite>Sonigo</cite> (*ibid*)) dans ce cas fortement exotisée.

La boucle…

Il me reste ici à vous inviter à lire « Ni Dieu, ni gène », de <cite>Jean-Jacques Kupiec</cite> et <cite>Pierre Sonigo</cite> ; « Procréation et imaginaires collectifs. Fictions, mythes et représentations de la PMA » de <cite>Doris Bonnet</cite>, <cite>Fabrice Cahen</cite> et <cite>Virginie Rozée</cite> (*dir.*) ; et le booklet / zine of [the Transgenic Human Genome Alternatives Project (thGAP)](booklet / zine of the Transgenic Human Genome Alternatives Project (thGAP)) (*auquel j'ai participé et contribué*).

Les luttes sont longues, la voie est libre…

---
## Notes et références
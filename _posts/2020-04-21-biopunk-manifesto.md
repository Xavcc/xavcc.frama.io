---
title: "Un Manifeste Biopunk"
layout: post
date: 2020-04-21 13:48
image: /assets/images/kerbors_moon-2017.jpg
headerImage: false
tag:
- hsociety
- Sciences
- Biohacking
category: blog
author: XavierCoadic
description: Traduction française 10 ans plus tard…
---

<figcaption class="caption">La déclaration suivante a été faite le 30 janvier 2010 au symposium du *UCLA Center for Society and Genetics*, "<a href="http://outlawbiology.net/">Outlaw Biology ? Public Participation in the Age of Big Bio</a>".Par Meredith L. Patterson et <a href="https://maradydd.livejournal.com/496085.html">originalement publiée</a> par elle même le 30 janvier 2010 sous licence libre <a href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0</a></figcaption>
<br>

<figcaption class="caption">Il s'inspire et suit délibérément la forme de "<a href="http://www.activism.net/cypherpunk/manifesto.html">A Cypherpunk Manifesto</a>" d'Eric Hughes.</figcaption>
<br>

La culture scientifique est nécessaire au bon fonctionnement de la société moderne. La culture scientifique n'est pas un enseignement scientifique. Une personne formée aux sciences peut comprendre la science ; une personne ayant des connaissances scientifiques peut *faire* de la science. La culture scientifique permet à toutes celles et tous ceux qui la possèdent de contribuer activement à leurs propres soins en santé, à la qualité de leur nourriture, de leur eau et de leur air, à leurs interactions avec leur propre corps et au monde complexe qui les entoure.

La société a fait des progrès spectaculaires au cours des cent dernières années en matière de promotion de l'éducation, mais dans le même temps, la prévalence de la science citoyenne s'est amenusée. Qui sont les équivalents au XXe siècle de Benjamin Franklin, Edward Jenner, Marie Curie ou Thomas Edison ? Peut-être Steve Wozniak, Bill Hewlett, Dave Packard ou Linus Torvalds - mais la portée de leur travail est bien plus limitée que celle des philosophes naturalistes qui les ont précédés. La science citoyenne a souffert d'un déclin inquiétant de la diversité, et c'est cette diversité que les biohackers, biohackeuses, cherchent à récupérer. Nous rejetons la perception généralement répandue selon laquelle la science ne se fait que dans des laboratoires d'universités, de gouvernements ou d'entreprises valant des millions de dollars ; nous affirmons que le droit à la liberté d'enquête, de faire de la recherche et de poursuivre la compréhension sous sa propre direction, est un droit aussi fondamental que celui de la liberté d'expression ou de la liberté de religion. Nous n'avons aucun conflit avec la grande science ; nous rappelons simplement que la petite science a toujours été tout aussi essentielle au développement de l'ensemble des connaissances humaines, et nous refusons de la voir s'éteindre.

La recherche nécessite des outils, et la liberté d'interrogation exige que l'accès aux outils soit sans entrave. En tant qu'ingénieur⋅e⋅s, nous développons des équipements de laboratoire peu coûteux et des protocoles standard accessibles au citoyen et citoyenne moyen. En tant qu'acteurs, actrices, politiques, nous soutenons les revues ouvertes, la collaboration ouverte et le libre accès à la recherche financée par des fonds publics, et nous nous opposons aux lois qui criminaliseraient la possession d'équipements de recherche ou la poursuite personnel d'enquêtes.

Il peut sembler étrange que des scientifiques et des ingénieur⋅e⋅s cherchent à s'impliquer dans le monde politique - mais les biohackers se sont, par nécessité, engagé⋅e⋅s à le faire. Les législateurs qui souhaitent restreindre la liberté individuelle de recherche le font par ignorance et par peur − son jumeau maléfique − respectivement, la proie naturelle et le prédateur naturel de la recherche scientifique. Si nous pouvons l'emporter sur la première, nous dissiperons la seconde. En tant que biohackers, il est de notre responsabilité d'agir comme des émissaires de la science, en suscitant de nouvelles et nouveaux scientifiques à partir de chaque personne que nous rencontrons. Nous devons communiquer non seulement la valeur de nos recherches, mais aussi la valeur de notre méthodologie et de notre motivation, si nous voulons repousser l'ignorance et la peur dans les ténèbres une fois pour toutes.

Nous les biopunks, nous nous efforçons de mettre les outils de la recherche scientifique entre les mains de quiconque le souhaite. Nous construisons une infrastructure de méthodologie, de communication, d'automatisation et de connaissances accessibles au public.

Les Biopunks expérimentent. Nous avons des questions, et nous ne voyons pas l'intérêt d'attendre que quelqu'un d'autre y réponde. Armé⋅e⋅s de la curiosité et de la méthode scientifique, nous formulons et testons des hypothèses afin de trouver des réponses aux questions qui nous tiennent éveillé⋅e⋅s la nuit. Nous publions nos protocoles et la conception de nos équipements, et nous partageons notre expérience de paillasse, afin que nos collègues biopunks puissent apprendre et développer nos méthodes, tout en reproduisant les expériences des autres pour en confirmer la validité. Pour paraphraser Eric Hughes, « Notre travail est libre d'utilisation pour toutes et tous, dans le monde entier. Il nous importe peu que vous n'approuviez pas nos sujets de recherche ». Nous nous appuyons sur les travaux des Cypherpunks qui nous ont précédé⋅e⋅s pour faire en sorte qu'une communauté de recherche profondemment disséminée ne puisse pas être empêchée.

Les biopunks déplorent les restrictions imposées à la recherche libre, car le droit de parvenir de manière indépendante à une compréhension du monde qui nous entoure est un droit humain fondamental. La curiosité ne connaît pas de frontières ethniques, de sexe, d'âge ou socio-économiques, mais la possibilité de satisfaire cette curiosité se tourne trop souvent vers les opportunités économiques, et nous voulons briser cette barrière. Un enfant de treize ans du centre-sud de Los Angeles a autant le droit d'enquêter sur le monde qu'un professeur d'université. Si les thermocycleurs sont trop chers pour pouvoir en donner à chaque personne intéressée, alors nous en concevrons des moins chers et nous enseignerons aux personnes comment les construire.

Les biopunks assument la responsabilité de leurs recherches. Nous gardons à l'esprit que nos sujets d'intérêt sont des organismes vivants dignes de respect et de bon traitement, et nous sommes parfaitement conscients que nos recherches peuvent avoir des répercussions sur notre entourage. Cependant nous rejetons catégoriquement les remontrances faites sur principe de précaution, qui ne sont rien d'autre qu'une tentative paternaliste de réduire les chercheuses et chercheurs au silence en instiguant la peur de l'inconnu. Lorsque nous travaillons, c'est en ayant à l'esprit le bien-être de la communauté − et cela inclut notre communauté, votre communauté et les communautés de personnes que nous ne rencontrerons peut-être jamais. Vos questions sont les bienvenues, et nous souhaitons simplement vous donner les moyens d'y répondre vous-mêmes.

Les biopunks sont activement engagé⋅e⋅s à faire du monde un endroit que toute personne peut comprendre. Venez, faisons de la recherche ensemble. 

<p align="right"><U>Meredith L. Patterson</U> − A Biopunk Manifesto. CC By 3.0 Lisence</p>

**Note Personnelle**

> Je l'avais déjà traduit plusieurs fois sur papier depuis 2012 pour des ateliers spécifiques. Il existe aussi peut-être des versions françaises au format numérique, je ne les ai pas trouvées. Hier, je me suis senti complice d'une faute, celle de ne pas fournir cette ressource en langue compréhensible par un public francophone plus large que ne le permet la version anglaise. Une chose du monde qui n'existe pas dans un langage donné est au mieux quasi invisible dans la communauté de culture qui pratique ce langage, plus souvent inexistante. J'espère avoir comblé avec retard cette faute.

### Remerciements

+ Mily1000V
+ Jonathan Keller
+ Marc Fournier



---
title: "Reverse? (V0.3 english version)"
layout: post
date: 2020-10-26 17:40
image: /assets/images/biopanique_pasteur-2017.jpg
headerImage: true
tag:
- hsociety
- Reverse
- Biohacking
category: blog
author: XavierCoadic
description: We know how to do things but we don't know what we do
---

```
v0.3-en (2020/10/31)

v0.2.1-en (2020/10/27)

V0.2-en (2020/10/26)

V0.1-en (2020/10/06)

V0.0.1-en (2020/09/10)
````

*If you are here, please feel free to add your notes, ideas, sentences, questions, resources…*

Following the exchanges and discussion [here](https://framagit.org/Xavcc/xavcc.frama.io/-/issues)

### Context

I have been teaching biomimicry and biohacking in French engineering school for 5 years, I have been collaborating for more than a year with the NGO Tactical Tech on "civic investigation" (and techniques, tools, methods and mindset).

> *"We want Exposing the Invisible to inspire a new generation of people committed to transparency and accountability.*" Tactical Tech

I merged an open biomimicry laboratory and biohackerspace in 2014, and I am currently part of a team working on Rapid Diganostic Tests (RDT) in Health and Environment in the Prakash Lab's Frugal Science program (Curiosity-Driven Science).

The present text follows a sum of questions that were initially multiple, because they spring from several communities and configurations, and yet they seemed concurrent to me:

* Exposing the Invisible: how to conceive other means of appropriating the stakes, methods, state of mind, techniques (or transforming the existing material for that purpose) in order to facilitate their apprehension?


* PrakashLab: what posture (and then configuration) should we choose to anchor ourselves and move towards "frugal" test designs that would be "accessible" to a very large number of people?


* Engineering school: under what conditions do we produce and transmit assets and knowledge on the verge of upheavals (in science, climate, politics, etc.)?


* I could also add what [Movilab.org](https://movilab.org/wiki/Accueil), a wiki that serves as documentation for a GNU/Linux Third Places, can help to produce or reproduce.

I would have gladly answered: "let's make a campfire and see what happens around it". Unfortunately, I left a more misty and wordy text. 

As I am committed  to this matter, I have seen the deep questioning of what we do and the tension that can arise between this questioning and the current articulation of the technique with science and knowledge, surrounded by technology. 

The transmission of knowledge, especially the ways of accessing it, as well as our relationship to technique, technology and science, leads me to question what we do, why and how we do it. A person who is skillful and intelligent in the conduct of her or his existence is not necessarily a person who is lucid and enlightened about what she or he produces, about why she or he acts the way she/he does… What matters is how we choose our way of being in the world and what it does to others, what it produces in our relationship to the world.

* *Flemish-speaking Belgian friends say in French "être au monde" (why I am in the world) when French-speaking in France say "venir au monde" (why I was born, why I came into the world),*
* *The way we see our articulation with the world expresses itself differently. When technique guides our relationship to the world, bigger differences in relationships can appear.*

So, are we well aware of how demanding this knowledge is? Although we know a few things, we sometimes ignore the essential. We simply ignore the object of our knowledge: "***We know how to do things but we don't know what we do***" (Socrates)

> "*The written text suffers from 3 congenital evils: first, it is frozen and "to whom the word is addressed, it is a unique thing that it merely signifies, the same once and for all"; second, once published, the text leads its own existence and addresses everyone in the same way, whatever their competence; finally, left to itself, it is unable, in the absence of its author, to defend itself and respond to its opponents*" -- Socrates quoted by Plato (Phaedra), 370 B.C.


This text could have been used as a support for a finer and more detailed descriptive exercise, with reports of internal narratives of the situations experienced and observed. It seemed to me that a lot of vagueness was above all to be explored, perhaps to be clarified, before trying to practice the fine and sharp descriptive exercise on what is covered. Here I open a dialog essay. It could, and I hope, continue in other, more precise, descriptive, analytical and even operational writings.


## Trying to be clearer day by day

When we share, give or receive information, during investigation or knowledge sharing session, we have the responsibility of a complex architectural object. (We : ETI contributors and al. <https://exposingtheinvisible.org/> and <https://kit.exposingtheinvisible.org/en/>)

Information is built with data, containing bits that are packaged and structured in order to be shared as a message. An investigator following his or her motivation/idea tries to disassemble information to find the finest info inside, to find evidence within its own primary idea, and when an investigator wants to verify an evidence she or he confront bits from a data package with a fact or reality's law. There is a similar *way of doing* in medical investigation, in science, in biohacking, in water testing, in reverse engineering, etc. , of course with some others steps behind and before.


The point here is: **I see 2 common *anchors***

* We try to deal with a complex architectural object
* We often try to disassemble objects, even if we don't really figure it

![](/assets/images/radare2.png)
<figcaption class="caption">Trying to config my radare2 work environment in <code>~/.config/radare2/radare2rc</code></figcaption>
<br />

## From fog to sunray

At the beginning, there was a feeling… some thoughts on the start of a new season − fall.

I've asked friends to jostle my mind with their sights

I'm starting to work on this (without really knowing where it is going to lead me/us) and I'd like to share with you a synthetic formulation and get your questions, feedback, criticisms, and maybe desire, on it. (short, chaotic and dirty)

<figcaption class="caption">From reverse engineering to open wetware in citizen hacking / investigation In a context of tension on environmental issues, as well as risks on biological data, and still of open and participatory science, We will demonstrate, through feedback, techniques used by small groups of people how to regain control over these issues. Hardware and software can be very expensive and also raise questions of reliability.
   <ul>Non-free bio-chemical materials have recently shown their limitations in a pandemic situation (PCR probes and markers). Reverse engineering practices allow us to consider these paradigms from different angles, then open windows, similar to those of open hardware and free open source software, in the fields of environment, biology, health. It is also a fertile ground for questionning and collective emancipation in the face of issues of privacy, safety and normativity.</ul>
   <ul>Then we try to prototype our own materials, including wetware. Between collaborations with local investigation group, the use of pftools (Linux tools to build protein and DNA generalized profiles and use them to scan and align sequences, and search databases.) in genome investigation (including human), the design of kit for GMO detection in food, a demonstration electroporation and genome transfection (DIY kit) on yeast to obtain Open Insulin (large POC for an example), and finally wetware material for water sampling and monitoring and analysis in the Breton river in Rennes (fr),</ul>
   <ul>In this effort, we propose to explain and demonstrate that these practices, which are still little (un)known and largely improvable, are at the heart of societal issues for which both the public and private sectors are responsible.</ul></figcaption>
<br /> 

"Language is an epistemology of potential domination" (Lena Dormeau[^1], Somewhere over the WolrdWideWeb, 2020). Let us be attentive and attentive to what could be of the "sharp, technical, jargonish" order. Else, we would be adding more complex architectural over complex architectural object, and so on. Sounds like an Ouroboros of the technique.

> "*What is the price to pay for the use of this language?*
> *In other words, does it or does it not allow us to memorize, process, criticize, share our collective experience?
This is the question of Walter Benjamin; Experience and Language*." Alain Giffard, 2018 − Digital Literacy, Technical Culture <https://archive.is/65pPQ>

Thanks to people and their shining point during yesterday discussion, today I'm trying to allay blur.

As One of us said we should try to demystify the technique and perhaps enable playgrounds (brave place in LGBTQI term, ex safe place) to be in conviviality and free and legit to disassemble what *weighs on us*

The gestures that make up the technique folds a phenomenal amount of time[^2] into the complex architectural objects we exchange.

We do not enjoy the same availability of time when we are racialized, oppressed, privileged. And the cost of that time, the access of that time is/could be pure violence.

My question here is : **How to find ways to empower people with time and knowledge acquirement?**

Two points here:

* **We try to do it with the Exposing The Invisible kit and workshops**
* **Time and knowledge/knowhow/know-how-to-be are central basis in investigation**

Objects are a specific way of transmitting essential statements about the actors' social lives by reminding that certain things, thoughts, hierarchies, stories, materials and gestures must be "thought out together"[^3].

Ordinary objects could able us[^4]:

* to do,
* to have done and,
* to make people think

So, how is a common world created through our objects?

By studying and describing our own objects (those of investigation and transmission of knowledge) through their materiality, how they work, in their physicality, how they are transmitted and reused, how we assemble them... "The technical object is subject to a genesis, but it is difficult to define the genesis of each technical object because the individuality of technical objects changes during genesis [...]." (Simondon, 1958, genèse de l'objet technique, le processus de concrétisation)

And let's remember that objects have a history, resulting from tensions, strategies, etc., and that they have a history of their own. And they condition our relationship with the invisible, and they say a lot about us in the face of this relationship. The says, and produce or reduce, our social inequalities architecture. Finally, they are a delegation of the social order − speed bumps (*extended policemen*) who force us to conform to a rule of social and political conduct (slowing down for security issues).

> "*1958. The first computers are here. For the philosopher Gilbert Simondon, ignorance of the machine is the major cause of the alienation of the contemporary world, men who know technical objects seek to impose themselves by conferring on them the status of sacred objects. Thus is born "an intemperate technicism that is nothing but an idolatry of the machine"* Gilbert Simondon, On the Mode of Existence of Technical Objects.

It's a long way to dissemble object built with and embedded within technique. We mostly share complex things or dark boxes while thinking that is transparent object and process. A worse thing would be to share pet themes (*tarte à la crème in french*) thinking that they are not dark boxes.

<figcaption class="caption">"Being lost is normal
<br />
feeling lost is the way"<br />
Manu Prakash, Frugal Science. 2020</figcaption>

## Walking towards sharing with care and faraway

But, and there's always a but, the kit is a complex architectural object, mindset (cornerstone of the kit) is a complex architectural object, etc. They are boxes with open source licenses on the content, but still boxes ; and opening, deconstructing a box, even a non patented box, is a big move.

During last winter, I *facilitated* a [4 day workshop](https://openfacto.fr/2020/03/11/atelier-exposing-the-invisible-exposer-linvisible-rennes-10-12-fevrier-2020/) in France, during the Festival des Libertés Numériques, focused on the Exposing The Invisible kit and shared knowledge, experiences, learning by doing on the fields.

Well, in short :

People do it for instance when:

* Copying & pasting things
* Mimicking & imitating methods and behaviors
* Doing it by themselves & adjusting to their needs
* Opening a window on a new technique without figuring it

7 months after, they say "I handle the technique and develop mindset, but […]", always a *but*, "How to be able (personal note: with freedom) to understand how we learn and how we produce knowledge (personal note: and free time to do it)?".

I swear that people who come without any tech/sci/engineering background in DIY biology café every week ask the same questions and face the same path with similar trouble.

Biohacktivists working on water issues confront patented darkboxes to analyze samples, like commercial, industrial, primer in PCR process. Reverse engineering doesn't really matter here, time matters. We need to learn and describe how to acquire **knowledge/knowhow/know-how-to-be** beyond technique.

Now, I'm a newbie, a newbie in course in Bio-engineering, and I am at the same asking point. (Always be a newbie)

![](/assets/images/e-coli-dna.jpg)
<figcaption class="caption">pftools ❤️ − build and search protein and DNA generalized profiles. Here in E. Coli</figcaption>

Here, a dark box can be seen as a device of which we know the existence, since we created it, and of which we cannot give a clear description and just an approximate functioning, or roughly (the ETI kit and the Mindset for example).

We can describe what goes into this dark box, maybe we should be more accurate and descriptive about these entries. We can also detail and explain what comes out of it. However, it is likely that we are not yet at the point where we can clearly and simply describe what happens in this box, how what goes in is transformed to form *what comes out* (Why, How, Where to investigate).

Well,

now, What can we do together to go further than the contents of the kit and its mindset? How can we offer and equip people with time and power?

My personal experience often made me think that disassembling little bits of stuff together allows to learn a lot, from each other and inside the initial stuff. Rebuilding a modified stuff, bits by bits, adapted to our needs is also a great learning time and often opens us a path to give something (**knowledge/knowhow/know-how-to-be**) to others.

Here I'm not only writing and thinking about reverse engineering, I'm on disassembling and **Disassembling** **is one of the main way to Reverse.**

I'd like to put in the hand of anyone who wants the idea, the pen to draw, the tools to work, the support to get help, the mindset to feel free and legit to do, the *how to…*, as a gift-giving to reverse and (re)build any ways.

For investigators, for citizens, for investigation…

**And when you reverse something, when you disassemble something, you expose its invisible sides and bits and mechanic.**

Yesterday, to name this configuration desire, we dropped fleeting thoughts during the online meeting like :

Investigation lab, physical workshops, boiler room… <whatever you imagine>

Framasoft says "The road is long, but the way is free"

(*I don't really know if my thoughts become something else step after step, day after day, discussion after discussion, a clearer thing, probably a proto complex architectural object in itself*)

* Moto, mojo, mood, of the days
   * Grow almost everything
   * Reverse almost everything
   * Be excellent with others
   * Take care
   * Read between your errors rows

![](/assets/images/disassemble-1.jpg)

## Where to start?

"Tech is stupid"[^5] and in fact we often pull "A question of "tech"" as a misleading[^6]. Technology is the study of tools and techniques, of how they work together. It means observations on the state of the art in the various historical periods, in terms of tools and know-how.⋅ Trying to reverse stupid things, a badly named one, could be a challenge for some, trying to reverse a misleading could be to. Is this really what we expect as an evolution? Is this really what we hope to do?

You can confuse a person with a kit, you can search for plankton from a long-haul plane or detect a virus on a piece of paper. The technique is not born ex nihilo, the technique is not neutral, it does not have in its nature a potential for intrinsic emancipation.
Perhaps the most honorable path does not lead to the conception of stupid and misleading things. What if we didn't start to build dark boxes in the fist place?

![](/assets/images/quad1.png)
<figcaption class="caption"> Quad by l3n "There are many ways of drawing a Sierpinski triangle, I'm using a technique I call "quadtree grammars", in C language <a href="https://git.sr.ht/~l3kn/quad">source code</a></figcaption>
<br />

Other people before us have asked themselves these questions, others have also tried to provide answers through practical experimentation.

### Do not reinvent the wheel or lead the wheel's reivention?

In a recent interview with Emmanuel S. Boss, from the MiscLab at the University of Main, as part of the PrakashLab's Frugal Science program, we discussed some approaches from the situation in science and engineering education.

The first insight is literacy matters[^7] and practices matters to[^8], through gesture and posture. The words, gestures, postures that come to manufacture this literacy can belong to cooperation and respectful collaboration, or belong to domination and imposition. It's about choosing the type of boxes that we want to establish together, to live in, and to do together.  Emmanuel Boss told me about "can do attitude" which would emerge at the same time as the ah-ah effect of the realization of the final project in the particular situation and configuration of these engineering courses − "Frontal “teaching” is limited to the first class meeting"[^9]. This leads to the appropriation of the property of curiosity as an ability/capacity (reverse in the sense of a more "traditionalist" education/teaching reversed), with an understanding of the need to develop the ability to improvise more than to repeat.

It reminds me of how I used to learn at school as a child.

A child would take an empty cup, intended for drinking water normally, turn it over to place the circular surface on a sheet of paper, grasp a felt pen. All this posture and gestures were allowed.

He drew a red circle with his marker with the cup as a guide.

There were several of us observing him. We had to copy his gestures, his posture/attitude *on freely taking and doing*, copying his method with markers of different colors, sometimes even drawing directly on the table, or drawing squares. In a way, this led to copy and paste with errors as a final result.

Apparently, this was considered as a configuration allowing learning situations.

Then, at the age of about 7 years old, all this became forbidden, because "it is the devil to copy-paste", and reprehensible.

> "*Does the punishment focus on the transmission of knowledge in our relationship to machines, these complex objects with their information architectures?*" A personal asking about an installation in which a robot executes a preventive punishment for its possible future disobedience, By Filipe Vilas-Boas <https://cargocollective.com/filipevilasboas/The-Punishment>, archive <https://archive.is/7Nnsl>

> "*At first I was terrified when the pen or the felt pen ran out of ink.*
> *And then quickly I loved to see the chance AND the limits of the system playing, making the few words of the sentence "i must not..." disappear or barely make visible. "* " Filipe Vilas-Boas

The way we teach and the way we learn are complex object with their architecture that could be dark boxes -or not. And those boxes are used as patterns to think and prototype then, even worst in competitive contexts and so on with with a foggy environment. Sometimes people are obsessed with their invention, the mission to invent and transmit haunts these people and at the they load inside their created dark boxes a *spark of being*, like Victor Frankenstein did (Mary Shelley, ***The Modern Prometheus***, 1818).

Reverse engineering consists in studying an object in order to determine its internal functioning. it is above all understanding. Understanding what the omnipresent objects are doing to regain control! Perhaps if we controled them, we would be able to refrain from building unneeded, unnecessary things, or stupid, misleading ones.

For all that plagiarism[^10] does not exactly means learning by reverse/hacking.

We are also in charge of demystifying a lot of things in gesture and technique. It's about Inhabitable Information

> "_To understand what it means to “inhabit” information, we need a language: the language of **information architecture**_"  **Louis-Olivier Brassard** <https://journal.loupbrun.ca/en/n/061/>

### Starting small and from your everyday life

"Enter in its environment to feel it" (Emmanuel S. Boss, during interview)[^11] could be a component of a method aiming at starting from a lived experience and sharing it. It also opens a window towards self-confidence, understanding and also the confrontation of our perception with other critical approaches - I can learn by doing wrong, I can try to demystify because I have self-confidence. Learning from one's mistakes is already solving a problem, often by going through the way of deconstructing the paths that lead us to error. It is the beginning of a thinking taken upstream on any investment that will lead to the manufacture of an object (conceptual or material).

For example, in the case of environmental monitoring, this approach consists in reversing our relationship to measurement - where, why, and how often to measure - and allows us to consider a reverse of a particular environmental measurement configuration in order to question what it says about our relationship to measurement.

   * What do you really learn and know from water?
   * What do monitoring devices and systems (and their information architecture) say about our current ignorance?
   * Where can I start a measurement/sample/analysis from my own "feelings" and ignorance?

Health diagnosis or investigation can also be considered from this starting point.

**Then, what's from everydayness?**

A pregnancy test… 

An simple device -in appearance- can actually hide a lot of things and a lot of complex objects with architectures.

![](/assets/images/pregnancy-urine-test.jpg)

Pregnancy tests are intended for women rapid diagnostic. The principles of accessible, affordable, usable tests, do not apply to all women in the same way within a given country or region, and there is an even greater gap between countries or continents. The price of a pregnancy test in México ranges between 3 and 10 dollars, which, on average, represents 80% of the minimum wage (~ 1 € to 4 € in France[^12] for a minimum wage 559,74 €). The socio-political relationship to the body, to motherhood, to intimacy, to freedom of choice, to abortion, are also stacked around this small object. And in the thin layers of this object hides an advanced engineering in bio-chemistry, or electronics components, when there are digital results.

Even though some of the best designed pregnancy tests could approach a wonderful utopia point of effectiveness[^13] it will not resolve all issues. And we can solve some non negotiable constraints to this complex problem[^14] :

   * Intimacy & privacy concerns of the test use and the result 
   * Low cost or free cost access 
   * Accessible, affordable, usable 
   * No domination provided to a person who has scientific or technical knowledge, understanding or skill 
   * Reliable and readable result

**What if we reverse an apparently simple urine pregnancy test? What happens if we do it in collaboration?**[^15]

> "*I think that what most prevents a lambda person from handling medical testing equipment is this sense of lack of technical knowledge*" Paola Arias Martinez, *Frugal Science*, 2020


I've worked for a while with a workshop called "Biopanic, Kitchen, and Feminism", food DNA extraction at home to demystify science and techniques and open doors to sociology-anthropoligical views on Women position in society and men contribution to this *asymmetry* due to gender[^16]. From gained experiences in this configuration and practices we can extract X points:

   *  Before any willingness or ambition to share, learn, reverse-engineering, etc.
       * Take care to create appropriate condition for self-confidence
       * Configuration for peer asking (I'm not here to teach, I'm here to ask) 
       * Clear rules of doing and non-agression place
   * Do not be jargonish as far as you can
   * Curiosity first
   * I can failed and feeling lost
   * Before playing the technique and science, explain social issues and narrative context

Starting to disassemble the demonstration by gesture may be more illustrative than giving a speech. Plus, you will start to share an experience in common.

In a urine pregnancy test, you can find some very thin layers with different functions and typesetting. Very often, it is an astonishing surprise for many people and a source of questioning: "how these paper shapes can reveal a pregnancy?"

With a person without any specific skills or knowledge in the topic, nor in science or engineering, some questions about the potential animal origins of the reagents or rare metals may probably invade the *brave* room. Take this opportunity to broaden the discussion from the outset with a non-technical-scientific centered approach. For example:

   *  The modern home pregnancy testing kit was based on the work of Judith Vaitukaitis, was born in Hartford, Connecticut, and Glenn Braunstein, who developed a sensitive hCG assay at the National Institutes of Health (USA). That test went onto the market in 1978[^17]. In France, the law on the right to voluntary interruption of pregnancy was passed in 1975, today in USA for 30 states abortion is still Illegal. 
   * And in history fact, pregnancy urine tests are in the women and society history since 1350 BC[^18]. But History of Women in Engineering and science is still difficult[^19].
   * Did you know Elizabeth Blackwell,  the first woman to receive a medical degree in the United States?


I did this kind of workshop experience during an evening workshop (2h30 long) with people from different sociological backgrounds, and most of the people present wondered why and how the past or present social configuration influences their curiosity and capacities on science and technology, on the freedom of access to knowledge. Some wanted to go further by trying to put together by themselves a Rapid Diagnostic Test derived from the urine pregnancy test, or to imagine how it would be possible to make a 100% home-made urine pregnancy test RDT.

By acting this way, by disassembling complex objects and their architectures, we could learn a lot and we could teach a lot from each others, to even set up the first conditions to avoid the dark boxes, or, at worst, be able to reverse them if they are encountered[^20].

The information we work with in our configurations is composed of bits and data that we compile to form and make objects (conceptual or material). This compilation gives us what we can call instruction text.

When we attempt to disassemble objects, we find, through collaborative practices, that :

   * A source code can be compiled in an infinite number of ways
   * Not all source codes can be compiled
   * An instruction text can be decompiled in an infinite number of ways
   * Not all instruction texts can be decompiled.
   * An instruction text can be disassembled in several ways
   * An object can be disassembled in several ways


By taking care to describe each step of this and to explain these articulations in our documentations for each object we work on, we open liberties towards Inhabitable Information.

> "*Expertise has many components and can be evaluated along many dimensions. It is thus not only about competences but also about social status. Having an English education, having a degree, and coming from a high caste and class make an Indian expert in terms of social status. Having inside expertise of a certain domain amounts to expertise in terms of competence. Such inside expertise can come in different forms. We distinguish two forms: (1) expertise to understand and follow discussions and (2) expertise to actively contribute to the further development of the inside knowledge or to the design of a particular technology. The first is easier to acquire than the second. The first kind of expertise is typically sufficient for interaction with scientists and engineers about policy choices or about balancing risks and benefits of a specific scientific or technical development. The second kind of expertise is needed to actively contribute to the making of scientific or technical knowledge. The mistaken opinion that citizens, users, patients, or stakeholders cannot be consulted on issues scientific and technological results from confusing these two forms of expertise. Since most of the time non-scientists indeed cannot contribute to substantive scientific work, it is erroneously assumed that neither can they interact on choices of priority, policy and ethics."* **Knowledge Swaraj : An Indian Manifesto on Science and Technology** "8. Interrogating Expertise", 2009, <https://scienceetbiencommun.pressbooks.pub/swaraj/chapter/interrogatin-g-expertise/>


### Thanks and acknowledgement:

Antoine Burret, Institut of service science, Centre Universitaire d'Informatique, Unige ; Tactical tech and Exposing The invisible cohort ; Manu Prakash, PrakshLab Standford, and Frugral Science cohort ; Emmanuel Boss, Maine In-situ Sound & Color Lab, School of Marine Sciences ; Julien Voisin « Introduction à la rétro-ingénierie » 2015, MiNet; Filipe Vilas-Boas, artist exploring our use of tech and its political, social, psy & environmental implications ; Mily1000V


## Notes & references

[^1]: Chercheuse indépendante en Philosophie, Enseignante/Membre associée Université Rennes 2

[^2]: « Qu'est-ce que l'humain », re-lecture on Michel Serres [https://xavcc.frama.io/humain-4/](https://xavcc.frama.io/humain-4/)

[^3]: Pierre Lemonnier, Mundane Objects. Materiality and Non-verbal Communication, Walnut Creek, Left Coast Press, 2012 (p. 138)

[^4]: Pierre Lemonier, "Anthropologie des objets ordinaires : faire, faire faire et faire penser, nouveaux regards sur les techniques". March 2015, conference Quai Branly [https://archive.is/AC6CR](https://archive.is/AC6CR)

[^5]: Marek Tuszynski, Featuring Tactical Tech, July 21, 2020 [https://exposingtheinvisible.org/en/articles/technology-is-stupid/](https://exposingtheinvisible.org/en/articles/technology-is-stupid/)

[^6]: Gauthier Roussilhe, A question of "tech"  30/03/2020 ,  [https://gauthierroussilhe.com/en/posts/une-erreur-de-tech](https://gauthierroussilhe.com/en/posts/une-erreur-de-tech)

[^7]: Engineering Literacy for Undergraduates in Marine Science - A Case for Hands On. 2012, Oceanography 25(2):219–221, [http://dx.doi.org/10.5670/oceanog.2012.61](http://dx.doi.org/10.5670/oceanog.2012.61).

[^8]: Teaching Physical Concepts in Oceanography: An Inquiry Based Approach. Sept 2009 [https://tos.org/hands-on/teaching\_phys.html](https://tos.org/hands-on/teaching\_phys.html). Karp-Boss, L., E. Boss, H. Weller, J. Loftin, and J. Albright. 2009. Teaching Physical Concepts in Oceanography: An Inquiry Based Approach. *Oceanography* 22(3), supplement, 48 pp, [http://dx.doi.org/10.5670/oceanog.2009.supplement.01](http://dx.doi.org/10.5670/oceanog.2009.supplement.01).

[^9]: Introduction in "Boss, E., and J. Loftin. 2012. Spotlight on education—Engineering literacy for undergraduates in marine science: A case for hands on. Oceanography 25(2):219–221, [http://dx.doi.org/10.5670/oceanog.2012.61](http://dx.doi.org/10.5670/oceanog.2012.61)."

[^10]: Plagiarism on Wikipedia [https://en.wikipedia.org/wiki/Plagiarism](https://en.wikipedia.org/wiki/Plagiarism)

[^11]: "Use of, Satisfaction with, and Requirements for In-Situ Turbidity Sensors", 2005 [https://drum.lib.umd.edu/bitstream/handle/1903/13738/act\_insitu\_turbidity\_sensor\_report.pdf?](https://drum.lib.umd.edu/bitstream/handle/1903/13738/act\_insitu\_turbidity\_sensor\_report.pdf?) \& "USE OF, SATISFACTION WITH, AND REQUIREMENTS FOR IN SITU HYDROCARBON SENSORS", 2011 [https://drum.lib.umd.edu/bitstream/handle/1903/13396/act\_cnua11-01\_hydrocarbon.pdf](https://drum.lib.umd.edu/bitstream/handle/1903/13396/act\_cnua11-01\_hydrocarbon.pdf)

[^12]: « Tests de grossesse: en pharmacie ou en grande surface, c'est le même prix »,  Armelle Bohineust, Le Figaro, 16 décmbre 2014 [https://www.lefigaro.fr/conso/2014/12/16/05007-20141216ARTFIG00309-tests-de-grossesse-en-pharmacie-ou-en-grande-surface-c-est-le-meme-prix.php](https://www.lefigaro.fr/conso/2014/12/16/05007-20141216ARTFIG00309-tests-de-grossesse-en-pharmacie-ou-en-grande-surface-c-est-le-meme-prix.php)

[^13]: We think here about a « R.E.A.S.S.U.R.ED point », "REASSURED diagnostics to inform disease control strategies, strengthen health systems and improve patient outcomes, December 2018, Nature Microbiology 4, Kevin John Land \& Al. DOI: 10.1038/s41564-018-0295-3

[^14]: We use this idea board from Frugal Science program [https://www.notion.so/Can-pregnancy-tests-be-100-homemade-89e5cd15dd0f49df895fbd52c45f95da](https://www.notion.so/Can-pregnancy-tests-be-100-homemade-89e5cd15dd0f49df895fbd52c45f95da) ; archive [https://archive.is/4XRsm](https://archive.is/4XRsm)

[^15]: Disassemble pregnancy hCG* urine test , Frugal Science Home work, [https://www.notion.so/Disassemble-pregnancy-hCG-urine-test-6eff42d0737749e3b7a345cc26175ff6](https://www.notion.so/Disassemble-pregnancy-hCG-urine-test-6eff42d0737749e3b7a345cc26175ff6)

[^16]: institution of education as asymmetry gender bias, « * Home ec students at Shimer College practice cooking on an electric stove, 1942 Wikipédia Family and consumer science * », Wikipedia [https://en.wikipedia.org/wiki/Home\_economics](https://en.wikipedia.org/wiki/Home\_economics)

[^17]: A Thin Blue Line: The History of the Pregnancy Test Kit. "A Timeline of Pregnancy Testing". National Institutes of Health. [https://history.nih.gov/display/history/Pregnancy+Test+-+A+Thin+Blue+Line+The+History+of+the+Pregnancy+Test](https://history.nih.gov/display/history/Pregnancy+Test+-+A+Thin+Blue+Line+The+History+of+the+Pregnancy+Test)

 [^18]: "Pee is for Pregnant: The history and science of urine-based pregnancy tests", by Kelsey Tyssowski [https://web.archive.org/web/20200526073609/http://sitn.hms.harvard.edu/flash/2018/pee-pregnant-history-science-urine-based-pregnancy-tests/](https://web.archive.org/web/20200526073609/http://sitn.hms.harvard.edu/flash/2018/pee-pregnant-history-science-urine-based-pregnancy-tests/)

 [^19]: "wikipedia: History of Women in Engineering": [https://en.wikipedia.org/wiki/History\_of\_women\_in\_engineering](https://en.wikipedia.org/wiki/History\_of\_women\_in\_engineering)

 [^20]: "Understanding biology by reverse engineering the control", Claire J. Tomlin and Jeffrey D. Axelrod. Published online 2005 Mar 14. doi: 10.1073/pnas.0500276102 − PMCID: PMC555517

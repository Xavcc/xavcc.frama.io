---
title: "Mardi catastrophologie : votre fournée de ressources (n°5)"
layout: post
date: 2023-06-13 05:20
image: /assets/images/collage-femme-ballon.jpg
headerImage: true
tag:
- hsociety
- Catastrophologie
category: blog
author: XavierCoadic
description: Et qu'il tombait encore de la neige en hiver…
---

Les numéros précédents : [[1](/mardi-catastrophologie/)], [[2](/mardi-catastrophologie-2/)], [[3](/mardi-catastrophologie-3)], [[4](/mardi-catastrophologie-4/)].

<figcaption class="caption">Image d'en-tête en illustration : Une petite fille au ballon, collage - #Paris #streetart, par Pylapp, 2 mai 2023, Licence CC BY SA. <a href="https://pixelfed.social/p/pylapp/558976236058527421">Source</a></figcaption>
<br />

Les « grandes catastrophes, avec leurs médiations et médiatisations, à résonances internationales dites naturelles » existent sous cette étiquette par le fait d'institutions qui produisent des normes permettant de le mesurer et de les ordonner, de les traiter. L'EM-DAT de Louvain la Neuve donne comme critères de cadrage : Au moins 10 mort⋅e⋅s, et, au moins 100 personnes affectées, et production d'une déclaration de catastrophe (naturelle ou autre) au niveau administratif, et une production de demande d'aide internationale. Nous avions abordé cet ensemble de situations possibles contraintes ou facilités dans un révérenciel donné, spécifique, et établi par un groupe en position de surplomb exogène[^note-1], avec des programmes, plans et actions conçus et déployés par des organes de coordination internationale, « entrepreneurs frontière » (<cite>S. Revet</cite>, 2018)[^note-2]. Voir également "Centre for Research on the Epidemiology of Disasters (CRED)" <https://www.cred.be/> & "United Nation Office for Disasters Risks Reduction" <https://www.undrr.org/>

[^note-1]: *Tiers-Lieux : Prise sur le quotidien d’un monde catastrophé*, 20/08/2022, <https://xavcc.frama.io/tiers-lieux-et-catastrophes/>

[^note-2]: Sandrine Revet, *Les Coulisses du monde des catastrophes « naturelles »*, Paris, Éd. de la Msh, 2018.

> « Or, depuis les travaux d’<cite>Anthony Oliver-Smith</cite> (1970), la catastrophe comme objet, puis de <cite>Kenneth Hewitt</cite> (1983), une autre voie est ouverte pour « une école latino-américaine » (<cite>Garcia Acosta</cite>, 1996 ; <cite>Lavell</cite> et <cite>Franco</cite>, 1996 ; <cite>Maskrey</cite>, 1993) qui a pour idée qu’il n’y a pas de catastrophes sans un contexte ou des facteurs socio-historico-politiques qui vont construire la vulnérabilité des groupes » (<cite>Sandrine Revet</cite>, La rupture de l’événement. Une anthropologie des catastrophes. Entretien avec <cite>Baptiste Moutand</cite> (2011)). Avec cette approche critique et radicale il n’y a pas que les “catastrophes” normées par une “grande ampleur”. Il y a tout autant de “petites catastrophes” » 
>
> *Tiers-Lieux : Prise sur le quotidien d’un monde catastrophé* <https://xavcc.frama.io/tiers-lieux-et-catastrophes/>

![](/assets/images/fc73261554dc9c61.jpg)
<figcaption class="caption">Global sea surface temperatures over all the oceans between 60⁰N and 60⁰S are in uncharted territory this year. Chart shows deviation from the 1982-2011 average for the date. same data visualized by Climate Reanalyzer / University of Maine https://climatereanalyzer.org/clim/sst_daily/</figcaption>
<br />

+ [Souvenir (merci Genma)] **Révélations de Snowden, ça fait 10 ans**. Dans la nuit du 5 au 6 juin 2013, le quotidien britannique The Guardian publie sur son site Internet un document secret inédit, le premier d’une invraisemblable archive soustraite à la National Security Agency (NSA)

+ [Podcast] **Ex0208 Révélations Snowden, 10 ans après, deuxième partie** <cite>CPU, Carré, Petit, Utile</cite>, (*Et l'“alégal” a-t-il bon dos ?*) <https://cpu.dascritch.net/post/2023/06/08/Ex0208-R%C3%A9v%C3%A9lations-Snowden,-10-ans-apr%C3%A8s,-deuxi%C3%A8me-partie>

+ [Publi] **L’anthropologie du « fait environnemental »  : retours réflexifs sur une spécialité en devenir**, <cite>Félicie Drouilleau</cite>, *p. 169-184*, doi.org/10.4000/sds.3858, <https://journals.openedition.org/sds/3858>

> « Cet article propose un rapprochement des travaux anthropologiques et ethnographiques sur l’écologie, l’environnement et l’énergie autour de la notion de «  fait environnemental  ». Les développements contemporains de l’anthropologie du «  fait environnemental  » posent en effet des questions méthodologiques complexes en termes de financement de la recherche, de travail en interdisciplinarité et de collaboration entre approches quantitatives et qualitatives des faits sociaux. À partir de la présentation synthétique de trois terrains menés en Colombie et France, l’auteur indique un certain nombre de fils réflexifs pour cette spécialité naissante. »

+ [Livre] **Mondes toxiques**, Revue Monde commun 2020/2 (N° 5), Presses Universitaires de France, <https://inventaire.io/items/66bf8257a43f8cf634ce0578c68b7b8c>

> « […] Elise Boutié revient en Californie après les incendies qui ont ravagé la superbe région de Paradise. Elle explore ce que veut dire vivre en territoire abîmé, comment accepter la toxicité, le déni et l’omission… » <cite>[Géographies en mouvement](https://geographiesenmouvement.com/2021/02/26/comment-nous-vivons-dans-des-mondes-toxiques/), 2021

+ [Livre (merci Mathieu Triclot)] **Quand la nature s’effondre**. Comprendre les transitions abruptes dans les écosystèmes
<cite>Alexandre Génin</cite>, *un ouvrage tiré d'une thèse sur les modèles mathématiques en écologie théorique et la prévision des transitions abruptes* <https://materiologiques.com/fr/modelisations-simulations-systemes-complexes-2425-5661/372-quand-la-nature-seffondre-comprendre-les-transitions-abruptes-dans-les-ecosystemes-9782373614008.html>

> « Bienvenue dans l’Anthropocène ! Nous nous apprêtons à entrer dans un monde dans lequel il fera plus chaud, souvent plus aride, et dans lequel la nature va profondément changer. Les écosystèmes vont se réorganiser, souvent se dégrader, parfois au cours de transitions abruptes que certains appellent « effondrements écologiques ». Quelle réalité se cache sous cette notion ? »

+ [Publi] **The Climate Crisis is a Digital Rights Crisis: Exploring the Civil-Society Framing of Two Intersecting Disasters**

> "We report and discuss our findings from exploring the intersection of the climate crisis and digital rights in an interactive workshop with civil society representatives and policy makers at an important digital-rights event in Brussels in the European Union."
>
> by <cite>Fieke Jansen, Merve Gülmez, Becky Kazansky, Narmine Abou Bakari, Claire Fernandez, Harriet Kingaby</cite>, and <cite>Jan Tobias Mühlberg</cite> <https://limits.pubpub.org/pub/8544yai8/release/1>, LIMITS ’23, June 14–15, 2023

+ [Discours] <cite>Nadine Moawad</cite> **APC Closing Ceremony in Internet Governance Forum 2015** <https://piped.hostux.net/watch?v=nzGDhclAJcg>

+ [Actu] **3 months on from the earthquakes, 5 reasons women and girls in Syria and Türkiye still need your support** <https://www.unfpa.org/news/3-months-earthquakes-5-reasons-women-and-girls-syria-and-turkiye-still-need-your-support>
  + Crises can mean life or death for pregnant women
  + Disrupted sexual and reproductive health access could trigger a secondary disaster
  + Gender-based violence protection needs soar in a crisis
  + Displacement takes a severe physical, mental and social toll
  + Solidarity and financial support must be sustained

> « […] le tremblement de terre de 1976 dans l'est de la Turquie, qui a détruit la plupart des habitats et autres abris de la région. Le tremblement de terre [Çaldiran–Muradiye](https://en.wikipedia.org/wiki/1976_%C3%87ald%C4%B1ran%E2%80%93Muradiye_earthquake), à la frontière de l'Iran et de la Turquie, à la suite duquel les responsables gouvernementaux turcs se sont efforcés de reloger les survivant⋅e⋅s des zones rurales vers les zones urbaines en prévision des conditions hivernales difficiles. C'est aussi en Turquie un moment de politique avec des choix effectifs liés à l’industrialisation qui a activé la croissance des besoins d'emplois non agricoles, donc d'une main d'œuvre disponible dans des zones conçues à cet effet. »
>
> *Tiers-Lieux : Prise sur le quotidien d’un monde catastrophé* <https://xavcc.frama.io/tiers-lieux-et-catastrophes/>

+ [Publi] **The 1966 Tashkent Earthquake and the People of Tashkent** (Soviet Union, Uzbek SSR)), <cite>Kulahmatovich, K. K.</cite>, & <cite>Bohodirovich, I. M</cite>. (2021) <https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=the_1966_tashkent_earthquake_and_the_people_of_tas.pdf>, and also Wikipedia <https://en.wikipedia.org/wiki/1966_Tashkent_earthquake>


+ [Réseaux] **Marš sa Drine** (Serbia) is a national and international network of activists, experts and local village farmers and landowners against the mining of lithium, borate and related minerals in Serbia <https://marssadrine.org/en/>

+ [Publi] **Mapping global inputs and impacts from of human sewage in coastal ecosystems** (*les eaux usées ajoutent chaque année environ 6,2 millions de tonnes d’azote aux eaux côtières autour du globe, soit l’équivalent de 40 % de ce qui est émis par le ruissellement agricole*) <https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0258898>

+ [Publi] **Farmland practices are driving bird population decline across Europe** <https://www.pnas.org/doi/10.1073/pnas.2216573120>

+ [Actu] **À Plouvorn, 80.000 cochons, de l’ammoniac et des nitrates**, par <cite>Kristen Falc'hon</cite>, <cite>Floriane Louison</cite> <https://splann.org/plouvorn-80000-cochons/>

> « Avec sa concentration record de méga-porcheries, la commune finistérienne de Plouvorn est un symbole de l’intensification de l’élevage et de ses conséquences. Enquête sur un coin de Bretagne où rien n’échappe aux cochons. Ni l’eau, ni l’air, ni la mairie. »

+ [Podcast (merci Julie Moynat)] **Journal breton - saison 1** <https://www.radiofrance.fr/franceculture/podcasts/serie-journal-breton-saison-1>

> « Installée pour quelques mois dans un hameau en Centre-Bretagne, Inès découvre d'histoires en histoires, une Bretagne bien énigmatique...
>
> Le journal Breton d'Inès Léraud est une série en deux saisons, un journal un peu intime, une enquête qui nous plonge dans l'univers de l'agroalimentaire breton, au milieu des algues vertes, des poulaillers et des abattoirs dans une région qui est la première région agroalimentaire de France. En Bretagne, 1 salarié sur 3 travaille dans l’agroalimentaire.
>
> Installée au cœur de la Bretagne pour plusieurs mois, Inès voit de ses propres yeux les effets sur l'écologie, les animaux et le vivant du système agroalimentaire Breton. »

+ [Actu] **Le rapporteur public du tribunal administratif vient de donner raison à Eau & Rivières de Bretagne : l'Agence régionale de santé pourrait avoir écarté les "mauvaises" analyses des eaux de baignade. Un scandale sanitaire enfin révélé au grand jour.** <https://www.eau-et-rivieres.org/eaux-baignade-TA>

+ [Podcast] **Des catastrophes pas naturelles**, 30 septembre 2022, Les pieds sur terre, France Culture <https://www.radiofrance.fr/franceculture/podcasts/les-pieds-sur-terre/des-catastrophes-pas-naturelles-2133795>

+ [Data / Monitoring] **EMSR668 (Emergency Management Service) Floods in Norway**, Snowmelt❄️ is causing several floods and high river discharges in the country (23/05/2023). Updates: <https://rapidmapping.emergency.copernicus.eu/EMSR668> (*Event time: 22/05/2023 evening ;	Activation time: 22/05/2023 afternoon*)<marginnote> Liste : EMSR668: Flood in Norway, 22/05/2023, 17:00 ; EMSR667: Wildfire in Caceres, Spain, 17/05/2023, 21:44 , EMSR666: Landslides in Slovenia, 17/05/2023, 00:00 ; EMSR665: Flood in Bosnia and Herzegovina, 17/05/2023, 11:00 ; EMSR664: Flood in Italy, 16/05/2023, 11:00 ; EMSR663: Flood in Mozambique, 12/03/2023, 00:00 ; EMSR662: Flood in Uganda 11/05/2023, 01:00 ; EMSR661: Storm in Myanmar, 14/05/2003, 06:00 ;
EMSR660: Forest fire in Ulricehamn region, Sweden, 10/05/2023, 16:30 ; EMSR659: Flood in Emilia-Romagna, Italy, 02/05/2023, 18:00</marginnote>

![](/assets/images/snowmelt-floods-normway-media_FwzC89EXsAE9i7S.jpg)
<br />

+ [Actu (thanks to RebelGeo)] **No wonder Alberta is on fire. We made this planet into a volcano**

> "We can’t call these supercharged seasonal infernos our ‘new normal.’ There’s nothing natural about how we changed the Earth’s climate. <https://www.theglobeandmail.com/opinion/article-no-wonder-alberta-is-on-fire-we-made-this-planet-into-a-volcano/>

> « 1824 : Découverte de l'effet de serre
>
> 1896 : Découverte du changement climatique
>
> 1957 : Augmentation du CO2 prouvée
>
> 1988 : Création du GIEC 
>
> 1995 : A. Merkel ouvre la 1ère conférence mondiale sur le climat
> 
> 1997 : Le protocole de Kyoto est adopté.
> 
> 2015 : Accord de Paris sur le climat adopté
>
> 2019 : Des millions de personnes manifestent dans le monde entier pour une meilleure protection du climat. » *Perowinger94*, Mastodon, 2023


+ [Data / Monitoring (thanks to Zack Labe)] **Antarctic: Sea-Ice Concentration/Extent/Thickness**  <https://zacklabe.com/antarctic-sea-ice-extentconcentration/>
![](/assets/images/antatrtic-seaIce-anomalies.png)
<figcaption class="caption"> 1st graphic: Current Antarctic sea ice extent (NSIDC, DMSP SSM/I-SSMIS F-18) in addition to climatology (blue, 1981-2010) and 2 standard deviations from the mean (updated 5/19/2023). 2nd graphic [Special (temporary) graphic to monitor the ongoing record low] Antarctic sea ice extent for each year from 1979 to 2023 (satellite-era; NSIDC, DMSP SSM/I-SSMIS). 2022 is highlighted with a yellow line. 2023 is shown using a red line (updated 5/19/2023). 3rd graphic: Antarctic sea ice extent anomalies for each year from 1979 to 2023 (satellite-era; NSIDC, DMSP SSM/I-SSMIS). Anomalies are calculated using a 5-day running mean from a climatological baseline of 1981-2010. 2023 is shown using a red line (updated 5/19/2023).</figcaption>
<br />

+ [Publi] **Climate and Environmental Change in the Mediterranean Basin – Current Situation and Risks for the Future. First Mediterranean Assessment Report** <https://www.medecc.org/medecc-reports/climate-and-environmental-change-in-the-mediterranean-basin-current-situation-and-risks-for-the-future-1st-mediterranean-assessment-report/>
  + **MedECC booklet: risks associated to climate and environmental changes in the Mediterranean region** (ENG, FR, AR) <https://www.medecc.org/medecc-reports/booklet-preliminary-results-2019/>

+ [Actu] **Espagne : 700 personnes évacuées à cause d'un feu de forêt « hors de contrôle »** <https://www.lefigaro.fr/international/espagne-550-personnes-evacuees-a-cause-d-un-important-feu-de-foret-dans-l-estremadure-20230519>

> « Les nations unies avertissent ce vendredi 19 mai qu’une quarantaine de pays et plus d’un milliard de personnes sont désormais exposées au choléra. »

+ [Actu] **Inondations en Italie : au moins 3 morts, des milliers de personnes évacuées, le Grand Prix de Formule 1 annulé** « […] *les autorités italiennes, qui préviennent que le pire est peut-être encore à venir.* <https://www.lindependant.fr/2023/05/17/inondations-en-italie-au-moins-3-morts-des-milliers-de-personnes-evacuees-le-grand-prix-de-formule-1-annule-11202270.php>

![](/assets/images/ESMR664-italia.png)
<figcaption class="caption"><a href="https://rapidmapping.emergency.copernicus.eu/EMSR664">EMSR 664 Copernicus Emergency Management Service, Flood in Italy, 16/05/2023</a></figcaption>
<br />

+ [Publi (merci Lapineige)] **Les retenues d’eau : une opportunité d’adaptation au changement climatique ?**, Centre de ressources et d’expertise scientifique sur l’eau de Bretagne (Creseb), <https://www.creseb.fr/les-retenues-opportunite-adaptation-au-changement-climatique/> (et video <https://piped.hostux.net/watch?v=aYL9pJwTdzU>)

+ [Actu] **Ces bombes qui menacent la nappe d’eau de la Crau**

> « Des explosifs militaires attendent encore d’être neutralisés près de Saint-Martin-de-Crau. Des obus, immergés au fond d’un puits, sont une menace pour la nappe phréatique. » <https://www.lamarseillaise.fr/societe/ces-bombes-qui-menacent-la-nappe-d-eau-de-la-crau-KP14280813>

+ [Actu (thanks to tuxom)] **Au Myanmar, le cyclone Mocha a apparemment fait beaucoup plus de victimes qu'on ne le pensait jusqu'à présent**. Au moins 400 personnes auraient perdu la vie dans l'État plutôt pauvre de Rakhine, sur la côte ouest.
Les morts seraient principalement des membres de la minorité musulmane des Rohingyas, persécutés depuis des décennies dans l'ancienne Birmanie - qui est principalement bouddhiste. <https://www.wienerzeitung.at/nachrichten/politik/welt/2188924-Toedlicher-Tropensturm-in-Suedostasien.html>

+ [Actu (thanks Snoro)] **Almost 250,000 flee floods in Somali city that ‘became like an ocean’** <https://edition.cnn.com/2023/05/18/africa/somalia-flooding-displaced-intl/index.html>

> "Floods have caused almost a quarter of a million people to flee their homes after the Shabelle river in central Somalia broke its banks and submerged the town of Beledweyne, even as the country faces its most severe drought in four decades, according to the government."

+ [Actu] **Le Canada lance un appel à l'aide international en raison des feux de forêt**, 18 mai 2023

> « Le Canada a lancé un appel à l'aide international en raison des #incendies de forêt. "La situation en #Alberta reste très préoccupante et dangereuse".
> Le fait qu'il y ait désormais un nombre considérable d'incendies en Saskatchewan, en Colombie-Britannique et dans les Territoires du Nord-Ouest constitue un défi supplémentaire, a-t-il ajouté. Le Canada demande l'aide d'autres pays, les États-Unis,  le Mexique,  l'Australie et  la Nouvelle-Zélande. » <https://orf.at/stories/3317162/>

+ [Actu] **Canadian province of Quebec looks for international support to fight over 160 wildfires** <https://www.pbs.org/newshour/world/canadian-province-of-quebec-looks-for-international-support-to-fight-over-160-wildfires>

> "Québec is dealing with over 160 wildfires.
>
> Over 400 fires are currently burning across #Canada right now.
>
> Over 100 of the fires in Québec right now are "out of control".
>
> It's bad. So bad that Canada is enlisting help from across the hemisphere.
>
> Our air quality is just one symptom of this." <cite>ylove</cite>, Mastodon, June, 6, 2023

![](/assets/images/smog_multiples_hp-Artboard_3.jpg)
<figcaption class="caption">Smoky Air Disrupts Life in the Northeast: A hazy plume from Canadian wildfires delayed flights and prompted the cancellation of three big New York theater productions and two Major League Baseball games. Gov. Kathy Hochul of New York called the worsening air quality “an emergency crisis.” : source <a href="https://www.nytimes.com/live/2023/06/07/us/canada-wildfires-air-quality-smoke">New York Times</a>, June 7, 2023</figcaption>
<br />

+ [Actu (merci Khrys)] **Choléra : un milliard de personnes exposées à cette « pandémie des pauvres », alerte l’ONU** <https://www.liberation.fr/societe/sante/cholera-un-milliard-de-personnes-exposees-a-cette-pandemie-des-pauvres-alerte-lonu-20230519_CQBNHRSJKZFNFPCZCDFM63PQ4Q/>

> « Les nations unies avertissent ce vendredi 19 mai qu’une quarantaine de pays et plus d’un milliard de personnes sont désormais exposées au choléra. L’OMS et l’UNICEF réclament 160 et 480 millions de dollars d’aides pour contenir les épidémies. »

+ [Ressource] **Catalogue de Films sur les Migrations** - Réseau Traces <https://traces-migrations.org/2020/10/26/films-sur-les-migrations/>

+ [Ressource (merci Nicolas Loubet)] **Les publications du Crisis-Lab de SciencesPo** <https://www.sciencespo.fr/crisis-lab/fr/publications>

+ [Film] **Jasmine**, réalisé par <cite>Alain Ughetto</cite>, 2013. « *Dans les années 1970, son cœur s'emballe comme l'Histoire. Il rencontre Jasmine, une jeune Iranienne venue faire ses études en France. Le couple s'aime, mais Jasmine doit retourner dans son pays* » <https://fr.wikipedia.org/wiki/Jasmine_(film)> (voir également « Iran » dans mardi catastrophologie [[3](/mardi-catastrophologie-3)])

+ [Actu (merci Martin Clavey)] **L’exécutif [français] veut déployer le SNU dans les lycées**, « *Ceux qui acceptent cette option seront, eux, récompensés sur Parcoursup.* » <https://www.politis.fr/articles/2023/05/info-politis-lexecutif-veut-deployer-le-snu-dans-les-lycees/>

> « Dès septembre prochain, tous les enseignants au lycée pourront déposer une candidature pour que leur classe réalise un séjour de cohésion de 12 jours, en uniforme et sur temps scolaire. »

+ [Communiqué] **Affaire du 8 décembre : le chiffrement des communications assimilé à un comportement terroriste** (France) <https://www.laquadrature.net/2023/06/05/affaire-du-8-decembre-le-chiffrement-des-communications-assimile-a-un-comportement-terroriste/>

> + Le chiffrement des communications assimilé à un signe de clandestinité
> + Et preuve de l’existence d’« actions conspiratives »
> + Criminalisation des connaissances en informatique
> + et de leur transmission
>
> <cite>La Quadrature du Net</cite>

+ [Ressources] 
  + **Un kit smartphones et vie privée pour animer un atelier !**, <cite>Exodus Privacy</cite> <https://exodus-privacy.eu.org/fr/post/kitdebutant/>
  + **Data Detox Kit** Des actions quotidiennes pour prendre le contrôle de votre vie privée, sécurité et bien-être en ligne à votre manière. <https://www.datadetoxkit.org/fr/home/>
  + **Security in a Box** outils et tactiques de sécurité <https://securityinabox.org/fr/>

+ [Communiqué] **Crise humanitaire et sécuritaire à Paris, l’État ferme les yeux, les associations menacent de se retirer.**

> « Depuis le 4 avril dernier, soit 58 jours, 500 jeunes isolés survivent à même le sol, sans eau courante ni électricité, dans une école désaffectée du 16e arrondissement de Paris. Tous sont ici sans leur famille et sans aucune ressource financière. Beaucoup souffrent de traumatismes psychologiques ou physiques, ainsi que de nombreuses maladies respiratoires et dermatologiques. Tant d’éléments qui définissent, de fait, une situation d’urgence humanitaire en plein centre de la capitale française. Sans action immédiate de l’État, un drame semble inévitable. » <https://utopia56.org/urgent-crise-humanitaire-et-securitaire-a-paris-letat-ferme-les-yeux-les-associations-menacent-de-se-retirer/>

+ [Communiqué] **Bilan canicule et santé : un été marqué par des phénomènes climatiques multiples et un impact sanitaire important** (ex: surmortalité de 2816 décès en France), 22 novembre 2022, <https://www.santepubliquefrance.fr/presse/2022/bilan-canicule-et-sante-un-ete-marque-par-des-phenomenes-climatiques-multiples-et-un-impact-sanitaire-important>

> « La période de surveillance estivale 2022 a été marquée par trois épisodes de canicule intenses, qui ont concerné 78% de la population métropolitaine. L’été 2022 est ainsi le 2ème été le plus chaud depuis 1900 et a eu un impact sanitaire important »

+ [Enquête] **Inondations et barrages dans la Vallée de la Vesdre. L’aménagement du territoire en question** <https://www.terrestres.org/2023/06/05/inondations-et-barrages-dans-la-vallee-de-la-vesdre-lamenagement-du-territoire-en-question/>

> « À l'été 2021, la Belgique a connu des pluies torrentielles qui ont entraîné 100 000 sinistrés. Cette enquête historique retrace 150 ans de projets de transformation et de modernisation de la vallée belge de la Vesdre. A travers l'histoire des barrages c'est une véritable « machine organique » où la technique, la géographie, la biologie, le politique et le social interagissent constamment. »

+ [Livre] **L'odyssée des gènes**, <cite>Èvelyne Heyer</cite>, 2020, <https://inventaire.io/items/2dc1353f406f9ab57b70783c938dbe03>

> « Il est légitime de penser que les Bantous, lors de leur installation en Afrique centrale, ont entraîné un morcellement des populations Pygmées. […] les populations Pygmées ont ensuite échangés des gènes avec les agriculteurs voisins [échanges sexo-spécifique, les femmes Pygmées *voyages* vers les voisins, et très influencé par la place assignées aux femmes et à ségrégation vis à vis des Pygmées, les enfants reviennent avec leurs mères]. Des mutations dans le gène de l'hémoglobine peuvent entraîner une maladie [drépanocytose] lorsque l'on est porteur de deux copies de ce gènes, l'une reçue de la mère et l'autre du père. […] Lorsque l'on est porteur d'une seule copie […] qui confère un avantage […] [qui] protège contre des pathogènes comme *Plasmodium falciparum* qui cause la Malaria (autre nom du paludisme) […] **Malaria** […] la mutation qui confère une résistance à la Malaria. Elle est intervenue il y a environ 7 000 ans, soit à l'époque du Néolithique. En arrivant dans les zones forestières où vivaient les Pygmées, les agriculteurs [de langue Bantoues et originaires d'un zone correspondant à l'actuel Cameroun] ont développé le brûlis en pratique technico-culturelle, ce qui a favorisé la prolifération de moustiques et donc la Malaria. Le variant du gène qui confère la résistance au paludisme s'est glissé dans le génome des Pygmées par mélange génétique et a ensuite augmenté en fréquence par sélection naturelle. C'est encore un exemple de mélange adaptatif, avec un prix important à payer puisque les Pygmées se sont mis à souffrir de drépanocytose. » <cite>Évelyne Heyer</cite>, *L'odyssée des gènes*, 2020 ; 2022. Ed. Flammarion , *p. 194 - 196*

+ [Dossier] **Environnement : Comprendre et agir** par Ritimo (Réseau d’Information Tiers-Monde des centres de documentation pour le développement)), <https://www.ritimo.org/Environnement-Comprendre-et-agir>
  + Introduction
  + Bibliographie générale sur l’environnement
  + Sitographie générale sur l’environnement
  + L’impact des catastrophes naturelles et des risques environnementaux dans le monde
  + Les forêts, biens communs mondiaux
  + Pollution et gestion des déchets dans un monde globalisé
  + Climat : la nécessaire mobilisation citoyenne et internationale
  + Biodiversité, comment inverser la tendance ?
  + Construire un environnement urbain durable

![](/assets/images/collage-femme-ballon.jpg)

## Notes et références
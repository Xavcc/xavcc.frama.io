---
title: "Le jour où j'ai fiché la Liberté..."
layout: post
date: 2019-05-28 15:40
image: /assets/images/fucked_freedom_lucy-1.png
headerImage: true
tag:
- hsociety
- Biononymous
- Bioprivacy
- Rennes
category: blog
author: XavierCoadic
description: ... Et desincubé les revolutionnaires pour fermer les laboratoires aux horreurs
---

La liberté est-elle morte avec l'ordinateur ? Ou alors je me _fiche_ de vous et c'est de la main des humains que nous sommes qui nous détruit ?

La semaine passée je donnais beaucoup de temps à des questions de société, du moins je l'espérais. Une journée « donnée, partage et confiance » par un _service métropolitain_, avec un OuiShaRien qui ajoute de la poudre pour masquer la vacuité et vendre de le poltique de l'attention, une poudre qu'il dit _hack_ de chose, que nous devrions comprendre _hype_ ou happe de cette chose. Mais la responsabilité c'est surfait aujourd'hui et l'éthique ça empêche l'innovation à [en croire Gilles Babinet](https://twitter.com/fo0_/status/1132684944373899264), Vice-Président du Conseil national du numérique.

#### Je suis la vengeance narquoise de Jane

![](/assets/images/a_harvey.jpg)

<figcaption class="caption">Souriez <a href="https://metropole.rennes.fr/sites/default/files/file-PolPub/DP_lancement_SPMD.pdf">SPMD</a> Nous sommes filmés, photographié⋅e⋅s en permanence, et cela est de la donnée − qui renforce ou qui détruit la confiance et le partage ? <a href="https://megapixels.cc/datasets/brainwash">« 5 ans avant l'interdiction de la reconnaissance faciale à San Francisco, un chercheur de Stanford a créé et distribué un ensemble de données de formation sur la détection faciale de clients au Brainwash Café de San Francisco. Leurs images ont atterri dans une université de recherche militaire en Chine »</a> Par Adam Harvey </figcaption>

<div class="breaker"></div>

> « _Ces expressions toutes faites, utilisées mécaniquement, empêchent l’imagination, elles entrainent une incapacité à être affecté par ce que l’on fait et, la personne se drapant dans un aspect banal, entretiennent l’absence de pensée_ » Absence de pensée et responsabilité chez Hannah Arendt − À propos d’Eichmann

La semaine passée, j'ai pris aussi deux jours de travail sur les questions de _reconnaissance faciale_, de captation de l'intimité, de fichage bio-morphologique... Du temps de cerveau disponible investit ou perdu...

J'ai un ami, il s'appelle John Doe. John est intelligent, John est cultivé, John travail dans la photo, la communication et le graphisme, il est doué. Le métier de John consiste à faire passer des messages forts à l'aide d'une grammaire au travers du graphisme.
À  veille des élections européenne, dans le week-end, J'ai demandé à mon ami John Doe ce que l'on pouvait imaginer pour stimuler les esprits, les corps. Il me répondit « C'est presque foutu ! Tout le monde s'en fout des libertés. Tout le monde s'est habitué à donner sans condition » 

Alors que je continue aujourd'hui à travailler sur un atelier pompeusement intitulé « [travail du clic et la surveillance généralisée](https://notecc.frama.wiki/atelier:digital_labor_et_surveillance) », j'ai eu envie d'explorer autrement les questions de corps, de Liberté, dans un environnement numérisé qui _fabrique_, qui _capte_, avec des apôtres qui lubrifient de l'espace et du temps de cerveau disponible. J'ai eu l'espoir de faire réagir, d'abîmer la roue de la logorrhée qui prépare le terrain et les esprits à accepter une banalité du mal, à se contenter du _ce pas grave de se faire violer la vie privée_, la roue la véritable stratégie de Google et autres _AFAMés_... Une roue douce, feutrée, sans heurt, ni violence, ni bruit dans une société où TOUT le monde dit « C'est pas si noir... On peut leur prendre `<insert n'importe quelle connnerie>` ».

Après tout, l'image, la peinture, la photo, la vidéo, l'écran, le mire-écran, semblent être des parties intégrées à nos intimités. Si bien intégrées que presque plus personne ne se rappelle de ce que nous aurions refusé il y 15 ou 20 ans, si bien lissées que presque plus personne ne se soucie de ce qu'elles nous prennent, si profondément implémentées que presque plus personne ne s'inquiète de sa propre liberté. Il ne reste plus qu'à profiter d'être biberonné au [sein artificiel](/damasio-rennes) conçu par des paternalistes fiers héritiers de [Patrick Le lay](https://fr.wikipedia.org/wiki/Patrick_Le_Lay), se conforter dans le techno cocon de big Tata[^1].

Pour des puissants nous ne sommes plus que de la matière à extraire et à raffiner ; pour des _logorrahtrices et logorrahteurs_ nous sommes ne sommes plus que la marchandise d'échange dans un processus de création de dette. Nous sommes certes des corps, mais des corps qui se _zombifient_ en abandonnant nos libertés. Et puis on remplit en permanence notre bol de l'information à notre insu, dupant notre satiété cognitive, culpabilisant notre réactivité face à l'immonde, noyant notre esprit critique dans leurs dystopies.

> « La reconnaissance faciale est devenue la partie la plus visible de la question de la dominance spectrale : à qui appartiennent les longueurs d'onde et qu'est-ce que la résolution d'échantillonnage admissible, la fréquence d'échantillonnage, l'analyse et l'extraction des types, et la propriété, doivent être mieux définies pour une meilleure réglementation. » Adam Harvey de [MegaPixel](https://megapixels.cc)

La semaine passée j'ai aussi croisé Jean-Kevin Marketing. Jean-Kevin son truc à lui, en plus de la poudre à yeux et _hype_ du langage, c'est le maquillage. La peinture pour faire croire qu'il a la solution _start-upable_ et _scalable_ pour vous sauver. Mais Jean-kev' il deux ou trois choses qu'il a pas bien imprimé... Tout d'abord, sa bêtise le rend suspect face au monde entier, face à une police qui traque celles et ceux qui voudraient ne pas être espionnables, face aux personnes qui savent que sa supercherie est une tentative de placement produit dans un espace-temps de cerveau disponible. Et enfin, Jean-Kevin n'a pas compris que 68 points de repères faciaux et moins de 50 lignes de python ce n'est qu'une infime partie dans « [L’œil sécuritaire – Mythes et réalités de la vidéosurveillance](https://linc.cnil.fr/elodie-lemaire-la-videosurveillance-nest-pas-une-preuve-ideale) », Jean-Kevin n'a pas compris que la vraie liberté n'a pas de prix − contrairement à la poudre de liberté.

#### Je suis le canal biliaire irrité de Jane

![](/assets/images/fucked_freedom_jkm.png)

<figcaption class="caption">Fucked Freedom JKM</figcaption>

La liberté c'est un peu comme  la grand-mère de votre famille. Elle est à l'origine de ce que vous êtes, de ce que vous êtes collectivement et individuellement ; elle se meurt dès lors qu'on l'oublie, elle se meurt dès lors que l'on n'en prend pas soin, elle se meurt dès lors qu'elle n'est plus un membre constitutif de notre famille. Liberté c'est notre grand-mère à toutes et tous.

> « _Les zombies sont l’incarnation parfaite – décomposée, fétide, pestilentielle – du questionnement politique peut-être le moins absurde. Que faire, en effet, lorsqu’il est déjà trop tard ?_ » Pierre Déléage anthropologue, chargé de recherche CNRS au Laboratoire d’Anthropologie Sociale, dans Lundi Matin « [Histoire politique du zombi](https://lundi.am/Histoire-politique-du-zombi)

Si je décide d'abandonner notre Liberté sur l'autel du capitalisme de surveillance, si je cède à la tentation d'embrasser Liberté avec du chloroforme ou du cyanure, si je décide de _baiser_ l'iconographie de notre Liberté... Vous réagiriez ? _Oui parce que **ficher** une personne ça ne veut presque plus rien dire, tout du moins cela ne dit plus la peur de perdre une liberté_

#### Je suis le disque dur de Jane

![](/assets/images/fucked_freedom_lucy.png)

<figcaption class="caption">Souriez, le capitalisme de surveillance nous permet d'espèrer pouvoir ficher presque toute personne ayant existé, y compris un ancêtre commun à tous les humains : Lucy. « A reconstruction of 3.2-million-year-old Lucy, perhaps our most famous ancestor. Reconstruction by John Gurche, courtesy of the Cleveland Museum of Natural History</figcaption>

<div class="breaker"></div>

Se pose alors la question « Qu'est ce que l'on numérise lorsque l'on fiche votre bio-morpho-métrie ? ». Pour poser cette question je fais appelle à ma grand-mère Jane[^2].

## Que se passe t'il lorsque je décide de vous « identifier » ?

> **Article 12** « _Nul ne sera l'objet d'immixtions arbitraires dans sa vie privée, sa famille, son domicile ou sa correspondance, ni d'atteintes à son honneur et à sa réputation. Toute personne a droit à la protection de la loi contre de telles immixtions ou de telles atteintes._ » [La Déclaration universelle des droits de l'homme](https://www.un.org/fr/universal-declaration-human-rights/index.html)

D'un point de vue de chez Diderot, un réseau de réseaux, ce que nous appelons Internet, est un corps qui permet des entre-impressions (data) et véhicule des affects par des interactions dans des intra-actions, le « tout structuré » avec une temporalité de rémanence. Et, comme vous, je suis la sédimentation des impressions que j'ai eues et transmises depuis que je suis né, y compris celles fournies par notre grand-mère.
Nous sommes des surfaces sensibles et impressionables, comme les ordinateurs, surface sensible, molle ou dure, qui eux n'ont pas d'affects. La promesse et la beauté du « don à coût marginalement nul », c'est à dire que la donnée brute est un oxymore, elle est forcément prise quelque part pour après être données - offertes. **De l'importance de poser la question du quand, pourquoi et comment de la « prise »**.

#### Je suis l’hypothèse permanente de Jane

![](/assets/images/fucked_freedom_Marianne.png)

<figcaption class="caption">Fucked Freedom Marianne</figcaption>

C'est seulement l'approche individualiste computationnelle qui _dit_ ce coût marginal nul. Alors que ce réseau qui permet ces données et dons, réseau qui est corps, il a un coup, écologique par exemple.

> « _Internet est à la fois de l'information et un corps_ » Yves Citton, dans le Mouton Numérique sur France Culture mars 2019.

Un corps laisse des traces, un corps capte des traces. C'est peut être aussi pour cela que nombreux sont les prétendants à en vouloir au corps d'Internet et aussi à celui des internautes. L'information est différente de la matière, cette information dont l'existence dans la matière est liée à sa circulation, À la différence du [bien rival](https://fr.wikipedia.org/wiki/Rivalit%C3%A9_(%C3%A9conomie)). C'est alors « une dynamique de l'information ». [Le corps, machine à recevoir, traiter et produire des traces ? À partir d'une telle définition, le parallèle semble évident entre corps et ordinateur](https://www.franceculture.fr/amp/conferences/mouton-numerique/le-corps-a-lere-numerique)

### Je suis l'absence totale de surprise de Jane

De l'analyse de nos corps biologique, cellules, atomes, comme de l'analyse du numérique, par la donnée, on ne fait que traduire, par prisme, par discrétisation, nous n'avons pas accès au réel mais à une interprétation du réel. Le réel ne se donne pas à voir dans les données. Se pose la question du taux d'échantillonnage, notamment dans les entre-impressions. La question du taux d'échantillonnage peut réduire, une opération de réduction d'une complexité d'un réel. Ce que fait le numérique. Les données sont des prises par quelqu'un en fonction d'une certaine pertinence prédéfinie et préconçue par ce quelqu'un. S'ajoute des dispositifs techniques qui influent sur ces filtres »

![](/assets/images/fucked_freedom_68_markup.jpg)

<figcaption class="caption">Fucked Freedom Faces</figcaption>

> « _Nous sommes dépossédé·e·s de nos désirs ? De nos individualités ?_ » Yves Citton, dans le Mouton Numérique sur France Cutlure mars 2019.

#### Je suis le bulbe rachidien de Jane, sans moi Jane ne pourrait réguler ni son rythme cardiaque ni sa tension ni sa respiration

J'avais posé une question apparentable à Alain Damasio à Rennes (à [47'](https://www.c-lab.fr/article/vie-etudiante/alain-damasio-emporte-le-tambour.html)) « _Comment (re)devenir corps libres, corps furtifs, aujourd'hui avec ces nouveaux problèmes, corps en mouvement ? Comment les traces biologies et morphologiques pour faire corps libre ?_» :

> « _[...] On investit énormément de désir et d'argent aujourd'hui pour contrôler, dans la contrôle de l'incertitude, contrôle de l'angoisse [...]
Que veut celui qui veut le contrôle ? Il veut le controle de son environnement, la conjuration de l'incertitude. [...] Pour changer ça, il faudrait changer notre vision du monde, accepter l'incertitude, sortir du techno-cocon. [...] Il y a une "conforteresse", c'est une volonté du pouvoir mais aussi du citoyen, dans lequel on veut vivre. [...] Il faudrait "Ne pas vouloir être tranquille d'avance" pour s'en sortir [...] Quand on aura renoncé à ça on arrêtera de relever de traces_ »

![](/assets/images/fucked_freedom_emma_goldman.png)

<figcaption class="caption">Fucked Freedom Emma Goldam, <a href="https://reporterre.net/Emma-Goldman-l-anarchie-un-ideal-d-emancipation">qui  faisait partie, selon le FBI, « des plus dangereux anarchistes d’Amérique »</a> Source Image  cahier central de Vivre ma vie, via Reporterre.net</figcaption>

Les data du numérique ont toutes été _prises_, par vous, par moi, par une multinationale, par des États. C'est un principe de captation, de tractation d'un capital. Ceci étant effectué par des choix. Donc vous prenez une certaine procession de mon corps, des impressions. La plupart des images qui se retrouve sur internet proviennent de nous. Où est alors le capitalisme là maintenant ? Qui lui donne vit et corps ? C'est aussi nous. Avant ces choix opérés, il y a tout un travail de prè-conception avec de conditionnement opérant chargé à la récompense aléatoire pour délivrer de la dopamine dans le processus de participation. Il s'agit de nous fournir une posture observation liée à la récompense ou à la punition avec l'effet du surpris au moment où celle-ci tombe. Puis lors des choix opérés vient la phase de surveillance, souvent enjouée de gamification. Nous pouvons y voir « une [miniaturisation inquiétante de l'affect et de l'empathie, comme un processus qui se déroule en parallèle avec une accélération technologique](https://notecc.frama.wiki/norae:hsociety:homo_note-3-p) ». Un processus qui nous amène nous même à [Surveiller et Punir](https://notecc.frama.wiki/norae:hsociety:homo_note-2-p). Par le passé, des femmes et des hommes ont été enfermé⋅e⋅s pour moins que cela, aujourd'hui c'est à portée de clic de tout potentiel illuminé au pouvoir.

![](/assets/images/fucked_freedom_Mandela.png)

<figcaption class="caption">Fucked Freedom Mandela</figcaption>

McKenzie Wark parle de « classe (sociale) vectorielle » et s’interroge : tou·tes·s hackers ?. Je dirais que nos sommes toutes et tous des prolétaires ou des _commoners_ pris dans la nasse. « La classe vectorialistes est aussi, google par exemple, celle qui se branche sur cette circulation de l'information, pour Wark. Cela peut être du poison ou chose merveilleuse. Qu'est cette chose qui vient de brancher sur de l'information binarisée, composée de 0 et 1 ? Par discretisation. La chaîne d'interprétation sur des corps entre-impressionnés devient complexe. » (Yves Citton).

Commoners ? Peut-être, si et seulement si nous nous défendons de superficialité capitaliste, si nous prenons garde à ne transformer une ressource immatérielle, comme notre Liberté, en tropisme qui favorise la récupération par le marketing électoral et politicien, comme le rappelle Lionel Maurel, pour ne pas tomber encore plus bas dans la « Vulgate des communs »[^3].

> « _Les mots ont un sens et leur emploi est tout sauf neutre. Ré-étiqueter l'élément "ressource" comme un "capital" ne va absolument pas de soi et peut provoquer des glissements problématiques. D'ailleurs, il y a des précédents assez édifiants... <https://www.nber.org/papers/w7600>_ » Lionel Maurel, quelque part sur Twitter

Nous devrions avec passion et fierté reprendre le temps, l'espace et l'attention que nous volent des GAFAMs lorsqu'ils violent nos intimités. Nous nous en servirions pour pratiquer l'[Apprentissage latent](https://en.wikipedia.org/wiki/Latent_learning) ; couler la stratégie de la surveillance ; période d'observation sans récompense ou ni punition. Un apprentissage simple par observation de l'environnement sans motivation particulière, ni  pour apprendre la grammaire du monde ou sa géographie ; puis, à une date ultérieure, être en capacité d'exploiter cette connaissance lorsqu'il y a besoin, comme la nécessité de défendre et de cultiver la Liberté.

Dans cette _explicabilité_ de ce qu'est la surveillance faciale, la captation morphologique et biométrique, la privatisation de l'intimité, expliquer n'est pas le but final. Aujourd'hui nous semblons beaucoup chercher à expliquer les résultats des choix opérés, plus rarement les raisons amont du processus. Cela postule « que le monde est explicable. Plus exactement que nous sommes capables d’expliquer et de comprendre le monde »[^4]. Le politique y a t'il intêret après nous avoir offert à Google ? c'est pourtant simple : **La surveillance faciale c'est une des entrée du viol de nos intimités**. Ça n'approte pas plus de sécurité, ni à votre _smart_ phone, ni à l'établissement recevant du public ; ça n'apporte pas grande plus-value à la preuve juridique. Ça sert à nous espionner et nou sficher pour exercer un contrôle social.

Il existe effectivement bien des mises tensions[^5] fortes entre ce qui concerne le rendu des informations publiques, c'est à dire un affichage brut de la surveillance, une ouverture sans assurer de chemin vers une possible compréhension, et œuvrer à offrir des capacités d'un travail interprétatif, ce qui diffère de la justification, ouvrant une autre étape et d'autres tensions. Une tension entre le descriptif, le prescriptif et le justificatif, qui dans le [monde algorithmisé tend parfois vers le compromis](https://dataanalyticspost.com/lexplicabilite-des-algos-une-affaire-de-compromis), c'est à dire vers une concession, voir une dette.
 
Le but final est la défense des liberté, de notre Liberté, passant peut être par « [La vie privée pour les faibles, la transparence pour les puissants](https://www.laquadrature.net/2019/05/01/la-vie-privee-pour-les-faibles-la-transparence-pour-les-puissants/) », sans endetter les faibles.

Un problème majeur persiste dans la difficulté de s'assurer qu'une personne puisse comprendre ou a compris un tel message, les conditions de l'exercice du labour interprétatif étant inégalement distribuées en fonction des personnes ou du contexte, il est aujroud'hui confié aux petites mains que nous sommes, [micro travailleuses et travailleurs de l'ombre](https://lejournal.cnrs.fr/articles/ces-microtravailleurs-de-lombre?utm_term=Autofeed&utm_medium=Social&utm_source=Facebook#Echobox=1558692129), et dans cette _mission_ on nous pré-charge de confort à l'acceptabilité de perdre quelques libertés. 

Délivrer un message est un effort compliqué, l'interpréter en est que plus délicat. Dans l'exercice même de cet article, je subjective et objective tour à tour dans un flou éditorialisé une interprétation de _la surveillance des visages_. 

#### Je suis la vie gâchée de Jane

Quid de technologies conçues de mains humaines auxquelles l'on confère un pouvoir, ou un espoir, d'opérations logiques lorsqu'elles ne produisent pas ce que l'on attend d'elles ? Lorsque qu'elles font défaut à la _reconnaissance_ qu'on leur exige ? Quand bien même cette opération serait hors de toute éthique.

![](/assets/images/fucked_freedom_saigon.png)

<figcaption class="caption">Fucked Freedom People of Saigon − Downtown Saigon 1970 - Đường Tự Do - Soldiers used for crowd control. Photo by Phillip Jones Griffiths</figcaption>

Et le droit à l'erreur des humains conceptrices et concepteurs ? Et le droit à l'erreur, droit au pardon, droit à l'oubli, des humains aux visages captés ? Ça ne s'efface pas la biométrie, le visage, la morphologie... Sauf à très grand renfort de torture ignoble.

#### Je suis la sueur froide de Jane

Ils étaient venus ficher les autres, je n'avais rien dit. Quand il sont venus me ficher... Je me suis rendu compte que j'étais dèjà dans la partie des autres.

![](/assets/images/fucked_freedom_ERA.png)

<figcaption class="caption">Fucked Freedom for Women in Equal Rights − Photograph of Jimmy Carter Signing Extension of Equal Rights Amendment (ERA) Ratification, 10-20-1978</figcaption>

Aux USA en 2019, des conservateurs « pro-vie » s'acharnent contre le droit à l'avortement qu'ils tentent de restreindre dans plusieurs États américains. Dans unE époque où criminaliser une personne et l'enfermer est à portée d'un clic. La vibrante chaleur auburn à tendance noire d'un œeil totalitaire qui voudrait se faire passer pour la main équilibrée de la justice aveugle.

### Je suis le colon de Jane

Mémoire du Mal et Tentation du bien... Les tortionnaires et les _faiseurs_ de fichage à grande échelle sont aussi victimes de la surveillance et du viol de leur vie privée. Devons-nous protÉger leur Liberté ?

![](/assets/images/fucked-freedom_bolsonaro.png )

<figcaption class="caption">Fucked Freedom Bolsonaro</figcaption>

#### Je suis le sentiment de rejet exacerbé de Jane

![](/assets/images/fucked_freedom_Biden.png)

<figcaption class="caption">Fucked Freedom Joe Biden − Source <a href="https://github.com/ageitgey/face_recognition">Face recognition</a></figcaption>

#### Je suis l’absence totale d’optimalité de Jane

La liberté meurt, elle crève de ne pas recevoir l'attention et les soins que nous lui devons. Nous servons le foie encore saignant de la liberté à l'Hannibal Gafam Lecter avec un délicieux Chianti. Les politiqiues font la logistique des corps, préparent la soupe pour le cannibale et pour nous. Mais qui prépare le cadavre, qui dépèce le corps, qui chambre le vin, qui sert le plateau avec un sourire courtois ? C'est moi, c'est vous, c'est nous...

![](/assets/images/fucked_freedom_StateUSA.png )

<figcaption class="caption">Fucked Freedom Statue of Liberty</figcaption>

Nous devons réapprendre que la véritable vie organique ne s’enferme pas dans des gangues. Que celles et ceux qui acquiescent à cette aliénation, qui constsuisent des cocons anti-libertés, se posent la question suivante:
« En tant qu’organisme vivant synthétiquement assemblé pièce à pièce, peut-on vieillir et mourir d’une vie que l’on a jamais vraiment eue ? » . Déchirez la gangue !

Liberté n'est pas chose marchande, n'est pas objet de start-up. 

La volonté de nous incuber, de nous accélérer pour notre _plus grand bien_ est profondément dérangeante. A titre d’être vivant c’est déjà plus que dérangeant. 
Incuber ? Mettre en cube, sous cloche, copie qu’on forme !

Si l’on applique cette volonté d’incubation et d’accélération aux nécessités de réelles innovations et transitions qui se présentent devant nous, l’avenir devient très sombre car, sous couvert de bonnes intentions, nous contaminons dans l’œuf (voire empoissonnons) tout embryon de réel changement d’un système pervers et nocif. 

> « _Des millions de personnes ont téléchargé des photos dans l'application Ever. Ensuite, l'entreprise s'en est servie pour développer des outils de reconnaissance faciale. "Les développeurs de l'application n'étaient pas clairs sur leurs intentions," a dit un utilisateur de Ever. "Je crois que c'est une énorme invasion de la vie privée._" » May 9, 2019, By Olivia Solon and Cyrus Farivar, dans [NBC NEWS](https://www.nbcnews.com/news/amp/ncna1003371)

> « "Faire des souvenirs" : C'est le slogan sur le site de l'application de stockage de photos Ever, accompagné d'un logo et d'un album d'exemple intitulé "Weekend avec grand-père" »

Accélération? Vous réassembler pièce à pièce après vous avoir capturé sans vous mutiler, avec vos goûts, vos désirs, vos envies puis vous dire comment vous êtes et qui vous devez être. C'est aussi un peu ça la _reconnaissance_ faciale, surveillance, privatisation et privation qui de dit pas son histoire, fichage qui ne fait plus peur.

Mémoire du mal et tentation du bien...

#### Je suis le cœur brisé de Jane

Ce capitalisme vorace est le même que celui de la colonisation, seul les moyens techniques ont changés afin d'accroître son impact social. Il permet l'accaparement des cultures pour les uniformiser en une surface de marché globalisé, des ressources (d'ailleurs il nomme presque tout ressource) naturelles pour les marchander. La sédition de notre intimité et de notre métrie bio-morphique semble être le prix à payer et décider comme acceptable lors de l'achat d'un _smart_ coffre à métaux rares qui ne fait presque plus téléphone. Le travail du clic, travailleuses et travailleurs presque gratuit pour _entrainer_ les _algorithmes_ du capitalisme, puis on vous demandera le bénévolat pour votre bonne conscience et réparer les dommages causés.

[Shoshane Zuboff](https://fr.wikipedia.org/wiki/Shoshana_Zuboff) vous auraient bien offert des lignes à lire et des concepts à réfléchir sur le vol de ressource, la captation de l'expéreince humaine, l'asymétrie des actrices et acteurs dans la tension des pouvoirs, mais il y a trop de Jean-kevin Marketing pour laiser de l'esapce-temps cultivable pour les émancipations − et sortir des _Requirementos de los Conquistadores_ like a CGU.

> « _Nous, les habitants du numérique, sommes comme les Tainos des Caraïbes du XVIe siècle, lorsqu'ils ont rencontré pour la première fois les conquistadors espagnols (perçus comme des " dieux "), ou l'arrivée de la "voiture sans chevaux" dans la société américaine du début du XXe siècle, tirée par des chevaux_. » [Vermont Independant]

Liberté n'a pas de prix. 

![](/assets/images/fucked_freedom_delacroix.png)

<figcaption class="caption">Fucked Freedom « La Liberté guidant le peuple » − It Urges to Rise !</figcaption>

Votre grand-mère, vos enfants, ne sont pas des marchandises. Leurs visages, vos visages, nos visages, ne sont pas des choses que l'on échange conte un droit de passage. **Ce sont nos intimités, Ce sont une part non négociable de nos droits fondamentaux à toutes et tous**

<iframe src="https://mamot.fr/@LaQuadrature/102174327179932614/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400" allowfullscreen="allowfullscreen"></iframe><script src="https://mamot.fr/embed.js" async="async"></script>

<iframe src="https://mamot.fr/@LaQuadrature/102174329218040112/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400" allowfullscreen="allowfullscreen"></iframe><script src="https://mamot.fr/embed.js" async="async"></script>

<iframe src="https://mamot.fr/@LaQuadrature/102174330313388401/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400" allowfullscreen="allowfullscreen"></iframe><script src="https://mamot.fr/embed.js" async="async"></script>

## Notes et références

[^1]: techno cocon, Big tata, expressions empruntées à Alain Damasio qui en use pour illustrer ses propos de société panoptique et son choix d'histoire pour le livre « Les Furtifs », comme ici dans une interview Thinkerview <https://thinkerview.video/videos/watch/d3472aea-41dd-43bd-bffe-b5e383cf1a74> ou encore lors de sa conférence à Rennes <https://www.c-lab.fr/article/vie-etudiante/alain-damasio-emporte-le-tambour.html>

[^2]: « Je suis [...] de Jane » est un figuration empruntée et remixé à l'ouvre « Fight Club » lorsque le narrateur Jack, interprété par Edward Norton, s'exprime.

[^3]: Communs & Non-Humains (1ère partie) : Oublier les « ressources » pour ancrer les Communs dans une « communauté biotique »  <https://scinfolex.com/2019/01/10/communs-non-humains-1ere-partie-oublier-les-ressources-pour-ancrer-les-communs-dans-une-communaute-biotique> 

[^4]: Claire Journiac, Sandra Drevet, Soins infirmiers: Démarches relationnelles et éducatives, initiation et stratégies de recherche, Éditeur Elsevier Masson, 2002, 

[^5]: « Cette tension entre la transparence comme diffusion d’informations et la trans-parence comme mise en visibilité des processus traverse l’ensemble des initiatives de ce type. Les « seules » données nous disent bien peu, car ce qui importe est davantage lié à leur élabo-ration (collecte, manipulation, traitement, etc.) et aux différents acteurs impliqués dans leur divulgation (équipe interne, infomédiaires, etc.). » − “Le design de la transparence : une rhétorique au coeur des interfaces numériques”. Loup Cellard et Anthony Masure. Revue Multitudes, 2018/4 n°73, pages 107


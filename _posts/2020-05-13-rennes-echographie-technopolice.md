---
title: "Échographie : Rennes 10 ans de glissement en techno-surveillance policière"
layout: post
date: 2020-05-13 14:20
image: /assets/images/Screenshot_2020-05-13.png
headerImage: true
tag:
- hsociety
- Rennes
- Catastrophologie
category: blog
author: XavierCoadic
description: toutes et tous suspecté⋅e⋅s
---

À Rennes, depuis les « 7 premières caméras municipales de 2010[^1] », la vague des premières caméras de surveillance ont été installées en 2010 et « se déploient à Rennes » ensuite en 2012[^2]. Les premières contestations contre le dispositif se font entendre début juillet 2016 : plusieurs caméras du centre commercial Italie sont dégradées à l'aide d'un engin de chantier[^3]

En 2016, on aime pas les caméras à Rennes. Des manifestant·e·s mettent à terre une caméra de vidéosurveillance pendant une manifestation contre la loi travail[^4] :

![](/assets/images/Screenshot_2020-05-13.png)

En 2017 : malgré un rapport externe réalisée par Eric Heilmann (commandé par la ville) démontrant l'inefficacité de la vidéosurveillance[^5], il a été décidé de redéployer les 32 caméras de vidéosurveillance déjà présentes sur le territoire et d’en acquérir 17 autres (dont 9 pour remplacer les plus usagées). À cela, la ville se dotera en plus de 8 caméras mobiles qui pourront être fixées sur les lampadaires puis retirées dès la fin d’une manifestation ou d’un rassemblement, par exemple

![](/assets/images/Screenshot_2020-05-13-Rennes-of.png)

En ces temps passés, le groupe Europe Écologie Les Verts s'émouvait de la situation[^6], par la plume d'une membre de la majorité municipale (Conseillère municipale, Co-présidente du groupe écologiste).

Le [rapport la Gazette](https://archive.is/pHQsB), « Vidéosurveillance 2013 / 2020 », à partir d’informations recueillies dans les médias et auprès des mairies, donne pour les caméras municipales Rennaises une évolution de 100% du nombre de caméras, selon les chiffres de la municipalité pour les dispositifs dont elle a la responsabilité (25 en 2013 à 50 en 2019). Et celle ne concerne que les caméras relevant de la responsabilité de la municipalité.

Ironie dans le même temps, la vidéosurveillance sert juste alors son procès contre les œuvres de la police. Une camera d'un bistrot, surveillance privée, fournie un élément de preuve pour [faire condamner l'ex-chef de la brigade anti criminalité rennaise](https://web.archive.org/web/20191025230854/http://alter1fo.com/proces-chef-bac-rennes-114988).

Rennes est une des 11 lauréates « écologiques » avec entre autres Marseille et Saint-Étienne, en 2015, pour la ville « démonstrateur industriel pour la ville durable »[^7]. Pourtant Rennes est depuis 2013, dans son plan local d'urbanisme via son institut d'aménagement et d'urbanisme, le terrain d'expérimentation « associé » à Dassault système, un éditeur de logiciels spécialisé dans la conception 3D[^8], propriété du Groupe industriel Marcel Dassault. En avril 2019[^9], [Eegle](https://www.eegle.io) expose une plate-forme développée avec Rennes Métropole, Dassault Systèmes et le pôle de compétitivité Images & Réseaux − « faire disparaître les contraintes de la gestion des données numériques territoriales dans un environnement visuel de dernière génération ». Le groupe d'armement Industriel Dassault n'est pas le seul à composer le paysage économique et politique rennais, la société [Idemia](https://fr.wikipedia.org/wiki/Idemia) leader de l'identification et de la sécurité biométrique, en Afrique[^10] et en Europe entre autres, trouve ses racines chez Oberthur Technologies, issue de l'imprimerie Oberthur fondée à Rennes.

Concevoir et occuper les espaces matériels et immatériels, les champs médiatiques, politiques, économiques, urbanistiques avec comme ingrédients pré-requis la numérisation, le fichage, la surveillance… S'encrent dans la morphologie de la ville « à la rennaise », les plumes sont tenues dans les mains Partenariales Public-Privé (PPP) et trouvent des pages d'expérimentations à l'international.

En février 2017, à l’initiative de l’association de commerçants Le Carré Rennais, trente boîtiers sont installées dans des boutiques du centre-ville[^11]. Comme l'explique Isabelle Laperche, directrice du Carré : « Equipés de capteurs, ils localiseront tous les smartphones connectés au réseau wifi (soit en moyenne 70 % des mobiles). L’objectif, c’est de connaître le parcours des clients. Nous pourrons par exemple savoir à quelle heure ils viennent, combien de temps ils restent dans nos magasins et où ils vont ensuite », avec la société Bealder[^12]. La CNIL avait saisi de ce cas[^13], le président de l'association Le Carré rennais démissionné[^14].

C'est en octobre 2017 que les drones de la gendarmerie arrivèrent pour « remplacer »[^15] l'hélicoptère de surveillance des personnes en manifestation équipé de puissantes caméras − Hélicoptère dont la présence et la musique de la mécanique est aujourd'hui bien inséré dans le folklore local.

La surveillance de la population par les drones prend son envol sur le prétexte des manifestations. L'année 2019, devient celle de prise en photo du drone, ou des drones, de la préfecture police

![](/assets/images/Screenshot_2020-05-13-POLITISTUTION .png)

L'État en région Bretagne usant même du ressort de la communication, pareille à celle de l'attractivité territoriale, pour diffuser la vidéo sur les réseaux sociaux.

![](/assets/images/Screenshot_2020-05-13-bretgane-gouv.png)

Le 6 juillet 2019, un drone était utilisé au Centre de Rétention Administrative de Rennes St-Jacques lors des parloirs extérieurs pour surveiller les personnes venues intervenir et pour surveiller des personnes étrangères sans papiers placées en rétention [^16].

En 2019, à la CCI de Rennes, le 17 novembre, VINCI Facilities a organisé sa première journée « innovation nationale ». Un événement avec des ateliers de démonstration de solutions co-animés par ses partenaires. Les participants, des clients et prospects, ont pu notamment appréhender la reconnaissance faciale avec la start-up rennaise Dynamixyz, l’approche hôtelière personnalisée appliquée en entreprise avec Opal, ou encore la maquette numérique pour la gestion technique de bâtiments (BIM) »[^17]. Un nouvel événement marketing qui vient compléter quatorze autres expérimentations de « smart city » menées depuis deux ans à Rennes.

Le projet  « Rennes Urban Data Interface » nommé dans la règle de l'art du marketing d'attractivité territoriale devient fer de forge de la « ville à la rennaise ».

En décembre 2019, le projet Rennes Urban Data Interface « RUDI » a été présenté à la journée du Service Public Métropolitain de la Donnée. Un projet dans lequel l'Union Européenne a investi 4 millions d'euros [^18].

> « Lancement du projet en septembre 2019 pour une durée de trois ans (2019-2022)

> Budget : 4,95 millions d'euros dont un co-financement européen (FEDER) de 3,96 millions d'euros

> Partenaires : Irisa, Lego (laboratoire d'économie et de gestion de l'Ouest), LTSI (laboratoire traitement du signal et de l'image), Ouishare, Fing, Tiriad, Keolis, Enedis, GRDF, Ouest-France et Conseil de développement de Rennes Métropole.
Pour en savoir plus sur l'appel à projet européen UIA > lire le communiqué de presse de la Commission européenne. 

On compte aujourd'hui plus 400 caméras de surveillance à Rennes « intra-rocade »[^19], notamment avec [Keolis Rennes](https://fr.wikipedia.org/wiki/Keolis) (opérateur privé franco-québécois de transport public) comme opérateur de surveillance.

![](/assets/images/rennes-cam_intramuros.png)
![](/assets/images/rennes-cam-centre.png)
![](/assets/images/rennes-sous_surveillance.png)
![](/assets/images/rennes-sous-surveillance-2.png)

<figcaption class="caption">Sources Openstreet Map et <a href="https://rennes.sous-surveillance.net/">Sous-surveillance.net</a></figcaption>
<br/>

Ce travail des « données », possible grâce aux contributions cartographiques de citoyennes et citoyens, ne prend pas en compte les dispositifs de vidéosurveillance présents dans les bus − plusieurs caméras par véhicule − ni ceux du métro et des stations largement équipés.

L'évolution de la vidéo surveillance rennaise est symptomatique, bien que petite ville avec un capital de départ en infrastructure de petite taille, l'augmentation du parc et des moyens sont massifiés et surtout délégués aux entreprises du secteur privé.

> « *Depuis deux ans, quatorze solutions y sont testées sur une plateforme numérique collaborative développée par Dassault system, mise à disposition par Rennes métropole et orchestrée par le pôle Images et réseaux. Sur le terrain de jeu logiciel, une dizaine d’entreprises du territoire testent des solutions intelligentes : « un double de la ville et des bâtiments pour suivre les consommations énergétiques quartier par quartier », « une cartographie de la biodiversité rennaise », « un système de suivi des déplacements des habitants des petites communes alentour », liste Darin Beach, chargé de pro-jets. Un supermarché de solutions dites « intelligentes », dans lequel la Métropole se réserve la possibilité de piocher, ou non, les futurs services de ses habitants.* » Le Mensuel de Rennes, Janvier 2020 

Pour les municipales de 2020, après 19 années de mairie Socialiste à Rennes, le second et principale engagement de Carole Gandon, candidate La République en Marche à Rennes, est le triplement du nombre de caméra de surveillance pour la ville[^20]. 

En décembre 2019, un personne est décédée après sa tentative de suicide au centre de rétention administrative[^21] et en janvier 2020 un nourrisson de 5 mois y est enfermé alors que son père est hospitalisé pour maladie grave.

> À Rennes, au centre de supervision urbaine, les caméras traquent les délinquants

> « En janvier 2020, 36 caméras de vidéoprotection sont installées dans la ville, sur la voie publique. Elles rassurent, quand le climat politique se focalise sur la question de la sécurité. » [Ouest-France](https://www.ouest-france.fr/elections/municipales/rennes-au-centre-de-supervision-urbaine-les-cameras-traquent-les-delinquants-6747300), Angélique Cléret, 21/02/2020
<br/>

<figcaption class="class">vidéoprotection pour désigner surveillance de la population par caméra et logiciel</figcaption>

Ce qui correspond à des dépenses à auteur 75 000 € pour la création du centre de supervision urbaine, au cours du mandat de Daniel Delaveau (2008 -2014). Puis, 440 000 € pour les nouvelles caméras et 205 000 € de mise à niveau du système, au cours du mandat de Nathalie Appéré (2014 -2020). L’État aurait financé 50 % de l’investissement, entre 2008-2014, réduisant ensuite sa participation à ces dépenses, selon Angélique Cléret pour Ouest-France.

À Rennes, depuis 2015 et encore aujourd'hui en 2020, la mort de Babacar Gueye tué de 5 balles par un policier[^22], continue à être une affaire judiciaire non résolue dans la ville « smart protégée écologique jusqu'au parcours client ». L'absurdité de la surveillance et l'horreur de l'injustice sociale se mêle dans les feuilles de l'histoire de la ville. L'efficacité, la pertinence de cette chute résonne chaque jour dans le nouvel ordinaire d'une ville dite paisible. Jusqu'à l'Université de Rennes 1 qui, sous couvert d'impériosité due à la pandémie Covid-19, se complait à surveiller ses étudiant⋅e⋅s pour les examens de l'été 2020.

> « *Bien sûr, il est très probable que les fantasmes de la DGESIP soient écrasées par le pragmatisme des universités, comme cela a déjà été le cas pour les autres dispositifs d’examens télésurveillés et comme cela sera très certainement le cas s’agissant des recrutements télésurveillés – qui ont suscité l’hilarité générale, teintée d’un sentiment de malaise inquiet. Ce qui est sûr, en revanche, c’est qu’il y a comme un écho terrible entre cette ouverture inattendue du monde universitaire à la reconnaissance faciale et le « vote » – purement indicatif, conformément à l’article 50-1 de la Constitution – sur le tracking « Stop Covid »3 qui se tiendra demain à l’Assemblée nationale  : décidément, l’épidémie de covid-19 joue le rôle d’horrible accélérateur pour les dispositifs technologiques de contrôle des individus.* » [Et maintenant la reconnaissance faciale pour les examens ?](https://academia.hypotheses.org/22959)

10 années de surveillance *technologique* qui laissent apparaître une certaine logique politique en responsabilité partagée.

![](/assets/images/screen-alter1fo.png)
<figcaption class="caption">Source Alter1fo <a href="https://web.archive.org/web/20200408122454/https://alter1fo.com/drone-rennes-chronologie-126868">Dronologie rennaise</a></figcaption>
<br/>

## Important

Afin d'améliorer et de poursuivre cette échographie de la surveillance et de la chute techno-policière à Rennes je vous invite à vous inscrire et contribuer dans **[Forum.Technopolice.fr](https://forum.technopolice.fr/)**

## Remerciements

+ Politistution pour les contributions
+ Les contributrices et contributeurs au Carré du Forum <https://technopolice.fr/>
+ Toutes les personnes impliquées dans la soirée « Café-débat à Rennes sur la Technopolice » du 13 Mars à Rennes <https://technopolice.fr/blog/cafe-debat-a-rennes-sur-la-technopolice/>

## Notes et références

[^1]: Archive <https://web.archive.org/web/20100904035103/https://www.video-surveillance-nantes.fr/actualite/rennes-video-surveillance.html>
[^2]: les caméras de surveillance s'ancrent à Rennes, <https://archive.fo/SIgje>
[^3]: Ouest France Juillet 2016 <https://archive.fo/HGb3A>
[^4]: Archive de la vidéo <https://archive.is/iD2ee>
[^5]: Archive de l'article de presse locale, AlterInfo par Politistution <https://web.archive.org/web/20191106111354/http://alter1fo.com/rapport-videosurveillance-inefficace-112513>
[^6]: Archive du communiqué du groupe EELV https://web.archive.org/save/https://elus-rennes.eelv.fr/souriez-vous-netes-presque-pas-filmes/
[^7]:  Paris, vendredi 25 mars 2016 Ségolène Royal et Emmanuelle Cosse  réunissent les lauréats de l’appel à projets « Démonstrateurs industriels pour la ville durable » <https://web.archive.org/web/20200107152458/http://www.divd.logement.gouv.fr/IMG/pdf/2016_03_25_sr_ec_demonstrateurs_ville_durable.pdf>
[^8]: La solution 3DEXPERIENCity de Dassault Systèmes réunit les intervenants du projet de ville intelligente dans « Virtual Rennes » <https://web.archive.org/web/20200513115032/https://decryptageo.fr/rennes-metropole-definit-virtuellement-son-avenir-durable-avec-dassault-systemes/>, (2016) et <https://web.archive.org/web/20140208200834/http://decryptageo.fr/la-ville-de-rennes-et-rennes-metropole-sassocient-a-dassault-systemes-pour-penser-la-ville-de-demain/>, (2013) et CR du CNIG de 2018 <https://web.archive.org/web/20200107150642/http://www.cnig.gouv.fr/wp-content/uploads/2018/07/2018-04-11-CR-GT-Geostandard-3D-1.pdf>. Partenariat Rennes Métropole / Dassault SystèmesApplication au plan Local d’Urbanisme de la Ville de Rennes - Rennes 2030 <https://web.archive.org/web/20200107151725/http://www.reseaunationalamenageurs.logement.gouv.fr/IMG/pdf/4-rennesdiaporama22-9-2016.pdf>
[^9]: source Usine Digitale <https://archive.is/NMl7Q>
[^10]: Ficher biométriquement 400 000 pers. en Guinée, « des futurs bénéficiaires de la politique nationale d’inclusion économique et sociale » et l'entreprise Ademia <https://web.archive.org/web/20191101032831/http://www.ledjely.com/2019/10/04/inclusion-sociale-vers-lenrolement-des-beneficiaires-de-lanies/>
[^11]: Le Carré Rennais et le tracking par wifi en centre ville <https://archive.fo/uf4Ic>
[^12]: Bealder <https://web.archive.org/web/20180820045819/https://bealder.com/>
[^13]: Saisine CNIL wifi rennes Carré Rennais <https://archive.fo/RcCXs>
[^14]: démission du président du Carré Rennais <https://archive.fo/0RxEX>
[^15]: « Dronologie Rennaise par Alter1fo <https://web.archive.org/web/20200408122454/https://alter1fo.com/drone-rennes-chronologie-126868>
[^16]: Drone pour le CRA <https://web.archive.org/save/https://france3-regions.francetvinfo.fr/bretagne/ille-et-vilaine/rennes/centre-retention-rennes-drone-servi-surveiller-etrangers-1698546.html>
[^17]: par Vinci <https://archive.is/E3JEm>
[^18]: Source Rennes Métropole <https://web.archive.org/web/20200130033322/https://metropole.rennes.fr/4-millions-de-leurope-pour-le-projet-rudi>
[^19]: Code source Overpassturbo <https://overpass-turbo.eu/s/Pyb> et données OpenStreet Map <https://overpass-turbo.eu/s/Pyc> ; données et cartographie « sous-surveillance.net » <https://rennes.sous-surveillance.net/>
[^20]: Archive <https://archive.is/pb2g2>
[^21]: Journal libération <https://www.liberation.fr/direct/element/tentative-de-suicide-au-centre-de-retention-de-rennes_106806>
[^22]: Babacar Gueye, « Rennes. Babacar : « On veut la vérité sur sa mort » » Ouest France <https://www.ouest-france.fr/bretagne/rennes-35000/rennes-babacar-veut-la-verite-sur-sa-mort-6643284>


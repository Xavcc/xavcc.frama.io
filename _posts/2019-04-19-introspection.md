---
title: "Entre recherche de traces, leurs effacements et le prétoire du rien"
layout: post
date: 2019-04-19 13:40
image: /assets/images/f18049583.jpg
headerImage: true
tag:
- hsociety
- Biononymous
- Bioprivacy
- Rennes
category: blog
author: XavierCoadic
description: Solitude, insulation, isolation, introspection
---

Bloqué... Je suis dans un moment où la sensation d'un quotidien obstrué est suffocant. Engoncé entre l'écriture d'une suite de « [De l’hypothèse de la documentation comme technique de résistance et du wiki comme objet de ces résistances](https://sens-public.org/article1375.html) » et des recherches et tests dans le programme « [Biononymous & bioprivacy](/biononymous-bioprivacy-research), je me sens comme un clandestin dans la cale d'un bateau, dans une coquille de noix à la dérive sur un océan, sans vraimment savoir où, sans lueur ni emprise sur mes libertés. J'ai besoin de vous écrire mes amers et mes abers.

## Refroidissement forcé et contraint

Insulé[^1], je me rebiffe par un « Blogito ergo sum », contre une forme de pression par sélection. Sélection exercée sur nos corps par des [dévoreurs de Mondes](https://video.passageenseine.fr/videos/watch/eddda14c-69a2-470c-8a3a-068ab3e59f49), biopouvoir[^2] et pressions biopolitiques, « la miniaturisation inquiétante de l'affect et de l'empathie, comme un processus qui se déroule en parallèle avec une accélération technologique ». Notre insulation est devenue une hyperinsulation, le monde s’est rapproché de nous par l'Internet, pourtant ses parois externes (utérus de [Sloterdjik](https://fr.wikipedia.org/wiki/Sph%C3%A8res_(Peter_Sloterdijk))) contrissent la sphère en épaississant ses parois et cela individualise l’espace de construction de soi. Incarnée, ma militance, qualité de ce qui est, vient se collisionner avec la plus intime des radicalités qu'est mon corps. Cette action intense est accélérée par ma posture assumée de « recherhce-action embarquée. »

Je suis autant en réseaux que je me sens souffrir de mon insulation.

En puisant chez Diderot, un réseau de réseaux, ce que nous appelons Internet, est un corps qui permet des entre-impressions (data) et véhicule des affects par des interactions dans des intra-actions, le « tout structuré » avec une temporalité de rémanence. « Je suis la sédimentation des impressions que j'ai eues et transmises depuis que je suis né. »[^3]

Je suis au temps des réseaux de réseaux qui me fait souffrir d'isolement.

> _« No man is an island, Entire of itself, Every man is a piece of the continent, A part of the main. »_ John Donne

Dans l'ether de l'océan sur lequel je dérive d'autres navires s'arment et écumes les espace-temps :

+ Ils violent vos intimités, vie privées, et celles de toutes personnes autour de vous, avec des données / data
+ Ils ne paient pas d'impôts, ce mécanisme qui permet les écoles, hôpitaux, aides sociales…
+ Ils violent les lois
+ Ils s'approprient notre génome
+ Ils aident à tuer des humains avec des drones sur signature de métadonnées
+ Des associations, des politiques, des entreprises sont complices de ces navires. Celles tout près de chez nous comme au plus niveau national et international.

Je désespère de trouver des îles délaissées par ces prédateurs.

## Des îles abandonnées[^4]

... Qui sont autant de bateaux immobiles que nous ne commandons pas. Des morceaux de corps, parfois visités, parfois désertés, qu'il serait trop facile de considérer innocent avec des humains qui viendraient y concevoir et vivre leur paradis ou leur enfer. Poule hor survivre nous cherchons le désirable.

« Une île, c'est d'abord une ombre à  l'horizon de la mer. une ombre qui grossit, s'étale, prend du relief et des couleurs au rythme de l'approche, puis à un moment, la silhouette se dessine et se fige ; alors elle ne fera plus que grossir au fil de l'heure ». Une de mes îles que j'aperçois à l'horizon des évènements s'appelle « [île des levures](https://notecc.frama.wiki/norae:biologicus:biomimetisme) ». Elle se tient à la limite éventuelle d'une région biopoltique qui peut être influencée dans le futur par une observatrice ou un observateur situé⋅e en un endroit donné à une époque donnée, à la frontière de mondes tangibles par la grande partie des personnes de la Terre.  

« Les îles sont un compromis acceptable de notre inconscient ». La fascination pour les îles repose sur l'imaginaire d'espace dominable, de lieux de _possibles_ a priori, une proposition de voyage. Et quand bien même vous trouveriez une île où vous voudriez pas accoster, ni vivre, elle resterait désirable car loin des autres, et que tout ce qui est loin des autres peut parfois sembler délicieux.

Une autre île vers laquelle je tends parfois dans ma dérive est « [l'île aux traces](https://notecc.frama.wiki/norae:biologicus:bioprivacy) ». Là j'y explore les paradis et enfers de l'obfuscation ou de l'effacement de nos traces, parfois je le fais avec des personnes réfugiées, celles qui pistées, traquées, fichées, car migrantes dans le langage des dominants. Il faut alors se méfier des cartes géopolitiques, là où la matière génétique devient propriété contrôlée par des ravageurs des mers, qui en font parfois des cartes [géo-politico-génétique](/pandore-adn). Elles ne reflêtent que la couleur de ceux qui gèrent provisoirement telles ou telles autres couleurs biopolitiques. « L'image cartographique, celle du vivant, nous en dit plus, elle qui, au lieu de nationaliser la nature, nous permet d'en comparer les formes au mépris des frontières dressées par les Hommes. Elle nous permet d'envisager de reprendre le contrôle sur notre intime et de nos corps. Elle me permet de comprendre l'existant, d'émettre de hypothèse, de définir un modèle de menace, de tester des stratégies (effacement, flood, injection de bruit blanc, recours juridique...) et d'en espèrer des résultat pour qui aurait besoin de ne plus être esclave emprisonné⋅e sur une île ou dans les cales d'un bateau. 

Parfois, je profite d'une île qui me semble plus accueillante que d'autres pour y faire un feu et inviter _les autres_ à l'appréhension des mondes, à l'orientation de nos polarisations, à l'embrasement de nos regards.

<iframe src="https://mamot.fr/@XavCC/101946606355618201/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400"></iframe><script src="https://mamot.fr/embed.js" async="async"></script>

La plupart du temps, dans nos séries d'îles nous sommes très éloignées des règles de contrôle panoptique et cartographie géopolitique actuelle. On nous ignore parfois, on nous relègue dans des marges ; nous sommes alors parqué⋅e⋅s dans une petite case dotée d'une échelle de mesure propre, sans que l'on sache réellement où l'on nous situe. Nous et nos îles devenons des notes de bas de page des continents, pas vraiment indispensables, et pourtant infiniement plus intéressantes que l'imposante masse  du continent. Nos dominants succombent eux aussi à la fascination des lieux retirés, pour nous y enfermer. Sur le globe terrestre qui n'a pas d'extrémité, tout point peu devenir le centre, nombril d'un monde de contrôle et de surveillance. 

Nous recherchons, à biais bien empli d'espoir, les îles perdues. Celles que l'on pense vierges, car des _gens_ disent qu'il n'y a _rien_. Et ce rien est justement ce qui nous paraît magnifique. Nous voudrions y établir notre prétoire<sup>[Wikipédia](https://fr.wikipedia.org/wiki/Pr%C3%A9toire)</sup> (prætorium) du rien, devenir notre propre chef(-lieu) du rien dans l'espoir de protéger nos corps.

> _« C'est ce(ux), celle(s), qui bouge(nt) ensemble et qui produise(nt) des effets qui forme(nt) un corps »_ Yves Citton

## Du corps perdu au Tombeau ouvert

Nous nous égarons jamais autant que lorsque nous cherchons l'inconnu ou le rien. Tout du moins ceci est valable pour moi. J'erre parfois jusqu'aux îles « λ-calcul » et « λ-calcul binaire »[^5], à temps perdu dans la folie de (re)conquérir des espaces. Merci aux traces de grandes anciennes et grands anciens de nous aider à retrouver où accoster. Je n'ai jamais autant relu qu'aujourd'hui les manuels de navigation comme le [Biopunk manifesto](https://vimeo.com/18201825) de Meredith Patterson et le [Manifeste de Xénoféminisme](http://laboriacuboniks.net/fr/).

Je me jette au corps perdu dans l'aventure, voguant après la reconquête de nos corps. Je tiens là une forme paradoxale de l'aliénation, d'autant plus appuyée par une recherche-action embarquée dans un _Dehors_, courant pas _mainstream_ qui n'a pas vraiment de gyre de monnaie. [Les poches](https://liberapay.com/Xav.CC/donate) poches sont souvent vides. Qui paie pour aventures farfelues ? Personne...

> _« Comme les terriers, les tiroirs, les armoires et les coins Gaston , les poches sont des microcosmes — des querencias — où viennent s’échouer les débris du monde »_ Marc JahJah, [Objet technique, empreinte et divination : le téléphone dans une poche](https://www.marcjahjah.net/3073-objet-technique-empreinte-revelation-et-divination-le-telephone-dans-une-poche)

Qu'importe, les libertés n'ont pas de prix. 

Ça va vite, très vite, trop vite, du côté des scélérats des océans qui dévore nos vies. Malgré des efforts pour garder les gens en vie, nous voyons parfois les fantômes de celles et ceux qui ont été aspiré⋅e⋅s. Je tente parfois de me convaincre de tout arrêter et de vivre reculé, de me faire renvoyer à une insulation isolante, mais je ne parviens pas à quitter la bataille. Je souffre plus de voir des vies privatisées et contrôlées que de mes propres introspection. Cela provient de mon corps, mes tripes. J'ai plus besoin d'autrui que je n'ai besoin de moi-même.

<iframe src="https://mamot.fr/@XavCC/101935314105711996/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400"></iframe><script src="https://mamot.fr/embed.js" async="async"></script>

Et je suis la sédimentation des impressions que j'ai reçues et transmises depuis que je suis né[^6]. Nous sommes des surfaces sensibles et impressionables, comme les ordinateurs, surfaces sensibles, molles ou dures, qui eux n'ont pas d'affects. La promesse et la beauté du « don à coût marginalement nul », d'une partie de soi, ne tient pas la mer. Une donnée brute est un oxymore, elle est forcément prise quelque part pour après être données - offertes. De l'importance de poser la question du quand, pourquoi et comment de la « prise », la question de nos traces.

C'est seulement l'approche individualiste computationnelle qui _dit_ ce coût marginal nul.  Alors qu'un réseau qui permet ces données et dons, réseau qui est corps, il a un coup, écologique par exemple. »


> _« Internet est à la fois de l'information et un corps »_ Yves Citton

Un corps laisse des traces, un corps capte des traces. L'information est différente de la matière, cette information dont l'existence dans la matière est liée à sa circulation, À la différence du bien rival.

> _« Il s'agit d'une dynamique de l'info »_

De l'analyse de nos corps biologiques, cellules, atomes, comme de l'analyse du numérique, par la donnée, on ne fait que traduire, par prisme, par discrétisation, nous n'avons pas accès au réel mais à une interprétation du réel. Le réel ne se donne pas à voir dans les données (Pour Diderot et Spinoza, les humains sont des « machines » biologiques). 

> _« Se pose la question du taux d'échantillonnage.  Notamment dans les entre-impressions »_

La question du taux d'échantillonnage peut réduire, une opération de réduction d'une complexité d'un réel. Ce que fait le numérique. Les données sont des prises par quelqu'un en fonction d'une certaine pertinence prédéfinie et préconçue par ce quelqu'un. S'ajoute des dispositifs techniques qui influent sur ces filtres (Lire « L'inséparé » de Quessada). Nous sommes dépossédé·e·s de nos désirs ? De nos individualités ? « Les data » du numérique ont toutes été _prises_, par vous, moi, une multinationale, des états. Principe de la captation. Ceci étant effectué par des choix. Donc vous prenez une certaine procession de mon corps, des impressions. La plupart des images qui se retrouvent sur internet proviennent de nous, nos traces proviennent de nous, nos données proveinnet de nous et de nos frotements. Où est alors le capitalisme là maintenant ? Qui lui donne vie et corps ? C'est nous ( McKenzie Wark parle de « classe (sociale) vectorielle » et s'interroge : tou·tes·s hackers ?). De la lutte des classes mais qui ne possède même plus leurs propres corps et traces ? La classe vectorialistes est aussi celle qui se branche sur cette circulation de l'information, google par exemple, pour Wark. Cela peut être du poison ou chose merveilleuse, un pharmakon. Qu'est cette chose qui vient se brancher sur de l'information binarisée, composée de 0 et 1 ? (Par discretisation). La chaîne d'interprétation sur des corps entre-impressionnés devient alors complexe.

> _« Opération consistant à remplacer des relations portant sur des fonctions continues, dérivables, etc., par un nombre fini de relations algébriques portant sur les valeurs prises par ces fonctions en un nombre fini de points de leur ensemble de définition. »_ Larousse

La puissance toute nouvelle dans l'histoire de l'humanité, par exemple que possède une agence de surveillance, provient de l'accès aux vecteurs d'Internet. Il y a quelque chose _de capter ce qui à déjà été capté_, puis filtrer depuis n'importe où, et par la Vertu de l'information pouvoir en tirer une puissance cognitive inédite.

Pour Y. Citton, on peut dire « l'ADN c'est de l'information, c'est du code, c'est de langue... (...) » (Voir ubuweb et conf Kenneth Goldsmith - jeu numérique poésie et ADN - « programmer de l'ADN ») qui recombine des molécules de corps vivant. l'ADN est une forme de langage complètement incarné qui se comporte différemment de la langue française, par exemple.

> _« C'est ce(ux), celle(s), qui bouge(nt) ensemble et qui produise(nt) des effets qui forme(nt) un corps »_ Yves Citton

Notre corps, nos traces, sont politiques. Nous mettre en mouvement est politique. Ce qui nous tue c'est le silence, l'invisibilisation, la mise en réclusion et l'isolement[^7].


<iframe src="https://mamot.fr/@XavCC/101952478141257834/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400"></iframe><script src="https://mamot.fr/embed.js" async="async"></script>

Il est enjeu de (re)conquérir ces îles politiques que sont nos vies, nos corps,  de celles et ceux que l'on aime. Ça tangue pas mal en ce moment, ça bastonne. J'ai parfois le mal de mer dans ces traversées. Peut-être que les amers s'adoucissent et les abers sont un peu moins tristes à plusieurs. Ce qui est loin des autres peut parfois sembler délicieux, ce qui est vécu avec les autres est de notre humanité, de notre vitalité. 


## Notes et références

[^1]: Pour approfondir ce qu'est l'insulation, je renvoie à mes notes et la vidéo de conférence d'Alain Damasio sur « Être ou ne pas être... Artificiel à Rennes » <https://xavcc.frama.io/damasio-rennes>

[^2]:  Instigatrice des premiers mouvements punk et LGTB (sub)culturels de l'ex-Yougoslavie, Marina Gržinić parle d'amnésie, d'aphasie et de crise, de biopouvoir et de nécropolitique, de frontières et de volumes, de corpus et de cadavres, sur les paysages mortuaires, et d'hologrammes <https://notecc.frama.wiki/norae:hsociety:homo_note-3-p>

[^3]: Passage emprunté à Yves Citton lors de son intervention à l'émission Mouton Numérique <https://www.franceculture.fr/conferences/mouton-numerique/le-corps-a-lere-numerique>, Le corps, machine à recevoir, traiter et produire des traces ? À partir d'une telle définition, le parallèle semble évident entre corps et ordinateur.

[^4]: Dans ce paragraphe j'emprunte sans honte et remixe avec plaisir beaucoup de mots et phrases à Judith Schalansky et Olivier De Kersauson dans « Atlas des îles abandonnées », édition Arthaud, Flammarion − 2009

[^5]: je prépare un atelier de découverte de cela, je ne sais ni quand, ni où pour l'instant <https://notecc.frama.wiki/norae:si:dev_note-form_3>

[^6]: Je commence ici une partie en reprise de mes notes <https://mamot.fr/web/statuses/101930662163081812>, sur l'intervention de Yves Citton Le corps à l'ère numérique. Le corps, machine à recevoir, traiter et produire des traces ? À partir d'une telle définition, le parallèle semble évident entre corps et ordinateur. <https://www.franceculture.fr/conferences/mouton-numerique/le-corps-a-lere-numerique>

[^7]: Epistole, L'idiot du Village Google <https://xavcc.frama.io/epistole/2018/03/20/google>
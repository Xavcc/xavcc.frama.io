---
title: "Lecture Notes : Refugees in the 21st century -- A world of refugees and 2 questions about camps"
layout: post
date: 2024-04-11 16:30
image: /assets/images/Réfugiés_serbes_près_des_tentes.JPEG
headerImage: true
tag:
- hsociety
- Notes
- Anthropologie
category: blog
author: XavierCoadic
description: It is not with the instruments of those who live in the opulence of privilege that we can understand and analyse what is happening today.
---

<figcaption class="caption">Serbian refugees near the tents sheltering them -- [press photograph] / Agence Meurisse. 1915. Source gallica.bnf.fr / Bibliothèque nationale de France.  Collection notice: http://catalogue.bnf.fr/ark:/12148/cb40499420x</figcaption>

I'm starting the University of London's "Refugees in the 21st Century" MOOC. The speakers are :
+ Professor David Cantor, the Director of the Refugee Law Initiative at the University of London
+ Dr. Sarah Singer, Senior Lecturer in Refugee Law at the Refugee Law initiative and Program Director of our online master's programme in Refugee Protection and Forced Migration Studies.

I share my annotations and references in a dedicated space in an open Zotero group [here](https://www.zotero.org/groups/4989006/catastrophologie/collections/S33VZF5B).
.

This blog post is the first step in sharing my work. This first post is about week 1, entitled 'A refugee world', and the two questions asked in that week about the camps.


**TOC**

+ [Some numbers](#some-numbers)
+ [Why do some countries prefer to receive refugees in camps?](#why-do-some-countries-prefer-to-receive-refugees-in-camps)
+ [What could be the disadvantages of this approach, both for host communities and for the refugees themselves?](#what-could-be-the-disadvantages-of-this-approach-both-for-host-communities-and-for-the-refugees-themselves)
+ [References & notes](#references--notes)

## Some numbers

_Editor's note: remember that you should always ask at least the following questions: **Where does the data come from Who collects and supplies it? Who finances the collection and supply of this data?**_

Number of people displaced by persecution, suffering and conflict 2012-2022

(*in* “[Conflict and crisis in an interconnected world
Global displacement trends](https://stories.nrc.no/conflict-and-crisis-in-an-interconnected-world/)”)

- 2012 : 42,7 millions
    
- 2013 : 51,3 millions
    
- 2014 : 59,2 millions
    
- 2015 : 65,1 millions
    
- 2016 : 65,5 millions
    
- 2017 : 68,5 millions
    
- 2018 : 70,8 millions
    
- 2019 : 19,5 millions
    
- 2020 : 82,3 millions
    
- 2021 : 89,3 millions
    
- 2022 : 108,4 millions

Most of the people fleeing their country of origin will be welcomed in neighbouring countries.

The 10 most neglected population displacements in the world in 2022

1. Burkina Faso
2. DR Congo
3. Colombia
4. Sudan
5. Venezuela
6. Burundi
7. Mali
8. Cameroon
9. El Salvador
10. Ethiopia

**Overview of the situation**

+ 108.4 million displaced people in total
  + 62.5 million displaced within their own country
  + 45.5 million are refugees in other countries

**Africa in figures**

+ 37.1 million people displaced in total 
  + 28.1 million displaced within their own country
  + 9 million are refugees in other countries

**Middle East and Asia**
+ 41.7 million people displaced in total 
  + 19.9 million displaced within their own country
  + 2.8 are refugees in other countries

**America**
  + 14.5 million people displaced in total 
  + 5.9 million refugees in other countries
  + 8.6 million are refugees in other countries

**Europe** 
+ 4.8 million people displaced in total 
  + 8.5 million displaced within their own country
  + 6.3 million are refugees in other countries

## Questions asked at the end of the week

### Why do some countries prefer to receive refugees in camps?

Not all countries choose the "camp" as a reception system for displaced persons.
The "camp" is a system rooted in history. Yet historians have long neglected to include refugees in their writings of general national, regional or world history (D. Stone (2018)). The camp can be 'improvised', by a group of displaced individuals; or set up and organised by NGOs, institutions and humanitarian action consortia; or by a state and its administrations; or a mix of these 3 design orientations.

The camps therefore have a historical and political charge, like the "_'Gulag Archipelago' of Soviet labour camps" (K. Hewitt, 1983, p. 13), The colonial legacy [...] [In Belize] Later in the 18th century small isolated camps were developed in the interior_ (Jerry A. Hall (1983) p. 145, ibid). As Ben White, Simone Haysom and Eleanor Davey (2013) point out in "For good historical reasons many Syrians approach camp life with trepidation".

The ancient Egyptian Empire is said to have used the camp form for displaced populations 4,000 years ago (W. M. Patton (1903)). The Roman Empire used the camp form for its legions in occupied territories, and the UN uses different camp forms for its blue helmets and for refugees.

Until the 19th century it was possible to move around and cross borders without identity documents. In Norway, the government launched forced settlement programmes for Sámi reindeer herders in the 19th and 20th centuries, and the so-called 'nomads' were forcibly rounded up, placed in reform camps and their children forcibly removed from their families and placed with Norwegian foster families (Engebrigtsen, A. I. (2017)).
France has been an innovator in the field of camps since the end of the nineteenth century, particularly for so-called nomadic and gypsy populations (Hacker (2023)). Today, France has set up a number of camps on its own territory and also in neighbouring countries (Le Cour Grandmaison (2007)). The British Empire and its Commonwealth systematised refugee camps as a military and humanitarian response to so-called 'natural' disasters or disasters caused directly by human activities (Mans & Panayi (2020)).

It was after the First World War that inter-state agencies (the League of Nations (SDN)), with the Norwegian Nansen (1861-1930) as High Commissioner for Russian refugees, and with the increase in the number of "mass" displaced persons, that *concerned* states and agencies went from "setting up camps, or setting them up in a very summary and temporary way for limited periods and limited populations" (where people were "quickly resettled") to "overwhelmed" states and agencies systematising and massively expanding reception camps (Ryfman (2015)).

The "camp" is a political choice with its own dedicated strategies and full administration (sometimes delegated to NGOs) (Atlani-Duault (2009)).

> Camps are not lawless zones; on the contrary, they are legal institutions. These instruments of a particular technique aim to separate nationals from foreigners" <cite>Gilles Lhuillier</cite> (Essai de définition : l'Institution juridique des camps, p. 16).
>
> [...]
>
> these places and zones are interconnected, people are moved there by the authorities, and so form a mesh network, "an abstract and concrete, legal and geographical zone"(<cite>Gilles Lhuillier</cite>, p. 28) *in* Le retour des camps ? Sangatte, Lampedusa, Guantanamo..., <cite>Olivier Le Cour Grandmaison, Gilles Lhuillier and Jérome Valluy</cite>. 2007. ISBN-10: 2-7467-0926-0

The category "refugee⋅es" is connotative and refers to an identity as a foreigner, the refugee having been, by definition, forced to leave their homeland (Foxen (2007), *ibid*). Long-standing ethnographic studies of the urban economic migrations triggered by colonization and modernization in Africa and Latin America (Gluckman (1961); Turner (1957); Padilla (1958)) also note this.

Autonomous populations and host communities" (White, Haysom and Davey (2013)). may also choose to offer forms of hospitality other than camps. However, while "In the decades from 1860 to 1914, the Ottoman Empire provided land, tax relief and agricultural development assistance to resettle millions of Muslim refugees from the Caucasus and Balkans." (ibid) the case of the 1976 earthquake in eastern Turkey, which destroyed most of the region's housing and other shelters. The Çaldıran-Muradiye earthquake on the Iran-Turkey border, following which Turkish government officials worked to relocate survivors⋅es from rural to urban areas in anticipation of harsh winter conditions. In Turkey, this was also a political moment with effective choices linked to industrialization, which activated the growing need for non-agricultural jobs, and therefore for a workforce available in areas designed for this purpose (G. E. B. Morren (1983))[^others].

[^others]: See also URBANISATION, INÉGALITÉS URBAINES ET DÉVELOPPEMENT EN TURQUIE (1950-2000), Maurice CATIN et Abdelhak KAMAL, Région et Développement n° 34-2011; Le passé et le présent des politiques d’urbanisation et de logement en Turquie, Gülçin Erdi (2021)

> **(10) There is an apparent paradox of order and disorder created by the interplay or points (8) and (9) above, corresponding to the difficulty of assessing balance between the real benefits and real cots of escalation from phase to phase.**
>
>> “According to Bateson (1972b. p. 498), it is a paradox that authorities, including planners and analysts, need consciously weigh, and it is, I assert, weighted by ordinary people often to the consternation of such authorities. Thus in the aftermath of the 1976 earthquake in eastern Turkey, which effectively destroyed most shelter in the area, government officials laboured vigorously to relocate survivors from rural to urban areas in anticipation of harsh winter conditions. On their part, many common people resisted this with equal vigor in anticipation of the loss of their livelihood [subsistence] the quality or state of being lively, and the fate of their livestock, which would not be able to survive without human care.”
>
> <cite>Georges E. B Morren</cite>, 1983, *p. 295*

Depending on how individuals seeking help, assistance and refuge are perceived, the treatment "granted" by the institutions in charge varies.

So, whether the choice is made by states or humanitarian agencies, the act of setting up and running a camp for "refugees" is conditioned by:

+ the relationship to the figure of the "other", the "foreigner", the "refugee", etc.
+ The number of displaced persons to be accommodated
+ The financial resources available
+ The historical and political context of displaced populations
+ The historical and political context of the State and humanitarian actors responsible for hosting the displaced population
+ The geographical area available and the state of the land
+ Refugees' acceptance or rejection of camp facilities
+ Daily living and sanitary conditions in a camp
+ The willingness or unwillingness and associated strategy for absorbing and integrating displaced populations

### What could be the disadvantages of this approach, both for host communities and for the refugees themselves?

For thousands of years, people have been gathering together to establish a collective living space. Archaeologists, paleoanthropologists and many others will readily speak of camps. These sites of camp life bringing benefits for individual and group subsistence (García-Diez M, Vaquero M (2015)).

The use of "camp" in its *modern* versions intended for refugees does not provide a tenable solution in the increasing flow of displaced populations. Displaced populations are also used as weapons in power relations between states (Behrouz Boochani (2018)). Nor is the camp a response formulated to help resolve the causes of population displacement.

The "camp", by its very configuration and the fact that it *concentrates*, favors the emergence of epidemics, as in Haiti in 2010, whose point of origin is a camp of peacekeepers (Piarroux (2019)). For another case in point, camps for displaced patients are the scene of colonial perpetuation, as in Guinea during an Ebola pandemic in 2014 (Veronica Gomez-Temesio and Frédéric Le Marcis (2019)). "In some countries, refugee people are excluded from vaccination plans. In their fight against Covid-19" (Amnesty International (2021)). In 2023, a health crisis hits IDP camps as conflict rages in Sudan (UNCHR (2023)).

The functions of the "camp" include separating the encamped population from the rest of the population, as well as sorting individuals within the camp itself. The camp reduces social interaction. And many displaced people are in exile for 20 years.

Overall, the management of displaced populations suffers from a lack of financial resources.
Gap between funds needed and funds pledged (Source: UNHCR/UNRWA/IDMC, in Conflict and crisis in an interconnected world,P. Ireland (2024))

**2012:**
+ pledged: 6 billions
+ needed: 9 billions

**2022**
+ pledged: 29.7 billions
+ needed 51.6 billions

The installation, management and maintenance of a camp are very high costs for governments and institutions with responsibilities. These costs can go hand in hand with a desire to reduce expenditure and minimise investment. "the Canadian protection system exhibits a series of deficiencies, ranging from detention policies and deportation in the case of asylum seekers, down to the integration obstacles and other associated challenges encountered by resettled refugees" (Martani, E., & Helly, D. (2022)). The institutions in charge of running the camps may resort to the strategy of delegating the construction of housing and internal infrastructure, as in the case of Zaatari and Azraq in Jordan in 2014 and 2015 (Doraï (2014, 2015)). After expelling refugees from their administered camp, cohabitation with so-called informal camps can also be agreed, as in 2016 in Lebanon, where 400,000 refugees were concentrated (Bonnet (2014)), and the informal camp of refugees⋅es from Syria in the Bekaa (Marj) (Aubin-Boltansky (2016)). We recall here that the individual, and probably the individuated entity through him, is not solely or strictly conscripted to a prescribed role of task performer.

Life in the camps is often a *horror*, as [The logbook of Moria](https://wearesolomon.com/mag/focus-area/migration/the-logbook-of-moria/) reveals. A horror experienced by employees of the International Organization for Migration ([IOM](https://www.iom.int/countries/greece)) and refugees alike.

Combined with the health conditions described above, the approach to managing displaced populations can become a dehumanising solution in which many fundamental human rights are not respected.

States are also wary of creating camps on their soil (Ben White, Simone Haysom and Eleanor Davey (2013)). They may therefore try to make the camps *invisible* in the architecture and landscape, or set them up with trans-national bilateral agreements in neighbouring countries, as we mentioned above in the case of France previously mentioned (Olivier Le Cour Grandmaison, & al. (2007)).

They can also deploy camps as far away as possible. The [Nauru files](https://web.archive.org/web/20200810013430/http://suzonfuks.net/reading-nauru-files-2017/) are leaked reports from staff (Save the Children teachers, social workers, leisure staff and some may have been written by the security services) of abuse, self-harm and neglect of refugees, including children, who were detained for 7 years or indefinitely by the Australian government. Refugees⋅e⋅s and asylum seekers who attempted to reach Australia by boat from 19 July 2013 were deported by the Australian government to camps on Nauru and Manus Islands (3~4500 km from Australia). In 2020, 12 people died and the mental health of individuals reached alarming and catastrophic levels on these two prison islands.

The camp encloses, detains, keeps away and renders invisible "foreign" populations, making them even *stranger*. The result is a breeding ground for rumours and fears. As Hannah Arendt reminds us in her 1943 political philosophy speech "We Refugees".

> If we are saved we feel humiliated, and if we are helped we feel degraded. We fight like madmen for private existences with individual destinies, since we are afraid of becoming part of that miserable lot of *schnorrers* whom we, many of us former philanthropists, remember only too well.
>
> Just as once we failed to understand that the so-called schnorrer was a symbol of Jewish destiny and not a *shlemihl*, so today we don’t feel entided to Jewish solidarity; we cannot realize that we by ourselves are not so much concerned as the whole Jewish people. Sometimes this lack of comprehension has been strongly supported by our protectors
>
> […] The natives, confronted with such strange beings as we are, become suspicious; from their point of view, as a rule, only a loyalty to our old countries is understandable.

The camp makes it difficult for the host population to see the host population. The grey writing accentuates this phenomenon. Also, in the host population, displaced people can be seen as a problem. "Assistance can cause resentment towards refugees, particularly when the host society is also suffering from deprivation or political tensions" (Ben White, Simone Haysom and Eleanor Davey (2013)). These fears can then be turned into a political tool/weapon.

+ In 1999, the Sangatte emergency humanitarian accommodation and reception centre (CHAUH) was set up. The closure of the centre was ordered by Nicolas Sarkozy, then Minister of the Interior, in 2002. The centre was demolished in December 2021. Between 65,000 and 70,000 people are thought to have been detained there between 1999 and 2002.

+ In the case of Belgium, the concept of a "Neutral Zone" to receive refugees was used. This was done to minimise the political claims and rights of the refugees themselves. A neutral zone under international law is a territory set up during a conflict, on the territory of the belligerents, to shelter non-combatants (civilians, wounded, sick)[^neutre].

[^neutre]: In 2022, on Radio Panik, we could hear staggering accounts of undocumented workers toiling from 8am to 8pm for €25 a day, a "wage" of €2.50 per hour worked. Journal des Sans-Papiers, March 2022, "They are the people living in Belgium without a legal residence permit, the so-called 'sans-papiers'. For years, undocumented migrants have been mobilising on a daily basis to demand their rights. The right to housing, to a decent job, to education, to... 100,000 women, children and men are deprived of these rights in Belgium today, and the government's policy of restriction and rejection is creating new undocumented migrants every day among the thousands of would-be refugees who are (poorly) received in this country". [https://archive.org/details/dkbct-2-offdem](https://archive.org/details/dkbct-2-offdem)

From the point of view of historians[^crise], institutions, States and delegated administrations, are major purveyors of grey writings. With these writings, the Institution stages a fiction of stability(s) through the production of regulations. Actors may have an interest in the "crisis", so there are social uses of the crisis.

In 3 phases:
+ Invoking the crisis creates a sense of urgency.
+ Reforming an institution: reforming within the discursive framework of a crisis.
+ In the narrative of stability: it's a question of self-justifying its own legitimacy through the Institution to put it at the service of a narrative of stability.

This was the case for "Les camps d’internement canadiens durant la Seconde Guerre mondiale : entre mise en ordre et remise en cause "[prison camps](<cite>Lucile Chaput</cite>, *Tempora* Université Rennes 2 (2023), *opt cit*).

[^crise]: Xavier Coadic, 2023, Notes depuis le colloque : À l’épreuve des tempêtes. Institutions et crises : approches historiques <https://xavcc.frama.io/notes-colloque-institutions-crises-recherche-histoire/> ; Les camps d’internement canadiens durant la Seconde Guerre mondiale : entre mise en ordre et remise en cause ; Lucile Chaput, *Tempora (Université Rennes 2*) <https://xavcc.frama.io/notes-colloque-institutions-crises-recherche-histoire/#les-camps-dinternement-canadiens-durant-la-seconde-guerre-mondiale--entre-mise-en-ordre-et-remise-en-cause>

These grey writings do not contain the accounts, testimonies or analyses of the people put into the camps. In the management and administration of the camps, a history is written without the first people affected. The term "Undocumented people" also says this - *Why 'Undocumented' or 'Irregular'*? (UNCHR (2018))[^unchr] -- since it involves denying the existence of valid documentation in a given administrative area for people seeking refuge because their history is documented elsewhere.

Movements of displaced populations are often linked to what can be called a "crisis".
Crisis" is seen as a break(*in the face of supposed continuity*). The crisis is part of a period of conjecture. Migration, refugees and camps can be used to invoke crisis, as in the case of "Fortress Europe" continues to resist new arrivals" (Ireland (2023)).
Drawing on her own experience as a German Jew in America, Arendt challenges us to imagine the world from an immigrant's point of view. "History has imposed on all of us the status of outlaws, pariahs and upstarts". (H. Arendt (1943)).

Before setting up camps and then locking people up in them, it's a *common* idea to stigmatise one or more *categories* of individual. Putting them on the fringes of society through language already signifies their exclusion. "The pariah. A figure of modernity". Which is only an almost marginal thought. (<cite>Eleni Varikas, Martine Leibovici</cite>, *Collectif*, Tumultes, n° 21-22, 2003). Stigmatisation is a punishment and a plank for the worst[^pariat].

[^pariat]: We worked on this issue as a preamble (2022) to Notes de lectures : Les camps et enfermement(s) [https://xavcc.frama.io/notes-lecture-les-camps/#la-figure-du-paria--une-exception-qui-%C3%A9claire-la-r%C3%A8g](https://xavcc.frama.io/notes-lecture-les-camps/#la-figure-du-paria--une-exception-qui-%C3%A9claire-la-r%C3%A8gle)

> "Never has the rhetoric of human rights co-existed so much with the proliferation of men and women deprived of that primary condition that Arendt associates with the exercise of rights: a place in the world that guarantees that our opinions carry weight and our actions have effect".
>
> <cite>Eleni Varikas</cite> (2003), *[La figure du Paria : une exception qui éclaire la règle](https://www.cairn.info/revue-tumultes-2003-2-page-87.htm)*

[^unchr]: <https://www.unhcr.org/cy/wp-content/uploads/sites/41/2018/09/TerminologyLeaflet_EN_PICUM.pdf>

The words spoken by Hannah Arendt more than 80 years ago bring us back to emphases and memories from a political, socially situated and philosophical perspective, particularly in the face of the Nazi programme and the extermination camps. This is still a reminder today that speech and legitimacy are asymmetrical.

Today, analyses and discourses are still mostly offered to people who have not experienced the camps *as* refugees, and only sometimes in the 'expert knowledge' (Chivallon (2025)) of NGOs, journalists, politicians, or more in the grammars and arcana of scientific academies that are de facto not or hardly accessible to refugees and encamped people. This ties in with the questions and problems raised by the "perspective of sustainability sciences", "the status of the human in terms of rights and law" (Toivanen R. & Cambou D. (2021)).

The "camp" is a device that participates in the narratives from a very specific point and with erasures, with the double question of who has the right to the podium and who is rendered mute. This camp device orders and implements the relationship to the 'other', by being a tool of separation and sorting. In several fields of academic and scientific work, 'camps' are studied and detailed as a continuum of colonialism with segregation and racism. We have cited some of these fields with recognised references. In most of these literatures, the voices of displaced and encamped people, labelled refugees, are relegated to the background and are not taken as authoritative figures. Those in the know debate and the refugees suffer.

The "camps" also influence our social, intimate and political relationships.

Once the refugee camps have been dismantled or destroyed, their place in history is consigned to the administrative archives, while that of the refugees and their experiences in the camps is consigned to oblivion.
It is not with the instruments of those who live in the opulence of privilege that we can understand and analyse what is happening today.

> It is sadly nothing new to argue that oppressed and colonised people have been and are subject to epistemic violence – othering, silencing, and selective visibility – in which they are muted or made to appear or speak only within certain perceptual views or registers – terrorists, protestors, murderers, humanitarian subjects – but absented from their most human qualities.  Fabricated disappearance and dehumanisation of Palestinians have supported and continue to sustain their physical elimination and their erasure as a people.
>
> <cite>Salih, Ruba</cite>. *December 2023*. ’Can the Palestinian speak?’. Allegra Lab. https://allegralaboratory.net/can-the-palestinian-speak/

## References & notes

- Amnesty International, Réfugiés : les laissés-pour-compte de la vaccination, Publié le 18.06.2021 <https://www.amnesty.fr/refugies-et-migrants/actualites/refugies--les-laisses-pour-compte-de-la-vaccination>

- ATLANTI-DUAUKT, L. & Vidal, L. (2009). *Anthropologie de l’aide humanitaire et du développement: Des pratiques aux savoirs des savoirs aux pratiques*. Armand Colin. [https://doi.org/10.3917/arco.atlan.2009.02](https://doi.org/10.3917/arco.atlan.2009.02)

- ARENDTH, H. “We Refugees,” Menorah Journal 31, no. 1 (January 1943): pp 69-77.

- AUBIN-BOLTANSKY, E. (2016). Camp informel de réfugiés syriens.. Photography. Lebanon. [⟨medihal-02023512⟩](https://media.hal.science/medihal-02023512)

- BOOCABI B. (2018), Refugees’ lives have become weapons in a rugged political contest <https://www.theguardian.com/commentisfree/2018/jun/20/our-lives-are-have-become-weapons-in-a-rugged-political-contest>

- BONNET, M. (2014). Réfugiés syriens : d’une crise humanitaire nationale à un défi pour l’humanité. *Les Cahiers de l’Orient*, 116, 67-77. [https://doi.org/10.3917/lcdlo.116.0067](https://doi.org/10.3917/lcdlo.116.0067)

- CHIVALLON, C. (2005) Les enjeux de la qualification des savoirs : l’exemple afro-américain (Qualifying knowledges : the african-american example. January 2005. Bulletin de l Association de géographes français 82(3):343-357 DOI:[10.3406/bagf.2005.2469](http://dx.doi.org/10.3406/bagf.2005.2469)

- DORAÏ, K. (2014) Des enfants syriens de retour de l’école dans le camp de réfugiés de Zaatari, Jordanie. Photography. Camp de Zaatari, Jordan. [⟨medihal-01316405⟩](https://media.hal.science/medihal-01316405)
    
- DORAÏ, K. (2015). Un atelier de menuiserie dans le camp de réfugiés de Zaatari - Jordanie. Photography. Camp de Zaatari, Jordan.  [⟨medihal-01314370⟩](https://media.hal.science/medihal-01314370)
    
- DORAÏ, K (2015) Un habitat auto-construit par un réfugié syrien, Azraq - Jordanie (4). Photography. Azraq, Jordan. [⟨medihal-01508959⟩](https://media.hal.science/medihal-01508959)

- Engebrigtsen, A. I. (2017). Key figure of mobility: The nomad. Social Anthropology/Anthropologie sociale, 25(1), 42-54. Retrieved Apr 8, 2024, from https://doi.org/10.1111/1469-8676.12379

- García-Diez M, Vaquero M (2015) Looking at the Camp: Paleolithic Depiction of a Hunter-Gatherer Campsite. PLoS ONE 10(12): e0143002. https://doi.org/10.1371/journal.pone.0143002

- GLUCKMAN, (1961), “Anthropological problems arising from the african industrial revolution”, in A.W. SOUTHALL, Social Change in Modern Africa, London, Oxford University Press.

- GOMEZ-TEMESIO V. et Le MARCIS F. (2019), « La mise en camp de la Guinée », L’Homme [En ligne], 222 | 2017, mis en ligne le 01 juin 2019, consulté le 05 avril 2024. URL : http://journals.openedition.org/lhomme/30147 ; DOI : https://doi.org/10.4000/lhomme.30147

- HACKER W. (2023) Où sont les « gens du voyage » ? Une histoire actuelle de l’antitsiganisme.

- Hewitt, K. (Ed.). (1983). Interpretations of Calamity: From the Viewpoint of Human Ecology (1st ed.). Routledge. https://doi.org/10.4324/9780429329579

- Le Cour Grandmaison (2007) *Le retour des camps ? Sangatte, Lampedusa, Guantanamo…*, *Olivier Le Cour Grandmaison, Gilles Lhuillier et Jérome Valluy. 2007.* ISBN-10:  2-7467-0926-0

- MARTANI, E., & HELLY, D. (2022). Asylum and resettlement in Canada. Historical development, successes, challenges, and lessons. Zenodo. https://doi.org/10.5281/zenodo.7609641

- MANZ, Stefan, and PANIKOS Panayi, Enemies in the Empire: Civilian Internment in the British Empire during the First World War (Oxford, 2020; online edn, Oxford Academic, 23 Apr. 2020), https://doi.org/10.1093/oso/9780198850151.001.0001, accessed 4 Apr. 2024

- MORREN, E. B *Jr*, *in* Hewitt, K. (Ed.). (1983). Interpretations of Calamity: From the Viewpoint of Human Ecology (1st ed.). Routledge. https://doi.org/10.4324/9780429329579

- PADILLA E. (1958), Up From Puerto Rico. New York, Columbia University Press.

- PATTON, Walter Melville, (1903), Ancient Egypt And Syria. Bibliotheca Sacra 1903-01: Vol 60 Iss 237

- PIARROUX P. (2019) Choléra. Haïti 2010-2018. Histoire d’un désastre. // CNRS éditions,

- RYFMAN Philippe, « L’héritage des camps de la Retirada au XXIe siècle. L’impasse de la réponse par le camp à l’existence du « réfugié » », Exils et migrations ibériques aux XXe et XXIe siècles, 2015/1 (N° 7), p. 256-267. URL : https://www.cairn.info/revue-exils-et-migrations-iberiques-2015-1-page-256.htm 

- TURNER V., (1957), Schism and Continuity in an African Society, Manchester, Manchester University Press for Rhodes-Livingstone Institute.

- STONE, D. (2018). Refugees then and now: memory, history and politics in the long twentieth century: an introduction. Patterns of Prejudice, 52(2–3), 101–106. https://doi.org/10.1080/0031322X.2018.1433004

- TOIVANEN R. & CAMBOU D. (2021). Human Rights. In: Krieg C. & Toivanen R (eds.), *Situating Sustainability*. Helsinki: Helsinki University Press. DOI: [https://doi.org/10.33134/HUP-14-4](https://doi.org/10.33134/HUP-14-4)

- UNCHR, 2023, Une crise sanitaire frappe les camps de déplacés alors que le conflit fait rage au Soudan <https://www.unhcr.org/fr/actualites/articles-et-reportages/une-crise-sanitaire-frappe-les-camps-de-deplaces-alors-que-le>

- VARIKAS, E. (2003). La figure du Paria : une exception qui éclaire la règle. *Tumultes*, 21-22, 87-105. <https://doi.org/10.3917/tumu.021.0087>

- WHITE B, HAYSOM S. and DAVEY E. (2013) Refugees, host states and displacement in the Middle East: an enduring challenge.

### Notes



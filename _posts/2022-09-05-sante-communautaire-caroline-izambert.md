---
title: "Santé Communautaire : discussion avec Caroline Izambert"
layout: post
date: 2022-09-05 19:40
image: /assets/images/moss_biolab.jpg
headerImage: true
tag:
- hsociety
category: blog
author: XavierCoadic
description: "Qu'est-ce que la santé communautaire"
---

*Discussion du 7 juillet 2022 via une vidéoconférence BigBlueButton, retranscrite et publiée avec l'aimable autorisation de Caroline Izambert.*

**Question** :

<figitw>
Bonjour Caroline Izambert,<br>

merci de m'accorder de votre temps pour discuter de santé communautaire.<br>
Pour voir depuis où vous parlez, je vais tenter de récapituler votre curriculum.<br>
Vous êtes docteure en histoire.
</figitw>

<figitw>
Vous avez soutenu une thèse en 2018, si je ne dis pas de bêtises, sur le l'accès aux soins des personnes étrangères.
</figitw>

<figitw>
Vous avez été directrice du plaidoyer chez AIDES. Vous avez publié, co-autrice, un livre qui titré « Pandémopolitique : réinventer la santé en commun » en 2020.
</figitw>

<figitw>
Vous avez été enseignante d'histoire-géographie au collège et vous êtes aujourd'hui chargée de mission santé auprès du secrétariat général de la ville de Paris.
</figitw>

**<u>Caroline Izambert</u>** :

Mon activité professionnelle qui, du coup, a remplacé, entre guillemets, la direction du plaidoyer de l'association AIDES, oui. Tout et tout est exact.

**Q** : 
<figitw>
Je voudrais commencer ainsi si ça vous met à l'aise. Si vous n'êtes pas à l'aise avec ça, je passerai à autre chose.<br>
Je pars du principe que raconter son rapport à son objet de recherche et à son champ d'intérêt c'est un peu se raconter soi-même au travers du sujet de recherche.
</figitw>

<figitw>
J'aimerais savoir si vous pourriez me parler un peu de comment vous en êtes arrivée à vous intéresser à la santé communautaire.
</figitw>

**<u>Caroline Izambert</u>** : 

Alors, vraiment mon métier de base et de cœur est enseignante.
J'ai été enseignante une toute petite dizaine d'années. En Seine-Saint-Denis après un parcours d'études d'histoire très classique, cela a toujours été doublé d'un engagement militant plutôt à l'extrême gauche. On va dire de mes quinze ans à vingt-deux, vingt-trois ans.

Cela dit, ça s'est un peu effiloché en cours de route, cet engagement militant. Alors j'étais un peu jeune à ce moment-là qui était quand même dans la suite de des grandes grèves de 1995.
Voilà, j'étais plutôt du côté des parties, voir des micro mouvements, Trotskistes.Comme dans « Sacré Graal » des  Monty Python. Donc des « sous sous groupe » avec des débats qui étaient primordiaux à l'époque et qui paraissent rétrospectivement un peu dérisoire. Sur la nature profonde de l’URSS, état ouvrier dégénéré ou nation proto-capitisme. Mais ça  été aussi  une une école, notamment en termes de de lecture et d’acquisition d’une forme de de culture générale sur le mouvement ouvrier. C’était quand même assez, assez génial.

Alors même que je n'avais pas une famille très engagée, en tout cas pas d'un point de vue militant et encore moins à gauche.
Et puis c'est vrai que je ne m'y retrouvais pas quand ce même militantisme révolutionnaire que je trouvais parfois un peu en déconnexion avec le monde. Tel qu'il était ou devenait un peu patenté, et plus égoïstement les sacrifices demandés par le militantisme, c'est-à-dire peu d'ambition professionnelle, un dévouement H vingt-quatre. Un investissement minimum dans son métier pour réserver tout le reste à la militance ne me convenait pas des masses.
Donc, je me suis éloigné des pratiques militantes pendant deux, trois ans.

Puis quand j'ai eu fini de passer les concours de l'éducation nationale, je cherchais une autre forme d'engagement et, pour des raisons que je n'identifie pas très bien, je ne voulais plus retourner dans un parti politique, je voulais retourner dans les associations. 
L'association paraissait une bonne force, mais j'y voyais une forme de, de de dépolitisation. Je ne sais pas vraiment bien pourquoi, et puis je me voyais pas du tout faire de la maraude, faire du de la distribution alimentaire. Il y avait, j'ai changé depuis, mais il y avait toute une dimension caritative dans laquelle je ne me reconnaissais pas.

Et il se trouve qu'Act-Up, dans une expérience du VIH, pas la première personne, était à la fois une association avec une action extrêmement concrète et une forme de politisation.
Donc en 2006 je suis rentrée à Act-up, ce qui a inauguré une dizaine d'années d'engagement bénévole dans la lutte contre le VIH.
Avec ce truc intéressant, c'est que Act-up était l'organisation avec manière la plus radicale, mais qui, en soi, n'était pas un lieu de promotion de la santé communautaire du tout.
Enfin du tout, c'est pas qu'ils sont contre ou qu'ils sont pour, mais en tout cas ce n'est pas un axe structurant de ce qui intéresse Act-up.

Donc, c'est pas vraiment là que j'ai rencontré la, la santé communautaire. J'avais travaillé à la fac sur les questions de Guerre d'Algérie, rapport entre notamment des Algériens, de la relation entre ceux qui n'étaient pas encore des immigrés, mais des Algériens en métropole pendant la Guerre d'Algérie, et leurs relations avec le parti communiste, y compris dans les grandes citadelles ouvrières, type Usine Renault.

C'est un travail qui ne va pas aller plus loin qu'un DEA [diplôme d'études approfondies] qui m'a permis d'acquérir une sorte de culture générale sur les questions d'immigration, et alors même que je venais à Act-up, un peu plus motivée par les questions d'accès aux médicaments, propriété intellectuelle, mais dont j'ai découvert assez rapidement que c'était une forme d'élite. 
Alors, rétrospectivement, je me dis, je me suis peut-être auto-censurée. Enfin, c'est pas grave, ça s'est très bien fini. Ceux qui travaillaient sur l'accès aux médicaments, je comprenais pas ce qu'ils disaient, ils étaient plus beaux, plus forts, plus grands ; ils avaient des contacts à travers le monde, discutaient avec des génériqueurs indiens et je pensais qu'ils n'avaient pas une seule minute à me consacrer.
En revanche, sur les questions d'accès au titre de séjour pour soins et d'accès aux soins pour les personnes étrangères, là il avait un peu plus de place et ma petite expérience, mon petit intérêt pour ces questions d'immigration, j'étais, à l'époque, très liée avec des copains qui vivaient entre l'Algérie et la France.

C'était l'utilisation des problématiques qui me touchaient. Je le reconnais, ça a vite pris.
Avec quand même cette question qui est structurante dans la lutte contre le VIH, en France, d'une très rare, et encore plus à l'époque, parole à la première personne sur le VIH pour les hommes gay. 
Alors, pas tous les hommes gays vivant avec le VIH, mais malgré tout et alors même que, déjà à l'époque, les personnes nées sub-sahariennes représentaient un peu plus de la moitié des nouvelles découvertes de séropositivité.
Avec toutes les questions qu'on peut se poser sur ces catégories épidémiologiques où on mélange une orientation sexuelle avec, avec une origine. Évidemment, il y a des gens qui qui sont à la rencontre des deux.
Et avec des gens qui, pour le coup ça n'a pas beaucoup changé, n'avaient pas du tout le même accès à la parole.
Et puis c'est moment où il y a eu des publications très important à mes yeux, en tout cas moi dans mon parcours, très importante comme l'Histoire sociale de l'héroïne, dirigé par Anne Coppel, Michel Kokoreff et Michel Peraldi.

Un travail qui exhumait toute cette histoire du VIH, des usagers de drogues, mais aussi des quartiers populaires immigrés des années quatre-vingts, quatre-vingt-dix.

Expérience qui quand même résonnait, pas de façon quotidienne, pas de façon constante, mais beaucoup avec mon expérience d'enseignante. 
En tout cas, plusieurs fois je me suis rendu compte, c'est des quartiers dans lequel il y a beaucoup de de turn-over parce que les gens cherchent à en partir, parce que les gens ont des vies très compliquées, du coup sont amenés à se déplacer souvent.
Parfois, le quartier n'est qu'une étape dans un trajet de migration et mène à un trajet social avec plusieurs étapes.

Je voyais bien que lorsque l'on creuse un peu on s'apercevait pour les familles qui étaient anciennement implantées dans ces quartiers, que cette histoire du VIH dans les années quatre-vingts, quatre-vingt-dix, c'était une histoire tue, mais était quand même une bonne clé de compréhension. 
Alors aussi, c'est vraiment de la sociologie de bazar. Je pense que c'est des questions, soit pas explorées, soit mal explorées. 
Il y avait des questions sur le rapport aux pratiques religieuses. Moi je suis me souvient de gens qui disaient qu'ils s'étaient retourné⋅e⋅s vers des formes de piété suite à des décès liées au HIV, avec des histoires tragiques de deuil empêché, de gens qui n'ont a pu rapatrier les corps. 
Enfin, en tout cas, c'était une histoire très présente et qui restait très tue. D'ailleurs, le boulot n'est toujours pas fait et je ne sais pas s'il sera fait un jour.

Ça m'a beaucoup marqué, cela m'a conduit à m'inscrire en thèse et donc à travailler. Alors je savais que je voulais pas travailler, on va dire, sur les communautés elles-mêmes.
Je me sentais pas légitime. À part un anglais complètement utilitaire, je ne parle pas d'autres langues que le français, ce qui ne paraissait pas possible pour un travail pointu sur l'histoire d'un certain nombre de communautés ou de groupes en France.
Donc, je me suis dirigée vers ce qu'était plus conforme à ma formation historique. C'était une histoire des politiques publiques de l'accès aux soins des étrangers.
Là où c'était aussi un type d'histoire aveugle cette question de la parole des gens eux-mêmes et de l'auto-organisation. C'était quand même très, très absent et c'est quand même une histoire très institutionnelle, avec une forme, je ne dirais pas confiscation, mais c'est vrai que l'on a une mobilisation autour de la double peine. 
On a une mobilisation avec des militant⋅e⋅s qui sont passés par la marche pour l'égalité, en quatre-vingt-trois, qui vont se réinvestir dans la lutte contre la double peine, qui, souvent, une lutte en fait dans ces années-là, fin quatre-vingt-cinq, début quatre-vingt-dix, une lutte contre l'expulsion de personnes qui ont été condamnées pour usage ou trafic de drogue.

Plutôt de l'héroïne, plutôt par voie intraveineuse, et qui soit se contaminant prison, soit ont été contaminés avant de rentrer en prison et pour qui l'expulsion à la sortie de la prison signifie le décès au pays. Donc il y a un lien qu'on voit, que l'on modélise peut-être un peu moins aujourd'hui, mais entre ce combat contre la double peine et la lutte contre le VIH. 

Et puis ces acteurs lessivés, parce que confiscation, ça donne l'impression d'organisation type médecins sans frontières, médecins du monde, qui se seraient appropriées cette cause parce qu'elle permettrait de donner des revenus, accès à des ressources symboliques.

En tout cas, moi, je suis peut-être trop indulgente avec ces acteurs, enfin peu importe, mais c'est que ce que j'ai vu dans la thèse, c'est que c'est un processus, plus bêtement et tout aussi violemment, social d'acteurs qui n'ont pas beaucoup de ressources du côté des mouvements l'immigration, qui sont lessivés, en fait par la séquence qui s'ouvre de quatre-vingt-trois, d'une certaine façon, qui va peut-être se finir, avec « St Bernard » [23 août 1996, évacuation de sans-papiers de l'église Saint-Bernard, ndlr] qui marque un peu le coup symbolique et les [grèves](https://fr.wikipedia.org/wiki/Gr%C3%A8ves_de_1995_en_France) de la faim quatre-vingt-quinze, quatre-vingt-seize, une forme de repolitisation des luttes de l'immigration. 
Mais en fait, c'est pas tout à fait les gens des quartiers populaires qui sont là, plutôt des gens récemment arrivés, ce qui marque un moment où l'on sort quand même de ce qu'est un sacré trou noir. 
Et ces gens ont peu de ressources, sont fatigués, sont touchés par le VIH dans les quartiers, donc, en fait, sont pris par une forme d'urgence ; et à côté, vous avez des organisations. 
Alors je ne dis pas qu'elles vont bien, mais qu'elles ont des ressources extrêmement fortes.
Et ça m'a posé la question communautaire et de l'auto-organisation.

Puis, en 2016, l'association AIDES.

Donc association de lutte contre le VIH, c'est assez intéressant parce que cette association, on va dire beaucoup plus institutionnelle que Act-up, association avec soixante-quinze lieux partout en France. 
Des gros quarante cinq millions de budget, la moitié en financement d'État.
Une asso plus classique, mais dont j'ai découvert, et ça a vraiment été non pas une découverte mais ça a été une réponse là où je m'attendais pas à la trouver.
Sur la revendication de la démarche communautaire en santé, avec l'une des plus grandes réussites de la santé communautaire, en tout cas d'une d'une forme de plaidoyer communautaire pour la santé, qui était la question de la PrEp (prophylaxie pré-exposition).
Où, là aussi, ça serait très long, mais où vous aviez eu des débats très violents entre AIDES et Act-Up. 

Il faut voir que AIDES avait compris un truc, je pense, sur la sexualité gay, mais aussi, c'est pas un hasard, par une pratique de la paire aidance, par une pratique du groupe de parole qui n'est pas du tout Act-Upienne.
Et puis Act-up était une association très intello, qui n'a jamais voulu être très grassroot du tout.
AIDES avait compris, dans les années quatre-vingt-dix et puis deux mille, que l'on pouvait pas prendre les options d'Act-Up, qui était d'une une sorte de politisation des débats autour du bareback, c'était une voie de garage parce qu'en fait il se passait quelque chose dans la communauté gay ou autour du préservatif, de la fatigue, de la prévention, de l'expérience pour les gens qui avaient survécu de vingt, de dix, vingt, trente ans d'épidémie.
Des gens qui pensaient et de gens qui disaient : « mais en fait là, moi j'en peux plus ». 
Ce seul horizon d'une d'une sexualité avec capote : « J'en peux plus ! ». Et de toute façon, j'ai l'impression que y a une forme d'inéluctable de la contamination qui fait que « punto, basta ! ».
Et je pense que AIDES, par son organisation, par son organisation communautaire qui était, je le dis franchement, parfois regardée avec un peu de « Le leur apéro bidule », mais leur avait permis de voir quelque chose et de voir que, de l'autre côté, on avait ces résultats sur les propriétés prophylactiques des antiviraux pris par des personnes séronégatives.

Ils ont, AIDES, réussi à faire ce qui est très rare, pour essayer d'en faire l'expérimentation dans d'autres domaines très difficilement répliquable, parce que là, on a les « ingrédients magiques » : on a une communauté où tout le monde n'a pas d'énormes ressources financières, culturelles, mais en fait, il y a des gens à l'intérieur qui ont des ressources financières et culturelles.

Vous aviez des organisations déjà constituées, connu des pouvoirs publics. Vous aviez une communauté scientifique et de soignants, y compris avec des pontes de l'APHP qui étant partants.
Donc ensuite je ne dis pas que cela a été simple et que AIDES s'en est pris vraiment plein la tronche quand il a dit : ben nous, on va lancer, en partenariat avec l'agence nationale de recherche sur le VIH, on va lancer un essai clinique communautaire.
Ça pas été un chemin tout rose, mais ils ont réussi.

C'est un exemple pas du tout de la santé communautaire telle que peut la raconter Alondra Nelson sur les Black Panthers, etc. 
Mais quand même cette idée de parler des besoins des personnes, de coconstruire, d'exiger des pouvoirs publics d'être parties prenantes sur les études, et d'organiser, même si ça a pu s'effilocher par la suite, une délivrance communautaire avec du counselling communautaire. 
Y avait quelque chose qui était quand même, très, très fascinant, et qui, en France, était très propre à la communauté gay. 

Je le vois aujourd'hui sur la variole du singe, et quand je dis communauté gay pour le coup c'est pas LGBTQI, je dirais même c'est même pas HSH c'est vraiment gay.
Il y a une sorte de savoir-faire, là où toute la campagne qui a eu ces deux dernières semaines pour exiger le vaccin et la transparence des stocks pour le vaccin anti variolique MonkeyPox, il y a quelque chose qui est très fort ici qui pour moi relève de la santé communautaire, pas la plus contestataire pas la plus « contre système », mais quand même très très efficace.
On a quand même des petits mystères sur, par exemple, pourquoi tout ça est beaucoup moins présent du côté santé reproductive des femmes.
Alors il y a un super article de Bibia Pavard et Maud Gelly qui fait la comparaison entre IVG et dépistage du VIH, le dépistage communautaire.

Par exemple le mouvement des femmes depuis les années soixante-dix a pas réussi en France à complètement garder, en fait, cette empreinte communautaire.
Le Planning familial n'est pas si pas tout à fait un opérateur de l'état, ce n'est pas le « France terre d'asile de l'IVG ».
C'est une association qui reste un ancrage, mais on n'est pas tout à fait dans la santé dans la santé communautaire.

Je travaille aujourd'hui aussi sur ces sujets-là, ça continue à me troubler parce que, pour le coup, des femmes avec des ressources sociales il y en a, mais on n'a pas réussi à garder ici quelque chose.
Ça pose des soucis sur les questions de contraception aujourd'hui, on a quasiment des controverses générationnelles hyper violentes, hyper conflictuelles, avec une jeune génération qui est vraiment pas emballée par la prise d'hormones et une génération plus ancienne qui trouve que l'on est pas bien reconnaissante du travail qui a été fait et on oublie ce qu'a été cette avancée.
En tout cas il n'y a pas une méthodologie communautaire et on le voit sur les questions de santé des femmes.

Ça donne une assez grande fragilité.
Là, j'imagine que l'ange répond à d'autres questions.

En fait, à partir d'une santé communautaire à laquelle j'ai pas pris part, je ne suis pas un homme gay. J'ai eu la chance quand même au sein de AIDES de la voir en action, de la voir, même si j'ai un tout petit peu moins travaillé dessus, la question des usagers de drogues.
Alors là on est peut-être moins dans des modèles un peu « parfait » mais quand même il y a cette culture, notamment du soutien entre pairs, du partage de connaissances.

Il y a un programme qui s'appelle AERLI, d'aide à l'injection pour limiter les risques de contamination au virus de l’hépatite C (VHC), puis aussi avec ce même effet de communautés transnationales.
J'ai des souvenirs de conférences de lutte contre le VIH avec les usagers de drogues Afghans, Écossais, Québécois, Français qui avaient fait des préoccupations hyper proches et qui avaient un dialogue, par-delà les frontières, éminemment communautaire, chacun depuis leur place.

Pour moi, c'est peut-être là l'aspect qui m'intéresse le plus, maintenant je suis dans le public dans une institution, c'est la façon dont justement ces organisations de santé communautaire réussissent à installer ou non une forme de tension avec les pourvoirs publics.
Pourvoirs publics qui y voient bien que la santé communautaire, parce qu'elle expérimente de délégation de tâches, par ce qu'elle fait émerger comme métier, alors après on peut avoir des débats sémantiques est-ce que c'est issu de la santé communautaire, moi j'en ai plutôt une définition large, mais des formes de médiation en santé quand vous assurez une partie des tâches de votre centre de santé par un médiateur de santé qui va être payé, allez, le SMIC plus 20%, ça vaut pour beaucoup moins cher que de faire assurer ses tâches par un médecin, voir un généraliste.

Et cela se voit bien que du côté des pouvoirs publics. Je pense que c'est un très grand risque pour les démarches communautaires. C'est que en fait, à la fois, elles peuvent avoir une forme contestataire, et à la fois, elles trouvent des solutions qui peuvent être tout à fait convergentes avec des objectifs de réduction des budgets de la santé.
Donc la santé communautaire sans la politisation ça comporte des risques, là je ne vous parle pas de risque théorique. On voit bien cette tendance là.
Et d'un et d'un autre côté le seul horizon de la santé communautaire façon street medic en manif il y a un plafond de ce qu'on peut faire quand même hyper grand quand on a on refuse, pour des raisons qui peuvent être parfaitement légitimes, la relation aux institutions

Il y a un autre gros sujet, c'est le rapport de la santé communautaire à la science. Je m'intéresse, et je peux dire par sensibilité, je me suis plutôt intéressée à des formes de santé communautaire qui restent en fait, je ne vais pas trouver de très bonne expression, qui restent dans un dialogue avec le savoir scientifique et qui lui accorde une validité.

Par un regard pas du tout professionnel et encore moins de recherche, je vois qu'en France il y a des communautés qui s'organisent et qui prennent en charge des questions de santé dans une rupture, en fait, totale avec les institutions de santé, avec les savoirs scientifiques académiques
Ce qui pose des questions extrêmement fortes.

Et puis j'arrêterai là, peut-être pour moi le dernier jalon dans la santé communautaire ça a été la crise du covid qui a été un échec d'essayer de vendre le système via l'approche communautaire aux pouvoirs publics.
Ça a été un moment d'expérience très différenciée aussi. Je mets ça sous le sous le chapeau santé communautaire, par exemple, les malades du rein sont très organisé⋅e⋅s en ligne depuis très longtemps, notamment autour d'une association qui s'appelle Renaloo, avec une pratique de forums, mais qui va beaucoup plus loin, et c'est déjà très bien, avec l'échange entre pairs qui est aussi dans un milieu de la néphrologie où il n'y a pas la qualité dialogue qu'on peut avoir dans le VIH.
Avec les infectiologues où vous avez des grosses controverses, mais qui n'ont rien à avoir avec celles comprises extrêmement violentes avec la communauté des néphrologues, sur la sélection sociale de l'entrée de l'accès à la greffe.

Vous avez un très bon papier de christian Baudelot sur la forme de sélection, de discrimination sociale dans l'accès à la greffe, sûr l'organisation de la dialyse.
C'est beaucoup autour de la question de la greffe. Je suis pas assez versée dans la question, en tout cas les associations et celle-là en particulier (Renaloo) dit, en gros, les néphrologues organisent le maintien dans la dialyse d'un certain nombre de patients qui seraient éligibles à la greffe, parce que la dialyse est super rentable.
Ce qui est intéressant c'est qu'ils sont arrivés à un niveau démonstration qui est quand même assez sérieux avec des données et avec une revendication est très forte.

Ça a été assez tragique de voir ces associations là. Chacun a développé des formes d'action communautaire très différentes, donc eux ils ont été dans compter leurs morts parce que les malades du rein, notamment ceux sous traitement immunosuppresseur, ceux en attente de greffe, ont payé un très lourd tribut dans toutes les vagues du covid jusqu'à jusqu'à aujourd'hui.
Donc ils ont une expérience très forte, et puis d'une espèce de plaidoyer, d'une espèce de conversion très Act-Upienne sur la revendication.

Tandis qu'à l'autre extrémité, nous à AIDES, après des mois, des semaines, d'inquiétude sur est-ce que les personnes vivant avec le VIH allaient avoir un sur-risque face au covid, on s'est rendu compte que non.
Il y a quelques papiers même qui font l'hypothèse est-ce que les antirétroviraux pourraient avoir un effet protecteur. Enfin, en tout cas, on a pas alors que à AIDES c'est quand même beaucoup de gens qui vivent avec le VIH, quand les gens sont à AIDES ils y sont à 99,9% sont sous traitement, et on s'est rendu compte assez vite que c'était c'était pas l'hécatombe.
En revanche là où c'était l'hécatombe c'était sur les gens qui fréquentent les actions de AIDES, qui peuvent avoir une insertion très forte dans ses actions ou qui peuvent n'en être que des usagers qui ne sont pas dans une démarche communautaire.

Là, en fait, on s'est retrouvé à organiser la distribution alimentaire. C'est-à-dire plus du tout le truc communautaire et tout. Les travailleuses du sexe nous disait qu'alors là elles ne pouvaient plus travailler, « nous distribuer des du gel des capotes, excusez-nous les gars, mais non ! C'est pas super utile ».
Donc on a eu une espèce de retour d'une forme de caritatif mais qui était la seule réponse communautaire possible.

Ça re-boucle avec ce que je vous disais mon rapport très jeune, et certainement un peu méprisant, à toute forme de caritatifs comme un dévoiement dépolitisant.
La suite m'a rendu un peu plus un peu plus un peu plus réservée, un peu plus nuancée en tout cas.
En posant la question de quelle santé communautaire et quelle santé communautaire y a un peu dans VIH, avec une espèce de recherche de santé communautaire des migrants.

Il y a certainement des lieux que je connais pas, que j'identifie pas, je en suis pas le public, d'une certaine façon c'est plutôt tant mieux.

D'une question d'une santé communautaire migrantes, qui ne renvoie pas à beaucoup de choses parce que les gens ont des parcours très divers, ont des vies très compliquées, où on pourrait décliner cette expérience communautaire de personnes nées à l'étranger.
C'est toujours un peu un échec, enfin, c'est souvent pas un échec mais ça prend pas, justement parce que les gens qui se tournent vers des associations qui se revendiquent de la santé communautaire, les gens se tournent vers ces associations en demandant à manger, un logement, des papiers et en fait de l'aide sociale dans sa version la plus la plus simple.
Souvent les groupes de parole, les bidules, les machins, ils ont pas le temps ça les fait suer. En fait, c'est pas ça qu'ils veulent et en gros, oui, ils veulent qu'on les accompagne en préfecture, qu'on les aide à faire  des recours.
C'est implacable, y a pas y a pas photo.

Et le dernier truc, voyez je suis un peu intarissable sur le sujet.

La question des centres de santé communautaires, je trouve cela très intéressant. Je trouve que les expériences qu'il y a à Saint-Denis, le château en santé, Échirolles, c'est des expériences…

**Q**
<figitw>
Au travers de votre riche parcours, de rapport que vous avez aux questions autours des centres communautaires,<br> de mon côté je perçois des élément forts jalons qui participent à la définition  de santé communautaire,<br> comme la libération de la parole, le fait de faire communauté, soit qu'on le décide soit qu'elle nous soit imposée par des zones géographiques que l'on a quittées, comme l'Afrique sub-saharienne, ou que l'on se retouve re-localisé⋅e, re-géographisé⋅e, comme dans des cités ouvrières pour prendre en exemple une situation que vous citez ; par sa  sexualité ou sa romance on se retrouve à faire communauté ; ou par une maladie on se retrouve à dans une communauté de maladie.<br>
</figitw>
<figitw>
Il y a aussi comme point fort l'expérience forte d'un vécu difficile, je reste poli pour dire difficile,par exemple les personnes décédées pendant les vagues COVID alros qu'elles avaient une maladie du rein.</figitw>

<figitw>
Ces premiers éléments forts qui commence à jalonner et servent de phare pour servir de repères pour pouvoir voir la naissance et ce que peut être une santé communautaire.</figitw>

<figitw>
Ma question, très terre-à-terre, quelle définition vous pourriez donner de la santé communautaire ?<br> Ensuite, comme vous venez du monde l'histoire, et que cela fait partie de vos recherches, quand est-ce que l'on peut envisager un début historique, dans une frise chronologique, de la santé communautaire ?
</figitw>

**<u>Caroline Izambert</u>** :

Je dirais que la santé communautaire c'est, si j'essayais, c'est une action en santé qui, ça va pas être une définition très académique, qui laisse plus de pouvoir aux usagers qu'aux soignants.
C'est d'ailleurs plus un critère qu'une définition que je vous propose; Il faudrait regarder si ça marche tout le temps.
C'est quand les objets des soins sont des sujets.

Alors, ça peut prendre des formes très diverses. Ça peut être de la consultation à la participation, à des formes de gouvernance. Ça n'exclut pas la place des soignants, mais il y a, moi je trouve, qu'on n'est dans la santé communautaire quand il y a un effort permanent de ne pas laisser libre cours. Je trouve que la santé communautaire elle n'existe vraiment que dans le bras de fer avec le pouvoir médical.
Dans la volonté de gagner de l'espace, de gagner du pouvoir par rapport aux médecins
Pour moi, c'est ce qui est déterminant, dis dans un sens un peu bourdieusien, en lien direct c'est le rapport à la critique médicale qui fait qui fait la santé communautaire.

**Q**
<figitw>
Quand commence ce bras de fer dans l'histoire ?
</figitw>

**<u>Caroline Izambert</u>** :

Je pense, si ça pas été complètement étudié, mais je pense qu'il y a y a des choses à trouver avec quelqu'un comme Laure Pitti travaille ça sur le passage de la médecine ouvrière, de la médecine du travail et des combats autour.
Là on rejoint des un des enjeux de santé environnementale, dans les années soixante soixante-dix, et je pense qu'il y a quand même dans les années soixante-dix entre l'émergence d'une médecine sociale, l'Organisation Mondiale de la Santé (OMS), et, on va dire le moment tiers-mondiste de l'OMS, et des expériences de radicalité à l'image des black panthers, qui font qu'on a quand même quelque chose qui se crée à ce moment-là.
Et qui émerge de tout point, je pense qu'il y a une histoire, là moi je connais vraiment moins bien et qui à mon avis est passionnante. Peut-être vous la connaitrez mieux ?

Je pense qu'il y a une histoire au sud, c'est-à-dire la façon par contrainte les pays du sud ont eu des questions de démographie médicale qui leur ont amené à organiser un peu autrement
Enfin l'exemple je le connais pas bien, cependant je connais bien quelqu'un qui en parle très bien. C'est tout ce que raconte Jean-Paul Gaudillière sur le Kerala en Inde.

Voilà c'est le moment où il y a des endroits des petites portions où l'on fait de la santé sans que ça soit les médecins qui ont tout le pouvoir.

Je pense que c'est fin soixante, et pour moi la bascule, parce qu'il faut en trouver une et puis allez historiquement elle marche toujours, c'est soixante-dix-neuf avec l'éradication de la variole où on re-rentre dans un paradigme biomédical très très triomphant qui sera un peu entamé par le VIH.

Mais le développement de l'humanitaire international, la fin de la variole, là on est dans un truc, dans une médecine très viriliste, très occidentale, très bio-centrée, qui va un peu un peu balayer.
Je pense qu'il faut distinguer toutes les expériences rattachées aux mouvements sociaux, santé des femmes, santé ouvrière, santé des minorités, et puis les systèmes de santé, à l'image du québec et du kerala, qui revendiquent de s'organiser sur une base communautaire.

Je pense que c'est là qu'on a qu'on a un moment de bascule dont l'héritage est vraiment à éclipse. Cette histoire des black panthers de la santé, c'est vraiment ce bouquin qui a une dizaine d'années, qui a cartonné, le bouquin d'Alondra Nelson sur le sujet.
C'est assez drôle de voir qu'elle est maintenant *Chief of Science* de la maison-blanche, même y compris avec le fait de voir comment les histoires radicales ont des pouvoirs d'instituts et d'institutionnalisation.


**Q**
<figitw>
Vous avez commencé avec le bras de fer de pouvoirs entre les personnes soignées et les personnes soignantes qui est un marqueur pour vous de la santé communautaire.<br>


Puis maintenant, ce rapport radical au pouvoir qui vient s'institutionnaliser, organisé par l'état.
</figitw>

<figitw>
Je me rappelle d'un bouquin du début des années soixante-dix « Sorcières, sages-femmes et infirmières; Une histoire des femmes soignantes » de Barbara Herenreich et Deidre English.<br>
Qui raconte de comment l'État s'est mis à organiser en fait et a institutionnalisé ces femmes soignantes et qui n'avaient pas de titre, qui étaient indépendantes, sans d'ordre supérieur, etc.<br>
</figitw>
<figitw>
Cette institutionnalisation par l'État est venue apporter quelque chose de délétère, finalement, dans le système de santé. Qui en plus est devenu capitaliste et libéral par la suite.<br>

C'est aussi voir comment l'État qui vient organiser vient aussi rentrer dans les jeux de pouvoirs
</figitw>

<figitw>
Et vous mentionnez aussi comment la santé communautaire vient se substituer à bas coût ou substituer de la responsabilité de l'État.<br>
Il y a là aussi des rapports de pouvoir pouvant se reconfigurer au travers de la santé communautaire.
</figitw>

<figitw>
Peut-être aussi, et ça c'est une intuition que je vous partage en question, parce que vous l'avez évoqué par vous-même les Black Panthers, les Young Lords.<br>
Il y avait les Free Clinic, les luttes et dépistages contre la drépanocytose, et aussi pandémie de drogue. Les Young Lords avaient appris eux-mêmes des Black Panthers en ajoutant un côté habitat social géographiquement situé dans la considération de la santé communautaire.
</figitw>
<figitw>
Il y avait aussi le mouvement de la liberté de l'avortement et pour la contraception (MLAC)<br>
</figitw>

<figitw>
Cela reste des grandes figures de lutte sociale, je vois la santé communautaire comme un mouvement social, et je ne vois les grands phares historiques comme uniquement nés dans la souffrance.<br>

En est-on encore là aujourd'hui ?
</figitw>


**<u>Caroline Izambert</u>** :

Bonne question…

Alors oui et non. Non, d'accord si là on ressentait, là ça fait un peu éditoriale de l'Obs à deux francs six sous, mais la crise des démocraties représentatives, etc. font que du côté pas n'importe quelle collectivité, État, bidule, essaye de retrouver un peu d'allant dans les processus participatifs et de ce point de vue-là la santé n'y échappe pas.
Il y a un numéro de la revue Mouvements sur « mondialisation sud et nord dans la santé » qui est assez intéressant, on a quand même l'accumulation d'une partie des systèmes de santé, par exemple d'Afrique de l'ouest, qui reposent sur des travailleurs communautaires depuis vingt trente ans.

Il y a un effet aussi de sédimentation qui font que ces expériences, nées d'un fait qu'il n'y a pas beaucoup de médecins, infusent, y compris au nord.

Là où moi j'ai toujours une difficulté, j'adore les grandes mythologies, que j'adore l'histoire des young lords, et parfois je me dis qu'il y a un petit côté « les contes de la santé ». Je sais pas comment dire.
On se met fond de la couette et on se remémore./
Ça reste, et je ne demande pas que ça soit autre chose parce que je pense qu'on a besoin de ces chansosn de geste militantes.
Mais voilà, ça reste quand même toujours un petit peu enchanté. On ne sait trop jamais combien de personnes étaient vraiment concernées, ni combien de temps ça a duré.

D'une certaine façon ce sont toujours une histoire des organisateurs, c'est pas une histoire des gamins.
Je connais les mouvements politiques, peut-être que les gamins qui venaient manger les petits-déjeuners des black panthers ils devaient aussi être tirés du lit par le chef de section et très tôt le matin pour que la photo soit belle.
Je trouve que cette histoire est mal connue.  Les travaux des quinze dernières années ont été utiles, mais je trouve parfois restent dans un certain romantisme.
Romantisme qui nous est tellement utile que je suis pas sûre de vouloir le dépasser, mais je pense qu'il faut quand même se dire qu'il y a une petite part de mythes.

Aujourd'hui, je pense qu'on est dans un moment compliqué. Ça l'est moins dans la douleur, par contre ça naît dans une proximité institutionnelle.

Les centres de santé participatifs sont financés par appels à projets du ministre de la Santé. C'est pas tenu par Che Guevara jusqu'à preuve du contraire malgré les changements opérés.
Il y a une institutionnalisation qui ne m'empêche pas de dormir, je suis quelqu'un qui adore les institutions.
Mais qui fait que le critère de radicalité, et notamment critique du pouvoir médical, du capitalisme, etc., s'effrite beaucoup et je ne sais pas très bien quels lieux il y aura.
Ou alors ces lieux hyper-minoritaires où, par exemple, on fait le choix de ne pas consommer de médicaments. C'est rare que les gens fassent le choix jusqu'au bout de ne pas consommer de médicaments produits par l'industrie pharmaceutique.
Il y a des choses que je peux pas. Je peux trouver une organisation communautaire, si elle dit que c'est super quand on est séropo d'arrêter ses traitements qui empoisonnent et qui sont produits par des brigands capitalistes, ce qui est vrai.  Mais je veux pas, ça me paraît compliqué.

Il y a une fenêtre d'opportunité pour que ces expérimentations, mais je ne sais pas comment on fait pour qu'elle garde cette combativité, cette relation tendue mais pas absente aux institutions et au reste du système de santé.
Le désarrimage complet me semble pas avoir beaucoup d'avenir, voire être dangereux pour les personnes elles-mêmes.
Mais l'agrégation complète avec les institutions de santé n'arrivera qu'à la création, en fait, d'une énième catégorie de sous-travailleurs sociaux moins bien payés.
Je caricature, mais je ne sais pas comment on fait exister cet espace.

En tout cas, je pense que vous le verrez, je pense que d'autres pays ont des configurations sociales différentes.
Dans un pays comme la France où ces espaces, on va dire d'auto-organisation militante, sont communautaires et n'ont pas forcément une pérennité hyper longue.

J'ai un tout petit peu travaillé avec les organisations de consommateurs italiens. Ce sont des trucs là aussi assez institutionnels mais qui ont des centrales d'achat, qui ont des comités locaux et ne dépendent pas de l'État mais sont pas non plus une espèce de groupuscule.
Ils sont puissants et continuent à rendre des services très très fort pour leurs adhérent⋅e⋅s;
Je sais pas comment vain va maintenir ça. Pour les centres de santé communautaire, il y a pour moi ce qui me pose un souci par la place que les médecins y occupent.
Plutôt des médecins supers, ais c'est pas des centres qui sont pilotés par, en tout cas ce que je connais et je dis pas du tout hyper compliqué, il y a une place pour les patients, mais il n'a pas beaucoup de tensions.

D'ailleurs ceci dit, pour moi, c'est des centres qui renvoient à une tradition, là beaucoup plus ancienne, qui est celle de la médecine sociale.
C'est pas du tout un héritage dont qui que ce soit devrait pouvoir rougir. Là, c'est héritage du dix-neuvième siècle, du choix de médecin d'êtres dans autre chose que la production de soins et un autre rapport à la rémunération.

Je trouve ça super, je trouve qu'il y a une petite dimension, des formes d'engagement laïque. Tout le monde est payé pareil, il y a une forme d'ouvriersime, de vieilles formes d'engagement, de vieilles formes d'engagement militant.
Soit ça reste des espèces de points sur la carte, des refuges, de lieux ressources, soit ça s'institutionnalise.
Mais j'ai du mal à voir quel est l'avenir et de quelle est la façon dont maintient cette tension. Et surtout où les gens qui viennent sont en position de dire qu'ils sont pas contents.

J'ai discuté avec des médecins d'un centre communautaire qui évoquaient avaient des patients qui voulaient des antibiotiques, qui disait « moi j’ai est un rhume depuis, euh, j’ai une angine depuis une semaine. Je veux des antibiotiques, et plutôt des antibiotiques à large spectre ». Là, les nouveaux médecins qui disaient « mais non, on a fait le test et c’est pas une angine bactérienne il n’y a pas d’intérêt à avoir lutte contre l’antibiorésistance » Mais au fond, ils voyaient bien qu'au nom d'un intérêt supérieur, ils retrouvaient la posture très verticale, très sachante de la médecin, exactement ce contre quoi ils essaient de construire leur pratique. Là il y avait un endroit que je trouvais vraiment passionnant.

Qu'est-ce qu'on fait de ce moment on est dans un centre communautaire quand un patient vous demande des antiobiotiques alors que ce n'est pas prescrit, où on dit qu'on est dans la co-construction de l'acte médical avec le le patient et le patient qui dit « regardez, les antibiotiques moi je vous dis… », quand un patient vous demande des antiobiotiques alors que ce n'est pas prescrit.
En fait le médecin, oui, à une forme de révélation de son statut de prescripteur d'autorité médicale qui dit « moi je veux pas ».

Comment on fait cette négociation ? Je pense pas qu'il y ait de solution simple, mais je pense que ça sera intéressant, justement, que tous ces moments de tension dans ces expériences, un petit peu moins rose, puissent être explicités et de ça savoir comment on gère ça et on gère le fait que ces centres restent des endroits où vous avez plutôt des médecins qui sont qui viennent de milieux favorisés, ou en tout cas classe moyenne supérieure, plutôt blancs, avec plutôt des quartiers populaires de gens immigrés, enfants d'immigrés, racisés, et que cette ligne de classe, cette ligne de races, elles traversent ces centres.

J'ai l'impression que c'est dans les difficultés de ces expériences qui a les choses les plus intéressantes mais c'est dur de rendre compte de ces difficultés quand on a déjà du mal à faire survivre son modèle.

Je sais pas du tout si je réponds à vos questions, et  si je peux paraître un peu confuse.

**Xavier Coadic** :

<figitw>
Si, vous y répondez, et avec beaucoup de profondeur, de détails et de haute couture, je dirais.<br>

J'aurais juste une dernière question.
</figitw>

<figitw>
J'aperçois au travers de ce que vous me dites, et aussi de ce que j'ai pu commencer à travailler, ce sont des questions de santé publique et en face des formes d'épistémologies.<br>
C'est-à-dire, qui a le droit au chapitre ? De prendre le pouvoir dans cette direction, de ce choix en santé publique ?<br> L'institution que peut être l'État ?
</figitw>


<figitw>
Je viens à l'instant de penser, lorsque vous datez la naissance de la santé communautaire, fin des années soixante-dix. C'est 1980 en France le début de la santé publique selon Didier Fassin.<br>
Il prend en exemple de référence l'épidémie d'intoxication au plomb, notamment chez les enfants, et encore plus notamment les enfants immigrés, pour dessiner les piliers de la santé publique et l'État dans la santé publique.
</figitw>

<figitw>
Aujourd'hui, on voit la santé communautaire fleurir différemment et participer à la santé publique. Il y a ces nouveaux rapports de force, et en plus de dates historiques que je trouve très intéressantes, et que cela se joue, en fait réellement, par des humains au travers de leurs rapports.<br>
C'est ce que vous décrivez bien dans les discussions et les conflits et les tensions entre le soignant-soigné.
</figitw>

<figitw>
Là, j'arrive au point où j'ai très très envie d'être sur le terrain pour aller observer cela.<br>

Je crois que c'est là l'un des marqueurs de définition.
</figitw>


**<u>Caroline Izambert</u>** :

Ce qui est très intéressant, je suis vraiment là dans ce qui fait l'objet quasiment quotidien du travail que je peux faire à  la ville [de Paris]. La santé publique porte en elle, quoi qu'il arrive, un projet un peu autoritaire.
La santé publique, d'où que l'on se tourne, c'est compter les corps, normer les corps. Il y a quelque chose de très fort là-dedans, et pourtant je pense qu'elle est nécessaire, je trouve qu'en France on n'en fait pas assez, mais elle porte en germe quelque chose de cette de cette dimension-là.

À l'autre extrémité, la santé communautaire, normalement, c'est l'inverse. Justement, c'est le projet, entre autres, anti-autoritaire par excellence.

Du coup, être dans un moment où les deux, alors que c'est vraiment pour moi l'eau et l'huile, essaient se rencontrer c'est super.
Je trouve ça fascinant et je trouve que parfois on fait semblant qu'on ne voit pas le problème.


## Bibliographie

+ **Pandémopolitique. Réinventer la santé en commun**, Jean-Paul Gaudillière, Caroline Izambert, Pierre-André Juven. Éditions la Découverte, 2020

+ **La catastrophe invisible. Histoire sociale de l’héroïne (France, années 1950-2000)**, De Michel Kokoreff, Anne Coppel et Michel Peraldi (dirs.). Éditions Amsterdam, 2018

+ **Body and Soul: The Black Panther Party and the Fight Against Medical Discriminatio**, Alondra Nelson, University of Minnesota Press, 2013

+ **De la fabrique des militant-e-s à la fabrique des patient-e-s**, Maud Gelly, Bibia Pavard. Dans Genèses 2016/1 (n° 102), pages 47 à 66

+ **MALADIES RÉNALES ET INÉGALITÉS SOCIALES D’ACCÈS À LA GREFFE EN FRANCE**, Christian Baudelot, Yvanie Caillé, Olivier Godechot, Sylvie Mercier. Institut national d'études démographiques (INED), 2016.

+ **Réinventer la médecine ouvrière ?** Pascal Marichalar, Laure Pitti. Dans Actes de la recherche en sciences sociales 2013/1-2 (n° 196-197), pages 114 à 131

+ **Mondialisation, santé et droits de l'homme au Sud et au Nord**, Moncef Marzouki. Dans Santé Publique 2003/3 (Vol. 15), pages 283 à 289

+ **Naissance de la santé publique**, Didier Fassin, discours d'introduction au Collège de France, 2021.

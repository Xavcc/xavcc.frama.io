---
title: "Appel à dons pour financer et développer un test d'enquête environnementale à base de graines sous licence libre"
layout: post
date: 2023-01-06 07:40
image: /assets/images/water-sampling.jpg
headerImage: true
tag:
- hsociety
category: blog
author: XavierCoadic
description: "Bioassai et graines de luttes"
---

<figcaption class="caption">Description image d'en-tête : une main d'une personne prélève de l'eau dans un bras mort de la Vilaine à Rennes à l'aide d'un tube plastique. Cela se fait depuis un bord sur une berge végétalisée et pelousée, le coin d'eau est lui aussi verdi par des lentilles et d'autres végétaux. Photo issue du Chantier Reprises des Savoirs, Pluri-versités à Rennes, octobre 2022</figcaption>
<br />

En 2015, sur le bief de partage du canal de Nantes à Brest à Glomel (22), je démarrais des expériences en écotoxicologie dans les eaux locales (le canal, le bief, le lac du Korong). [Depuis 2019](https://notesondesign.org/xavier-coadic/) je travaille sur la conception, les tests, et déploiement d'un ensemble de méthodes et techniques de tests environnementaux sur les qualités de l'eau et des sols. Je fais cela avec l'objectif que ce dispositif soit utilisable dans votre cuisine, votre cave, votre garage, votre squat, votre cabane et même depuis « lieux de privation de liberté (►►) »<marginnote>C'est un petit truc que je mets en place avec des complices en ce moment même</marginnote>… Bref, là où vous en avez besoin et quand vous en avez besoin, avec vos ressources et les matériaux dont vous disposez déjà. Ceci devant permettre d'obtenir des résultats d'une qualité comparable avec ce qui est fait dans l'industrie et dans les laboratoires.

Ce matin j'ai vécu une petite scène, presque quotidienne pour ma part, qui a déverrouillé en moi une motivation et un sentiment de rassurance (dont j'avais bien besoin).

Je ramasse ~50 centilitres d'une flaque d'eau au milieu d'un square. Elle est bien irisée dans ses reflets.

Une dame, qui a terminé son obligation de se sacrifier au travail il y a bien longtemps, passant juste devant moi me demande ce que je fais.

« Je ramasse cette eau puis je la mets dans des petits pots en verre en différentes quantités avec de l'eau distillée. Ensuite j'y mets des graines de radis et j'observe, photographie et consigne les observations pendant 10 jours. »

Silence, et second silence, et regards qui me décortiquent…

« Vous faites cela pour quel laboratoire ? » me dit-elle.

« Pour vous, pour les enfant⋅e⋅s, pour moi ».

`[Yeux écarquillés et sourire]`

Je remarque que les trois ouvriers perchés sur les échafaudages de la façade attenante, en chantier, se sont arrêtés et nous observent en silence.

Je me souviens qu'hier j'avais reçu un courriel en provenance de matière grise d'université et d'Enseignement Supérieur et Recherche (ESR) classant cela dans « architecture oppositionnelle et contre-école ». Quel curieux lexique !

Il y a là dans mon tendre vendredi anecdotique au moins 4 points de vue qui se collisionnent au travers d'un petit geste et de quelques mots pour l'accompagner dans un sujet plus vaste qu'est celui de l’écologie et de la qualité de notre environnement (et de nos responsabilités et capacités d'agir).

Ce que je fais à du sens et de l'importance. Je l'ai fait et continue à la faire en Bretagne, en Dordogne, en Belgique, en dans des vidéoconférence pour des personnes des dizaines de pays du globe. Bientôt ailleurs, j'espère, avec une bonne dose de confiance.

Enfermée dans un laboratoire, et réservée à de petits groupes d'une « élite », l'enquête environnementale, et les questions de pollutions sont le fait de privilèges. J'essaie d'en faire un mode d'action politique qui est un droit populaire plus qu'un privilège.

![](/assets/images/gege-neratoir-bioassay-2023.png)
<figcaption class="caption">Description image : une place de village avec église un fond, arbres et pelouses au second plan. Au premier plan, sur le trottoir des personnages dessinés par Simon « Gee » Giraudot : Une personne à droite demande « Mais que faites-vous par terre à genoux les mains dans l'eau sale ? ». L'autre à gauche répond : « Fernand Deligny nous a légué « Graine de crapule ». Bah, nous, aujourd'hui, nous devenons des Graines de Luttes. Et nous ouvrons des voies libres et des capacités d'agir et de prendre soins. » Réalisé et monté avec Gégé, le générateur de Grise Bouille. Grise Bouille et l'intégralité de son contenu (images et textes) sont l'œuvre de Simon « Gee » Giraudot et diffusés sous licence Creative Commons By-Sa. GéGé est une adaptation de Comic Gen de Willian Carvalho réalisée par Cyrille Largillier pour Framasoft.</figcaption>
<br />

Dans la même veine que des [milliers de personnes qui surveillent la qualité de l'air environnant](https://sensor.community/fr/), que d'autres [partagent les données de biodiversité](https://www.gbif.org/fr), nous pouvons agir de manière similaire sur l'eau et les sols. C'est alors créer un bien commun des moyens de production.

**Table des contenus :**

1. [Un appel aux dons sur quoi et pouruqoi ?](#un-appel-aux-dons-sur-quoi-et-pourquoi)
2. [Combien d'argent, où donner et pourquoi faire − et que pouvez-vous donner si vous n'avez pas d'argent ?](#combien-dargent-où-donner-et-pourquoi-faire-−-et-que-pouvez-vous-donner-si-vous-navez-pas-dargent)
3. [Que se passera t'il si je ne reçois pas 3 800 € ?](#que-se-passera-til-si-je-ne-reçois-pas-3-800-€)
4. [Quelques précédentes étapes clés pour cette petite histoire](#quelques-précédentes-étapes-clés-pour-cette-petite-histoire)

## Un appel aux dons sur quoi et pourquoi ?

Prélever de l'eau dans un caniveau à côté d'un chantier, ou dans un rivière, ou les eaux de pluie, ou filtrer la terre d'un sol… puis y placer des graines et observer leur germination (ou absence) et formes et couleurs de ce qui germe, c'est une manière simplifiée de vous dire que je fais des [bioessais](https://fr.wikipedia.org/wiki/Essai_biologique) sur le « terrain ». Avec des graines et un milieu liquide, il faut 5 à 8 jours pour obtenir des résultats observables avec des différences significatives.

![](/assets/images/graines-control-serie-1-reprises-des-savoirs-rennes.jpg)<marginnote>Des boites en plastique vendu pour stocker des perles à bijoux peuvent aussi faire l'affaire et elles sont autoclavables, Plastic Shuttle Cup 2 1/2" with Removable Lid − réutilisables, Fabriquées en polypropylène et peuvent être réautoclavé plusieurs fois.</marginnote>
<figcaption class="caption">Description image : des graines de radis qui ont germé dans de l'eau distillée dans un petit rond en verre récupéré dans une poubelle de supermarché (puis désinfecté avant ré-usage) qui servait à contenir une crème brulée</figcaption>
<br />

Je débauche des énergies et du temps son compter pour arriver à la conception d'un « bioassai » avec sa recette de fabrication et sa mise en œuvre sous licence libre qui soient effectivement appréhendables par les personnes qui en ont l'envie et besoin, et cela dans les [conditions de vie quotidienne qui sont les leurs](https://open.audio/library/tracks/347219/). Dans la même temporalité j'ai glissé dans la grande précarité et n'ai pas accès à mes droits aux minima sociaux, ni autres couvertures.

Ce travail prend déjà forme en deux langues au travers des documentations existantes :

+ [Investigating Water with Seeds](https://publiclab.org/notes/xavcc/12-17-2021/investihating-water-with-seeds) dans Public Lab
+ [Initiation aux essais biologiques avec des graines pour exposer la toxicité dans l'eau](https://wiki.kaouenn-noz.fr/hors_les_murs:hack2eaux:initiation-aux-essais-biologiques-avec-des-graines-pour-exposer-la-toxicite-dans-eau) dans le wiki du Biohackerspace Kaouenn Noz

Ces débuts de « recettes mises à disposition » sont à améliorer et les expérimentations sont à poursuivre afin d'affiner la méthode, le protocole, et le « kit ». J'ai d'ailleurs écrit ce matin à Donat Hader et Gilmar Erzinger, auteurs de "Bioassays Advanced Methods and Applications" (SBN: 978-0-12-811861-0), pour leur demander de l'aide.

> « D'autres considérations pour un essai biologique praticable sont la robustesse de l'instrument et une longue durée de conservation dans le cas de biomatériaux tels que l'ADN extrait, les bactéries lyophilisées ou les graines de plantes. » "*A comparison of commonly used and commercially available bioassays for aquatic ecosystems*"
<cite>Azizullah Azizullah and  Donat-P. Häder</cite> in "Bioassays Advanced Methods and Applications"

Cette recherche et développement fait suite au guide, sous licence libre, que j'avais rédigé en 2020 « [Bio-investigations sur le Terrain](https://kit.exposingtheinvisible.org/fr/bio-investigation.html) (disponible en français et en anglais). Ces efforts sont aussi une partie du projet [Hack2O](https://ps.zoethical.org/t/hack2o-intro-explication/6116) dans lequel je mettrais tout autant d'engagement très prochainement.

Aujourd'hui je vous écris à vous aussi en vous demandant votre aide.

![](/assets/images/Bioinvestigation_montage.png)
<figcaption class="caption">Description de l'image : un dessin à la main sur papier blanc représentant une place d'un centre-ville aménagée avec une zone pavée piétonne, des arbres et des ronds d'espaces végétalisés à leurs pieds. Le dessin est agrémenté de photographie illustrant des points d'intérêts pour l'enquête environnementale : Feuilles des arbres, lichens sur le tronc et l'écorce, végétation rase au pied et prélèvement de terre, point de rétention d'eau</figcaption>

<div class="breaker"></div>

Cette recherche et développement que je mène est contenue dans un cadre de critères :

+ Doit être **reproductible et répétable** (sinon, ce n'est qu'une performance).
  + les « défaillances », y compris dans la « reproductibilité » sont [considérées comme essentielles](https://www.pnas.org/doi/full/10.1073/pnas.1806370115)
  + Le *Lent travail de [Debug est souhaitable](https://www.codeworks.fr/articles/un-manifeste-du-slow-debug)* (valable bien au-delà du code informatique)
+ **Publication, données, documentation en gratuité et licence libre** ; **logiciel, dispositif, hardware, wetware** sous licence libre (gratuité au plus possible).
+ Véritable ouverture (c'est-à-dire **accessibilité et inclusivité**).
  + Respect des différents rythmes et façons de procéder, entre et au sein des vies/individus/pratiques
  + La matérialité de l'accès aux équipement de base pour ce « kit / test », et les différences selon lieu de vie/statut social/ressources économiques, sont considérées avec grande attention (exemple accès à des graines, accès à des récipients en verre, temps disponible pour réaliser le bioessai, etc.)
+ Impliquer **les contributeurices** "citoyen⋅nes" **en tant que pairs** (c'est-à-dire au niveau « co-auteurices » à minima).
+ Méthodes qui mettent l'accent sur les valeurs d'**humilité, d'équité et de bonne relation avec l'environnement, dont le non-humain et le non-vivant** (exemple https://civiclaboratory.nl).
+ Prise en **considérations des enjeux politiques, des menaces potentielles et effectives** pour les individus qui pratiquent
+ **Sûreté, sécurité individuelle et collective et numérique** (oui, peut en France faire de la garde à vue pour des prélèvements d'eau ou avoir affaire au parquet national antiterroriste (PNAT) ou vouspouvez être menacé⋅e⋅s par un⋅e voisin⋅e avec un outil ou une arme)
  + J'ai contribué et donné des formations sur :
    + Exposing The Invisible <https://exposingtheinvisible.org/>
    + [Gendersec](https://en.gendersec.train.tacticaltech.org) , [Gender & Tech Ressources](https://gendersec.tacticaltech.org/wiki/index.php/Main_Page)
    + DataDetox Kit <https://datadetoxkit.org/fr/home>
    + Security in a Box <https://tacticaltech.org/projects/security-in-a-box/>
    + The GlassRoom <https://theglassroom.org/>

Il y a une sorte de bonus caché sous cette somme de travail : j'y développe aussi et conjointement des appareils / aides techniques « lowtech » sous licence libre qui viennent en complément et/ou en besoin avec les bioessais à base de graines :
+ [Une centrifugeuse à bas de sous-bacs de bière](https://wiki.kaouenn-noz.fr/hors_les_murs:lab0_bi0_p0p:la_sauce_bio:ohl_sauce_centrifuge)
+ Un [filet de pêche à micro-plastique](https://wiki.kaouenn-noz.fr/hors_les_murs:hack2eaux:pieges_a_microplastiques)

Dans les champs de l'écotoxicologie et des bioassais, le standard de qualité scientifique est nommé ECOTOX, crée par l'INRA en 2009, encadré par la norme ISO 6341 (2012). Il se déroule sur [daphnies](https://fr.wikipedia.org/wiki/Daphnie) juvéniles placée dans un milieu dans lequel on suspecte ou l'on connait la présence d'un perturbateur chimique / polluant ou d'un cocktail d'agents. Le paramètre mesuré est l’inhibition de la nage, mouvement et capacité de déplacement, pendant 24 à 48h. C'est ce point d'horizon qui pourrait me servir de comparaison avec les bioassais à base de graines. Avec les graines, autre standard, il y a celles des Arabidopsis thaliana Col-0 ecotype qui sont [organismes modèles](https://fr.wikipedia.org/wiki/Organisme_mod%C3%A8le) de références en biologie. Celles-ci étant quasi-impossibles à obtenir pour une personne hors d'un laboratoire spécialisé, et encore plus difficile de les vérifier ensuite. La très petite taille des graines en font un objet diffcile à manipuler et une piètre candidate pour l'observation à l'œil nu / appareil photo « grand public ».

Pour pratiquer des « vérifications » sur les résultats obtenus avec les prototypes de bioassais avec graines, et aussi pratiquer des expérimentations en écotoxicologie parallèlement à ceux-ci, j'utilise le matériel biologique libre de [Standford Free Genes](https://stanford.freegenes.org/) sous [convention ouverte de transfert de matériel](https://fr.wikipedia.org/wiki/Convention_ouverte_MTA) (OpenMTA). Il existe maintenant  petit réseau qui permet d'obtenir ces wetware gratuitement ou au cout d'un envoie postale.

![](/assets/images/ruisselement-chantier-rennes-automne-2022.JPG)
<figcaption class="caption">Description image : photographie de l'intérieur chantier d'immeuble en construction depuis le ras du niveau du trottoir adjacent. Un tuyau gris en PVC rigide rejette de l'eau puisée dans les fondements du bâtiment en construction. Elle est couleur « ciment » et est rejeté directement dans la bouche d'égout sous le trottoir.</figcaption>
<br />

Il y a un certain nombre de conditions préalables à la mise en place d'un système de bioassai moderne efficace :

+ Le ou les paramètres du système doivent être très sensibles pour la substance toxique analysée/visée.
+ Le temps de réponse doit être « rapide »
+ L'essai biologique doit pouvoir être utilisé pour des mesures aiguës (à court terme) ou à long terme.
+ Le dispositif doit être facile à utiliser et ne pas nécessiter une longue formation du personnel.
+ Le prix ne doit pas être excessif, surtout lorsqu'il est destiné à être utilisé dans les pays en  dit en développement.
+ Le matériel biologique doit être facilement accessible et les coûts de fonctionnement doivent être faibles.

## Combien d'argent, où donner et pourquoi faire − et que pouvez-vous donner si vous n'avez pas d'argent ?

**3 800 €** à recevoir en dons avant le 31 janvier 2022, voici l'objectif ! (et mon *sauf-conduit* pour traverser la précarité et dans un [département qui veut conditionner l'accès au RSA](https://alter1fo.com/rsa-departement-illeetvilaine-137577) [dont mon dossier est en attente] à 20h de travail sans les acquis et sans les droits du travail).

Pour l'année 2022 j'ai reçu 3 902,19 € de dons via Liberapay. Majoritairement à partir de Juin de la part de personnes qui m'ont aidé à ne pas tomber plus précaire que je l'étais déjà.

![](/assets/images/liberapay-2022.png)
<figcaption class="caption">Description image : Capture d'écran depuis mon compte Liberapay. Sur fond blanc, 2 graphiques, 1 Revenu par semaine (en euro) et 1 Nombre de donateurs par semaine. Graphiques horizontaux de l'historique chronologique des dons reçus avec des barres verticales violentes pour illustrer les nombres d'euros reçus pas semaine et le nombre de personnes donatrices.</figcaption>
<br />

Il est possible de faire un don avec une carte bancaire ou par virmement, les devises acceptées sont : € ; $ ; $AU ; $CA ; CHF ; £.

**Je reste donc avec un appel aux dons via [Liberapay ici](https://liberapay.com/Xav.CC/donate)**. Pas de site web de service de « crowdfunding » car ce serait rentrer dans un jeu de concurrence avec d'autres projets et je n'ai ni le temps ni l'énergie pour cela, aussi je n'ai pas cette forme de compétition. De plus, ce serait vous amener à fournir de vos informations personnelles à ces « plateformes » et ça c'est non sans détour.

**3 800 €** pour :
+ **3 mois de travail** (février, mars, avril), soit 1 000 € par mois
  + Corriger et améliorer les documentations existantes
    + ajouter « Comment choisir les graines pour les essais biologiques et où les trouver ? »
    + ajouter « Comment choisir l'équipement et le matériel »
  + Refaire des séries de bioassai avec micro-dosages et différentes graines
  + Comparer avec d'autres protocoles et expériences puis évaluer les différents standards
  + Réaliser des comparatifs entre le mode « depuis ta cuisine » et « laboratoire d'analyse » sur les protocoles et les résultats
+ **Animer 3 ateliers de 5 jours consécutifs** chacun dans des lieux/communautés en demande (voir le chantier d'octobre 2022 [Louzaouiñ Graines de luttes 2022](https://wiki.kaouenn-noz.fr/hors_les_murs:reprises_des_savoirs:louzaouin_graines_de_luttes))
  + Avec des méthodes en éducation populaire
  + Avec une demi-journée « Biopanique, cuisine et féminisme »
+ Mettre tout cela en documentation
+ **800 €** de frais
  + 500 € de déplacement (Sud France, Nord Italie, Belgique)
  + 300 € de matériel
    + équipements pour expériences avec les standards de référence dans les laboratoires classiques
    + consommables divers (micro-tubes, eau distillée, réactifs, papier Whatman, etc.)

![](/assets/images/pocket-pcr.jpeg)
<figcaption class="caption">Description image : une petite machine, carte et circuit électronique avec écran led et un bouton de navigation menu. Sur la gauche de cette carte : Une sorte de carrousel (il est fixe) permettant de placer des micro-tubes 0,2 ml et de le chauffer / refroidir par cycles, thermocyclage, permettant entre autres choses de faire des réactions en chaîne par polymérase (PCR). Il s'agit de la Pocket PCR du Gaudi Lab sous licence Open Hardware et logiciel libre que j'utilise régulièrement.</figcaption>
<br />

**Si vous n'avez pas d'argent, vous pouvez aussi donner un peu de :**

+ de votre capital social : en mobilisant vos réseaux et relation et relayant cet appel au don avec un petit mot de votre part
+ de votre temps en discutant avec moi
+ de vos crayons / logiciels libres si vous avez quelques savoirs en illustrations
+ de vos équipements / matériels qui ne vous seraient pas utiles et qui pourraient aider dans cette aventure.

![](/assets/images/bioessai-serie-janv-2023.jpg)
<figcaption class="caption">Description d'image : un plan de travail avec 5 cassettes plastique transparent contenant chacune 6 cuvettes à l'intérieur desquelles 3 graines de radis par cuvette. Chaque cassette est dédiée à la germination des graines dans une concentration spécifique au sein d'un milieu liquide : 1. Control 100% eau « stérilisée » ; 2. 75% eau « stérilisée » et 25% eau de prélèvement ; 3. 50% d'eau « stérilisée » et 50 % d'eau de prélèvement ; 4. 25% et 75% ; 5. 100% eau de prélèvement</figcaption>

## Que se passera t'il si je ne reçois pas 3 800 € ?

[Don et contre-dons](https://www.editionsladecouverte.fr/justice_don_et_association-9782707135919), je suis obligé envers vous par [relation](https://journals.openedition.org/sdt/29087) « […] de la promesse, d’une promesse qui exige la confiance de tous envers tous et s’en nourrit »

Quoi qu'il advienne de mon appel ici écrit, le travail d'investigation environnementale continuera et les prototypages aussi. Je ne sais à quel rythme ni dans quel délai. Aussi, j'ai D'ores Et Déjà engagé des interviews avec hacktivistes de l'eau au Pakistan et à Hawaï pour ce début d'année pour diffuser leurs actions et peut-être apporter un peu de soutiens. De plus, j'ai lancé cette semaine 3 séries de bioassais avec des graines dans la première série un échelonnement d'eau prélevée sur un parking automobile et dans la seconde un échelonnement avec micro-dosage inférieur ou égal à 0,1 ml de gasoil dans de l'eau distillée, la troisième micro-dosage inférieur ou égal 0,01 ml.

J'écrirais tous les détails de cette quête et des dons le premier week-end de février ici sur mon blog.

![](/assets/images/micro-dosage-2023.jpg)
<figcaption class="caption">Description de l'image : un plan de travail avec 5 cassettes plastiques contenant chacune 6 cuvettes à l'intérieur desquelles 3 graines de radis par cuvette. Une micropipette est posée sur le plan de travail au milieu des 5 cassettes. Chaque cassette ici est dédiée à la germination des graines dans une concentration spécifique au sein d'un milieu liquide avec un micro-dosage inférieur ou égal à 0,1 ml.</figcaption>

## Quelques précédentes étapes clés pour cette petite histoire

+ 2002 à 2012, [Section Opérationnelle Spécialisée en dépollution](https://defense-zone.com/blogs/news/bataillon-marins-pompiers-marseille), BMPM, Marseille
+ [Écotoxicologie, en 2015, sur le bief de partage du canal de Nantes à Brest à Glomel (22)](https://pixelfed.social/p/XavCC/114362136498671616)
+ Les ateliers « [Biopanique, cuisine et féminisme](https://wiki.kaouenn-noz.fr/ateliers:biopanique_cuisine_feminisme) » depuis 2016
+ Les [IndieCamps](https://movilab.org/wiki/IndieCamp#IndieCamp) (2016 à 2022)
+ Les cours en école d'ingénieur⋅e⋅s des Métiers de l'Environnement (2017 à 2020), et UT Compiègne
+ L'action [Rivières Pourpres](https://notesondesign.org/xavier-coadic/) (2019)
+ [Frugal Science](https://www.frugalscience.org/) (2020) du Prakash Lab, Standford
+ Tactical Tech et Exposing The Invisible (2019 à 2022)
+ Bruxelles et Liège, Belgique (2022)
+ Chantier [Reprises des savoirs, Pluri-versités](https://www.reprisesdesavoirs.org/), Rennes, France, octobre 2022
+ [Hack2o](https://ps.zoethical.org/t/hack2o-intro-explication/6116) (2023)

![](/assets/images/liege-biohack-2022.jpg)
<figcaption class="caption">Description d'image : Scène d'un atelier de biohacking dans un lieu « improvisé » en Belgique, extraction d'ADN et initiation aux techniques de bases de microbiologie et de l'enquête environnementale. 2 Personnes se penchent sur un socle en carton plié dans lequel 2 tubes à essais sont le théâtre des manipulations à la main et avec micro-pipette d'ADN extrait précédemment de graines.  Liège, Belgique, 2022, lors de l'occupation temporaire et conventionnée de l'ancienne mairie d’Ougrée sur les bords de La Meuse au pied  des usines abandonnées des hauts fourneaux d'Arcelor Mital</figcaption>

<div class="breaker"></div>
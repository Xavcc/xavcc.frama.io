---
title: "Mardi catastrophologie : votre fournée de ressources (n°3)"
layout: post
date: 2023-04-25 06:40
image: /assets/images/9f79cea6d9ddc88e.png
headerImage: true
tag:
- hsociety
- Catastrophologie
category: blog
author: XavierCoadic
description: Mange des cailloux dans ton compost !
---

Les numéros précédentes : [[1](/mardi-catastrophologie/)], [[2](/mardi-catastrophologie-2/)]

> « Or, à quelle classe appartient un être qui a, certes, l'obligation de travailler, mais n'a ni le droit ni la compétence de porter la responsabilité de son activité ?
À la classe des machines et des pièces de machines » <cite>Günther Anders</cite>, Le rêve des machines, lettre à [Francis Powers](https://fr.wikipedia.org/wiki/Francis_Gary_Powers). 25 août 1960. <https://inventaire.io/items/6b744070509c84a741412350e142f047>

+ [Actu] **France's Interior Minister, Gérald Darmanin, is attacking the League for Human Rights LDH by announcing that it will have to take a close look at its funding.** <https://www.francetvinfo.fr/faits-divers/police/violences-policieres/video-maintien-de-l-ordre-gerald-darmanin-laisse-entendre-que-les-subventions-a-la-ligue-des-droits-de-l-homme-pourraient-etre-remises-en-cause_5753729.html>

(The last time the LDH was attacked to this extent and dissolved was under the Vichy regime which collaborated with the Nazi regime. LDH which has been fighting for human rights since 1898)

+ [Actu] **Gérald Darmanin sème la terreur à Mayotte** <https://www.humanite.fr/societe/mayotte/gerald-darmanin-seme-la-terreur-mayotte-792188>

> « S’appuyant sur l’emprise coloniale française aux Comores, le ministre de l’Intérieur s’apprête à lancer une opération de lutte contre l’immigration révélatrice de ses intentions au niveau national. Des défenseurs des droits l’exhortent à y renoncer. 
>
> Des maisons marquées de croix et de numéros, les sirènes omniprésentes des véhicules de police, les discours de haine et la rumeur… l’île de Mayotte, dans l’archipel des Comores, vit depuis plusieurs semaines dans un climat de terreur. Et ce ne sont là que les prémices de l’opération qu’entend lancer Gérald Darmanin, ministre de l’Intérieur, dès la fin du ramadan, le 22 avril Wuambushu »

+ [Actu] **Wuambushu : débusquer les pauvres et s'en débarrasser** <https://blogs.mediapart.fr/63811/blog/230323/wuambushu-debusquer-les-pauvres-et-sen-debarrasser>
  + **À Mayotte, Darmanin lance une gigantesque opération de lutte contre l’immigration** <https://www.streetpress.com/sujet/1682006813-mayotte-darmanin-lance-gigantesque-operation-lutte-immigration-wuambushu>

+ [Archive] **La catastrophe**, <cite>Adrien Dézamy Dézamy</cite>, Adrien (1844-1891).<https://gallica.bnf.fr/ark:/12148/bpt6k6483224d/f3.item.r=catastrophe>

> « La Catastrophe 
>
> Et c'est l'heure où minuit, brigand mystérieux,
>
> Voilé d'ombre et de pluie, et le front dans la bise,
>
> Prend un pauvre marin frissonnant et le brise
>
> Aux rochers monstrueux apparus brusquement. » <cite>Victor HUGO</cite> (Les Pauvres Gens)

+ [Actu] **Historic Asia heat breaks hundreds of records, with extremes in Thailand and China**, “Numerous April heat records were set in China, while Thailand recorded its all-time hottest temperature” <https://www.washingtonpost.com/weather/2023/04/17/historic-heat-wave-asia-thailand/>

![](/assets/images/heatwave-asia-arpil-2023.png)
<figcaption class="caption"><a href="https://www.wxcharts.com/?panel=default&model=gfs,gfs,gfs,gfs&region=india&chart=2mtemp,850temp,wind10mkph,snowdepth&run=06&step=003&plottype=10&lat=34.067&lon=67.166&skewtstep=0">Severe heatwave engulfs Asia</a> SUnday 23 Arpil 9:00 UTC</figcaption>
<br />

+ [Actu] **Asia's next "airpocalypse"** Seasonal agricultural burning is choking northern Thailand and its neighbors <https://asia.nikkei.com/Print-Edition/Issue-2023-04-20>
  + **Thailand's air pollution crisis deepens amid seasonal crop burning** Smoke from agricultural field and forest blazes chokes Southeast Asia <https://asia.nikkei.com/Spotlight/The-Big-Story/Thailand-s-air-pollution-crisis-deepens-amid-seasonal-crop-burning>
  + **Air pollution: Asia's deadliest public health crisis isn't COVID** As governments fail to curb the world's worst air, millions are dying avoidable deaths <https://asia.nikkei.com/Spotlight/The-Big-Story/Air-pollution-Asia-s-deadliest-public-health-crisis-isn-t-COVID>
<br />

+ [Actu] **Très forte chaleur en Espagne** (semaine du 24/04 au 31/04)

> « L'agence météorologique nationale espagnole confirme qu'une canicule historique va frapper l'Espagne, dont l'agriculture a déjà les deux genoux à terre. 35-40°C sont attendus, en avril.
> + +16°C aux normales saisonnières.
> C'est inédit en Espagne.
> C'est inédit en Europe. »

+ [Publi] **State of the Global Climate 2022 (WMO-No. 1316)** <https://library.wmo.int/index.php?lvl=notice_display&id=22265>

+ [Actu] **Plans for new sites in UK for asylum seekers ‘risk humanitarian catastrophe’**

> "About 170 organisations warn ministers not to put people in military bases, barges and ferries around the country"

<https://www.theguardian.com/world/2023/apr/06/plans-for-new-sites-in-uk-for-asylum-seekers-risk-humanitarian-catastrophe>

+ [Actu] **The 2 child limit for UK benefits is now affecting 1.5 million children, with more than a million of them growing up in poverty, a campaign group has said in a report into what it termed “this nastiest of policies”.** <https://www.theguardian.com/society/2023/apr/06/more-than-1m-children-growing-up-in-poverty-under-two-child-benefits-limit>

+ [Actu] **L’eau potable en France contaminée à vaste échelle par les métabolites du chlorothalonil, un pesticide interdit depuis 2019** <https://www.lemonde.fr/planete/article/2023/04/05/l-eau-potable-en-france-contaminee-a-vaste-echelle-par-les-metabolites-du-chlorothalonil-un-pesticide-interdit-depuis-2019_6168450_3244.html>

+ [Actu] **L'Anses bannit le S-métolachlore et maintient la phosphine pour l'export**

> « Les principaux usages du S-métolachlore, un puissant herbicide polluant les eaux souterraines, vont être interdits en France, tandis que l'insecticide phosphine, crucial pour les exportations vers l'Afrique, est pleinement autorisé, selon des décisions publiées jeudi par l'Agence de sécurité sanitaire (Anses). » <https://www.terre-net.fr/produits-phytos/article/226172/l-anses-bannit-le-s-metolachlore-et-maintient-la-phosphine-pour-l-export>

+ [Actu] **Sécheresse dans les Pyrénées-Orientales : quatre communes privées d’eau potable** <https://www.lemonde.fr/planete/article/2023/04/14/secheresse-dans-les-pyrenees-orientales-quatre-communes-privees-d-eau-potable_6169505_3244.html> Bouleternère, Corbère, Corbère-les-Cabanes, Saint-Michel-de-Llotes

> « La nappe du pliocène est stratégique, il nous faut absolument la préserver pour les prochaines générations, et pour faire face au réchauffement. C'est la pression de cette nappe qui maintient à distance l'eau de la mer. En pompant trop, on l'affaiblit et l'on permet au “biseau salé” d'entrer dans les eaux souterraines. Si l'on en arrive là, nos nappes seront salinisées et c'est un processus irréversible. » , *Sécheresse : plus une goutte d'eau au robinet, le scénario qui inquiète les Pyrénées-Orientales* <https://www.lemonde.fr/planete/article/2023/04/22/secheresse-une-rupture-d-acces-a-l-eau-le-scenario-qui-inquiete-les-pyrenees-orientales_6170629_3244.html> (merci susatari)

+ [Actu] **La Confédération paysanne et le CETIM saisissent l'ONU au sujet des méga-bassines** <https://www.confederationpaysanne.fr/actu.php?id=13388>

+ [Avis (merci Pôle InPACT)] CESE

> « Le Cese propose des préconisations, dont certaines rejoignent celles du Plan Eau, mais qui vont plus loin sur l’accompagnement de la transition écologique en insistant sur plusieurs aspects : le développement de meilleures connaissances sur le cycle de l’eau et sur l’impact des dérèglements climatiques en ce domaine ; l’enjeu d’une sobriété accrue la nécessité d’amplifier les efforts de préservation de la qualité de l’eau, notamment par la lutte contre la pollution ; la nécessité d’investir massivement dans la rénovation des infrastructures de potabilisation et de traitement des eaux, ainsi que de progresser dans le ré-usage. » *[Comment favoriser une gestion durable de l’eau (quantité, qualité, partage) en France face aux changements climatiques ?](https://www.lecese.fr/travaux-publies/comment-favoriser-une-gestion-durable-de-leau-quantite-qualite-partage-en-france-face-aux-changements)*

+ [Actu] **Le Cese appelle à «ne pas subventionner les mégabassines avec de l’argent public »** <https://www.liberation.fr/environnement/climat/le-cese-appelle-a-ne-pas-subventionner-les-megabassines-avec-de-largent-public-20230413_M2ZAQCRWRFHRLDEPBV2DAAHT4A/>

+ [Actu (merci Corinne Morel Darleux)] **Sécheresse : un rapport appelle à un « changement radical » pour éviter de futures catastrophes en eau potable**

> « Sécheresse 2023 : Selon un rapport interministériel, “la période de plus d’un mois sans pluie début 2023” ainsi que l’organisation de la Coupe du monde de rugby à l’automne et les JO de Paris 2024 risquent d'aggraver le risque de rupture d’approvisionnement en eau potable. Ils font allusion à une dérogation obtenue par un club de foot national pour sa pelouse et soulignent que la dérogation qu'ont les golfs pour arroser en période de crise n’est  “pas compréhensible”. » <https://www.lemonde.fr/planete/article/2023/04/08/secheresse-un-rapport-appelle-a-un-changement-radical-pour-eviter-de-futures-catastrophes-en-eau-potable_6168806_3244.html>

+ [Actu] (2023) **European State of the Climate 2022**

> “Last year, #Europe experienced its hottest summer and second warmest year on record, according to the 2022 edition of the Copernicus Climate Change Service’s European State of the Climate report. Globally, the last eight years have been the warmest on record, while in recent decades Europe has warmed faster overall than any other continent.” <https://climate.copernicus.eu/esotc/2022>

![](/assets/images/9f79cea6d9ddc88e.png)

![](/assets/images/80fbbf24249c6367.png)

+ [Retro (merci MMR Nmd)] Dessin de presse de <cite>Mana NEYESTANI</cite><marginnote>Born in Tehran, Iran in 1973, Mana Neyestani trained as an architect in Tehran University, but began his carrer in 1990 as a cartoonist and illustrator for many cultural, political, literary and economic magazine. With the rise of Iranian reformist newspapers in 1999, he became an editorial cartoonist. In 2006, he was sent to jail because of a cartoon. Neyestani is now living in Paris, as a member of ICORN (the International Cities of Refuge Network), <a href="https://www.cartooningforpeace.org/en/dessinateurs/mana-neyestani/">Cartoon For Peace</a>.</marginnote>, artiste iranien, datant de 2021.
Le régime des mollahs avait fait tirer ses miliciens sur des foules qui réclamaient un accès pour tous à l'eau, et ce fut déjà le cas en 2018 [Police Open Fire on Thirsty Crowds](https://iranwire.com/en/features/65385/)
  + [Iran Forces Fire on Water Protesters in Most Violent Crackdown Since July](https://www.voanews.com/a/iranian-police-clash-with-protesters-at-water-shortage-rallies-/6329639.html)
  + [Mass Protest in Isfahan as River Drought and Farming Crisis Continue](https://iranwire.com/en/iran/70813/)
  + En 2008, des personnes ayant manifester pour la préservation de lac Urmia ([Dying Lake Urmia Threatens Human Rights](https://iranwire.com/en/society/60630/)) avaient été condamnées à 6 mois de prison ferme et 20 coups de fouets <https://iranwire.com/en/society/60692/>

![](/assets/images/iran-water-crime.jpg)

+ [Actu] **Accès à l’eau, un défi mondial 2/4 : Fleuves transfrontaliers : une source de conflits**, <cite>Franck Galland</cite>, directeur d’Environmental Emergency & Security Services, chercheur associé à la Fondation pour la Recherche Stratégique.

+ [Actu (merci Jacques Caplat)] **Ce que disent les archives disparues du procès du chlordécone** (France) <https://www.radiofrance.fr/franceinter/podcasts/secrets-d-info/secrets-d-info-du-samedi-22-avril-2023-1179961>
  + **Chlordécone : les scientifiques alertaient sur les risques de cancer depuis les années 80, selon des archives retrouvées** <https://www.francetvinfo.fr/france/martinique/enquete-chlordecone-les-scientifiques-alertaient-sur-les-risques-de-cancer-depuis-les-annees-80-selon-des-archives_5782469.html>

> « Inauguré en 2022, le barrage éthiopien de la Renaissance, sur le Nil, constitue une source de tensions avec les pays en aval, le Soudan et l'Égypte » <https://www.radiofrance.fr/franceculture/podcasts/cultures-monde/fleuves-transfrontaliers-une-source-de-conflits-4504977>

+ [Publi] **Despotisme hydraulique** pré-moderne détaillé notamment dans l’œuvre de <cite>K. Wittfogel</cite>, 977
  + « La grande hydraulique au service du pouvoir : l’exemple du Projet de l’Euphrate en Syrie (1966-2013) », <cite>Roman-Oliver Foy</cite> et <cite>Jack Keilo</cite>
  + « Le Despotisme oriental » de Karl Wittfogel, <cite>Pierre-François Souyri</cite>, dans mensuel 483, mai 2021 « _En 1957 l'historien allemand théorisait la forme du pouvoir inhérent, selon lui, aux sociétés hydrauliques._ » <https://www.lhistoire.fr/classique/%C2%AB-le-despotisme-oriental%C2%A0%C2%BB-de-karl-wittfogel>
<br />

+ [Publi] **Violence Against the Earth Begets Violence Against Women**, *An Analysis of the Correlation between Large Extraction Projects and Missing and Murdered Indigenous Women, and the Laws That Permit the Phenomenon through an International Human Rights Lens* <cite>Blaze Aubrey</cite>, 2019, The Arizona Journal of Environmental Law and Policy, <https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=aubrey_final.pdf>

> “This article looks at the intersection between environmental degradation and violence against indigenous women in extraction zones, focusing on extractive industries in the United States. First, the article traces the history of colonial oppression and cultural genocide imposed upon the American Indigenous population. Then the article presents a series of extraction project case studies. The author critiques current United States law and policy that disregards the ongoing disappearance and murder of Native women. She compares the shortcomings of current United States legal principles to international human rights norms and standards. The author recommends changes to government funding, legislation, and consultation practices with Native communities that better align with international human rights treaties and valorize the rights of Indigenous women.”

+ [Publi] **Crimes environnementaux : si la pollution de l’eau tue… malheureusement elle rapporte !**, <cite>Sylvie Paquero</cite>, 2016 <https://www.erudit.org/fr/revues/crimino/2016-v49-n2-crimino02877/1038423ar/>

+ [Ref] l’acte d’accusation émis par la CPI à l’encontre d’Omar Al Bashir (président du Soudan), pour crime de génocide, repose entre autres sur **la contamination de puits et de pompes à eau dans certaines villes et villages**, (Le Procureur c. Omar Hassan Ahmad Al Bashir, deuxième mandat d’arrêt, 12 juillet 2010,  <https://asp.icc-cpi.int/sites/asp/files/asp_docs/Non-coop/ICC-02-05-01-09-266-FRA.pdf>)

> « Elles auraient, dans l’ensemble de la région du Darfour, à certaines occasions, contaminé les puits et les pompes à eau des villes et des villages principalement peuplés de membres des groupes four, massalit et zaghawa qu’elles attaquaient ; et encouragé des membres d’autres tribus, alliées du Gouvernement soudanais, à s’installer dans les villages et sur les terres
où vivaient précédemment principalement des membres des groupes four, massalit et zaghawa » p. 2 <https://www.icc-cpi.int/sites/default/files/CaseInformationSheets/AlBashirFra.pdf>

+ [Livre] **Anthropologie de l'aide humanitaire et du développement : des pratiques aux savoirs, des savoirs aux pratiques**, <cite>Laurent Vidal</cite>, <cite>Laëtitia Atlani-Duault</cite>,  2009 <https://inventaire.io/entity/isbn:9782200350734>

+ [Ressources] **Investigating Climate Change Adaptation: Methods and Principles** <https://exposingtheinvisible.org/en/workshops/climate-change-adaptation/>, <cite>Tactical Tech / Exposing The Invisible</cite>.

> **No disaster is natural** – a disaster happens when three factors meet:
>
> + **Exposure to a hazard** — such as a storm, a flood, a fire, a heatwave or an earthquake.
> + **Local vulnerabilities** — the physical, social, economic and environmental factors that make a specific context vulnerable to external shocks. In Brazil, for example, favelas, or historically neglected informal neighbourhoods, are often [most impacted](https://rioonwatch.org/?p=51579) by landslides and flash floods, because they are built with low-quality material, on steep slopes and away from hospitals. Similarly, depending on their gender or religion, Bangladeshi farmers are more or [less protected](https://theconversation.com/climate-change-impacts-in-bangladesh-show-how-geography-wealth-and-culture-affect-vulnerability-128207?utm_source=twitter&utm_medium=bylinetwitterbutton) by local policies from climate extremes.
> + **Coping capacities** — the ability or inability to respond to and recover from the effects of stresses and external shocks.

+ [Actu] **L'effondrement du Rana Plaza, symbole des dérives de l'industrie textile, a eu lieu il y a 10 ans** <https://www.rts.ch/info/monde/13963008-leffondrement-du-rana-plaza-symbole-des-derives-de-lindustrie-textile-a-eu-lieu-il-y-a-10-ans.html>

> « Il y a 10 ans, le 24 avril 2013, l'accident le plus meurtrier de l'histoire récente de l'industrie textile se produisant au Bangladesh. L'effondrement de l'immeuble du Rana Plaza faisait 1130 morts et 2500 blessés. La plupart des victimes confectionnaient des vêtements pour les grandes marques occidentales. »

+ [Publi] **Excess mortality attributed to heat and cold: a health impact assessment study in 854 cities in Europe**, <cite>Pierre Masselot</cite>, <cite>Malcolm Mistry</cite>, <cite>Jacopo Vanoli</cite>, </cite>Rochelle Schneider</cite>, <cite>Tamara Iungman</cite>, <cite>David Garcia-Leon</cite>, et al. 16 March 2023 <https://www.thelancet.com/journals/lanplh/article/PIIS2542-5196(23)00023-2/fulltext>

+ [Actu] **La vaccination des enfants, victime indirecte de la pandémie de Covid**

> « 67 millions d’enfants dans le monde ont été privés d’un ou plusieurs vaccins depuis la pandémie de Covid-19, alerte l’#Unicef. En cause, la surcharge des systèmes de santé, les conflits, mais aussi la perte de confiance dans la #vaccination des enfants. En France, ce niveau de confiance chuterait de 11,5%. » <https://www.mediapart.fr/journal/international/200423/la-vaccination-des-enfants-victime-indirecte-de-la-pandemie-de-covid>


+ [Actu] **Les invasions biologiques, aussi coûteuses que les catastrophes naturelles**

> « 1200 milliards de dollars de pertes financières entre 1980 et 2019. Un montant quasiment équivalent aux dégâts provoqués par les tempêtes, ou bien par les tremblements de terre ou encore par les inondations.
> 
> Ces résultats publiés dans Perspectives in Ecology and Conservation ont été obtenus en recensant les coûts de chaque invasion biologique avec le programme de recherche français Invacost.
> 
> Cela va du frelon asiatique qui tuent les pollinisateurs, aux termites qui rongent les bois des infrastructures en passant par les moustiques-tigres vecteurs de maladies parfois mortelles. En France, ces invasions ont coûté entre 1,2 et 10 milliards d’euros.
> 
> Les auteurs indiquent que ces coûts augmentent plus rapidement que ceux induits par les catastrophes naturelles et que les investissements qui permettraient de les prévenir et de les gérer sont dix fois moins élevés que les pertes financières qu’elles engendrent. » <https://www.radiofrance.fr/franceculture/podcasts/le-journal-des-sciences/les-caracteristiques-qui-font-des-oiseaux-les-victimes-ou-les-bourreaux-des-invasions-biologiques-5131216>

Ailleurs :

> « Les espèces invasives représentent la seconde cause de dégradation de la biodiversité. Les pertes induites en Europe sont estimées à 12 milliards d’euros par an. » Journal CNRS, 2020 <https://lejournal.cnrs.fr/nos-blogs/dialogues-economiques/especes-invasives-organisons-la-lutte>

+ [Publi] **La lutte contre l’ambroisie. Gestion biosécuritaire d’une espèce invasive**, <cite>Clarine Julienne</cite>, Dans Revue d'anthropologie des connaissances 2018/3 (Vol. 12, N°3). <https://www.cairn.info/revue-anthropologie-des-connaissances-2018-3-page-455.htm>

+ [Actu] **La réglementation des navires transocéaniques a réduit l'introduction d'espèces invasives dans les Grands Lacs** <cite>Anthony Ricciardi</cite> <https://theconversation.com/la-reglementation-des-navires-transoceaniques-a-reduit-lintroduction-despeces-invasives-dans-les-grands-lacs-192134>

+ [Podcast] **Qui sème le vivant récolte l’urticaire** <https://www.radioescapades.org/qui-seme-le-vent-recolte-lurticaire/>

> « parle d’écologie en collant bord à bord des bouts d’histoires. Des histoires de végétaux, de champignons, d’insectes, de vertébrés et de toutes les petites bêtes du monde microscopique. Au fil des récits des trajectoires de nos compagnons non humains vivant, proliférant ou menacés qui s’entremêlent à nos mondes d’humains, l’émission explore les marges de notre monde civilisé, ses coulisses et ses artères vitales.
>
> En s’inspirant librement des méthodes de l’ethnobiologie et de l’histoire environnementale, chaque épisode s’intéresse à un objet différent et invite dans le studio des éléments d’enquête. »

+ [Actu] **Sudan ‘resistance’ activists mobilise as crisis escalates** <https://www.aljazeera.com/news/2023/4/22/sudan-resistance-activists-mobilise-as-crisis-escalates>

> “This article details how grassroots mutual aid groups in Sudan have provided supplies to hospitals, coordinated evacuations for besieged civilians, and endeavored to prevent civilians from being swept up in the civil war.“

+ [Publi] **Anti-Crisis : penser avec et contre les crises ?**, Entretien avec <cite>Janet Roitman</cite>, Traduction et notes de <cite>Sara Angeli Aguiton</cite>, <cite>Lydie Cabane</cite>, <cite>Lise Cornilleau</cite> <https://www.cairn.info/revue-critique-internationale-2019-4-page-107.htm?ref=doi> . Dans Critique internationale 2019/4 (N° 85), pages 107 à 121

+ [Publi] **L’Afrique, autrement. Dépasser la crise**, <cite>Janet Roitman</cite> <https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=mult_069_0173.pdf> Dans Multitudes 2017/4 (n° 69)2017/4 (n° 69), pages 173 à 179

+ [Archive] **Recipes for Disaster: an anarchist cookbook**, by CrimethInc , 2004, <https://archive.org/details/RecipesForDisasterAnAnarchistCookbook/mode/2up>

+ [Publi] **La dimension environnementale des inégalités sociales**, <cite>Élias Burgel</cite> <https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=etudesrurales-11797.pdf>, <cite>Catherine Larrère</cite> (dir.), Les Inégalités environnementales, Paris, Presses universitaires de France (« La Vie des idées »), 2017, 104 p.

+ [Publi] **Comment faire l’anthropologie d’un objet « trop lourd »?**, Catherine Neveu, 2010, <https://www.erudit.org/fr/revues/as/2009-v33-n2-as3641/039296ar/>

> « […] l’article s’intéresse à deux enjeux centraux : celui de la « question nationale » et de ce que Rosaldo appelle « cultural citizenship » ; et celui des processus de territorialisation et des échelles de la citoyenneté. »

+ [Ressource] **Épsitémologie et exercices de L'anthropologie**, CNRS, <https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=projet_14-18_axe1.pdf>

> « Les nanoparticules manufacturées, par exemple, ne sont que partiellement prises en compte dans les réglementations existantes des produits chimiques, et les incertitudes sur leurs risques potentiels sont directement liées aux difficultés rencontrées pour assurer une caractérisation physico-chimique univoque. De même pour les nucléides qui, au sortir d’une catastrophe nucléaire, semblent offrir de la résistance aux programmes de mesures et partant, aux efforts pour qualifier la contamination et définir l’échelle de l’événement. C’est précisément cet aspect qu’abordera S. Houdart dans le projet qu’elle entame sur la gestion, au Japon, de ce qu’on appelle aujourd’hui le « post-Fukushima », en partenariat avec la Fondation 93 (F93), Centre de Culture Scientifique Technique et Industrielle de la Seine-Saint-Denis. L’entrée par la question de l’échelle lui permettra de saisir ensemble les opérations de gestion du microscopique (les contaminants dans l’espace public) et l’appréhension des effets macroscopiques, les modélisations de leur diffusion à l’échelle de la planète, qui viennent croiser d’autres phénomènes macro, comme les séismes ou les transformations atmosphériques. »

+ [Chronique] **Que met-on derrière le mot de "Nature" ?** <https://www.radiofrance.fr/franceculture/podcasts/le-pourquoi-du-comment-science/que-met-on-derriere-le-mot-de-nature-9346750>

> « Très souvent, on pense que “nature” et “naturel” c’est bon, par opposition au “chimique”, “artificiel”. Un traitement “naturel” fera forcément plus de bien qu’un traitement “chimique”. C’est oublier que dans la nature, il y a plein de choses pas cool : les plantes vénéneuses, les cancers, les autres maladies… Et puis ne faisons-nous pas partie de la nature ? » (merci Eorn)


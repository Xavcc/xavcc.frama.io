---
title: "Pas Sage En Seine 2019 : Biopanique, Cuisine et Streethack"
layout: post
date: 2019-07-02 15:40
image: /assets/images/seed_lab.jpeg
headerImage: true
tag:
- hsociety
- Paris
- Biononymous
- Bioprivacy
category: blog
author: XavierCoadic
description: Quelques ressources...
---

Du 27 au 30 juin 2019, j'ai participé au festival [Pas Sage en Seine](https://passageenseine.fr), qui se déroulait dans la médiathèque de Chois-Le-Roy, avec notamment une conférence « Biopanique, Cuisine et Streethack » et un atelier « Cuisine des pirates : Bactéries et levures contre g00gle »

Je m'étais engagé auprès des personnes présentes à publier sous licence libre des contenus utilisés, de recettes et de ressources employées ou évoquées.

Voici les éléments.

#### Table des contenus
1. [Préambule](#préambule)
2. [Texte en introduction](#texte-en-introduction)
3. [Ressources pour atelier](#ressources-pour-atelier)
  + 3.1 [Réseaux Bio](#réseaux-bio)
  + 3.3 [Matériel pour biobricoler](#matériel-pour-biobricoler)
  + 3.3 [Faire sa cuisine](#faire-sa-cuisine)
  + 3.4 [Autodéfense bionumérique](#autodéfense-bionumerique)
4. [Remerciements](#remerciements)
5. [Notes et références](#notes-et-références)

## Préambule

Pour esquisser une géodésie (arpentage de la Terre à partager) de ce que nous allons ensemble partager dès maintenant, je voudrais commencer par une liste de dates avec les évènements associés : 

**1953** : Découverte du rôle des molécules de l'ADN, "[Molecular Structure of Nucleid Acids : a Structure for DNA](https://www.nature.com/articles/171737a0)"

**Années 70** : Nous développons les capacités de modifier le génome. Dans le même temps, Dans les années 1970, en formalisant le modèle des grammaires, ce sont des linguistes et des mathématicien.ne.s qui ont collaboré pour améliorer les langages informatiques[^1]. Les interprètes ou les compilateurs sont alors constitués par une chaine de traitements : analyse lexicale, son syntaxique, son sémantique. Les travaux de Noam Chomsky[^2] ont fait l’hypothèse d’une similitude entre les langues naturelles et les langages informatiques.

**1978** : Après le projet SAFARI, la 1<sup>ère</sup> loi informatique et liberté est promulguée en France

**1981** : Dougal Dixon publie "[After Man](https://en.wikipedia.org/wiki/After_Man)", un ouvrage de biologie de l'évolution spéculative, ou zoofiction, avec des questionnements sur le parasitisme, la symbiose, l'hybridation, dans un monde advenu sans humain.

**1<sup>ère</sup> moitié des années 80** : La notion de logiciel libre est décrite pour la première fois dans la première moitié des années 1980 par Richard Stallman qui l’a ensuite formalisée et popularisée avec le projet GNU (GNU signifiant “Gnu is Not Unix”) et avec la Free Software Foundation (FSF)

**1986** : Première version par la FSF des [4 libertés](https://fr.wikipedia.org/wiki/Logiciel_libre#D%C3%A9finition_de_la_Free_Software_Foundation_(FSF)) liées au logiciel libre

**1987** : Une [« erreur » de manipulation](https://www.ncbi.nlm.nih.gov/pubmed/3316184) devient le premier pas vers une technologie de génie génétique

**Fin des années** : l'[Office européen des brevets (OEB)](https://fr.wikipedia.org/wiki/Office_europ%C3%A9en_des_brevets) travail sur la piraterie du vivant.

**Début 90** : L'arrivée des [premiers instruments législatifs](https://www.wipo.int/tk/en/databases/tklaws/) comte la piraterie du vivant

**1990** : Dougal Dixon publie Man After Man, explore un hypothétique cheminement futur de l'évolution humaine, de 200 ans dans le futur à 5 millions d'années dans le futur, avec plusieurs futures espèces humaines évoluant par le génie génétique et des moyens « naturels »

**1993** : [Formalisation scientifique](https://www.broadinstitute.org/what-broad/areas-focus/project-spotlight/crispr-timeline) de ce qui deviendra ensuite CRISPR

**2002** : Prix Nobel de médecine à l'équipe qui a décodé le génome humain, le 10 décembre à Sydney

**2001** : Francisco Mojica et Ruud Jansen rendent l'idée opérable et reproductible d'une nouvelle ingénierie du génome : [CRISPR](https://en.wikipedia.org/wiki/CRISPR)

**2003** : Suite au prix Noble de médecine et face aux [pressions de privatisation et de brevetisation](https://www.monde-diplomatique.fr/2002/12/SULSTON/9777), le génome humain est placé dans le domaine public.

**7 août 2004** : [Loi de procédure judiciaire](https://www.legifrance.gouv.fr/affichCode.do;jsessionid=BA632435040AA3FD8317FACB6718520C.tplgfr27s_2?idSectionTA=LEGISCTA000006165397&cidTexte=LEGITEXT000006070719&dateTexte=20181220) (Code de la Santé Publique), reposant sur la loi informatique et liberté, amenant en France la condamnation à 3 750€ si vous commandez un test ADN.

**2007** : Les signataires des conventions sur la piraterie du vivant s'assoient sur leurs engagements, ne respectent pas la Déclaration des Nations unies sur les droits des Peuples autochtones (DNUDPA) de 2007 pour s'[approprier la Quinine de Cayenne](https://theconversation.com/debat-la-biopiraterie-ou-le-vol-des-savoirs-ancestraux-92780).

**2010** : [Projet de loi Française](https://www.senat.fr/leg/ppl09-720.html) (Présidence du Sénat) proposait d'interdire ou de taxer le fait de ressemer ses propres graines depuis sa propre récolte.

**2011** : [Adoption de cette loi](https://www.assemblee-nationale.fr/13/ta/ta0775.asp)

**2012** : Merediht Patterson déclame le "[Biopunk Manifesto](https://vimeo.com/18201825)"[^3]


## Texte en introduction

> « _De la cuisine libre ta vie privée tu protègeras. Les identités biologiques nous en héritons, des traces bio nous en laissons partout, nos caractéristiques morphologiques sont capturées. Face, œil, empreintes, veines de la main, la marche, l’ADN... Et biome bactérien. Vous êtes plus de bactéries que vous n’en êtes humain... Humanités bactériennes. Fichages d’État, collectes de données privatisées et compréhension de leurs valeurs de contrôle social, persistances des données ; brevetabilité et imprescriptibilité du vivant. Viens on y fout le bordel du serveur à la rue !_ » [Pitch du programme Pas Sage En Seine](https://programme.passageenseine.fr)

![](https://pixelfed.social/storage/m/2f379dea1475aae71ee95f89a049885298819585/84138976bd0c83fbc4e555c25b01217e9455d976/5djVXRdXtlC9I0iQKLcdpm1YoLBOQnIxj4TGDC2o.jpeg)

<figcaption class="caption"> NDLR : La structure de ce texte déclamé en introduction de la conférence est un remix de la structure du texte de l'émission Bits d'Arte : « L'omniprésence de la Culture Pop la rend-elle "invisible" ? »</figcaption>

<div class="breaker"></div>

L'idée de départ pour « Biopanique, Cuisine et Streethack » est née avec l'intention de désenclaver des approches de nos intimités morphologiques, physiologiques et biologiques.

Il y a un désir qui accompagne cette intention que je pose là dans un espace-temps public :

> **Emparez-vous en et documentons ce qui est, ce que nous en faisons ou ce que nous allons en faire**

Il s'agit de monter en quoi nos comportements dans et avec ces intimités se nourrissent, et nourrissent, nos socialités, qui elles sont plus visibles que ces intimités. La quotidienneté de ces intimités, et leur omniprésence dans nos rapports les unes et les uns envers les autres, peuvent avoir pour effet de les rendre invisibles, ou tout du moins difficilement discernables.

Presque tout le monde se laisse filmer ou photographier, parfois avec des filtres Facebook, parfois pour des raisons ludiques. Beaucoup trouveront étrange voire dangereuse l'idée de l'extraction et du prélèvement de 70 points de morphologie sur les visages des enfants à l'entrée d'une école.

Ceci est pourtant une question en profondeur sur nous-même et sur le monde que nous voulons habiter.

C'est un piège sémantique de l'intimité. `Intimus` : ce qui est dedans. Qui caractérise ce qui touche l'essence réelle, ce qui est profond. Piège qui nous freine à comprendre ce qui est pourtant essentiel : **l'éminence collective de nos intimités**.

Or, des _sub_-cultures telles que Cyberpunk, Biopunk, [Gynepunk](https://hackteria.org/wiki/GynePUNK), [Exofeminism](http://laboriacuboniks.net/), DiyBio, ont montré leurs caractères contre-culturel en refusant un endettement de l'intimité individuée face aux enjeux collectifs des intimités.

En considérant que des choses insignifiées comme nos traits de visage, notre démarche, nos fluides corporels, nos [humanités bactériennes](https://www.academia.edu/5103178/Biohistory_A_Primer_in_the_Bacterial_Humanity), ou encore notre ADN, en considérant que ces choses pouvaient être des éléments qui disent et transcrivent de nous, soient des trésors de nos humanités.

Cette tension entre intimités des individus et intimités collectives est primordiale.

En 2016, lorsque j'ai participé à concevoir et animer un 1<sup>er</sup> atelier « Biopanique, Cuisine et féminisme », des petites manipulations d'extracttion d'ADN dans la cuisine avec les produits domestiques disponibles pour décortiquer des questions de société...

En 2016, la reconnaissance faciale était ludique depuis l'arrivée plus ancienne de la [Kinect](https://fr.wikipedia.org/wiki/Kinect), et [23Andme](https://fr.wikipedia.org/wiki/23andMe), qui existait déjà depuis 10 ans, était déjà dans une expansion marketing du test génétique récréatif.

Aujourd'hui out discours politique et communications d'entreprises reposent sur une dilution de nos intimités dans un imaginaire collectif de cocon terriblement normé, froidement totalitaire, ouvrant d'immenses trous de vers à l'immixtion dans nos vies privées.

En fait, de 2002 à 2016 en France, le nombre de personnes fichées au [Fichier National Automatisé des Empreintes Génétiques](https://fr.wikipedia.org/wiki/Fichier_national_automatis%C3%A9_des_empreintes_g%C3%A9n%C3%A9tiques) (FNAEG créé en 1998) est passé de 4 369 personnes à près de 3,5 millions. Soit presque 5% de la population française.

Le [28 mai 2019](http://hudoc.echr.coe.int/fre?i=001-194229)[^4], la France pour s'éviter une nouvelle condamnation[^5] par la Cours Européenne des Droits Humains (CEDH) à propros de l'usage abusif du FNAEG et du Fichier Automatisé des Empreintes Digitales (FAED) reconnait avoir violé le droit à la vie privée et s'engage à verser 2 700€ par requérant⋅e.

Cependant, une étude de 2018, "[Statistical Detection of Relative Typed With Disjoiint Forensic and Biomedical Loci](https://www.cell.com/cell/fulltext/S0092-8674(18)31180-2)", notamment correlée avec celle "[Indentity Inference Of Genomic Data using Long-range Familial Searches](https://science.sciencemag.org/content/362/6415/690)", permet d'envisager que le fichage génétique de seulement 
2% d'un population au sein d'un État puisse mener à l'identification de toute personne cousine au 3<sup>ème</sup> degré ou plus proche.

Donc l'ensemble d'une population identifiable avec seulement 2% dans un fichier national, du moins potentiellement.

Et aujourd'hui 23AndMe, très [liée à google](https://no-google.frama.wiki/g00gle:google-et-genetique), qui [s'allie avec AirBnb](https://www.airbnb.fr/b/heritagetravel) pour « des séjours à nulle autre pareil grâce au tourisme _géo-génétique_[^6]. Le firmament de la collitude de la dissolution des intimités est atteint. Test de grossesse connectés, sextoys, pseudo-serrure à empreinte digitale, connectés, parfois même au téléphone portable...

Cela signifie t'il que nos intimités morpho-physio-biologiques soient reconnues, visibles, et considérées par la société ?

Sont-elles devenues des trésors à préserver ?

Sont-elles de nouveaux gisements de matériaux à exploiter ? 

Alors, comment parler de nos intimités et des enjeux associés sans sombrer dans l'enfermement et l'isolationnisme ? Comment cela s'applique aussi au vivant non-humain ?

Je suis partisan de voir, par exemple, une linguistique en jeu dans la biopolitique. De voir, par exemple, des stratégies et des opérations de détournement en paysage rural et urbain ; de voir, par exemple, une phénoménologie dans les fictions qui traitent de la confiscation du rapport à soi et à autrui ; de voir de l'ingénierie et de la documentation dans les pratiques de la cuisine.

L'un des défis me semble se situer sur _comment_ parler et reprendre main sur nos intimités avec les enjeux pointus de société sans trahir ce qui est la force de ces intimités, leur accessibilité au plus grand nombre puisque nous avons toutes et tous des intimités.

Il reste d'obscurs mécanismes par lesquels nous sommes mis⋅e⋅s en morceaux symboliques, désassemblé⋅es  pièce par pièce de nos intimités. Nos pas avec mutilation physique mais une forme suave, naïve, dans le but de nous fabriquer. Nous (re)fabriquer tels des individus prêts à être fiché⋅e⋅s et à ficher autrui pour être contrôlé⋅e⋅s.

En contre-partie, nous n'avons jamais eu à portée de main autant de savoirs, de techniques, et de technologies, abordables pour nous réapproprier nos cors et intimités. de la prothèse de corps par imprimante 3D domestique, au kit de DIY Biology et DIY Gynécologie.

Enfin, il y a le danger de circonscription.

À chaque effort de définir nos corps et nos intimités, elles prennent d'autres sens dans nos rapports humains sociaux et sociétaux. Elles n'ont de cesse de nous échapper.

Nous pouvons nous regarder plus bactérien qu'humain ; que se passe t'il alors si mon humanité bactérienne est fichée ou privatisée pour raison de sécurité ? Nous pouvons regarder notre ADN personnel comme un don de nos ancêtres et un leg aux suivants tout en considérant les caractères profondément intime qu'il contient. Nous avons à regarder les intimités morpho-physio-biologiques dans leurs dimensions collectives, y compris du non-humain, pour protéger nos vies privées.

Nous avons des morceaux de nous à nous réapproprier et réagencer politiquement ainsi que des unités d'informations à récupérer. Entre allégorie des Legos et mémétique, nous trouverons l'hardware, le software, et le [wetware](https://openwetware.org/wiki/Main_Page), qui viennent jouer un rôle dans la manipulation des processus des intimités dont nous les humains traitons la matière.

> **Pour ce faire, nous avons besoin de cultures libres et populaires**

<figcaption class="caption">S'en suivit 30 minutes d'échanges avec le public de la conférence. La vidéo sera publiée par l'équipede Pas Sage En Seine.</figcaption>

<div class="breaker"></div>

## Ressources pour atelier

> « _**Cuisine des pirates : Bactéries et levures contre g00gle**_ »

> « _Après les idiot⋅e⋅s du Village g00gle, voici le temps de la flibuste arrivé ! Faire des levures de boulangeries à partir de pomme de terre, oui oui oui. Tu pourras apprivoiser les Saccharomyces cerevisiae pour faire des recettes libres #Fuckoffgoogle sous AGPL pour nourrir les troupes. Du papier à partir de levures et de bactéries ? Oui, oui, oui. Tu pourras envoyer des courriers (gravé au laser ?) #fuckoffgoogle de demande de paiement de leurs amendes ou impôts ou rappel du RGPD. OU alors tu boiras du Kombucha_ » [Pitch du progamme Pas Sage En Seine](https://programme.passageenseine.fr)

![](https://pixelfed.social/storage/m/2f379dea1475aae71ee95f89a049885298819585/84138976bd0c83fbc4e555c25b01217e9455d976/jrYspsHqeV3FTx4vJdehLNFtc3whUK8xhXQP9TdG.jpeg)

<figcaption class="caption"> Anecdote : Un personne aux cheveux longs venant assister au festival avait trop chaud. Elle a pu attacher ses cheveux une fois tressés avec une petite sangle réalisée au préalable en cellulose à partir d'un symbiote bactéries et levures (SCOBY) présent à l'atelier</figcaption>

<div class="breaker"></div>

### Réseaux Bio

+ [Hackteria](https://www.hackteria.org/wiki/Main_Page), Open Source Biological Art, DIY Biology, Generic Laboratory Infrastructure
+ [DIYBio](https://diybio.org) réseau et annuaires de la biologie à faire soi-même
+ [Syntechbio](https://www.syntechbio.com/tools), faire son biohackerspace soi-même en version française, anglaise, portugaise, espagnole.
+ Liste de discussion des BiohackLab européens : majordomo@biohacklabs.org

### Matériel pour biobricoler

+ [Apprendre à faire et s'équiper en wetware](https://openwetware.org/wiki/Main_Page)
+ [Incubateur lowtech](https://wiki.breizh-entropy.org/wiki/Incubateur_lowtech)
+ [Bioimpression 3d](https://wiki.breizh-entropy.org/wiki/Bioimprimante_3d)
+ [Open-source scientific hardware collections et ressources](http://www.appropedia.org/Open-source_Lab#Examples)
+ [DropBot Digital Microfluidics](http://microfluidics.utoronto.ca/trac/dropbot)
+ [Arduino PCR (thermal Cycler) for Under $85](https://www.instructables.com/id/Arduino-PCR-thermal-cycler-for-under-85/)
+ [Fabriquer son PCR](http://openpcr.org/#build-pcr)
+ [Open source centrifugeuse](https://www.instructables.com/id/OpenFuge/)
+ [DIY agitator](http://www.instructables.com/id/CD-ROM-Agitator/)
+ Bioprinter : 
  + <http://hackteria.org/wiki/index.php/HackteriaLab_2011_Commons#Micro_Manipulator>
  + <http://hackteria.org/wiki/index.php/DIY_Micro_Dispensing_and_Bio_Printing>
  + <http://hackteria.org/wiki/index.php/DIY_Micro_Laser_Cutter>
  + <http://hackteria.org/wiki/index.php/DIY_Microfluidics#Advanced_DIY_Microfluidics>
  + <http://hackteria.org/?p=1186>
  + <http://diybio.org/2012/06/12/gaudilabalgaepicker>
+ Les kits prêts à acheter pour le génie génétique sur [Odin](https://www.the-odin.com)

### Faire sa cuisine

+ Bière et fleur
  + Note n°1 : [Capturer et cultiver des levures sauvages pour la bière](https://notecc.frama.wiki/norae:biologicus:biomimetisme_note-culture-1)
  + Note n°2 : [Cultiver ses propres levures de boulangerie](https://notecc.frama.wiki/norae:biologicus:biomimetisme_note-culture-2)
  + Note n°3 : [medium de culture pour levures sauvages à destination de bièrification](https://notecc.frama.wiki/norae:biologicus:note-cutlure-levures-bieres)
+ [Collecte et extraction d'ADN](https://notecc.frama.wiki/norae:biologicus:bioprivacy_note-autodefense-1)
+ [Reconnaissance de visage et 68 points de structures faciales sur le visage](https://notecc.frama.wiki/norae:biologicus:bioprivacy_note-morpho-face-2)
+ [Récupérer les levures de pomme de terre pour faire de la boulangerie](https://wiki.breizh-entropy.org/wiki/Levures_DIY)
+ Faire de [la cellulose à partir de symbiote bactéries et levures](https://github.com/OpenBioFabrics)
+ [Protocoles pour prélèvements et analyses ADN quantitatives dans les sols, par le groupe genosol INRA Dijon](https://www2.dijon.inra.fr/plateforme_genosol/protocoles)
+ [Raspberry et décodage du génome de framboise](https://www.inria.fr/centre/rennes/actualites/le-genome-de-la-framboise-sur-raspberry-pi) +  [Genome Analysis Toolbox with de-Bruijn graph (GATB)](https://gatb.inria.fr/) + [Minia](https://wiki.gacrc.uga.edu/wiki/Minia)

### Autodéfense bionumérique

+ [Révéler l'invisible](https://kit.exposingtheinvisible.org/) (la version française arrive, j'y travaille `(^_^)` )
+ [GynePunk stuff](https://hackteria.org/wiki/Category:GynePunk)
+ [Notes sur les analyses DIY sur OGM](https://notecc.frama.wiki/norae:biologicus:biohacking_note-protocole-analyses-gmo-1)
+ [Brouiller des traces biologiques ?](https://notecc.frama.wiki/norae:biologicus:bioprivacy_note-autodefense-3)
+ [Note pour la conception d'un atelier sur la « reconnaissance faciale »](https://notecc.frama.wiki/norae:biologicus:bioprivacy_note-morpho-face-2)
  + [rencontre et atelier sur la reconnaissance faciale et morphologique de masse.](/fdln-reconnaissance-faciale/)
+ [boulette de graines](https://fr.wikipedia.org/wiki/Boulette_de_graines)
+ You can now download the protocol fo [food DNA extraction + sequencing](https://www.foodrepo.org/dna) : separation of amplified DNA using magnets 

## Remerciements

+ Toutes les personnes de l'équipe PSES 2019
+ Alex de la médiathèque
+ Shaft
+ TkPx
+ Anachitect
+ Audrey Bartis
+ Liandri
+ Hans Bodart, qui a co-animé l'atelier et les discussions (^_^)
+ Monsieur Tesla
+ L'équipe Pépin
+ Will T pour [le bogue](https://framagit.org/Xavcc/xavcc.frama.io/issues/24)

## Notes et références

[^1]: André Lentin <https://fr.wikipedia.org/wiki/Andr%C3%A9_Lentin>
[^2]: Noam Chomsky, La Linguistique cartésienne suivi de La Nature formelle du langage, Éditions du Seuil, 1969, (ISBN 2-02-002732-1)
[^3]: Le Biopunk Manifesto était prérédigé depuis 2010 <https://blog.9while9.com/manifesto-anthology/2010.html>
[^4]: Requête no 62196/14 − Céline BERTRAND et autres contre la France
[^5]: Comme en 2017 <https://www.lemonde.fr/europe/article/2017/06/22/la-france-epinglee-par-la-cedh-pour-fichage-abusif_5149476_3214.html>
[^6]: Remember the recent combo "Go on an *Airbnb vacation* with an experience like never before by using *your DNA via 23andMe* to have fun like a spécial one"? − Surprise, the Guardian warns against a "dangerous flirtation with race". <https://www.theguardian.com/commentisfree/2019/jun/04/dna-based-holidays-encourage-a-dangerous-flirtation-with-race>

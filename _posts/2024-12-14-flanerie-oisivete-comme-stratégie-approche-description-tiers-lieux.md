---
title: "La flânerie et l'oisiveté pour embarquer en configuration tiers-lieux, et les décrire…"
layout: post
date: 2025-12-14 07:40
image: /assets/images/hotel-pasteur-dec-2022.jpg
headerImage: true
tag:
- hsociety
- Tiers-Lieux
category: blog
author: XavierCoadic
description: ""
---

Je n'avais de matelas que le sol. J'ai été pressé jusqu'à être l'huile de chevilles des ouvrages dans des roues violentes. Mon état était celui de « l'art de rue »[^ffamily], coup de poin et « nique tout » et qui dans le mal le capitalisme trouve son compte[^compte].

[^compte]:Europes, Russies. Comme de la monarchie en même teps que la capitalisme, *Des révoltes aux révolutions Euprope, Russie, Amérique. (1770-1802), Essai d'interprétation. <cite>S. Blanchi</cite>, Presses Univervister de Rennes (2004)

[^ffamily]: <cite>Fonky Family</cite> - Hors-Serie Vol.1 (*1999*) (EP) ; puis : Live à Marseille, Espace Julien <cite>Fonky Family</cite> *(live 2015)*

> « L'oisiveté la mère de tous les vices, conduit à l'opprobre et à l'échafaud; elle engendre la misère et la maladie pour quelques jouissances passagères que procurent des aumônes extorquées à la commisération publique », (*Le livre de l'ouvrier*, <cite>Adrien Egron</cite> (1844))

<marginnote>Depuis plus d'un douzaines d'années je suis entré dans plusieurs dizaines de « Tiers-Lieux », et leur configuration. J'ai été reçu, accepté parfois, ou *remis dehors* certaines fois ou non-permis. J'ai contribué à différents niveaux et de différentes manières, j'ai participé sur plusieurs centaines de documentations. J'ai tenu la *conciergerie* de certains lieux et animé des *hackatons* en dormant dans la rue car en grande précarité. J'ai mendié. J'ai squatté, j'ai reçu l'hospitalité et générosités de très nombreuses fois. J'ai croisé et vécu avec des personnes formidables, et même plus que cela. J'ai donné des cours, fais des conférences et des biennales, pris des notes partagé, deses en direct depuis des soutenances de thèses, fait des ateliers, des chantiers… <b>L'énumération foutraque de vécus et de souvenirs ici ne vaut pas d'autre place qu'une timide mise de côté, et n'est pas un argument d'autorité</b><img class="image" src="/assets/images/tkkrlabnl.jpg"
    width="300" 
     height="400" />
</marginnote>

J'exposerais ici une stratégie, parmi d'autres possibles et pas nouvelle, ainsi qu'une posture pouvant être déployées à des fins d'observation et de description *in situ*, notamment pour ce qui est d'écrire et illustrer des configurations en tiers-lieux. J'en écris des ébauches restreintes appuyées d'expériences et de vécus, avec parfois les documentations qui y ont été produites, et des références bibliographiques. Je les placerais ensuite dans un exemple de « mise en œuvre sur le terrain ». L'ensemble ici partagé étant souhaité également comme vecteur de discussions vivaces.

Je fais le choix, jugement ??? (<cite>Husserl</cite> (1929)), de prendre la flânerie et l’oisiveté, et leurs articulations possibles, comme axe d'action pratique, du moins dans certains cas, avec incidence transformatrice. Cette direction a ses contradictions, et aura encore, et surtout vient indéniablement mettre en tension des rapports de forces et des dominations[^mercantilistes]. Cette divergence, quasi constante, pourra elle aussi être vécue, observée et décrite, donc non pas empécher l'étude et bien apporter un angle saillant sur des caractères du groupe et des formes regardées.

[^mercantilistes]: « Lutter contre l'oisiveté des pauvres et aiguiser leur convoitise : les préconisations développementalistes des économistes mercantilistes et classiques », <cite>Alain Clément</cite>,  Revue Tiers Monde (2006) <https://www.cairn.info/revue-tiers-monde-2006-1-page-183.htm>

> « […] non le doute désœuvré de l'oisif, mais le doute prudent et réfléchi de celui qui sait voir […] » Cypselus, Agrégé d'Université, dans *[La revue Anarchiste. Article : La lumière qui tue](https://gallica.bnf.fr/ark:/12148/bpt6k96216199/f9.item)* (Mai Juin 1923)

Le don et l'économie du don (<cite>M. Mauss</cite>, <cite>P. Chanial</cite>),ainsi que le temps dans son acception sociologique (<cite>N. Elias</cite>) sont des composantes structurelles de cette approche par la flânerie et l'oisiveté dans les pratiques de *l'étude embarquée*.

Je vais procéder à une opération de *démontage* afin de me placer par la suite dans une auto-réflexivité, au sens principiel, soit uun *être-impliqué*, ajusté de la conscience de l'*expérience* qui livre l'expérience, dirigé vers des modalités de notre implication radicale dans un positivisme dégagé de sa naiveté et de ses penchants conquérants et coloniaux[^praxis].

[^praxis]: *Décoloniser la recherche, pratiquer la recherche en contexte de décolonisation : retours d’expérience sur la promotion d’une école plurilingue. nouvelle-Calédonie, Polynésie Française, 2002-2012* Marie Salaün et Jacques Vernaudon, dans « Terrains océaniens : enjeux et méthodes », *Sous la direction de Véronique Fillol et Pierre-Yves Le Meur*, Actes du 24e Colloque CORAIL - 2012 <https://horizon.documentation.ird.fr/exl-doc/pleins_textes/divers16-05/010064301.pdf>

**Table des contenus**

+ [Considération préliminaires](#considérations-préliminaires)
  + [Honnêteté et clarification](#honnêteté-et-clarification)
  + [Proximité avec le « Passager Clandestin](#proximité-avec-le-«-passager-clandestin-»)
+ [Flânerie](#flânerie)
+ [L'oisiveté](#loisiveté)
+ [Notes et références](#notes-et-références)

![](/assets/images/hotel-pasteur-dec-2022.jpg)
<figcaption class="caption">Photographie de la cage d’escalier de l'Hôtel Pasteur, décembre 2022. « Prends le temps de ne pas avoir de temps », aérosol du mur de cage d'escalier sous néon d’éclairage incliné. Xavier Coadic, sous Licence CC BY SA 4.0</figcaption>
<div class="breaker"></div>


## Considérations préliminaires

### Honnêteté et clarification

Une stratégie n'est pas neutre, ni son embrayage et débrayage énonciatif (<cite>Émile Benveniste</cite>), l'éthique énonciative m'oblige à rappeler cette évidence. J'établie mon opération depuis un système duquel je suis déjà en marge. « [Habermas au Starbucks. Clients, oisifs et traînards dans le tiers-lieu capitaliste](https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=habermas_au_starbucks_clients_oisifs_et.pdf) » (<cite>Robin Wagner-Pacifici</cite> (2021)). 

Je porte mon propos ici sur la mise en capacité d'un⋅e observateurice, embarqué⋅e avec des degrés variables de contribution endogènes et exogènes, qui entre en relation avec des individus engagé⋅e⋅ « intentionnellement à la conception d’une représentation commune, c’est-à-dire à responsabilité partagée » (<cite>A. Burret</cite> (2017)), au sein de moments, de lieux, et d'espaces particuliers avec des contextes spécifiques à chaque *forme*, sans lui attribuer une imposition de devenir. Je porte aussi un rappel sur les question du *terrain de recherche*, certes affaires de définition géographique, dont la « localisation ne veut pas dire enfermement ou confusion entre le lieu et l’objet » (<cite>Véronique Fillol et Pierre-Yves Le Meur</cite> (2014))[^terrain], avec une configuration en tiers-lieux qui ne devrait pas être prise comme les « nouvelles îles exotiques ».

[^terrain]: « Terrains océaniens : enjeux et méthodes », *Sous la direction de Véronique Fillol et Pierre-Yves Le Meur*, Actes du 24e Colloque CORAIL - *op. cit.*

Il s'agit ainsi ici d'une « quête d’une autonomie par des épistémologies émancipatrices » (<cite>P. Nicolas-Le Strat</cite> (2015))[^le-strat] sans « illusion romantique de relation de résistance au pouvoir » (<cite>Abu-Lughod Lila</cite> (2006))[^Abu-Lughod].

[^le-strat]: « *En quête, en conquête d’une autonomie – entre “Do It Yourself” et “Do It Together”*, P. Nicolas-Le Strat  <https://pnls.fr/en-quete-en-conquete-dune-autonomie-entre-do-it-yourself-et-do-it-together/>

[^Abu-Lughod]: « *L'illusion romantique de la résistance : sur les traces des transformations du pouvoir chez les femmes bédouines* », Abu-Lughod Lila <https://ancrages.org/wp-content/uploads/2015/04/Abu-Lughod-Illusion-romantique-de-la-r%C3%A9sistance.pdf> Tumultes, 2006/2 n° 27, p. 9-35. DOI : 10.3917/tumu.027.0009



![](/assets/images/affichette-rennes-un-jour.jpg)


## Proximité avec le « Passager Clandestin »

L’économiste-sociologue américain Mancur Olson (1932-1998), contributeur à la théorie des choix publics, a montré qu’un agent rationnel a intérêt à profiter d’une action collective sans y prendre part. C’est le phénomène du « passager clandestin », celui qui ne participe pas à une action collective (grève, manifestation, sitting…) pour ne pas en supporter les coûts (temps passé à défiler, perte de salaire, prise de risque…), *La Logique de l’action collective* (1965), Olson. C'est une approche par l'individualisme méthodologique.

D'un autre bord, la Convention visant à faciliter le trafic maritime international, 1965, telle que modifiée (Convention FAL), énonce des mesures visant à prévenir les cas d'embarquement clandestin ainsi que des dispositions sur le traitement des passagers clandestins lorsqu'ils sont à bord et sur le débarquement et le retour d'un passager clandestin.
La Convention FAL définit un passager clandestin comme suit : "Personne qui est cachée à bord d'un navire, ou qui est cachée dans la cargaison chargée ultérieurement à bord du navire, sans le consentement du propriétaire ou du capitaine du navire, ou de toute autre personne responsable, et qui est découverte à bord du navire après que celui-ci a quitté le port, ou dans la cargaison lors du déchargement au port d'arrivée, et est déclarée aux autorités compétentes, par le capitaine, comme étant un passager clandestin." [Organisation Maritime Internationale](https://www.imo.org/fr/OurWork/Facilitation/Pages/Stowaways-Default.aspx)

> « Qui est vraiment ce passager clandestin, ce free-rider ? Cette notion d’origine économique est plurale, chacun y va de sa définition et y voit ce qu’il veut y voir.
Parfois le passager clandestin est un acteur économique qui utilise un bien collectif sans en payer sa part (par exemple le passager du train qui ne paie pas son billet).
Parfois c’est une personne qui va bénéficier d’une campagne d’investissements sans y avoir participé (par exemple l’actionnaire d’une entreprise en faillite qui attend que les autres actionnaires remettent de l’argent dans les caisses pour remettre la société à flots).
Ou alors ce peut être une personne qui va bénéficier d’une action faite par un groupe mais là encore qui ne va pas y participer (par exemple du salarié qui bénéficie des acquis sociaux sans avoir participé au mouvement social). » <cite>Vladimir Ritz</cite>, (2014) *doctorant en propriété intellectuelle et explorateur associé de PiNG impliqué dans le groupe de réflexion C LiBRE*

Les éditions le « Passager clandestin » m'avise de la parution le 17 février prochain de « Mes trompes mon choix », un ouvrage sur la stérilisation contraceptive par Laurène Lévy. 

<div class="breaker"></div>

## Flânerie

> « […] que l’économie du don ne précise jamais le montant de la restitution, le coût de la réparation. On ne nous dit pas que donner, c’est parfois déjà voler. Que reprendre c’est faire circuler. Et que rendre, c’est souvent réparer. » Léna https://enmarges.fr/2020/12/17/ce-quon-ma-vole-je-vous-le-rends/

## L'oisiveté

L'oisiveté *dé-range* des règles d'un regime de « vérités » établi par ensemble de groupes d'individus, elle bouscule dans un ordre institué par le capitalisme avec ses rapports de dominations et de violences une épistémologie.

L'oisiveté comme valeur positive (L'Otium de <cite>Sénèque> ; <cite>Matthieu 6:26</cite>), l'oisiveté comme élévation (<cite>Søren Kierkegaard</cite>, influençant <cite>Lacques Ellul), l’oisiveté comme vice, « Éloge de l'oisiveté » de <cite>Bertrand Russell</cite>

Documentation et description constituent-elles deux approches fondamentalement différentes ? Peut-on attribuer le terme d’enquête à la documentation, puisque le « recueil de données », au sens large, se veut illimité et non cadré ? <cite>Claire Moyse-Faurie</cite> « Terrains océaniens : enjeux et méthodes », *op. cit.*

## Notes et références
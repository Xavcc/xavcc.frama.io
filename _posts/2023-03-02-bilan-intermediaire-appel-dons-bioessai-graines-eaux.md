---
title: "Bilan intermédiaire de la campagne de dons pour un test d'enquête environnementale à base de graines sous licence libre"
layout: post
date: 2023-03-01 07:40
image: "/assets/images/labo-PTSD-fev-2023-small.jpg"
headerImage: true
tag:
- hsociety
- Biohacking
category: blog
author: XavierCoadic
description: Bioassai et graines de luttes
---

Le 6 janvier 2023 j’écrivais un [appel aux dons pour avancer sur la conception d’un bioessai](appel-dons-pour-developper-test-enquete-environnementale/) reproductible dans nos cuisines, caves, garages, jardin, squats et par des personnes « non-expertes ». Je m’engageais aussi à écrire un *bilan* de cette campagne. Au départ la publication était prévue tout début février, or la vie est pleinne de rebondissements et d’imprévus (et de délais et cela participe aux charmes qui nous ambiancent). 

Je dis ce *bilan* comme *intermédiaire*, car les actes et les conséquences de cette campagne n’ont pas livré tous leurs effets, ni l'ouverture de leurs angles, ni encore la portée de cette petite histoire qui me dépasse. Aussi, les dons sont toujours possibles via ce [lien](https://liberapay.com/Xav.CC/).

J’ai reçu une vingtaine de messages, la plupart de personnes que je ne connaissais pas il y a 2 mois, et ce bien en dehors des frontières de la France hexagonale. Le simple fait qu’**une personne ayant lu l’appel lance au milieu d’une salle pleine d’individus potentiellement réceptifs** « _Tiens, tu devrais jeter un œil à ces travaux “Bioessai”, graines et pollutions de l’eau_ » **est d’une efficacité incroyable**. C’est aussi valable dans d’autre boucle et format de communication.

Idem pour les compte-rendus, j'imagine.

À la suite de l’aspect financier je partage des éléments des travaux faits en janvier et février, puis ce qui viendra en mars et après.

+ [Il y a quelques belles surprises en plus](#il-y-a-quelques-belles-surprises-en-plus)
+ [Du côté des travaux de février](#du-côté-des-travaux-de-février)
  + [Des enfant⋅e⋅s et un jeu des 7 différences](#des-enfant⋅e⋅s-et-un-jeu-des-7-différences)
  + [Des ouvriers et des petites mains qui arpentent (et codent)](#des-ouvriers-et-des-petites-mains-qui-arpentent-et-codent)
  + [Concevoir et produire nos propres outils / machines sous licence libre](#concevoir-et-produire-nos-propres-outils--machines-sous-licence-libre)
+ [Mars et après](#mars-et-après)
  + [Un petit bout truc punk et queer](#un-petit-bout-truc-punk-et-queer)
+ [Remerciements](#remerciements)

## L’argent et les dons

**J’ai reçu 1 575,45 €**, au 01/03/2023 en 13 dons et déduction des frais de Liberapay compris, sur les 3 800 € demandés en prévision de :

+ **3 mois de travail** (février, mars, avril), soit 1 000 € par mois
  + Corriger et améliorer les documentations existantes
    + ajouter « Comment choisir les graines pour les essais biologiques et où les trouver ? »
    + ajouter « Comment choisir l’équipement et le matériel »
  + Refaire des séries de bioessai avec micro-dosages et différentes graines
  + Comparer avec d’autres protocoles et expériences puis évaluer les différents standards
  + Réaliser des comparatifs entre le mode « depuis ta cuisine » et « laboratoire d’analyse » sur les protocoles et les résultats
+ **Animer 3 ateliers de 5 jours consécutifs** chacun dans des lieux/communautés en demande (voir le chantier d’octobre 2022 [Louzaouiñ Graines de luttes 2022](https://wiki.kaouenn-noz.fr/hors_les_murs:reprises_des_savoirs:louzaouin_graines_de_luttes))
  + Avec des méthodes en éducation populaire
  + Avec une demi-journée « Biopanique, cuisine et féminisme »
+ Mettre tout cela en documentation
+ **800 €** de frais
  + 500 € de déplacement (Sud France, Nord Italie, Belgique)
  + 300 € de matériel
    + équipements pour expériences avec les standards de référence dans les laboratoires classiques
    + consommables divers (micro-tubes, eau distillée, réactifs, papier Whatman, etc.)

Récapitulatif par tableau :

![](/assets/images/recap-fin-dons-graine.png)
<figcaption class="caption">Description de l’image : Un tableau sur fond blanc qui fournit le récapitulatif des 13 dons reçus via liberapay depuis le 8 janvier 2023, avec leur montant par dons et date de versement. Les dons sont « anonymisés »</figcaption>
<br />

**J’ai ainsi 1 mois de travail financé, selon les termes annoncés, et 575,45 € à répartir sur différents postes de dépenses/investissements**

C’est un temps 3 fois plus court par rapport à ce que j’espérais et c’est pourtant un fort soutient que je reçois. Aussi, un campagne de dons ne se fait pas sans débauche d’énergie et de temps, avec leurs lots de stress, avec une bonne dose « d’exposition de soi » qui draine parfois de mots déplaisants, voir des insultes ou plus virulents.

### Il y a quelques belles surprises en plus

[Graine de Moutarde](https://graine-de-moutarde.arree.bzh/), fonds de dotation au service des habitant·es de l’Arrée en commun, a versé un don conséquent et s’engage en plus à payer entre 300 et 500€ de matériel. Je m’efforce de trouver du matériel sous licence open hardware, comme le [spectromètre  du GaudiLab](https://gaudishop.ch/index.php/product/3d-printed-fiber-spectrometer-kit/) entre autres matériels. Nous avons également convenu que ces appareils achetés seront mis en communs avec les personnes et collectifs parties prenantes du fond et de ses actions.

> « Notre survie et celles de nos éco-systèmes sont en jeu
>
> le fonds de dotation « Graine de Moutarde » est un outil au service de l’émancipation des habitants des monts d’Arrée. Notre survie passe en effet par la réappropriation de nos moyens de subsistance, la création de communs à la base d’une solidarité forte et l’auto-gestion démocratique de notre environnement.
>
> Tout cela requiert d’abattre le capitalisme, mais pour cela il faut démontrer qu’une autre façon de faire société est possible et enviable. Le fonds de dotation a pour but de financer les premiers communs de cette société nouvelle. »
>
> <cite>Graine de Moutarde</cite>

J’ai profité de L'[OFFDEM O<sub>3</sub>](https://ps.zoethical.org/pub/offdem-ozone-cfp), à Bruxelles, pour faire quelque démo du proto de [centifugeuse à base de sous-bocks de bière](https://wiki.kaouenn-noz.fr/hors_les_murs:lab0_bi0_p0p:la_sauce_bio:ohl_sauce_centrifuge) en carton et amorcer quelques discussions sur le temps long avec de nouvelles rencontres à venir. <marginnote>voir <a href="https://pad.public.cat/offdem-ozone-notes">notes de l’OFFDEM Ozone 2023</a> et <a href="https://pad.public.cat/KATDhh6fTeWp6KO-M_EwXw#">notes GenderSec // operational security in transmigratory/exile contexts</a></marginnote>.

Une fedinaute pour j’ai un grand respect et admiration m’a offert une ampoule inactinique. Ce don va permettre de réaliser de très préceiuses chromatographie, dont des chromato circualire, qui seront d’une puissante aide à la conception et amélioration du bioessai et aussi dans les enquêtes environnementale sur l’eau et les sols.

Sun, camarade de biohacking, m’envoie actuellement des graines mises à disposition sous licence libre.

Vittorio Saggiomo, Assistant professor in BioNanoTechnology in WUR (Wageningen), m’envoie en ce moment des [prototypes d’appareils microfluidics](https://chemrxiv.org/engage/chemrxiv/article-details/63fb0342937392db3d1126e6), pour séparer et visualiser des microparticules de 200 à 25 micromètres, et des microscopes "Matchboxscope" (3d print + ESP32). Ces deux types de dispositifs vont aider à affiner les paramètres du bioessai dans les prélèvements d’eaux (microfluidique) et sur les différentes zones des racines des graines qui réagissent (ou pas) aux perturbateurs et polluants.

Nicolas Voisin, avec enfantspaysans.org, a accepté de mettre en œuvre un prototype de bioessai avec des grines sur l’eau avec des enfant⋅e⋅s. Je dois terminer une *recette* qui leur permettra de l’utiliser et ensuite recevoir les retours d’expériences pour amméliorer le prototype et la procédure. Une série du test qui sert de base à ce prototypage, la recette de base, sera très prochainement mise en œuvre dans un coin du Bois de Vincennes par des enfant⋅e⋅s, YEAH !.

Nous concevons ce bioessai ensemble. Toutes et tous ensemble.

## Du côté des travaux de février

### Des enfant⋅e⋅s et un jeu des 7 différences

J’ai travaillé avec un groupe d’enfant⋅e⋅s, entre 7 et 13 ans, pour préparer une aide à l’observation des racines des graines mise en *boiessai* dans des prélèvements d’eau. Il s’agit d’une sorte de jeu de « 7 différences » sur des parties choisies de la racine.

C’est très balbutiant et à l’état d’ébauche. Je ne suis pas dessinateur, ni cartographe.

![](/assets/images/small-roots-bioessay.jpg)
<figcaption class="caption">"Une photographie couleur composée d’un fond en bois clair sur lequel repose au premier plan une planche de dessin sur fond blanc. Le dessin au sujet de graine et germination, physiologie végétale, est fait de plusieurs parties. En haut à gauche : Une esquisse de graine avec germination de racine « saine » et annotation indicative des zones et des membres de celle-ci. Au centre : un double zoom dessiné avec annotations et indications, centrée sur la zone pilifère et mérismatique, avec second zoom précisant une coupe de cette zone spécifique. À droite : deux imprimés de page (en anglais) de cours de physiologie végétale au niveau cellulaire avec une illustration photographique incrustée d’une experience de bioessai sur graines ; graine 1 en milieu « sain oxygéné » et graine 2 en milieu contenant de l’éthylène."</figcaption>
<br />

Nous concevons des bioessais ensemble.

### Des ouvriers et des petites mains qui arpentent (et codent)

D’un autre bord d’approche du problème, j’ai travaillé avec des peronnes dans un bar ouvrier. En partant d’un chapitre du livre "Bioassays Advanced Methods and Applications", de <cite>Azizullah Azizullah and  Donat-P. Häder</cite>, nous avons quelque peu lu et œuvré par arpentage pour reproduire à notre *sauce* des classifications des types de polluants / pertubateurs dans l’eau. Nous sommes allé⋅e⋅s jusqu’à utiliser le logiciel [PlantUML](https://www.plantuml.com/), ce qui est un remarquable pied-de-nez de la part d’ouvrier du bâtimentet  & tireur euses de fibre optique qui sont plus souvent placé⋅e⋅s dans le spectre de la « fracture numérique » par des personnes bien privilégiées.

![](/assets/images/graph-1-pollu.png)
<figcaption class="caption">Description de l’image : "Diagramme en cascade verticale de « classification » des polluants de l’eau en fonction qu’ils soient dissous ou non-dissous. Chemin de la cascade de gauche : 1. Polluants non-dissous 1.2 Flottants, en suspensions, précipités, colloïdes 1.3 un exemple : Les Micro-organismes (unicellulaires, virus, fungi) Chemin de droite de la cascade 1. Polluants dissous 1.2. Polluants organiques et polluants non-organiques 1.2.1 Solides, Liquides, Gazeux 1.2.1.1 Exemples : anioniques (charge négative), cationique, amphotériques (dont la charge dépend du pH de l’eau), composé covalents et/ou complexes 1.2.2 Polluants biologiques non-dégradables 1.2.3 Polluants biologiques dégradables"</figcaption>
<br />

![](/assets/images/graph-2-pollu.png)
<figcaption class="caption">Description de l’image : "Diagramme de » classification des polluants organiques et polluants non-organiques de l’eau en fonction de d'origines (dite naturelle ou anthropogénique) et leurs caractéristiques physiques et chimiques) Chemin de cascade de gauche : 1.1 Polluants dits naturels 1.1.1 Types cationiques 1.1.2 types anioniques 1.1.3 métaux lourds 1.1.4 substances humiques *1.1.5 de types naturels (dont les métaux lourds et par exemple l’oxygène et le dioazote. Chemin de droite de la cascade du diagramme : 1.2 Polluants d’origines anthropogéniques 1.2.1 résidus de médicaments et drogues humaines et animales, pesticides, insecticides, composés tensioactifs, POPs, PAHs, CHCs, HCs, etc. *1.2.2 Les métaux lourds 1.2.3 liquide résiduel de percolation d’eaux en décharge 1.2.3.1 Alcool, Toluène, Phénol, Ethers, acides, phtalates, etc. etc. etc."</figcaption>
<br />

C’est pas dans la *norme* de beauté et d’esthétique qui prédomine et j’assume, nous assumons. Ces diagrames et classifications seront améliorés *au fil de l’eau* des rencontres.

Nous concevons ce bioessai ensemble. Les sources du code sont [ici](https://pad.kaouenn-noz.fr/p/diagramm-polluants-hack2o), si vous aussi vous souhaitez jouer.

### Concevoir et produire nos propres outils / machines sous licence libre

Janvier et février furent hauts en couleurs, en rencontres, en efforts, en émotions. Il manquait pourtant un pas très important. La possibilité de travailler sur les aspects génétiques<marginnote>J'y reviendrais avec « Reprendre la biologie aux machines » dans quelques jours.</marginnote> des graines utilisées afin d’approcher un niveau de standard de test bioessai qui rivalise avec ce qui est fait en laboratoire universitaire (c’est l’horizon affiché et voulu). Autrement dit, travaillé sur la misee à disposition d’éléments de preuve portant sur l’ADN de ces graines (quantification de partie cible d’une séquence et aussi aide à la purification avant analyse).

Pour cela une éélectrophorèse sur gel avec transillumination est une option solide et éprouvée. Le matériel commercial est en grande majorité non-libre de droits, de plus très cher, parfois très très cher.

Je me suis lancé dans la modélisation, la fabrication et l’utilisation d’un apparail « fait maison » sous licence libre / open Hardware.

Reprenant ce qui avait été fait avec OpenWetWare entre 2008 et 2011, [Open Gel Box 2.0](https://openwetware.org/mediawiki/index.php?title=DIYbio:Notebook/Open_Gel_Box_2.0&action=history) devenu [PEARL Blue](https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=18740-6458.pdf) puis disparu, et [BLUE Transluminator](https://www.gaudi.ch/GaudiLabs/wp-content/uploads/Transiluminator_electronic_pcb.pdf) du GaudiLab [DIY Transluminator Hackuarium](https://wiki.hackuarium.ch/w/Diy-transilluminator), et le [projet ancien du Genetic Lab Center](https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=build_gel_box.pdf), et [Cheapass science – How to build a $21 gel box](https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=cheapass_science_how_to_build_a_21_gel_box_citizen_science_quarterly.pdf), enfin et surtout le 
Hardware/MiniGel de Sebastian Coccioba (Binomica-Labs).

![](/assets/images/gelbox-ohl.jpg)
<figcaption class="caption">Description image : "Une image sur fond blanc. 3 logos, 1 en haut à droite est celui du biohackerspace Kaouenn Noz, en bas à Droite logo de l’équipe Seeds Of Contend (Hackteria + Kaouenn Noz), en bas à gauche le logo de l’Open Hardware. Au centre de l’image : 6 pièces géométriques modélisées en 2D/3D pour conception d’un appareil d’électrophorèse sur gel, ces pièces une fois découpées ou imprimées en 3D pourront être assemblées. Une machine propriétaire pour électrophorèse sur gel coûte plusieurs centaines d’euros, voir 3 000 € pour certaines."</figcaption>
<br />

Il y a là une bonne tonne de travail avant d’aboutir à un appareil dont la mise sous licence open hardware ne rendra pas des gens et des organisations (potentiellement) agressives. Au passage, si vous voulez aider, je galère en électrique / électronique par exemple, je serais plus que joie de discuter avec vous et même de faire cela ensemble.

Les fichiers `.stl`, les sources, les documentations seront disponibles sous licence art libre, CC BY SA, et CERN Open Hardware dès que possible. 

AHAHAHAHA, le temps…

## Mars et après

Disons que les 1000 € ont déjà été un peu utilisé pour du temps de travail en février. Je vais alors me concentrer pour ce mois de mars sur les tâches suivantes :

+ Corriger et améliorer les documentations existantes
  + ajouter « Comment choisir les graines pour les essais biologiques et où les trouver ? »
  + ajouter « Comment choisir l’équipement et le matériel »
+ Refaire des séries de bioassai avec micro-dosages et différentes graines
  + Affiner la qualité et sensibilité du bioessai de base avec les matériels réceptionnés

Un mois de travail bien chargé durant lequel je vais veiller à ne pas tomber dans le trou financièrement, physiquement et mentalement.

Je répète, les dons sont toujours possibles via ce [lien](https://liberapay.com/Xav.CC/) et je suis persuadé que vous saisissez avec forces les enjeux et les besoins de continuer cette petite histoire.

### Un petit bout truc punk et queer

Avec des copaines venu⋅e⋅s au Biohackerspace la [Kaouenn Noz](https://wiki.kaouenn-noz.fr/) et d’autres dont les rencontres se font *en ligne* sur les internets, nous avons lancé [Seeds of Contend / Bioassay in your own dirt](https://www.hackteria.org/wiki/Seeds_of_Contend_/_Bioassay_in_your_own_dirt), avec des jeudi soirs du [Pipi / PTsD à Rennes](https://www.hackteria.org/wiki/Seeds_of_Contend_/_Bioassay_in_your_own_dirt#PTsD), puis d’autres rencontres à Bruxelles et en Italie à venir.

C’est en anglais. Cela pourrait paraître comme un projet parallèle et une débauche supplémentaire pas forcèment nécessaire, voir risquée et pleinne de dispersion.

C’est effectivement un peu *à côté* du travail de conception du bioessai à base de graine. Cependant c’est un espace-temps et un groupe vital pour moi et vecteurs de respirations non négociables. C’est aussi là que des collaborations viennent en soutient, même en aide directe, sur des points précis et difficile du bioessai, comme sur l’appareil d’électrophorèse sur gel. C’est tout autant une nécessité pour alimenter et poursuivre l’approche en éducation populaire et pédagogie critique sur les sciences et et techniques dans lesquelles mes mains et ma tête sont engagées et sur nos rapports sociaux / hiérachiques & hiérarchisés (dons et contre dons inclus), sur les héritages et responsabilités individuelles et collectives.

![](/assets/images/perf-PTsD-fev-small.jpg)

# Remerciements

Sincèrement et le cœur qui bat fort, merci `<3` :

+ À toi qui lis ceci jusqu’à ce point
+ Aux participant⋅e⋅s contributeurices d’un moment, d’un jour, ou d’un temps plus long
+ Aux donnateurices
+ Aux personnes qui ont relayé les infos
+ Aux personnes qui m’ont écrit
+ Aux personnes rencontrées
+ Aux personnes citées et organisations citées
+ À celles et ceux qui maintiennent les outils, logiciels, connexions, citées et évoquées.

<div class="breaker"></div>
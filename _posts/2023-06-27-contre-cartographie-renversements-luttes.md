---
title: "Contre-cartographie, renversement(s) et activismes"
layout: post
date: 2023-06-27 05:20
image: /assets/images/at-the-gates-1.jpg
headerImage: true
tag:
- hsociety
category: blog
author: XavierCoadic
description: La cartographie comme outil des luttes
---

En mai je préparais 2 sessions de 3 jours chacune d'ateliers « [Investigation de l'adaptation aux changements climatiques : Méthodes et principes](https://exposingtheinvisible.org/en/workshops/climate-change-adaptation/) », avec Tactical Tech / Exposing the Invisible & la [Sweden's government agency for development cooperation](https://www.sida.se/en) (SIDA). Ces sessions étaient ainsi voulues : « les personnes minorisées par leurs assignations, les précaires, les personnes en situations de violences sociales  sont prioritaires pour participer, y compris les personnes en situation illégale administrative ». Une vingtaine de participant⋅e⋅s et bien plus à dire, j'y viendrai dans un écrit ultérieur. Des « épingles » positionnées et des plans et des dimensions (avec leurs logiques)… Et des portes !

![](/assets/images/1G7A0166_0.jpg)
<figcaption class="captions">AT THE GATES : Artists’ Campaign to Repeal the Eighth Amendment (Áine Phillips). 'Bannerettes R-E-P-E-A-L', 2017. Installation view, At the Gates, 2018. Image courtesy Talbot Rice Gallery, The University of Edinburgh. (source: https://www.trg.ed.ac.uk/exhibition/gates)</figcaption>
<br />

Entre ces deux temps j'ai étais engagé pour assurer enseignement / formation et encadrement de cela, 1 semaine de « terrain », dans un MsC "Strategy & Design for the Anthropocene / redirection écologique", un autre contexte, un groupe très différent et une autre zone géographique *territorialisée*.

Dans les deux cas et situations des initiatives de cartographies, et de très nombreux questionnements ont été mis sur la table des discussions. Dans le premier cité ce sont les participant⋅e⋅s qui voulaient porter une enquête critique sur des cartes existantes, dans le second il s'agissait d'une forme de commande passée par des encadrants/participants sur le travail d'étudiant⋅e⋅s et de participant⋅e⋅s externes au cursus MsC.

Pour préparer ces deux *moments* j'avais participé au « [colloque : À l’épreuve des tempêtes. Institutions et crises : approches historiques](https://xavcc.frama.io/notes-colloque-institutions-crises-recherche-histoire/), entre autres préparatifs. L'enchaînement de ces *choses* m'ayant déjà re-mené dans les ateliers « [Biennale Économique de Rennes 2016 : Où nous mènent les nouveaux modèles d'organisation](https://xavcc.frama.io/next-eco/).

> « Les cartes ont pour fonction de spatialiser des données économiques et sociales. Les cartes « critiques », quant à elles, révèlent une réalité souvent occultée : les inégalités de conditions de vie et de droits, les compromis politico-économiques, l’accaparement des terres par l’agroindustrie, la pollution de la planète par l’industrie extractive… Alors que la cartographie traditionnelle reflète et conforte le pouvoir en place, les expériences de contre-cartographie ont pour but de renverser les points de vue et proposent des représentations spatiales alternatives à celles de l’ordre établi. » *[Exposition : Ceci n'est pas un atlas ](https://nouvelles.univ-rennes2.fr/event/exposition-ceci-nest-pas-atlas)*, *Nouvelles de  Université Rennes 2*, mars 2023.

![](/assets/images/Capture d’écran_2023-06-27_13-07-41.png)
<br />

J'avais aussi mis dans ma besace « Ceci n'est pas un Atlas  », <cite>Kollectiv Rangotango+</cite>, Coordination et traduction <cite>Nepthys Zwer</cite>. Ed. Du Commun (Rennes), 2023. (Licence CC BY SA NC) [([Sommaire, chapitres et fanzine en pdf](https://www.editionsducommun.org/pages/ceci-nest-pas-un-atlas-sommaire-chapitres-et-fanzine-en-pdf))].

![](/assets/images/waves-of-autonomy.png)
<br />

Ce « contre-atlas », traduction de [This Is Not an Atlas](https://notanatlas.org/book/) (merci Laurent Jégou), résonnant, aussi pour moi, avec les travaux du [Visual Sociology Research Group / Laboratorio di Sociologia Visuale ](https://www.laboratoriosociologiavisuale.it/new/), rencontré avec Tactical Tech Exposing The Invisible il y a quelques années. Avec du “[MAPPING QUEER SPACE(S) OF PRAXIS AND PEDAGOGY](https://wiki.kaouenn-noz.fr/_media/mapping-queer-space-s-of-praxis-and-pedagogy-james--annas-archive.pdf”), 2018, en supplément.

![](/assets/images/Capture%20d%E2%80%99%C3%A9cran_2023-06-27_15-34-18.png)
<figcaption class="caption">From https://notanatlas.org/book/</figcaption>
<br />

> « L’objectif de cet ensemble semble plutôt de rassembler des documents, uniques, monographiques, portant sur différentes zones et, donc, forcément non comparables puisqu’ils sont hétéroclites. Aussi et surtout de montrer comment des représentations de territoires sont pensées, discutées, fabriquées pour, voire en coproduction avec, des populations.
>
> Son élaboration s’inscrit en effet dans un contexte de recherche géographique critique, mêlant des considérations artistiques à des objectifs pédagogiques, d’éducation.
>
> Ce qui est vraiment intéressant – pour qui n’y est pas familier – est de voir comme la réflexion à l’origine des projets présentés, souvent collective, prend corps dans un contexte de lutte politique et implique des populations diverses qui vont ainsi pouvoir s’emparer de l’outil « cartographie » pour proposer (co construire) leur propre vision de leur territoire, celui où ils vivent – en s’appuyant sur des matériaux divers … en réponse à différents objectifs pouvant être scientifiques.
>
> […] La conclusion « qui n’en est pas une » précise que cet ouvrage s’inscrit dans la lignée de travaux déjà anciens de cartographie radicale (<cite>Mogel</cite> & <cite>Bhagat</cite>, 2007),
>
> C’est pourquoi ces [contre] « cartes » sont considérées [par le collectif] comme participant d’un “*mouvement fluide dont la tactique va de la création artistique à l’action directe en passant par l’élaboration de politiques. Ce travail lent, cumulatif et constant à plusieurs échelles d’action est ce qui crée le changement social*” (<cite>Moge</cite> & <cite>Bhagat</cite>, 2007 : 12) »
>
> *This is not an atlas*, <cite>Françoise Bahoken</cite>, [Carnet Néo-Carto](https://neocarto.hypotheses.org/5922) (Hypothèses), 2019. [*Merci Nicolas Roelandt*]

Cette œuvre et recherche me fait revenir en souvenirs "At The Gates" ([archive web](https://web.archive.org/web/20190820232522/https://www.la-criee.org/fr/at-the-gates/)), vue en 2019 à Rennes à la Criée.

At the Gates, histoire politique et sociale de l'intime, lutte des Femmes, défiant les lois, pour leurs émancipation et Liberté. Exemple observé : <cite>Jesse Jones</cite>, appropriation de la sculpture Irlandaise Païenne Matriarcale Sheela Na Gig, montrant ses dents et exposant sa vulve (voir [archive video *[Jesse Jones’ performance installation “tremble, tremble, the witches have returned”.](https://web.archive.org/web/20190727130843/https://player.vimeo.com/video/311194427?app_id=122963), Talbot Rice Gallery ; The University of Edinburgh]*)

![](/assets/images/SheelaWiki.jpg)
<figcaption class="caption">Sheela na Gig project www.sheelanagig.org. The (in)famous Kilpeck Sheela Na Gig, A 12th-century sheela na gig on the church at Kilpeck, Herefordshire, England. This file is licensed under the Creative Commons Attribution-Share Alike 3.0 Unported [Wikimedia]</figcaption>
<br />

Juste dessous, marteaux en spirale étoilante, outils d'émancipation, avec inscription « Thou shalt Not Suffer » détournement du Malleus Maleficarum, livre ignoble du XV<sup>e</sup> siècle qui est un Manuel de chasse, d'oppression et de torture des Femmes, appelées sorcières lorsqu'elles sont non soumises au diktat. L'autrice coupe alors « Thou shalt Not Suffer a witch to live » (*tu ne permettras point aux sorcières de vivre*), issu du « manuel », pour marteler un nouvel ordre « tu ne souffriras point ».

![](/assets/images/etoilante.jpg)

Une broderie de <cite>Teresa Margolles</cite>, accompagnée avant la salle obscure par une vidéo immanquable.
Broderie avec et par des Femmes Mayas à partir des linceuls des Femmes mortes par violence. Plongée dans la salle totalement sombre après avoir visionné la vidéo sur les discussions qui accompagnent la confection de cette œuvre.

![](/assets/images/at-the-gates-1.jpg)

Objet traumatique et expression de compassion à la fois. Beaucoup d'autres œuvres et HistoireS à y découvrir !

Tout ceci provoque encore un nouvel écho en moi, une phrase répétée plusieurs fois dans « L’odyssée des gènes » (<cite>Évelyne Heyer</cite>, *ed. Flammarion 2020, 2022, col. Champs sciences*) lorsqu'elle relate le mots de personnes, plusieurs à travers le monde, qui acceptent de participer, par prélèvement et analyse de ceux-ci, à son enquête anthropo-génétique : « Nous allons enfin exister sur la carte du monde »

Et encore, se font revenantes, les maintes initiatives « locales » auxquelles j'ai assisté ici et là de (s'auto) cartographier leurs initiatives et allié⋅e⋅s (*FabLab en Bretagne, La Myne à Lyon,  Prats de Mollo {qui vise l'indépendance énergétique}, [Bulloterie](https://www.bulloterie.org/) & [IndieCamp](https://movilab.org/wiki/IndieCamp),restitutions d'étudiant⋅e⋅s en Design lors de la biennale Internationale de Saint-Étienne 2017, SxSW eco Austin 2015, etc.*). Je tente de garder une persévérance à travailler sur une approche critique des représentions graphiques, voir la thèse de <cite>Fabrice Sabatier</cite> « [Saisir l'économie par le(s) sens : une approche critique de la visualisation de données économiques par le design. Désorceler la finance](https://www.fabricesabatier.com/) »[^note-sabatier] & les *architectures de choix*[^note-benque-1] [^note-benque-2] [^note-benque-3] qui sont incrustées dans les cartes et leurs productions/lectures.

[^note-sabatier]: ainsi que la mise en pratique par le « [Cabinet de curiosités économiques Laboratoire Désorceler la finance](https://wiki.erg.be/mw/index.php?title=Cabinet_de_curiosit%C3%A9s_%C3%A9conomiques_Laboratoire_D%C3%A9sorceler_la_finance) » (évoquée précédemment dans « [« Théorie générale de la Magie », Marcel Mauss, avec les pieds (partie 1)](https://xavcc.frama.io/theorie-generale-magie-mauss-a-pied-partie-1/), 2022)

[^note-benque-1]: *The Flower and the Future*, <cite>David Benqué</cite>, 2018, <https://theairpump.davidbenque.com/the-flower-and-the-future/>

[^note-benque-2]: *Diagram Studies*, <cite>David Benqué</cite>, <https://www.are.na/institute-of-diagram-studies/index>

[^note-benque-3]: *Case board, traces, & chicanes; Diagrams for an archaeology of algorithmic prediction through critical design practice.*, thesis, <cite>David Benqué</cite>, PhD, 2020, Royal College of Art, London UK. <https://davidbenque.com/phd/>

Plus encore, et bien plus lourdement, je revois [la tension entre « petites et grandes catastrophes »](https://xavcc.frama.io/mardi-catastrophologie-5/) (par les normes) et mise en cartographies associées (ex: <https://www.gdacs.org/> ; <https://rapidmapping.emergency.copernicus.eu/>).

Peut-être là, et autour de ce *là*, des frictions par rencontres en zones non étanches entre « culture de soi et sculpture de soi » se font et se défont ; notions qu'<cite>Alain Giffard</cite> traite bien plus finement que moi[^note-giffard-0] [^note-giffard-1], une aiguille et une orientation.

## Notes et références

[^note-giffard-0]: *Culture de soi, Manifeste, Une certaine anarchie*, 2023, <https://lesobscurs.wordpress.com/2023/02/21/cultures-de-soi/>

[^note-giffard-1]: *Culture de soi: quelques entrées*, 2023, <https://lesobscurs.wordpress.com/2023/05/16/culture-de-soi-quelques-entrees/>

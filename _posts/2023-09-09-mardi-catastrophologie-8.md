---
title: "Mardi catastrophologie : votre fournée de ressources (n°8)"
layout: post
date: 2023-09-12 06:40
image: /assets/images/cab1e28f13fb7805.png
headerImage: true
tag:
- hsociety
- Catastrophologie
category: blog
author: XavierCoadic
description: Un désastre révèle comment les choix en matière de développement créent des situations de vulnérabilité
---

+ [Actu] **Earth had hottest three-month period on record, with unprecedented sea surface temperatures and much extreme weather** <https://public.wmo.int/en/media/press-release/earth-had-hottest-three-month-period-record-unprecedented-sea-surface>

![](/assets/images/fa5aecb7-de24-4861-a780-e3c9986329a8.png)

> « Commençons par quelques mots sur l’anthropologie des désastres naturels. Ce champ de recherche découle d’approches pluridisciplinaires qui considèrent un désastre comme un produit en partie social, conjuguant un événement géophysique extrême (ouragan, séisme, tsunami) à des conditions de vulnérabilité, historiquement constituées (<cite>Cuny</cite>, 1983 ; <cite>Hewitt</cite>, 1997 ; <cite>Blaikie</cite>, 2004, <cite>Oliver-Smith</cite>, 2004). Un désastre n’est donc pas un acte de Dieu, ni la seule manifestation des forces de la Nature – bien que ces façons de les concevoir existent toujours, et tel était le cas d’ailleurs au Salvador – mais le produit de l’interaction entre les activités humaines et l’environnement, soit une conséquence du développement même des sociétés (<cite>Lavell</cite>, 1999). En d’autres termes, un désastre révèle comment les choix en matière de développement créent des situations de vulnérabilité – et tel est bien le concept opératoire ici (<cite>Bankoff</cite>, 2003) »
>
> <cite>Sliwinski, Alicia</cite>. *« Chapitre 4. L’environnement », Laëtitia Atlani-Duault éd., Anthropologie de l'aide humanitaire et du développement. Des pratiques aux savoirs des savoirs aux pratiques.* Armand Colin, 2009, *pp. 97-122.*

+ [Ressources (thanks Ilan Kelman)] **Disaster By Choice** How we turn natural hazards into catastrophes—and how we can change. <https://www.psychologytoday.com/gb/blog/disaster-choice>
  + **Island Vulnerability Borders** explores the challenges which isolated geographies face when dealing with risk and disasters by examining the processes which create, maintain, and could be used to reduce their vulnerability <https://islandvulnerability.org/borders.html>

+ [Data (thanks Tuxom)] **Attributing extreme weather to climate change** <https://www.carbonbrief.org/mapped-how-climate-change-affects-extreme-weather-around-the-world/>

![](/assets/images/cab1e28f13fb7805.png "Une image d'un stickers au fond blanc avec texte en noir:'We Make BioPorn'")

+ [Publication] **Dessins d’enfants et aide humanitaire : expressions et expositions transnationales** (Children’s Drawings and Humanitarian Aid: Transnational Expressions and Exhibitions) *Consortium Erudit (ISSN 0847-4478), Journal of the Canadian Historical Association, pages 1-65, 2016* <cite>Dominique Marshall</cite> <https://en.annas-archive.org/md5/6524886dd38aa9bd419221c2072772ca>

+ [Actu] **Hawaï : 1 100 personnes sont portées disparues après les incendies qui ont ravagé l’île** <https://www.liberation.fr/international/amerique/hawai-1-100-personnes-sont-portees-disparues-apres-les-incendies-qui-ont-ravage-lile-20230823_JMXCABDOIVDIBGMA33GA5BRW6I/>

> « Le bilan provisoire des incendies qui ont ravagé l’île du Pacifique s’établit à 115 morts mais risque de s’alourdir. 1 100 personnes sont encore portées disparues et l’identification des cadavres par les analyses ADN se poursuit. »

+ [Actu] **U.S. Aquifers Are Running Dry, Posing Major Threat to Drinking Water Supply** *“aquifers that supply 90% of the nation’s drinking water are being severely depleted,” according to the New York Times.* <https://www.democracynow.org/2023/8/31/aquifer_depletion>
  + In its latest environmental report, Microsoft disclosed that its global water consumption spiked 34% from 2021 to 2022 (to nearly 1.7 billion gallons, or more than 2,500 Olympic-sized swimming pools), a sharp increase compared to previous years that outside researchers tie to its AI research. <https://apnews.com/article/chatgpt-gpt4-iowa-ai-water-consumption-microsoft-f551fde98083d17a7e8d904f8be822c4>
  + Data centers among the top 10 water users in America's industrial and commercial sectors. <https://www.npr.org/2022/08/30/1119938708/data-centers-backbone-of-the-digital-economy-face-water-scarcity-and-climate-ris>
  + One-fifth of US “data center servers’ direct water footprint comes from moderately to highly stressed watersheds, while nearly half of servers are fully or partially powered by power plants located within water stressed reasons.” <https://escholarship.org/uc/item/3xw3z2qp>
  + “Getting Into Fights With Data Centres” <cite>Anne Pasek</cite> <https://emmlab.info/Resources_page/Data%20Center%20Fights_digital.pdf>
  + "Here’s where water is running out in the world — and why" (Washington Post) <https://www.washingtonpost.com/climate-environment/interactive/2023/water-scarcity-map-solutions/>

+ [Actu] **‘A never-ending fight’: cancer and diabetes cases soar in wake of Mariana dam disaster** The impact of Brazil's worst environmental disaster -- the failure of a tailings dam on the Doce River in 2015 -- continues today <https://www.theguardian.com/global-development/2023/aug/22/a-never-ending-fight-cancer-and-diabetes-cases-soar-in-wake-of-mariana-dam-disaster>

+ [Actu] **Hacktivists attack Japanese government over Fukushima wastewater release** *Claiming affiliation with Anonymous, e-hippies want more debate over radioactive flows* <https://www.theregister.com/2023/08/14/hactivitsts_claim_japanese_government_attack/>

+ [Ressources] **The PFAS Project Lab** Studying Social, Scientific, and Political Factors of Per- and Polyfluoroalkyl Substances <https://pfasproject.com/>

+ [Data / monitoring] **U.S. Drought Monitor & Related Products** <https://drought.unl.edu/Monitoring/DroughtMonitoringTools.aspx>

+ [Actu] **Crise de l'eau à Mayotte et catastrophe sanitaire en cours**
  + **2022, À Madagascar, la première famine climatique du monde** <https://www.nationalgeographic.fr/environnement/a-madagascar-la-premiere-famine-climatique-du-monde>
  + 2021, **Madagascar : le Grand Sud en proie à la famine et à sa pire sécheresse en 40 ans (OCHA)** <https://news.un.org/fr/story/2021/05/1095562> et <https://www.rfi.fr/fr/afrique/20210130-madagascar-un-d%C3%A9bat-public-sur-la-p%C3%A9nurie-d-eau-qui-touche-le-pays>
+ **Mayotte, Crise de l'eau à Mayotte : "Cela fait 20 ans que le gouvernement promet des investissements", dénonce Estelle Youssouffa** <https://la1ere.francetvinfo.fr/crise-de-l-eau-a-mayotte-cela-fait-20-ans-que-le-gouvernement-promet-des-investissements-denonce-estelle-youssouffa-1414139.html>
  + [merci Nershelam] de l'eau tous les 3 jours et pour quelques heures, donc :
    - pas d'eau pour se laver
    - quasiment pas d'eau pour boire (sauf de l'eau en bouteille dont les prix sont quasiment inaccessibles, le gouvernement à promis 2 litre d'eau pour les plus vulnérables, apparemment les magasins sont vides), sinon il faut de l'eau récupérée et la faire bouillir pour espérer assainir les microbes pour pouvoir boire.
    - Parce que quand  l'eau circule elle est pas potable parce que les bactéries prolifèrent et qu'il y a des problèmes aussi au niveau des égouts. **Autrement dit une catastrophe sanitaire**.
    - il y a les  2 litres d'eau promis aux populations les plus vulnérables (sur quels critères ?) , L'ONU considère qu'il faut 15 litres par jour et par personne minimum  en situation d'urgence et 20 litres minimum sur le long terme <https://emergency.unhcr.org/fr/emergency-assistance/water-hygiene-and-energy/normes-sur-l%E2%80%99eau-en-situation-d%E2%80%99urgence>

+ [Actu (merci Mershelam)] **Crowd funding pour créer un jeu vidéo de puzzle parlant des problèmes d'eau aux Antilles** (parce que les mobilisations ne marchent pas apparemment) <https://fr.ulule.com/eau-secours>
  + L' initiative :  <https://la1ere.francetvinfo.fr/eau-secours-un-jeu-video-pour-rendre-visible-le-probleme-de-l-eau-en-guadeloupe-1311328.html>

+ [Conférence] **Impact du changement climatique sur la ressource en eau et la production d’eau potable**, Haut Conseil Breton pour le Climat (HCBC) et Centre de ressources et d’expertise scientifique sur l’eau de Bretagne (Creseb), *Séminaire “Science et décisions publiques”* <https://www.creseb.fr/cgle-2023-impact-du-changement-climatique-sur-la-ressource-en-eau-et-la-production-deau-potable/>

+ [Actu] In 2017, the United Nations Environment Programme issued a report that found **benzene in parts of Nigeria at levels over 900 times above World Health Organisation guidelines**. <https://www.apc.org/en/news/photo-essay-enabling-nigerian-communities-see-soot-around-them>

> The Soot Mapping Project, by Media Awareness and Justice Initiative, uses low cost and open data technology to monitor air pollution in Rivers State, the oil nexus of the Nigerian oil industry.

+ [Actu] **Morocco earthquake: rescuers search for survivors as death toll passes 1,000 (09/09/2023) – as it happened** <https://www.theguardian.com/world/live/2023/sep/09/morocco-earthquake-ighil-marrakesh-latest-news-updates>
  + **Race against time to find survivors of Morocco earthquake as death toll nears 2,700** (11/09/2023) <https://www.independent.co.uk/news/world/africa/morocco-earthquake-marrakech-aid-uk-victims-b2409322.html>
  + [Solidarisme] **Suite au tremblement de terre au Maroc, le projet HOT d'OpStreetMap en est activé** pour actualiser les données cartographiques sur la zone touchée. Une centaine de contributeuricess sont déjà mobilisé⋅e⋅s au 2023/09/09 21:00 UTC. <https://tasks.hotosm.org/projects/15468>

+ [Actu] **Mayotte meurt de soif dans l’indifférence** <https://www.liberation.fr/politique/mayotte-meurt-de-soif-dans-lindifference-20230906_3J73XFBV4BBBDPYOQ56KU7FD4E/>

+ [Actu] **Storm Daniel has continued to wreak havoc, leaving thousands dead in Libya. Why was it so deadly?**

> "The situation is tragic," says Essam Abu Zeriba, the interior minister of the east Libya government. 
>
> As many as 10,000 people are missing after Mediterranean Storm Daniel unleashed torrential rain and extreme flooding across the north-east of the country.
>
> As many as 2,000 people are feared dead in the city of Derna alone, where heavy rainfall and floods broke dams and washed away entire neighbourhoods.
>
> Health authorities say they have so far buried 700 people in the city"

+ [Actu] **Inondations en Libye : le bilan pourrait atteindre les 20 000 morts, l'aide humanitaire s'intensifie** <https://www.france24.com/fr/afrique/20230914-inondations-en-libye-le-bilan-pourrait-atteindre-les-20-000-morts-l-aide-humanitaire-s-intensifie>
  + **La Croix-Rouge a évoqué ce mardi le chiffre de 10 000 disparus, et le gouvernement de l’est de la Libye fait savoir que le bilan final pourrait atteindre les 10 000 morts** <https://www.courrierinternational.com/article/inondations-en-libye-il-y-aurait-10-000-disparus-apres-la-tempete-daniel-et-la-rupture-de-barrages>

+ [Livre] **Anthropologie de humanitaire et du développement**, *dir.* <cite>Lætitia Altani-Duault</cite> et <cite>Laurtent Vidal</cite>.

> « Nous pouvons affirmer que l'on prend de plus en plus conscience du fait que l'épidémie de SIDA est alimentée par les “structures d'oppression […] Où qu'elle soit apparue, l'épidémie a révélé des structures d'inégalité et de clivages sociaux, et agit sur eux. Qu'il s'agisse des formes d'oppression et de discrimination sexuelles liées aux communautés d'hommes, de la marginalisation  et de la criminalisation des toxicomanes, des relations de pouvoir genrées liées à l'épidémie de SIDA chez les femmes, ou des injustices genrées liées à l'épidémie chez les femmes,  ou des injustices liés à la maladie au sein de la population pauvre (dans les pays dits développés comme dans le monde sous-développé), l'oppression et l'inégalité ont puissamment façonné le développement de l'épidémie, et cela, partout dans le monde ».
>
> <cite>Carl Kendal</cite>, La santé *in* Anthropologie de l'humanitaire et du développement *2009*

+ [Note] les FR-Alert par téléphone portable, dispositif d'alerte et d'information des populations, <https://fr-alert.gouv.fr/> cachent aussi des conceptions et des choix techniques imposés qui comportent plusieurs problèmes lourds.

Un exemple

Vous avez un téléphone portable caché, vous ne voulez pas que des personnes sachent que vous avez ce téléphone.
Cela peut être le cas dans un régime d’oppression et  de violence (couple, famille, communauté, etc) ou encore dans une activité pro (enquête, etc) avec de lourds risques et menaces.

Et lorsqu'il y a risque météo-climatique, entres autre alertes probables, le téléphone caché va émettre une alarme stridente et impossible à arrêter par votre action − sauf enlever batterie avant (et donc anticiper un alerte/un risque soudain et aléatoire) et/ou pendant (ce qui de plus en plus difficile sur les appareils téléphoniques mobiles) − ce qui est aussi une charge supplémentaire dans une situation déjà trop lourde.

Les conséquences de la révélation de l'existence du téléphone caché peuvent être terribles et extrêmes pour la personne concernée. Et cela est choisi, conçu et imposé par des personnes qui ne sont pas confrontées à ces types de situations. Et/ou engendre abandon du tel comme secours ou nécessite une grande maîtrise technique et technologique.
 


---
title: "Le pouêt, le monocle, le bar lesbien, la page Wikipedia, et les petites histoires"
layout: post
date: 2022-12-05 07:40
image: /assets/images/adelphité.jpg
headerImage: true
tag:
- hsociety
- Tiers-lieux
category: blog
author: XavierCoadic
description: "Les petites contributions tissent des trames"
---

<figcaption class="caption">Image d'en-tête : photographie d'un mur d'une école à Rennes, rue de Vern, « Liberté, égalité, adelphité » − Collage sur mur d'école. 21 juillet 2020. Par Xavier Coadic, Attribution-Share Alike 4.0 International (CC BY-SA 4.0)</figcaption>
<br />

Comment des histoires se relient ? Peut-on de suivre le circuit / parcours / chaine de l'information ? Je n'ai pas ici l'intention de verser dans la « grande réponse ». Voici simplement un petit fil.

Le 21 novembre 2022 dans la [Fediverse](https://fr.wikipedia.org/wiki/Fediverse) un⋅e Mastonaute (une personne qui navigue dans le réseau social mastodon) publie un message public, un pouêt. Citoyen Mr, sur l'instance <https://wandering.shop>, y parle d'une photo dans laquelle apparait une « femme très badass » regardant l'objectif d'un photographe dans les années 1930 en plein moment de vie d'un bar lesbien à Paris.

> I recently found this picture, taken in the 1930s at Le Monocle, one of the lesbian nightclubs in Paris at the time. 
>
> It's a great picture, with a lot of different people, absorbed in their own merriment, and at the center, my new reference for the concept of badass. 
>
> I don't know who she is, but she deserves a long, adventurous backstory -- maybe involving mutiny aboard an airship, spies, and broken hearts. 
>
> She's the only one looking at the camera, so the story is definitely about her.

Möria, mastonaute de l'instance norden.social, y adjoint une publication d'un site web ([archive](https://web.archive.org/web/20160213235446/https://rarehistoricalphotos.com/le-monocle-1932/)).

Je prends *connaissance* de cela le dimanche 4 décembre depuis l'instance Todon.eu, repartages et circulations de pouêt dans un réseau de réseaux sociaux libres et fédérés.

Je suis pris de curiosité et d'intérêt. Je cherche alors des compléments d'informations. Je m'aperçois assez rapidement que la Wikipedia francophone n'a pas de page dédiée pour ce bar nommé le Monocle et pour autant des informations parcellaires apparaissent dans différentes pages de plusieurs personnalités de l'époque 1920 - 1930.

Au début du XX<sup>e</sup> siècle, le monocle était porté par des lesbiennes comme symbole de reprise de puissance et de visibilité. Le nom de ce bar semblant alors lié à cette pratique. Je décide d'ouvrir un brouillon de page dans la Wikipedia, `Utilisateur:XavCCCzh/Le Monocle (bar lesbien))`, et commence à collecter des informations, des sources et écrire quelques lignes.

Très vite je prends *connaissance* que ce bar fut un lieu important de rencontres et que des *figures* y passèrent des instants importants de leur vie.

**Marlene Dietrich**, engagée contre le nazisme ; **Marthe Hanau**, « banquière des années folles », **Line Marsa**, mère d'Édith Piaf ; **Frede**, personnalité marquante des nuits parisiennes relatée par Modiano ; **Violette Morris**, championne sportive multi-disciplines et espionne, collaboratrice du régime nazi, etc. Et même une épreuve photographique de Brassaï, prise dans ce bar, vendue à prix d'or en 2012 chez Christie's.

Un lieu ouvert et tenu par **Lucienne Franchi**, dite Lulu de Montparnasse, qui n'a pas de page dans la Wikipedia. Femme dangereuse pour les forces de l'ordre et l'État de l'époque qui souhaitaient garder un œil sur ces lieux de sociabilités ; comme tous les lieux lesbiens, les femmes qui y travaillent sont « sans cesse surveillées par la police ». Une histoire à découvrir dans le travaux de l'historienne **Florence Tamagne** sur « les flamboyantes années 1920 et le repli des années 1930 » de l'homosexualité en Europe (Berlin, Londres, Paris : 1919-1939).

Je décide de partager mon mon brouillon dans le collectif et projet [Les sans pagEs](https://fr.wikipedia.org/wiki/Discussion_Projet:Les_sans_pagEs) de la Wikipedia Francophone. Quelques contributions plus tard, l'article [Le Monocle (bar lesbien)](https://fr.wikipedia.org/wiki/Le_Monocle_(bar_lesbien)) est publié ce lundi matin 5 décembre 2022. Et très grand merci aux contributrices et contributeurs  et à toutes les personnes derrière Les sans pagEs `<3 <3 <3`.

Ma curiosité et mon intérêt sont aujourd'hui encore plus fort⋅e⋅s. Quelles étaient les règles dans ce lieu ? Qui avait le droit de discussion et de rédaction de ces règles ? Quelless étaient les recettes ou  quels modes opératoires d'engagement et de responsabilité partagée (si existantes, même tacites) ? Dans quel(s) horizon(s) commun(s) étaient projetées ses directions ? Avec autant de personnalités marquantes passées ici qui se sont investies dans des camps politiques parfois radicalement opposés…

Je continuerai de remettre les mains ici et là.

Remerciements très chaleureux à toutes et tous les Mastonautes et Fédinautes qui m'aident d'une manière ou d'une autre, et tout aussi chaleureusement aux personnes qui allègent ma précarité par leurs dons en euros [ici](https://liberapay.com/Xav.CC/).

Les petites contributions tissent des trames… et les petites histoires derrière ce lieu et ses personnes ne sont pas terminées.

![](/assets/images/adelphit%C3%A9.jpg)
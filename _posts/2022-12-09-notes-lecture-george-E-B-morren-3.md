---
title: "Notes de lecture sur la catastrophe : “Identification of Hazards and Responses”, George E. B. Morren Jr, (1983) 3/3"
layout: post
date: 2022-12-09 07:40
image: /assets/images/1976-Caldiran_Page_41-495x400.jpg
headerImage: true
tag:
- hsociety
- Notes
- Catastrophologie
- Anthropologie
category: blog
author: XavierCoadic
description: "Regards sur la catastrophe"
---

*Image d'en-tête : “Çaldıran–Muradiye Earthquake of 24 November 1976, Ms7.3”, International Institute of Seismology and Earthquake Engineering (IIEES) from archive.*

<div class="breaker"></div>

Les précédentes notes sur ce texte :

+ [“Identifiaction of Hazards and Responses”, George E. B. Morren Jr, (1983) 1/3](/notes-lecture-george-E-B-morren/)
+ [“Identification of Hazards and Responses”, George E. B. Morren Jr, (1983) 2/3](/notes-lecture-george-E-B-morren-2/) (avec des informations contextuelles sur Morren fournies par <cite>Bonnie McCay Merritt</cite>)

----

1983, dans ce livre d'importance majeure en écologie occidentale et en anthropologie, les autrices et auteurs décrivent la catastrophe comme objet politique. George E. B. Morren écrit la partie finale dans laquelle il expose les décisions politiques, les doctrines, sur des infrastructures, avec des orientations techniques et méthodologiques, jusque dans leurs conséquences avant et après une catastrophe.

La seconde partie de mes notes se terminait sur une caractéristique typique des processus de réponse face aux désastres et catastrophes, notamment « la réversibilité des processus de réponse ». Par exemple, reconstruire dans les plaines inondables après une inondation ou revenir sur la zone « catastrophée » pour rebâtir des vies, des infrastructures et des villes est encore et toujours une activité humaine courante.<marginnote>Voir aussi <a href="https://xavcc.frama.io/tiers-lieux-et-catastrophes/">Tiers-Lieux : Prise sur le quotidien d’un monde catastrophé</a> (Août 2022)</marginnote>

Dans cette « réponse » typique face aux désastres une double *mise en danger* se joue hors de la temporalité assignée à la « catastrophe », du moins de celle de son ressenti et de sa *visibilité* ordinaire. L’élan *expéditionnaire* des unités d’intervention externes, forçant la subalternité de personnes *endogènes*, n’est pas une action neutre et charrie des conséquences dont les effets peuvent être délétères. L’élan vers un « retour à la normale », que ce soit de « se comporter de la même manière » ou de « reconstruire sur site », se déroule sans prise en considération des facteurs peu visibles de la « catastrophe » et surtout avec peu de considération sur les contingences de risques liés à la « catastrophe » elle-même.

![](/assets/images/wip-diagram-disaster-marginalisation.png)
<figcaption class="caption">"Diagram illustrating the process of marginalisation and the relationship to disaster" P. Susman, P. O'Keefe, B. Wisner, (1983) Interpretation of Calamity From the Viewpoint of Human Ecology, Disasters A Radical Interpretation</figcaption>
<div class="breaker"></div>

Je continue et termine ici mes notes et un regard porté sur les désastres et la « catastrophe » au travers du livre “Interpretation of Calamity From the Viewpoint of Human Ecology”, <cite>Kenneth Hewitt & Al.</cite> (1983), et surtout  le chapitre final de ce livre écrit par Morren.

### Les axes 7 à 13 


**(7) Escalation and de-escalation from phase to phase may involve shifts *in* the unit of response, as from individual to groups in various kinds and degrees of inclusiveness, and back to individuals.**

> “We should, according to Vayda and McCay (1975), be interested in how hazards are responded to by both groups and individuals, the unit of response being view as a part or dimension of the response. An obvious example of this kind of shift is when the efforts of individual survivors of a disaster are augmented, superseded or suppressed by the activities of outside relief workers, police, or troops under the control of a supra-local unit.”

Morren prend ici un essai de Vayda et McCay "*New directions in ecology and ecological anthropology. Annual Review of Anthropology*" (dispo [ici en pdf](https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=1975new_directions_in_ecology.pdf)) qui prend en considération 4 critiques portées sur l'anthropologie de l'écologie, l'étude des adaptations culturelles à l'environnement : 

1. Sa trop grande importance accordée à l'énergie (ou l'*obsession calorifique*),
2. son incapacité à *expliquer* les phénomènes culturels, 
3. sa pré-occupation, apriorique, des équilibres statiques, 
4. son manque de clarté quant aux unités d'analyse appropriées.

Morren ayant étudié et traité des questions énergétique dans ses recherches ethnologiques, notamment dans le Pacifique sud. J'y reviendrais en fin des notes ci-après.

> “The kind of environmental problems we are especially concerned with here are those constituting hazards to the lives of the organisms experiencing them. In other words. we are particularly concerned with problems that carry the risk of morbidity or mortality, the risk of losing an "existential game" in which success consists simply in staying in the game […]
>
> Our focus upon hazards and responses to them emerges partly from consideration of neo-Darwinian selection theory.
>
> Our focus reflects also the new concern of biologists such as Slobodkin with the actual processes of responding to hazards or environmental perturbations mther than with formal alterations in hypothetical genetic systems.[…]” (<cite>Andrea, P. Vayda and Bonnie J. McCay</cite>, *1975*)

Une publication qui traite, entre autres axes forts, de l'examen des connaissances aprioriques et empiriques qui dirigent les regards portés, donc de la logique des conceptualisations et l'*état-des-choses dont il s'agit* (<cite>Husserl</cite>, *1929*), sur les réponses (formes, directions, méthodes, opérations, etc) proposées et déployées face à des risques et des « désastres », avec dans l'argumentaires beaucoup de références au non-humain dans le vivant. Un essai critique sur les jugements opérés en anthropologie de l'écologie, sur les concepts formulés qui interviennent, les déductions, les démonstrations, modalités et théories.

Sont pris en examen : l'adhocratie, l'homéostasie, ego-centered networks, "new ecology", "newer ecology" and "new
functionalism" "neo-functionalism" ; afin d'« arpenter » les relations entre les processus de formation et de dissolution des groupes et les problèmes ou dangers environnementaux, ainsi que l'approche par individu face à ces mêmes problèmes.

Cet essai se termine sur une conclusion ouvrant sur la question des problèmes environnementaux et la façon dont les gens y répondent à ces problèmes, dans le prisme de l'anthropologie de l'écologie<marginnote>40 ans après Hewwitt, Morren, Vayda et MacCay, l'exotisation de populations « lontaines et peu touchées par la “technologie“ » sert base à un renouvellement du colonialisme, ou néo-colonialisme, par l'invocation d'un « mode de vie indexé sur celui de populations autochtones vivant d’une manière supposément plus naturelle » (A. Monnin (2022), blog Mediapart), notamment dans leurs modes opératoires techniques, dans une direction d'écologie politique dans plusieurs mouvements actuels occident</marginnote> :
1. Prêter attention à de nombreux dangers ou problèmes possibles en plus de ceux liés à l'utilisation de l'énergie.
2. Étudier les relations possibles entre les caractéristiques des dangers, telles que leur ampleur, leur durée et leur nouveauté, et les propriétés temporelles et aux autres propriétés des réactions de la population.
3. Abandonner une vision centrée sur l'équilibre et s'interroger plutôt sur le changement par rapport à l'homéostasie.
4. Étudier la façon dont les dangers sont affrontés non seulement par les groupes mais aussi par les individus.

Pour son axe directif, autrement dit pour sa prescription ici commentée, Morren prend appui sur un essai comportant lui-même une importante dose prescriptive : essai rédigé par ses collègues de département de recherche.

En 1975, après la guerre « secrète » du Laos, la France s'illustre, notamment par son système camp et technique d'enfermenent[^notes-camps], avec le déplacement de réfugié⋅e⋅s Hmong, préalablement emcapé⋅e⋅s sous la contrainte », en Guyane pour « Développer le territoire et l'agriculture », avec le préjugé post-colonial d'un peuple « montagnard-cultivateur » donc adapté aux conditions de vie de la Guyane (et utiles à la « Nation »), avec son lot d'aberrations et de pollutions par obligations de techniques agricoles[^hmongs-france]. Ceci par la planification d'Olivier Stirn, socialiste, vice-président du parti radical et « écologiste progressiste », voulant venir en aide et sauvetage avec un « Plan vert » au bilan très contestables[^plan-vert]. Ces personnes fuyaient les désastres d'une guerre fruit de choix politique produisant des conséquences ré-instrumentalisées politiquement[^laos-huetz] [^laos-chomsky] ; avec des familles qui ont été obligées de signer des reconnaissances de dettes à des organisations « caritatives » pour payer leur « rapatriement » dans l'hexagone. Le Lieu-dit Cacao en Guyane, où de nombreuxes Hmongs ont été déplacé⋅e⋅s, devenu l'un des premiers fournisseurs de produits maraîchers du département, est aussi aujourd'hui une attraction touristique.

La catastrophe et les désastres sont bel et bien un objet politique. Et dans les choix stratégiques issus la conception que l'on se fait de la « catastrophe » les modus de réponses, et la finesse de leur élaboration, les escalades et la désescalade de phase du déploiement de secours/réponses face aux désastres impactent les efforts individuels des survivant⋅e.s d'un désastre ; efforts qui sont alors soit augmentés, détournés ou remplacés ou supprimés par les activités des secours extérieurs, de la police ou des troupes d'intervention sous le contrôle d'une autorité supra-locale.

[^notes-camps]: Notes de lectures : Les camps et enfermement(s) (2022) <https://xavcc.frama.io/notes-lecture-les-camps/>

[^hmongs-france]: *Passé le pont, vous êtes au Laos : les Hmong en Guyane* Nathalie Verhaege-Gatine, Hommes & Migrations Année 2001 1234 pp. 72-75, France, terre d'Asie. Cheminements hmong, khmers, lao, viêtnamiens

[^plan-vert]: *L'échec du Plan vert en Guyane* Par JEAN-ÉMILE VIÉ, Journal Le Monde, Publié le 01 juillet 1977

[^laos-huetz]: *Le retour des réfugiés rapatriés au Laos*, Christian Huetz De Lemps, Les Cahiers d'Outre-Mer Année 2001 54-214 pp. 216-217, Ville et campagnes d'Asie (Vietnam - Thaïlande - Inde)

[^laos-chomsky]: Une guerre américaine « camouflée ». Le Laos est devenu un champ d’expérimentation des techniques de lutte anti-insurrectionnelle, Noam Chomsky, 1970


**(8) A shift from a lower-level response (and unit of response) to higher-level response (and unit of response) entails a lost of flexibility for the lower-level unit.**

> “This corresponds to notions of dependency, previously referred to, but there is more to it than irreversible losses implied by the concept. We noted earlier the evidence that in the course of the San Francisco earthquake and fire the illegitimate insertion of federal troops, who forced mass evacuations, prevented individuals, including individuals householders and business people, from protecting their property and otherwise salvaging belongings. In the same disaster it has been shown that most people who were left without shelter were able to arrange emergency accommodations on their own. They were permitted to do so because it took some time for locally and nationally supported shelter programmes to he organised. It is a typical feature of higher-level responses that individuals and groups otherwiSe capable of coping on their own are preventing from doing so. In the absence of de-escalation, the range of normal variability with which the people can cope on their own is permanently narrow.”

J'avais détaillé ce cas dans la première fournée de notes de lecture, plus précisément dans les prémices et la prévision du Coalinga earthquake (1983) [\[…\]une étude la prédiction des tremblements de terre, une « technologie émergente » à l’époque](https://xavcc.frama.io/notes-lecture-george-E-B-morren/).

Ici le point d'argument de force de Morren est : « Lors de cette même catastrophe, il a été démontré que la plupart des personnes qui se sont retrouvées sans abri ont été en mesure d'organiser elles-mêmes un hébergement d'urgence. Ils ont pu le faire parce qu'il a fallu un certain temps pour organiser des programmes d'hébergement soutenus par les autorités locales et nationales. Les réponses de haut niveau se caractérisent par le fait que les individus et les groupes capables de se débrouiller seuls sont empêchés de le faire. »<marginnote>Sur la question de « catastrophe et colonialité », configuration spécifique dans laquelle sont à l'œuvre des milieux hiérarchisés avec une forte concentration des pouvoirs sont propices à une impunité d’un côté dominant et à la vulnérabilité de l’autre sur les dominé⋅e⋅s, c’est un espace très fertile pour les violences. Souvent, un remède adoucissant les maux sera conçu et proposé par les membres du côté élevé.  Voir : Le film Assistance Mortelle de Raoul Peck (2013). Il traite du rôle des puissances étrangères dans le pillage d'Haïti, sous couvert d'aides humanitaires, filme depuis l'immédiat après le tremblement de terre « cataclysme / catastrophe » de 2010 puis il tire les fils d’angles saillants beaucoup plus loin. R. Peck est  réalisateur, scénariste, producteur de cinéma et homme politique haïtien.  « Film coup de poing », <a href="https://journals.openedition.org/com/10479">Sociétés et espace haïtiens contemporains : nouveaux regards</a>, par Alice Corbet ; « <a href="https://www.critikat.com/panorama/festival">Quintessence et investigation</a></marginnote>. En l'absence de diminution de l'intensité de la réponse *Top to down* face aux désastres, la gamme de marges de manœuvres avec laquelle les individus (premières personnes concernées) peuvent se débrouiller seuls est réduit avec permanence.<marginnote>Pour un exemple en France, de l'année 2022, Châteauroux, en France, dans  la catastrophe récemment vécue concernant la rupture d'eau potable au robinet, par inadaptation des infras / développement d'E. Coli et impréparation des autorités, lors du dernier en date des épisodes de canicule. « Châteauroux: l'eau de la ville polluée et inconsommable en pleine canicule » Ou plus exactement le réseau d'eau courante est contaminé à l' escherichia coli et 25 000 personnes sont privées d'eau courante en pleine canicule. Dans les quartiers Nord. <a href="https://sig.ville.gouv.fr/Territoire/243600327">https://sig.ville.gouv.fr/Territoire/243600327</a>. Sur un total de 44 000 âmes pour la ville. Voix officielle : suspicion de "dysfonctionnement" dans le système de traitement de l'eau au chlore. Ajoutant du malheur à leurs détresses et précarité, la ville a déclenché son plan communal de sauvegarde et mis en place, avec le gestionnaire du réseau d'eau Saur, quatre centres de distribution de packs d'eau en bouteilles. Un dispositif bien peu adapté et peu efficace. En chiffre, 125 000 bouteilles ont été distribuées pour le vendredi suivant le premier jour de catastrophe. <a href="https://www.linfodurable.fr/chateauroux-leau-de-la-ville-polluee-et-inconsommable-en-pleine-canicule-32758">https://www.linfodurable.fr/chateauroux-leau-de-la-ville-polluee-et-inconsommable-en-pleine-canicule-32758</a></marginnote>

Cependant, les réponses déployées par des instances et institutions « de niveau supérieur », de part leurs ampleurs, peuvent étre efficaces à condition de considérer celles-ci dans leur situation *apriorique* avant, pendant et après la phase opérationnelle. Autrement dit, il s'agit de porter un regard sur la réflexité[^compte] conditionnée par la direction de la logique, ce qui mène souvent à constater l'intrication des automatismes de mépris (de classes, par racialisation, et autres ségrégations) portés sur certains groupes des populations victimes de désastres. Réflexivité est ici entendue comme le « où et le comment » se situe l'entité supra-locale qui intervient en qualité de « sauvetage » conjointement au « où et comment celle-ci situe les groupes à « sauver ». 

Nous pouvons aussi ajouter aux propos de Morren un questionnement sur des considérations préliminaires à cette réflexivité qui portent sur le sous-bassement subjectif qu'effectue le supra-local dans sa propre « évidence rationnelle » (ou rationalisation de la *vérité* qui sert de justification à sa légitimité d'intervention). Autrement dit, faire l'interrogation des démarches élémentaires dont l'enchainement est édifié comme nécessaire, face à une « calamité », selon un référentiel spécifique pré-existant.

**(9) Higher-level responses may be more effective in dealing with environmental problems and hazards of larger (relative) magnitude than lower level responses.**

> “For example, a standard (historical) response to both long and short-term local resource scarcity has been to make use of the areally integrating networks which move resources between localities. For long-term scarcity, this has involved the expansion and intensification of networks such as those cited earlier. e.g roads, pipelines, electrical grids, etc. But an irreversible dependency and commitment is involved,entailing loss of flexibility to virtually all of the local units participating though with real gains for some as well (see Hewitt, Ch. 1). The short-term situation may involve only the emergency consignment of commodities thought to be in short supply. This is potentially reversible, but not inevitably so. Where already extant, resource-moving networks are particularly vulnerable to damage in disasters and their restoration is the first order of business for higher-level units. Indeed being 'marroned' or cut off from the outside world, while great Marrons`*`, is frequently perceived by members of urban societies as worse than sustaining casualties.
The official national relief effort for San Francisca in 1906 was aimed particularly at restoring the city's function in the national economy: business, transportation, banking and the like. The post office operated throughout conflagration!”
>
> `*` *A term applied in various parts of the New World communities of runaway slaves who obviously benefited form their isolation.*

Dans cet axe argumentaire n°9 il y a deux articulations remarquables. 

1. Une population urbaine, et les secours déployés lors d'une catastrophe auprès de celle-ci, porte un jugement de *pathos* sur un groupe externe constitué d'individus dont le stéréotype, induit par racialisation dans l'exemple ici, est peu visible, car minorisé, dans cette population urbaine.
  + Il y a une condescendance
  + Il y a un régime établissant qui a le « mérite » de l’atermoiement, et de la charité ordonnée comme juste.
2. La priorité est mise à profit du rétablissement de la fonction de la ville dans l'économie nationale : commerces, transport & infrastructures, banque et autres.
  + Il s'agit de rétablir une fonction dans un système reconduit face à un évènement, parfois dit « naturel », parfois dit industriel ou sociale ou autre, qui par son intensité et/ou son « irruption » dans ce même système, et sur ces infrastructures, provoque une rupture dans les conditions de vie, dans les limites de ce qui est acceptable et parfois dans les limites de ce qui est désirable.

J'ajoute que ce sont toujours les « petites mains », souvent invisibles et issues de groupes les plus dominés, qui sont les chevilles ouvrières des rouages dans la fonction de la ville dans l'économie nationale.


**(10) There is an apparent paradox of order and disorder created by the interplay or points (8) and (9) above, corresponding to the difficulty of assessing balance between the real benefits and real cots of escalation from phase to phase.**

> “According to Bateson (1972b. *p. 498*), it is a paradox that authorities, including planners and analysts, need consciously weigh, and it is, I assert, weighted by ordinary people often to the consternation of such authorities. Thus in the aftermath of the 1976 earthquake in eastern Turkey, which effectively destroyed most shelter in the area, government officials laboured vigorously to relocate survivors from rural to urban areas in anticipation of harsh winter conditions. On their part, many common people resisted this with equal vigor in anticipation of the loss of their livelihood *[subsistence ►►]*<marginnote>the quality or state of being lively</marginnote>, and the fate of their livestock, which would not be able to survive without human care.”

Pour Morren, il y avait à l'époque de cette publication, une réelle difficulté, même un paradoxe −− un « contraire à l'opinion commune », dans le fait d'établir une balance et une évaluation entre les différents niveaux de réponses, et leurs intensités associées, notamment au regard des 2 précédents axes traités (8 et 9).

![](/assets/images/bateson-1977.png)
<figcpation class="caption">The Dynamics of Ecological Crisis. "Steps to an Ecology of Mind", Bateson, 1972, p. 491.</figcpation>
<br />
<marginnote>Passé le temps l'industrialisation et la concentration de populations conséquente, des pollutions peuvent apparaitre, comme celle des sols. Les personnes ayant été forces de production ouvrière, et leurs descendances, pourront alors être redéplacées, sous couvert de prévention sanitaire et aussi par « programme de rénovation urbaine » des terrains pollués, ouvrant le foncier à des personnes et organisations possédant un plus grand pouvoir économique, comme c'est le cas actuellement à Liège, en Belgique. L'« état d'esprit de l'écologie » c'est aussi cela, encore cela, aujourd'hui et pour une partie de la population</marginnote>

Il prend comme exemple le tremblement de terre de 1976 dans l'est de la Turquie, qui a détruit la plupart des habitats et autres abris de la région. Le tremblement de terre [Çaldıran–Muradiye](https://en.wikipedia.org/wiki/1976_%C3%87ald%C4%B1ran%E2%80%93Muradiye_earthquake), à la frontière de l'Iran et de la Turquie, à la suite duquel les responsables gouvernementaux turcs se sont efforcés de reloger les survivant⋅e⋅s des zones rurales vers les zones urbaines en prévision des conditions hivernales difficiles. C'est aussi en Turquie un moment de politique avec des choix effectifs liés à l’industrialisation qui a activé la croissance des besoins d'emplois non agricoles, donc d'une main d'œuvre disponible dans des zones conçues à cet effet.

![](/assets/images/1976-Caldiran_Page_41-495x400.jpg)

+ Voir également :
  + [URBANISATION, INÉGALITÉS URBAINES
ET DÉVELOPPEMENT EN TURQUIE (1950-2000)](https://regionetdeveloppement.univ-tln.fr/wp-content/uploads/8-CatinKamal.pdf), Maurice CATIN et Abdelhak KAMAL, Région et Développement n° 34-2011
  + [Le passé et le présent des politiques d’urbanisation et de
logement en Turquie](https://shs.hal.science/halshs-03479632/file/article-NAQD_version-auteur.pdf), <cite>Gülçin Erdi</cite> (2021)

Nous sommes là dans une logique qui me semble assez bien représentée par le diagramme schématique de Bateson. Je suis aussi tenté de le reprendre et lui apporter des précisions et y faire apparaitre des subtilités plus fines. Peut-être plus tard, un jour peut-être….

**(11) There are potontially three kinds of responses involved in response processes: those which restore equilibrium by effectively dealing with the problem(s) at hand, those which restore flexibility to other responses, and those which do both.**

> “This is more general statment of the considerations embodied in Slobodkin's (1968) view that 'organisms repsond to rapidly fluctuating features of their environment behaviourally and to slower fluctuations physiologically, and just as learning restores flexibility to behaviour, so does adaptation restore flexibility to physiological responses'. Equilibration is concerned with 'normal' environmental variations with which people and other organisms can cope 'routinely', whereas flexibility restoration is concerned with extreme or survival/threatening variations exceeding the capacity of equilibrating  responses at a given level. The restoration of flexibility involves either a return to a facsimile of the previous equilibrium situation or the establishment of a new equilibrium. Elsewhere, I have argued that demographic responses restore flexibility to resource management by providing the circumstances under which intensification may occur and that, similarly, certain innovations in the sphere of resource management may restore flexibility to demographic responses in populations experiencing 'population pressure' (<cite>Little & Morren</cite> (1976), *p. 26*)”.

Il y aurait ainsi potentiellement trois types de réponses impliquées dans les processus des interventions en contexte de désastres : 

+ celles qui restaurent un équilibre en opérant une performance sur le(s) problème(s) en cours, 
+ celles qui restaurent la flexibilité des autres réactions possibles, 
+ et celles qui font les deux.

Morren appuie son affirmation de typages sur Slobodkin dans "toward a predictive theory of evolution", in *Population biology and evolution* éditons R. C Lewontin (1968). Slobodkin était écologue et scientifique, considéré comme pionnier, spécialiste des dynamiques des populations. Je n'ai pas eu accès à cette publication (grande tristesse !). La réputation d'enseignement provocateur et d'une démarche de visionaire pour Slobodkin, aussi auteur de “A Citizen's Guide to Ecology”, est toujours vive (mort en 2009). En 1968, Slobodkin a quitté le Michigan pour rejoindre la faculté de SUNY Stony Brook en tant que professeur de biologie et directeur fondateur de son département d'écologie et d'évolution, l'un des premiers départements de ce type aux USA.

Ici, il est marquant de relever que la « normalité comportementale » (initiale et désirée en retour après catastrophe) est écrite avec des *pincettes*. Aussi il est considéré dans l'affirmation de l'argument qu'est considéré les individus et les autres organismes vivants, humain⋅e⋅s et non-humain⋅e⋅s pourrait-on écrire aujourd'hui. L'accointance entre anthropologie et biologie pour tendre vers une forme d'union dans l'écologie me semble importante, bien qu'avec peine de solidité scientifique ici (je suis un posteriori confortable là `{0_>}`, rien de plus).

Il y a ici une convocation, à demi-mot, d'un ersatz d'*instrument optique* issu de la biologie. Dans le sens où un regard est déployé depuis l'appel d'un angle ayant son *sommet de raison à l'évidence* l’œil d'observation *depuis ce qui ce fait en biologie* et dont les bords passent par les extrémités d’une ligne considérée, ici la « catastrophe » sous la loupe particulière de l'“Identification of Hazards and Responses”. C'était osé à l'époque, et l'est probablement toujours. D'autres bien avant allaient piocher dans la biologie des angles pour servir des justifications ailleurs, certains courants de pensées politiques continuent cet alambiquage.

Ensuite Morren fait référence à “[Ecology, energetics, and human variability](https://archive.org/details/ecologyenergetic0000litt)”, Little & Morren (1976) (dispo dans Internet Archive, entre autres puits de ressources libres d'accès) pour faire intervenir la question démographique dans les choix opérationnels face à un « problème du genre désastre ».<marginnote>Il est salutaire de porter une analyse et un regard critique sur les représentations graphiques et schématiques « mécanisantes » de nos fonctionnements. Comme dans la thèse de Fabrice Sabatier, « Saisir l’économie par le(s) sens. Une approche critique et sorcière de la visualisation de données économiques par le design » (2021). Les fonctionnements humains ne sont pas inscriptibles dans un circuit imprimé et nous n'avons, jusqu'à preuve du contraire, que peu ou prou en commun avec des comportements de machines automatisées par un chemin et des calculs de prédicats</marginnote>

![](/assets/images/little-morren.png)
<figcaption class="caption">Capture d'écran depuis la version “Ecology, energetics, and human variability” disponible dans Internet Archive, p. 80 & 81</figcaption>

<div class="breaker"></div>

**(12) For individuals and groups of various kinds the ultimate response is the abandonment of group (or community) way of life, or territory.**

> “This kind of response is triggered by some form of coercion, immediate or chronic: persecution, economic and social alienation, loss of life-support, banishment, permanent refuge from disaster, forced relocation, or a combination of such factors. It may be seemingly voluntary as with members of millenarial cults or farmers who sell their land to developers, but the wider context of such choices is, nevertheless, coercive. Minimally people find themselves in situations in which they perceive themselves to have no other alternatives, possibly excepting violence<marginnote>Je me souviens ici, souvenirs des années 2000 et plus, de <a href="https://echelleinconnue.net/eichange/">Délit de lire</a>, en signe de solidarité avec les interpellés du 11 novembre 2008 en France, et les fers à béton ; et aujourd’hui « <a href="https://www.echelleinconnue.net/accueil/revue_11.php">Écrire sur les ruines du futur</a>. In any event, radical changes in life-style, often irreversible, are involved.”

Cet axe argumentaire résume avec puissance l'état de l'art de la « réponse », par des entités instituées au niveau supra, face à la catastrophe. Je vais le traduire sans autre commentaire.

Pour les individus et les groupes de toutes sortes, le summum de réponse est l'abandon du mode de vie du groupe (ou de la communauté), ou du territoire.

Ce type de réponse est déclenché par une certaine forme de coercition, immédiate ou chronique : persécution, aliénation économique et sociale, perte des moyens de subsistance, bannissement, mise en refuge permanent en prévention d'une catastrophe, déplacement forcé, ou une combinaison de ces facteurs. Il peut s'agir d'un choix apparemment volontaire, comme dans le cas des membres d'une société cultuelle séculaire ou des agriculteurices qui vendent leurs terres à des promoteurs, mais le contexte plus large de ces choix est néanmoins coercitif. Minimalement, les personnes se retrouvent dans des situations dans lesquelles iels perçoivent qu'iels n'ont pas d'autres alternatives, à l'exception peut-être de la violence. En tout état de cause, il s'agit de changements radicaux, souvent irréversibles, dans le mode de vie.

**(13) Survival (according to Slobodkin 1968, *p. 191*) in its various dimensions and degrees is the true measure of the effectiveness of responses, rather than measures of efficiency or optimisation and the like.**

> “A number of more practical tests of effectiveness of responses can also be applied: (a) the absence of escalation, because lower-level responses 'act as a bulwark' against the necessity for higher-level responses (Slobodkin 1968, p. 198); (b) the reversal of the process of response; (c) the reduction or mitigtion of the intensity of a particular problem or its manifestations by a response (Morren 1977, p. 276, 283-4); and (a) the biological state of the members of a population (or its demographic state) with respect to the problem or hazard in question (<cite>Morren</cite> (1977), *p. 276, 284*).”

Nous sommes en 1983, ce livre “Interpretation of Calamity From the Viewpoint of Human Ecology” marque un tournant radical dans la conception occidentale de l'écologie et aussi dans l'anthropologie de la catastrophe. Il se termine sur cet axe argumentaire :

La survie (selon Slobodkin 1968, p. 191), dans ses différentes dimensions et degrés, est la véritable mesure de l’efficacité, plutôt que les mesures d'efficience (de productivité), d'optimisation, etc.
Avec :
+ (**a**) l'absence de montée en intensité des modalités de réponses, puisque que les réponses de niveau inférieur « agissent comme un rempart » face à l'impératif de réponses poussé par le niveau supra.
+ (**b**) la réversibilité du processus de réponse
+ (**c**) la réduction ou l'atténuation de l'intensité d'un problème particulier ou de ses manifestations par une réponse
+ (**d**) l'état biologique *[comprendre ici dans le sens de l'état de santé.]* des membres d'une population (ou son état démographique) en relation sur le problème ou sur le risque aléatoire en question.

Slobodkin était aussi une référence importante dans l'essai de Vayda and McCay (1975) sur lequel s'appuyait Morren dans l'axe 7, axe au sein duquel la référence à l'essai de Vayda et MacCay est construit atour de nombreux arguments pris encore dans la biologie.

Dans l'auto-réference de Morren de 1977, il s'agit d'une monographie dans laquelle il mobilise les concepts énergétiques pour étudier le contrôle par l'humain des flux d'énergie<marginnote>J'invite ici à prendre les devant pour discuter avec Rieul Techer et son travail démarrant à l'EHESS</marginnote>. Importance de l'élevage et de la consommation du porc, avec une comparaison entre trois systèmes : celui des Miganmin, celui des Maring et celui des Raiapu Enga. Cette étude est réutilisée comme source dans « [Collaboration et convergence des disciplines en écologie humaine : les enseignements d'une recherche dans la commune de **Vidauban (Var)**](https://www.persee.fr/doc/medit_0025-8296_1982_num_46_3_2091) » en 1982, par <cite>R. Eches et M. Marchetti</cite>.

> « Dans le cadre d’un échantillon de sociétés de la Papouasie, Nouvelle Guinée, Morren a démontré (1977) que l’intensification de la culture de la terre s’est accrue avec le déclenchement de l’élevage porcin ; bien que ce dernier compromette l’autonomie politique locale, l’égalité sociale, l’autarcie économique jusqu’à l’équilibre écologique de longue durée, il renforce l’autorité des « Big Men » au centre de l’organisation » (*Essai dans le cadre du Néolithique ancien méditerranéen pour extraire de sa coquille le « facteur social »*, <cite>James Lewthwaite</cite> (1987). Dans *Premières communautés paysannes en Méditerranée occidentale*, <cite>Jean Guilaine, Jean Courtin, Jean-Louis Roudil, et al</cite>).

La commune de Vidauban (Var) a évidemment un plan de prévention des « risques » :

+ Feu de forêt Plan de prévention depuis 1983
+ Inondation Plan de prévention depuis 1997, Arrêtés de catastrophes naturelles 1983
+ Mouvement de terrain Arrêtés de catastrophes naturelles 1998
+ Autres risques
  + Rupture de barrage
  + Séisme zone de sismicité 2
  + Transport de marchandises dangereuses

Un plan avec sa logique, avec ses décisions politiques, sa doctrine, sur des infrastructures, avec des orientations techniques et méthodologiques, jusque dans leurs conséquences avant, pendant et après une catastrophe.

<div class="breaker"></div>

## Remerciements

+ Mayhoua et Noush

# Notes et références

[^compte]: Un homme bien apprêté, d’une ville au loin à l’est du pays, certainement fait de diplômes et de savoirs, vint du coté de Penmarc’h. Il avait entendu dire que l’eau de mer soignait quelques maux dont souffrait son épouse. Arrivé face à la côte il vit un pécheur. « Est-ce vous le maître de cette étendue ? »<marginnote>Il y a dans cette petite histoire, et plus encore dans le livre « À la gloire des manants » (Pierre-Jakez Hélias, 1977), une poésie et une mise en récit conté de la puissance narrative des histoires des personnes vues comme « exotiques » par des « grosses-têtes » qui, elles, dorment et rêvent sur leur oreiller de privilèges. Ce confort amène régulièrement ces « grosses-têtes » à produire des analyses, des autres récits, des livres, qui parlent, avec toute exoticité, des « manants » qui ne se « laissent pas aller, ne se laissent pas faire » avec une forme de roman dont les cadres et les normes sont établis, sous gros biais, sur les oreillers et les songes des privilégiés. C’est ré-écrire un éthique et une philosophie non vécue et non pratiquée, tout en reléguant à une position sub-alterne les sujets pratiquants et vivants ; c’est faire commerce de quelques choses extraites et réappropriées tout en dépossédant, même de l’immatériel, les premières concernées. Ce que firent et font encore des personnes des « sciences » dans divers coins du monde.</marginnote>. « Pour sûre ! Quand bien même Dieu m’y reprend de menues aubaines, j’en suis le maître et teneur ». « Pourriez-vous m’en vendre quelques quantités ? » Le marin remplit un tonneau et cents litres. L’homme du monde repartit plein de satisfactions et sa femme fut régalée. Le bigouden pris fortune et écuma les ribouldingues, rinça ses fiers semblables. L’an écoulé le bien habillé revint dans le pays face à la mer. Elle était retirée au loin, si loin qu’il ne pouvait presque plus apercevoir le bateau ni le marin. Il songeât alors que le bougre avait fait prospère commerce et surtout fortune jusqu’à vider son stock puis construire le chateau qui pointait dans le bois voisin. Il repartit vers la ville avec le sentiment d’avoir aidé un des petites gens à s’élever vers la civilisation et dans la société. **Histoire de Bigoudenie**.
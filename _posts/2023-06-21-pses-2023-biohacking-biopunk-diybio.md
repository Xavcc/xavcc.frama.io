---
title: "PSES 2023 : Biohacking, Bio Punk, DIY Biologie"
layout: post
date: 2023-06-21 05:20
image: /assets/images/pses-2023-conf.png
headerImage: true
tag:
- hsociety
- Biohacking
category: blog
author: XavierCoadic
description: petites histoires de libertés dans la cité
---

Pas Sage En Seine, édition 2023, où nous avons tenté de parler de [Biohacking](https://fr.wikipedia.org/wiki/Biohacking) dans la très agréable médiathèque Aragon de Choisy-le-Roy.

> « Né dans la (sous)culture du hacking en 1988, le [biohacking](https://fr.wikipedia.org/wiki/Biohacking) et la biologie à faire soi-même se lient à l’histoire du numérique et aux enjeux de liberté et droits fondamentaux, notamment au travers des questionnements des droits fondamentaux. Des communautés de pratiques ont forgé une diversité de mise en action qui traite par exemple de médicaments sous licence libre, d’auto-gynécologie, des rapports entre humains et non humains, des concepts d’intimité et de vie privée… Avec une "géographie" politique et des antagonismes ».

**Se doter, du moins en essayer de nous doter *nous-même*, des modalités et des moyens de discuter et de pratiquer et d'éditer nos règles aussi de faire par nous, pour nous, avec d'autres, et pour faire communS**.

![](/assets/images/pses-2023-conf.png)
<figcaption class="caption">Description de l'image : Capture d'écran de la diffusion vidéo de la conférence « Biohacking, Bio Punk, DIY Biologie » avec en haut à gauche la diapositive d'introduction de la conférence et en bas à droite un insert de caméra sur la personne qui parle (moi, pieds nus) avec un masque de protection respiratoire FFP2 noir</figcaption>
<br />

La vidéo de la conférence est disponible [ici](https://video.passageenseine.fr/w/pcbbNpEaq2GRKbnjdEZamB) (Licence CC By Sa, Attribution - Partage dans les mêmes conditions, via peertube). Les diapositives utilisées sont disponibles [ici](https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=biohacking-pses-2023.pdf) (sous [licence Art Libre 3.0](https://artlibre.org/)). Une session similaire avait eu lieu lors des « Journées des Libertés Numériques » à Nantes, avril 2022 ([vidéo](https://videos.lescommuns.org/w/jpHcvAV5AA6GaWeLs6LGCb)).

## Bibliographie et références mobilisées et utilisées dans la conférence

+ « Forum: Roses are black, violets are green - The emergence of amateur genetic engineers » ([archive](https://archive.wikiwix.com/cache/?url=https%3A%2F%2Fwww.newscientist.com%2Farticle%2Fmg12516984-100-forum-roses-are-black-violets-are-green-the-emergence-of-amateur-genetic-engineers%2F), New Scientist)
+ <cite>Michael Schrage</cite>, « Playing god in your basement » ([archive](/https://archive.wikiwix.com/cache/?url=https%3A%2F%2Fwww.washingtonpost.com%2Farchive%2Fopinions%2F1988%2F01%2F31%2Fplaying-god-in-your-basement%2F618f174d-fc11-47b3-a8db-fae1b8340c67%2F), The Washington Post, 31 janvier 1988.)
+ <cite>Fabrice Cahen</cite>, <cite>Virginie Rozée Gomez</cite>, <cite>Simone Bateman-Novaes</cite> et <cite>Emmanuel Betta</cite>, Procréation et imaginaires collectifs : fictions, mythes et représentations de la PMA : [actes de la journée d'études internationale "Reproduction médicalement assistée et imaginaires sociaux" tenue à l'Ined de Paris le 29 novembre 2018], 2021 (ISBN 978-2-7332-9052-1 et 2-7332-9052-5, OCLC 1291400539, [lire en ligne](https://www.worldcat.org/oclc/1291400539) [archive](https://www.worldcat.org/oclc/1291400539))
+ <cite>Thierry Hoquet</cite>, <cite>Francesca Merlin</cite> (Dir.). Précis de philosophie de la biologie. Ed Vuibert, pp.XIII-354, 2014, Philosophie des sciences, 978-2-311-40019-9. ⟨hal-01423651⟩
  + le terme biologie en tant que science, et ça c'est assez peu connu en tant que domaine scientifique et défini pour la première fois en 1766  Michael Christoph Hanow ; puis en 1797 par les médecins Roose, Burdach et Treviranus (1800 -1802) puis par Lamarck (1800 - 1802) naturaliste, botaniste, zoologiste.
+ « L'amateur: une figure de la modernité esthétique », <cite>Laurence Allard</cite>, Communications, 1999
+ <cite>Élise Rigot</cite>, dans : « Ex0167 Design embarqué en laboratoire de biologie », CPU radio, <https://cpu.dascritch.net/post/2021/06/24/Ex0167-Design-embarqu%C3%A9-en-laboratoire-de-biologie>, le Design pourrait avoir comme première émergence historique l'année 1750
+ <cite>Max Weber</cite>, la *Modernité* se caractérise par l'intellectualisation de l'objet qu'on travaille et la spécialisation
+ « Collectionneurs, amateurs et, curieux , Paris, Venise : XVIᵉ - XVIIIᵉ » siècle, <cite>Krzysztof Pomian</cite>
+ Les Indie Camps <https://movilab.org/wiki/IndieCamp>, « Vis ma vie d’IndieCamper en Bretagne », dans la revue Makery <https://www.makery.info/2017/07/18/vis-ma-vie-dindiecamper-en-bretagne/>
+ « Esquisse d’une théorie de la magie », <cite>Marcel Mauss</cite>, dans l’*Année sociologique*, 1904
+ « L'Art et ses agents. Une théorie anthropologique », d'<cite>Alfred Gell<cite> : l'art comme technologie de l'enchantement, <https://books.openedition.org/ugaeditions/596> ; <https://hal.parisnanterre.fr/hal-01658893>
+ <cite>Luis Felipe R. Murillo</cite> Revue MAUSS - Complément du n°56 : Magie et/comme hacking, 2020, <https://journaldumauss.net/?Complement-du-no56-Magie-et-comme-hacking>
+ Par <cite>InterHack</cite> « UNE INTRODUCTION AUX HACKERSPACES ET À LEURS ACTIVITÉS », archive <https://web.archive.org/web/20220131181327/https://interhacker.space/>
+ "How Power Pervades Portrayals of Human Evolution", <cite>Rui Dogo</cite> <https://www.sapiens.org/biology/racism-sexism-human-evolution/>
+ « Ni Dieu ni gène. *Pour une autre théorie de l'hérédité* » <cite>Jean-Jacques Kupiec</cite> et <cite>Pierre Sonigo</cite>
+ Lorsque <cite>Orwell</cite> écrit dans 1984, publié en 1949, un slogan très connu et repris largement : « La guerre c'est la paix, la liberté c'est l'esclavage, l'ignorance c'est la force ». En fait, Orwell remix là une œuvre classique plus ancienne. Three Witches, dans Macbeth de <cite>Shakespeare</cite>, 1602 - 1603, qui représentent le chaos, le conflit, le mal, la noirceur (oui, 4 fléau car 1 chacune et lorsque réunies un 4 devient possible). Elles sont des agents de témoignage d'interprétation de la réalité et procède à l'influence sur Macbeth, ses capacités de décisions prises ou non. Première ligne du premier acte
  > « Ce qui est juste est injuste et ce qui est injuste est juste
  >
  > Planons (godons) dans le brouillard et l'air crasse »
  >
  > *"Fair is foul, and foul is fair / Hover through the fog and filthy air"*
    + En fait ces trois sorcières proviennent des Holinshed's Chronicles, 1577, Bretagne, Ireland, Écosse.
    + Le concept des trois sorcières a peut-être été influencé par le poème skaldique Darraðarljóð, dans lequel douze valkyries tissent et choisissent qui doit être tué à la bataille de Clontarf (qui s'est déroulée près de Dublin en 1014). <https://en.m.wikipedia.org/wiki/Three_Witches>
    + Une apparition dans la littérature anglophone qui elle même empreinte aussi beaucoup aux figures des mythologies romaines et grecques, par exemple les Moires. <https://fr.wikipedia.org/wiki/Moires>
    + Vous retrouvez aussi ces 3 sorcières dans le roman Dracula, 1897.
+ Pour les sorcière, le démonisme et plus, voir : <https://notecc.kaouenn-noz.fr/doku.php?id=pages:norae:hsociety:hacking_notes-sorcieres-1>
+ Pee Prophets, 2 300 ans avant J.C, <https://sitn.hms.harvard.edu/flash/2018/pee-pregnant-history-science-urine-based-pregnancy-tests/>
+ the medical Papyri of ancient Egypt <https://en.wikipedia.org/wiki/Egyptian_medical_papyri> ; Papyrus Berlin 3038 <https://en.wikipedia.org/wiki/Brugsch_Papyrus> : Seeds of Contend / Bioassay in your own dirt in Hackteria <https://www.hackteria.org/wiki/Seeds_of_Contend_/_Bioassay_in_your_own_dirt>
+ « Sorcières, sages-femmes et infirmières. » , Une histoire des femmes soignantes <cite>Barbara Ehrenreich</cite> & <cite>Deirdre English</cite>. Traitant notamment de l'évolution des agentes de la santé vers une santé contrôlée par les hommes puis instituées et enfin industrialisée. Passant par les révoltes et révolutions féministes, les sectes, le rapport aux corps. C'est assez centré USA, un peu Amérique du nord. La première partie du livre est « très légère » en qualité et travail de filiation et sources. En autre, je regrette beaucoup l'économie de traitement des travaux de [Margareth Murray](https://fr.wikipedia.org/wiki/Margaret_Alice_Murray), « Culte de la Sorcière en Europe occidentale », 1921, et les polémiques qui ont accompagnées la compréhension et diffusion de cette recherche anthropologique.
+ La représentation des sorcières vient de celle des brasseureuses ? Le chaudron était celui du brassage, le balai servait à indiquer que la bière était disponible, le chat chassait les souris, le chapeau les distinguait sur les marchés. <https://fr.wikipedia.org/wiki/Alewife>.
+ "At The Gates" « Dehors, les gardiens de la morale, dehors, les individualistes, dehors, les réactionnaires. La jeunesse est à vos portes ! » At the Gates met à l’honneur des artistes qui ne demandent pas la permission. <https://www.la-criee.org/fr/at-the-gates/> ; à la Criée de Rennes, 2019; histoire politique et sociale de l'intime, lutte des Femmes, défiant les lois, pour leurs émancipation et Liberté. 
Exemples : Jesse Jones, appropriation de la sculpture Irlandaise Païenne Matriarcale Sheela Na Gig, montrant ses dents et exposant sa vulve.Juste dessous, marteaux en spirale étoilante, outils d'émancipation, avec inscription « Thou shalt Not Suffer » détournement du Malleus Maleficarum, Livre ignoble du XV siècle qui est un Manuel de chasse, d'oppression et de torture des Femmes, appelées sorcières lorsqu'elles sont non soumises au dictat. L'autrice coupe alors « Thou shalt Not Suffer a witch to live » (« tu ne permettras point aux sorcières de vivre ») pour marteler un « tu ne souffriras point ». Une broderie de Teresa Margolles, accompagnée avant la salle noir d'une vidéo immanquable. Broderie avec des Femmes Mayas à partir des linceuls des Femmes mortes par violence. Plongée dans la salle totalement noir après avoir visionné la vidéo sur les discussions qui accompagnent la confection de cette œuvre, objet traumatique et expression de compassion à la fois. Beaucoup d'autres œuvres et Histoire à y découvrir avec des Femmes Mayas à partir des linceuls des Femmes mortes par violence. <https://web.archive.org/web/20190820232522/https://www.la-criee.org/fr/at-the-gates/>
+ Film, "I Am Not A Witch", par Rungano Nyoni, 2017
+ "REASSURED diagnostics to inform disease control strategies, strengthen health systems and improve patient outcomes", <cite>Kevin J. Land</cite>, <cite>Mabey, D.</cite>, <cite>Peeling, R. W.</cite>, <cite>Ustianowski, A.</cite> & <cite>Perkins, M. D</cite>, et *alii*.
+ "ADVENTURES IN SYNTHETIC BIOLOGY" <https://openwetware.org/wiki/Adventures> (fanzine), voir aussi cette page wiki de recensement <https://www.hackteria.org/wiki/Ziness>
+ Open Wet Ware <https://openwetware.org/wiki/Main_Page>
+ Un Manifeste Biopunk <https://xavcc.frama.io/biopunk-manifesto/>
+ Convention ouverte MTA <https://fr.wikipedia.org/wiki/Convention_ouverte_MTA>
+ Mind Th_GAP project <https://thgap.hackteria.org/WorldCongress/>
+ Biohacking Safari <https://web.archive.org/web/20221003011247/https://makezine.com/article/maker-news/get-to-know-biohacking-safari/>
+ Le réseau Hackteria <https://www.hackteria.org/>
+ zine of the Transgenic Human Genome Alternatives Project (thGAP) − Only with collaborative help and participation, they could make this happen and collect inputs for the CGCB - Creative Germline Constructs Bank, which is still Open for World Commentary! <https://archive.org/details/thGAP/thGAP_booklet_screen/>
+ GynePunk et leurs réseaux <https://www.hackteria.org/wiki/GynePUNK>
+ THE CRITICAL ENGINEERING MANIFESTO <https://criticalengineering.org/>
+ <cite>Léna Dormeau</cite>, « Habiter l’instabilité ; vivre dans les interstices du monde », En marges ! – l’intime est politique – revue de sciences humaines et d’arts, N°7, « Les corps hors-normes », 2021 <https://enmarges.fr/2021/12/15/habiter-linstabilite-vivre-dans-les-interstices-du-monde/> ; « Pour une épistémologie liminale », <https://reflexivites.hypotheses.org/11594>
+ « À la marge des sciences institutionnelles, philosophie et anthropologie de l'éthique du mouvement de biohacking en France » par <cite>Guillaume Bagnolini</cite>, 2018
+ "Biohacking Gender", <cite>Hil Malatino</cite> <https://en.annas-archive.org/md5/56bc2ed31b4137cc07a0c08d8cedc84c>
+ “Medical histories, queer futures: Imaging and imagining ‘abnormal’ corporealities” , by Hil Malatino (<https://wgss.la.psu.edu/people/hjm30/>) [TW: genitals images taken form Institutes at the paper] <https://www.gla.ac.uk/media/Media_180307_smxx.pdf>
+ Collaborative and interdisciplinary research, Open Source Estrogen combines biohacking and artistic intervention to demonstrate the entrenched ways in which estrogen is a biomolecule with institutional biopower. It is a form of biotechnical civil disobedience, seeking to subvert dominant biopolitical agents of hormonal management, knowledge production, and anthropogenic toxicity. Thus, the project initiates a cultural dialogue through the generation of DIY/DIWO (do-it-yourself/do-it-with-others) for the detection and extraction of estrogen, and contextualized as kitchen performance and queer body worship. <https://media.ccc.de/v/34c3-9036-open_source_estrogen>
+ Tract de recherche, « Prendre soin, prend pouvoir en cuisine » <https://wiki.kaouenn-noz.fr/_media/tract-cuisine-pandelakis.pdf> (Queer)
+ <cite>Vanessa Lorenzo</cite> <https://hybridoa.org/> ; <https://festival2021.rixc.org/vanessa-lorenzo/>
+ <cite>François-Joseph Lapointe</cite>, « Danser son ADN et modeler son microbiome » par <cite>Marianne Cloutier</cite>
+ « Gènes, génies, gênes » de <cite>Jens Hauser</cite>
+ « Qu'est ce que le non humain fait au genre ? » de <cite>Luca Greco</cite>

<div class="breaker"></div>



---
title: "Les Mains Dans Le Temps : Introspection et suspension en DIYbio − Ep.1"
layout: post
date: 2020-05-25 12:40
image: /assets/images/mur_nantes_nomad-2015.jpg
headerImage: true
tag:
- hsociety
- Lmdlt
category: blog
author: XavierCoadic
description: Randonnée et pensées adressées aux travers de pratiques
---

Le corps enfermé plus de 60 jours, l'esprit occupé presque autant dans la force du quotidien, j'avais décidé de marcher pour m'aérer.

Profitant des petits pas dans une *nature* proche de la ville densifiée, je m'exerce à une nouvelle pratique. Exprimer à voix haute en temporalité de randonnée des interrogations dans l'intention de les partager à un ami, et finalement à des personnes qui habitent, perçoivent, conçoivent le monde.

Do It Yourself Biology, biohacking[^1], tiers-lieux qui trouve une origine dans la définition du purgatoire entre le XII<sup>e</sup> et le XIII<sup>e</sup> siècle[^2], faisons un pas de côté pour préparer dans de prochains épisodes des descriptions de phénomènes rencontrés.

Bien au-delà des techniques, des sciences et de l'ingénierie, voici une tentative d'émission de réflexion. Paroles, bruits, ambiance et musiques, propulsées avec sincérité. Aiguisons nos couteaux :

<iframe width="100%" height="150" scrolling="no" frameborder="no" src="https://open.audio/front/embed.html?&amp;type=track&amp;id=87210"></iframe>
<figcaption class="caption"><strong>Randonnée et pensées adressées aux travers de pratiques.</strong> <i>Vocale : Xavier Coadic. Montage, mixage, production : Eorn, doctorant en écologie marine, étude des cyanobactéries</i>. Licence CC BY SA 4.0 XavCC & Eorn. Pour la chaine de podcasts <a href="https://open.audio/channels/horscircuit/">Hors Circuit</a> libre et en sources ouvertes</figcaption>
<br />

Suspendre des phénomènes que nous appréhendons bien ou mal gré lorsque nous nous penchons sur des rapports au monde et des productions au travers lesquelles nous espérons poser des questions saillantes et parfois apporter de petits bouts de réponses constructives.

Le corps fut confiné, presque immobilisé ; remis en effort semblable à la première épreuve qui suit une lourde blessure ; rouvrant une mobilité qui revient en affinité avec des sensations. Une pérégrination qui dans cette *release* servit à aérer l'esprit et se termina dans une dernière ligne de 5 km de souffrance dans la chair. Le corps meurtri et l'esprit soulagé.

Plus ou moins 23 km loin de la course effrénée dans la paraphrénie de l'innovation.

## Notes et références

[^1]: ElectroPen: An ultra-low–cost, electricity-free, portable electroporator <https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.3000589>

[^2]: Selon l'historien Jacques Le Goff, c’est entre 1170 et 1180 qu’a lieu la « naissance » du Purgatoire dans le milieu intellectuel parisien. La purgation cesse d'être un état pour devenir un lieu. Cette évolution se fait conjointement à l'apparition progressive, au haut Moyen Âge, du concept de péché léger <https://fr.wikipedia.org/wiki/Purgatoire>
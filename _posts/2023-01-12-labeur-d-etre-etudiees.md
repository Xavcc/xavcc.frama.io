---
title: "Le labeur d'être étudi⋅é⋅e⋅s et questions du temps, obligations et charges"
layout: post
date: 2023-01-12 07:40
image: /assets/images/hotel-pasteur-dec-2022.jpg
headerImage: true
tag:
- hsociety
- Anthropologie
category: blog
author: XavierCoadic
description: "La politique de l'enquête sur les travailleureuses"
---

En parcourant le [Cabaret Common](https://cwrc.ca/project/cabaret-commons), "An Online Archive & Anecdotal Encyclopedia for Trans-Feminist and Queer Artists, Activists and Audiences", j'ai trouvé un texte qui me semble incontournable :

> « Cet article aborde les logiques économiques de la recherche universitaire communautaire et de la collecte d'archives, et propose un système de comptabilisation du travail collaboratif à travers différents lieux au sein des scènes sous-culturelles, et du travail d'être étudié dans un milieu académique-culturel qui camoufle de plus en plus le travail affectif libre en collaboration et recherche-co-création. Nous examinons ici la manière dont une économie universitaire 2.0 se nourrit des valeurs de partage de communautés [communality] qui étaient autrefois la marque de fabrique des scènes subculturelles contre-institutionnelles et nous suggérons qu'en introduisant des mesures comptables dans le cadre d'une praxis de recherche, nous pouvons étudier les conditions matérielles qui constituent les relations de production de la recherche. » "*[The labour of being studied in a free love economy. The politics of workers' inquiry](https://web.archive.org/web/20200724054749/http://www.ephemerajournal.org/sites/default/files/pdfs/contribution/14-3cowanrault.pdf)*" <cite>T.L. Cowan & Jasmine Rault</cite> (2014), under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

En fin de ce texte des mesures d'atténuation face aux problèmes, et parfois toxicité, qu'apportent des recherches / chercheureuses universitaires lorsqu'iels interviennent avec leur logique spécifique, et leurs privilèges, de déploiement d'une charge sur et dans des groupes qui ne sont pas autre *chose* que des objets d'études. Dans cette tentative d'atténuation deux points d'articulations sont privilégiés : la comptabilité / logique comptable et le temps (comme « ressources à compter et à rendre visible »). En somme, c'est donc une logique de solution planifiée dans le cadre productif. Je rappelle que ce qui est vu par « épistémologie de point de vue » comme qualifiable d'oisiveté et de flânage peut entrainer une mise à l'écart jusqu'à l'exclusion, même une punition et une judiciarisation[^oisif], car considéré⋅e⋅s comme menace et/ou mise en péril d'une certaine forme d'équilibre. « Les obligations et les charges du capitalisme pèsent différemment sur les individus ».

[^oisif]: HABERMAS AU STARBUCKS. CLIENTS, OISIFS ET TRAÎNARDS DANS LE TIERS-LIEU CAPITALISTE, Robin Wagner-Pacifici <https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=habermas_au_starbucks_clients_oisifs_et.pdf>

> « Un horaire intégrant adéquatement le nycthémère est indispensable au fonctionnement de sociétés où la productivité va croissant et  où l'extension des loisirs ne s'achète pas au prix du labeur exténuant et de la misère d'autres hommes. Mais la pression du temps, en sa qualité d'aspect du code social, engendre sous sa forme actuelle des problèmes qui attendent encore leur solution » *Du temps*, <cite>Nobert Elias</cite> (1984).

Ce temps et sa comptabilité pour former un « échange » est aussi présent à l'Hôtel Pasteur, tiers-lieu de Rennes métropole[^pasteur-temps]. Là, ce forme d'échange participe d'une « [formation d’un compromis civico-marchand dans la fabrique de la ville en France](https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=tiers-lieux-et-ville-parti_033_0181.pdf) ». D'un autre angle d'approche, le développeur web Thomas Parisot s'attache sur un « [Taux Journalier Militant](https://www.24joursdeweb.fr/2022/tjm-tarif-journalier-militant/) » (TJM), en relation avec le taux journalier moyen utilisé comme *base* de calcul du prix d'une prestation professionnelle. Il y recroise avec des notions de mécénat de compétences, de temps mis à dispositions (« comptes du temps passé ») et des prix fixés par discussions préalables.

[^pasteur-temps]: Voir fragment documentaire photographique <https://pixelfed.social/i/web/post/519175479838871013>

De "The labour of being studied in a free love economy. The politics of workers' inquiry", je voudrais ici partager la traduction des 22 questions qui étaient proposées pour dessiner une tactique d'atténutation face aux problèmes détaillés par .L. Cowan & Jasmine Rault. Elles me paraissent importantes et aussi être une base sur laquelle tirer des enseignements et porter des fermetures (ou liquider des sous-axes) et des améliorations. Ces questions me semblent aussi efficaces à (se) poser avant de répondre, ou non, à un « appel à projet » d'autant plus lorsqu'il est en « recherche participative ».


1. Que faites-vous comme travail ?
2. Êtes-vous payé⋅e pour tout ce travail ?
3. Avez-vous fait des études ou suivi une formation spécialisée pour pouvoir faire votre travail ?
    + Si oui, quel était ce diplôme, certificat ou formation et comment l'avez-vous payé ?
4. Disposez-vous d'un revenu stable et assuré ?
    + Si oui, combien d'argent gagnez-vous chaque mois ?
    + Si non, quelle est la fourchette de votre revenu mensuel au cours des deux dernières années ?
5. Acceptez-vous des emplois supplémentaires pour compléter le salaire que vous recevez de votre travail principal ?
6. Combien d'emplois/contrats avez-vous eu au cours des deux dernières années ?
7. Avez-vous reçu une bourse au cours des cinq dernières années ? Bourse artistique ou universitaire ? Quel en est le montant ?
    + S'il s'agissait d'une subvention universitaire, combien d'argent avez-vous alloué pour le distribuer aux artistes/organisateurs impliqués dans votre projet ? Comment le reste de l'argent a-t-il été dépensé (c'est-à-dire allocations, voyages, fournitures) ?
    + S'il s'agit d'une subvention, combien d'argent avez-vous alloué à la documentation et à l'archivage ? Comment le reste de l'argent a-t-il été dépensé (c.-à-d. allocations, voyages, fournitures) ?
8. Quel est le montant approximatif de vos dépenses mensuelles ?
9. Quel montant approximatif consacrez-vous chaque mois au remboursement de vos dettes ?
10. À l'exception des prêts hypothécaires ou des prêts automobiles, comment avez-vous accumulé vos dettes et comment sont-elles réparties (dette de production artistique, prêts étudiants, frais de subsistance, achats impulsifs) ?
11. Êtes-vous locataire ou propriétaire de votre logement ? Dans l'un ou l'autre cas, veuillez décrire votre relation économique avec votre logement (paiements de l'hypothèque ou du loyer, services publics, taxes, etc.)
12. Avez-vous des colocataires ? Si oui, pourquoi ? Si non, pourquoi pas ?
13. Possédez-vous une voiture ? Si oui, décrivez votre relation économique avec votre voiture (paiements, frais d'essence, entretien, etc.).
14. Avez-vous déjà reçu un héritage familial important ? Si oui, pour quel montant et qu'en avez-vous fait ?
15. À quand remonte votre dernier voyage de vacances que vous avez payé ?
16. Faites-vous un "travail" pour lequel vous n'êtes pas rémunéré⋅e ? Pourquoi ?
17. Quel impact votre couleur de peau, votre genre, votre sexualité, vos handicaps, votre classe sociale, votre taille, votre citoyenneté ou votre niveau d'éducation ont-ils eu sur votre situation financière ?
18. Avez-vous participé à une autre forme d'archivage en ligne, ou à un projet de mise en réseau d'artistes/activistes ? Si oui, qu'en avez-vous retiré ou appris ?
19. Pourquoi participez-vous à ce projet ?
20. Qu'espérez-vous retirer de ce projet, le cas échéant ?
21. Pensez-vous que les autres personnes participant à ce projet en bénéficieront plus ou moins que vous ? Pourquoi ?
22. Veuillez décrire les conditions de travail liées à votre participation à ce projet. Pensez-vous qu'elles sont équitables ?
    + Souhaiteriez-vous changer ces conditions ? Si oui, comment ?

## Notes et références
---
title: "Internet féministe & des féminismes pour des internets"
layout: post
date: 2024-10-21 07:40
image: /assets/images/adelphity-hack.jpeg
headerImage: true
tag:
- hsociety
category: blog
author: XavierCoadic
description: "L'un des principes fondamentaux du féminisme est de se demander pourquoi les choses sont telles qu'elles sont et, par la même occasion, d'examiner comment les rendre plus justes"
---

Passant quelques temps pour amorcer un second travail sur [BEST PRACTICES − Diversity and Inclusion in Open Source Projects](https://nlnet.nl/NGI0/bestpractices/DiversityAndInclusionGuide-v4.pdf) (*Immense merci à la belle équipe d'Association For Progressive Communication*) j'en profite pour repasser par [Feminist Principles of the Internet - Version 2.0](https://www.apc.org/index.php/en/pubs/feminist-principles-internet-version-20) et [Feminist by design and designed by diverse feminists](https://www.apc.org/index.php/en/pubs/feminist-design-and-designed-diverse-feminists).

APC, Association For Progessive Communication, a publié de très [nombreuses ressources](https://www.apc.org/index.php/en/apc-wide-activities/feminist-internet) en anglais, français, espagnol sur les enjeux et question des technologies de la communication, d'internet, et des féminismes.

![](/assets/images/feminist-internet-apc.jpg "Capture d'écran du site web apc.org dans la partie dédiée à l'Internet féministe. Il y apparaît sur un fond jaune 8 images d'illustration pour les 8 premiers articles proposés à la lecture")

Je tiens à vous faire part de **Data Feminism** de Catherine D’Ignazio et Lauren Klein, paru en 2020 et en libre accès [ici](https://data-feminism.mitpress.mit.edu/). Hubert Guillaud est écrit une excellente recension dans chez [danslesalgorithmes.net](danslesalgorithmes.net). Dans ce travial d'Hubert Guillaud, Il y a fait plusieurs fois référence au travaux de Maria Salguero [sur les féminicides au Mexique depuis 2016](https://mapafeminicidios.blogspot.com/p/inicio.html).

![](/assets/images/data-feminism-cover.jpg "Capture d'écran : sur fond blanc la couverture du livre Data Feminism, écrit en lettres blanches sur bandeau noir qui survole un tapissage de plusieurs dizaines d'affiches de revendications féministes").

Depuis cette recension et dans un *reproche* adressé sur un *manque* de données dites fines et *staticisées* il peut y avoir du brutal, et par la convocation « stricte » d'une forme réductrice du « calcul par la mathématique » il peut y avoir obliteration totale (*la totalité peut être vécue comme violence BTW*) des facteurs et besoins sociaux tout autant que tous les points d'achoppement de transformation sociales prises avec différents angles d'actions. 

Je fais aussi grand attention à la convocation d'un utilitarisme de la data pour un « sauvetage du  monde » ou « amélioration d'un bout du monde » qui est une articulation fréquente et usitée par des groupes politiques dont le projet de société est terriblement liberticides. Tout autant que d'autres doctrines d'action politique rejetant en totalité les apports des « données », et des sciences, comme par exemple l'anthroposophie, seront promotrices et productrices.

En paraphrasant [Esther Payne](https://www.apc.org/node/40340) « les codes que nous créons et les outils que nous utilisons peuvent aider ou nuire à l'humanité. Nous inscrivons nos valeurs politiques dans ceux-ci ».

Enfin, ne cessons pas de nous poser la question d'[une critique de la technologie qui serait excessivement d'un point vue occidental](https://mitpress.mit.edu/9780262049306/from-pessimism-to-promise/) ! L'Occident invisibilise et écrase parfois la majorité du monde. Le livre de Payal Arora, *From pessimism to promise, Lessons from the global south on designing inclusive tech (MIT Press, 2024)* est finement décrit [ici](https://maisouvaleweb.fr/sortir-du-techno-pessimisme/) par Irénnée Régnauld.

Raccrochant avec les travaux de Maria Salguero, je rappelle l'existence de « Ceci n’est pas un Atlas » et du sublime “At the Gates”, histoire politique et sociale de l’intime, lutte des Femmes, défiant les lois, pour leurs émancipation et Liberté, dont je vous avais déjà fait part [ici](https://xavcc.frama.io/contre-cartographie-renversements-luttes/).

Le booklet sur **Data Feminism** publié [ici](https://danslesalgorithmes.net/wp-content/uploads/2024/09/FDD.pdf) est excellent − *une synthèse du livre Data Feminism de Catherine D’Ignazio et Lauren Klein, réalisée par Hubert Guillaud. Livret édité par l’association Vecteur et le média Danslesalgorithmes.net. Mise en page et conception graphique, Mathurine Guillaud*

> **L’objectivité n’a rien d’immanent**.
>
> L’éthique des données vise à produire de la responsabilité et de la transparence pour remédier à ce manque d’objectivité. Mais pour les chercheuses, les termes du débat posent problème. Il nous faut passer de concepts qui sécurisent (et entérinent le pouvoir) à des concepts qui le défient, expliquent-elles en nous invitant à modifier les concepts que nous utilisons dans les discussions sur l’utilisation des données et des algorithmes. En passant de l’éthique à la justice, du biais à l’oppression, de la loyauté à l’équité, de la responsabilité à la co-libération, de la transparence à la réflexivité, de la compréhension des algorithmes à la compréhension de l’histoire, de la culture et du contexte… l’enjeu est de sortir de la recherche de solutions technologiques aux problèmes que pose la technologie.
>
> Pour D’Ignazio et Klein, le tableau de changement de concepts qu’elles proposent ne signifie pas par exemple que l’éthique n’a pas sa place
dans la science des données ou que la question des biais ou de la transparence ne devrait pas être abordée, mais que ces concepts ne suffisent pas pour rendre compte des causes profondes de l’oppression et limitent finalement l’éventail de réponses pour démanteler l’oppression ! C’est un moyen de remettre de la politique là où certains tentent de l’ôter pour mieux conserver le pouvoir.
>
> <cite>Hubert Guillaud</cite>

Le **féminisme des données** est un concept d'action selon D'Ignazio et Klein.

> L'un des principes fondamentaux du féminisme est de se demander pourquoi les choses sont telles qu'elles sont et, par la même occasion, d'examiner comment les rendre plus justes.

**Data Feminism** propose des stratégies pour les féministes qui souhaitent concentrer leurs efforts sur le domaine en plein essor de la science des données, et pour les scientifiques des données qui veulent savoir comment le féminisme peut les aider à œuvrer pour la justice. Mais le féminisme des données n'est pas seulement une question de genre. Il s'agit du pouvoir, de qui en dispose et de qui n'en dispose pas, et de la manière dont ces différences de pouvoir peuvent être remises en question et modifiées.

**Cela peut être résumé en 7 principes d'action :**

+ **Examiner le pouvoir** : le féminisme des données commence par l'analyse du fonctionnement du pouvoir.

+ **Remettre en question le pouvoir** : Le féminisme des données s'engage à remettre en question les structures de pouvoir inégales et à œuvrer pour la justice.

+ **Valoriser l'émotion et l'incarnation.** Le féminisme des données nous apprend à valoriser de multiples formes de connaissances, y compris celles qui proviennent des personnes en tant que corps vivants et sensibles dans les endroits et envers du monde.

+ **Repenser le binarisme et les hiérarchies**. Le féminisme des données nous oblige à remettre en question le binaire du genre, ainsi que d'autres systèmes de comptage et de classification qui perpétuent l'oppression.

+ **Embrasser le pluralisme**. Le féminisme des données insiste sur le fait que la connaissance la plus complète provient de la synthèse de multiples perspectives, la priorité étant donnée aux perspectives locales, indigènes et expérientielles locales, indigènes et expérientielles.

+ **Tenir compte du contexte**. Le féminisme des données affirme que les données ne sont pas neutres ou objectives. Elles sont le produit de relations sociales inégales, et ce contexte est essentiel pour une analyse précise et éthique.

+ **Rendre le travail visible**. Le travail de la science des données, comme tout travail, est le travail de nombreuses mains. Le féminisme des données rend ce travail visible afin qu'il soit reconnu et valorisé.

Cette approche peut être mise en dialogue avec « [Le féminisme dans la conception des langages de programmation](https://www.felienne.com/archives/8470) » par Felienne Hermans et Ari Schlesinger

> Hermans : « Au fil du temps, j'ai appris qu'un des principes fondamentaux du féminisme est de se demander pourquoi les choses sont telles qu'elles sont, et par là même d'examiner comment nous pouvons les rendre plus justes. »
>
> https://www.felienne.com/archives/8470

Pour donner le change à [Loud Men Talking Loudly: Cultures d'exclusion dans la « Tech » et dans la gouvernance de l'Internet](/Loud-Men-Talking-Loudly-gouvernance-internet/) (Avril 2023), j'ai envie ici de vous partager des morceaux éparses.

Aussi, pour prolonger dans des intersections vous trouverez quelques morceaux que j'ai pu partager par le passé :
+ [Cyber Feminism Index](https://cyberfeminismindex.com/) de 1985 à 2023
+ [Recettes, conjurations et remèdes pour des futurotopies transféministes](https://ps.zoethical.org/t/hacker-la-terre-2023-calafou-1-06-4-06/6839)
+ [Gender Security](https://gendersec.tacticaltech.org/wiki/index.php/Main_Page)
+ Les travaux du collectif [Hackqueen](https://pad.sans-nuage.fr/p/Hacqueen) lié au hackerspace [hackstub](https://hackstub.eu) à Strasbourg.
+ Claire L. Evans : « [Quand l’informatique a pris de la valeur, les femmes ont dû quitter le terrain](https://www.lemonde.fr/pixels/article/2019/03/23/claire-l-evans-quand-l-informatique-a-pris-de-la-valeur-les-femmes-ont-du-quitter-le-terrain_5440125_4408996.html) ». Propos recueillis par Pauline Croquet (2019)
+ [Infra Red](https://in.fra.red/)

> InfraRed members include over 20 independent, feminist, cooperative, anti-fascist and alternative service providers from 19 different regions that have come together in solidarity to support one another in our shared work. Our network exists to facilitate communication, transparency, and collaboration on shared projects among the members, while also affirming the autonomy of each organization.
---
title: "Une procédure générale de bioessai avec graines par Nilda. R. Burgos (en contexte particulier de résistance aux herbicides)"
layout: post
date: 2023-04-21 06:40
image: /assets/images/jaune-2023.jpg
headerImage: true
tag:
- hsociety
- bioassay
category: blog
author: XavierCoadic
description: Whole-Plant and Seed Bioassays for Resistance Confirmation
---

Cette publication “Whole-Plant and Seed Bioassays for Resistance Confirmation” (Creative Commons SA NC ND), 2015, par <cite>Nilda R. Burgos</cite>, dans *Weed Science 2015 Special Issue:152–165*, est évoquée dans « [Seed Bioassay: Some stuffs and Tips (Tears, water droplet and Tiny Hacks)](/seed-bioassay-some-stuffs-and-tips/).

J'ai écrit à Nilda R. Burgos le 20 avril 2023 dans l'espoir d'engager un dialogue.

Voici une traduction en langue française d'une partie de cette publication portant sur les procédures de boiessai à base de graine dans un context d'usage de produits herbicides.

**TOC**

+ [Procédure générale](#procédure-générale)
  + [Collecte d'échantillons](#collecte-déchantillons)
  + [Bioessai classique pour la confirmation de la résistance](#bioessai-classique-pour-la-confirmation-de-la-résistance)
  + [Bioessai sur des herbicides appliqués au sol](#bioessai-sur-des-herbicides-appliqués-au-sol)
  + [Bioessai avec des herbicides appiqués par voie foliaire](#bioessai-avec-des-herbicides-appliqués-par-voie-foliaire)
  + [Bioessais rapides pour tester la résistance](#bioessais-rapides-pour-tester-la-résistance)
    + [Essai rapide sur plante entière pour les herbicides appliqués en POST](#essai-rapide-sur-plante-entière-pour-les-herbicides-appliqués-en-post)
    + [Essai rapide sur plante entière pour les inhibiteurs de croissance racinaire](#essai-rapide-sur-plante-entière-pour-les-inhibiteurs-de-croissance-racinaire)
    + [Essais de germination des graines en milieu gélosé](#essais-de-germination-des-graines-en-milieu-gélosé)
    + [Essai en Petri avec des graines prégermées](#essai-en-petri-avec-des-graines-prégermées)
    + [Essai sur plaque ELISA utilisant des graines prégermées](#essai-sur-plaque-elisa-utilisant-des-graines-prégermées)
    + [Essai sur les graines prégermées en milieu perlite](#essai-sur-les-graines-prégermées-en-milieu-perlite)
    + [Essai de semis sur gélose](#essai-de-semis-sur-gélose)
+ [Remarques finales](#remarques-finales)
+ [Références](#références)

# Whole-Plant and Seed Bioassays for Resistance Confirmation

*Published online by Cambridge University Press:  20 January 2017*

Une grande partie de la recherche agricole actuelle porte sur la résistance des herbes indésirables aux herbicides. L'évolution de la résistance est peut-être le principal moteur de la recherche de nouvelles cibles d'herbicides, de nouvelles technologies d'intervention sur les herbes et de la promotion des meilleures pratiques de gestion pour une production agricole durable (<cite>Burgos</cite> et al., 2006 ; <cite>Norsworthy</cite> et al. 2012 ; <cite>Vencill</cite> et al. 2012). À ce jour, 222 espèces d'herbes ont collectivement développé une résistance à 150 herbicides correspondant à 21 sites d'action (<cite>Heap</cite> 2014). Depuis des décennies, les scientifiques ont développé de nombreux protocoles pour la confirmation de la résistance en utilisant des graines, différentes parties de plantes ou des plantes entières. Ces protocoles ont été passés en revue par <cite>Beckie</cite> et al. (2000) et <cite>Burgos</cite> et al. (2013). Nous nous inspirons de ces sources et d'autres pour présenter des lignes directrices générales pour la confirmation de la résistance que les étudiant⋅e⋅s et les nouvelles chercheuses et nouveaux chercheurs peuvent utiliser pour planifier leurs expériences. Les questions les plus immédiates auxquelles les parties prenantes cherchent à répondre avec les essais biologiques de résistance sont les suivantes :

1. La population est-elle résistante ?

2. Quel est le niveau de résistance ?

3. Quels autres herbicides peuvent être utilisés ?


## Procédure générale

### Collecte d'échantillons

Cette activité est fondée sur l'observation d'un échec de la lutte contre des herbes dites « indésirables » qui ne peut être pas être attribuée à des contraintes environnementales, à un ou à des erreurs d'application de l'herbicide.

L'échantillonnage pour la confirmation de la résistance concerne généralement la collecte de semences. Les questions essentielles qu'il faut se poser avant de procéder à l'échantillonnage sont les suivantes

1. Les plantes seront-elles échantillonnées individuellement ou en vrac ?
2. Combien de plantes individuelles seront échantillonnées ; combien d'échantillons en vrac, ou combien de plantes seront prélevées pour constituer l'échantillon global ?
3. Quelles sont les données à collecter au cours de l'échantillonnage ?

**Étape 1. Déterminer le nombre d'échantillons et la quantité de semences à prélever**

Il est essentiel de disposer d'une procédure appropriée pour la collecte de semences de plantes supposées résistantes (R) en vue d'essais d'herbicides.

Si l'enquêteurice veut connaître la variation de la résistance d'une plante à l'autre au sein d'un champ, iel doit utiliser des semences en échantillons individuels(Hausman et al. 2011 ; Patzoldt et al. 2005). Dans le cas contraire, un échantillon global suffira. Pour augmenter le pouvoir de détection des allèles de résistance peu fréquentes, un grand nombre d'échantillons est nécessaire (par exemple, 20 à 40 plantes) (<cite>Burgos</cite> et al. 2013). Pour les plantes endogames, le brassage génomique est minime ; ainsi, différents morphotypes avec différents traits de résistance peuvent être présents dans le même champ. Une espèce principalement autofécondée comme le riz adventice (*Oryza sativa* L.), pourrait avoir jusqu'à 15 morphotypes différents dans un champ (observation personnelle).

Dans ce cas, il convient de collecter un échantillon en vrac suffisamment important de chaque morphotype (au moins 5 000 graines) pour réaliser les bioessais. Pour les espèces allogames strictes comme Lolium spp. (<cite>Terrell</cite> 1968), seules quelques plantes sont nécessaires pour représenter la population en raison du fort mélange génomique intrapopulationnel. Ainsi, 5 à 10 plantes sont considérées comme le minimum pour les espèces à pollinisation croisée (<cite>Burgos</cite> et al. 2013). Il existe des exceptions. L'amarante de Palmer (*Amaranthus palmeri* S. Wats.), par exemple, est une espèce interfécondation qui présente une grande diversité morphologique (<cite>Bond</cite> et <cite>Oliver</cite> 2006). Dans ce cas, il faut s'efforcer de représenter la diversité des morphotypes sur le terrain. Cette espèce dioïque présente également une grande diversité intrapopulationnelle, la distance génétique (GD) la plus faible et la plus élevée entre les plantes d'une même population étant donné respectivement à 0,44 et 0,64 (<cite>Chandi</cite> et al. 2013). Ces chiffres sont remarquablement élevés si l'on considère que les données ont été obtenues à partir de populations résistantes au glyphosate, qui ont été homogénéisées par la sélection au glyphosate. Les individus sont plus similaires lorsque GD s'approche de 0, ou plus diversifiés lorsque GD s'approche de 1 (<cite>Nei</cite> 1973). La fréquence des descendants résistants de chaque plante mère de Palmer Amaranthus dans un champ varie considérablement (Burgos et al., données non publiées).

Dans les cas où il y a très peu de plantes dans le champ (par exemple, < 10), collecter les semences de toutes les plantes. Ne pas prélever d'échantillons dans des zones manifestement oubliées par les applicateurs d'herbicides, à moins que des plantes sensibles ne soient également recherchées.
Les survivantes étant généralement présents en parcelles, prélever un échantillon global pour représenter une parcelle. Inspecter l'ensemble du champ et prélever jusqu'à dix échantillons pour représenter différentes sections du champ. Cela devrait de tirer une conclusion solide sur le statut de résistance d'un tel champ. La fréquence de résistance attendue dans un champ peut être déduite depuis des échantillons en vrac.

**Étape 2. Déterminer le moment de la collecte des échantillons**

Il faut programmer la collecte de l'échantillon lorsque les graines commencent à se détacher. Si possible, ne récoltez que les graines qui se détachent facilement de l'inflorescence.
En général, ces graines sont matures et ont une capacité de germination élevée. Les graines de nombreuses espèces de dicotylédones doivent être récoltées sous forme de fruits entiers ou d'inflorescences.

**Étape 3. Déterminer les données externes à collecter**

Avant de procéder à l'échantillonnage sur le terrain, évaluez s'il est nécessaire de collecter des informations supplémentaires concernant l'échantillon. Ces informations se répartissent en trois catégories générales : (**1**) les caractéristiques des espèces, (**2**) la distribution et l'abondance des espèces, et (**3**) l'historique du terrain. Le choix des données à collecter dépend des objectifs de la recherche. Les caractéristiques des plantes (hauteur, taille des feuilles, mode de ramification, mode de croissance, mode de pubescence, morphologie des inflorescences, morphologie des fruits, mode d'égrainage des graines) fournissent des indices sur la différenciation des écotypes ou la sous-espèce, dont certains pourraient influer sur la réaction aux herbicides. Ceci pourrait à son tour affecter l'interprétation des résultats. Les notes sur la composition, la distribution et l'abondance des espèces (Nkoa et al. 2014) nous informent sur l'efficacité générale des pratiques de désherbage, la gravité du problème de résistance, la possibilité d'espèces résistantes multiples, ou l'effet à long terme des pratiques de gestion telles que le déplacement des espèces. L'historique des champs est nécessaire pour une première exploration d'un problème suspecté ou pour des études sur la dynamique ou l'évolution des populations. Il s'agit de l'historique du travail du sol, des cultures et des herbicides au cours des cinq dernières années (y compris l'année en cours) dans la mesure du possible (voir l'annexe A à titre d'exemple). La quantité de données historiques collectées est un compromis entre ce qu'il est idéal d'avoir et ce qui peut être obtenu auprès des agriculteurices, des spécialistes, des agents d'informations ou des archives publiques. Si les échantillons prélevés sont résistants, mais que l'historique du champ n'indique pas de pression de sélection, il est possible que les individus résistants aient été introduits à partir d'autres endroits par des facteurs biotiques (activités humaines, animaux) ou abiotiques (mouvement du vent ou de l'eau).

![](/assets/images/attachement-A.png)
<figcaption class="caption">Attachment A. Sample Field History Form.</figcaption>
<br />

**Étape 4. Enregistrer l'emplacement du champ**

Attribuer un identifiant de champ ID pour associer l'échantillon à l'emplacement et enregistrer les les coordonnées GPS pour permettre un retour sur le site si nécessaire.

**Étape 5. Planifier la logistique du transport, du traitement et du stockage des échantillons**

Les graines et les tissus végétaux qui les accompagnent (panicules, certaines feuilles, tiges, fruits) peuvent contenir des quantités considérables d'humidité. Il convient donc de placer le matériel végétal fraîchement collecté dans des sacs en papier, des sacs en tissu ou d'autres récipients permettant une bonne ventilation. Certaines espèces comme l'Amaranthus ont des inflorescences avec beaucoup de tissus verts au moment de l'échantillonnage. Bien que les têtes de graines soient placées dans des sacs en papier, les échantillons respirent, accumulent de la chaleur et libèrent de l'humidité après plusieurs heures passées dans un véhicule. Dans la mesure du possible, réduisez au minimum la durée pendant laquelle les échantillons sont empilés dans le véhicule. Déchargez les échantillons dès leur arrivée au centre de recherche et disposez-les dans un endroit protégé du soleil et où l'air circule librement. Des conditions extrêmes entraîneront la détérioration des graines ou déclencheront une dormance profonde (<cite>Bewley</cite> et <cite>Black</cite> 1994 ; <cite>Bewley</cite> et al. 2012).

Des insectes peuvent être présents dans les tissus végétaux collectés. Le séchage des échantillons à l'intérieur d'une serre avec la ventilation éteinte tuera les insectes. Alternativement, les tissus collectés peuvent également être placés dans un séchoir à 60 C° pendant 3 jours pour dessécher des insectes à tous les stades de vie (des œufs aux adultes) qui peuvent se trouver dans les échantillons de plantes.
Deux semaines suffisent généralement pour que les échantillons à l'air *libre*. L'extraction des graines consiste à séparer les graines des des autres matières végétales, ce qui peut nécessiter l'utilisation d'une batteuse mécanique, suivie d'un nettoyage affiné à l'aide d'une soufflerie ou d'une séparation gravitationnelle des graines. Au cours de toutes ces étapes prendre des précautions pour éviter la contamination croisée des échantillons.

Les graines expriment généralement une dormance innée à la maturité physiologique ou lorsqu'elles sont dispersées de la plante mère ; par conséquent, elles ne germeront pas à moins d'être soumises à une période suffisante de post-maturation
(<cite>Bewley</cite> et al. 2012). Par exemple, l'échinochloa pied-de-coq [*Echinochloa crus-galli* (L.) P. Beauv.] présente une capacité de germination de 5 à 50 % à maturité ; le reste des graines reste en dormance pendant des périodes plus ou moins longues (<cite>Honek</cite> et <cite>Martinkova</cite> 1996 ; <cite>Kovach</cite> et al. 2010 ; <cite>Martinkova</cite> et al. 2006).

Par conséquent, il faut laisser suffisamment de temps aux graines pour mûrir avant de réaliser le bioessai en stockant les graines à température ambiante pendant au moins un mois. La durée optimale de post-maturation diffère selon les espèces et même selon les écotypes au sein des espèces (<cite>Kovach</cite> et al. 2010 ; <cite>Tseng</cite> et al. 2013). Par exemple, la grande majorité des riz adventices doivent être post-maturés pendant 90 jours pour obtenir une germination optimale ; cependant, certains atteignent 90 % de germination dans les 30 jours suivant la récolte, et d'autres ont besoin d'une période de post-maturation plus longue, au-delà de 90 jours (<cite>Tseng</cite> et al. 2013). Pour un stockage à long terme, les graines peuvent être conservées dans une chambre froide avec une faible humidité relative ou dans un congélateur. Parmi les références générales sur la dormance des semences, citons <cite>Bewley</cite> et al. (1990). Il est nécessaire de rechercher dans la littérature informations sur la dormance des semences de l'espèce concernée avant d'entreprendre des bioessais sur les semences ou les jeunes plants.


**Étape 6. Tester la germination des échantillons de semences et rompre la dormance si nécessaire**

Une bonne germination est importante dans les essais biologiques de résistance, en particulier pour les herbicides POST dont l'efficacité dépend de la taille de la plante. Les conditions de température et de lumière pour une germination optimale sont les facteurs les plus importants à déterminer. Ils diffèrent selon les espèces d'un même genre et les écotypes d'une même espèce (<cite>Kovach</cite> et al. 2010 ; <cite>Tseng</cite> et al. 2013). Dans le genre *Echinochloa*, par exemple, la température optimale de germination varie entre 25 et 30°C. Certaines espèces ont besoin de lumière pour germer, d'autres ont besoin d'une incubation à l'obscurité (<cite>Kovach</cite> et al. 2010). Si le besoin en lumière n'est pas satisfait, la graine ne germera pas. Il convient donc de réaliser un test de germination (dans les bonnes conditions) pour déterminer la quantité de semences à planter pour obtenir le nombre souhaité de plantes de taille similaire par traitement. Tout d'abord, si les conditions de germination de l'espèce concernée sont déjà déterminées, consultez la littérature. Outre les exigences en matière de température et de lumière, il est utile de savoir, par exemple, que l'arroche chinoise (*Leptochloa chinensis* L.) germe mieux (80 %) lorsqu'elle est placée à la surface du sol (<cite>Chauhan</cite> et <cite>Johnson</cite> 2008a). Sans savoir cela, læ chercheureuse aurait recouvert la graine d'un peu de terre et n'aurait pas obtenu une bonne germination.

Si nécessaire, rompre la dormance des semences pour améliorer l'uniformité de la germination et la capacité de germination. La dormance des semences peut être rompue par post-maturation, stratification à froid, scarification mécanique ou chimique, traitements chimiques, traitement thermique, exposition à des températures alternativement chaudes et froides, ou une combinaison de ces méthodes (Buhler et Hoffman 1999 ; <cite>Chauhan</cite> et <cite>Johnson</cite> 2008b, 2009 ; <cite>Ðikic</cite> et al. 2011 ; <cite>Karlsson</cite> et al. 2006 ; <cite>Martinkova</cite> et al. 2006 ; <cite>Tseng</cite> et al. 2013). Les tests de germination sont généralement effectués dans des boîtes de Pétri tapissées de papier filtre, avec 50 à 100 graines par boîte, répétées trois fois ou plus. répliquées trois fois ou plus, humidifiées avec jusqu'à 6 ml d'eau et incubées. jusqu'à 6 ml d'eau, et incubées à la température et aux conditions température et à la lumière appropriées pendant 1 à 2 semaines (par exemple, voir les références citées dans cette section), voir les références citées dans cette section). Les graines récalcitrantes récalcitrantes peuvent devoir être prégermées puis transplantées dans le milieu d'essai (<cite>Burke</cite> et al. 2006 ; <cite>Huan</cite> et al. 2011 ; <cite>Xu</cite> et al. 2010). Allouez du temps du temps dans votre calendrier d'activités pour ces expériences préliminaires potentielles.

### Bioessai classique pour la confirmation de la résistance

Ce bioessai concerne l'approche traditionnelle consistant à planter des graines dans des pots et à appliquer des herbicides dans le sol soit (**1**) incorporés avant la plantation, soit (**2**) immédiatement après la plantation et des herbicides POST à la taille ou au stade de croissance recommandé pour les semis. Si l'on teste pour la première fois la résistance à un herbicide particulier, il faut effectuer un essai dose-réponse pour déterminer le niveau de tolérance et choisir la dose discriminante pour tester un grand nombre d'échantillons (<cite>Burgos</cite> et al. 2013).

**_Sélection d'une population sensible standard_**

Il est impératif qu'un échantillon connu pour être sensible à l'herbicide en question soit inclus dans chaque série
d'un bioessai de résistance. Les niveaux de résistance ne peuvent être comparé que si le même étalon sensible (S) est utilisé dans le bioessai. La sélection d'un étalon S n'est pas toujours simple. L'idéal est que l'étalon S soit prélevé dans la même zone que les échantillons R présumés. Cette solution est préférable à l'utilisation d'un lot de semences d'herbes dites indésirables acheté à des marchands d'une autre région ou d'un autre pays. De nombreux  groupes de recherche utilisent une norme S qui n'a pas été exposée l'herbicide (<cite>Burgos</cite> et al. 2013). Cependant, ces populations peuvent être extrêmement sensibles à l'herbicide et donneront lieu à des valeurs d'indice de artificiellement élevées. Dans ce cas, il est possible qu'un échantillon enregistre une valeur élevée de l'indice de résistance, mais qu'il puisse encore être contrôlé par la dose totale prévue. Pour évaluer la tolérance de fond d'une espèce à un herbicide, il faut tester différentes populations S et utiliser celle qui représente le niveau de tolérance de la majorité.

Lors de l'étude d'un cas de résistance précédemment non documenté chez une espèce, obtenir la population S présumée d'une zone proche du champ problématique, car les populations d'une même localité sont censées être plus proches génétiquement que celles de régions différentes ; les différences génétiques de fond pourraient affecter la réponse de la plante aux herbicides.

### Bioessai sur des herbicides appliqués au sol

**Etape 1. Préparer le sol comme medium** 

Pour tester les herbicides PRE, il faut utiliser un sol de champ non stérilisé. Dans la mesure du possible, ce sol doit représenter le type de sol moyen ou le plus courant dans la région. Évitez les sols au pH extrême ou à la teneur en matière organique très élevées. Ramassez la terre lorsqu'elle est friable, non motteuse et non dure. Étalez le sol uniformément sur une surface plane pour qu'il sèche à l'air. Il peut être nécessaire de passer la terre dans un broyeur pour pulvériser les mottes avant de la mettre dans les pots, car les mottes empêchent une distribution uniforme de l'herbicide à la surface du sol.

**Etape 2. Déterminer le nombre de traitements, le nombre d'échantillons, les répétitions et le plan d'expérience. expérimental**

Tenez compte de la disponibilité de la main-d'œuvre et de l'espace. Pour un essai préliminaire de la relation dose-effet sur un échantillon suspecté d'être résistant, un titrage en sept points consistant en trois doses inférieures et trois doses supérieures au taux plein constituerait une bonne fourchette, plus un contrôle non traité. Ainsi, pour un échantillon R présumé, la gamme de doses pourrait être la suivante : 0, 0,125 ; 0,25 ; 0,5 ; 1 ; 2 ; 4 et 8 fois la dose recommandée sur le terrain. Pour l'étalon S, le titrage de la gamme de doses serait inférieur au taux plein, par exemple 0 ; 0,0625 ; 0,125 ; 0,25 ; 0,5 ; 0,75 ; 1 et 2 fois la dose recommandée sur le terrain. 

Une autre approche consiste à commencer par une progression logarithmique des doses pour étudier la réponse des plantes sur une plus grande échelle (<cite>Kaundun</cite> et al. 2011), puis à effectuer un essai de suivi pour affiner la gamme de doses. Les traitements doivent être répétés au moins trois fois. Pour un essai dose-réponse, les unités expérimentales (pots) pourraient être disposées selon un plan de parcelles divisées avec la population (R et S putatifs) comme parcelle principale et la dose d'herbicide comme sous-parcelle. Cette disposition facilite l'observation de la réponse des plantes à l'augmentation de la dose d'herbicide lors de l'évaluation visuelle. Elle permet également de vérifier immédiatement les valeurs aberrantes ou des erreurs d'application, avant même la période d'évaluation prévue. Veillez à ce que les réplicats(blocs) soient disposées de manière à prendre en compte des gradients environnementaux (par exemple, température, lumière) de l'endroit où les plantes sont conservées. On peut également utiliser une disposition factorielle des traitements.

Si la dose discriminante a déjà été déterminée, l'expérience consistera à utiliser l'étalon S et autant d'échantillons suspectés que l'installation peut en contenir. Si une population R a déjà été identifiée, l'inclusion d'un étalon R (comme contrôle positif) est bénéfique. Les traitements herbicides consisteront alors uniquement en un contrôle non traité et en une dose discriminante. Dans ce cas, le plan expérimental peut être factoriel, mais il faut veiller à ce que, lors de l'évaluation visuelle de l'activité de l'herbicide, la plante traitée soit évaluée sur la base de sa contrepartie non traitée. Cela implique de trier les pots en paires traitées et non traitées au moment de l'évaluation. Si deux ou trois doses sont utilisées dans le dépistage de la résistance, plus le témoin non traité, il est possible d'opter pour un plan de parcelles divisées, la dose d'herbicide étant la parcelle principale et la population la sous-parcelle.

Cela permet de mieux détecter les différences entre les populations en réponse à chaque dose d'herbicide. Pour les essais à grande échelle, la dose discriminante peut être la seule qui puisse être prise en compte. Ce que l'on considère comme une population de grande taille diffère d'un centre de recherche à l'autre. En fin de compte, la taille de l'expérience (population × doses) dépend de la question que læ chercheureuse veut traiter et des ressources dont iel dispose. Des informations utiles peuvent être obtenues en ajoutant une dose sublétale si l'espace et la main-d'œuvre le permettent. Cela révélera une faible résistance ou une tolérance différentielle entre les populations et indiquera les propensions relatives à l'évolution de la résistance. D'autre part, il peut être intéressant de savoir si les herbes indésirables peuvent tolérer une dose plus élevée. Ainsi, certains laboratoires incluent des doses 0,5 et 1 fois, ou 1 et 2 fois les doses dans leur programme de test (<cite>Burgos</cite> et al. 2013 ; <cite>Kaloumenos</cite> et al. 2011 ; <cite>Maneechote</cite> et al. 2005 ; <cite>Wise</cite> et al. 2009). Répétez l'expérience pour vérifier les résultats.

**Étape 3. Choisissez les contenants appropriés pour les plantes.**

Choisissez un contenant de taille appropriée au nombre et à la taille des graines qui seront plantées. Répartissez les graines uniformément et ne plantez pas une quantité excessive de graines au-delà de ce qui est nécessaire pour atteindre la population végétale souhaitée. Plusieurs études ont montré une corrélation négative entre l'efficacité de la dose d'herbicide et la densité des herbes (<cite>Baldoni</cite> et al. 2000 ; <cite>Kim</cite> et al. 2002 ; <cite>Lati</cite> et al. 2012 ; <cite>Oveisi</cite> et al. 2010). Cependant, les études sur les taux de semis optimaux qui peuvent être utilisés sans réduire l'efficacité de l'herbicide sont peu nombreuses ou inexistantes pour de nombreuses espèces. Placez la même quantité de terre dans chaque pot. Fermez de façon à ce que le niveau du sol ne se tasse pas lors de l'arrosage.

**Étape 4. Planter**

Décidez à l'avance du nombre de graines à planter par pot. Vingt graines par pot suffisent généralement, avec quatre répétitions. On peut aussi planter 50 graines dans un bac avec deux répétitions. Plantez les graines à une profondeur uniforme dans tous les pots. Pour ce faire, étalez les graines sur la surface raffermie du sol et recouvrez-les
avec le même poids de terre dans tous les pots pour afin d'obtenir la profondeur de semis souhaitée.

**Étape 5. Irrigation.**

Il est préférable de sub-irriguer les pots afin de ne pas déloger les graines plantées. Placez les pots dans un bac et remplissez le bac avec suffisamment d'eau pour détremper le sol. Enlevez l'excès d'eau et laissez l'eau gravitationnelle s'écouler des pots. Le sol doit être à la même capacité que celui du champ (<cite>Veihmeyer</cite> et <cite>Hendrickson</cite> 1931) avant l'application de l'herbicide. Les herbicides actifs dans le sol doivent se déplacer jusqu'à la zone d'implantation des semences pour être efficaces. L'irrigation par aspersion faciliterait ce processus. Si l'irrigation par aspersion est utilisée, choisir une tête d'arrosage qui délivre un léger jet d'eau et utiliser une faible pression d'eau.

**Étape 6. Calibrer le pulvérisateur** 

De nombreux laboratoires sont équipés d'une chambre de pulvérisation intérieure avec une rampe de pulvérisation motorisée. L'utilisation d'un pulvérisateur intérieur permet de pulvériser avec une grande précision. La rampe de pulvérisation peut être réglée à la bonne distance de la cible et ne pas être soumise à l'erreur humaine tout au long de l'application. L'application de l'herbicide peut se faire à tout moment, selon les besoins, sans avoir à se soucier de la vitesse du vent ou de l'imminence de la pluie. Toutefois, l'application d'herbicides dans une chambre de pulvérisation prend plus de temps que la pulvérisation à l'extérieur, car la plate-forme ne peut accueillir qu'un petit nombre de pots à la fois.

Si l'on ne dispose pas d'un pulvérisateur motorisé pour l'intérieur, les herbicides peuvent être appliqués à l'aide d'une rampe manuelle. Cela implique de disposer les pots à pulvériser à l'extérieur et d'appliquer les herbicides de la même manière que pour le traitement des parcelles de terrain. L'application d'herbicides en l'extérieur est plus rapide car il y a peu de déplacement des pots. Les plantes sont également exposées à l'éclairage et à la température naturels jusqu'au moment où elles doivent être retournées dans la serre. Cependant, il faut tenir compte des les conditions météorologiques, le risque de dérive et l'erreur humaine potentielle, telles que la variabilité et la précision de la vitesse de marche et de la hauteur de la rampe.

Pour calibrer le pulvérisateur, il faut connaître le volume de solution herbicide (ml) qui devrait passer par la buse de pulvérisation par unité de temps (min) pour pour appliquer le volume de pulvérisation d'herbicide souhaité (L) par unité de surface (ha). Les paramètres suivants sont nécessaires :

+ **1** Volume cible d'application de la pulvérisation. Ce volume varie de 94 à 187 L ha<sup>-1</<sup>> . Pour les herbicides appliqués sur le sol, utiliser la fourchette haute afin d'obtenir une meilleure répartition de l'herbicide sur le sol.

+ **2** Vitesse de l'applicateur. Les pulvérisateurs de laboratoire peuvent servir à une vitesse de 1,67 km h<sup>-1</sup> . Si l'herbicide est appliqué manuellement, la vitesse de marche peut être comprise entre 3,34 et 5,01 km h<sup>-1</sup>
.
+ **3** Largeur de pulvérisation (cm). Il s'agit de la largeur de la zone couverte par la rampe de pulvérisation en un seul passage. Calculez le débit de pulvérisation escompté à l'aide de la formule :

> [Équation 1]: [Débit du pulvérisateur (ml min<sup>-1</sup>) = Volume de pulvérisation (L ha<sup>-1</sup> × vitesse (km h<sup>-1</sup> × Largeur (cm)] / 60

Pour déterminer si le pulvérisateur motorisé fonctionne à la vitesse souhaitée (c.-à-d. 1,67 km h<sup>-1</sup>), faites fonctionner le pulvérisateur sur une certaine distance (c.-à-d. 1 m) et notez le temps (s) nécessaire pour couvrir cette distance. Ajustez la vitesse si nécessaire. Utilisez la vitesse réelle du pulvérisateur pour calculer le débit attendu des buses. 

Calculer le débit par buse en divisant la réponse obtenue à l'équation [1] par le nombre de buses. Faites fonctionner le pulvérisateur pendant 15 s, récupérez le débit de chaque buse et mesurez le volume. De plus amples informations sur l'étalonnage du pulvérisateur et les questions connexes sont disponibles en ligne dans les catalogues de systèmes de pulvérisation tels que le système de pulvérisation TeeJetH (TeeJet Technologies, <https://web.archive.org/web/20120209155449/https://www.teejet.com/english/home/tech-support/nozzle-technical-information.aspx>). Pour obtenir le débit souhaité, la modification de la vitesse ou de la taille des buses entraîne des changements importants dans le flux de pulvérisation.
Le réglage de la pression ne modifie que faiblement le débit de pulvérisation.

**Étape 7. Application de l'herbicide.**

Appliquer l'herbicide dans les 24 heures suivant la plantation, après que le sol ait été arrosé. Respecter le volume d'application recommandé. En cas d'application de doses multiples, appliquer les mélanges d'herbicides de la dose la plus faible à la dose la plus élevée. Après l'application de l'herbicide, placer les pots pulvérisés avec différentes doses d'herbicide dans des bacs séparés afin d'éviter la contamination croisée des concentrations d'herbicides lors du processus de sub-irrigation. Arrosez les pots si nécessaire.

**Étape 8. Surveiller la période d'émergence.**

Noter le nombre de plantules levées dans les pots non traités au moins une fois avant la date prévue pour l'évaluation de l'efficacité de l'herbicide. En l'absence d'informations préalables, il est utile (mais pas nécessaire) de prendre des notes sur la levée 7 et 14 jours après la plantation.

L'enregistrement de l'émergence à différents moments fournira des informations supplémentaires sur le comportement germination des échantillons. Il peut s'écouler 3 ou 4 semaines avant qu'une évaluation juste de l'efficacité de l'herbicide puisse être faite.

**Étape 9. Évaluation de l'efficacité de l'herbicide**

À la date date de fin de l'expérience, noter le nombre de plantes encore vivantes, le niveau de le niveau de blessure et la biomasse des pousses. La biomasse fraîche soit la biomasse sèche.

### Bioessai avec des herbicides appliqués par voie foliaire

Les étapes 1 à 3 sont sont les mêmes que pour le bioessai des herbicides appliqués sur le sol_**. Il convient de noter que le terreau commercial peut être utilisé pour tester les herbicides POST. pour tester les herbicides POST. En choisissant la taille du l'objectif ultime est d'éviter l'entassement et d'obtenir une couverture uniforme des plantes par la pulvérisation. et d'obtenir une couverture uniforme des plantes par la pulvérisation.

**Étape 4. Plantation**

Ajustez le nombre de graines à planter en fonction de la capacité germinative du lot de semences afin d'obtenir le nombre de plantules de taille uniforme souhaitées par traitement. Les lots de semences peuvent être très différents en matière de capacité germinative. Par conséquent, il convient d'effectuer un test de germination avant de réaliser l'essai biologique.
test de germination avant d'effectuer l'essai biologique.

Au stade d'une feuille (ou plus tôt), éclaircir les plantules jusqu'au nombre désiré. Il est essentiel de le faire le plus tôt possible afin que les plantules puissent être extraites facilement sans endommager les racines de la plante restante. Il est également possible de couper les semis à la surface du sol. Les pousses d'herbe doivent être coupées sous la surface du sol pour enlever le méristème ; sinon, la pousse se régénérera dans les 2 jours.

En général, cinq plantes par pot avec quatre répétitions, ou dix plantes par pot avec trois répétitions sont suffisantes pour un essai biologique dose-réponse. L'essai dose-réponse nous indique si la plante est résistante et quel est le niveau de résistance, mais pas la fréquence de la résistance.

Pour détecter la fréquence de la résistance avec un niveau de confiance élevé (par exemple, une probabilité de 95 %), un plus grand nombre total de plantules (par exemple, 100) doit être testé en utilisant une dose discriminante. Il peut s'agir de deux répétitions de 50 plantules par répétition. Le nombre de plantes utilisées dans les bioessais sur les plantules varie considérablement (<cite>Burgos</cite> et al. 2013). La règle empirique est de tester suffisamment de plantes par population pour avoir confiance dans les résultats des tests et pour formuler des recommandations. En cas de doute, consultez un statisticien.

**Étape 5. Irrigation**

Il est préférable de sub-irriguer les pots jusqu'à ce que les plantes soient suffisamment grandes pour résister à la pression de l'eau des arroseurs aériens. Ce faisant, gardez les plantes qui ont été pulvérisées avec la même dose dans le même bac (pour la sub-irrigation) afin d'éviter la contamination croisée des traitements. Lors de l'utilisation d'herbicides volatils tels que la clomazone, le 2,4-D ou le dicamba, il peut être nécessaire de séparer les différentes populations, même si elles ont été traitées avec la même dose, afin d'éviter une exposition supplémentaire.

**Étape 6. Application de l'herbicide**

Appliquer les herbicides POST au stade de croissance recommandé en utilisant les agents tensioactifs et les additifs recommandés. Attendre au moins une semaine après l'éclaircissage avant d'appliquer l'herbicide afin que les plantules puissent se remettre des perturbations des racines avant d'être exposées au stress de l'herbicide. Suivre les pratiques recommandées pour l'application d'herbicides POST.

**Étape 7. Surveiller la période d'émergence.** Suivre le même principe que pour les herbicides PRE.

**Étape 8. Évaluation de l'efficacité de l'herbicide**

À la date de fin de l'expérience, noter le nombre de plantes émergées encore vivantes, le niveau de blessure des plantes vivantes et la biomasse des pousses. Évaluer les herbicides à action rapide dans les 7 à 14 jours suivant le traitement (DAT / *Day After Traitment*) pour observer l'activité maximale, puis à nouveau dans les 28 à 30 DAT pour observer toute repousse. Les herbicides à action lente peuvent être évalués dans les 3 et 4 semaines suivant le traitement (WAT / *Weeks After Traitment*).

**_Note supplémentaire sur la réplication_**. 

En général, trois à quatre répétitions sont utilisées dans les bioessais sur plantes entières (<cite>Burgos</cite> et al. 2013). Dans certains cas, les tests de criblage de la résistance n'ont pas pu être répliqués en raison d'un manque d'espace. Les bioessais non répétés ne sont fiables que si un grand nombre de plantes (p. ex. 100) sont évaluées par population (<cite>Dickson</cite> et al. 2011 ; <cire>Wise</cite> et al. 2009 ; <cite>Zheng</cite> et al. 2011). Même dans ce cas, on risque de perdre des données si un événement malheureux survient et fait périr des plantes. Une meilleure alternative est de planifier la taille de l'expérience pour permettre deux réplications avec 40 à 50 plantes par réplication (<cite>Dickson</cite> et al. 2011 ; <cite>Owen</cite> et al. 2012). Quelle que soit l'approche retenue, il convient de répéter le test.

**_Évaluation du niveau de résistance. Utilité des estimations du niveau de résistance_**. 

La détermination du niveau de résistance est importante pour les chercheureuses car elle nous informe sur la nature des mécanismes de résistance, ce qui peut à son tour influencer les stratégies de gestion ou déboucher sur de nouvelles technologies de lutte contre les adventices. Un exemple récent est le développement de la technologie BioDirect TM (Monsanto 2014) basée sur la compréhension du mécanisme de résistance au glyphosate chez l'amarante de Palmer (<cite>Gaines</cite> et al. 2013).

**_Gamme de doses_**. 

Une expérience dose-réponse sert non seulement à déterminer la dose discriminante pour les tests de résistance à grande échelle, comme indiqué dans la section précédente, mais aussi à évaluer la dose la plus élevée qu'une population peut tolérer et à obtenir des indices sur le(s) mécanisme(s) de résistance potentiel(s). Pour générer une courbe dose-réponse robuste permettant une estimation fiable du niveau de résistance, il convient de tester une large gamme de doses afin de définir les extrémités asymptotiques de la courbe. Étendez l'extrémité inférieure de la fourchette pour couvrir la région où l'effet est à peine visible ; de même, étendez l'extrémité supérieure de la fourchette jusqu'à un point où aucune autre réponse n'est observée. Une gamme de doses appropriée implique l'ajustement de la gamme généralisée donnée dans la section sur les bioessais et l'ajout d'intervalles de doses plus rapprochés si nécessaire (<cite>Ritz</cite> et al. 2014).

**_Remarque concernant les herbicides contenant des agents tensioactifs intégrés_**

Certains herbicides sont formulés avec des mélanges exclusifs d'additifs qui facilitent l'entrée et le transport dans la plante pour garantir une activité optimale. Le glyphosate en est un exemple. Dans ce cas, il est conseillé d'utiliser la formulation simple de l'herbicide (sans les additifs) et d'ajouter l'agent tensioactif approprié à une concentration constante lors du mélange de la solution de pulvérisation de l'herbicide (<cite>Dickson</cite> et al. 2011 ; <cite>Legleiter</cite> et <cite>Bradley</cite> 2008 ; <cite>Salas</cite> et al. 2012). Cela permet d'éliminer l'effet de confusion lié à l'augmentation de la concentration de l'agent de surface à mesure que la concentration de l'herbicide augmente.

**_Estimation du niveau de résistance_**. 

La courbe dose-réponse basée sur l'évaluation des dommages ou la biomasse peut être utilisée pour estimer la quantité d'herbicide qui provoque un certain niveau d'inhibition de la croissance (par exemple, GR<sub>50</sub>, GR<sub>90</sub> [réduction de la croissance de 50 % ou 90 % par rapport à l'échantillon non traité]). Les données relatives à la mortalité peuvent être utilisées pour estimer la quantité d'herbicide qui tuera une certaine proportion de la population, également appelée dose létale (DL<sub>xx</sub>, par exemple DL<sub>50</sub> , DL<sub>90</sub> ). Ces paramètres sont utiles pour prédire le taux d'expansion ou de réduction de la population résistante. Les rapports R/S des valeurs LD<sub>XX</sub> ou GR<sub>XX</sub> sont utilisés pour comparer l'ampleur de la résistance de différentes populations (<cite>Burgos</cite> et al. 2013). L'ampleur de la résistance nous informe sur les mécanismes de résistance potentiels. Par exemple, trois populations de chénopode (*Eleusine indica* L.) en Malaisie ont montré différents niveaux de résistance au fluazifop, un inhibiteur de l'acétyl coenzyme-A carboxylase (ACCase, EC 6.4.1.2) (<cite>Cha</cite> et al. 2014). Deux populations présentaient des niveaux de résistance résistance plus faibles (R/S = 62 et 88) attribués à une mutation de l'ACCase, Trp<sub>2027</sub>Cys. La troisième population portait une mutation différente de l'ACCase, Asn<sub>2097</sub>Asp, et était environ deux fois plus résistante (R/S = 150) que les deux premières. De même, le niveau de résistance et l'aptitude des plantes R sont affectés par la nature de la mutation du site catalytique dans la cible de l'herbicide, l'acétolactate synthase (ALS, EC 2.2.1.6) (<cite>Yu</cite> et <cite>Powles</cite> 2014). Un niveau de résistance très élevé indique non seulement une forte mutation du site cible, mais aussi un mécanisme de résistance du site non cible très efficace ou une accumulation de mécanismes de résistance multiples. La détection d'un faible niveau de résistance est bénéfique car elle indique l'imminence d'un problème. 

Les informations sur les mécanismes de résistance sont importantes pour les scientifiques car elles nous permettent de mieux comprendre l'évolution des plantes et pourraient servir de base à de nouvelles stratégies d'intervention pour la gestion de la résistance et la gestion générale des herbes indésirables. D'autre part, les producteuricess ne se préoccupent généralement que de la confirmation de la résistance et de la manière de la contrôler dans l'immédiat afin d'éviter les pertes économiques.

### Bioessais rapides pour tester la résistance

Étant donné que le bioessai classique sur les semis en pots prend environ 2 mois, des essais rapides ont été mis au point. Ces essais ont utilisé des plantes entières, des graines ou des plantules dans différents milieux de croissance. Dans cette section, quelques exemples d'essais rapides sont présentés en utilisant le matériel mentionné ci-dessus, à l'exception des essais visant à évaluer la résistance spécifique à un mode d'action, qui sont traités par <cite>Dayan</cite> et al. (2014).

#### Essai rapide sur plante entière pour les herbicides appliqués en POST

Cette procédure permet de confirmer la résistance au cours de la saison de culture en cours sans avoir à planter des semences ou à faire face à d'éventuels problèmes de dormance des semences. Les échappements d'une application POST précoce peuvent être testées et un traitement correctif immédiat peut être appliqué pour contrôler les herbes résistantes dans la culture en cours. Un test rapide sur plante entière a été développé spécifiquement pour le ryegrass (*Lolium rigidum* Gaudin) et le blackgrass (*Alopecurus myosuroides* Huds.) par <cite>Boutsalis</cite> (2001). Il s'agit de prélever des parties végétatives de plantes sur le terrain, de les transplanter dans des pots en serre et de pulvériser les transplants régénérés avec l'herbicide en question. Cela concerne généralement les plantes qui ont échappé à une application avant plantation ou à une application POST précoce, mais s'applique également aux plantes laissées dans le champ après la récolte. Bien que le protocole ait été développé pour les graminées, il a été adapté à certaines espèces de dicotylédones (<cite>Walsh</cite> et al. 2001). La procédure générale est la suivante.

**Étape 1. Collecte des plante** 

Suivre le même principe d'échantillonnage que précédemment, sauf qu'il s'agit ici de récolter des plantes entières. Les plantes en touffes qui ne suivent pas un schéma associé à la récolte, au travail du sol ou à l'application d'herbicides sont de bons candidats pour les tests de résistance. Prélever suffisamment d'échantillons pour représenter la distribution des plantes échappées dans le champ. Pour avoir suffisamment de matériel à tester, sélectionnez des plantes saines qui peuvent être divisées en de nombreux transplants. Déterrez les plantes individuelles, enlevez autant de terre que possible, coupez la plupart des pousses, enveloppez la partie racinaire dans du papier humide et placez-la dans un sac en plastique scellé pour éviter la dessiccation.

Les plantes qui se reproduisent déjà ne peuvent pas être utilisées pour cet essai, à moins qu'il ne s'agisse d'une espèce qui peut être clonée même lorsqu'elle fleurit ou porte des fruits. Les graminées qui commencent déjà à fleurir peuvent encore être collectées à condition que de nouvelles talles puissent être régénérées après que les pousses ont été coupées.

**Étape 2. Préparez les sections de plantes pour la transplantation**

Rincez les racines à l'eau du robinet. Séparez les talles en autant de sections que nécessaire pour le bioessai. Pour un contrôle rapide de la résistance, les traitements peuvent comprendre 0, 1 et 2 fois la dose recommandée au champ, pour au moins deux répétitions. au moins deux répétitions. Utilisez jusqu'à quatre répétitions si le matériel végétal le permet. si le matériel végétal le permet. Coupez les pousses de 2,5 à 5 cm et les racines de 0,5 à 3 cm et transplantez chaque bouture dans un pot séparé. chaque bouture dans un pot séparé. Le terreau commercial ou de la terre de culture peuvent être utilisés.

**Étape 3. Soin des plantes**

Gardez les plants bien arrosés, mais pas détrempés, sauf s'il s'agit d'une espèce qui prospère dans un sol inondé. Au cours de la première semaine, arrosez avec une solution d'engrais pour accélérer le processus de régénération. Fournissez un éclairage supplémentaire, tel que des lampes à halogénures, pour améliorer l'intensité lumineuse dans la serre. Cela accélérera la régénération des pousses et la repousse.

**Étape 4. Application d'herbicides** 

Laissez les plants se régénérer suffisamment pour que l'application de l'herbicide soit efficace. Le raygrass italien (*Lolium perenne* ssp. *multiflorum* L.) a besoin d'au moins 2 semaines après la transplantation pour être prêt à recevoir un traitement herbicide foliaire (<cite>Salas</cite> et al. 2012). Respecter le volume de pulvérisation et les additifs recommandés. Le chlorsulfuron (inhibiteur de l'ALS), le diclofop, le fenoxaprop, le fluazifop, l'haloxyfop et le sethoxydim (inhibiteurs de l'ACCase) et l'isoproturon (inhibiteur du photosystème II) ont été testés sur des boutures de de raygrass noir ou de raygrass rigide et ont produit une confirmation solide de la résistance à ces herbicides.

Des clones de raygrass à tallage dont la résistance a été confirmée ont été utilisés dans des essais dose-réponse et des expériences de suivi (<cite>Salas</cite> et al. 2012). 

Si les plantes sont échantillonnées au début de la saison de culture, les résultats peuvent être utilisés pour recommander une application potentielle d'herbicide correctif au cours de la même saison. La détection de la résistance n'est pas affectée par l'âge des plantes car les plantes régénérées réagissent de la même manière que les semis (<cite>Boutsalis</cite> 2001).

#### Essai rapide sur plante entière pour les inhibiteurs de croissance racinaire

Une variante du protocole ci-dessus a été développée par <cite>Cutulle</cite> et al. (2009) pour le pâturin annuel (*Poa annua* L.) en culture hydroponique. L'essai a été développé pour la pendiméthaline et la prodiamine, mais il pourrait être adapté à d'autres herbicides similaires. Ce test prend moins de temps temps que le test rapide pour les herbicides herbicides foliaires, mais il pourrait être coûteux à mettre en place pour des tests à grande échelle. La procédure générale est la suivante.

**Étape 1. Calculer les éléments nutritifs et l'herbicide pour le milieu liquide**

Pour les éléments nutritifs, utiliser la recette de <cite>Hoagland</cite> et </cite>Arnon (1950) ou ses dérivés plus récents. La composition de la solution nutritive peut nécessiter une optimisation pour s'adapter à l'espèce testée. Effectuer un essai pour calibrer la force de la solution nutritive à utiliser. La concentration normale de micronutriments peut être excessive pour de petites plantes. Par exemple, les semis de riz poussent mieux avec de l'eau déionisée pendant la première semaine d'établissementde leur installation, puis avec une solution nutritive à demi-dose pendant la deuxième semaine (<cite>Sales</cite> et al. 2011). Les options de stock des nutriments nécessaires peuvent alors être préparées en conséquence. Avant de procéder aux calculs finaux des quantités d'herbicides à différentes concentrations, effectuez un en utilisant un étalon sensible. Le dosage complet d'un mélange d'herbicides pour l'application sur le terrain peut être trop forte pour les transplants en culture liquide. Calibrer la gamme de doses d'herbicides pour qu'elle corresponde au mieux à la réaction attendue des plantes sur le terrain.

**Étape 2. Préparer les récipients de culture et le système d'aération**

Examinez la littérature pour trouver les récipients de culture et les systèmes d'aération appropriés (<cite>Gibeaut</cite> et al. 1997 ; <cite>Resh</cite> 2012 ; <cite>Sales</cite> et al. 2011). Les publications abondent sur la culture hydroponique des plantes. Pour les clones ou les semis, des récipients en forme de tubes sont suffisants. La mise en place d'une culture hydroponique pour la première fois est plus complexe que la préparation d'une expérience en pot. Si la réponse de la plante peut être évaluée après environ 1 semaine, l'agitation quotidienne de la solution nutritive peut être suffisante. Effectuez un essai de test.

**Étape 3. Préparer les transplants et surveiller la réaction de la plante**

Séparez les talles et coupez les racines à une longueur uniforme (par exemple, 1 à 2 cm) et les suspendre dans la solution nutritive. Si vous utilisez des récipients en forme de tubes à essai, la plante peut être maintenue en place à l'aide d'une boule de coton. Une autre alternative peut être la styromousse, coupée pour s'adapter au diamètre intérieur du récipient, avec une ouverture au milieu pour y placer la tige de la plante.

**Étape 4. Évaluer la réaction de la plante** 

Mesurer la régénération des racines régénération des racines en 10 j. Une simple évaluation visuelle ''oui'' ou ''non'' de la régénération des racines pourrait suffire à évaluer la réponse de la plante pour indiquer la résistance. Noter les autres réactions de la plante qui sont indicatives de l'effet de l'herbicide, telles que la chlorose (par évaluation visuelle ou à l'aide d'un compteur de chlorophylle SPAD-502, Konica Minolta Sensing, Inc.), le nombre de nouvelles racines ou la longueur des racines et des pousses.

#### Essais de germination des graines en milieu gélosé

L'adoption d'essais de germination dans des boîtes de Petri, des supports en papier ou d'autres milieux pouvant être utilisés sur des paillasses permet d'évaluer un grand nombre d'échantillons en moins de temps qu'il n'en faudrait pour effectuer l'essai classique sur la plante entière. Dans les essais en boites de Petri, les populations R et S putatives sont également testées en premier lieu avec une large gamme de doses afin de déterminer la dose discriminante avant d'effectuer des tests de résistance à grande échelle (<cite>Bourgeois</cite> et al. 1997 ; <cite>Kaundun</cite> et al. 2011). Bourgeois et al. (1997) ont utilisé un milieu gélosé dans un essai en boîte de Pétri pour déterminer les profils de résistance croisée d'une large collection d'avoine sauvage (*Avena fatua* L.) pour travailler sur les inhibiteurs de l'ACCase. Avant de s'engager dans cette approche, il convient d'apprendre la biologie de la graine. La procédure générale est la suivante.

**Étape 1. Collecte, nettoyage et stockage des semences**

Suivre le même principe que précédemment.

**Étape 2. Effectuer un test de germination pour déterminer le nombre minimum de graines à utiliser par boîte** 

Cela permet d'éviter d'éviter les pertes de temps et de ressources. L'objectif est d'avoir 10 plantules par traitement et par répétition dans les contrôles non traités et ajuster le nombre de graines en conséquence. Ceci est difficile à réaliser avec des espèces à graines fines ou avec des espèces de graminées telles que *Leptochloa* spp., où il est difficile de distinguer les fleurons pleins des fleurons vides.

**Étape 3. Déterminer la concentration d'herbicide à utiliser dans l'essai** 

Effectuez d'abord un essai dose-réponse pour déterminer la dose discriminante qui sera utilisée pour tester un grand nombre d'échantillons. La gamme de concentrations varie en fonction de l'herbicide.Par exemple, Bourgeois et al. (1997) ont utilisé des concentrations comprises entre 0 et 5 mmol pour le clodinafop et la cléthodime, et entre 0 et 30 mmol pour la tralkoxydime.

**Étape 4. Préparer le medium d'agar**

L'agar sert de point d'ancrage pour les graines en germination et de support pour distribuer l'herbicide et maintenir la plantule hydratée pendant toute la durée de l'expérience. On peut utiliser de 0,25 à 1,3 % (p/v) d'agarose (<cite>Cirujeda</cite> et al. 2001 ; <cite>Kaundun</cite> et al. 2011 ; <cite>Kim</cite> et al. 2000 ; <cite>Letouze</cite> et <cite>Gasquez</cite> 2000). Un mélange contenant trop peu d'agar peut ne pas se solidifier ; un mélange contenant trop d'agar peut se solidifier trop rapidement avant que le milieu puisse être versé dans les Petri. Calculez le volume total d'agar nécessaire pour remplir les boîtes de Petri.

Déterminez d'abord le volume d'eau volume d'eau nécessaire pour remplir chaque boîte de Pétri à moitié et multiplié par le nombre total de boîtes nécessaires.
Mesurez et mélangez l'agar avec de l'eau et faites-le fondre au micro-ondes. dans un four à micro-ondes. Refroidir l'agar fondu à environ 50°C et ajouter la concentration d'herbicide souhaitée. La concentration finale d'herbicide doit être celle qui se trouve dans le milieu agar et non dans la solution mère.

**Étape 5. Déterminer la dose discriminante à partir de l'étape 2**

Pour les essais à grande échelle, choisissez une concentration qui tue ou inhibe la croissance de toutes les plantules S, mais pas des plantules R. Pour ce faire, il faut au moins deux répétitions de l'essai dose-réponse.

**Étape 6. Préparez les boîtes de Petri et les semences**

Versez le volume nécessaire d'agar dans les boîtes de Petri dès que la température cible indiquée ci-dessus est atteinte. Sinon, le mélange d'agar peut se solidifier avant que tout le mélange ne soit versé sur les plaques. Laisser l'agar prendre au moins une nuit avant de placer les graines dans les plaques. Pour raccourcir la période d'attente, placer les plaques au réfrigérateur.

**Étape 7. Incuber les plaques pendant la période et les conditions de germination souhaitées**

Une fois que l'agar s'est solidifié, placer le nombre désiré de graines dans chaque plaque. Incuber pendant 7 jours ou plus, selon les besoins. De nombreuses espèces germent à température ambiante avec de la lumière ; d'autres sont sensibles à la lumière, et d'autres encore germent mieux à des températures plus fraîches ou plus chaudes. Une chambre de germination peut être nécessaire dans les cas où une température fraîche ou chaude est requise pour la germination, d'où la nécessité de déterminer les conditions optimales de germination avant de commencer ce test.

**Étape 8. Évaluer la réaction des semis**

Mesurer la longueur des coléoptiles à la fin de la période d'incubation (7 à 14 jours). Ce test a permis d'identifier différents types de résistance de l'avoine aux inhibiteurs de l'ACCase (<cite>Bourgeois</cite> et al. 1997). Le type A présente une forte résistance aux herbicides à base d'aryloxyphénoxypropionate
(AOPP), mais une résistance faible ou nulle aux herbicides à base de cyclohexanedione (CHD). Le type B présente une résistance faible à modérée aux herbicides à base d'AOPP et de CHD, tandis que le type C est très résistant à tous les herbicides inhibiteurs de l'ACCase testés.

#### Essai en Petri avec des graines prégermées

Une légère variante de la procédure ci-dessus consiste à incuber des graines prégermées dans des boîtes de Pétri tapissées de papier imbibé de diverses concentrations de l'herbicide en question. Mesurer la longueur des coléoptiles dans les 3 à 7 jours suivant l'incubation. Cette méthode a été utilisée pour tester la résistance aux inhibiteurs de l'ACCase chez l'échinochloa pied-de-coq (<cite>Huan</cite> et al. 2011), la sétaire verte (*Setaria viridis* L.) (<cite>De ́lye</cite> et al. 2002) et le millet vivace [*Sorghum* (L.) Pers.] (<cite>Burke</cite> et al. 2006) et résistance aux inhibiteurs de l'ALS chez la renouée persicaire [*Descurainia sophia* (L.) Webb ex Prantl] (<cite>Xu</cite> et al. 2010).

#### Essai sur plaque ELISA utilisant des graines prégermées

La résistance du raygrass au glyphosate peut être détectée en en faisant prégermer les graines dans des boîtes de Pétri et en transférant les graines germées sur des plaques d'essai immuno-enzymatique (ELISA), remplies d'une solution de glyphosate à différentes concentrations (<cite>Ballot</cite> et al. 2009).

**Étape 1.**

Faites germer un grand nombre de graines dans une boîte de Pétri tapissée de papier filtre humecté d'une d'une solution aqueuse contenant un fongicide. Ballot et al. (2009) ont utilisé 0,76 g L<sub>-1</sub> d'iprodione pour la germination des semences de raygrass. Incuber les graines dans des conditions optimales pour germination.

**Etape 2** 

Après 2 jours, transférer les graines germées dans des puits de plaques ELISA remplis de 150 ml de solution de de solution de glyphosate en 12 concentrations allant de de 0 à 3 200 g L<sub>-1</sub>. Utiliser une solution mère fraîchement préparée.

**Étape 3** 

Placer les plaques ELISA dans des boîtes en plastique transparent recouvertes de papier humide et munies d'un couvercle. Pour une herbe de saison fraîche comme le raygrass, incuber à 15 C avec une photopériode de 16 heures.

**Étape 4** 

Après 7 jours, mesurer la longueur des coléoptiles. L'essai est terminé en 16 jours. Les résultats sont étroitement corrélés (R² = 0,95) avec ceux du test classique.

#### Essai sur les graines prégermées en milieu perlite

La perlite peut être utilisée comme alternative au milieu liquide. Les graines prégermées peuvent être placées dans des pots remplis d'un meidum perlite et arrosés quotidiennement avec une solution nutritive avec ou sans herbicide (<cite>Breccia</cite> et al. 2011). Placer les pots dans la serre ou la chambre de croissance selon le cas. Cette méthode a été développée pour tester la résistance du tournesol (*Helianthus annuus* L.) aux herbicides à base d'imidazolinone, en particulier l'imazapyr, à une dose allant jusqu'à 10 mM. Dans 2 semaines, mesurer la longueur des pousses et des racines. Ce test fonctionne avec des herbicides actifs dans le sol ou dans le feuillage.

#### Essai de semis sur gélose

Kaundun et al. (2011) ont mis au point un test RISQ (resistance in-season quick) pour détecter la résistance aux inhibiteurs de l'ACCase et de l'ALS parmi les espèces de graminées, y compris le blackgrass, la sétaire verte, le phalaris (*Phalaris paradoxa* L.), le raygrass et l'avoine sauvage dans un milieu gélosé. Cette méthode a d'abord été mise au point pour les inhibiteurs de l'ACCase, le clodinafop-propargyl et le pinoxaden, et pour l'inhibiteur de l'ALS, l'iodo-mésosulfuron. Utilisez des boîtes de Pétri pouvant accueillir des semis de l'espèce testée. L'objectif est de détecter la résistance dans une
champ infesté par l'espèce à problème au stade précoce de la avant la première application d'un herbicide POST . Ce test a été utilisé dans le laboratoire de Burgos pour détecter la résistance du raygrass au pinoxaden dans des champs de blé. La procédure générale est la suivante.

**Étape 1. Collecte des semis** 

Prélevez 15 plantules de 1 à 3 feuilles sur quatre sites dans le champ, en veillant à ce que les racines soient intactes. Enlevez autant de terre que possible possible et placez chaque groupe de plantules dans un dans un sac en plastique scellé. Rincer délicatement les racines à l'eau du robinet, enveloppez-les dans une serviette en papier mouillée et placez-les dans une glacière pour le transport vers l'installation du bioessai. Ce test fonctionne avec du raygrass à 4 talles. Séparez lesles talles avant la mise en culture et n'utiliser qu'une seule talle par plante.

**Étape 2. Préparation des populations de référence**

En prévision du moment de l'échantillonnage sur le terrain, cultiver suffisamment de plantules de la population S dans la serre (ou à l'extérieur, si possible). Si une population R connue est disponible, l'inclure dans tous les essais. Pour s'assurer que les populations standard seront au même stade de croissance que les plantules prélevées sur le terrain, planter chaque semaine un lot de la (des) population(s) de référence.

**Étape 3 : Préparation de la gélose**

Mélanger 0,5 à 0,8 % (wt/v) d'agarose avec de l'eau. Faire fondre l'agar au micro-ondes. Laisser l'agar refroidir à environ 50°C et ajouter la quantité d'herbicide calculée pour chaque concentration. Verser l'agar dans les boîtes de Pétri à une profondeur de 1 cm et laisser reposer à 4°C pendant 24 h avant de placer les plantules sur le milieu.

**Étape 4. Déterminer la dose discriminante**

Comme pour les autres bioessais, procédez à un essai adose-réponse. La recherche de la gamme de concentration appropriée peut nécessiter plus d'essais sur ce milieu que dans l'essai classique sur les plantules. Répétez chaque traitement trois fois.

**Étape 5. Transplanter les plantules sur le milieu gélosé**

Placez quatre plantules exemptes de sol dans chaque plaque, en positionnant soigneusement les plantes de manière à ce que les racines soient fermement en contact avec l'agar. Placez le couvercle sur la boîte de Pétri et scellez-le avec du parafilm pour éviter que l'agar ne se dessèche.

**Étape 6. Incubation** 

Incuber les plaques en chambre de croissance ou en serre avec une photopériode de 16 heures à une intensité lumineuse de 15 à 180 umol m<sub>-2</sub> s<sub>-1</sub> et à une température appropriée. Pour le raygrass, l'essai donne de meilleurs résultats à une température jour/nuit de 25/16 °C. Lorsque vous effectuez l'essai pour la première fois, optimisez d'abord les conditions d'incubation.

**Étape 7. Évaluation**

Les traitements peuvent être évalués entre 10 et 14 jours. À la fin de la période d'incubation, les plantes S seront soit mortes, soit gravement chlorotiques et n'auront pas de croissance racinaire. En revanche, les plantes R resteront principalement vertes, développeront une nouvelle feuille  et auront une croissance racinaire importante. Ce test détermine la fréquence de la résistance, mais pas le niveau de résistance. 
Toutefois, on peut également enregistrer les données relatives aux lésions et à la biomasse et déterminer si ces réponses sont en corrélation avec la dose d'herbicide.

Si aucune information supplémentaire n'est collectée, les données résultantes seront la fréquence des plantes résistantes dans le champ échantillonné. Le fabricant, le spécialiste ou l'agent d'information peut alors décider si l'herbicide en question peut encore être recommandé pour ce champ. L'agriculteurice peut également décider si son programme de gestion permet de contrôler le nombre d'échappements lors d'une application du même herbicide.

## Remarques finales

Le bioessai classique sur les semis reste l'essai biologique le plus couramment utilisé pour tester la résistance aux herbicides. En effet, il s'agit de la simulation la plus proche des plantes qui poussent dans les champs. Il détecte également la résistance quel que soit le mécanisme. Les tests alternatifs et rapides décrits ici détectent également la résistance quel que soit le mécanisme, mais les conditions de culture sont "radicalement différentes". 
Pour avoir confiance dans les résultats de ces tests biologiques alternatifs, il est important de comparer leur diagnostic avec celui du bioessai classique. Si le test alternatif produit le même diagnostic que le test classique, alors le test rapide est le meilleur pour tester un grand nombre d'échantillons dans un court laps de temps. Une stratégie efficace de gestion de la résistance dépend de la qualité des études de terrain, de l'échantillonnage des plantes ou des semences, du stockage des semences, du choix des tests, de l'utilisation de populations de référence et de l'interprétation des résultats des tests.

# Références

**Literature Cited**

+ Baldoni, G, Catizone, P, Viggiani, P (2000) Relationship between seed bank and actual weed flora as influenced by soil tillage and chemical control. Ital J Agron 4(1):11–22
+ Ballot, R, Deschomets, G, Gauvrit, C (2009) A quick test for glyphosate resistance in ryegrass. Pages 1–8in 13th International Colloquium on Weed Biology. Dion, FranceAFPP
+ Beckie, HJ, Heap, IM, Smeda, RJ, Hall, LM (2000) Screening for herbicide resistance in weeds. Weed Technol 14:428–445
+ Bewley, JD, Black, M (1994) Seeds: Physiology of Development and Germination. The Language of Science. New YorkPlenum Press. 230 p
+ Bewley, JD, Bradford, K, Hilhorst, H, Nonogaki, H (2012) Seeds: Physiology of Development, Germination, and Dormancy, 3rd edn. New YorkSpringer. 391 p
+ Bond, JA, Oliver, LR (2006) Comparative growth of Palmer amaranth (Amaranthus palmeri) accessions. Weed Sci 54:121–126
+ Bourgeois, L, Kenkel, NC, Morrison, IN (1997) Characterization of cross-resistance patterns in acetyl-CoA carboxylase inhibitor resistant wild oat (Avena fatua). Weed Sci 45:750–755
+ Boutsalis, P (2001) Syngenta quick-test: a rapid whole-plant test for herbicide resistance. Weed Technol 15:257–263
+ Breccia, G, Vega, T, Nestares, G, Mayor, ML, Zorzoli, R, Picardi, L (2011) Rapid test for detection of imidazolinone resistance in sunflower (Helianthus annuus L.). Plant Breed 130:109–113
+ Buhler, DD, Hoffman, ML (1999) Andersen's guide to practical methods of propagating weeds and other plants. Lawrence, KSWeed Science Society of America. 248 p
+ Burgos, NR, Culpepper, S, Dotray, P, Kendig, JA, Wilcut, J, Nichols, R (2006) Managing Herbicide Resistance in Cotton Cropping Systems. Cotton Inc Tech Bull for the Southern U.S. http://www.cottoninc.com/fiber/AgriculturalDisciplines/Weed-Management/Herbicide-Resistance-Cotton-Cropping-Systems/Managing-Herbicide-Resistance.pdf. Accessed: January 20, 2014
+ Burgos, NR, Tranel, PJ, Streibig, JC, Davis, VM, Shaner, D, Norsworthy, JK, Ritz, C (2013) Review: confirmation of resistance to herbicides and evaluation of resistance levels. Weed Sci 61:4–20
+ Burke, IC, Thomas, WE, Burton, JD, Spears, JF, Wilcut, JW (2006) A seedling assay to screen aryloxyphenoxypropionic acid and cyclohexanedione resistance in johnsongrass (Sorghum halepense). Weed Technol 20:950–955
+ Cha, TS, Najihah, MG, Sahid, IB, Chuah, TS (2014) Molecular basis for resistance to ACCase-inhibiting fluazifop in Eleusine indica from Malaysia. Pestic Biochem Physiol 111:7–13
+ Chandi, A, Milla-Lewis, SR, Jordan, DL, York, AC, Burton, JD, Zuleta, MC, Whitaker, JR, Culpepper, AS (2013) Use of AFLP markers to assess genetic diversity in Palmer amaranth (Amaranthus palmeri) populations from North Carolina and Georgia. Weed Sci 61:136–145
+ Chauhan, BA, Johnson, DE (2008a) Germination ecology of Chinese sprangletop (Leptochloa chinensis) in the Philippines. Weed Sci 56:820–825
+ Chauhan, BA, Johnson, DE (2008b) Germination ecology of southern crabgrass (Digitaria ciliaris) and India crabgrass (Digitaria longiflora): two important weeds of rice in the tropics. Weed Sci 56:722–728
+ Chauhan, BA, Johnson, DE (2009) Seed germination and seedling emergence of synedrella (Synedrella nodiflora) in a tropical environment. Weed Sci 57:36–42
+ Cirujeda, A, Recasens, J, Taberner, A (2001) A qualitative quick-test for detection of herbicide resistance to tribenuron-methyl in Papaver rhoeas. Weed Res 41:523–534
+ Cutulle, MA, McElroy, JS, Millwood, RW, Sorochan, JC, Stewart, CN Jr. (2009) Selection of bioassay method influences detection of annual bluegrass resistance to mitotic-inhibiting herbicides. Crop Sci 49:1088–1095
+ Dayan, FE, Owens, DK, Corniani, N, Silva, FML, Watson, SB, Howell, J, Shaner, DL (2015) Biochemical markers and enzyme assays for herbicide mode of action and resistance studies. Weed Sci 63:23–63
+ Délye, C, Dohoux, A, Pernin, F, Riggins, C, Tranel, P (2015) Molecular mechanisms of weed resistance. Weed Sci 63:91–115
+ Délye, C, Wang, T, Darmency, H (2002) An isoleucine substitution in chloroplastic acetyl-CoA carboxylase from green foxtail (Setaria viridis L. Beauv.) is responsible for resistance to the cyclohexanedione herbicide, sethoxydim. Planta 214:421–227
+ Dickson, JW, Scott, RC, Burgos, NR, Salas, RA, Smith, KL (2011) Confirmation of glyphosate-resistant Italian ryegrass (Lolium perenne ssp. multiflorum) in Arkansas. Weed Technol 25:674–679
+ Đikić, M, Gadţo, D, Gavrić, T, Šapčanin, S, Podrug, A (2011) Dormancy and weed seed germination. Herbologia 12:150–155
+ Gaines, TA, Wright, AA, Molin, WT, Lorentz, L, Riggins, CW, Tranel, PJ, Beffa, R, Westra, P, Powles, SB (2013) Identification of genetic elements associated with EPSPS gene amplification. PLoS ONE 8:e65819 DOI:10.1371/journal.pone.0065819
+ Gibeaut, DM, Hulett, J, Cramer, GR, Seemann, JR (1997) Maximal biomass of Arabidopsis thaliana using a simple, low maintenance hydroponic method and favorable environmental conditions. Plant Physiol 115:317–319
+ Hausman, NE, Singh, S, Tranel, PJ, Riechers, DE, Kaundun, SS, Polge, ND, Thomas, DA, Hager, AG (2011) Resistance to HPPD-inhibiting herbicides in a population of waterhemp (Amaranthus tuberculatus) from Illinois, United States. Pest Manag Sci 67:258–261
+ Heap, I (2014) The International Survey of Herbicide Resistant Weeds. www.weedscience.org. Accessed January 16, 2014
+ Hoagland, DR, Arnon, DI (1950) The water-culture method for growing plants without soil. Journal circular. 2nd edn. Calif Agric Exp Stn 347:1–32
+ Honek, A, Martinkova, Z (1996) Geographic variation in seed dormancy among populations of Echinochloa crus-galli. Oecologia 108:419–423
+ Huan, Z, Zhang, H, Hou, Z, Zhang, S, Zhang, Y, Liu, W, Bi, Y, Wang, J (2011) Resistance level and metabolism of barnyardgrass (Echinochloa crus-galli L. Beauv.) populations to quizalofop-P-ethyl in Heilongjiang province, China. Agric Sci China 10:1914–1922
+ Kaloumenos, NS, Adamouli, VN, Dordas, CA, Eleftherohorinos, IG (2011) Corn poppy (Papaver rhoeas) cross-resistance to ALS-inhibiting herbicides. Pest Manag Sci 67:574–585
+ Karlsson, LM, Ericsson, JAL, Milberg, P (2006) Seed dormancy and germination in the summer annual Galeopsis speciosa. Weed Res 46:353–361
+ Kaundun, SS, Hutchings, H-J, Dale, RP, Bailly, GC, Glanfield, P (2011) Syngenta ‘RISQ’ test: a novel in-season method for detecting resistance to post-emergence ACCase and ALS inhibitor herbicides in grass weeds. Weed Res 51:284–293
+ Kim, DS, Brain, P, Marshall, EJP, Caseley, JC (2002) Modeling herbicide dose and weed density effects on crop:weed competition. Weed Res 42(1):1–13
+ Kim, DS, Riches, CR, Valverde, BE (2000) Rapid detection of propanil and fenoxaprop resistance in Echinochloa colona. Weed Sci 48:695–700
+ Kovach, DA, Widrlechner, MP, Brenner, DM (2010) Variation in seed dormancy in Echinochloa and the development of a standard protocol for testing. Seed Sci and Technol 38:559–571
+ Lati, RN, Filin, S, Eizenberg, H (2012) Effect of tuber density and trifloxysulfuron application timing on purple nutsedge (Cyperus rotundus) control. Weed Sci 60:494–500
+ Legleiter, TR, Bradley, KW (2008) Glyphosate and multiple herbicide resistance in common waterhemp (Amaranthus rudis) populations from Missouri. Weed Sci 56:582–587
+ Letouze', A, Gasquez, J (2000) A pollen test to detect ACCase target-site resistance within Alopecurus myosuroides populations. Weed Res 40:151–162
+ Maneechote, C, Samanwong, S, Zhang, XQ, Powles, SB (2005) Resistance to ACCase-inhibiting herbicides in sprangletop (Leptochloa chinensis). Weed Sci 53:290–295
+ Martinkova, Z, Lukas, J, Honek, A (2006) Seed age and storage conditions influence germination of barnyardgrass (Echinochloa crus-galli). Weed Sci 54(2):298–304
+ Monsanto. 2014. BioDirect™ Technology: An Agricultural Biological Platform. http://www.monsanto.com/products/pages/biodirect-ag-biologicals.aspx. Accessed September 9, 2014
+ Moss, S (1999) Detecting herbicide resistance: guidelines for conducting diagnostic tests and interpreting results. Herbicide Resistance Action Committee (HRAC). http://www.hracglobal.com/Education/DetectingHerbicideResistance.aspx. Accessed January 20, 2014
+ Nei, M (1973) Analysis of gene diversity in subdivided populations. PNAS 70:3321–3323
+ Nkoa, R, Owen, MDK, Swanton, CJ (2015) Weed abundance, distribution, diversity, and community analyses. Weed Sci 63:64–90
+ Norsworthy, JK, Ward, SM, Shaw, DR, Llewellyn, RS, Nichols, RL, Webster, TM, Bradley, KW, Frisvold, G, Powles, SB, Burgos, NR, Witt, WW, Barrett, M (2012) Reducing the risks of herbicide resistance: best management practices and recommendations. Weed Sci 60:31–62
+ Oveisi, M, Mashhadi, HR, Baghestani, MA, Alizadeh, H, Andujar, JL (2010) Modeling herbicide dose effect and multiple weed species interference in corn. Pages 548–551in Proceedings of the 3rd Iranian Weed Science Congress. Volume 1: Weed Biology and Ecophysiology. Babolsar, IranIranian Society of Weed Science
+ Owen, MJ, Goggin, DE, Powles, SB (2012) Non-target-site-based resistance to ALS-inhibiting herbicides in six Bromus rigidus populations from western Australian cropping fields. Pest Manag Sci 68:1077–1082
+ Patzoldt, WL, Tranel, PJ, Hager, AG (2005) A waterhemp (Amaranthus tuberculatus) biotype with multiple resistance across three herbicide sites of action. Weed Sci 53:30–36
+ Resh, HM (2012) Hydroponic Food Production: A Definitive Guidebook for the Advanced Home Gardener and the Commercial Hydroponic Grower. 7th edn. New YorkCRC Press. 524 p
+ Ritz, C, Kniss, AR, Streibig, JC (2015) Statistics in weed science. Weed Sci 63:166–187
+ Salas, RA, Dayan, FE, Pan, Z, Watson, SB, Dickson, JW, Scott, RC, Burgos, NR (2012) EPSPS gene amplification in glyphosate-resistant Italian ryegrass (Lolium perenne ssp. multiflorum) from Arkansas. Pest Manag Sci 68:1223–1230
+ Sales, MA, Burgos, NR, Shivrain, VK, Murphy, B, Gbur, EE Jr. (2011) Morphological and physiological responses of weedy red rice (Oryza sativa L.) and cultivated rice (O. sativa) to N supply. Am J Plant Sci 2:569–577
+ Simpson, GM (1990) Seed Dormancy in Grasses. New YorkCambridge University Press. 297 p
+ Terrell, EE (1968) A Taxonomic Revision of the Genus Lolium Washington, DCU.S. Government Printing Office, U.S. Department of Agriculture Technical Bull. 1392. 2 p
+ Tseng, TM, Burgos, NR, Shivrain, VK, Alcober, EA, Mauromoustakos, A (2013) Inter- and intrapopulation variation in dormancy of Oryza sativa (weedy red rice) and allelic variation in dormancy-linked loci. Weed Res 53:440–451
+ Veihmeyer, FJ, Hendrickson, AH (1931) The moisture equivalent as a measure of the field capacity of soils. Soil Sci 32:181–193
+ Vencill, WK, Nichols, RL, Webster, TM, Soteres, JK, Mallory-Smith, C, Burgos, NR, Johnson, WG, McClelland, MR (2012) Herbicide resistance: toward an understanding of resistance development and the impact of herbicide-resistant crops. Weed Sci 60:2–30
+ Walsh, MJ. Duane, RD, Powles, SB (2001) High frequency of chlorsulfuron-resistant wild radish (Raphanus raphanistrum L.) populations across the western Australian wheatbelt. Weed Technol 15:199–203
+ Wise, AM, Grey, TL, Prostko, EP, Vencill, WK, Webster, TM (2009) Establishing the geographical distribution and level of acetolactate synthase resistance of Palmer amaranth (Amaranthus palmeri) accessions in Georgia. Weed Technol 23:214–220
+ Xu, X, Wang, GQ, Chen, SL, Fan, CQ, Li, BH (2010) Confirmation of flixweed (Descurainia sophia) resistance to tribenuron-methyl using three different assay methods. Weed Sci 58:56–60
+ Yu, Q, Powles, SB (2014) Resistance to AHAS inhibitor herbicides: current understanding. Pest Manag Sci 70:1340–1350 DOI: 10.1002/ps.3710
+ Zheng, D, Kruger, GR, Singh, S, Davis, VM, Tranel, PJ, Wellerd, SC, Johnson, WG (2011) Cross-resistance of horseweed (Conyza canadensis) populations with three different ALS mutations. Pest Manag Sci 67:1486–1492
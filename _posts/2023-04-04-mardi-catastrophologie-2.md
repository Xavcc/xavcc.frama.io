---
title: "Mardi catastrophologie : votre fournée de ressources (n°2)"
layout: post
date: 2023-04-04 06:40
image: /assets/images/rain-seed.jpg
headerImage: true
tag:
- hsociety
- Catastrophologie
category: blog
author: XavierCoadic
description: 
---

> « La finalité de l’activité scientifique est une finalité d’émancipation, orientée par une éthique située et par la contribution au bien commun (<cite>Piron</cite>, 2005 ; <cite>Charmillot</cite>, 2017). » *Merci Clailou*

+ [Publi] **Réponse au changement climatique et questions migratoires** Par <cite>Camille Le Coz</cite> (2023)

Politiste au Migration Policy Institute aborde le sujet du nécessaire et très difficilement croisement interdisciplinaire entre les études relatives au changement climatique et aux défis migratoires afférents. Ses travaux et ceux de cette communauté de recherche, tout en battant en brèche certaines antiennes des associations et ONG agissant dans ces deux champs (sur la nature et la force des migrations observées), l'auteur montre une nouvelle fois les grandes difficultés de croisement de regards disciplinaires.
L'interdisciplinarité est un combat d'indisciplinés combatifs...

> « Loin des discours alarmistes sur de futures vagues de réfugiés climatiques, la recherche sur les liens entre climat et migrations a permis de mettre en lumière des dynamiques nuancées. Le changement climatique et les dégradations environnementales peuvent pousser des populations à se déplacer, mais celles-ci s’installent souvent dans une région voisine.

<https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=re_ponse_au_changement_climatique_et_questions_migratoires.pdf>

+ [Publi] **Décryptage : projet de loi « Immigration et intégration »** (France, Mars 2023) Dirigé par <cite>Tania Racho</cite>, <cite>Marie-Caroline Saglio-Yatzimirsky</cite> et <cite>Emeline Zougbédé</cite>. Avec <cite>François Héran</cite>, <cite>Francesca Sirna</cite>, <cite>Serge Slama</cite> et <cite>Adèle Sutre</cite>

<https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=de-facto_actu_vdef-planches.pdf>

+ [Bulletin] **Etat des nappes d’eau souterraine de la Bretagne à fin février 2023** (BRGM)

<https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=bulletin_brgm_nappe_03-2023.pdf>

+ [Investigation]
  + Nigeria
    + [SIAT Nigeria Land Grabbing, Pollution, Causing Hardship In Host Communities](https://saharareporters.com/2023/03/03/siat-nigeria-land-grabbing-pollution-causing-hardship-host-communities), Sahara Reporters, 3 March 2023
    + [SIAT Nigeria land acquisition, pollution, causing hardship in host communities](https://newswirengr.com/2023/03/01/siat-nigeria-land-acquisition-pollution-causing-hardship-in-host-communities/), NewsWireNGR, 1 March 2023
  + France
    + [ArcelorMittal : révélations sur un pollueur hors-la-loi](https://disclose.ngo/fr/article/arcelormittal-revelations-sur-un-pollueur-hors-la-loi), Disclose, 10 March 2023
    + [ArcelorMittal : un champion des émissions de CO2 biberonné  aux aides publiques](https://disclose.ngo/fr/article/arcelormittal-un-champion-des-emissions-de-co2-biberonne-aux-aides-publiques), Disclose, 10 March 2023
    + [ArcelorMittal dépasse les seuils de pollution 240 jours par an malgré les millions de l’État](https://marsactu.fr/arcelormittal-depasse-les-seuils-de-pollution-240-jours-par-an-malgre-les-millions-de-letat/), Marsactu, 10 March 2023
    + [Le système d’autocontrôle d’ArcelorMittal mis en cause](https://marsactu.fr/le-systeme-dautocontrole-darcelormittal-mis-en-cause/), Marsactu, 10 March 2023
    + [Ex Ilva di Taranto, transizione impossibile](https://irpimedia.irpi.eu/ex-ilva-taranto-transizione-verde-impossibile/), IRPI, 10 March 2023

+ [Actu] « **Pollution atmosphérique. La Bretagne émet beaucoup plus d’ammoniac que tout le reste de la France, alerte le collectif Bretagne contre les fermes-usines** » <https://web.archive.org/web/20230312225038/https://france3-regions.francetvinfo.fr/bretagne/finistere/pollution-atmospherique-la-bretagne-emet-beaucoup-plus-d-ammoniac-que-tout-le-reste-de-la-france-alerte-le-collectif-bretagne-contre-les-fermes-usines-2730542.html>

+ [Actu (mery Khrys)] **Au sud de Lyon, des PFAS retrouvés dans les œufs, la préfecture recommande de ne plus en manger dans 16 communes** <https://www.liberation.fr/environnement/pollution/au-sud-de-lyon-des-pfas-retrouves-dans-les-oeufs-la-prefecture-recommande-de-ne-plus-en-manger-dans-seize-communes-20230403_NGDUZHTRXJD57II7OOIJYA6474/>

> « Depuis mai dernier, le sud de Lyon est secoué par les révélations de l’émission Vert de Rage sur une contamination aux PFAS : ils ont été retrouvés dans l’eau, l’air, les sols et même le lait maternel. Au fil des prélèvements s’est révélée l’ampleur de la pollution : des PFAS sont décelés dans les œufs, les poissons du Rhône, les mâches de producteurs des monts du Lyonnais et même les captages d’eau potable de communes situées plus au sud. »

+ [Actu] **Sainte-Soline : l’aveuglement « à haut risque » du gouvernement** 

>  «De nombreux blessés, parmi les manifestants et les gendarmes, dont plusieurs « en urgence absolue » : la mobilisation contre les mégabassines dans les Deux-Sèvres a été marquée par des violences. Mais on assiste bien en France à l’émergence d’un mouvement social pour l’eau. » <https://www.mediapart.fr/journal/ecologie/250323/sainte-soline-l-aveuglement-haut-risque-du-gouvernement>

> « **À Sainte-Soline, des armes de guerre employées sans retenue** » <https://www.liberation.fr/societe/police-justice/a-sainte-soline-des-armes-de-guerre-employees-sans-retenue-20230326_5M4HFKHWCBD73BVQBS2UIQNFHM/>

> « **Les mégabassines sont une mal-adaptation aux sécheresses présentes et à venir** ». En soutien au mouvement contre les méga-bassines, Scientifiques en rébellion publie aujourd’hui une tribune rappelant les enjeux qui se jouent avec cette bataille de l’eau : Le déploiement de ces retenues à ciel ouvert menace la préservation de l’eau et freine la transformation de notre modèle socio-économique et de nos modes de vie <https://telegra.ph/Les-mégabassines-sont-une-mal-adaptation-aux-sécheresses-présentes-et-à-venir-03-27>

+ [Déclaration] **CNOE : Manifestations en France : les libertés d’expression et de réunion doivent être protégées contre toute forme de violence** <https://www.coe.int/fr/web/commissioner/-/manifestations-en-france-les-libert%C3%A9s-d-expression-et-de-r%C3%A9union-doivent-%C3%AAtre-prot%C3%A9g%C3%A9es-contre-toute-forme-de-violence>

+ [Actu] **Sainte-Soline : la Défenseure des droits se saisit des cas des deux manifestants grièvement blessés** <https://www.lefigaro.fr/actualite-france/sainte-soline-la-defenseur-des-droits-se-saisit-des-cas-des-deux-manifestants-grievement-blesses-20230330>

+ [Actu] **L'humanité "vampirique" épuise "goutte après goutte" les ressources en eau de la planète, a alerté l'ONU avant le début mercredi d'une conférence pour tenter de répondre aux besoins de milliards de personnes, en danger face à une crise mondiale de l'eau "imminente".** (Mars 2023) <https://www.rts.ch/info/monde/13882642-les-penuries-deau-se-generalisent-avec-un-risque-imminent-de-crise-mondiale-alerte-lonu.html>

+ [Militance] **Soirée de solidarité avec les luttes contre Danone et l’accaparement de l’eau au Mexique**

> « "Depuis près de trente ans, la multinationale française DANONE pille les nappes phréatiques du territoire des villages nahuas de la région de Cholula, État de Puebla, au Mexique, au travers de sa filiale mexicaine BONAFONT, principale marque d’eau en bouteille de ce pays. » <https://www.agendamilitant.org/Soiree-de-solidarite-avec-les-luttes-contre-Danone-et-l-accaparement-de-l-eau-2229.html>

+ [Publi (merci Nershelam)] **GIEC chapitre 4 du 6e rapport du 2e groupe de travail (vulnérabilités et adaptation)**. Il y a dans le sommaire du chapitre : 
  + partie 4.3.8 **Impacts observés sur l'usage culturel de l'eau de peuples indigènes, communautés locales et peuples traditionnels**
  + partie 4.5.8 **Risques projetés sur l'usage culturel de l'eau de peuples indigènes, communautés locales et peuples traditionnels**
  + partie 4.6.9 **Adaptation sur l'usage culturel de l'eau de peuples indigènes, communautés locales et peuples traditionnels**(c'est une des deux sous partie sur les 'solutions basées sur la nature, la deuxieme est sur les migrations' ) 
  + partie 4.8.4 **Inclusion des savoirs indigènes et savoirs locaux**
    + la partie 4.8 est intitulée principe permettant d'atteindre la sécurité en eau de manière durable et un développement résilient au climat à travers la transformation des système , les sous parties sont  : 
      + 1- technologie appropriées
      + 2- financement appropriés
      + 3- genre, égalité et justice sociale
      + 4- inclusion des savoirs indigènes et savoirs locaux 
      + 5- participation, coopération et engagement bottom-up (du bas vers le haut) 
      + 6- gouvernence de l'eau multi centre 
      + 7- fort support politique

<br />
+ [Actu] **Liberté de manifester et liberté de la presse en danger** (France)

> « Après des journées marquées par des manifestations dans toute la France, la Commission nationale consultative des droits de l’homme (CNCDH) s’inquiète de certains agissements des forces de l’ordre observés en particulier depuis jeudi 16 mars. » <https://www.cncdh.fr/actualite/liberte-de-manifester-et-liberte-de-la-presse-en-danger>

+ [Actu] **La France, premier pays d’Europe à légaliser la surveillance biométrique** <https://www.laquadrature.net/2023/03/23/la-france-premier-pays-deurope-a-legaliser-la-surveillance-biometrique/>

+ [Actu] **Acrylate Water Safety Emergency Hits Philly; Residents Scramble for Bottled Water**, Philadelphia (USA) <https://www.anarchistfederation.net/acrylate-water-safety-emergency-hits-philly-residents-scramble-for-bottled-water/>


+ [Livre] **Géographie de la domination. Capitalisme et production de l’espace**, <cite>David Harvey</cite> (ed. Amsterdam, 2018) <https://www.editionsamsterdam.fr/geographie-de-la-domination-2/>

+ [Livre] **Les catastrophes et l'interdisciplinarité ; Dialogues, regards croisés, pratiques**. <cite>Virginia Garcia Acosta & Alain Musset</cite> (2018) <https://www.editions-harmattan.fr/livre-les_catastrophes_et_l_interdisciplinarite_dialogues_regards_croises_pratiques_virginia_garcia_acosta_alain_musset-9782806103673-58407.html>

+ [Publi] **The Yungay Avalanche Of 1970: Anthropological Perspectives On Disaster And Social Change**, <cite>Anthony Oliver-Smith</cite> (1979) <https://onlinelibrary.wiley.com/doi/abs/10.1111/j.1467-7717.1979.tb00205.x>
  + Interview with Anthony Oliver-Smith (2021) <https://www.appliedanthro.org/publications/news/november-2021/interview-anthony-oliver-smith>

+ [Publi] **Virginia García-Acosta (éd.), The Anthropology of Disasters in Latin America. State of the Art**
(Londres, Routledge, 2019) par <cite>Sandrine Revet</cite> *p. 265-268* https://doi.org/10.4000/cal.12370

> « Publié chez Routledge dans la série « Routledge Studies in Hazards, Disaster Risk and Climate Change », dirigée depuis 2015 par le chercheur britannique Ilan Kelman qui est un spécialiste de la réduction des risques de catastrophe, ce volume est dédié à un état de l’art de l’anthropologie des catastrophes en Amérique latine. Virginia García-Acosta, anthropologue et historienne mexicaine qui fait partie des pionnières dans l’étude anthropologique et historique des désastres dans la région et dont les trois volumes qu’elle a coordonnés sous le titre Historia y desastres en América Latina sont désormais des classiques, a réalisé ici un important travail en réunissant des chercheurs de plusieurs pays et en les faisant travailler sur ce qui se produit en anthropologie sur la thématique des catastrophes dans chacun de leur pays. » <https://journals.openedition.org/cal//12370>

+ [Dossier] **Modes de gouvernement en HaÏti après le séisme de 2010** <https://journals.openedition.org/cal//3083>
  + [Publi] **Mark Schuller, Pablo Morales (éd.) Tectonic Shifts. Haïti since the earthquake** (Sterling, Kumarian Press 2012), <cite>Sandrine Revet</cite> *p. 209-214* https://doi.org/10.4000/cal.3225 <https://journals.openedition.org/cal//3225>

+ [Actu] **[Cyclone Freddy : le Malawi en première ligne face au dérèglement climatique ?](https://www.radiofrance.fr/franceculture/podcasts/la-question-du-jour/la-question-du-jour-emission-du-lundi-20-mars-2023-6202880)** (mars 2023)

> « Le Malawi a été touché pendant six jours par le passage du cyclone Freddy, le plus violent de son histoire. Les dégâts matériels et humains sont considérables et les autorités appellent à l'aide internationale. Le réchauffement climatique est-il la cause de ce phénomène dévastateur ? »

+ [Livre (merci Clailou)] **Guide décolonisé et pluriversel de formation à la recherche en sciences sociales et humaines**
sous la direction de <cite>Florence Piron</cite> et <cite>Élisabeth Arsenault</cite>, livre sous licence libre CC - BY - SA et accès ouvert <https://scienceetbiencommun.pressbooks.pub/projetthese/front-matter/introduction/>

+ [Livre] **Anthropologie de l'aide humanitaire et du développement : des pratiques aux savoirs, des savoirs aux pratiques** <cite>Laëtitia Atlani-Duault</cite>, <cite>Laurent Vidal</cite> *et alii*. Armand Colin, 2009. Voir aussi <https://www.cairn.info/revue-critique-internationale-2010-1-page-181.htm>

+ [Publi] **Les experts du Groupe intergouvernemental sur l’évolution du climat (Giec) dévoilent, lundi 20 mars, une synthèse qui boucle huit ans de travail et qui fait consensus dans la communauté scientifique.**
  + Voir aussi "IPCC WGI Interactive Atlas" <https://interactive-atlas.ipcc.ch/>

> « Climat : nous assistons à un changement climatique sans précédent depuis 125 000 ans, selon le Giec […] Ce monde plus chaud entraîne déjà davantage d'incendies de forêts, de canicules, de tempêtes, de baisses des rendements agricoles et pour limiter les risques de dégâts irréversibles, il faudrait rester en dessous de 1,5°C ou 2°C de réchauffement, d’ici la fin du siècle, mais ce n’est pas la trajectoire sur laquelle nous sommes. Aujourd’hui nous allons plutôt vers un réchauffement qui pourrait être de 2,7°C d’ici 2100. »

+ [Question (merci [Pixelous](https://pixouls.xyz/))] En 2004, quelqu'un a mis le feu à une vieille voiture près d'une **mine de charbon à Olyphant**, en Pennsylvanie, USA. Cela a déclenché un incendie connu sous le nom de **Dolph Mine Fire**, qui a brûlé pendant plus de 18 ans et s'est étendu sur 6 hectares / 15 acres. Au lieu de lutter contre l'incendie, il a été choisi en 2007 de construire une tranchée de 200 pieds et de sceller dans l'argile, en espérant qu'il s'éteindrait de lui-même malgré la contamination de l'environnement. 

Des mesures ont finalement été prises pour l'éteindre en 2020 afin de commencer à le combattre. Il est censé avoir été éteint en janvier 2023, cependant je n'ai trouvé aucune trace de cet événement.
Un « incident » fait écho à celui de la mine de charbon souterraine de Centralia, Pennsylvanie, qui dure depuis 1981 à partir d'un feu de poubelles (cf [Des milliers de gisements de charbon en feu de par le monde](https://web.archive.org/web/20220924030434/https://www.nouvelobs.com/rue89/rue89-monde/20080531.RUE4422/des-milliers-de-gisements-de-charbon-en-feu-de-par-le-monde.html))

<https://www.alleghenyfront.org/coal-powered-the-industrial-revolution-it-left-behind-an-absolutely-massive-environmental-catastrophe/>

<https://undergroundminers.com/olyphant-dolph-mine-fire/>

<https://www.wnep.com/article/news/local/lackawanna-county/olyphant-mine-fire-dep/523-0a888379-3b03-4bc9-bdc2-1ddc884d550c>

+ [Académique]
  + **La cause des catastrophes. Concurrences scientifiques et actions politiques dans un monde transnational** <cite>Lydie Cabane</cite>, <cite>Sandrine Revet</cite> <https://www.cairn.info/revue-politix-2015-3-page-47.htm>
  + **La production sociale d’un service public en des temps difficiles : secourir, dispenser des soins médicaux d’urgence et pratiquer des expertises médico-légales (Bogotá, Medellín et Cali, 1980 - début des années 2000)** Thèse par <cite>Luis Miguel Camargo Gomez</cite> <https://www.theses.fr/2021EHES0051>
  + **Haïti, un État en catastrophe : la gestion transnationale du séisme du 12 janvier 2010**, thèse par <cite>Clément Paule</cite> <https://www.theses.fr/2019PA01D032>



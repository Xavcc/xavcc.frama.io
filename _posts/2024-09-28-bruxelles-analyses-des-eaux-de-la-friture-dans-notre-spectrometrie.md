---
title: "Bruxelles: analyses des eaux, de la friture dans notre spectrométrie"
layout: post
date: 2024-09-28 20:30
image: /assets/images/spectro-bruxelles-septembre-2024.jpeg
headerImage: true
tag:
- hsociety
- Biohacking
category: blog
author: XavierCoadic
description: Faire des petits bouts de trucs et les assembler ensemble. Les mains dans le cambouis et les pipis cacas.
---

En ce mois de septembre 2024 j’organisais 3 jours d'atelier « [Apprendre en s’entraînant](https://forum.hack2o.eu/t/learning-by-training-3-day-workshop-in-brussels-in-september-2024/50#version-fran%C3%A7aise) » portant sur l’investigation de problèmes de l'eau, à Bruxelles, dans le cadre du projet [Hack₂O](https://hack2o.eu/).

**Grand Merci aux participant⋅e⋅s <3 !**

Le mojo premier est « la sécurité avant tout ! », un truc acquis en commun avec [Exposing The Invisible](https://kit.exposingtheinvisible.org/fr/). Une demi journée sur ce sujet, puis sur les bases de l'enquête en générale, et 2 jours de « terrains et analyses », avec démo de cas d'usages de *software defined radio* (cf ce [cas](https://forum.hack2o.eu/t/using-a-software-defined-radio-solution-in-the-context-of-investigating-water-soil-issues/131)). Le second mojo est « l'investigation c'est collaboratif ! ».

Hack₂O est projet porté par l'association belge à but non lucratif [petites singularités](https://ps.zoethical.org/). Le forum discourse que nous administrons est ouvert à toutes personnes voulant traiter des enjeux politiques de eaux (qualité, quantité, droits humains, changements climatiques, mines et extractions, anthropologie / ethnographie, ect). Certains groupes thématiques sont privés pour de considérations d'intimités et de confort de travail. Je serais ravi de vou sy accueillir.

À Bruxelles, nous avons prélevé des eaux de la Senne, du canal, des égouts, flaque de boues de chantier et eaux de pluie et du robinet; et des microplastiques aussi. Ah, et oui un truc *à notre sauce pour du fun* de filtrat de boue de chantier avec de la bière belge.

Le mojo premier portant sur *sécurité* avec un travail collectif n'a pas empêché dans notre cas présent un incident nécessitant une consultation médicale.

[BruWater](https://geodata.environnement.brussels/client/bruwater/index) portail de visualisation des données qualitatives et quantitatives des eaux souterraines et de surface de la Région de Bruxelles-Capital et [Smart Water Bruxelles](https://www.smartwater.brussels/) pourrait vous intéresser vous lecteurices et lectaires.

![](/assets/images/Capture%20d’écran_2024-09-28_22-37-03.jpg)


Nous avons travaillé ensuite avec 3 dispositifs techniques sous licences libres, y compris open hardware, d'analyses :
+ Le microscope [MatchBox](https://github.com/Matchboxscope/Matchboxscope) pour les microplastiques
+ Le test d'[écotoxicité avec des graines](https://forum.hack2o.eu/t/training-on-bioassay-test-with-seed-to-expose-toxicity-from-water-sample/24)
+ Le spectromètre à fibre optique avec impression 3D.

Tout n'a pas fonctionné comme espéré. Bien que nous étions dans un atelier « d'apprentissage en faisant », sans but de produire de données solides pour *exposer* des faits *invisible*, cela est très frustrant. L'un des gros points de *frictions* est le spectromètre et les logiciels de spectrographie associés, et ceux d'analyses de données.

![](/assets/images/draft-spectro.jpeg "Photo of a work surface on which the pre-assembly of the spectrometer's Cuvette Holder is placed, an LED shines in front of it, the optical fibre is connected to the Cuvette Holder and an empty cuvette is placed in front of a Becher containing distilled water.")

Le [spectromètre à fibre optique avec impression 3D](https://gaudishop.ch/index.php/product/3d-printed-fiber-spectrometer-kit/), du GaudiLab, permet d'opérer des analyses dans le spectre de la lumière visible (UV) et dans le spectre infrarouge. Le mettre en œuvre a déjà épique en soi − *ahaha débouche des trucs et aligne des millimètres*. C'est documenté dans un page wiki du forum Hack₂O [ici](https://forum.hack2o.eu/t/diy-spectrometry-on-a-tap-water-sample/46).

Comme nous étions en entraînement, j'avais choisi de rester sur dans spectrométrie dans le spectre visible, notamment pour limiter ne nombre de manipulations et de calibrages dans le temps impartis et avec considérations du nombres d'échantillons à passer.

Une fois le spectromètre opérationnel et étalonné, nous avons besoin de logiciels pour afficher les données de sorties, les exporter en qualité convenable et format libre standard. Et là, c'est la panne par [merde de fosse sceptique à déboucher](https://write.tedomum.net/argeveller/raconte-moi-une-panne) !

Le premier sur la site est celui qui a été conçu et maintenu par l'ONG nord américaine PublicLab (*dans laquelle j'ai contribué). L'ONG n'existe plus, le logiciel n'est plus maintenu, ce qui reste est très bogué (voir [ici](http://forum.hack2o.eu/t/diy-spectrometry-on-a-tap-water-sample/46/7) notre documentation).
Une autre option fut d'utiliser le [logiciel de spectrographie](https://gaudi.ch/Spectrometer/) de Gaudi Lab (le code source n'a pas de licence *ahem*). Que ce soit online ou offline, les calibrages se font de manières acceptable. Cependant, l'export des données c'est kaput car la code JS [ici](https://developer.mozilla.org/en-US/docs/Web/API/Window/showSaveFilePicker#browser_compatibility) utilise une fonctionnalité « [expérimentale](https://developer.mozilla.org/en-US/docs/Web/API/Window/showSaveFilePicker#browser_compatibility) » non supportée par presque tout les navigateur web.

![](/assets/images/spectro-bruxelles-septembre-2024.jpeg "A screenshot of the spectrograph's graphical interface. On a black background, a graph with X and Y axes in which the measured data of luminosity intensity and visible wavelengths are rendered in a curve drawn in white displaying several peaks").

Un peu énervé par tout cela j'ai basculé vers la technofutilité (*et ça fait du bien*) en faisant une spectro de friture issue da la cuisine faite au [Caldarium](https://caldarium.be/fr:start). Just for Fun ! Ainsi, j'ai un gros paquet de données exportées à l'arrache dont je dois vérifier la qualité et concordance avec d'autres signatures spectro connues, des autres gros paquets de données à la pertinence incertaine.
Il y a aussi des personnes participantes à Bruxelles qui sont reparties avec des doutes concernant ce moyen technique dans une optique opérationnelle.

La question de fork ou développement d'un logiciel qui fonctionne et qui est adapté aux besoins d'enquêtes de terrain (sécurité, offline/online, analyses in situ, transmission des données par radio) est sur la table avec quelques options de financements probable pour ce travail. Contactez moi si cela vous intéresse.

Pour le prochain petit bout de faire avec les pieds, ce sera à Berlin à [Publix](https://www.publix.de/en) dans le cadre de [Digital Investigation Residency  & Critical Climates](https://exposingtheinvisible.org/en/news/call-investigation-institute-2024) avec d'autres copaines. Si ça vous dit, venez en discuter sur notre forum [ici](https://forum.hack2o.eu/t/investigation-residency-in-berlin-critical-climates/160).
Et les des deux mojos fondamentaux seront remis dans le cœur de l'ouvrage de nos travaux.

![](/assets/images/spectro-bxl-by-sun.cleaned.jpg)
<figcaption class="caption">Image by Sun</figcaption>
<br />





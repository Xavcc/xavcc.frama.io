---
title: "JDLN & FDLN : Des bibliothèques universitaires et des libertés numériques"
layout: post
date: 2024-03-13 15:40
image: /assets/images/WE_MAKE_BIOPORN.png
headerImage: true
tag:
- hsociety
category: blog
author: XavierCoadic
description: Une petite histoire de continuités et des membres d’une même culture
---

Qui se souvient de l'année 2018 ? Oui, celle de la première édition du Festival des Libertés Numériques coordonné par les bibliothécaires de l’INSA<marginnote>INSA: Institut National des Sciences Appliquées</marginnote> Rennes avec son organisation voulue décentralisée ([archive](https://web.archive.org/web/20211016172744/https://fdln2018.insa-rennes.fr/le-festival/)).

Pour ma part, j'ai prévu d'y faire un « Atelier et table ronde “Biopanique, cuisine et féminisme `|` [Bionomymous](/tags/#biononymous) DIY” » que j'avais reporté le cœur brisé ([archive](https://web.archive.org/web/20210124193847/https://fdln2018.insa-rennes.fr/agir/biopanique-cuisine-et-feminisme-bionomymous-diy/)).

Puis 2019, « les tentacules du Festival des Libertés Numériques (FDLN) continuent de se déployer un peu partout dans le grand Ouest (Rennes, Nantes, Brest, Lannion, etc.) avec une cinquantaine d’événements programmés.» ([archive](https://web.archive.org/web/20231219225352/http://fdln2019.insa-rennes.fr/le-festival/)). J'y avais pris enfin mes aises avec [Rencontre atelier sur la reconnaissance faciale et morphologique de masse.](/fdln-reconnaissance-faciale/), [Autodéfense bionumérique. Collecter, effacer ou brouiller de l'ADN. Et un peu plus...](/fdln-bioprivacy/), et [une anticonférence gesticulée](fdln-anticonference-gesticulee/). Dans des bars, en bibliothèque, là où nous étions accepté⋅es et considéré⋅es.

La 3ème édition en 2020 était « une invitation à se retrouver tous·tes ensemble autour d’un feu de camp, à la tombée de la nuit. Parce que, cette année plus que jamais, il est urgent de nous réunir, de réfléchir, de créer, de partager » ([archive](https://web.archive.org/web/20230928133043/https://fdln.insa-rennes.fr/le-festival/)). Je n'y avais rien proposé pour le programme des activités, j'avais participé à un excellent [Forum Ouvert](https://fr.wikipedia.org/wiki/M%C3%A9thodologie_Forum_Ouvert) organisé par les tout aussi excellent⋅es La [Dérivation](https://dérivation.fr/) & Alexis.
Ce fut la dernière. Le « [Clap de fin](https://web.archive.org/web/20230928153548/https://fdln.insa-rennes.fr/actualites/clap-de-fin/) » fut annoncé en juin 2020 avec un espoir : « que les lucioles continueront à briller dans la forêt et que les temps à venir verront apparaître une multitude de nouveaux feux de camp. ».

![](/assets/images/WE_MAKE_BIOPORN.png "Image d'un stickers au fond blanc et texte noir: 'We Make BioPorn'")

Motivé⋅es depuis des discussions et des rencontres avec des « membres d’une même culture » (Harold Garfinkel, 1967, 2007, p.64)[^note-garfinkel], ce magnifique parcours avait laissé des traces et des fondations communes.

[^note-garfinkel]: Garfinkel, H. (1967, 2007). Recherches en ethnométhodologie. Presses Universitaires de France, 2020. https://doi.org/10.3917/puf.garfi.2020.01

Alors en 2021, un surgeon poussa dans une foret non loi de là. Un *truc* toujours à l'Ouest ! « Les Journées des libertés numériques : comprendre les enjeux des technologies et cultures numériques avec les Bibliothèques Universitaires (BU) de Nantes » ([archive](https://web.archive.org/web/20240313160344/https://bu.univ-nantes.fr/jdln/jdln-edition-2021)). Les JDLN et de nouvelles lucioles.

> Pour la première édition, 8 animations ont réuni plus de 150 participants, dans les BU de Nantes et de la Roche-sur-Yon. Des spécialistes, des chercheurs, des artistes, des militants... ont proposé une diversité d’activités

J'ai attendu 2022 pour retrouver un peu de forces et d'y proposer une conférence suivie d'un atelier « [Le biohacking, le biopunk, le Do it yourself : petites histoires de libertés dans la cité](https://videos.lescommuns.org/w/jpHcvAV5AA6GaWeLs6LGCb) ». Oh que j'aime les bibliothèques ! (*aussi celle de Choisy-Le-Roi ([a](/2023-biohacking-biopunk-diybio/), ([b](pas-sage-en-seine-2019/), ([c](https://video.passageenseine.fr/w/vnBFxY9Aq5HNDKGpJVs56X)*))

Pour 2023 le chiffres donnés des JDLN étaient :

> Quelques chiffres de la 3ème édition de mars à avril 2023 :
> + 28 animations 
> + 1114 participants 
> + 20 lieux différents 

J'avais suivi à distance et aussi en *replay* bien plus tard dans le temps.

En cette année 2024, j'ai eu la très agréable surprise de recevoir une invitation. Le 28 mars plus exactement, pour un « Plateau radio : culture numérique avec Radio PRUN'». Une émission enregistrée en public, [30 places faut réserver](https://bu.univ-nantes.fr/jdln/jdln-plateau-radio-culture-numerique-avec-radio-prun-2).

> Les ateliers animés par Julia Gley, de janvier à mars 2024, ont été l’occasion pour les participant·e·s de se former à l’expression journalistique et radiophonique par la pratique, en réalisant des reportages sur le terrain.

Cette émission est la restitution avec des invité·es en plateau. C'est à l'UFR<marginnote>UFR:  Unités de formation et de recherche</marginnote> de Médecine : Salle des assemblées (1er étage).

**Chapeaux bas les bibliothèques universitaires et les bibliothécaires !** <marginnote>Appellées aussi BU</marginnote>

----

Anecdote personnelle supplémentaire liée aux radios et Nantes :

Mi Janvier 2024, une belle 'tite histoire qui mène vers de la puissante Histoire. Poésie et Palestine depuis 1948 à aujourd'hui.

Je passais me poser par Nantes , à portée d'antenne Hertzienne de Plum' radio (que je recommandais ensuite très chaudement).

Lors d'un cercle d'hacktivistes (feu de camps) le choix fut fait d'écouter « [Transpoésie 11 : Poésie de Palestine](https://podcloud.fr/podcast/transpoesie/episode/transpoesie-11-poesie-de-palestine) », écouter sans dire un mot et dans un *silence* de champ perdu dans l'hiver, privilégié⋅es dans la boue.

Luttes, résistances, vengeances, reprises sur un monde de violences, la joie comme moyen de se battre et de ridiculiser les bombes, et bien plus…

**SUBLIME <3 !**

Ce que la poésie et les poètes tentent d'opposer à la mort et à la condamnation dans l'oubli et la déshumanisation.

Mention spéciale: Subjective Atlas of Palestine ISBN 978-906-450-648-2

> Transpoésie met en avant les croisements entre les styles, les frottements entre les cultures, le chevauchement entre les langues qui transcendent le champ poétique.

Pas de podcast chez Prun'. Alors je leur ai écrit. Réponse utlra rapide de [Nina Hellboid](https://ninahelleboid.com/creation-sonore/), qui est l'autrice de cette série − Merci <3, qui me revoie vers JetFM qui passe un lien vers un pod de la [Fediverse](https://fr.wikipedia.org/wiki/Fediverse) <3.

## Référence
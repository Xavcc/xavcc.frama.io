---
title: "Questionnements linguistiques, IA, pillages, violences et appauvrissements"
layout: post
date: 2023-06-29 05:20
image: /assets/images/necklaces-aztec.jpg
headerImage: true
tag:
- hsociety
category: blog
author: XavierCoadic
description: Lorsque les conquistadors volaient l’or aztèque
---


*Traduction d’un texte posté sur mastodon par **Michał "rysiek" Woźniak**, (CC By Sa 4.0 Licensed), auteur du blog [Songs of Security of Networks](https://rys.io)*


## Les questions linguistiques

Avec le « contenu généré par l’IA » omniprésent, peut-être que plus de gens comprendront pourquoi je me suis opposé et continue de m’opposer à l’utilisation de l’expression « contenu généré par l’utilisateurice » pour  pour tout ce qui a trait à ce sujet, où que ce soit, toujours.

+ Il s’agit d’**individu**, pas d’« utilisateuricess ».
+ Ce sont des contenus **créés**, pas des contenus « générés ».

Et souvent, c’est tout aussi créatif que n’importe quel « art véritable ».

Bien sûr, les barons de la technologie veulent bien plus faire croire qu’il s’agit d’une substance grise non-déterminée, d’un « contenu » à extraire et à filtrer et avec lequel il faut « s’engager »

Je ne « génère pas de contenu ». Je débats, je partage mes pensées, je fais du shitpost, je fais preuve d’empathie et j’apporte un accompagnement, et ainsi de suite.

Me considérer comme un⋅e « utilisateurice » qui « génère du contenu », alors que je communique, partage et fais preuve d’empathie avec mes semblables, est réducteur et − j’ose le dire − déshumanisant.

Cela enlève l’humanité, cela enlève la volonté humaine de communiquer, ne laissant que des artefacts vides derrière.

Des artefacts qui peuvent ensuite être pris et fondus en *données* (data).

## Le vol de l’or et la destruction d’artefacts

Lorsque les conquistadors volaient l’or aztèque, qui se présentait principalement sous la forme d’artefacts culturels ou d’objets religieux, ils le fondaient souvent en simples lingots d’or, mêmes grossiers, pour en faciliter le transport :
[Spanish Conquistadors Stole This Gold Bar From Aztec Emperor Moctezuma’s Trove](https://www.smithsonianmag.com/smart-news/gold-bar-once-belonged-aztec-emperor-moctezuma-180973959/
), *Forces led by Hernán Cortés dropped the looted treasure during a hasty retreat from the Aztec capital of Tenochtitlan in June 1520*, <cite>Theresa Machemer</cite>, 2020.

Après tout, ils ne s’intéressaient pas à la signification culturelle et religieuse de ces objets. Ils voulaient simplement l’or, qu’ils pouvaient facilement vendre à d’autres.

En le réduisant à l’état de matière première, ils ont détruit le contexte culturel et historique. Nous en sommes toustes appauvri⋅e⋅s.

Je pense qu’il est juste de dire qu’une telle destruction d’artefacts culturels, une telle réduction d’objets qui avaient un sens et une signification pour les individus, est un type de violence en soi, indépendamment de la manière dont ces objets ont été acquis.

Mais bien sûr, on n’a jamais demandé le consentement de celleux qui ont créé ces œuvres d’art et ces objets religieux, ni de celleux pour qui ils avaient une signification - peut-être s’agissait-il d’héritages familiaux ?

Les conquistadors ont tout pris parce que personne ne pouvait les arrêter.

Lorsque les techniciens *[techbros](https://fr.wikipedia.org/wiki/Culture_bro)* de l’AI entraînent leur  Large Language Model (LLM) sur les œuvres des individus, ces œuvres sont également retirées de leur contexte individuel, « fondues » en une matière première − les données d’entraînement. Ce ne sont plus que des mots, des phrases et des expressions désincarnées que le *modèle* pourra répéter plus tard.

Le contexte historique, culturel et social de ces œuvres est détruit, fondu.

Et cela se fait également sans consentement.
Et cela se fait à une échelle gigantesque.
Et cela se produit aussi parce que les techniciens *[techbros](https://fr.wikipedia.org/wiki/Culture_bro)* peuvent vendre le produit extrait.

Rédaction et écritures…

Et c’est aussi le cas parce que personne ne peut les empêcher de parcourir le web ouvert et de s’emparer de tous ces artefacts culturels pour en faire leur matière première d’entraînement.

Après tout, « tout était là, au grand jour ; en le publiant en ligne, les auteuricess ont demandé à ce que soit ramassé ».

Et c’est aussi, j’ose le dire, une forme de violence.

Et nous nous en trouverons également appauvri⋅e⋅s.

![](/assets/images/necklaces-aztec.jpg)
<figcaption class="caption"> Necklace Ornaments, Frogs. Aztec or Mixtec. 15th–early 16th century. Source: MET Museum</figcaption>

<div class="breaker"></div>
---
title: "Offdem Ozone (Bruxelles), Universités d'hiver Interhack (Montpellier) : Février 2023 sera chaud !"
layout: post
date: 2022-12-16 07:40
image: /assets/images/seeds-bioassay-r2s-rennes-2022.jpg
headerImage: true
tag:
- hsociety
- Tiers-Lieux
category: blog
author: XavierCoadic
description: "Des événements qui font battre mon cœur"
---

Oh ! Que vous qui lisez ces lignes si vous saviez combien je me réjouis de vous rencontrer ! Ou de vous revoir `{>‿o}/*`

Tout d'abord il y aura “[Ozone ►► Fast Forward: Decolonizing Europe Matters](https://ps.zoethical.org/pub/offdem-ozone-cfp), OFFDEM O<sub>3</sub> février 2023, Bruxelles (l'appel à propositions est en français et en anglais). Il y a des notes de l'OFFDEM O<sub>2</sub>, Oxygène, en français et en anglais, de quelques activités [ici](https://pad.public.cat/offdem-o2-index#). Si vous voulez donner un coup de main d'orga pour 2023, même à distance, passez vos mains et claviers par [ici](https://ps.zoethical.org/t/volunteering-at-offdem/6251)

Puis ce sera les [Universités d'hiver de l'Interhack au BIB](https://pretalx.lebib.org/universit-d-hiver-2022), à Montpellier, du jeudi 23 février au dimanche 26 février 2023. (Appel à proposition en français). Il y a déjà quelques activités au menu [ici](https://pretalx.lebib.org/universit-d-hiver-2022/featured/).

Alors, si vous avez envie vous aussi et que vous le pouvez, on peut s'y croiser. J'y viendrais avec quelques trucs dans mes poches :

+ Biologie de garage, Biopunk, Biohacking
+ Enquête environnementale
+ [Exposing The Invisible](https://exposingtheinvisible.org/), [DataDetox Kit](https://www.datadetoxkit.org), [GenderSec](https://gendersec.tacticaltech.org/wiki/index.php/Main_Page), [Security in a Box](https://securityinabox.org/en/)
+ Des lieux de sociabilités en situation critique :
  + Qui font de l'enquête environnementale
  + Des communautés LGBTQIA+
  + De l'hospitalité, de l'accueil, des réfugié⋅e⋅s (hey, c'est aussi le collectif Zone Neutre, dans l'orga OFFDEM, qui rassemble les citoyen·ne·s sans papiers à Bruxelles)
+ Catastrophes et désastres comme objet politique et des lieux de sociabilités en front
+ L'oisiveté et la flânerie comme tactique politique (hahaha, si, si, si, j'y travaille actuellement à en écrire quelques lignes)

Évidemment je viens aussi pour juste chiller et discuter au gré des surprises et des balades. On peut aussi commencer à en discuter avant.

Au plaisir d'interagir !


![](/assets/images/seeds-bioassay-r2s-rennes-2022.jpg)
<figcaption class="caption">Photographie d'un moment au biohakerspace la Kaouenn Noz, Rennes, octobre 2022, pendant le chantier Reprises de savoirs, Louzaouiñ Graines de luttes 2022. Mise en pratique de bioassay sur prélèvements d'eau avec des graines</figcaption>

<div class="breaker"></div>
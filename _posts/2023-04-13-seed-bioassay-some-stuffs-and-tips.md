---
title: "Seed Bioassay: Some stuffs and Tips (Tears, water droplet and Tiny Hacks)"
layout: post
date: 2023-04-13 06:40
image: "/assets/images/a-thaliana-atlas.png"
headerImage: true
tag:
- hsociety
- bioassay
category: blog
author: XavierCoadic
description: 
---

Here I'm talking and writing about "water, polutants, investigations, research, Hacktivism". Give a shoot at [hack2O](https://ps.zoethical.org/t/hack2o-intro-explication/6116) and please contribute on [Investigating Water with Seeds](https://publiclab.org/notes/xavcc/12-17-2021/investihating-water-with-seeds) (<cite>PublicLab</cite>), and [Seeds of Contend / Bioassay in your own dirt/PTsD](https://www.hackteria.org/wiki/Seeds_of_Contend_/_Bioassay_in_your_own_dirt/PTsD) (<cite>Hackteria</cite>).

Maybe like me you work outside the walls…

> "In short, I am currently working with partners to develop a device that can be used wherever and whenever needed, using existing resources and materials, with the aim of producing results of a comparable quality to those obtained in industries and laboratories." [Call for donations to fund and develop a libre and open-source seed-based environmental survey test](/call-for-fund-for-free-libre-environmental-seed-based-bioassay-test/)

![](/assets/images/seed-hack2o.jpg)
<br />

For a modern and effective bioassay system to be established, there are several prerequisites that must be met:

+ The system’s parameters / endpoints must be very sensitive to the toxic substance being analyzed/targeted.
+ The response time must be “fast”.
+ The biological test must be able to be used for both acute (short-term) and long-term measurements.
+ The device must be easy to use and not require extensive personnel training.
+ The cost should not be excessive, especially when intended to be used in developing countries.
+ The biological material should be easily accessible and the operating costs should be low.

**Like seeds?**

Take 16 seeds (even the ones in the plant growing on the sidewalk across the street)

Take 4 empty glasses (I said empty not to be emptied of who knows what 😉)

Put 4 seeds in each empty (clean) glass

Add :
+ Glass A: 2 cl of distilled water
+ Glass B : 1,5 cl distilled water and 0,5 cl tap water
+ Glass C: 1 cl distilled water and 1 cl tap water
+ Glass D: 2 cl tap water 

You cover, you put in the cupboard sheltered from the light.

You come to observe, and photos if you want, 3 or 4 times a day during 5 to 7 days

and tadam
you play the game of the 7 differences between the roots (or root absence) of the seeds of the 4 glasses

(Works also with water of basin, rain water, tap water, gutter, building sites, river, pond, tears of bourgeois, etc)

Simple the recipe ?

Pssttt: it's already a form of bioassay of qualitative water study 😱 

**It's towards as simple as I tend in the works and efforts, so you can do it with a "robust" enough test so that in addition you can use the results in the debate in the public place.**

![](/assets/images/a-thaliana-atlas.png)
<br />

## Search, find, shout out and share writing resources

You will probably need to find out some writings and/or books / publications for free.

Sometimes, Yeah sometimes the need to refer to "must-read" scientific publications is like hunting for rare mushrooms (that may sound cool and bucolic).

Except that some assholes have privatized the forest, put up barbed wire and a paywall, and other assholes are having food orgies in the middle of the forest clearings.

Fortunately there are green community plains, fields and libre forest like sci-hub and z-lib, Internet Archive. And of course great and lovely little hands that garden every day and are so welcoming <3

Anna's Archives: 
+ Underground library search engine: books, newspapers, comics, magazines. ⭐️ Z-Library, Genesis Library, Sci-Hub. ⚙️ Fully resilient with open source code and data. ❤️ Spread the word: everyone is welcome here! <https://en.annas-archive.org>
  + example for specific resarch about a book on Plant Physiliogy <https://en.annas-archive.org/md5/8f654db7e0325da94c32cd93641ef02d>

On other hand you can directly reach z-library (with Tor), whose address is: `zlibrary24tuxziyiyfr7zd46ytefdqbqd2axkmxm4o5374ptpc52fad.onion` (account creation and credentials needed)

If you have troubles and concerns, I can help you and offer you books and/or pdf's (see my [Inventaire](If you have concerns, https://inventaire.io/users/xavcc/inventory).

![](/assets/images/expe-and-risk.jpeg)
<figcaption class="caption">Illustration from: <a href="https://errantscience.com/about-us/cc-by-nc/">Errant Science and Errant Science Clutter and webcomic content</a> is CC-BY-NC</figcaption>
<br />

### What's behind the glass room?

It is not easy to do "research" outside/on the periphery of *walls/borders* and this is not without consequences., including for your safety and that of your family, friends and colleagues.

I also invite you to read:
+ **Safety First!** <https://kit.exposingtheinvisible.org/en/safety.html>

Especially the part about "the risk is inherited". <https://kit.exposingtheinvisible.org/en/safety.html#field-safety>

Also, look for and/or share evidence (even contextual) to prove useful as a risk manager
And also :
+ **Investigation is Collaboration: How to Make It Work** <https://kit.exposingtheinvisible.org/en/collaboration.html>

+ **Collecting Visual Evidence** <https://kit.exposingtheinvisible.org/fen/visual-evidence.html>

+ **Bio-investigations in the field** <https://kit.exposingtheinvisible.org/en/bio-investigation.html>

And after all…

> "**Water Risk Atlas of the World Resources Institute**"
>
> water stress, drought risk and riverine flood risk using a peer-reviewed methodology.
>
> The group evaluated the water stress levels of 189 countries and the regions within them. The top 17 high risk countries are Qatar, Israel, Lebanon, Iran, Jordan, Libya, Kuwait, Saudi Arabia, Eritrea, the UAE, San Marino, Bahrain, India, Pakistan, Turkmenistan, Oman and Botswana.
>
> You should also check from Major & Minor basin perspective<marginnote>Hydra bioassay for the evaluation of chlordecone toxicity at environmental concentrations, alone or in complex mixtures with dechlorinated by products: experimental observations and modeling by experimental design <a href="https://dx.doi.org/10.1007/s11356-022-22050-8">paper</a></marginnote>
>
> <https://www.wri.org/applications/aqueduct/water-risk-atlas/#/?advanced=false&basemap=hydro&indicator=w_awr_def_tot_cat&lat=-10.487811882056683&lng=-174.0234375&mapMode=view&month=1&opacity=0.5&ponderation=DEF&predefined=false&projection=absolute&scenario=optimistic&scope=baseline&timeScale=annual&year=baseline&zoom=2>
>
> #hack2O #hack2eaux

## A Must Read

+ **Whole-Plant and Seed Bioassays for Resistance Confirmation**, <cite>Nilda R. Burgos</cite>, 2015, <https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=wholeplant_and_seed_bioassays_for_resistance_confirmation.pdf>

## Now? Let's work with "liberated"  and free resources

+ **Bioassays Advanced Methods and Applications** Edited by <cite>Donat-P. Häder</cite> (Friedrich-Alexander University, <cite>Gilmar S. Erzinger</cite>, University of Joinville Regio, UNIVILLE, Joinville, Brazil

![](/assets/images/calis-water-plouts-1.png)
<figcaption class="caption">Figure 2.1 Classification of water pollutants as dissolved and undissolved substances.</figcaption>
<br />

![](/assets/images/2-bioassais-2.png)
<figcaption class="caption">Classification of water pollutants as organic and inorganic substances</figcaption>
<br />

### Relationships among the Toxicology Protocols

_from (Assessing Toxic Risk, Teacher Edition (<cite>Nancy M. Trautmann</cite>))_

**Chemical Dilutions**

Protocol 1. Serial Dilutions

⬇️

*Protocols 2–4 use serial dilutions made in Protocol 1.*

⬇️ 

Dose/Response Bioassays
+ Protocol 2. Dose/Response Experiments Using Lettuce Seeds
+ Protocol 3. Dose/Response Experiments Using Duckweed
+ Protocol 4. Dose/Response Experiments Using Daphnia

⬇️ 

*Protocol 5 uses techniques learned in Protocols 2, 3, or 4 to test environmental samples*

⬇️ ⬆️ 

Bioassays on Environmental Samples Protocol 5. Testing Environmental Samples Using Bioassays

⬇️ ⬆️

*Protocols 6 and 7 provide techniques for figuring out what types of chemicals are causing the toxicity of a solution or sample.*

⬇️ ⬆️

Water Purification and Chemical Classification of Toxicants Protocol 6. Preparing Water Treatment Columns Protocol 7. Using Ion Exchange and Activated Charcoal to Purify and Analyze Chemical Solutions.

![](/assets/images/assessing-risks.png)
<br />

**Choosing Bioassay Organism** _(Assessing Toxic Risk, Teacher Edition (Nancy M. Trautmann))_

Many different types of organisms are used for environmental bioassays, ranging from bacteria to rainbow trout. They include instructions for use of lettuce seeds, duckweed, and Daphnia because these organisms are commonly available, appropriate for classroom use, and widely used by scientists.

+ **Lettuce seeds** are inexpensive, easy to store, and don’t require maintenance between experiments. Lettuce seeds are used in bioassays of water and other environmental samples because of their sensitivity to environmental contaminants such as heavy metals and some types of pesticides, solvents, and other organic compounds.

+ **Duckweed** is a small aquatic plant commonly found floating on the surface of ponds, wetlands, and nutrient-rich lakes. It is a useful bioassay organism because of its small size, rapid growth, and sensitivity to chemicals including herbicides and metals. Because duckweed floats, it is a good organism to use for bioassays involving oil or other chemicals that float or concentrate at the water surface. Scientists in government and industry also use duckweed to test the toxicity of complex chemical mixtures such as landfill drainage or discharges from industries and wastewater treatment plants. Duckweed can easily be gathered in the field and cultured in the laboratory, or you can order it from scientific supply companies. If you decide to collect your own, make ure to select a relatively clean source so you will not get plants that are tolerant to pollution. Before using the duckweed for bioassays, you should culture it for a couple of weeks
in the lab to make sure the plants are healthy and adapted to your culture conditions.

+ **Daphnia**, popularly but inaccurately known as water fleas, are tiny crustaceans that live in fresh water such as ponds, lakes, and wetlands. Daphnia are excellent organisms to use in bioassays because they are sensitive to changes in water chemistry and are simple and inexpensive to raise in an aquarium. They mature in just a few days, so it does not
take long to grow a culture of test organisms. It is best to use small young Daphnia to avoid individuals about to give birth or to die of old age. (Daphnia usually live only one or two months.) Because the appearance of resting eggs indicates a poor culture environment, do not use Daphnia with resting eggs (see Student Edition, Figure 3.5, p. 87). To obtain a good supply of juvenile Daphnia, begin 24 hours in advance by removing females bearing embryos from the stock culture and placing them in 40 mL beakers containing 300 mL of spring or stream water and the appropriate amount of food. Once the young are released from the brood chamber, use these juveniles for the bioassay. Some biological supply companies simplify this process
by offering cultures made up of juvenile Daphnia ready for use.

<br />
<figcaption class="capation">Meanwhile…</figcaption>
<br />

After reading some resources from College of Agriculture and Life Sciences / Cornell University and watching some videos:
+ [Bean Root BioAssay Demonstration](https://yewtu.be/watch?v=IOvLbfb-vb0)
+ [Cornell Soil Health Laboratory](https://soilhealthlab.cals.cornell.edu/)
+ [Assessing fungal and nematode root pathogens with visual on-farm soil bioassays](https://www.vegetables.cornell.edu/pest-management/disease-factsheets/cropping-sequences-and-root-health/assessing-fungal-and-nematode-root-pathogens-with-visual-on-farm-soil-bioassays/)

Well, I wrote to Kirsten Kurtz, Manager, Cornell Soil Health Laboratory /Research Support Specialist. Here's her answser:

> "We used the bean seeds because we had access to been seeds that were highly susceptible to the root pathogens we were looking for. (To my knowledge we cannot get those seeds anymore)" <cite>Kirsten Kurtz</cite>.

Well, I can get this point, it's fair. But here are some other arguments for Beans:

+ [Similarity between soybean and Arabidopsis seed methylomes and loss of non-CG methylation does not affect seed development](https://www.pnas.org/doi/10.1073/pnas.1716758114), <cite>Jer-Young Lin</cite>, <cite>Brandon H. Le</cite>, <cite>Min Chen</cite>, <cite>Robert B. Goldberg</cite>. 2017
  + Data vizualsation tool for multiple levels plant data <https://bar.utoronto.ca/eplant/>

This being said, on the angiosperm side, the seed plants, there are about 390,000 known species at present (new ones are discovered every year. about 2,500 to 3,000).

![](/assets/images/1fd738aabf73c6f4.png)

<figcaption class="caption">Source <a href="https://lifemap.univ-lyon1.fr/">Exploring the Entire Tree of Life</a></figcaption>
<br />

What about old archives? Cucumber seed bioassay, Nature n°223, 1969. <https://archive.org/details/dli.calcutta.07254/page/965/mode/2up?q=seed+bioassay>

## Think and make it accurate, accessible, affordable

**Making, prototyping, hacking, and using free licenses are not enough to make resources, objects, or kits truly Libre**.

Oops, ASSURED criteria (affordable, sensitive, specific, user-friendly, rapid, equipment-free, delivered) is 10 years old this year <https://www.nature.com/articles/s41564-018-0295-3.pdf> (and the 3 A's: Accuracy, Accessibility, Affordability)

REASSURED diagnostics by <cite>Kevin J. Land</cite>, 4 years old

My training: 3 years old "[FRUGAL SCIENCE DESIGNING DIAGNOSTICS IN LOW RESOURCE ENVIRONMENTS](https://notecc.kaouenn-noz.fr/lib/exe/fetch.phpmedia=frugal_science_reassured_20200922-kevinland.pdf)"

Pfiouuuu, time flies and I can't write a little note on "how I try to adapt it" on the seed-based bioassay

And again and again, nights and days, I think about the (hypothetical) moments when I would be sitting next to great people, including from other Hack worlds.

## You should be happy to give a chance on those equipments

+ [PocketPCR](https://gaudishop.ch/index.php/product/pocketpcr-kit/) 
+ [3D Printable Fiber Spectrometer Kit](https://gaudishop.ch/index.php/product/3d-printed-fiber-spectrometer-kit/)
+ [Paperfuge](https://www.biorxiv.org/content/biorxiv/early/2016/08/30/072207.full.pdf)
+ [ElectroPen](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.3000589), *An ultra-low–cost, electricity-free, portable electroporator*

and…

![](/assets/images/small-lab.jpg)
<br />

<figcaption class="caption">laying with microfluidic device and Microscope 3dprinting</figcaption>
<div class="breaker"></div>
<ul>
     <li><a href="https://matchboxscope.github.io/docs/Matchboxscope/">Matchboxscope Documentation</a></li>
     <li>Microfluidics: Step-by-step : A microfluidic (PDMS) staircase device for size sorting microparticles down to 25 µm using a 3D-printed mold | Analytical Chemistry | #ChemRxiv | Cambridge Open Engage <a href="https://chemrxiv.org/engage/chemrxiv/article-details/63fb0342937392db3d1126e6">publication</a></li>
</ul>

Huge thanks to <cite>Vittorio Saggiomo</cite>!


If your love *adventures*: "SeedExtractor: An Open-Source GUI for Seed Image Analysis" <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7882627/>

#### At least some references:

+ The National Seed Health System (NSHS) laboratory-based Seed Health Testing methods examine for plant pathogens in seeds. A comprehensive list of approved NSHS Seed Health Testing Methods & Codes are published here: <https://seedhealth.org/seed-health-testing-methods/>
+ Conducting a Bioassay For Herbicide Residues (Tomato) <https://content.ces.ncsu.edu/conducting-a-bioassay-for-herbicide-residues>

+ The large number of organic and inorganic molecules both naturally occurring and industrially produced, numbering in the hundreds of thousands, as well as the large areas and many habitats, preclude large-scale analyses of environmental health and biodiversity. While many of these substances are toxic to microbes, plants, animals, and humans⋅e⋅s through acute or chronic exposure, only a few are monitored in the environment. A recent census lists only 223 organic chemicals that are monitored in freshwater ecosystems on a continental scale at 4,000 European sites (also only) <https://www.pnas.org/doi/pdf/10.1073/pnas.1321082111>

+ E-waste recycling is a relatively new source of toxic substances in water quality issues (which we need so badly)
  + Assessment of toxic metals in waste personal computers
<https://www.sciencedirect.com/science/article/abs/pii/S0956053X14001603> (modulo the Basel Convention on the Control os Transboundary of Hazardous Wastes and their Disposal (1989, then reinforced in 1992) with which so-called industrialised countries blow their nose every morning)
  +  A grand challenge for environmental toxicity: what are the permissive limits of toxic pollutants in the environment? <cite>Donat-P. Häder</cite> <https://core.ac.uk/download/pdf/82843605.pdf>


+ Calamity 
> In 1976 a factory in Seveso, Italy (town and river), was the site of a "disaster".<https://fr.wikipedia.org/wiki/Catastrophe_de_Seveso>
>
> "A massive cloud of TCDD smoke covered the sky for a distance of up to 15 kilometers in an area inhabited by more than 37,000 people
>
> <https://fr.wikipedia.org/wiki/2,3,7,8-T%C3%A9trachlorodibenzo-p-dioxine>
>
> this 2,3,7,8-Tetrachlorodibenzo-p-dioxin (TCDD) remained for a long time a point of "non-detection" because this product was not in the list of those to be monitored, logic and norm and epistemology and administration still and always. (An epidemiological study of Seveso 20 years after the incident showed that elevated 2,3,7,8-TCDD)
>
> An event that links with the north of Japan, Yushō disease, poisoning in 1968, rice bran oil from the Kanemi company was contaminated by PCBs and polychlorinated dibenzofurans (PCDF); then Taiwan in 1979 <https://www.sciencedirect.com/science/article/abs/pii/S0045653515003045>
>
> and also the use of Agent Orange as a weapon of war: Agent Orange is made up of 2,4,5-T and 2,4-D. Their mixture can react and produce highly toxic TCDD.  <https://fr.wikipedia.org/wiki/2,3,7,8-T%C3%A9trachlorodibenzo-p-dioxine>
>
> #Hack2o #hack2eaux

<div class="breaker"></div>
---
title: "« Théorie générale de la Magie », Marcel Mauss, avec les pieds (partie 1)"
layout: post
date: 2022-06-27 17:40
image: /assets/images/peppercarrot.jpg
headerImage: true
tag:
- hsociety
- Notes
- Anthropologie
category: blog
author: XavierCoadic
description: ""
---

<figcaption class="caption">Image d'en tête par "David Revoy (<a href="www.davidrevoy.com">davidrevoy.com</a>) <a href="https://www.peppercarrot.com">dans l'œuvre Pepper & Carrot</a> sous licence Creative Commons Attribution 4.0</figcaption>
<br>

« Esquisse d'une théorie générale de la magie » est un extrait publié en 1902 - 1903 dans L'Année *sociologique* (texte téléchargeable [ici](http://classiques.uqac.ca/classiques/mauss_marcel/socio_et_anthropo/1_esquisse_magie/esquisse_magie.html), mis à disposition par L'Université du Québec à Chicoutimi). C'est un ouvrage de Marcel Mauss, un des fondateurs de l'anthropologie sociale, aidé de [Henri Hubert](https://fr.wikipedia.org/wiki/Henri_Hubert).

Cet été entre deux confinements pour cause de pandémie mondiale (COVID-19), là où la détermination du temps en référence à la « nature » (temps physique) lié celle du temps en référence à la « société » (temps social) [Nobert Elias, « Du temps », 1984] semblait se contracter ou se dilater hors de nos habitus, j'ai parcouru le Jura à pied −− sorte de marche introspective et critique[^1].

Arpentage des plateaux, des sources et rivières, des vallées, des pics et montagnes, des parfums et des mets, de la faune et de la flore… Cela me semblait un espace-temps favorable à la lecture, à l'annotation, à la réflexion, à l'intégration dans un vécu des synthèses[^2] obtenues dans une configuration au sein de laquelle un « sujet » n'est pas projeté par une ontologie hasardeuse sur « un objet » qui me serait étranger [N. Elias, 1984].

Au départ de cette randonnée, je savais où mon corps devait aller, j'avais une idée flottante de ce que mon esprit voulait traverser ; je n'avais pas de certitude de ce que ma pensée allait traverser.

De ces lectures et notes de Marcel Mauss, dont Claud Lévi-Strauss introduit la publication, je tente aujourd'hui de vous en rendre et partager quelques piquets de bivouac.

NDLR : *Marcel Mauss est décédé en 1950, ainsi une élévation dans le domaine public serait envisageable au 1er janvier 2021 (Modulo les « ennuis » habituels)*

## Introduction (par C. Lévi-Strauss)

<figcaption class="caption">Mon livre compagnon de périple est « Marcel Mauss : Sociologie Anthropologie (introduction par Claude Lévi-Strauss) » aux Presses Universitaires de France dans la collection Quadridge (octobre 2013)</figcaption>
<br>

Je fus surpris d'apprécier, au sens de me sentir en capacité d'intégrer, l'écrit de C. Lévi-Strauss. J'ai éprouvé du plaisir à comprendre et relier à mes savoirs et connaissances à ce qu'il décrit ; à l'instar du plaisir que j'éprouve dans mes champs de pratiques et d'études habituels. Je me suis bien délecté des écrits de Marcel Mauss, trouvant alors des conditions plus favorables, pour ma part, à l'appréhension de ses recherches et de ses concepts, de l'articulation de sa pensée −− bien que dite « opaque par sa densité » avec des « démarches tortueuses qui semblaient égarer […] » selon Claude Lévi-Strauss.

En revanche, je n'ai pas gouté à la plume de C. Lévi-Strauss, trop *hautaine* pour ma gourmandise littéraire. Assez probable que *Patterns of Culture* [Ruth Benedict, 1934] m'aiderait à comprendre la différence d'appréciation d'arômes de plume entre un « favorisé de la fortune » et un « anormal ». Le langage est un système symbolique sur lequel Lévi-Strauss attache beaucoup d'efforts et d'importance, un système visant à exprimer des réalités sociale et physique, et plus encore exprimer la relation que différents types de réalités entretiennent entre elles. Je n'ai probablement pas reçu les enseignements et pas vécus les rituels de transmissions des savoirs qui permettent de digérer les signes de Lévi-Strausset je ne savoure que peu le décalage que ces *notes de parfum d'un au-dessus* me font expérimenter.

C'est ici mon problème de déférence, de sentiment d'illégitimité, et des savoirs. Autrement dit, je préfère une distribution épistémique qui produit et critique des savoirs, de la connaissance sans violenter et en prenant soin d'empuissancer autrui ([Being-in-the-Room Privilege : Elite Capture and Epistemic Deference](https://www.thephilosopher1923.org/post/being-in-the-room-privilege-elite-capture-and-epistemic-deference), Olúfẹ́mi Táíwò)

> « Car c'est la linguistique, et plus particulièrement la linguistique structurale, qui nous a familiarisés depuis lors avec l'idée que les phénomènes fondamentaux de la vie et de l'esprit, ceux qui la conditionnent et déterminent ses formes les plus générales, se situent à l'étage de la pensée inconsciente » <u>C. Lévi-Strauss</u>, <cite>Introduction à l'œuvre de M. Mauss</cite>

L'évolution de l'inclusivité dans la langue française avec ses phases principales de masculinisation de la langue, aux XIII - XIV<sup>ème</sup>, puis du XVII<sup>ème</sup> siècle jusqu'au XX<sup>ème</sup> [Alpheratz, Grammaire du français inclusif. 2018] nous renvoie cette linguistique en plein visage avec les questions d'humanité.

Par un autre abord, c'est aussi un problème que je pose perpétuellement lors d'un choix de langage pour la programmation informatique ou dans le jargon de la biologie.

> « L'étude du basculement vers le langage est du ressort de la psychologie et de la biologie » 

Dès le langage apparu, l'Univers tout entier est devenu signifiant. Nous aurions alors vécu une transition de « Rien n'avait de sens » à « […] tout possédait un sens ».

> « L'Univers entier est devenu “significatif”, il n'en a pas pour autant été tout mieux connu » <u>C. Lévi-Strauss</u>.

Pour Lévi-Strauss « l'apparition du langage précipite le rythme du développement de la connaissance », ainsi « les deux catégories du signifiant et du signifié se sont constituées simultanément et solidairement ». Ce qui forme deux blocs complémentaires.

Le nom qui indique la « substance » qui relève de la « nature » sous-jacente d'un signifié vient parfois se lier jusqu'à la fusion avec ce que nous espérons être signifiant.

> « […] “le vent souffle” et “la rivière coule”, le vent serait-il donc autre chose que l'action même de souffler, la rivière autre chose que l'eau qui s'écoule ? Y a-t-il un vent qui ne souffle pas, une rivière qui ne coule pas ? » <u>Norbert Elias</u>, <cite>*Du temps, 1984*</cite>

Après plusieurs cours sur le biomimétisme en école d'ingénieur⋅e⋅s cette année, avec le mot « nature » employé dans cette micro-bulle de pratiques (et un podcast abordant la question en préparation), j'avoue me sentir profondément concerné et souvent englué dans ce problème du langage et de notre rapport au monde. Le recours à des « substantifs réifiants » [N. Elias]…

La connaissance est un processus intellectuel qui « permet d'identifier les uns (signifiants) et les autres (signifiés) ». Cette activité est une « mise en route plus lente que le langage ».

Un signifiant flottant, sans correspondance avec un objet précis et sans description ni mesure précise, est « une servitude de tout art, toute invention mythique et esthétique » ce qui est en soi « une pensée finie ». Ce que nous verrons ci-après avec le concept de *mana* chez Marcel Mauss. 

Pour dépasser (voir pour déborner puis reborner ?) cette situation, la pensée par la connaissance scientifique nous permet de tenter de discipliner un signifiant flottant, du moins pour Lévi-Strauss.

> « La fonction sémantique (qui) permet à la pensée symbolique de s'exercer sur un essai de description d'un phénomène d'un monde signifiant » <u>C. Lévi-Strauss</u>

C'est dans cette marche que se joue l'effort vers une traduction d'une idée. J'avais, au temps des premières lectures de cet ouvrage, randonné durant trois semaines sur au moins 300 kilomètres de distance dans le *topos* du Jura. La marche, je suis sur ce geste qui « enveloppe dans des instants menus des durées colossales. » (Michel Serres). Combien de gestes, qui replient une quantité phénoménale de temps sur eux-mêmes, pour rédiger ce fil ici ?

J'écris ici et maintenant comme je marche : « au fil de l'eau » et avec quelques-uns de mes instruments d'orientation qui m'apparaissent opportuns au gré d'une rencontre et/ou d'une contrainte. Tel est mon exercice présent.

L'eau a coulé sous les ponts, les pollutions aussi (7 000 litres d’hydrocarbures ont été récupérés dans la Loue en Mars 2022).

## Historique et sources (M. Mauss, chap. I)

Les « devanciers » aux travaux de Marcel Mauss sont l'anthropologue anglais Edward Burnett Tylor (1832 - 1917), qui fut le premier à parler d'animisme ; le prusso-allemand Ulrich Wilcken (animisme) et l'anglais Edwin Sidney Hartland (1848 - 1927) qui par la perspective du folklore étudia la magie au prisme du shamanisme. Tylor rattachant la démonologie magique à l'animisme primitif.

Ces travaux constituaient une base de « bonnes monographies », avec l'acquisition des notions de survivance et de sympathie, tout en comportant des lacunes, notamment dans « le catalogue le plus grand possibles de catégories », et des contradictions face à d'autres hypothèses d'explication de la magie.

Pour Mauss ce sont l'écossais James George Frazer (1854 - 1941) et le danois Alfred Georg Ludvig Lehmann (1858-1921), pour qui « la magie implique l’excitabilité mentale de l’individu », qui posent les pierres qui permettent d'arriver « à de véritables théories ». Les travaux de Lehmann porte sur la psychologie avec une histoire de magie qui sert de préface.

> « La théorie de M. Frazer, telle qu'elle est expliquée dans la deuxième édition de son *Rameau d'or*, est , pour nous l'expresion la plus claire de toute une tradition à laquelle ont contribué, outre M. Tylor, sir Alfred Lyall, M. Jevons, M. Lang et aussi M. Oldenberg. Mais comme, sous la divergence des opinions particulières, tous ces auteurs s'accordent à faire de la magie une espèce de science avant la science[…] » <u>M. Mauss</u>, <cite>*p. 4*</cite>

> « Pour Frazer, la magie est régie par deux principes, qui gouvernent les associations d’idées dans l’esprit humain : le principe de similarité et le principe de contact » <u>Frédéric Keck</u>, <cite>Les théories de la magie dans les traditions anthropologiques anglaise et française. 2002</cite>

Les travaux de Frazer et Jevons pour circonscrire la magie sont « entachés de partialité ».

La distinction entre magie et religion est immédiatement rappelée, notamment par l'essence de celle-ci, puisque les études nous montre que le rite magique agit directement sans l'intermédiaire d'un agent spirituel, et l'efficacité de la magie est nécessaire pour que la magie existe.

> « Cette dernière propriété, par laquelle la magie semble se distinguer essentiellement de la religion dans tous les cas où l'on serait tenté de les confondre, reste, en fait, d'après M. Frazer, la caractérique la plus durable et la plus générale de la magie.» <u>M. Mauss</u>, <cite>*p.5*</cite>

Aujourd'hui, toujours et encore, cette distance avec la religion est réaffirmée dans une alliance de l'art et de la science, pour exemple le jeu de tarot ”[Women of Science](https://we-make-money-not-art.com/the-women-of-science-tarot/)“ , qui raconte des histoires sur nos possibles devenirs « en se basant sur les principes de la science » [[MIT press](https://mitpress.mit.edu/books/women-science-tarot),2020], mettant en scène des femmes qui ont changé le cours des *STEM*. Et dans un autre registre, la thèse de Fabrice Sabatier « [Saisir l'économie par le(s) sens : une approche critique de la visualisation de données économiques par le design. Désorceler la finance](https://www.fabricesabatier.com/) », ainsi que la mise en pratique par le « [Cabinet de curiosités économiques Laboratoire Désorceler la finance](https://wiki.erg.be/mw/index.php?title=Cabinet_de_curiosit%C3%A9s_%C3%A9conomiques_Laboratoire_D%C3%A9sorceler_la_finance) »

Depuis la publication de Marcel Mauss nous pouvons suivre une classification « stratifiée » en 3 étages de la présence de la magie dans notre évolution, allant du plus bas niveau d'agrégats de connaissance intégrés en complexes dans notre système de représentation (via des symboles qui servent d'outils de mesure et d'orientation dans le monde) au niveau le plus complexifié d'intégration.

**1<sup>er</sup> étage** :

> « La magie constitue à la fois toute la vie mystique et toute la vie scientifique du primitif » <u>M. Mauss</u>, <cite>*p.5*</cite>

C'est l'étage de l'évolution mentale

**2<sup>ème</sup> étage** : 

> « La religion est sortie des échecs et des erreurs de la magie » <u>M. Mauss</u>, <cite>*p.5*</cite>

J'ajouterai volontiers « dans ces tentatives d'habiter et d'expliquer le monde » en prenant une perspective Husserlienne. Avant cet étage nous pensions avoir objectivé nos idées sans « doute », nous restions dans les certitudes sur la manière d'agencer ces idées, nous nous croyons maîtresses et maîtres des forces de nos créations et des forces la nature. Comme nous ne savions ni créer, ni dompter, l'eau par exemple, nous avons alors conçu des « forces mystérieuses » pour peupler ce monde qui nous résistait.

**3<sup>ème</sup> étage** :

> « […] l'esprit humain s'achemine vers la science ; devenu capable de constituer un monde et de constater les erreurs de la religion, il revient à la simple application du principe de causalité ; mais dorénavant il s'agit d'une causalité expérimentale et non plus une causalité magique ». <u>M. Mauss</u>, <cite>*p.5*</cite>

Ces transformations menant à de synthèses de plus en plus complexe ne sont pas irréversibles, et les « empiètements récents » de la religion sur la magie n'ont pas à entrer en compte dans la définition de la magie.

Notre société actuelle confrontée à une crise majeure globalisée, oubliant les précédentes comparables (et la transmission de connaissances acquises de ceux-ci vers le savoir social), me semble se recroqueviller dans des visions idéologiques partageant la même logique[^3] : la croyance messianique dans l’efficacité d’une programmation basée sur des causalités magiques passant par une idéologisation de la science et de la technique [J. Habermas, 1968], notamment celle du calcul. Une sorte d'empiètement orchestré par des dominant⋅e⋅s et récursivement entretenu par des dominé⋅e⋅s exité⋅e⋅s dans un système d'information planétarisée et d'information continue. Celle-ci est telle regard porté sur un *modèle*, le document, que l'on donne à voir (docere), sans cesse redéfilant avec à chaque *scroll* une légère altération. Ce qui veut dire que des personnes instituées sont des pivots dans la manière de constituer le regard, de définir un programme de vérité ou d’entretenir les *greedy daemons* d’un groupe. 

> « Ce genre d’assemblées et les sentiments qu’elles produisent sont perpétués par la curiosité impatiente des badauds qui se pressent, dans nos foires, autour des charlatans, vendeurs de panacées » […] « En définitive, c’est toujours la société qui se paie elle-même de la fausse monnaie de son rêve. La synthèse de la cause et de l’effet ne se produit que dans l’opinion publique ». <u>M. Mauss</u>, <cite>Théorie générale de la Magie (1902-1903), p.130 − Analyse et explication de la magie, les états collectifs et les forces collectives</cite>

> « La documentarité est une mesure des propriétés d'un document aux niveaux culturel et technique. » *[Le document numérique n'existe pas, il faut l'inventer (principe de documentarité)](https://stph.scenari-community.org/pres/20190222-documentarite/co/20190219-documentarite.html)*, <u>Stéphane Crozat</u>

**DAEMON**
(Extrait du carnet d'Olivier Lanctot, « Soigner la Technologie ? Stasis)

> « selon ce que soutient la science, les démons n'existent que dans la tête. C'est pourquoi il est logique que les machines qu'elle a conçues à l'effigie du cerveau — celles que nous connaissons sous le nom "ordinateur" — abritent aussi des démons »
<u>User180811972</u>

> «… le geste des religions : la création d'un arrière-monde » <u>Tiqqun 2</u>


## Définition de la Magie (M. Mauss, chap. II)

NDLR : *Essayez de garder en tête qu'il y a en fil de travail derrière chaque passage ici présenté les observations et les questions depuis les gestes, les techniques, les corps et techniques des corps, et de langage*

> « […] Ce sont les magies de quelques tribus australiennes ; celles d'un certain nombre de société mélanésiennes ; celles des nations de souche iroquoise, Cheerokees, Hurons, et, parmi les magies algonquines, celle des Objiways. Nous avons également pris en considération de la magie de l'ancien Mexique. Nous avons également fait entrer en ligne de compte la magie moderne des Malais des détroits, et deux des formes que la magie a revêtue en Inde : forme populaire contemporaine étudiée dans les provinces du Nord-Ouest ; forme quasi savante, que lui avaient donnée certains brahmanes de l'époque littéraire, dite védique. Nous nous sommes assez peu servi des documents de langue sémique, sans pourtant les négliger. L'étude des magies grecques et latines nous a été particulièrement utile pour l'étude des représentations magiques, et du fonctionnement réel d'une magie bien différenciée. Nous nous sommes enfin servi de faits bien attestés que nous fournissait l'histoire de la magie au Moyen-âge et le folklore français, germanique, celtique et finnois. » <u>M. Mauss</u>, <cite>*p. 8 - 9*</cite>

Il y a des bases fortes posées par Marcel Mauss concernant la magie. Telles que les agents, les actes (effectués par des magiciens, (c'est toujours genré au masculin dans ces écrits datant de 1902 - 1903)), et les représentations qui sont comprises dans la magie. Les représentation magiques sont les idées et les croyances qui sont performées par les non magiciens. Enfin, les actes servent de socle pour définir les autres éléments de la magie : les rites magiques.

> « Les actes rituels […] sont, par essence, capables de produire autre chose que des conventions ; ils sont éminemment efficaces ; ils sont créateurs ; ils font » <u>M. Mauss</u>, <cite>*p. 11*</cite>

Il m'est impossible ici de na pas penser à la présence de la « magie numérique »[^4] [^5] [N. Meylan, 2013 ; Luis Felipe R. Murillo, 2020] et à l'injonction actuelle *à faire*, à « l'âge du faire » [M. Lallement, 2015], « [Faire école ensemble (Fée)](https://faire-ecole.org/) », etc. Une injonction qui prend aussi forme dans les discours autour et dans les FabLabs, dans les campagnes de communication de ceux-ci. Aussi, concernant les conventions qui s'établissent par des actes la thèse « 
Étude de la configuration en Tiers-Lieu : la repolitisation par le service » [A. Burret, 2017], « enquête [qui] s’est déroulée entre 2010 et 2015 auprès de services – espaces de coworking, fablabs, hackerspaces, makerspaces, biohackerspaces, etc. dans leur composition et recomposition successives », m'apparait aujrourd'hui intéressante à relire en  parallèle des travaux de Marcel Mauss − y compris « Essai sur le don. Forme et raison de l'échange dans les sociétés archaïques » (1923 - 1924).

Il y a aujourd'hui des *instances* de représentation de tiers-lieux er des communautés de ceux-ci, pas vraiment exsangues de manipulations et de tentatives d'instauration, par des agents, « de régimes de vérité qui permettent à ces individus troubles, au carrefour de multiples intérêts, de se maintenir pendant de nombreuses années » (Marc Jahjah, 2022). Il y a aussi des *Coven* (Margareth A. Murray, The Witch-Cult in Western Europe , 1921), lieux de rencontres et assemblées rituelles renouvelées et qui luttent[^6] encore et toujours dans les interstices, marges, liminarités ; qui sont autant de centres moteurs de la société (Léna Dormeau, 2022).

Les rites religieux et les rites magiques, eux, sont des agents différents, « ils ne sont pas accomplis par les mêmes individus ».

> « L'isolement, comme le secret, est un signe presque parfait de la nature intime du rite magique » <u>M. Mauss</u>, <cite>*p. 15*</cite>

Alors qu'une pratique religieuse est « prévue, prescrite, officielle », la magie, elle, part non pas d'une obligation mais bien d'une nécessité (comme celle de soigner par exemple), avec un magicien aux gestes furtifs qui fuit, qui cache, qui s'isole. « L'acte et l'acteur sont enveloppés de mystère ». La magie ne fait partie d'un système organisé contrairement au culte.

> « Nous appelons ainsi *tout rite qui ne fait pas partie d'un culte organisé*, rite privé, secret, mystérieux et tendant comme limite vers le rite prohibé »

Pour Mauss, cette définition de la magie et du rite magique était « provisoirement suffisante » [*M. Mauss, p.16*] avec ses éléments tels que :

+ Les magiciens
+ Les actes
+ Les représentations

## Les éléments de la magie (chap. III)

Le magicien est l'agent de rites magiques qu'il fut ou non professionnel et/ou spécialiste, y compris pour ce qui concerne les « recettes de bonnes femmes », les pratiques de campagne, médecine magique, ou lors de chasse et de pêche.

Les actes du magicien sont des rites présents en très grand nombre et d'une grande complexité.

Les pratiques magiques sont remplies de sens. Elles correspondent à des représentations riches.

Le rite est une espèce de langage, il traduit donc une idée.

Il y a dans la magie des représentations impersonnelles, qui peuvent être divisées en représentations abstraites et concrètes, et des représentations personnelles qui sont de « nature » concrète.

> « Il y a dans la magie d’autres représentations à la fois impersonnelles et concrètes que celles des propriétés. Ce sont celles du pouvoir du rite et de son mode d’action ; nous en avons parlé plus haut à propos des effets généraux de la magie, en signalant des formes concrètes de ces notions, mâmit, mana, effluves, chaînes, lignes, jets, etc » [Université de Lausanne](https://www2.unil.ch/hubert-mauss-magie/l-article.html)

### Le magicien

Pour œuvrer le magicien doit changer d'état : il a rêvé, il a jeûné, il a pratiqué une interdiction sexuelle. Pour un instant, au moins, durant le rite, le rite fait de lui « un autre ».

> « Le paysan qui dit “la recette de ma grand-mère” est ainsi qualifié pour s'en servir » <u>M. Mauss</u>, <cite>*p.18*</cite>

Cela fonctionne dans un groupe d'individus qui partagent une croyance commune, qui, lorsque ce groupe fait société avec d'autres, peut devenir une croyance publique. Une configuration dans laquelle les « magiciens d'occasion ne sont pas des purs laïques ».

les rites « à la portée de tous et dont la pratique ne requiert pas d'habileté spéciale » est issu d'un procédé de vulgarisation par leur répétition :

La connaissance de la recette et l'accès à la tradition donnent un minimum de qualification. La magie est alors *populaire*, moins exclusive.

Cependant, et en règle générale, les pratiques magiques sont accomplies par des spécialistes ; et ces pratiques sont théoriquement réservées à ceux-ci, c'est-à-dire que l'établissement de la théorie leur appartient.

L'intéressé, celui qui reçoit la magie :

+ est dans la répétition des instructions
+ répétition des formules dictées
+ touche l'officiant

Il n'est pas acteur autonome.

#### Les qualités du magicien (pour se distinguer du commun)

+ Certaines sont acquises, donc portées et peuvent être reprises
+ d'autres sont congénitales, effectivement possédées

**A. Les phénomènes nerveux, signes de dons spirituels, manifestations d'une puissance inconnue**
  + la pupille qui a mangé l'iris
  + l'absence d'ombre corporelle
  + agitation d'une intelligence hors normes
  + dons oratoires ou poétiques
  + les extases

**B. Particularités physiques ou dextérité extraordinaire**
    + Ventriloques, jongleurs, bateleurs
    + Bossus, borgnes, aveugles

> « Remarquons que tous ces individus, infirmes et extatiques, nerveux, forains, forment en réalité des espèces de classes sociales. Ce qui leur donne des vertus magiques, ce qui n'est pas tant leur caractère physique individuel que l'attitude prise par la société à l'égard de tout leur genre » <u>M. Mauss</u>, <cite>*p. 20*</cite>

**C. Sentiment social apposé aux femmes**
  + les périodes critiques de leur vie (menstruations, grossesses, ménopause)
  + Virginité
  + Vieillesse
  + « L'hystérie » comme pouvoir sur-humain

> « Les femmes font l'objet soit de superstition soit de prescriptions juridiques et religieuses qui marquent bien qu'elles forment une classe à l'intérieur de la société » <u>M. Mauss</u>, <cite>*p.20*</cite>

Un sentiment social que l'on retrouve dans “[Pee is for Pregnant: The history and science of urine-based pregnancy tests](https://web.archive.org/web/20200526073609/http://sitn.hms.harvard.edu/flash/2018/pee-pregnant-history-science-urine-based-pregnancy-tests/)“, 1350 ans avant notre ère ; aussi dans le *signum diaboli* au Moyen-âge. Un stigmate qui l'on retrouve aujoud'hui fréquemment retourné comme outil d'empuissancement et de luttes[^7].

Ainsi, en religion, le rôle et la place des femmes est conscrit à un réduit de magie, une passivité confinée.

> « Le caractère magique des femmes révèle si de leur qualification sociale qu'il est surtout affaire d'opinion ». <u>M. Mauss</u>

Au regard du l'actualité aux USA de Juin 2022, la Cour suprême américaine révoque le droit à l'avortement puis au moins 7 États américains ont interdit le recours à l’IVG dès ce vendredi suivant la décision, je ferai volontier une paraphrase d'Yves Gringas (Sociologie des sciences, 3ème édition, 2020). Malgré le déclin du pouvoir temporel du christianisme depuis le XIXè siécle, les valeurs religieuses conservent un poids social important lorsqu'elles réussissent à s'incarner dans des mouvements sociaux ou des partis politiques qui, une fois au pouvoir, peuvent reconfiner et reviolenter les femmes.

**D. les enfants sont des auxiliaires spécifiquement requis, surtout pour les rites divinatoires**

Par leur âge et par leur « initiation » pas terminée, ils ont une place particulière. « ce sont encore des qualités de classe qui leur donnent leurs vertus magiques ».

**E. Rattachement à une profession**
  + Médecin, barbier, berger, forgeron, fossoyeur

Alors le pouvoir magique est bien attribué à des corporations plus qu'à des individus.

Tous ces praticiens précédemment cités sont, au moins virtuellement, des magiciens. Parce qu'ils touchent ou manipulent des tabous, des matières chargées de superstitions universelles, et que leurs métiers sont environnés de secret. Cette séparation du commun confine l'autorité magique. Et dans les sociétés où les fonctions sacerdotales sont « toutes a fait spécialisées, il est fréquent que les prêtres soient supects de magies » *M. Mauss, p. 22*.

… Ou les personnes pratiquant l'informatique ou le hacking, Magie et/comme hacking (Luis Felipe R. Murillo)

**F. Les étrangers, en tant que groupes, sont un groupe sorcier**

> « Pour les tribus australiennes, toute mort naturelle, qui se produit à l'intérieur de la tribu, est l'œuvre des incantations de la tribu voisine. C'est là-dessus que repose tout le système de Vendetta » <u>M. Mauss</u>, <cite>*p. 23*</cite>

L'étranger est celui qui habite un autre territoire, de ce point de vue les pouvoirs magiques ont été définis topographiquement.

> « Quand deux civilisations sont en contact, la magie est d'ordinaire attribuée à la moindre. […] Toutes les tribus non fixées, qui vivent au sein d'une population sédentaire, passent pour sorcières ; c'est encore le cas de nos jours avec les tsiganes […] » <u>M. Mauss</u>, <cite>*p. 23*</cite>.

Et aujourd'hui encore, dans le continuum entre sédentaires et « non-fixé⋅e⋅s, un racisme environnemental systémique est actif et entretenu en France. Phénomène excellemment documenté et décrit par Willam Acker dans « Où sont les “gens du voyage”. Inventaire critique des aires d'accueil », 2021, Éditions du commun.

Si, aujourd'hui, les personnes de l'informatique et du hacking jouissent de conditions de travail et de vies confortables, probablement enviables, d'autres catégories de population sont chassées, confinées, enfermées, prives de droits fondamentaux. Les résidus de magie sont distribués avec ségrégation.

Pour une thèse générale de la magie, d'après Mauss en 1902 - 1903, les individus auxquels ont été attribué l'exercice de la magie ont une condition distincte à l'intérieur de la société qui les traite de magiciens. Cependant, toute condition sociale anormale ne prépare pas à l'exercice de la magie.

Ce sont des « sentiments sociaux attachés à des conditions caractérielles qui vouent un individu à la magie, avec une situation socialement définie comme de la norme commune » <u>M. Mauss</u>, *p. 24*

> « Même, l'éducation magique semble avoir été, comme l'éducation scientifique ou technique, donné le plus souvent d'individus à individus. » <u>M. Mauss</u>, <cite>*p. 133*</cite>

Les savoirs et la connaissance ne volent pas dans les airs, ni ne s'écoulent dans l'eau. Les savoirs et la connaissance passent par les humain⋅e⋅s et en celleux-ci nous charrions un peu de nous-même.

## Notes et références

[^1]: “Essai sur la nature et la fonction du sacrifice.” (1899) Henri Hubert et Marcel Mauss

[^2]: Pour reprendre une note de N. Elias (« Du temps », 1984) : « j'évite intentionnellement de parler de “niveau d'abstraction”. À partir de quoi le concept de temps serait-il donc obtenu par abstraction ? »

[^3]: J'emprunte ici des mots et une articulation de Célya Gruson-Daniel sans parvenir à me remémorer où et quand ils ont été utilisés. 

[^4]: « [Magie numérique, magie anthropologique. Le mana dans la construction de l'efficacité rituelle](https://www.cairn.info/revue-les-cahiers-du-numerique-2013-3-page-15.htm) » Nicolas Meylan. Dans Les Cahiers du numérique 2013/3-4 (Vol. 9), pages 15 à 40

[^5]: Complément du n°56 : « [Magie et/comme hacking](https://www.journaldumauss.net/?Complement-du-no56-Magie-et-comme-hacking) » Luis Felipe R. Murillo. Complément du n°56 de La revue du MAUSS semestrielle. <https://www.journaldumauss.net/?Le-nouveau-MAUSS-est-arrive-Nous-l-avons-tant-aimee-la-sociologie-Et-maintenant>

[^6]: Rencontres OFFDEM O₂ : Les actes <https://pad.public.cat/offdem-o2-index#>, [Présence Solidaire et le soin Radical − Pænser Ensemble](https://thx.zoethical.org/pub/presence-solidaire)

[^7]: [Note de travail sur les sorcières. Démonisme et purgatoire](https://notecc.kaouenn-noz.fr/doku.php?id=pages:norae:hsociety:hacking_notes-sorcieres-1#invention_de_la_sorcellerie_criminalisation) & [Atelier biopanique, cuisine et féminisme V2](https://notecc.kaouenn-noz.fr/doku.php?id=pages:norae:hsociety:hacking_notes-biopanique-cuisine-feminisme-version-2)
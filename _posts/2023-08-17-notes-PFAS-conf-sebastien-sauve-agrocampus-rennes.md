---
title: "Notes : Conférence : « Contaminants émergents dans l’environnement – focus PFAS »"
layout: post
date: 2023-08-17 05:20
image: /assets/images/1-s2.0-S245222362300024X-ga1_lrg.jpg
headerImage: true
tag:
- hsociety
- bioassay
- Catastrophologie
category: blog
author: XavierCoadic
description: 
---

*Sur le site Agrocampus Rennes, conférence du 07/04/2023*

<figcaption class="caption">Image d'en-tête: source Hanna Joerss, Frank Menger,
The complex ‘PFAS world’ - How recent discoveries and novel screening tools reinforce existing concerns, Current Opinion in Green and Sustainable Chemistry, Volume 40, 2023, 100775, ISSN 2452-2236, https://doi.org/10.1016/j.cogsc.2023.100775 , Attribution 4.0 International (CC BY 4.0) </figcaption>
<br />

« Contaminants émergents dans l’environnement – focus PFAS » [Les substances per- et polyfluoroalkylées], <cite>Sébastien Sauvé</cite>, Université de Montréal.

Sébastien Sauvé est correspondant de l'Académie d'Agriculture de France, professeur en chimie environnementale à l'université de Montréal.

Agronome, spécialisé en chimie des sols, plomb, cuivre, cadmium, puis biodisponibilité et contaminants émergents : « devons-nous nous inquiéter lorsque nous mesurons de nouvelles molécules ? »<marginnote>Voir aussi, anthropologie : Mondes toxiques, dans Monde commun 2020/2 (N° 5)</marginnote>, avec pratiques de Chromatographie liquide couplée à la spectrométrie de masse, environ 1 million € l'appareille.

Je remercie chaleureusement Sébastien Sauvé pour m'avoir transmis les [Diapositives de sa conférence](https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=atelier:diapos-sebastien-sauve-rennes-2023-pfas.pdf), qui me permettent de rédiger mes notes et vous les partager.

**TOC**
+ [Préambule et contexte](#préambule-et-contexte)
  + [Exemple du plomb](#exemple-du-plomb)
  + [Exemple du « cube de sucre »](#exemple-du-«-cube-de-sucre-»)
  + [Eau potable et mesures et participations aux mesures](#eau-potable-et-mesures-et-participations-aux-mesures)
+ [PFAS](#pfas)
  + [Généralités](#généralités)
  + [Sur la bioaccumulation](#sur-la-bioaccumulation)
  + [Évaluation de concentration et critères](#évaluation-de-concentration-et-critères)
  + [La question des PFAS dans le sol et les plantes](#la-question-des-pfas-dans-le-sol-et-les-plantes)
+ [Conclusion](#conclusion)
  + [Les polluants de demain](#les-polluants-de-demain)
+ [Autres ressources](#autres-ressources)
+ [Références](#références)

*Voir aussi : « [Conférence-discussion sur l'enjeu des PFAS dans le milieu agricole](https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=pfas_biosolides_-_sauve_-_fevrier_2023.pdf) (diapos)*


## Préambule et contexte

Polluants dits d'intérêts émergents, ils ne sont pas de molécules nouvelles, ce sont les intérêts qui sont nouveaux / émergents. Comme par exemple, la contamination des ruisseaux pas des hormones / ex: réunion des premiers humains au bord du ruisseau (donc avec des hormones)…, il y a là un statut émergent lié au manque d’informations portant sur les composés chimiques et leur présence, concentration, durabilité, conséquences.

**Exemples des pesticides − Séquence de « nouveautés » pour
les pesticides**
+ Arséniate de plomb
+ DDT, dichlorodiphényltrichloroéthane, 
+ Organophosphoré
+ Atrazine
+ Néonicotinoïdes
+ Glyphosates


Des pesticides qui se sont remplacés tour à tour au gré des informations et des inquiétudes, on invente de nouvelles molécules pour remplacer les précédentes.<marginnote>Voir aussi: <b>Épsitémologie et exercices de L'anthropologie</b>, CNRS, <a href="https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=projet_14-18_axe1.pdf">PDF</a>, « Les nanoparticules manufacturées, par exemple, ne sont que partiellement prises en compte dans les réglementations existantes des produits chimiques, et les incertitudes sur leurs risques potentiels sont directement liées aux difficultés rencontrées pour assurer une caractérisation physico-chimique univoque. De même pour les nucléides qui, au sortir d’une catastrophe nucléaire, semblent offrir de la résistance aux programmes de mesures et partant, aux efforts pour qualifier la contamination et définir l’échelle de l’événement. C’est précisément cet aspect qu’abordera S. Houdart dans le projet qu’elle entame sur la gestion, au Japon, de ce qu’on appelle aujourd’hui le « post-Fukushima », en partenariat avec la Fondation 93 (F93), Centre de Culture Scientifique Technique et Industrielle de la Seine-Saint-Denis. L’entrée par la question de l’échelle lui permettra de saisir ensemble les opérations de gestion du microscopique (les contaminants dans l’espace public) et l’appréhension des effets macroscopiques, les modélisations de leur diffusion à l’échelle de la planète, qui viennent croiser d’autres phénomènes macro, comme les séismes ou les transformations atmosphériques. »</marginnote>

### Exemple du plomb

Le plomb a émergé dans l'antiquité avec les grecs et romains, puis dans l'industrie / « révolution » industrielle.<marginnote>Voir aussi : scandale « ville de Flint et l'empoisonnement au plomb » ET le cours de Didier Fassin (anthropologue, sociologue et médecin français) au Collège de France : « Naissance de la santé publique : de la clinique à la population, le cas du saturnisme infantile conséquence de l'intoxication par le plomb, des peintures notamment. Prendre en compte l'habitat insalubre, a contamination (<b>au plomb</b>), la xénophobie et la morale et l'éthique pour penser la santé. » Avec évidemment une prise de recul sur la pandémie actuelle et aussi la naissance du libéralisme et de la biopolitique. Quels sont les enjeux que les problèmes de la santé publique peuvent éclairer ? Durée 58 minutes <a href="https://www.college-de-france.fr/site/didier-fassin/course-2021-04-14-10h00.htm>URL</a></marginnote>

**Le plomb a « émergé » avec les Grecs et les Romains dans l’Antiquité**
+ Apport colossal avec l’exploitation du
charbon et la révolution industrielle
+ Utilisé comme additif dans l’essence
+ Encore présent en excès beaucoup trop
souvent dans l’eau potable
+ Pas fini d’en entendre parler…

Production plomb en tonne par année, traces mesurées[^ref-plomb] :
+ il y a 5 mille ans : 10⁰ tonne par an
+ aujourd'hui plus de 10⁶

[^ref-plomb]: <cite>C Boutron, K Rosman, C Barbante, M Bolshov, F Adams, S Hong, C Ferrari</cite>. 2004. *L'archivage des activités humaines par les neiges et glaces polaires : le cas du plomb.* Comptes Rendus Géosciences 336:846-867

Dans les mesures de glace au Groenland, il y a un pic il y a 2000 ans. Avant ce pic, 05.5 picogramme par gramme de glace ; puis il y a 2000 ans : 3,5 picogramme par gramme, les années 2000 : 170 picogramme par gramme, et l'explosion de concentration commence en ~ 1860.

![](/assets/images/plomb-glace.png)

Pour contaminer la terre entière, l'idée maléfique : mettre du plomb dans tous les véhicules qui circulent partout et aussi dans l'essence qui leur sert de carburant.

Les niveaux pré-humain était à 0.5 picogramme, puis un pic à 200 picogramme par gramme avant dans les années 70.

Puis, après des efforts de limitation d'utilisation et d'interdiction, la mesure dans les pôles arctiques donne une diminution des concentrations.

### Exemple du « cube de sucre »

Analyse par Haute résolution (LC-HRMS [haute résolution]) Q-Exactive
exemple d'un cube de sucre que je dilue dans un stade olympique : cela représente 2.15 nanogrammes par litre.
+ Qu'est-ce que ça veut dire si l'on mesure l'équivalent d'un cube *de sucre* dans le volume d'un stade olympique ?
  + Exemple pour Estradiol : 0.15 ng/l, et cela va « féminiser » (sic), aka influer sur les hormones et le développement, chez les poissons en bas age.

![](/assets/images/q-exactive.png)

**4 anti-biotiques dans les effluents à Montréal** (2006)[^ref-antibio] :

+ SMX (sulfaméthoxazole): 100 ng/L
+ TRI (triméthoprime): 50 ng/L
+ CLA (acide clavulanique): 250 à 300 ng/L
+ AZI (Azithromycine): 150 ng/L

![](/assets/images/montreal-wastewater-2006.png)

[^ref-antibio]: <cite>Segura PA, Garcia Ac A, Lajeunesse A, Ghosh D, Gagnon C, Sauvé S</cite>. 2007. *Determination of six anti-infectives in wastewater using tandem solid phase extraction and LC/MS/MS*. Journal of Environmental Monitoring 9:307-313.

Sur une année cela fait, cumulativement, un poids d'une tonne de molécules actives dans les effluents.

Le traitement de l'eau usée n'est pas conçu pour traiter les rejets médicamenteux, il est uniquement conçu pour lever la matière organique.

Et l'impact de cela ?[^ref-antibio2]
+ Dans le foie des truites : +++
+ Dans les tissus du cerveau des truites : ++, accumulation intermédiaire
+ Tissus musculaires : un peu moins

[^ref-antibio2]: <cite>Lajeunesse A, Gagnon C, Gagné F, Louis S, Čejka P, Sauvé S</cite>. 2011. *Distribution of antidepressants and their metabolites in brook trout exposed to municipal wastewaters before and after ozone treatment - Evidence of biological effects**. Chemosphere (doi:10.1016/j.chemosphere.2010.12.02))

![](/assets/images/truites-antibio.png)

### Eau potable et mesures et participations aux mesures

> *(ndlr)* **Atrazine** :  Dans les sols, l’atrazine est dégradée par action microbienne aérobie et par hydrolyse, en ses résidus principaux, soit en ordre décroissant la diéthyl-atrazine (DEA), la déisopropyl-atrazine (DIA), la diaminochloro-atrazine (DACA), ainsi que l’hydroxy-atrazine (HA). Dans l’eau, l’atrazine est hydrolysée et biodégradée en ces mêmes métabolites, mais le résidu DACA est plus important que le DIA (United States Environmental Protection Agency, 2002a)

La Triazine dans l'eau du robinet du labo [de Sébastien Sauvé], ici le DEA, dans l'eau potable. Le traitement de Montréal de l'eau enlève la moitié de ce qu'il y a, en quantité, dans le fleuve ; ~ 40 à 90 tonnes sont rejetées par année dans le transit par le fleuve.

![](/assets/images/triazines-eau-robinet.png)

Dans l'eau du robinet potable du labo :
+ avec fluctuation par saisons, pic en autonome d'Atrazine à + de 200 ng/L, et DEA en pic en début d'été 250 ng/L

**Normes pour eau potable**<marginnote>Voir aussi :  Shapiro, N. (2020). Polluants éphémères persistants. Monde commun, 5, 112-128. https://doi.org/10.3917/moco.005.0112 ; & « La catastrophe comme objet (en anthropologie) » et l'établissement de seuils par les chiffres qui deviennent des normes https://xavcc.frama.io/mardi-catastrophologie-5/</marginnote>

+ **UE: 100 ng/L**<marginnote>Voir aussi: Per- and polyfluoroalkyl substances (PFAS) of possible concern in the
aquatic environment ; Magdalena Niegowska, Patrizia Pretto, Elena Porcel-Rodriguez,Dimitar Marinov, Lidia Ceriani and Teresa Lettieri, May 2021. https://publications.jrc.ec.europa.eu/repository/bitstream/JRC125254/pfas_report__jrc_19.05.2021_final_online%283%29.pdf</marginnote>
+ **OMS: 100 000 ng/L**<marginnote>En 2011, l’OMS a relevé sa valeur limite pour l’atrazine dans l'eau potable, sur la base d'une évaluation réalisée en 2007 par la Réunion conjointe FAO/OMS sur les résidus de pesticides (JMPR). La limite a été multipliée par 50, passant de 2 à 100 microgrammes d'atrazine par litre d'eau (μg/l)</marginnote>
+ **EPA: 3000 ng/L**
+ **Canada: 5000 ng/L** *La concentration maximale acceptable (CMA) pour l'atrazine dans l'eau potable est de 0,005 mg/L (5 µg/L). Cette recommandation s'applique à la fois à l'atrazine et à ses métabolites N-désalkylés*
+ **Québec: 3500 ng/L** *La norme prévue par le Règlement sur la qualité de l’eau potable concernant la somme de l’atrazine et ses métabolites est de 5 μg/l (annexe 1 du règlement) (Gouvernement du Québec, 2001). Le 8 mars 2012, les normes correspondant à un paramètre inorganique (l’arsenic) de même qu’à 27 pesticides et à 9 autres composés organiques ont été abaissées*

**Mesure dans la nourriture: portant sur 22 pesticides**, pour Tomates, raisin, pomme et pomme-de-terre (achats en épicerie).<marginnote>Voir aussi : 80 % de la production alimentaire dans les pays « en voies de développement » se fait avec des eaux d'irrigations polluées . "Hydropolitics in the third world: conflict and cooperation in international river basins", Elhance AP., 1999 ; & "Comprehensive assessment of fresh water resources of the world" UN Commission for Sustainable Dev. ; World Meteorological Organization, 1997</marginnote>

![](/assets/images/pesticices-food.png)

Traces de pesticides partout, y compris dans les produits bio (avec traces plus petites)[^ref-bio-pesticides] par contamination environnementale notamment par mesure très sensible et procédure pointue.

[^ref-bio-pesticides]: <cite>Montiel-León JM</cite> et al. 2019. *Occurrence of pesticides in fruits and vegetables from organic and conventional agriculture by QuEChERS extraction liquid chromatography tandem mass spectrometry*. Food Control 104:74-82

![](/assets/images/bio-pesticides.png)

Sur les algues, cyanobactéries, comment prédire qu'il peut y avoir un bloom dans des environnements d'eaux douces ?

**Projet Adopte un Lac** <https://fas.montreal.ca/adopte-un-lac>
Pour mesurer des cyanotoxines différentes, y compris des nouvelles / peu connues.

Intérêt à la présence de cyanobactéries dans les sols des champs, masse azote et masse de phosphore, donc conditions très favorable avec humidité pour des cyanobacter / cyanotoxines. Avec les questions d’apparition,, disparition, absorption, intégration, transfert ?

## PFAS

### Généralités

Il y a de 5000 à 10000 PFAS, même 14 000, selon les sources qui émettent des répertoires des chaînes<marginnote>Voir aussi: Hanna Joerss, Frank Menger, The complex ‘PFAS world’ - How recent discoveries and novel screening tools reinforce existing concerns, Current Opinion in Green and Sustainable Chemistry, Volume 40, 2023, 100775, ISSN 2452-2236, https://doi.org/10.1016/j.cogsc.2023.100775. ; & Outside the Safe Operating Space of a New Planetary Boundary for Per- and Polyfluoroalkyl Substances (PFAS), * Ian T. Cousins***** , * Jana H. Johansson , * Matthew E. Salter, * Bo Sha, and * Martin Scheringer. Environ. Sci. Technol.* 2022, 56, 16, 11172–11179, August 2, 2022, https://doi.org/10.1021/acs.est.2c02765 </marginnote>

**PFOS** et **PFOA** sont les 2 molécules les plus incriminées, et bannies par la convention de Stockholm sur les polluants organiques persistants (22 mai 2001)

**La durée vie du DDT est plus longue que celles des PFAS**

**Les PFAS** :
+ très fluoré, lien carbone, fluor stable
+ Résistance thermique et chimique
+ Repousse l'eau, et les corps gras, comme un surfactant

Quelques exemples dans l'industrie :
+ 1938-1950 : PTFE de la société américaine E.I. du Pont de Nemours and Company[^ref-dupont] puis PFOS de l'entreprise 3M
+ Initialement reconnu comme peu toxique et sans danger

[^ref-dupont]: voir <https://pfasproject.com/parkersburg-west-virginia/>

> [NDLR] j'ajoute : 
> [Ressources] **Anthropocene & Molecular Colonialism**
>  + <cite>Madriga Mendes</cite> "Molecular Colonialism", Consumption and Growth under Climate Austerity <https://www.anthropocene-curriculum.org/contribution/molecular-colonialism>
>   + [PDF] <https://inhabitants-tv.org/oct2018_colonialismomolecular/MargaridaMendes_MatterFictions_EN_126-141.pdf>
>   + [Podcast] The World in Which We Occur: Molecular Colonialism <https://archive.org/details/TWWWOsession2>
>   + [Podcast] The World in Which We Occur: **Water Politics** <https://archive.org/details/WaterPolitics> 
>   + Molecular Colonialism: A Geography of Agrochemicals in Brazil, <https://www.librarystack.org/molecular-colonialism-a-geography-of-agrochemicals-in-brazil/>
>   + <cite>Larissa Mies Bombardi</cite>, "A Geography of Agrotoxins use in Brazil and its Relations to the European Union" <https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=a-geography-of-agrotoxins-use-in-brazil-and-its-to--annas-archive.pdf>

### Sur la bioaccumulation

**Données de bioaccumulation de PFOS dans le foie d'ours polaire**[^ref-polarbear]

![](/assets/images/pfos-polarbear.png)

Depuis les années 2000, on démontre la des PFAS dans les animaux sauvages
+ Données confirmées dans le sang et sérum humain ou même lait maternel
+ La somme des PFAS mesurés, donc pas tous les PFAS, est de 10 à 50 ng/L dans les poissons affecté par la catastrophe de Mégantic du 6 juillet 2013.

[^ref-polarbear]: *Temporal Trends of Perfluoroalkyl Contaminants in Polar Bears (Ursus maritimus) from Two Locations in the North American Arctic, 1972−2002*. <cite>Smithwick</cite> et al. Environmental Science & Technology 2006 40:1139-1143

On savait la bioaccumulation, mais on ne savait pas s'il y avait des effets toxiques.

Évolution récente de régulation dans l'eau potable, le cas des PFAS est beaucoup moins réglementé dans les sols et dans les transferts aux végétaux.

**Règlementation dans l’eau potable**
+ West Virginia a initialement règlementé à 150 000 ng PFOA/L
+ USEPA a suivi avec un seuil de 400 ng PFOA/L en 2009, révisé à 70 ng PFOA/L ou
PFOS/L en 2016
+ Au Canada on est à 600 ng PFOS/L et 200 ng PFOA/L
+ Union Européenne cible un seuil d’une somme de différents PFAS à ne pas dépasser de 100 ng/L – effectif en 2026

La recherche se fait avec des calculs sur des bases de consommation courante, puis aide à définir à la réglementation.<marginnote>Voir aussi: Le programme REACH    . Hartung T. estimait en 2009 que 86% de données concernant les produits chimiques de l'industrie étaient manquantes, que REACH apportait l'opportunité d'agir sur ces manques. (Toxicity for twenty-first century. Nature, 2009;460(7252):208-12). Cette régulation concernait en 2008, année des premiers enregistrements, 27 000 entreprises et l'usages de 30 000 produits chimiques</marginnote>

**Effets des PFAS sur la santé** :
+ effet sur les lipides des humain⋅e⋅s, 
+ certains cancers, 
+ impacte le système immunitaire, 
+ dommages au foie, 
+ réduction du poids à la naissance.
+ Sur le système immunitaire, les concentrations de PFAS très basse provoque déjà des effets délétères.

*voir aussi : <https://www.epa.gov/sdwa/drinking-water-health-advisories-pfoa-an>*

![](/assets/images/pfos-santé.png)

PFAS dans le sang, dans le sérum humain, si on double la concentration "normale", il y a une chute de 25% de la réponse immunitaire avec un vaccin.<marginnote><i>How “forever chemicals” might impair the immune system
Researchers are exploring whether these ubiquitous fluorinated molecules might worsen infections or hamper vaccine effectiveness.</i>, Carolyn Beans, 2021, https://doi.org/10.1073/pnas.2105018118 ; Shih YH, Blomberg AJ, Bind MA, Holm D, Nielsen F, Heilmann C, Weihe P, Grandjean P. Serum vaccine antibody concentrations in adults exposed to per- and polyfluoroalkyl substances: A birth cohort in the Faroe Islands. J Immunotoxicol. 2021 Dec;18(1):85-92. doi: 10.1080/1547691X.2021.1922957. PMID: 34143710; PMCID: PMC10204592.</marginnote>

Pas assez d'études similaires dans la faune sauvage, pas grand chose sur les impacts sur le système immunitaire des mammifères marins, ou des animaux d'élevages.

### Évaluation de concentration et critères

Juin 2022, ré-évaluation des niveaux maximum à ne pas dépasser,

Ce qui est un peu plus bas que les limites de détection en labo. Pas de critère concernant l'eau potable, mais une valeur « guide de ressenti ».<marginnote>Les humain⋅e⋅s prélèvent plus de la moitié de l’eau qui s’écoule dans les rivières du monde entier, soit 24 000 km3 par an. Cette eau est utilisée principalement dans l’élevage d’animaux pour la consommation de viande, en majeure partie via la mise en culture de céréales et de fourrages pour l’alimentation animale. Une étude dans laquelle a été analysé plus de 450 représentations du cycle de l’eau dans 12 pays différents : 85% de ces schémas ne représentaient aucun des impacts dus aux interventions humaines sur le cycle de l’eau ; seulement 2% montraient l’impact du changement climatique ou de la pollution des eaux. Or ces facteurs sont les deux principales causes de la crise mondiale actuelle concernant nos ressources en eau. Abbott, B.W., Bishop, K., Zarnetske, J.P. et al. Human domination of the global water cycle absent from depictions and perceptions. Nat. Geosci. 12, 533–540 (2019) https://doi.org/10.1038/s41561-019-0374-y</marginnote>

Février 2023, Canada, **ALARA AS Low As Reasonably Achieveable…**

> « Il est recommandé que les stations de traitement s'efforcent de maintenir les concentrations de SPFA dans l'eau potable au niveau le plus bas qu'il soit raisonnablement possible d'atteindre (as low as reasonably
achievable, ALARA). »
>
> <https://www.canada.ca/fr/sante-canada/programmes/consultation-objectif-propose-qualite-eau-potable-canada-substances-perfluoroalkylees-polyfluoroalkylees/apercu.html>

**Recommandation: ∑PFAS < 30 ng/L dans l’eau potable**

> « C'est une des formes que peut prendre le principe de précaution dans le domaine de la toxicologie ou de la radioprotection, quand il y a conjointement “effet stochastique” et hypothèse LNT (hypothèse linéaire sans seuil) ou quand il y a incertitude sur la relation dose-effet (incertitude par manque de connaissance, mais avec faisceaux de présomptions ou indices forts de LNT, soit en raison de susceptibilités génétiques…) » <cite>Wikipedia</cite>

Mars 2023, USA, <https://www.epa.gov/sdwa/and-polyfluoroalkyl-substances-pfas>

![](/assets/images/pfas-EPA-USA-2023.png)

Critères, pas encore applicable, avec objectif zero présence.

> "EPA is proposing a National Primary Drinking Water Regulation to establish legally enforceable levels, called Maximum Contaminant Levels (MCLs), for six PFAS in drinking water. EPA is also proposing health-based, non-enforceable Maximum Contaminant Level Goals (MCLGs) for these six PFAS.
The proposed rule would also require public water systems to:
> + Monitor for these PFAS
> + Notify the public of the levels of these PFAS
> + Reduce the levels of these PFAS in drinking water if they exceed the proposed standards.
<https://www.epa.gov/sdwa/and-polyfluoroalkyl-substances-pfas>

**Compound** | **Proposed MCLG** | **Proposed MCL (enforceable levels)**
:---: | :---: | :---:
PFOA | Zero | 4.0 parts per trillion (also expressed as ng/L)
PFOS | Zero | 4.0 ppt
PFNA | 1.0 (unitless) Hazard Index | 1.0 (unitless) Hazard Index
PFHxS | 1.0 (unitless) Hazard Index | 1.0 (unitless) Hazard Index
PFBS | 1.0 (unitless) Hazard Index | 1.0 (unitless) Hazard Index
HFPO-DA (commonly referred to as GenX Chemicals) | 1.0 (unitless) Hazard Index | 1.0 (unitless) Hazard Index


Ici c'est ciblé et très strict contrairement à Europe et Canada qui vise une somme / cumulative. Et ici il n'y a pas d'intégration des « nouveaux » PFAS.

Chaque pays peu jongler différemment depuis une cible pour une mesure perfuchluoré totaux. France, fait depuis une sommation d'une vingtaine de PFAS à ne pas dépasser.

**Union Européenne**
+ La somme des PFAS dans l’eau potable ne doit pas dépasser 100 ng/L
+ Doit être mis en place par les pays membres pour le début 2026.
+ La somme des PFAS: Il s’agit d’un sous-ensemble des substances constituant le Total PFAS qui contiennent un
groupement de substances perfluoroalkylées comportant trois atomes de carbone ou plus (à savoir, –CnF2n–, n ≥ 3) ou un groupement de perfluoroalkyléthers comportant deux atomes de carbone ou plus (à savoir, –CnF2nOCmF2 m–, n et m ≥ 1).

*ref : <https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32020L2184&from=FR>*

**Somme des PFAS** 

Les substances qui suivent sont analysées sur la base des lignes directrices techniques élaborées en conformité avec l’article 13, paragraphe 7:

Somme de 20 PFAS (Union Européenne)
+ Acide perfluorobutanoïque (PFBA)
+ Acide perfluoropentanoïque (PFPeA)
+ Acide perfluorohexanoïque (PFHxA)
+ Acide perfluoroheptanoïque (PFHpA)
+ Acide perfluoroctanoïque (PFOA)
+ Acide perfluorononanoïque (PFNA)
+ Acide perfluorodécanoïque (PFDA)
+ Acide perfluoroundécanoïque (PFUnDA)
+ Acide perfluorododécanoïque (PFDoDA)
+ Acide perfluorotridécanoïque (PFTrDA)
+ Acide perfluorobutanesulfonique (PFBS)
+ Acide perfluoropentanesulfonique (PFPeS)
+ Acide perfluorohexane sulfonique (PFHxS)
+ Acide perfluoroheptane sulfonique (PFHpS)
+ Acide perfluorooctane sulfonique (PFOS)
+ Acide perfluorononane sulfonique (PFNS)
+ Acide perfluorodécane sulfonique (PFDS)
+ Acide perfluoroundécane sulfonique
+ Acide perfluorododécane sulfonique
+ Acide perfluorotridécane sulfonique

Au canada, il n'y a pas de site de production de PFAS, il y que de sites qui en utilisent.

**EU, Santé Canada et USEPA**
+ UE propose une cible pour l’eau potable à 100 ng/L pour la somme d’une vingtaine de PFAS
+ Santé Canada propose une cible pour l’eau potable à 30 ng/L pour la somme d’une trentaine de PFAS.
+ USEPA propose des cibles pour l’eau potable à 4 ng PFOA/L ou 4 ng PFOS/L
+ Approches très différentes mais les deux sont parmi les plus restrictives au monde
+ Au Québec, la santé publique recommande de boire l’eau du robinet et considère que pour le moment, il n’y a aucune
restriction à consommer l’eau à aucun endroit au Québec

![](/assets/images/carte-munioz-pfas.png)

Les PFAS sont associés avec les mousses de luttes contre les incendies, les agents mouillants, retardants / Bombardiers d'eau<marginnote>Voir aussi: Proposal to ban ‘forever chemicals’ in firefighting foams throughout the EU − ECHA/NR/22/05 − Corrigendum 11 July 2022: word 'export' replaced with 'formulation' to clarify the scope of the proposed restriction. https://echa.europa.eu/-/proposal-to-ban-forever-chemicals-in-firefighting-foams-throughout-the-eu</marginnote>, les agents formants un film flottant (A3F), dans le champ de lutte contre les incendies, notamment urbains / industriels, parfois en feux de forêts.

![](/assets/images/data-munoz-quebec.png)

![](/assets/images/pfas-quebec-eau.png)

![](/assets/images/pfas-bdd-quebec.png)

**Sur un total de 376 sites au Québec**

+ 5 sites au Québec avec la ∑PFAS > 30 ng/L (le seuil proposé par Santé Canada).
+ 7 sites au-dessus des seuils proposés par USEPA (et un site qui ne serait pas identifié avec la norme états-unienne).
+ Quand on applique les deux normes aux données du Québec, la norme de l’agence USEPA semble plus protectrice que la proposition de Santé Canada (cette dernière devrait être ajustée à ∑PFAS <15 ng/L pour avoir un résultat
comparable à la proposition de l’USEPA).
+ Aucune norme ou exigence de suivi ne sont requises pour les PFAS dans l’eau potable au Québec. Nous n’avons même pas de données pour toutes les municipalités du Québec

**Pour la régulation / recommandation :**
+ Santé canada part d'une sommation de 29 PFAS.
+ EU; une sommation de 20 PFAS
+ des mesures pour 77 PFAS, Laboratoire de S. Sauvé, donne des différences très significatives par rapport à ces choix.

![](/assets/images/pfas-tapwater-france.png)

![](/assets/images/pfas-france-tapwater.png)

![](/assets/pfas-limit-drinking-water.png)

**Etude 1 - Evaluation des composés perfluorés (PFAS) dans divers produits résiduaires organiques (PRO)** (en France) 
+ **6 sites SOERE** (*Système d'observation et d'expérimentation sur le long terme pour la recherche en environnement*) **PRO, 47 sites PRO**
  + Échantillons 1976-2018 (42 ans) (*→ Lyophilisation et envoi au Canada pour les échantillons*)
    + 6 effluents d’élevage, 2011-2018
      + Fumier de bovins (FYM-DC, n = 6)
      + Lisier porcs (PS, n = 4)
      + Litière volailles (PM, n = 4)
      + Compost de fumier de bovins (C-FYM-DC, n = 1)
      + Compost de fumier de porcs (C-FYM-P, n = 3)
      + Digestats lisier porc (DIG-PS, n = 3)
    + 5 PRO urbains, 1976-2018
      + Boue STEP (SLU, n = 10, 1976-2017)
      + Compost déchets verts / boue STEP (C-GWS, n = 6, 1996-2017)
      + Compost biodéchets (C-BIOW, n = 4, 2009-2016)
      + Compost ordures ménagères résiduelles (C-MSW, n = 3, 2011-2016)
      + Digestat déchets urbain (DIG-UW, n = 1, 2016)
    + 2 PRO industriels, 1996
      + Boue papetière (PSLU, n = 1)
      + Cendres (ASH, n = 1)

**Résultat**
+ les boues urbaines et les composts urbains d’ordures ménagères résiduelles peuvent être vecteurs
significatif d’entrée de nouveaux composés perfluorés dans les sols
agricoles.
+ Les effluents d’élevage (bruts ou traités) sont peu ou pas contaminés, tout comme les composts de biodéchets et les digestats urbains.

![](/assets/images/pfas-resultat.png)

**Voir aussi**

> Case study: PFAS in organic wastes of land application in France – importance of novel
zwitterions
> + ESI(+) fluorotelomers: 55% of ΣPFAS across 26 urban wastes.
> + Profiles have shifted from anionic to zwitterionic PFAS.
> + Standardized methods currently do not target the zwitterionic PFAS.
>
> *Munoz et al. Environmental Science and Technology (2021)*

![](/assets/images/pfas-studies.png)

![](/assets/images/pfas-distrubution.png)

La majorité des méthodes d'analyse, qui aide à guider les réglementations, se font sur des anions, charge négative, sauf ici Fluorometers

### La question des PFAS dans le sol et les plantes

![](/assets/images/context-pfas-sols.png)

**CONTAMINATION OF SOILS**

| Soils with Biosolids | Soil with composted biosolids |
| :------------------------: | :------------------------: |
| [PFOS] = 408 μg/kg dw[^ref-washington] <br />[PFOA] = 312 μg/kg dw[^ref-washington] | [PFOS] = 26,1 - 102,0 μg/kg dw[^ref-sungur] <br />[PFOA] = 0,211 - 0,649 μg/kg dw[^ref-sungur] |
| [PFAS] = 2 à 130 μg/kg dw[^ref-coggan]|
| |

*Soils having received biosolids. Concentration of PFAS in soils having received biosolids from wastewater traitement plants impacted by nearby manufacturing plants using PFAS*

[^ref-washington]: Washington et al., 2010, EU, 
[^ref-coggan]: Coggan et al., 2019, Australie,
[^ref-sungur]: Sungur et al., 2020

+ Municipal wastewater treatment plants are often receiving PFAS
+ Few studies for municipal wastes in soils

![](/assets/images/crops-contamination-pfas.png)

**Absorption des PFAS par les plantes**

*L'absorption et l'accumulation des PFAS dépendent :* 
1. du type de PFAS (longueur de la chaîne)
2. Les groupes fonctionnels affectent la translocation
3. Type de plante et propriétés du sol

+ Les PFAS à chaîne courte semblent s'accumuler plus facilement
  + Maïs :
    - Les PFCA s'accumulent plus facilement dans les résidus que les PFSA[^ref-sungur]
    - Les PFSA atteignent plus facilement les grains que les PFCA[^ref-sungur]

Peu d'études dans les sols qui ont reçu des biosolides.

![](/assets/images/china-crops-pfas.png)

+ PFAS à longue chaîne se comporte un peu comme le DDT et les PEP
+ Ceux à petites chaines vont se comporter de manière plus proche des contaminants plus polaire

Il y a là un enjeu à comprendre le pourquoi ces différences ? Entre carboxylates et sulfanates

Les longues chaines ont plus d'affinité avec le sols, la plante difficile à prendre pour elle
Les courtes chaines sont plus mobiles, donc plus accessible aux plantes

![](/assets/images/experimental-design-sauvé.png)

![](/assets/images/pfas-control-soil.png)

*(NDLR: L’Inra est devenu en 1968 propriétaire de Château Couhins)*

![](/assets/images/soils-couhins.png)

Questionnements sur les apports de PFAS dans l'environnement:
+ pesticides ?
+ Atmosphériques ? / poussières ?

![](/assets/images/pfas-qualiagro.png)

![](/assets/images/pfas-qualiagro-2.png)

Île de la Réunion, le PFPi est très concentré, alors que très rare dans les autres mesures<marginnote>Voir aussi: "The ECOTOX Knowledgebase includes ecotoxicological studies on invertebrates for 47 different PFAS that can be grouped into 6 broad categories: 19 fluorotelomer substances, 13 PFCAs, 5 PFSAs, 3 fluorocyclohexanes, 3 perfluoroalkane sulfonamides (FASAs), and 4 fluorinated plant protection products (FPPPs)" ; Ankley GT, Cureton P, Hoke RA, et al. Assessing the Ecological Risks of Per- and Polyfluoroalkyl Substances: Current State-of-the Science and a Proposed Path Forward. Environ Toxicol Chem. 2021;40(3):564-605. doi:10.1002/etc.4869</marginnote>

![](/assets/images/pfas-ile-réunion.png)

Il y a plus de "traces" dans les grains de maïs que dans les feuilles, sans différences significatives.

Un surfactant à base de PFAS avec apport foliaire est souvent une question face aux résultats d'analyses.

Île de la Réunion, en zone de culture de cannes à sucre et la présence de PFPi, dans tous les sols, y compris sol control.

**Apports aux sols et risques de transfert dans les plantes et les eaux de percolation ?** (*QUANTIFICATION OF PFAS IN Draining Water − En cours)*

+ PFAS retrouvés dans eaux percolation (40 cm)
+ Y compris dans eaux issues sols témoins sans apports PRO
+ Valeurs à valider et résultats à analyser

## Conclusion

Avec amendements de biosolides en PFAS on va augmenter la concentration de PFAS dans les sols

PFOA/PFOS sont persistent dans les sols

PFAS à courte chaîne sont plus mobiles dans les sols

![](/assets/images/bassin-versant.png)

**Sols**
+ L'ajout de déchets organiques augmente les niveaux de PFAS dans le sol, mais reste inférieur à ce qui a été trouvé dans les différents amendements.

| Produits urbains | Fumier de bovins |
| :-----------------: | :----------------: |
| **0,25 - 24,11** μg/kg dw | **0,66 - 0,74** μg/kg dw |

+ **PFOA / PFOS** : persistants dans les sols malgré les réglementations et les restrictions
+ D'autres **PFAS à chaîne courte** sont apparus ces dernières années.

**Plantes**
- Faible contamination des parties consommées (céréales)
- Faible disponibilité et translocation des PFAS à longue chaîne
- Plus grande absorption et translocation des PFAS à chaîne courte par les plantes

![](/assets/images/bassin-versant-2.png)

+ On peut certainement détecter les contaminants émergents dans l’environnement mais pas toujours facile de démontrer la présence ou l’absence de risques.

+ La présences de ces contaminants soulèvent des questions pour:
  +  **Contribution à la résistance aux antibiotiques**
  +  **Agir comme perturbateurs endocriniens**
  +  **Potentiel pro-inflammatoire et cancérigène**
  +  **Vecteurs d’exposition (eau, air, nourriture)**
  +  **Exposition chronique**
  +  **Santé des sols**

### Les polluants de demain

+ Nouveaux matériaux pour capture et stockage d'énergie
+ Liquides ioniques
+ Cyanotoxines
+ Nouveaux pesticides

# Autres ressources

+ *A review of what is an emerging contaminant* <cite>Sébastien Sauvé</cite> and <cite>Mélanie Desrosiers</cite>, Chemistry Central Journal 2014, 8:15 <http://journal.chemistrycentral.com/content/8/1/15>

+ *Target and Nontarget Screening of PFAS in Biosolids, Composts, and Other Organic Waste Products for Land Application in France*, <cite>Gabriel Munoz</cite>, <cite>Aurélia Marcelline Michaud</cite>, <cite>Min Liu</cite>, et al. Environmental Science & Technology, American Chemical Society, May 1, 2022 <https://doi.org/10.1021/acs.est.1c03697> ([PDF](https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=acs.est.1c03697.pdf))

If you're PFAS interested 

Well, here's just an incomplete list

+ PFEtS
+ PFPrS
+ PFBS
+ PFPeS
+ PFHxS
 + PFHpS
+ PFOS
+ PFNS
+ PFDS
+ PFDoS
  + 4:2 FTSA
  + 6:2 FTSA
  + 8:2 FTSA
  + 10:2 FTSA
  + 12:2 FTSA
  + 14:2 FTSA
+ FPrSA
+ FBSA
+ FPeSA
+ FHxSA
+ FHpSA
+ FOSA
+ FNSA
+ FDSA
+ FUnSA
+ FDoSA
  + 6:2 FTSAS
    + But also:
     + :2 FtTAoS
  + And also:
     + 6:2 FTTh-PrAd-DiMeEtS
+ TFA
+ PFPrA
+ PFBA
+ PFPeA
+ PFHxA
+ PFHpA
+ PFOA
+ PFNA
+ PFDA
+ PFUnA
+ PFDoA
+ PFTrDA
+ PFTeDA
+ PFPeDA
+ PFHxDA
+ PFHpDA
+ PFOcDA

There are between 5,000 and 10,000 PFAS, or even 14,000, depending on the source of the directories. <https://www.nature.com/articles/d41586-023-02444-5> & <https://echa.europa.eu/-/echa-publishes-pfas-restriction-proposal>


# Références
---
title: "Notes de cours : Réfugié⋅es au 21e siècle − Qu’est ce ’Réfugié⋅es’ et confusion terminologique"
layout: post
date: 2024-06-01 09:30
image: /assets/images/eufemia.png
headerImage: true
tag:
- hsociety
- Notes
- Anthropologie
category: blog
author: XavierCoadic
description: Nous pouvons voir des dénis ancrés que nous reproduisons dans les biais de l’éducation et de l’information
---

*Toutes les ressources utilisées pour cette note de cours sont disponibles [ici](https://www.zotero.org/groups/4989006/catastrophologie/collections/ZYF5IMNA)*

Réfugié⋅es, migrant⋅es, exilé⋅es, c’est la désignation d’un « autre » qui vient dans un « ici » depuis des circonstances particulières, avec un contexte spécifique. Cette arrivée se faisant avec des imaginaires ancrés chez les « hôtes » de l’ici et chez les arrivant⋅es désigné⋅es. [^ref-cd-cahier9_web-18.pdf].

Opérer des classifications par catégories c’est procéder de réductions avec des simplifications. Ce type de discussion est toujours en cours depuis au moins Platon et Héraclite, en passant par Darwin et jusqu’aujourd’hui.

Pour établir des *formes thématiques* nous portons des jugements *a priori* et *a posteriori*. « Les réfugié⋅es » étant ici une formation thématique. Nous précédons selon des *règles d’un art*. Plus après, nous pourrions être tenté⋅es de *déduire* ce qui revient à donner naissance à des jugements conséquents à partir de jugements. Nous sommes en permanence dans la confrontation avec nos propres assertions qui portent un caractère de certitude.

Les ontologies, les formes, les nominations… Nous avons l’assurance percevoir et dans les faits nous avons la croyance de perception sur des bouts restreints par des prismes (E. Husserl). Nous sommes hérité⋅es de ces schémas (É. Durkheim). Nous nous laissons guider par de lignes directrices (M. Mauss).

Nous partons de jugements pour établir des critères qui nous servent ensuite d’appui pour confectionner des catégories. En faisant ainsi nous procédons par réductions, qui peuvent être autant de « destruction » d’une complexité. Ce réductionnisme se prend de matérialisme (ontologie matérielle *a priori* synthétique) fonctionnant avec des concepts issus d’idées (idéalisme). C’est une méthode qui amène du *confort* de travail tout en achalandant des lots d’approximations conséquentes et d’erreurs afférentes.

![](/assets/images/eufemia.png "Photographie d'un scène où eux humain⋅es sont assisses l'un⋅e en face de l'autre dans une pénombre. Entre Elleux un panneau d'affichage numérique à diode affiche 'Eufemia'. Les murs autours sont couvert des tags en plusieurs langues évoquant les déplacements de personnes et l'accueil. Aussi un ballon de baudruche peint prend les couleurs du drapeau de l'Union Européenne")
<figcaption class="caption"> Image issue des projets du <a href="https://www.laboratoriosociologiavisuale.it">Laboratorio di Sociologia Sisuale</a>, sous licence CC BY NC SA</figcaption><br>

Voir aussi: 
+ [Notes de cours : Réfugié⋅es au 21e siècle -- Un monde de réfugié⋅es et 2 questions sur les camps](https://xavcc.frama.io/refugies-21-eme-siecle-un-monde-de-refugi%C3%A9-es/)
+ HCR, [Guide de la procédure et des critères de détermination du statut de réfugié⋅e](https://www.refworld.org/policy/legalguidance/unhcr/2019/en/123881) ("Guide du HCR" (réédité en 2019), paragraphes 37-96 (c.-à-d. pages 19-26)).

Une première occurrence du terme date de 1675-85, de la langue française : réfugié, désigner les huguenots qui fuient la France avec un caractère très sélectif (<cite>Sylvie Aprile</cite>, <cite>Karen Akoka</cite>, 2024[^note-cairn-dossier].)

Le terme et dénomination « refugié⋅es » existent sous cette étiquette par le fait d’institutions − *The 1951 Convention relating to the Status of Refugees and its 1967 Protocol*−, souvent constituées de personnes dominantes socialement et favorisées qui n’ont pas vécu la « migration forcée », qui produisent des normes permettant de mesurer des *états* et de les ordonner, de les traiter.

Cette catégorisation qualificative ouvre l’accès à des droits pour les personnes qui y sont « conformes aux critères établis » − les choix et de critères et les procédures étant le fait d’entités indirectement impactées, je le rappelle.

Dans la définition du dictionnaire anglais « Réfugié : “Personne qui a été forcée de quitter son pays pour échapper à la guerre, à la persécution ou à une catastrophe naturelle” » (c’est supposé représenté un usage de « réfugié⋅es » dans un « langage courant en anglais »).

Différentes sources d’information européennes ont décrit les Syrien⋅nes fuyant leur pays pour se rendre dans l’UE en des termes très différents.

Pour certains, les Syrien⋅nes sont des réfugié⋅es. Pour d’autres, il s’agissait de migrant⋅es.

> Le mot refugees, dans la terminologie anglaise, désigne essentiellement les protestants chassés du royaume de France à la fin du XVIie siècle ; en allemand les concepts de Heimatlos et Staatenlos sont appliqués à partir de 1870 à certaines catégories d’apatrides, mais le mot Flüchtling, dont le sens premier est « fugitif » ou « fuyard », ne sera utilisé pour désigner les réfugiés qu’après la Première Guerre mondiale. Quant au terme « exilé », dépourvu de toute dimension juridique, sa connotation évoque plutôt la figure classique du réfugié d’opinion ou de conviction.
>
> <cite>Danièle Lochak</cite>, 2013[^note-lochack].

## Pourquoi certains médias ont-ils appelé les Syrien⋅nes arrivant en Europe "réfugié⋅es" alors que d’autres utilisaient le terme "migrant⋅es" ?

La confusion terminologique a atteint un tel niveau que certains médias ont demandé des conseils sur la question. L’Agence des Nations unies pour les réfugiés (HCR) a ainsi publié une intervention intitulée « [Réfugié ou migrant](https://www.unhcr.org/news/stories/unhcr-viewpoint-refugee-or-migrant-which-right) ».

Nous pouvons mettre en évidence quelques critères (<cite>Xavier Créac’h</cite>, 2002[^note-creach]) servant à l’interprétation du status affublé aux personnes déplacées :
+ L’interprétation des motifs de persécutions
+ L’interprétation du groupe social
+ L’interprétation de leurs opinions politiques
+ L’attribution de critères aux autaires de persécutions

J’ajoute à ces 4 critères :
+ L’interprétation d’une cause dite « naturelle » imputée à une catastrophe (*Aucune catastrophe n’est naturelle*)

Le thème des personnes « étrangères » est très présent dans les médias européens, le sujet des migrations aussi. Or ce ne sont presque jamais les personnes concernées qui y ont la parole (Seo, S., & Kavakli, S. B. (2022)). La migration est l’une des questions les plus pressantes et les plus conflictuelles de la politique mondiale actuelle, et les médias jouent un rôle crucial dans la manière dont les communautés la comprennent et y répondent. Ceci est fait avec variations significatives dans la manière dont les demandeurs d’asile sont présentés, y compris dans la terminologie utilisée et les sujets auxquels ils sont associés (Cooper, G., Blumell, L., & Bunce, M. (2021)).

D’après une étude an analyse comparative de la couverture dans 17 pays (2015-2018), en Europe, en Russie et aux États-Unis (Kreutler, M., Fengler, S., & al. (2022)):

+ Les migrant⋅es et les réfugié⋅es sont principalement décrit⋅es comme des groupes plutôt que comme des individus. Les hommes sont surreprésentés dans la couverture médiatique des migrants et des réfugiés, tandis qu’une proportion relativement faible de migrants et de réfugiés est citée dans les articles.
+ D’un pays à l’autre, la couverture médiatique des migrant⋅es et des réfugié⋅es a généralement augmenté et diminué simultanément. Toutefois, il existe une différence frappante dans l’intensité de la couverture entre les pays, les médias de certains pays se concentrant beaucoup plus sur ces sujets que ceux d’autres pays au cours de la même période.
+ Il a été observé également une nette distinction dans la manière dont les différents médias de chaque pays présentent les migrant⋅es et les réfugié⋅es dans leur couverture, ainsi que des tendances géographiques dans cette couverture.

Dans le discours journalistique, les concepts de migrant⋅e et de réfugié⋅e se chevauchent souvent, ce qui met en évidence l’absence de conceptualisation précise du phénomène. Le terme de migrant est plus couramment utilisé que celui de réfugié⋅e, avec des variations de prévalence dans le temps et selon les régions. Des connotations géopolitiques sont attribuées à ces termes, le discours sur les réfugiés étant surtout présent dans des cas comme l’invasion russe en Ukraine, tandis que le discours sur les migrations se concentre sur les migrations arabes/africaines et latino-américaines. (Mance, B., & Splichal, S. (2024)).

Aux USA les journaux ont davantage tendance à humaniser les réfugié⋅es ukrainiens que les réfugié⋅es de Syrie, et à présenter les réfugié⋅es de Syrie comme des menaces et des agresseurs (el-Nawawy, M., & Elmasry, M. H. (2024)).

Les médias ont une orientation politique qui peut être identifiée dans le spectre des courants de « droite » à « gauche » avec un « centre ». En Europe, différents médias ont couvert les déplacements des personnes venues de Syrie, avec des différences dans les qualificatifs attribués à ces groupes sociaux : chrétien⋅nes de Syrie ou musulman⋅es de Syrie, par exemple. En fonction des affinités de bords politique des médias, le traitement fait du régime Étatique syriens et des persécutions sont aussi variables. Ces variations sont répercutées sur les termes utilisés pour désigner les personnes venues de Syrie arrivant en Europe. Il est aussi présent dans le traitement médiatique que les personnes venues de Syrie sont connotées en tant qu’arabes.

En rattachant cela à l’Histoire de l’usage du terme « refugié⋅e » dans l’Europe, comme je l’ai évoqué, il est évident que ce traitement médiatique a pour conséquence un accueil par la population hôte, faite de diversités d’opinions et d’ancrages politiques, qui reflète les choix de termes employés pour désigner un groupe.
Ce traitement médiatique à aussi pour conséquences des inégalités dans les initiatives diverses de défense des droits par personnes arrivantes.

### L’étiquette médiatique « réfugié⋅e » suggère-t-elle qu’une personne mérite davantage l’aide de la société d’accueil qu’un⋅e « migrant⋅e » ?

Statutairement, juridiquement et administrativement, le qualificatif de réfugié⋅es, exilé⋅es, demandaires d’asile, apportent des droits et des protections différentes.

Pour l’Organisation des Nations Unies :

> Les migrants choisissent de quitter leur pays non pas en raison d’une menace directe de persécution ou de mort, mais surtout afin d’améliorer leur vie en trouvant du travail, et dans certains cas, pour des motifs d’éducation, de regroupement familial ou pour d’autres raisons. Contrairement aux réfugiés qui ne peuvent retourner à la maison en toute sécurité, les migrants ne font pas face à de tels obstacles en cas de retour. S’ils choisissent de rentrer chez eux, ils continueront de recevoir la protection de leur gouvernement.
>
> <cite>UNHCR</cite> viewpoint: ’Refugee’ or ’migrant’ – Which is right?

Tous les pays de l’EU ont ratifié la Convention du 28 juillet 1951 relative au statut des réfugié⋅es.

La couverture médiatique peut jouer un rôle crucial dans la formation de l’opinion publique et l’élaboration des politiques, cela est fait depuis des variables géopolitiques, historiques et culturelles dans le processus d’élaboration des politiques publiques. De ce point de vue une influence peut être identifiée et mise en pratique. (Matías Ibañez Sales (2023)).

Le traitement médiatique alloué aux groupes de personnes dites « refugié⋅es » pousse la population d’accueil à une empathie plus forte et une hospitalité plus développée par rapport aux groupes médiatiquement traités en « migrant⋅es ». C’est avec ce *jeu* de traitement médiatique que les institutions et les élus politiques composent pour proposer et faire appliquer des politiques publiques.

La loi belge sur les étrangers couvre tous les migrants, y compris les bénéficiaires d’une protection internationale[^note-belgium]. En 2023, Parlement Français adopte définitivement le projet de loi immigration contenant des propositions formulées par l’extrême droite depuis 40 ans, et avec le vote des partis d’extrême droite. Couplée à la programmation de la politique dite de « dématérialisation », notamment des démarches administratives, en résulte une augmentation des étapes et des difficultés pour faire reconnaître un status pour les personnes arrivantes, des critères plus restrictifs pour les status offrant le plus de droits et de protections, une augmentation des Obligations de Quitter le Territoire (OQTF), des enferments administratifs. Les personnes dites « migrant⋅es » étant, en France, les plus vulnérables et les plus victimes de ces conséquences[^note-mediapart].

Or, en France comme en Belgique (2 systèmes d’organisations différents), l’État et ses administrations s’appuient sur les lois et directives en vigueur pour déléguer le traitement et l’accueil des personnes arrivantes (exilé⋅es, refugié⋅es, migrant⋅es, demandaires d’asile) à des parties tierces :ONG Internationales, associations nationales et locales, entreprises, collectifs civiques, individus.

Pour pallier le désengagement des pouvoirs publics et la solitude provoquée par l’individualisation des modes de vie, pour rendre simplement possible le passage à l’action sur le plan individuel ou familial, il fallait un cadre fonctionnel. Un cadre qui mette en commun les expériences, dispatche et sécurise[^ref-cd-cahier9_web-18.pdf].

## Le cas des différences de traitement à Bruxelles

Bruxelles est l’un des principaux centres mondiaux de décisions internationales et aussi une capitale d’installation de personnes déplacées. J’insiste sur le fait que le plus grand nombre des personnes qui fuient leur pays d’origine seront accueillies dans les pays limitrophes et voisins.

Que l’on soit déplacé⋅e par une guerre directe, un conflit armé, ou un changement climatique, on trouvera toujours des facteurs communs et dans ces lignes factorielles un point d’origine socio-politico-géo-historique. Cependant, deux groupes différents de populations déplacées, par la guerre si l’on prend un exemple catégorique qui fait des réductions menant à des interprétations erronées depuis 2 points distincts du globe, seront acceptés et surtout refusés ou réorientés très différemment dans une époque politique contemporaine.

Belgique : émigrants, exilés, réfugiés, jugés plus légitimes que d’autres... Une étude sociologique s’est penchée[^note-1] sur deux situations similaires et pourtant très différentes, celle de l’accueil d’exilés en transit (de Somalie ou d’Érythrée à l’époque) soutenus par la Plateforme citoyenne (initiative paritaire belge) entre 2016 et 2019, et celle de 2022 lors de l’arrivée massive d’Ukrainien⋅nes fuyant leur pays suite à son invasion par les troupes russes.

L’hébergement des personnes venues d’Ukraine est décrit en termes de logique commerciale, d’organisation verticale et de sous-traitance. Qu’est-ce que cela implique en termes de politique d’accueil et d’hébergement par des citoyen⋅nes ? Et peut-on encore parler d’hébergement citoyen ?

> En Belgique, vous avez : la Plateforme citoyenne (dédiée aux personnes issues de conflits militaires dans les pays de l’UE et les pays alliés proches de la frontière) ; un opérateur public : Fedasil ; un opérateur privé non marchand : la Croix-Rouge ; et un opérateur privé marchand : G4S. Chaque opérateur agit différemment selon le « type » de réfugié, selon le « type » de mission financée. La différence réside dans la gestion publique. Quand on parle d’externalisation, on parle d’une question politique. Les dirigeants d’aujourd’hui prennent des décisions de gestion mais ne les confient pas à des organismes ou à des services publics. Au contraire, ils font appel au marché pour solliciter des acteurs privés. Et dans le cas de l’immigration, on peut considérer qu’il y a de plus en plus de sous-traitance. Une forme de sous-traitance des politiques publiques à des organisations non gouvernementales ou à des opérateurs privés du marché à qui l’on demande de mettre en œuvre des décisions politiques.
>
> C’est pourquoi nous utilisons tous ces termes en relation avec le marché, ils sont liés aujourd’hui au fait que les politiques publiques ne sont pas mises en œuvre par les services publics (qui ont été démantelés), mais par des opérateurs privés dans les secteurs marchands et non marchands. Et il est important de voir la différence entre les deux. Caritas Catholica n’est pas un opérateur commercial, mais fait partie des opérateurs de retour volontaire, alors que G4S est un opérateur commercial qui gère un centre d’accueil, par exemple. En Belgique, les opérateurs d’accueil des demandeurs d’asile comprennent un opérateur public, Fedasil, un opérateur privé non marchand, la Croix-Rouge, et un opérateur privé marchand, G4S.
>
> <cite>Andrea Rea</cite>, 2024. *Propos recueillis par* <cite>July Robert</cite>

Nous avons des biais de jugement. Établir des catégories mène à des simplifications ouvrant la pente à des erreurs et des incompréhensions.
Les personnes arrivantes demandant aides et assistances n’ont pas de paroles dans les médias européens. Ces personnes ne sont pas reconnues comme légitimes à participer à leurs propres catégorisation, ni aux droits conséquents.

Les institutions d’États et administrations fonctionnement avec une mise en scène de crises pour justifier une fiction de stabilité(s) au travers de productions de règlements[^note-colloque]. Les institutions médiatiques et étatiques s’inter-influencent.

Il nous est raconté des histoires sur le progrès, le développement, l’humanisme, l’hospitalité et la façon dont nous construisons une société.
Cette société fonctionne avec des agencements et des manières de se raconter qui effacent les coûts de l’Histoire. Nous travaillons avec des "ignorances" et des désinformations qui sont récompensées (<cite>V. Andreotti</cite>, 2024). Depuis la question de confusion terminologique entre « refugié⋅e » et « migrant⋅e » et les différences de traitement réservés à différentes personnes, nous pouvons voir des dénis ancrés que nous reproduisons dans les biais de l’éducation et de l’information.


## Bibliographie

+ Andreotti Vanessa on “Hospicing Modernity and Rehabilitating Humanity”, <https://www.thegreatsimplification.com/episode/125-vanessa-andreotti> 2024

+ Cooper, G., Blumell, L., & Bunce, M. (2021). Beyond the ‘refugee crisis’: How the UK news media represent asylum seekers across national boundaries. International Communication Gazette, 83(3), 195-216. https://doi.org/10.1177/1748048520913230

+ el-Nawawy, M., & Elmasry, M. H. (2024). Worthy and Unworthy Refugees: Framing the Ukrainian and Syrian Refugee Crises in Elite American Newspapers. Journalism Practice, 1–21. https://doi.org/10.1080/17512786.2024.2308527

+ Kreutler, M., Fengler, S., Asadi, N., Bodrunova, S., Budivska, H., Diop, L., Ertz, G., Gigola, D., Katus, E., Kovacs, D., Kuś, M., Láb, F., Litvinenko, A., Mack, J., Maier, S., Pinto Martinho, A., Matei, A., Miller, K. C., Oppermann, L., Pérez Vara, E., Polyák, G., Ravisankar, R., Rodríguez Pérez, C., Semova, D. J., Skleparis, D., Splendore, S., Štefaniková, S., Szynol, A., Telo, D., & Zguri, R. (2022). Migration Coverage in Europe, Russia and the United States: A Comparative Analysis of Coverage in 17 Countries (2015-2018). Central European Journal of Communication, 15 (2(31)), 202-226. https://doi.org/10.51480/1899-5101.15.2(31).2

+ Mance, B., & Splichal, S. (2024). Refugees and (Im)Migrants: (Re)Conceptualizing and (Re)Contextualizing Migration in the Media. Journal of Immigrant & Refugee Studies, 1–18. https://doi.org/10.1080/15562948.2024.2324305

+ Matías Ibáñez Salas (2023), The Refugee Crisis’ Double Standards: Media Framing and the Proliferation of Positive and Negative Narratives During the Ukrainian and Syrian Crises. IEMed - European Institute of the Mediterranean

+ Seo, S., & Kavakli, S. B. (2022). Media representations of refugees, asylum seekers and immigrants: a meta-analysis of research. Annals of the International Communication Association, 46(3), 159–173. https://doi.org/10.1080/23808985.2022.2096663

## Notes et références

[^ref-cd-cahier9_web-18.pdf]: *Habiter l’exil*, Cahiers de Culture et Démocratie (Belgique). Actes de la rencontre
du 11 octobre 2019 <https://www.cultureetdemocratie.be/uploads/2020/11/CD_CAHIER9_web-18.pdf>

[^note-lochack]: Lochak, D. (2013). Qu’est-ce qu’un réfugié ? La construction politique d’une catégorie juridique. Pouvoirs, 144, 33-47. <https://doi.org/10.3917/pouv.144.0033>

[^note-cairn-dossier]: *Réfugiés ou migrants ? Les leçons de l’Histoire*, Dossier Cairn. Mis en ligne sur Cairn.info le 05/01/2024

[^note-creach]: Créac’h Xavier. *Les évolutions dans l’interprétation du terme réfugié*. In: Hommes et Migrations, n°1238, Juillet-août 2002. Les frontières du droit d’asile. pp. 65-74.

[^note-belgium]: Governance of migrant integration in Belgium. [Last update published: January 2024]. European Website on Integration <https://migrant-integration.ec.europa.eu/country-governance/governance-migrant-integration-belgium_en>

[^note-mediapart]: *Loi immigration : « une fabrique à sans-papiers »*, Mediapart, *in* À l’air libre. 2023

[note-1]: Entretien avec Andrea Rea , « Il y a des exilé·es jugé·es plus légitimes que d’autres… », Propos recueillis par [July Robert](https://www.agirparlaculture.be/author/july-robert). 2024 <https://www.agirparlaculture.be/il-y-a-des-exile%c2%b7es-juge%c2%b7es-plus-legitimes-que-dautres-andrea-rea/>

[^note-colloque]: Notes depuis le colloque : À l’épreuve des tempêtes. Institutions et crises : approches historiques <https://xavcc.frama.io/notes-colloque-institutions-crises-recherche-histoire/>
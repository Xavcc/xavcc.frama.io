---
title: "Mardi catastrophologie : votre fournée de ressources"
layout: post
date: 2023-03-14 07:40
image: /assets/images/refugees-welcome-bxl-2023.jpg
headerImage: true
tag:
- hsociety
- Catastrophologie
category: blog
author: XavierCoadic
description: 
---

+ [Podcast] Du pied gauche « **De l'eau ! La gestion de ce bien commun, à Bruxelles et dans le monde !** » <https://www.radiopanik.org/emissions/du-pied-gauche/de-leau/>

+ [Podcast] « **Humanitaire d'urgence ou "pornographie sentimentale"?** » (2015) Radio Panik, émission Du Pied Gauche <https://www.radiopanik.org/emissions/du-pied-gauche/humanitaire-d-urgence-ou-pornographie-sentimentale-/>

+ [BD et témoignages] Chez Medor_mag : les inondations en Belgique, le statut et l'état de catastrophe et après cette norme de classification.
  + Belgique : **Chênée, ou la boîte à souvenirs vide. Cellule psy sur territoire inondé**
> 14 juillet 2021. Comme partout ailleurs dans la vallée de la Vesdre, les habitants de Chênée rentrent en mode survie. Les inondations ont tout dévasté. 
Une cellule psy mobile constituée de bénévoles voit le jour dans l’enceinte du centre culturel local. 
Ancien éducateur spécialisé et professeur à l’Académie des Beaux-Arts, <cite>Michel Vandam</cite> les rejoint. Il esquisse, dans cette bande-dessinée, ce qu’il a observé durant une quinzaine de mois 
<https://medor.coop/magazines/medor-n30-hiver-2023-2024/chenee-ou-la-boite-a-souvenirs-vide-inondations-traumatisme/>

Autre ressource
<https://xavcc.frama.io/la-catastrophe-sandrine-revet-2006/>

+ [Podcats] Numérique et **catastrophes « naturelles » qui provoque une rupture dans les infrastructures de communication**,
<cite>Benjamin Bayart et Gaël Musquet</cite> avec
Avec un très gros enclin à justifier l'absolu nécessité de « remettre un réseau de telco » par l'argument guider les secours et sauver des vies (et le besoin des interventions exogènes pour former et/ou sauver les populations endogènes)
  + Numériques essentiels ep. 1. Cool et bon tout de même, avec tactique et stratégie politique
<https://w.soundcloud.com/player/?url=https%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F1448903323>

+ [Podcast] « **Il faut mesurer la déchéance dont l’eau a été victime** » <cite>Olivier Rey</cite>

> Il n'y a pas que la pollution qui détériore l'eau ; Olivier Rey, mathématicien et philosophe nous démontre comment l'eau a été purgée au fil du temps de tout imaginaire et toute symbolique. <https://www.radiofrance.fr/franceculture/podcasts/affaire-en-cours/il-faut-mesurer-la-decheance-dont-l-eau-a-ete-victime-3525881>

+ [Archive] **À l’épreuve de l’eau** (Bruxelles, 2020). Dossier coordonné par <cite>Chloé Deligne</cite>.
  + Abondance de pluies, rareté de l’eau
  + L’aridité des communs : visages de la vulnérabilité hydrique
  + Ce droit tombé à l’eau ?
  + Bruxelles-les-bains ?
  + Forest : eaux (dé)couvertes
  + Forest Nord : quoi de neuf sous le soleil ?
  + L’eau liquide

<https://web.archive.org/web/20201129073012/https://ieb.be/spip.php?page=impression&id_article=43862>

+ [Colloque] **À l’épreuve des tempêtes. Institutions et crises : approches historiques**, Rennes, 16-17 mars 2023. <https://ahmuf.hypotheses.org/11299>. Notamment :
  + <cite>Solène Minier</cite> : les conséquences inattendues des terribles épidémies de peste qui ravagent l’Italie médiévale à partir de 1348 pour expliquer comment les nouveaux legs religieux apparus pendant les pestes permettent au XVe siècle à des aristocrates privés de pouvoir politique de réaffirmer leur domination sociale
  + <cite>Elias Burgel</cite>, HISTEMÉ (Université de Caen-Normandie) : « Des crises dans la gestion des ressources halieutiques. Les “malaigues” de l’étang de Manuguio (bas Languedoc, XVIIIe siècle) ».
  + <cite>Lucile Chaput</cite>, Tempora (Université Rennes 2) : « Les camps d’internement canadiens durant la Seconde Guerre mondiale : entre mise en ordre et remise en cause ».

<br />

+ **Catastrophologie & décolonialité. Le film Assistance Mortelle** <cite>Raoul Peck</cite> (2013)

> il traite du rôle des puissances étrangères dans le pillage d'Haïti, sous couvert d'aides humanitaires, filme depuis l'immédiat après le tremblement de terre « cataclysme / catastrophe » de 2010 puis il tire les fils d’angles saillants beaucoup plus loin.

R. Peck est  réalisateur, scénariste, producteur de cinéma et homme politique haïtien. 

Film coup de poing 
<https://journals.openedition.org/com/10479>

Quintessence et investigation
<https://www.critikat.com/panorama/festival/berlinale-2013/assistance-mortelle/>

+ **Catastrophologie et histoire : En 1976 une usine à Seveso, Italie (commune et cours d'eau),  est le siège d'une « catastrophe »**

<https://fr.wikipedia.org/wiki/Catastrophe_de_Seveso>

« Un énorme nuage de fumée de TCDD a couvert le ciel sur une distance allant jusqu’à une quinzaine de kilomètres dans une région habitée par plus de 37 000 habitants »

<https://fr.wikipedia.org/wiki/2,3,7,8-T%C3%A9trachlorodibenzo-p-dioxine>

ce 2,3,7,8-Tétrachlorodibenzo-p-dioxine (TCDD) est resté longtemps un point de « non détection » car ce produit n'était pas dans la liste de ceux à monitorer, logique et norme et épistémologie et administration encore et toujours. (An epidemiological study of Seveso 20 years after the incident showed that elevated 2,3,7,8-TCDD)

Un évènement que se lie avec le nord du Japon, maladie de Yushō, empoissonnement en 1968, de l'huile de son de riz de la société Kanemi a été contaminée par des PCB et des polychlorodibenzofuranes (PCDF) ; puis  Taiwan en 1979
<https://www.sciencedirect.com/science/article/abs/pii/S0045653515003045>

et aussi l'usage de l'agent orange comme arme de guerre : L'agent orange est constitué de 2,4,5-T et de 2,4-D leur mélange peut réagir et produire de la TCDD hautement toxique. 
<https://fr.wikipedia.org/wiki/2,3,7,8-T%C3%A9trachlorodibenzo-p-dioxine>

+ [Publi] **Le désastre comme événement fondateur**.  Même les épisodes exceptionnels tels que les pillages ou les actes héroïques sont les révélateurs des structures sociales du pays, à la fois fortement inégalitaires et articulées autour de la figure mythique du sauveur.

Le désastre est aussi un événement exceptionnel et fondateur. Il provoque, notamment autour des interventions de secours ou d’assistance, des situations gérées à partir de la pensée de l’urgence qui fait de la référence à la situation d’exception son régime d’action. »

<https://books.openedition.org/psn/1196>

+ [Publi] **Face à la catastrophe : avec ou contre l’État ?** Écologie & politique 2016/2 (N° 53) (Pages : 214. Éditeur : Éditions Le Bord de l’eau) <https://www.cairn.info/numero.php?ID_REVUE=ECOPO1&ID_NUMPUBLIE=ECOPO1_053>

+  [Ref] **Community-Based Disaster Risk Reduction**
<https://oxfordre.com/naturalhazardscience/view/10.1093/acrefore/9780199389407.001.0001/acrefore-9780199389407-e-47>

The Sendai Framework for Disaster Risk Reduction (SFDRR) was adopted by the member states for the period of 2015–2030. SFDRR has seven specific goals:

1. Reduce global disaster mortality
2. Reduce number of affected people
3. Reduce direct disaster economic loss
4. Reduce disaster damage to critical infrastructures
5. Increase number of countries with DRR strategies
6. Enhance international cooperation
7. Increase access to multihazard EWS, risk information, and assessment

+ [Publi] Un retour sur la catastrophe. « **Nouveau regard, nouvel objet pour l’anthropologue.** » <cite>Gaëlle Clavandier</cite> (2009) <https://journals.openedition.org/leportique/2073>
  + un livre important et majeur, publié en 1983, qui marque un virage important dans l'écologie et surtout dans les manières d'appréhender la « catastrophe », je tombe sur :

> Dans le cadre d’un échantillon de sociétés de la Papouasie, Nouvelle Guinée, Morren a démontré (1977) que l’intensification de la culture de la terre s’est accrue avec le déclenchement de l’élevage porcin ; bien que ce dernier compromette l’autonomie politique locale, l’égalité sociale, l’autarcie économique jusqu’à l’équilibre écologique de longue durée, il renforce l’autorité des « Big Men » au centre de l’organisation

Essai dans le cadre du Néolithique ancien méditerranéen pour extraire de sa coquille le « facteur social » <cite>James Lewthwaite</cite> (1987), dans  « Premières communautés paysannes en Méditerranée occidentale » <cite>Jean Guilaine,  Jean Courtin,  Jean-Louis Roudil, et al</cite>.
<https://books.openedition.org/editionscnrs/1199>

+ [Podcast] Rencontre avec <cite>Jake Rom D. Cadag</cite>, un géographe maître de conférences à l’Université des Philippines Diliman,  Avec un concept qu’il connaît bien, celui de '**capacités**'. 

Ça parle de l’émergence de ce concept et de son utilisation par les chercheuses et chercheurs, mais aussi par les praticiens et les ONGs. 

Comment lui, dans son travail, essaie de prendre en compte et de valoriser les capacités locales et traditionnelles
<https://lenversdescatas.podbean.com/e/episode-4-jake-rom-cadag-capacites/>

L’Envers des Catastrophes est la version française du podcast Disasters: Deconstructed. C’est un podcast qui, à partir de plusieurs disciplines et perspectives, entend réfléchir aux causes profondes des catastrophes afin de mieux comprendre les moyens, approches, et enjeux visant à leur réduction. Dans chaque épisode, une personne invitée francophone qui développe des idées, critiques, analyses ou initiatives qui nous permettent d’explorer les concepts clés, ainsi que les enjeux sociaux, économiques, culturels, idéologiques, ou politiques liés aux catastrophes dites « naturelles ». Bien que tous francophones, nos invités viennent de tous les continents.

+ **Drinking in inequality: the fight against lead contamination in Lahore, Pakistan**

Blackened teeth and gum issues were common and women frequently reported miscarriages and stillbirths <https://unbiasthenews.org/fight-against-lead-poisoning-in-lahore-pakistan/>

+ **Ma commune face aux risques d'inondations**

Utilisation de données en opendata pour une simulation de potentiels risques

Scénario pédagogique et documentation de la réalisation, classe de seconde, par l'Académie de Nantes appuyée par l'IGN
<https://www.pedagogie.ac-nantes.fr/histoire-geographie-citoyennete/ma-commune-face-aux-risques-d-inondations-1427643.kjsp?RH=1402406814861>

+ [BD]  Petit guide local, équitable et lombricompostable de l'effondrement - "Semez dès aujourd'hui les graines du monde d'après !" - Émile Bertier, Yann Girard (2021)

+ [Livre] « Histoire politique du barbelé » d'<cite>Olivier Razac</cite>, surtout l'édition de 2009

> « Le barbelé, lui-même « mur virtualisé », a ainsi ouvert la voie à des dispositifs de contrôle de plus en plus immatériels, dont la vidéosurveillance et le bracelet électronique sont les derniers avatars… »

les meilleurs outils d'exercice du pouvoir sont ceux qui dépensent le moins d'énergie possible pour produire le plus d'effets de domination.

<https://www.monde-diplomatique.fr/2001/01/PIRONET/7756>

<https://editions.flammarion.com/histoire-politique-du-barbele/9782081217010>

+ [Actu politique] France (2023) : **Projet de loi d'accélération du nucléaire** dans lequel devrait se trouver l'amendement actant la fusion de l'IRSN au sein de l'ASN, donc disparition de l'indépendance de l'iRSN
<https://www.dna.fr/economie/2023/02/21/surete-nucleaire-pourquoi-la-suppression-de-l-irsn-inquiete>

+ [Question] **Le Bassin du delta de la Seine dans lequel est Rouen, et d'autres, dans le Water Risk Atlas** est considéré comme à risque faible (multiples risques calculés (pollution, réchauffement climatique, montée des océans, etc)  concernant l'eau
<https://www.wri.org/applications/aqueduct/water-risk-atlas/#/?advanced=false&basemap=hydro&indicator=w_awr_def_tot_cat&lat=49.14219020768315&lng=-358.6898803710938&mapMode=view&month=1&opacity=0.5&ponderation=DEF&predefined=false&projection=absolute&scenario=optimistic&scope=baseline&timeScale=annual&year=baseline&zoom=9>

Et dans le Global Disaster Alerte and Coordination Syteme (service EU de visualisation des « catastrophe » (selon des critères d'éligibilité spécifiques) en ”remps réel”, la zone Rouen − Amiens − Paris nord est actuellement en sécheresse sévère
<https://www.gdacs.org/>

+ [Actu] **« Châteauroux: l'eau de la ville polluée et inconsommable en pleine canicule »** (2022)

Ou plus exactement le réseau d'eau courante est contaminé à l' escherichia coli et 25 000 personnes sont privées d'eau courante en pleine canicule. Dans les quartiers Nord.
<https://sig.ville.gouv.fr/Territoire/243600327>
Sur un total de 44 000 âmes pour la ville.
Voix officielle : suspicion de "dysfonctionnement" dans le système de traitement de l'eau au chlore.

Ajoutant du malheur à leurs détresses et précarité, la ville a déclenché son plan communal de sauvegarde et mis en place, avec le gestionnaire du réseau d'eau Saur, quatre centres de distribution de packs d'eau en bouteilles.
Un dispositif bien peu adapté et peu efficace.
En chiffre, 125 000 bouteilles ont été distribuées pour le vendredi (été 2022).

<https://www.linfodurable.fr/chateauroux-leau-de-la-ville-polluee-et-inconsommable-en-pleine-canicule-32758>

+ [Actu] (2022) **Eaux et Ille-et-Vilaine**

> Les rejets d'une champignonnière  labellisée bio, de l’entreprise Légulice pour sa marque Lou, continuent de polluer une rivière, à Poilley, près de Fougères (35). La préfecture connaît le problème depuis au moins deux ans. Eau et rivières dénonce du laxisme et annonce le dépôt d'une plainte.
>
> Le traitement des effluents de la culture se déversent dans la Guerge, un affluent du Couesnon
>
> Le problème, c’est que cette pollution dure depuis plusieurs années et que ni l’entreprise située en amont ni l’agglomération ne semblent décidées à y mettre un terme. La préfecture ? Elle a connaissance du dysfonctionnement depuis 2020 mais n’a pas engagé de procédure. <https://www.20minutes.fr/rennes/3312019-20220620-ille-vilaine-champignonniere-bio-accusee-polluer-cours-eau-gerge>

+ [Actu] **Louisiana: Disasters on the Horizon. The Colonial Roots of Climate Crises—and a Path toward Resilience** (2021-09-02) <https://crimethinc.com/2021/09/02/louisiana-disasters-on-the-horizon-the-colonial-roots-of-climate-crises-and-a-path-toward-resilience>

+ [Actu] **A joint statement from anarchists in the region about the earthquakes that hit Turkey and Syria**

<https://www.yeryuzupostasi.org/2023/02/07/joint-declaration-from-anarchists-lets-build-solidarity-and-the-struggle-against-this-order%ef%bf%bc/>

> Disaster aid is not reaching northern Syria as a consequence of the hostility of the Assad regime and the neighboring governments.

<https://www.heyvasor.com/en/banga-alikariya-lezgin-ji-bo-mexduren-erdheje/>

<https://molhamteam.com/en/campaigns/439>

> "In Istanbul, a planned rally by the Crisis Coordination of the Alliance for Labour, Peace and Democracy against the Turkish government's response to the earthquake disaster was violently prevented by police."
<https://anfenglish.com/news/istanbul-police-attack-demonstration-against-government-s-response-to-earthquake-65734>
>
> It took 15 hours for the AFAD (Turkey's equivalent of USA's FEMA) to arrive at all to the hit location, but very many still testified weeks later that they hadn't even seen ANY response from the State, whilst aid volunteers documented being violently obstructed and their aid stolen by the State which then didn't give it to the devastated people, etc, etc. Meanwhile Turkey-occupied NW Syria refused letting in humanitarian aid convoys from Doctors Without Borders, the UN and Rojava for the 13 first days. That government "response".

+ **Ouverture et mise à disposition d'un espace de groupe dans [Zotero nommé « Catastrophologie »](https://www.zotero.org/groups/4989006/catastrophologie)** au 14 mars 2023. Libre accès et édition, devenir membre se fait sur demande. Actuellement déposés dedans : **Tiers-Lieux et situation critique / configuration sociale particulière et catastrophes**, 56 items au 14 mars 2023.

<div class="breaker"></div>


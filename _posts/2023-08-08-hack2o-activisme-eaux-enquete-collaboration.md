---
title: "Hack2o : Eaux, enquêtes, activismes, collaborations, documentations"
layout: post
date: 2023-08-08 06:40
image: /assets/images/capture-map-hack2o-aout-2023.png
headerImage: true
tag:
- hsociety
- bioassay
- Catastrophologie
category: blog
author: XavierCoadic
description: Hacking, luttes, activismes, libertés et droits
---

**Hack2o** et <https://hack2o.eu> est un projet que je mène porté avec *[petites singularités](https://ps.lesoiseaux.io/)*. Un projet démarrant après 2 années de *[hack2Eaux](https://wiki.kaouenn-noz.fr/hors_les_murs:hack2eaux)* avec le Biohackperspace Kaouenn Noz, et aussi 4 années avec [Exposing The Invisible](https://exposingtheinvisible.org). Les explications à ce jour, dans leur état de prototype, sont [disponibles ici](https://ps.zoethical.org/t/hack2o-intro-explication/6116) en français et en anglais.

Il est en cohérence et structurant avec les recherches sur les [bioessais](https://xavcc.frama.io/tags/#bioassay), l'[étude des « catastrophes »](https://xavcc.frama.io/tags/#catastrophologie) et l'initiative « [Tiers-lieux en situation critique / Third Places in Critical Situation](/about/#ce-que-je-fais) ».

> C'est un espace web gratuit sous logiciel et licences libres qui fournit des services communautaires et collaboratifs à des groupes et lieux (de ses communautés) qui œuvrent dans les enjeux de l'eau et de la justice ; avec un inventaire cartographié des initiatives militantes, et pages de documentations liées, autour de la lutte civique pour l’eau.
>
> Ce que nous portons :
+  **Rendre visible les actions locales menées par les premières personnes concernées**, ainsi que les liens existants entre les différents groupes et activités : C'est la **cartographie**.
+ **Faciliter les rencontres et les discussions** entre individus et communautés à travers le monde. C'est le service de **forum multilingues**.
+ **Un espace de documentation, wiki, ouvert à toutes personnes intéressées** afin de rendre appréhendable les savoirs, les techniques, les stratégies, les méthodes, des différentes communautés locales et d'entretenir ceux-ci **tel un  commun**.

Vous pouvez nous rejoindre sur le forum de discussion [ici](https://ps.zoethical.org/c/engage/hack2o/176), sur Mastodon `ps.s10y.eu/@hack2o` ou m'écrire par courriel.

![](/assets/images/capture-map-hack2o-aout-2023.png)<marginnote> voir aussi « <a href="https://xavcc.frama.io/contre-cartographie-renversements-luttes/">Contre-cartographie, renversement(s) et activismes</a> »</marginnote>
<figcaption class="caption">Lien vers la cartographie : <a href="https://umap.openstreetmap.fr/fr/map/hack2o_823728#3/45.95/-24.96">https://umap.openstreetmap.fr/fr/map/hack2o_823728#3/45.95/-24.96</a>. cette cartographie est une ébauche librement accessible, imparfaite, biaisée, incomplète, destinée à la contribution et à la critique</figcaption>
<br />

> Nous proposons également gratuitement une animation trans-communautaires au travers de rencontres et d'ateliers d’entraînement, initiations, formations, aux enquêtes environnementales sur le sujet de l'eau. Webinaires et rencontres *offline* ouvertes aux cityoyen⋅nes, activistes, investigateur⋅ices, scientifiques, journalistes, comme tout ce qui est fournit dans le projet hack2o.

Les enjeux et problèmes que nous abordons à travers ce projet ne sont pas arrêtés, ni limités, par les frontières des États. Nous concevons nos réflexions et nos productions avec les bassins versants[^water-risk-atlas]. Par exemple, l'Himalaya est sujet à l'érosion et les conséquences sur l'eau et les populations se fait au Népal, en Chine, Pakistan, Inde, Bangladesh. De cette usure du massif provient de la pyrite, un minéral, en concentration importante contenant de l'arsenic qui réagit avec l'oxygène and le fer présent. Se forment alors des agrégats se déposant dans les sédiments et impactant, par relâchement, les eaux et pompages desservant des centaines de millions de personnes[^arsenic]. En ajoutant à cela les pollutions issues des activités industrielles, ce sont toujours les communautés et personnes les plus invisibilisées et les plus opprimées qui sont le plus fortement touchées[^water-pakistan].

[^water-risk-atlas]: Water Risk Atlas of the World Resources Institute: water stress, drought risk and riverine flood risk using a peer-reviewed methodology. The group evaluated the water stress levels of 189 countries and the regions within them (you should also check from Major & Minor basin perspective) <https://www.wri.org/applications/aqueduct/water-risk-atlas/#/?advanced=false&basemap=hydro&indicator=w_awr_def_tot_cat&lat=-10.487811882056683&lng=-174.0234375&mapMode=view&month=1&opacity=0.5&ponderation=DEF&predefined=false&projection=absolute&scenario=optimistic&scope=baseline&timeScale=annual&year=baseline&zoom=2>

[^arsenic]: En Asie, 140 million de personnes boivent de l'eau contaminée à l'arsenic. *DEATH IN THE WATER* Katy Daigle, Scientific American, Vol. 314, No. 1 (JANUARY 2016), pp. 42-51, Published By: Scientific American, a division of Nature America, Inc.

[^water-pakistan]: "Drinking in inequality: the fight against lead contamination in Lahore, Pakistan", Written by Anmol Irfan. Unbias the News, June 2022, <https://unbiasthenews.org/fight-against-lead-poisoning-in-lahore-pakistan/>

C'est dans ces bassins versants, et dans leurs replis, qui dépassent les frontières des administrations, que des luttes sont actives et d'autres à venir. L'impact des activités humaines est omniprésent et hautement impliqué dans la dégradations des eaux (usure et usage des sols et précipitations, changements dus à la fonte des glaciers, la pollution, montée des océans), aussi le réchauffement climatique et les pollutions ne sont que extrêmement rarement prises en comptes dans les représentation du cycle de l'eau à travers le monde[^water-cycle-human].

[^water-cycle-human]: Les humain⋅e⋅s prélèvent plus de la moitié de l’eau qui s’écoule dans les rivières du monde entier, soit 24 000 km3 par an. Cette eau est utilisée principalement dans l’élevage d’animaux pour la consommation de viande, en majeure partie via la mise en culture de céréales et de fourrages pour l’alimentation animale. Une étude dans laquelle a été analysé plus de 450 représentations du cycle de l’eau dans 12 pays différents : 85% de ces schémas ne représentaient aucun des impacts dus aux interventions humaines sur le cycle de l’eau ; seulement 2% montraient l’impact du changement climatique ou de la pollution des eaux. Or ces facteurs sont les deux principales causes de la crise mondiale actuelle concernant nos ressources en eau. Abbott, B.W., Bishop, K., Zarnetske, J.P. *et al.* Human domination of the global water cycle absent from depictions and perceptions. *Nat. Geosci.* **12**, 533–540 (2019). <https://doi.org/10.1038/s41561-019-0374-y>

2, 2 milliard d'humain⋅e⋅s vivent sans un assainissement de l'eau suffisant, 5 millions meurent chaque année de maladies liées à de l'eau polluée, 780 million d'humaines n'ont pas accès à de l'eau propre à la consommation. Aussi, les activités humaines terrestres ont de conséquences sur l'eau et les sols qui viennent ajouter des menaces sur les qualités des eaux océaniques et sur les équilibres écologiques dans celles-ci. La moitié de l'oxygène que nous respirons provient des micro-organismes des océans[^microbes-ocean].

[^microbes-ocean]: Field CB, Behrenfeld MJ, Randerson JT, Falkowski P. Primary production of the biosphere: integrating terrestrial and oceanic components. Science. 1998 Jul 10;281(5374):237-40. doi: 10.1126/science.281.5374.237. PMID: 9657713.

Ces petits organismes vivants sont à l'origine d'une grande partie des transformations de substrat et d'oxydoréduction qui alimentent l'écosystème terrestre[^microbes-ocean-substrat].

[^microbes-ocean-substrat]: Falkowski PG, Fenchel T, Delong EF. The microbial engines that drive Earth's biogeochemical cycles. Science. 2008 May 23;320(5879):1034-9. doi: 10.1126/science.1153213. PMID: 18497287.

## De l'eau

L'eau est un élément vital. Sous forme gazeuse est joue un rôle important dans l’atmosphère (0,25% de la masse totale de l’atmosphère), elle prend part aux régulations climatiques et constituent un facteur important dans les questions de réchauffement[^sea-surface] [^water-constraints], et avec des conséquences dans nos météos quotidiennes.

[^sea-surface]: Olmedo, E., Turiel, A., González-Gambau, V. *et al.* Increasing stratification as observed by satellite sea surface salinity measurements. *Sci Rep* **12**, 6279 (2022). <https://doi.org/10.1038/s41598-022-10265-1>

[^water-constraints]: Allen MR, Ingram WJ. Constraints on future changes in climate and the hydrologic cycle. Nature. 2002 Sep 12;419(6903):224-32. doi: 10.1038/nature01092. Erratum in: Nature. 2012 Sep 27;489(7417):590. PMID: 12226677

Sous forme solide, glaces et neiges, couvrent les pôles du globe et les hautes montagnes. Sous forme liquide elle est nécessaire aux processus d'apparition de la vie (prébiotique)[^prebiotic] sur notre planète puis à la perpétuation de cette vie une fois apparue. Or, de moins de 1 % de l'eau de notre planète est disponible pour l'usage des humain⋅e⋅s[^water-volume] [^water-volume-2]. 

[^prebiotic]: Norio Kitadai, Shigenori Maruyama, *Origins of building blocks of life: A review*, Geoscience Frontiers, Volume 9, Issue 4, 2018, Pages 1117-1153, ISSN 1674-9871, <https://doi.org/10.1016/j.gsf.2017.07.007>.

[^water-volume]: [Peter H. Gleick](https://link.springer.com/book/10.5822/978-1-61091-483-3#author-1-0), The World's Water Volume 8, The Biennial Report on Freshwater Resources, 2014

[^water-volume-2]: Global Aquatic and Atmospheric Environment, Har D. Kumar, Donat-P. Häder, 1999

En seulement quelques dizaines de décennies les activités humaines, notamment industrielles ont porté sur l'eau, dans toutes ces formes disponibles, des conséquences dramatiques pour les équilibres planétaires, pour tous les organismes vivants[^living-loss], pour les conditions de vies humaines. Les substances per- & polyfluoroalkylées (PFAS) [*PFAAs, PFOS, PFHx, PFNA*] sont désormais présentes partout dans les « eaux de pluie », sur toutes les zones habitées du globe, avec des concentrations délétères pour notre santé, constituant ainsi probablement une limite planétaire atteinte, et pouvant être considérées comme un _commun négatif_[^pfas] [^communs-negatifs], retrouvées dans le sang et certains organes des populations ; dans le corps des animaux sauvages et domestiques, y compris dans les glaces des pôles et l'air des plus hautes montagnes. Ceci n'est qu'un seul dramatique exemple parmi beaucoup trop d'autres cas.

[^living-loss]: 
    > "This loss is increased by the effects of global climate change: Extinction risks for some sample areas covering some 20% of the Earth’s terrestrial surface have been estimated as 15%−37% over the next three to four decades"

    *Bioassays advanced methods and application*, Donat-P. Häder and Gilmar S. Erzinger, 2018, in reference to Thomas, C., Cameron, A., Green, R. *et al.* Extinction risk from climate change. *Nature* **427**, 145–148 (2004). <https://doi.org/10.1038/nature02121>

[^pfas]: *Outside the Safe Operating Space of a New Planetary Boundary for Per- and Polyfluoroalkyl Substances (PFAS)*, * Ian T. Cousins***** , * Jana H. Johansson , * Matthew E. Salter, * Bo Sha, and * Martin Scheringer. Environ. Sci. Technol.* 2022, 56, 16, 11172–11179, August 2, 2022, <https://doi.org/10.1021/acs.est.2c02765>

[^communs-negatifs]: 
    > « Les communs désignent généralement des ressources partagées qui ont une utilité pour une communauté qui en prend soin, c’est-à-dire des effets positifs. La notion de « communs négatifs » s’attache aux problèmes soulevés par la gestion de certaines réalités dont les effets sont négatifs notamment dans le domaine environnemental : déchets, centrales nucléaires, mais aussi d’autres éléments dont nous allons hériter à l’avenir et dont il va bien falloir prendre soin. Plus les sociétés capitalistes se développent, plus elles perdent leur capacité à absorber ce qu’elles produisent en excès, reléguant ces déchets dans les zones les plus pauvres ou bien dans les profondeurs de la terre et des océans. À côté de cela, d’autres réalités, inassimilables à des déchets, nous privent de futur, de par leur simple existence. Comment politiser le traitement qu’il convient d’y apporter ? C’est à cette question qu’entend répondre le second pan de la réflexion ouverte par les communs négatifs »

    MONNIN Alexandre, « Les « communs négatifs ». Entre déchets et ruines », Études, 2021/9 (Septembre), p. 59-68. DOI : 10.3917/etu.4285.0059. URL : <https://www.cairn.info/revue-etudes-2021-9-page-59.htm>

Réchauffement climatique et sécheresses, acidification, évolution des rapports eau salée / eau douce, désertification, événements météorologiques extrêmes, pollutions, empoisonnements, accaparement et privatisation de l'eau… Partout à travers le monde des individus et des communautés sont confrontés aux problèmes concrets directement issus de difficultés d'accès à l'eau.

**Partout dans le monde des groupes s'organisent et agissent pour leur survie, leurs dignités, leurs droits et la gestion de l'eau comme un commun.**

## Notes et références
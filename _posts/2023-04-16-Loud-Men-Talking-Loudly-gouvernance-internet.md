---
title: "Loud Men Talking Loudly: Cultures d'exclusion dans la « Tech » et dans la gouvernance de l'Internet"
layout: post
date: 2023-04-16 06:40
image: /assets/images/biohacktivism-2023.png
headerImage: true
tag:
- hsociety
- Anthropologie
category: blog
author: XavierCoadic
description: Anthropologie d'Internet, de l'infrastructure et de l'informatique
---

_(…et jusque dans nos rues)_

Je flânais dans Rennes entre deux expressions de mouvement social<marginnote>Allez voir l'exposition "Des fourmis dans les jambes" au centre culturel le Phakt, qui a fait chouiner le RN et Alliance (Merci Dremmwel an Ankouak). Jusqu'au 17 mai</marginnote>. avec « [Le rêve des machines](https://inventaire.io/items/6b744070509c84a741412350e142f047) » (Günther Anders, ed. Allia, 2023). Des panneaux publicitaires *revisités* par la colère d'un peuple jonchaient les trottoirs, abandonnés de leurs maîtres<marginnote>Rennes :  fin provisoire des écrans publicitaire numérique. « Lassées d’intervenir sur du mobilier qui est de nouveau dégradé la semaine suivante, les équipes & la direction de Clear Channel, ont décidé provisoirement d'arrêter les réparations. » <a href="https://www.francebleu.fr/infos/faits-divers-justice/pas-un-seul-mobilier-urbain-n-a-echappe-a-la-violence-des-casseurs-a-rennes-2223536">France Bleu Info</a> Eric Bouvet, Jeudi 6 avril 2023</marginnote>.

Et là, au détour d'un carrefour, une panoplie criarde envahissait les trottoirs :

![](/assets/images/prenez-la-grosse-tech.png)
<figcaption class="caption">La BPI écrit sur son site web : « 25 milliards d’euros, c’est la somme déployée en 2021 par Bpifrance pour l’industrie, soit environ 50 % de l’ensemble des moyens injectés par la banque dans l’économie. Une action historique et massive qui se poursuit afin de construire l’industrie et la France de demain. En effet, face aux défis d’attractivité, de réindustrialisation et de décarbonation de l’économie, le rapprochement des écosystèmes industriel et de l’innovation constitue un enjeu crucial. Fort de cette conviction, Bpifrance, acteur-clé du soutien des industriels, mobilise un éventail complet de solutions pour renforcer leur solidité financière avec le soutien de France 2030.</figcaption>
<br />

La [culture Bro](https://fr.wikipedia.org/wiki/Culture_bro) se porte en étendard, par la voix de la Banque Publique d'Investissement, dans le cœur des villes et sur les voies publiques d'un pays à feu et à sang. « We ignore these cultural hurdles at our own peril. » (Corinne Cath, 2023)

> « Omnipotence ET ignorance − c'est une combinaison qui est marquante », <cite>Günther Anders</cite>, *Lettre à lettre à* [Francis Powers](https://fr.wikipedia.org/wiki/Francis_Gary_Powers). 6 août 1960, dans *Le rêve des machines*.

M'abritant de ces agressives propagandes, je vois passer sur Mastodon par le fait d'<cite>Anne Roth</cite>, Advisor on digital policy in the German parliament / Digital rights activist in Berlin, une communication intitulée « **Loud Men Talking Loudly: Exclusionary Cultures of Internet Governance** ».

[Ce rapport](https://notecc.kaouenn-noz.fr/lib/exe/fetch.php?media=loudmen-corinnecath-criticalinfralab.pdf) provient d'une d'enquête et est écrit par l'anthropologue <cite>Dr. Corinne Cath</cite>, inaugural fellow at the Critical Infrastructure Lab, University of Amsterdam, The Netherlands.

> "Policymakers and academics are often enamored with the multi-stakeholder governance model that characterizes organizations like the IETF, but neglect to consider how “embedded power hierarchies (e.g. of culture, gender and geopolitics) could skew global multi-stakeholderism in favor of already privileged circles in world politics.”" <cite>Corinne Cath-Speth</cite> in "*Loud Men Talking Loudly: Exclusionary Cultures of Internet Governance*", Primer prepared for the launch of the Critical Infrastructure Lab – Amsterdam, April 2023

Ah oui, la fameuse gouvernance ouverte avec **toute personne disposant d'une adresse électronique de courriel peut participer**…

> « Une autre façon de présenter ce dilemme est de dire que l'ouverture procédurale de l'IETF offre une opportunité, tandis que sa culture d'exclusion restreint les possibilités d'intervention. Un résumé plus précis de l'ouverture de l'IETF serait de dire que oui, toute personne disposant d'une adresse électronique peut participer, mais tout le monde ne sera pas écouté avec la même attention. mais tout le monde ne sera pas écouté avec la même attention. »

Je vous en partage ici une traduction au débotté issue d'un tableau présent dans ce rapport à la page 11, Cela concerne les Dynamiques (aka forces) culturelles & Effets d’exclusion dans la « Tech »

**Findings: Rough Cultures and Running Code**

+ Refus de la politique dans les
discussions techniques −> (_participe de_) −> Renforce les entreprises,
prive la société civile de ses moyens d'action

+ L'ouverture procédurale (aka process ouvert qui l'est pas vraiment) comme distraction −> (_participe de_) −> Délégitimation de la société civile en critique de l'influence de l'industrie

+ Recours aux réseaux informels −> (_participe de_) −> Marginalise les voix des minorités par l'exclusion des cercles sociaux 

+ Méthodes de travail par polissage  −> (_participe de_) −> Permet au sexisme et au racisme de persister, entravant la société civile

Il s'agit là d'une publication sur un cas d'étude concernant l'IETF, L’ Internet Engineering Task Force.

Corinne Cath détaille, illustre, et explicite ensuite chaque partie de ce tableau dans les 4 pages suivantes du rapport d'enquête.

La conclusion ?

> « Designations of internet governance organizations as exemplary in this regard often depend on ignoring or disregarding the exclusionary effects of cultural dynamics in favor of surface-level procedural access. We ignore these cultural hurdles at our own peril. »

Voir également “Radical infrastructure: Building beyond the failures of past imaginaries for networked communication”, <cite>Britt S Paris</cite>, <cite>Corinne Cath</cite>, and <cite>Sarah Myers West</cite> <https://journals.sagepub.com/eprint/IZ5WY6IIRE83ZBMVDFVU/full>

et 

+ La thèse de Gabriel Alcaras, « **Des logiciels libres au contrôle du code : l'industrialisation de l'écriture informatique** » ,  https://www.theses.fr/2022EHES0120

> « […] cette thèse analyse comment le contrôle du code s’exerce à l’échelle des pratiques expertes d’écriture, de la construction des outils de travail et de la propriété des moyens de production logicielle. »

Très, trè grand merci <cite>Antoine Fauchié</cite>

+ [Video] Nadine Moawad APC Closing Ceremony in Internet Governance Forum 2015 <https://piped.hostux.net/watch?v=nzGDhclAJcg>
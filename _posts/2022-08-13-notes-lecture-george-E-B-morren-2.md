---
title: "Notes de lecture sur la catastrophe : “Identification of Hazards and Responses”, George E. B. Morren Jr, (1983) 2/3"
layout: post
date: 2022-08-13 07:40
image: /assets/images/rhgsst.jpg
headerImage: true
tag:
- hsociety
- Notes
- Catastrophologie
- Anthropologie
category: blog
author: XavierCoadic
description: "Regards sur la catastrophe"
---

<figcaption class="caption">Image d'en-tête : Cette carte de température de la mer a été réalisée à partir des données de l'analyse SST de Medspiration, à gauche : Anomalie de la température de la surface de la mer en degré celsius − Climato 1971 - 2000 (au 9/08/2022) ; à droite : Température de la surface de la mer (SST) en degré celsius du mardi 9 Août 2022. Capture d'écran de carte depuis <a href="https://www.meteociel.fr/accueil/sst.php">MeteoCiel</a> Analyse RHGSST</figcaption>
<br>

Ceci est la seconde fournée de mes notes de lecture(s). La première est disponible [ici](/notes-lecture-george-E-B-morren/). Toujours en « pas à pas », j'ai décidé en fin de traitement de scinder une nouvelle fois mes écrits, qui seront ainsi publiés en 3 billets.

Cette partie finale de la synthèse de G. E. B. Morren porte sur « Les caractéristiques des processus de réponse », dans livre *Interpretation of Calamity From the Viewpoint of Human Ecology*, Kenneth Hewitt & Al. (1983). Il y détaille les *réponses* typiques face « aux désastres » et les problèmes que comportent ses modes de réponses. Je traite dans ce billet des « axes » 1 à 6, et mes notes de lecture des « axes » 7 à 13 seront prochainement disponibles.

Pendant que j'écris cette seconde fournée de notes plusieurs catastrophes se déroulent.

## Contexte

Alors que je travaillais sur mes notes, et me plaignais sur un réseau social ouvert et décentralisé (Mastodon), <u>Bonnie McCay Merritt</u> m'écrivait un courriel de réponse à une de mes demandes :

> « George, Vayda, Ken Hewitt et moi-même étions membres d'un nouveau département interdisciplinaire et d'une nouvelle unité de recherche à l'université Rutgers au début des années 1970, le département d'écologie humaine. Ken nous a apporté la tradition de Gilbert White de la géographie, à nous anthropologues, qui nous efforcions de donner un sens aux questions soulevées par l'adoption récente d'une approche des systèmes homéostatiques dans les relations entre l'humain⋅e et l'environnement (comme dans "Pigs for the Ancestors" de Rappaport, ironiquement issu du groupe de recherche précédent de Vayda basé à l'Université de Columbia). Ken Hewitt a quitté Rutgers, mais George, Vayda et moi-même sommes restés et avons développé notre travail en conséquence. Nous avons plaidé pour que l'on se concentre sur les réponses individuelles aux dangers afin de résoudre les problèmes méthodologiques et théoriques. George a très bien appliqué cette idée dans son travail sur la sécheresse en Grande-Bretagne. George est passé à autre chose, tout comme le reste d'entre nous − Vayda (et moi dans une certaine mesure) s'est orienté vers l'analyse des événements en tant que méthodologie ; j'ai travaillé sur certains aspects de la "tragédie des biens communs" ; Hewitt a été un chef de file dans la mise en évidence des dimensions politiques des catastrophes. »

![](/assets/images/copernicus-sentinel2-2022-08-11_13-40-15.png)

<figcaption class="caption">Capture d'écran d'image satelitaire Copernicus / Sentinel 2 d'une partie de la Bretagne au 11 août 2022</figcaption>
<br>

## The features of response processes

De manière directe, Morren affiche sans ambiguité son appui pris sur [Gregory Bateson](https://fr.wikipedia.org/wiki/Gregory_Bateson), un des fondateurs de [L'École de Palo Alto](https://fr.wikipedia.org/wiki/%C3%89cole_de_Palo_Alto), et son concept de *flexibilité*, et aussi sur l'éminent écologiste scientifique [Lawrence B. Slobodkin](https://en.wikipedia.org/wiki/Lawrence_B._Slobodkin)

> “The term that is basic to any discussion of the response process approach is Gregory Bateson concept, flexibility. He defines flexibility as 'uncommitted potentiality for change'. (1972 b. p. 497). For Bateson, and for others who have employed this approach, such as Slobodkin (1965) and Vayda and McCay (1975) a particularly significant cost of responding to a problem is the loss of flexibility, i.e. diminished ability to cope with other problems” <u>G. E. B. Morren</u>, <cite>*p. 290*</cite>

Vayda et McCay étaient déjà impliqué⋅e⋅s dans la publication de Morren “The Rural Ecology of the British Drought of 1975-1976“ (en 1980), ainsi que Kenneth Hewitt, comme relecteurs et contributeurs.

Cet appui est fait avec la référence “Ecology and Flexibility in Urban Civilization,” <u>G. Bateson</u> in *Steps to an Ecology of Mind* (Chicago: University of Chicago Press, 1972)<marginnote>Par manque de connaissance je n'effleure qu'à peine, et de loin en plus, l'influence de Bateson et de l'école de Palo Alto à l'époque de ce texte. Au moment où j'écris ces notes de lecture, je reste très interrogatif sur le fait que Morren, pour la conclusion de ce livre et avec ces co-auteurs, prenne en référence de base entière et quasi unique Bateson et son approche de la flexibilité “multi-disciplinaire”</marginnote>, 502–513, dont les 3 précédents chapitres nommé *From Versailles to Cybernetics\** (*p. 457*), *Pathologies of Epistemology\** (*p. 484*), et *The Roots of Ecological Crisis\** (*p. 494*, dans *Part VI: Crisis in the Ecology of Mind*) font appel à une « idée générale du fonctionnement de l'esprit » et des réseaux, à une « vision interactionnelle et stratégique » sur les comportements interpersonnels et la distance avec une *psychiatrie* face à des *troubles psychiques* liée au premier appel : 

> “I don't know how many people today really believe that there is an overall mind separate from the body, separate from the society, and separate from nature. But for those of you who would say that that is all "superstition," I am pre-pared to wager that I can demonstrate with them in a few minutes that the habits and ways of thinking that went with those supersitions are still in their heads and still determine a large part of their thoughts”. <u>Gregory Bateson</u>, <cite>“*Pathologies of Epistemology\**”, *p. 491*. (1972)</cite>[^bateson]

Si vous avez de la curiosité et du temps, la description en guise d'argumentaire à partir de « l'expérience » du paquet de cigarettes Lucky Strike, pour illustrer des *défauts* d'épistémologie au travers du parallaxe, vaut son pesant de cacahuètes foutraques… J'ai cru comprendre l'idée et me suis heurté à une inintellibilité, probablement liée à l'absence d'illsutration graphique… dans un exemple basé sur l'optique et l'ophotalmologique. <marginnote>Il s'agit d'un paragraphe entier résumant une expérience en ophtalmologie et psychologie d'<a href="https://en.wikipedia.org/wiki/Adelbert_Ames_Jr.Adelbert"> Ames Jr.</a> pour déployer une « conception du nom d'“écologie de l'esprit” ou écologie des idées, une science qui, en tant que branche de la théorie de la connaissance, n'existe pas encore » (Bateson) en prenant les chemins de la théorie des contextes. Vous trouverez un pdf de ce paragraphe <a href="https://archive.org/details/http___olivier.hammam.free.fr_imports_auteurs_bateson_eco-esprit_2-6-2-pathologies-epistemologie.htm">ici</a>. Aussi, je note que “<i>The Morning Notes of Adelbert Ames</i>” (1960), qui contiennent des lettres de correspondance avec John Dewey en 1946 (<a href="https://archive.org/details/in.ernet.dli.2015.139344/page/n181/mode/2up">p. 171 à p. 231</a>), ont été éditées par Rutgers University Press, université dans laquelle Morren était enseignant chercheur. Aussi, de Bateson : “<i>Reconsideration of the Origin and Nature of Perception</i>” (1953), in Vision and Action, edited by S. Ratner, toujours par Rutgers University Press</marginnote>

[^bateson]: STEPS TO AN ECOLOGY OF MIND [COLLECTED ESSAYS IN ANTHROPOLOGY,PSYCHIATRY, EVOLUTION, AND EPISTEMOLOGY], Gregory Bateson, *Jason Aronson Inc. Northvale, New Jersey*, Reprint. (Originally published: San Franciso : Chandler Pub. Co., 1972. With new pref. Collection of articles reprinted from various periodicals.) <https://ejcj.orfaleacenter.ucsb.edu/wp-content/uploads/2017/06/1972.-Gregory-Bateson-Steps-to-an-Ecology-of-Mind.pdf> ; [archives](https://web.archive.org/web/20220000000000*/https://ejcj.orfaleacenter.ucsb.edu/wp-content/uploads/2017/06/1972.-Gregory-Bateson-Steps-to-an-Ecology-of-Mind.pdf)

Puis, Bateson ouvre “*The Roots of Ecological Crisis\**” avec une tentative d'approche « racinaire du problème écologique » accompagnée d'une *conclusion versant dans la prédiction* :

> “Other testimony has been presented regarding bills to deal with particular problems of pollution and environmental degradation in Hawaii. It is hoped that the pro-posed Office of Environmental Quality Control and the Environmental Center at the University of Hawaii will go beyond this ad hoc approach and will study the more basic causes of the current rash of environmental troubles. <br> The present testimony argues that these basic causes lie in the combined action of (a) technological advance; (b) population increase; and (c) conventional (but wrong) ideas about the nature of man and his relation to the environment.<br> It is concluded that the next five to ten years will be a period like the Federalist period in United States history in which the whole philosophy of government, education, and technology must be debated.” <u>Gregory Bateson</u>, <cite>“*The Roots of Ecological Crisis\**”, *p. 494*</cite>.

Donc, depuis le chapitre suivant de Bateson, G. E. B. Morren détaille, dans l'introduction de sa dernière partie, ce qui est entendu par *flexibility*, pour insister le fait que ce concept *s'intéresse à la qualité plus qu'à la quantité* et *[qu']il nécessite une dimension temporelle*.

> “Gains or losses of flexibility involve qualitative changes in other system properties, including what Hotting and Goldberg (1971) refer to as the 'domain of stability' and 'resilience'.” <u>Morren</u>, <cite>*p. 290</cite>

Pour Morren, La *flexibilité* (et d'autres propriétés qualitatives), ici convoquée et rappelée comme un *primitive term* en philosophie, *ne peut être définie rigoureusement au-delà de l'énoncé de ces points généraux* qu'il expose. Il rappelle également qu'il s'agit d'une transposition conceptuelle depuis la biologie évolutive, notamment par Bateson et Slobodkin.

En fin d'ouverture de cette dernière partie du livre, il insiste sur le fait que « *Tous ces efforts* [face aux désastres et catastrophes] *ont mis l'accent sur le fait que le processus d'adaptation et de survie est continu et que la stratégie globale vise à réduire les pertes plutôt qu'à les prévenir. Par conséquent, l'accent doit être mis sur la relation entre les problèmes environnementaux qui menacent la survie et le processus réel de réponse à ces changements* ».

Pour terminer cette ouverture, il adresse une critique au *petit monde de la catastrophe* qui lui sert aussi de marche pied en transition vers les 13 hypothèses (*assumptions*) qu'il va développer :

> “Scientific investigators reporting on development, environmental hazards, social change, planning and the like are often at a loss to explain the great variability to be observed between individuals and between groups subjected to seemingly similar circumstances. The field of human adaptation, of which the areas cited are subfields, is riddled with contradictions and paradoxes, both apparent and real.” <u>Morren</u>, <cite>*p. 291*</cite>

### Les axes 1 à 6 caractéristiques de réponses faces aux catastrophes

**(1) In a parsimonious world there should be some correspondence between the characteristics of environmental problems and the characteristics of responses to them.**

> “Of particular interest are such features of problems as their magnitude, duration and novelty, and such features of responsent as their magnitude, reversibility, temporal order and persistence, as well as associated costs (Vayda & McCay 1975). The 'correspondence' specified is not a *necessary* one, except possibly in the context of long-term survival. In the shorter run them are numerous and familiar examples of responses which are, in some sense, disproportionate, in terms of both their quantitative costs and the loss of flexibility entailed for some responding units as these relate to benefits and effectiveness. Any analysis of alternatives would have to take this into account.”

Dans la référence prise par Morren, Vayda et McCay (que l'on retrouve plusieurs fois dans les œuvres de Morren) établissent 4 critiques formulées à l'encontre de l'anthropologie écologique de l'époque (1975) : 

+ l'importance excessive qu'elle accorde à l'énergie,
+ son manque de capacité (*inability*) à expliquer les phénomènes culturels,
+ sa préoccupation pour les équilibres statiques,
+ son manque de clarté quant aux unités d'analyse appropriées.

Les auteurs y reconnaissent aussi « que certaines de ces critiques peuvent ne pas être justifiées, nous soulignons néanmoins des préoccupations parallèles en écologie ».[^vayda-mccay]

Bonnie McCay est aussi Professeure émérite en géographie de l'université de Rutgers, passée par le département des humanités écologiques, et [Andrew P. Vayda](https://en.wikipedia.org/wiki/Andrew_P._Vayda) Professeur émérite d'anthropologie et d'écologie à l'université Rutgers.

[^vayda-mccay]: Vayda, A., & McCay, B.J. (1975). *New Directions in Ecology and Ecological Anthropology*. Annual Review of Anthropology, 4, 293-306.

Pour paraphraser <u>Yves Gringas</u> (2020, *p. 47*), l'Université de Rutgers semblait avoir développé une capacité « d'assurer une ambiance […] » et de reproduire ainsi un niveau social d'expertise propre à assurer une autonomie scientifique à une entité organisée [^gringas].

[^gringas]: Y. Gringas écrit « de reproduire ainsi un niveau social d'expertise propre à assurer au pays une autonomie scientifique ». *Sociologie des sciences*, 3e édition (2020), Presses Universitaires de France / Humensis.

**(2) People, like other organism, often respond to a new problem as if it were an old problem (Slobodkin 1968, p.196) and, hence, respond to variety of problems in an essentially similar way, or use the same responses for different problems.** 

> “This feature of responses is one of the factors tending to confound the first 'principle' stated above and, perhaps, it represents another form of par-simony. In his analysis of Enga (Papua New Guinea) responses to the risk of killing frosts Eric Waddell (1975; Ch. 2 above) describes a temporal sequence of responses including patterns of hazard mitigation, risk spreading and, under the most extreme circumstances, refuging.<marginnote>Enga est un peuple et une province de la Papouasie-Nouvelle-Guinée appartenant à la région des Hautes-Terres. Située dans une région très montagneuse et difficile d'accès, elle est peuplée quasiment uniquement par l'ethnie qui lui a donné son nom, parlant une même langue. (Wikipedia)</marginnote> The higher-level responses are essentially similar to Enga responses to defeat in traditional warfare, and were also characteristic responses of New Guinea populations to clashes with aliens in the novel early contact situation. Another example is the common response of farmers in places such as Britain and the United States to both low commodity prices and poor growing conditions. Characteristically, they respond to either problem by making more extensive plantings in the following year.<br> 
An obvious operational problem in connection with this and other features is that of *identity* of problems and of responses. The example of modern farmers presented above might be restated to any that farmers respond to a 'bad year' by expanding existing activities, thus blurring As distinctiveness of the problems. Similarly, one might speak of responding to regional or local scarcity of a vital resource by establishing a really integrating networks or grids. One would then to have to argue the essential identity of electrical grids, trade relationships, highway systems, water works and similarly functioning infrastructure. Identity is necessarily an artifact of the taxonomy used to classify problems or responses.”

Le parallèle ici tiré entre les réponses à des « catastrophes » entre le peuple Enga, enclavé en haute-montage sur une île (Papouasie-Nouvelle-Guinée), les agriculteurs de plaine d'un autre île (Angleterre), et ceux d'un large pays (USA) pour exposer qu'ils « réagissent souvent à un nouveau problème comme s'il s'agissait d'un vieux problème (Slobodkin 1968, p.196) et,<marginnote>J'ai ici eu l'envie de me replonger (et l'ai pas encore fait à cette date) dans <u>Wardekker, J.A., de Jong, A., Knoop, J.M. and van der Sluijs, J.P</u>. (2010) “Operationalising a Resilience Approach to Adapting an Urban Delta to Uncertain Climate Changes. Technological Forecasting and Social Change“ pour en avoir une relecture critique sur ses 6 pricnipes de « résilience ».<br> <strong>Homéostasie</strong> : des boucles multiples de rétroaction pour contrer les perturbations et stabiliser le système ; l'homéostasie est la capacité que peut avoir un système quelconque à conserver son équilibre de fonctionnement en dépit des contraintes qui lui sont extérieures.<br>
<strong>Omnivore</strong> : la vulnérabilité est réduite par la diversification des ressources et des moyens ; comme dans la nature l'ultra spécialisation entame les potentiels de survie.<br>
<strong>Flux rapides</strong> : des mouvements des ressources rapides à travers le système assurent la mobilisation de ces ressources pour faire face aux perturbations.<br>
<strong>Niveaux hiérarchiques faibles</strong> : afin de mettre en œuvre rapidement des réponses très locales non standard. Comme chez nos amis les fourmis ou les rats.<br>
<strong>Capacité tampon</strong> : capacités centrales sur-dimensionnées de telle sorte que les seuils critiques soient moins susceptibles d’être franchis. C'est ce qui est à l’œuvre dans les végétaux du désert et leur capacité interne en eau.<br>
<strong>Redondance</strong> : les fonctions se chevauchent, et un relais peut ainsi être assuré si certaines échouent. Les espèces d’animaux redondantes sont celles qui exercent la même fonction au sein de l’écosystème. Comme les vautours et les hyènes dont les fonctions de découpage des animaux morts sont une même fonction pour deux espèces.</marginnote>
par conséquent, réagissent à divers problèmes d'une manière essentiellement similaire, ou utilisent les mêmes réponses pour différents problèmes » enfonce le clou de l'argument du besoin de *flexibilité*. Morren souligne aussi le fait que cela « brouille la distinction entre les problèmes », et aussi l'inclinaison à mise en sur-capacité / sur-production en réponses aux désastres.

Afin de mieux classer les problèmes ou les réponses face aux « catastrophes », il invite ainsi à questionner et définir une identification des :
+ les réseaux électriques
+ les relations commerciales, 
+ les systèmes de routes et d'autoroutes, 
+ les ouvrages hydrauliques, 
+ et des autres infrastructures similaires.

**(3) Responding minimally at the onset of a problem prevents over-commitment of resources and faculties in the face of uncertainty concerning such problem characteristics as duration and magnitude.**

> “Manifestations of this feature frequently involve conflicts and paradoxes which are difficult for ordinary people, public leaders and scientists to resolve. Ordinary people often appear to be conservative, reluctant to abandon place and property to fate, with a strong commitment to the maintenance of the stands quo (which also strongly influences post-disaster behaviour and sentiment). The distribution of individual experience, preparedness, and authority in small groups and communities is patchy. In the earliest phases of a process many people prefer to bide their time and implicitly to resist external and impersonal authority or to select their own mix of small adjustments, perhaps in concert with kin, friends and neighbours. At this level, individual action can strongly Influence the effectiveness of responses, especially in situations where timeliness is an issue. Examples include such sudden onset or emergency situations as earthquakes, budding fires, marine disasters. or potential high-way accidents, where survival often depends upon the behaviour of one or two prepared and purposeful individuals who serve to organise and guide the behaviour of others close by. In such instances, 'taking charge' may consist merely of the insuance of a timely warning or a few directions. These can initiate separation of some of the exposed people from an amplifying hazard, people who might otherwise continue on their existing course of action or inaction to the point of panic and catastrophe. Political leaders responsible for higher-level responses are similarly .caught between the possible need for timely action and public inertia, on the one hand, and the long-term maintenance of the authority of their offices, on the other. The latter corresponds to one of the costs of making an 'inappropriate' decision. Officials am constantly and inevitably subjected to the charge of either inaction and not acting quickly enough *or* acting precipitously and unnecessarily. Scientists involved in the development of diagnostics and warning techniques aimed at triggering responses are in a similar bind. The chance that an attempted 'cure' will be worse than the 'disease' is a real one. The unvisresal mode of reducing this kind of cost is to defer as long as possible those responses which may have irreversible consequences.” 

G. E. B. Morren avance la modalité de réponse à un « désastre » par « la réponse minimale », notamment au travers des rapports de tensions et de forces entre l'engagement individuel de « personnes oridnaires » et des autorités institutées (scientifiques, administration d'État et réponse de « haut niveau). Il souligne que dans certaines « catastrophes » la première réaction menant à une pré-organisation des suites, dans le sens d'inlfuencer le *cours des choses*, peut être amorcée par des « individus préparés et déterminés qui servent à organiser et à guider le comportement des autres personnes à proximité ».

Il relève aussi que les fonctionnaires sont fréquemment accusé⋅e⋅s d'inaction ou de lenteur et les ingénieur⋅e⋅s et scientifiques en charge des diagnostics et systèmes d'alertes peuvent aussi être affublé⋅e⋅s de qualification similaire par les populations impactées.<marginnote>Pandémie COVID-19, France, après 2 années : la vague où le variant Omicron BA.5 est majoritaire a directement tuée 80 000 vies dans l’Union Européenne, dont plus de 10’000 en France, entre le 1er avril et le 12 août 2022</marginnote>

Surtout, dans ce *cocktail* de temporalité, de jugement, de besoin, de « niveaux », il écrit « Le risque qu'une tentative de "remède" soit pire que l'"affection" est réel. ». J'y vois une forme de mémoire du désastre et tentation du bienfaisant.

**(4) Related to the foregoing is the principle that the cause (or causes) of entry into one phase of the response process need not be the same as the cause (or causes) of escalation to other phases of the process (Vayda 1974).**

> “Just as responses may be temporally ordered, so too problems and other contingencies may be ordered in time. Typically, such post-disaster problems as loss of shelter, loss of livelihood and life-support, and infectious disease, occur in acute form at different times after onset. In his Rudy of warfare in the New Guinea highlands Vayda (1974) found that, while lower-level raiding and skirmishing might be triggered by an desire for revenge, the decisions to escalate to higher levels of armed conflict might occur when earlier activity revealed weakness on the part of the enemy.”

![](/assets/images/loire2021-2022-copernicus.jpg)

<figcaption class="caption">La Loire au Pont de Varades en Loire-Atlantique en 2021 à gauche et en comparaison de cette année 2022 à droite, vue par Copernicus / Sentinel-2</figcaption><marginnote>Le débit sur la station hydrométrique de Montjean, 13km en amont de Varades, était, au 11 août 2022, de 95m³/s. La station de Montjean existe depuis 1842. Ce débit correspond presque à l'étiage VCN3 décennal. C'est-à-dire que, à climat constant, il est attendu un débit de 95m³/s sur 3 jours en moyenne tous les 10 ans. Cette station a connu des débits inférieurs à 95m³/s lors de la sécheresse de 1976, avec un minimum de 73m³/s le 22 août, avec aussi des débits similaires en en 1870, 1905, 1906, 1921, 1947, 1949, 1950, etc. <a href="https://www.hydro.eaufrance.fr/stationhydro/M530001010/series">hydro France</a> ; <a href="https://www.hydro.eaufrance.fr/stationhydro/M530001010/synthese/regime/basses-eaux">Fiche de synthèse - Données hydrologiques de synthèse - Basses-eaux</a> ; <a href="https://www.persee.fr/doc/jhydr_0000-0001_2013_act_35_1_1298">Contribution de l’hydrométrie au suivi de l’évolution du lit de la Loire aval</a> Stéphanie Poligot-Pitsch, Gérard Geffray, Nicolas Pichon (2013). Merci Thibault Laconde pour avoir partagé des indications</marginnote>
<br>

En relation avec les agencements précédemment cités, il est typique, en réaction à des catastrophes, d'ordonner les réponses avec une temporalité avec une corrélation sur les problèmes, les conséquences, qui découle de la « catastrophe » tout autant avec un ancrage dans une temporalité qui n'est pas anodine.
Ces imbrications typiques ne sont pas sans poser des questions et charrier aussi leurs lots de problèmes exogènes. Ainsi, Pour Morren, s'appuyant sur sa collègue Bonnie Vayda, « l'entrée dans une phase du processus de réponse ne doit pas nécessairement être la même que la (ou les) cause(s) de l'escalade vers d'autres phases du processus ».

En écho à la publication de Vayda et aux écrits de Morren, force est de constater que 39 ans après cette synthèse et ce livre, en France, face à l'appropriation de ressources « naturelles », l'eau, par un petit nombre de groupes dominants très aisés, des petits groupes dominés et moins aisés s'expriment par des « escarmouches » dans une « Guerre des Golfs » (pour l'eau) et cherchent à rendre tangible une l'établissement de « distinctions dans l'extinction » (<u>Olivier Ertzscheid</u>, [La guerre des golfs. La distinction dans l'extinction](https://www.affordance.info/mon_weblog/2022/08/la-guerre-du-golf.html) (2022))

![](/assets/images/drought-europe-2022.png)

<figcaption class="caption">Situation de l'indicateur combiné de sécheresse en Europe - 3e période de dix jours de juillet 2022. Selon la carte de l'Indicateur Combiné de Sécheresse : <strong>47% du territoire de l'UE est en situation de vigilance accrue et 17% en situation d'alerte.</strong> Source <a href="https://edo.jrc.ec.europa.eu/edov2/php/index.php?id=1000">EDO</a></figcaption>
<br>


**(5) Similarly, escalation from phase to phase is not inevitable once a process of response is initiated, but rather escalation may depend upon the persistence of problems initiating the process or its later phases, or on the strength of mechanisms resisting escalation, as well as on the effectiveness of existing responses.**

> “Good general examples of anti-escalatory mechanisms include so-called peasant conservatism in the face of outside agents of socioeconomic change, and the policy of some nations, such as post-revolutionary China (and the United States at the time of the San Francisco earthquake) to reject offers of disaster relief tendered by other nations. Maris (1975) believes that a 'conservative impulse' in the face of changes of many sorts has great generality.”

Pour Morren, « l'escalade d'une phase à l'autre n'est pas inévitable une fois qu'un processus de réponse est lancé, cependant l'escalade peut plutôt dépendre de la persistance des problèmes à l'origine du processus ou de ses phases ultérieures, ou de la force des mécanismes résistant à l'escalade, ainsi que de l'efficacité des réponses existantes ».

La complexité des agencements et forces en jeu face à la « catastrophe ». Morren prend en référence Peter Maris, “Loss and Change” (1975)[^maris], ce qui est un nouveau ressort depuis la psychologie sur les processus de changements personnels et sociaux et leurs « sens », depuis plusieurs angles :

+ Une étude sur les veuves
+ Les effets dévastateurs des projets de rénovation urbaine sur les personnes dont les quartiers sont détruits, 
+ Une analyse des activités des associations tribales au Nigeria.

Avec les conséquences, comme l'écrit Morren, liées à l'“impulsion conservatrice“.

[^maris]: Archive numérique de “Loss and Change” (1974) disponible [ici](https://archive.org/details/losschange0000marr)

**(6) Reversibility of response processes, involving the restoration of flexibility (a return to a facsimile of a `normal' condition or level of response), while not inevitable, it also a typical feature of response processes.** 

> “A good example of the restoration of flexibility, indeed one involving behaviour which appears paradoxical to some experts (see (10) below), is the propensity of people to return to the site of an experienced disaster and attempt to replicate the old life-style which may itself have contributed to the disaster or its impact in the first instance. After a flood, people rebuild in the floodplain. One of the most famous photographs of the 1906 San Francisco earthquake and the shows a family sitting down at the dinner table in the out-of-doors with the ruins of the city smouldering in the background.
>
> There are also counter-exemples of failure of restoration, involving the permanent (irreversible) loss of flexibility. Erickson's (1916) analysis of the consequences of a federal emergency shelter programme for the survivors Buffalo Creek wax cited earlier. An example familiar to anthropologists conernerns the effects of contact on formerly isolated and self-suflicient primitive populations.<br>
Moorehead (1968) rightly describes it as 'fatal impact' involving the irrevocable penetration of a social, politiclal and economic capsule and very high mortality.<br>
Typically, tribal peoples, such as the group I worked with in Guinea (Morren 1977), lost first their political sovreignty as part of the process administrators referred to as 'pacification' and them more gradually, their economic self-sufficiency. 
The concomitant of this has been growing dependency, with the people concerned on their may to real or cultural extinction and/or peasantry. Waddell's (1975) description of the irreversible consequences of disaster relief for a New Guinea population is, according to some observers, generalisable to modern developing nations too (Waddell 1977, Hall 1975).”

![](/assets/images/rhgsst.jpg)

<figcaption class="caption">Cette carte de température de la mer a été réalisée à partir des données de l'analyse SST de Medspiration, à gauche : Anomalie de la température de la surface de la mer en degré celsius − Climato 1971 - 2000 (au 9/08/2022) ; à droite : Température de la surface de la mer (SST) en degré celsius du mardi 9 Août 2022. Capture d'écran de carte depuis <a href="https://www.meteociel.fr/accueil/sst.php">MeteoCiel</a> Analyse RHGSST</figcaption>
<br>

Le retour à une « normalité »<marginnote>Héritage, Catastrophe, Démantèlement, petites tâches pour survivre et espérer un retour à la « normale » : Homesick — de Koya Kamu. (2019) Court-métrage. « 2 ans après la catastrophe nucléaire de Fukushima, Murai se rend régulièrement en zone interdite, afin d'exhumer des objets à rendre à leurs propriétaires & passer du temps avec son fils, qui lui est bloqué en zone interdite »</marginnote>, même de l'ordre d'un état reproduisant avec *défauts* une impression de « normalité », est aussi une réaction typique aux « désastres ». Les réponses conçues pour agir dans la « catastrophe » sont ainsi conceptualisées avec une intention de réversibilité pour contribuer à ce « retour ».

Reconstruire dans les plaines inondables après une inondation ou revenir sur la zone « catastrophée » pour rebâtir des vies, des infrastructures et des villes est encore et toujours, 39 ans après ce livre, une activité humaine courante<marginnote>« toponymie […] les noms sont la mémoire des catastrophes » <a href="https://odileplattard.wordpress.com/">Odile Plattard</a></marginnote>. les raisons portées pour justifier cela, les facteurs influençant (ou forçant) ce « retour » peuvent être sociaux, économique, politique, religieux, sentimentaux.

![](/assets/images/feux-2022-france.jpg)
<figcaption class="caption">Carte et infographie des feux en France hexagonale en 2022 par <a href="https://www.lemonde.fr/planete/article/2022/08/12/la-carte-des-incendies-en-france-depuis-le-debut-de-l-ete_6137869_3244.html">Le Monde</a>. La surface totale brulée au 12 août 2022, 48 456 hectares, est 3 fois supérieure à la moyenne annuelle des dix années précédentes</figcaption>
<br>

La référence à [Alan McCrae Moorehead](https://en.wikipedia.org/wiki/Alan_Moorehead), correspondant de guerre, avec son livre “The Fatal Impact: An Account of the Invasion of the South Pacific, 1767–1840”[^moorehead], avec les effets *catastrophiques* de la « conquête » de l'Australie, de Tahiti, et de l'Antarctique, notamment sur l'impact de groupes venant *intervenir pour le bien(faisant)* sur des populations autonomes et « isolées » relève l'« "impact fatal" », impliquant la pénétration irrévocable d'une capsule sociale, politique et économique, et pouvant mener à  « une mortalité très élevée ».<marginnote>Voir aussi <a href="https://fr.wikipedia.org/wiki/%C3%89pid%C3%A9mie_de_chol%C3%A9ra_en_Ha%C3%AFti">Épidémie de choléra en Haïti</a> (2010)</marginnote>

[^moorehead]: Archive numérique du livre “The Fatal Impact: An Account of the Invasion of the South Pacific, 1767–1840” disponible [ici](https://archive.org/details/fatalimpact00moor)

Il y a là, dans cette « réponse » typique face aux désastres une double *mise en danger* qui se joue hors de la temporalité assignée à la « catastrophe », du moins de celle de son ressenti et de sa *visibilité* ordinaire. L'élan *expéditionnaire* des unités d'intervention externes, forçant la subalternité de personnes *endogènes*, n'est pas une action neutre et charrie des conséquences dont les effets peuvent être délétères. L'élan vers un « retour à la normale », que ce soit de « se comporter de la même manière » ou de « reconstruire sur site », se déroule sans prise en considération des facteurs peu visibles de la « catastrophe » et surtout avec peu de considération sur les contingences de risques liés à la « catastrophe » elle-même.

![](/assets/images/noaa-sst-anomalies.png)
<figcaption class="caption">11 août 2022 : Anomalie sévère de température de surface des eaux dans l'océan pacifique au large des côtes américaines. + 8,7 ° Celsius au-dessus de la normale. Le potentiel thermique de l'eau est plus de quatre fois supérieure à celle de l'air. (Il semble aussi que la zone maritime au nord de la Sibérie soit aussi dans une anomalie sévère. Je n'arrive à en tirer une évaluation). Source : <a href="https://www.ospo.noaa.gov/Products/ocean/sst/anomaly/index.html">OSPO NOAA SST Anomalies</a></figcaption>
<br>

> “We haven’t analysed fully this year’s event because it is still ongoing. There were no other events in the past 500 [years] similar to the drought of 2018. But this year, I think, is worse.” said <u>Andrea Toreti</u> of the European Commission’s Joint Research Centre. [The Guardian](https://www.theguardian.com/environment/2022/aug/13/europes-rivers-run-dry-as-scientists-warn-drought-could-be-worst-in-500-years) (2022/08/13)

# Remerciements

+ Rita Lehman, Environmental Sciences, Rutgers University
+ Bonnie McCay Merritt, Professor Emerita, Rutgers University <https://www.annualreviews.org/doi/abs/10.1146/annurev.an.04.100175.001453>


# Notes et références 
---
title: "Fdln, rencontre atelier sur la reconnaissance faciale et morphologique de masse."
layout: post
date: 2019-02-12
image: /assets/images/fdln_faceblind.jpg
headerImage: true
tag: 
- Biononymous
- Bioprivacy
- Rennes
- Numérique
category: blog
author: XavierCoadic
description: 
---

_La vie n'est pas un long fleuve tranquille_... Après deux mois de travail de préparation et [4 jours de conférences et d'ateliers](/fdln-2019) lors du Festival des Libertés Numériques 2019 à Rennes, je prends le temps de rédiger comme promis le récit concernant la séance sur le fichage morphologique. Ce bout de texte s'inscrit comme part d'appui à la publication dans le corps de la documentation. Dans une histoire de stress, de ~~catastrophe~~ chute, de fin heureuse.

![](/assets/images/documentation.jpg)

La semaine devait commencer par une « [anti-conférence gesticulée](/fdln-anticonference-gesticulee) », un lundi dans un bar qui ouvrait spécialement pour l'occasion. Soirée annulée car personne n'est venu et grosse boule au ventre d'avoir _fait ouvrir exceptionnellement_, et donc mobilisé patron et patronne, pour la soirée.

<iframe src="https://mamot.fr/@XavCC/101539670350534960/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400"></iframe><script src="https://mamot.fr/embed.js" async="async"></script>

Le lendemain devait se dérouler autour d'un atelier de mise en pratique « [autodéfense bionumérique. Collecter, effacer ou brouiller de l'ADN. Et un peu plus...](fdln-bioprivacy) » dans les locaux de l'entreprise [Zenika](https://www.zenika.com). Là, même punition, c'est à dire annulation car aucune personne pour pariticiper et un gérant mobilisé hors des horaires de travail.

Après avoir stressé longtemps avant les dates fatidiques, j'étais au fond de trou du moral avec des relents de honte.

<iframe src="https://mamot.fr/@XavCC/101538136916024186/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400"></iframe><script src="https://mamot.fr/embed.js" async="async"></script>

## Reboot le prévu pour sortir du _mirécran_

Pour préparer les temps, contenus, ressources et déroulés des séances lors de ce festival j'avais décidé de puiser dans la littérature, y compris la science-fiction et notamment dans un référence qui m'a beaucoup influencé ces dernières années : La zone du Dehors<sup>[Wikipédia](https://fr.wikipedia.org/wiki/La_Zone_du_Dehors)</sup>

> « _Il est extrêment difficile de mobiliser les gens contre les maniuplations douces, les demi-mensonges, les libertés grignotés dans le clair-obscur..._ » Kamio, “Réfléchir, c'est fléchir deux fois.” La zone du Dehors, ed. Folio SF, p.228.

Ok, repartons de _je sais pas d'où_, d'un Dehors pour ne pas s'apitoyer sur un _mirécran_ trop propre à soi construit dans nos considérations sociales ; celles qui filment, projettent et flattent notre égo pour nous _clastrer_ les un⋅e⋅s par rapport aux autres ; _gommant subtilement les défauts et les rides_ qui sont autant de traces et preuves de nos parcours, des composants de ce que nous sommes, de notre vécu. Répartir, repartir...

> « _Mais à condition de nous battre sur notre propre terrain. Pas dans les médias, mais la rue, le bouche-à-oreille, les actions ponctuelles, les contagions locales, virales._ » Kamio, “Réfléchir, c'est fléchir deux fois.” La zone du Dehors, ed. Folio SF, p.228.

## Qu'avons nous fait pour les libertés, Pourquoi et comment cette configuration rencontre-atelier ?

J'avais ainsi lourdement préparé chaque intervention en amont et j'avais vécu des déconvenues. 

Le jeudi 7 février, jours de l'atelier « reconnaissance faciale et morphologique de masse » je n'avais publié aucun texte préparatoire alors que publications il y eu pour les deux autres séances. Le moral dans le trou ça vous plombe les énergies.

Il me restait encore quelques larmes de jus, des gouttes d'espoirs, notamment, car j'apprécie au plus haut point la bibliothèque de l'INSA Rennes, lieu du dernier atelier. Et puis, j'avais quand investit beaucoup de [préparation documentée](https://notecc.frama.wiki/atelier:faceblind_facial_reco) dans cette dernière séance. En espérant que toutes ces ressources serviront à d'autres, et même au-delà du festival.

Il me reste à vous écrire quelques descriptions de cette séance plein surprise.

> « _Un tiers-lieu ? Prenez une intention, partagez là dans un configuration sociale et un lieu, documentez ce que vous faites ; c'est ça un tiers-lieu_ » Antoine Burret, Docteur en sociologie, auteur de la thèse [Étude de la configuration en Tiers-Lieu : la repolitisation par le service
](https://tel.archives-ouvertes.fr/tel-01587759)

### Sincérité

Juste avant de m'installer pour _l'atelier_ j'ai croisé dans la bibliothèque deux personnes de [Exodus Privacy](https://reports.exodus-privacy.eu.org). Deux personnes que j'apprécie beaucoup et cela m'a un peu mis la pression.

Une douzaine de personneS se présentaient pour participer, en plus de l'équipe Exodus Privacy..._Chouette, sourire, ça fait du bien, ça tourne_.

![](/assets/images/fdln_map.gif)

<figcaption class="caption">Montage ne gif avec le logicile GIMP de la bannière du Festival des Libertés Numérique, immage originale de Bibliothèuqe INSA, Licence Creative Commons Attribution - Partage dans les mêmes Conditions 4.0 International</figcaption>



J'ai pas joué, j'ai décrit franc la situation. Les deux soirées annulées et mes doutes profonds sur ce que je fais et ce que je propose, exposant ma pensée qu'il est peut-être une erreur de ma part d'imposer des considérations de situations propres uniquement à mon appréciation, la forme des séances ; erreur d'imposer des titres et textes de présentations qui ne correspondraient qu'à mon _état de l'art_, le fond des séances. J'avoue ne pas aimer une forme ou un risque de _doctrine politique de conquête_ alors que je cherche des configurations permettant l'émancipation collective, j'avoue que ce soir, bien qu'ayant préparé un déroulé à _[faire ensemble](https://notecc.frama.wiki/atelier:faceblind_facial_reco)_ je voulais tenter d'être simplement un participant parmi les autres participant⋅e⋅s.

Voilà ce que fut mon introduction orale.

### Partir des expressions des personnes présentes

Ainsi, j'ai proposé que chaque qui le souhaite puisse dire à haute voix :

+ Pourquoi êtes-vous ici aujourd'hui ?
+ Quelles questions vous posez-vous ?
+ Quels vécus avez-vous possiblement traversés qui ferait écho aux thématiques du festival et de cette séance ?

Rapidement les questions d'intimités (elles resteront un fil conducteur), de confidentialité (il en faut pour parler librement de soi), de libertés, de surveillance, de contrôle social, servaient de trame à diverses anecdotes et réflexions partagées.

Des deux pieds d'égalité, d'une posture pair à pair, j'ai aussi _confié de moi_ en parlant d'anecdotes de vie personnelle. _Une vraie thérapie ce festival ?_

À la manière d'un café où il fait bon _vivre ensemble_ avec ses quelques règles en commun exposées suite aux premiers échanges :

- Vous pouvez faire de photos mais vous devez avoir le consentement explicite de toute personne figurant dessus avant de la prendre
- Vous pouvez aller et venir comme bon vous semble
- Vous pouvez parler quand bon vous semble, sans couper la parole à autrui
- Vous pouvez ne pas parler si vous le souhaitez

### Improviser

Les échanges allant dans plusieurs directions sur un moment _tout doux_ (SIC une personne présente), il fallait de temps en temps raccrocher à des réalités technologiques, juridiques et des cas pratiques. 

> _NDLR : Heureusement que j'avais bien préparé en amont y compris sur d'autres sujets_

Traces de nous, empreintes, ADN, photo, caméra surveillance, aéroport, Facebook, google, _smartphones_, prévention d'attentat, avancée de la science et du secteur médical, médico-légal, comment discuter de cela en famille...

C'était fourni sans être la criée, c'était dense, précis, vrai et sincère... ce qui rend quasi impossible une tentative de retranscription écrite. Oui les personnes qui étaient là étaient les _bonnes personnes_[^1], alors que plusieurs avaient un sentiment de [syndrome d'imposture](https://fr.wikipedia.org/wiki/Syndrome_de_l'imposteur)[^2].

Je vous partage la paraphrase de [Tristan Nitot](https://standblog.org/blog/pages/Surveillance) par un participant pour illustrer l'aspect _en commun_ de l'intimité :

> « _Les WC pour faire ses besoins, tout le monde connais le film, on sait comment ça se passe. On fait toutes et tous la même chose. Pourtant on ferme tous et toutes la porte_ »

### Intimité, confidentialité et après ?

La confendialité serait comme échanger une photo, la partager entre deux personnes de confiance. Rompre cela, ça ressemble à Facebook qui la charge sur ces serveurs de stockage et en plus vous _dit_ “ Tag ton amie sur cette photo ” ou “Jean-dédé (que tu ne connais pas encore) apparait sur cette photo et vous travaillez dans la même ville ”

Ce sont des petits bouts de vous que l'on vous vole, pire encore avec un petit peu de _ludification_ on vous propose de participer à briser un peu de l'intimité d'autrui.

C'est une des formes du _travail du clic_, dont une séance à [faire ensemble était initialement prévue](https://notecc.frama.wiki/atelier:faceblind_facial_reco#parler_du_travail_du_clic) pour cette soirée. J'ai préféré décrire cette séance, montre le déroulé et le code source nécessaire à _coder par soi-même_. Puis j'ai demandé qui dans le public serait prête ou prêt à le refaire par elle ou lui-même ? Qui voudrait y apporter des modifications, des détournements, d'autres usages ? « je le ferais chez moi pour m'entrainer et ne pas être _exposé⋅e_ au public » ; « Je le ferais avec ma grand-mère mais dans l'autre sens pour l'intéresser et lui faire comprendre comme ça marche » ; « Sur une clé [Tails](https://tails.boum.org/) chez moi coupé du réseau » ; « Je testerais dessus les camouflages qui déjouent la reconnaissance faciale » ; furent quelques unes des idées exprimées.

Ce qui engendra de nouveaux échanges toujours plus poussés, des questions de plus en plus pointues, des personnes qui prenaient leur places de facilitations...

> _NDLR : J'aurais pu disparaître, le groupe semblait autonome_

Quelques récits d'expériences de travaux avec des réfugié⋅e⋅s pour les aider à se _désincarcérer_ de la surveillance morphologique et biologique[^3]

### Équiper pour résister

Nous exprimions avec des mots différents, propres à chacunes et chacuns, issus de nos vécus, pour tenter d'exprimer des craintes collectives sur des _choses_ que l'on nous prend, des libertés qui semblent disparaître, des morceaux de nous que l'on nous vole (comme les contours de notre visage ou notre intimité génétique et celle de notre généalogie). Les mots ont du sens et nommer c'est déjà reprendre du pouvoir.

Pour résister à ce qui nous semble des menaces pour et dans notre environnement, sur le collectif et sur notre individualité, il nous manquait encore quelques pas, quelques pas dans un _Dehors_.

J'ai expliqué ma volonté première avant de lancer ces trois temps pour le festival des libertés numériques. Travailler collectivement l'information, la confronter et la traduire en connaissance, passer cette connaissance par l'expérience et l'expérimentation pour la transformer en savoir, chercher des savoir-faire et des savoir-être. 

Beaucoup de participant⋅e⋅s ont nourri cette articulation avec leurs ressources, pour ma part j'ai puisé par exemple dans « [La boite de pandore des fichiers génétiques, regard croisé entre France et USA par l'histoire du meurtrier du Golden State](/pandore-adn) ou encore dans les [notes de recherche-action](https://notecc.frama.wiki/norae:biologicus:bioprivacy).

J'ai essayé d'illustrer les problématiques liées à la reconnaissance faciale avec une [démonstration en direct sur vidéo projecteur](https://notecc.frama.wiki/atelier:faceblind_facial_reco#python_et_reconnaissance_faciale_automatisee). La _facilité relative_ qui nécessite le test et la technologie, le maquillage _[dazzle](https://cvdazzle.com/) présenté parfois comme un moyen de camouflage et qui ne fonctionne quasiment pas, les faux positifs sur maquillage militaire, la reconnaissance faciale sur smartphone qui est déjouée par une simple photo...

![](/assets/images/fdln_faceblind.jpg)

Tiens donc... si on vous _vend_ de vous prendre en photo pour améliorer une sécurité mais que cette sécurité ne fonctionne pas, que reste t-il à part du fichage ? Et à quoi est destiné ce fichage ? À vous empêcher de désirer, d'espérer, d'imaginer agir, avant même que vous tentiez de passer à l'action ?

J'ai proposé de faire le même exercice rapide que pour la première séance de _code_ prévue : « Qui serait prête ou prêt à le refaire par elle ou lui-même ? Qui voudrait y apporter des modifications, des détournements, d'autres usages ? ». Les réponses et propositions ont fusé dans tous les sens. _Impossible de vous écrire tout cela_.

J'ai aussi posé une question telle que : « Il y a-t-il quelque chose qui vous choque dans la démo que je viens de faire ? ». Il s'agissait de s'apercevoir que les exemples étaient à l'écrasante majorité fait avec des portraits de personnes d'âge moyen à la peau claire. Ceci pour illustrer que nous avons des biais y compris dans nos tentatives de regain de libertés, surtout si nous définissons notre modèle de menace, et donc de défence, par le prisme de notre bulle sociale.

Nous avons tenté de ne pas rester ~~uniquement~~ binaires, nous avons tenté d'imaginer des cas de figure dans lesquels la surveillance par l'image, avec ou sans reconnaissance faciale, pourrait être « utile ».

### Cosmogonie

Pour conclure, après plus de 2 heures de plaisirs et d'échanges très nourrissants, tout doux, il restait une question. Cette question me vient du [Constance Social Club](https://constancesocialclub.org/), en Creuse. Elle vise à réimplanter chez nous et par nous-même des capacités à imaginer, à désirer par nous-même eloigné⋅e⋅s si possible des _manipulations douces_ de contrôle social. 

Cette question est simple mais potentiellement très déroutante : « **Quelle est votre [cosmogonie](https://constancesocialclub.org/) ?** Quelles sont vos légendes de création du monde et de récits de libertés pour non pas survivre mais bâtir d'autres Mondes ? Qu'advient alors si votre / notre cosmogonie devient _dominante_ ? »

Nous avons ainsi fini en rêves, en songes, en désirs, en fictions et utopies, citant aussi [l'Anté Monde](https://antemonde.org/) puisque nous étions en bibliothèque.

Ce moment étatait, et est toujours, tellement riche et beau qu'il presque impossible de l'écrire. Nous avons donc chacune et chacun emporté cet instant et ces particules avec nous pour les semer ailleurs, les essaimer dans d'autres Dehors.

Je vous invite à faire la même expérience remodelée à votre convenance. 


## Remerciements 

Pour ce formidable moment, qui retape et redonne des espoirs, je remercie les participantes et participants qui sont devenu⋅e⋅s des contributrices et contributeurs à cette séance, je remercie l'équipe de la bibliothèque de l'INSA, et l'équipe Exodus Privacy. 

## Notes de bas page

[^1]: Loi de la mobilité ou loi des deux pieds du forum ouvert <https://fr.wikipedia.org/wiki/M%C3%A9thodologie_Forum_Ouvert#M%C3%A9thode>

[^2]: Pour approfondir le sujet je conseille avec insistance « Je ne suis pas nulle et je le sais » <https://cotcotcot.eu/blog/je-ne-suis-pas-nulle-et-je-le-sais>

[^3]: Il y a plusieurs de couches de surveillances de corps <https://xavcc.frama.io/biononymous-bioprivacy-research>

